﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Finance;

namespace LaRenzo.ControlsLib.Grids
{
    public partial class PaymentGrid : UserControl
    {
        public Func<List<Payment>> GetData { get; set; }

        public PaymentGrid()
        {
            InitializeComponent();
            
        }

        public void LoadOrUpdateDate()
        {
            if (GetData == null) throw new Exception("Функция GetData не определена (равна null)");

            grid.DataSource = null;
            grid.DataSource = GetData.Invoke();
        }

        private void gridView1_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (e.RowHandle < 0 || view == null) return;
            
            var total = (decimal)view.GetRowCellValue(e.RowHandle, ColumnTotalOriginal);

            var payment = (Payment) view.GetRow(e.RowHandle);

            e.Appearance.BackColor = total >= 0 ? Color.LightGreen : Color.Salmon;

            if (payment.TransferID != null) e.Appearance.BackColor2 = Color.DeepSkyBlue;

        }

        private void grid_Click(object sender, EventArgs e)
        {

        }
    }
}
