﻿namespace LaRenzo.ControlsLib.Grids
{
    partial class PaymentGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grid = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColumnDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColumnWallet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColumnCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColumnTotalOriginal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColumnTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.Cursor = System.Windows.Forms.Cursors.Default;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.MainView = this.gridView1;
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(769, 548);
            this.grid.TabIndex = 0;
            this.grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.grid.Click += new System.EventHandler(this.grid_Click);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColumnDate,
            this.ColumnWallet,
            this.ColumnCategory,
            this.ColumnTotalOriginal,
            this.ColumnTotal,
            this.columnComment,
            this.columnUserName});
            this.gridView1.GridControl = this.grid;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", this.ColumnTotal, "{0} ₽")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            // 
            // ColumnDate
            // 
            this.ColumnDate.Caption = "Создан";
            this.ColumnDate.FieldName = "Created";
            this.ColumnDate.Name = "ColumnDate";
            this.ColumnDate.OptionsColumn.AllowEdit = false;
            this.ColumnDate.OptionsColumn.ReadOnly = true;
            this.ColumnDate.Visible = true;
            this.ColumnDate.VisibleIndex = 0;
            // 
            // ColumnWallet
            // 
            this.ColumnWallet.Caption = "Кошелек";
            this.ColumnWallet.FieldName = "Wallet.Name";
            this.ColumnWallet.Name = "ColumnWallet";
            this.ColumnWallet.OptionsColumn.AllowEdit = false;
            this.ColumnWallet.OptionsColumn.ReadOnly = true;
            this.ColumnWallet.Visible = true;
            this.ColumnWallet.VisibleIndex = 1;
            // 
            // ColumnCategory
            // 
            this.ColumnCategory.Caption = "Категория";
            this.ColumnCategory.FieldName = "Category.Name";
            this.ColumnCategory.Name = "ColumnCategory";
            this.ColumnCategory.OptionsColumn.AllowEdit = false;
            this.ColumnCategory.OptionsColumn.ReadOnly = true;
            this.ColumnCategory.Visible = true;
            this.ColumnCategory.VisibleIndex = 2;
            // 
            // ColumnTotalOriginal
            // 
            this.ColumnTotalOriginal.Caption = "Сумма со знаком";
            this.ColumnTotalOriginal.FieldName = "Total";
            this.ColumnTotalOriginal.Name = "ColumnTotalOriginal";
            this.ColumnTotalOriginal.OptionsColumn.AllowEdit = false;
            this.ColumnTotalOriginal.OptionsColumn.ReadOnly = true;
            // 
            // ColumnTotal
            // 
            this.ColumnTotal.Caption = "Сумма";
            this.ColumnTotal.FieldName = "Total";
            this.ColumnTotal.Name = "ColumnTotal";
            this.ColumnTotal.OptionsColumn.AllowEdit = false;
            this.ColumnTotal.OptionsColumn.ReadOnly = true;
            this.ColumnTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "{0} ₽")});
            this.ColumnTotal.UnboundExpression = "Abs([Total])";
            this.ColumnTotal.Visible = true;
            this.ColumnTotal.VisibleIndex = 3;
            // 
            // columnComment
            // 
            this.columnComment.Caption = "Комментарий";
            this.columnComment.FieldName = "Comment";
            this.columnComment.Name = "columnComment";
            this.columnComment.Visible = true;
            this.columnComment.VisibleIndex = 4;
            // 
            // columnUserName
            // 
            this.columnUserName.Caption = "Пользователь";
            this.columnUserName.FieldName = "User.Name";
            this.columnUserName.Name = "columnUserName";
            this.columnUserName.Visible = true;
            this.columnUserName.VisibleIndex = 5;
            // 
            // PaymentGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grid);
            this.Name = "PaymentGrid";
            this.Size = new System.Drawing.Size(769, 548);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn ColumnDate;
        private DevExpress.XtraGrid.Columns.GridColumn ColumnWallet;
        private DevExpress.XtraGrid.Columns.GridColumn ColumnCategory;
        private DevExpress.XtraGrid.Columns.GridColumn ColumnTotalOriginal;
        private DevExpress.XtraGrid.Columns.GridColumn ColumnTotal;
        private DevExpress.XtraGrid.Columns.GridColumn columnComment;
        private DevExpress.XtraGrid.Columns.GridColumn columnUserName;
    }
}
