﻿using System;
using System.Windows.Forms;

namespace LaRenzo.ControlsLib.Other
{
    public partial class NowYesterdayOtherDatePicker : UserControl
    {
        private DateTime _minDate;
        private DateTime _maxDate;

        public DateTime Date { get; set; }

        public DateTime MinDate
        {
            get { return _minDate; }
            set
            {
                _minDate = value;
                dateTimePicker1.MinDate = MinDate;
            }
        }

        public DateTime MaxDate
        {
            get { return _maxDate; }
            set
            {
                _maxDate = value;
                dateTimePicker1.MaxDate = value;
            }
        }

        #region events
        public event DateChangedEventHandler DateChanged;

        protected virtual void OnDateChanged()
        {
            var handler = DateChanged;
            if (handler != null) handler(this, new DateChangedEventArgs(Date));
        }

        public delegate void DateChangedEventHandler(object sender, DateChangedEventArgs e);
        #endregion

        public NowYesterdayOtherDatePicker()
        {
            InitializeComponent();
            Date = DateTime.Now;
            MaxDate = Date;
            MinDate = Date.AddDays(-30);
        }

        private void nowButton_Click(object sender, EventArgs e)
        {
            Date = DateTime.Now;
            OnDateChanged();
        }

        private void YesterdayButton_Click(object sender, EventArgs e)
        {
            Date = DateTime.Now.AddDays(-1);
            OnDateChanged();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Date = dateTimePicker1.Value;
            OnDateChanged();
        }

        


    }


    /// <summary>
    /// Event args for date changed event
    /// </summary>
    public class DateChangedEventArgs : EventArgs
    {
        public DateTime Date { get; set; }

        public DateChangedEventArgs(DateTime date): base()
        {
            Date = date;
        }
    }
}
