﻿using System.Drawing;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Tables;

namespace LaRenzo.ControlsLib.TableControls
{
	public partial class TableControl : UserControl
	{
		public TableControl()
		{
			InitializeComponent();

			InitializeStartState();
		}


		public Button TableButton
		{
			get
			{
				return btnMain;
			}
		}


		public Button Place1Button
		{
			get
			{
				return placeButton1;
			}
		}


		public Button Place2Button
		{
			get
			{
				return placeButton2;
			}
		}


		public Button Place3Button
		{
			get
			{
				return placeButton3;
			}
		}


		public Button Place4Button
		{
			get
			{
				return placeButton4;
			}
		}
		

		public Color OpenColor { set; get; }

		public Color CloseColor { set; get; }

		private void InitializeStartState()
		{
			TableState = TableState.Close;
		}

		private TableState state;
		private TableState statePlace1;
		private TableState statePlace2;
		private TableState statePlace3;
		private TableState statePlace4;

		public TableState TableState
		{
			set
			{
				btnMain.BackColor = value == TableState.Close ? CloseColor : OpenColor;

				state = value;
			}
			get
			{
				return state;
			}
		}

		public TableState Place1State
		{
			set
			{
				placeButton1.BackColor = value == TableState.Close ? CloseColor : OpenColor;
				statePlace1 = value;
			}
			get
			{
				return statePlace1;
			}
		}

		public TableState Place2State
		{
			set
			{
				placeButton2.BackColor = value == TableState.Close ? CloseColor : OpenColor;
				statePlace2 = value;
			}
			get
			{
				return statePlace2;
			}
		}

		public TableState Place3State
		{
			set
			{
				placeButton3.BackColor = value == TableState.Close ? CloseColor : OpenColor;
				statePlace3 = value;
			}
			get
			{
				return statePlace3;
			}
		}

		public TableState Place4State
		{
			set
			{
				placeButton4.BackColor = value == TableState.Close ? CloseColor : OpenColor;
				statePlace4 = value;
			}
			get
			{
				return statePlace4;
			}
		}

		public bool ShowTimer
		{
			set { lblTimer.Visible = value; }
			get { return lblTimer.Visible; }
		}

		public Point TimerPosition
		{
			set { lblTimer.Location = value; }
			get { return lblTimer.Location; }
		}

		public Font TimerFont
		{
			set { lblTimer.Font = value; }
			get { return lblTimer.Font; }
		}

		private Table _model;

		public Table Content
		{
			set
			{
				_model = value;
				btnMain.Text = value.Name;
			}
			get { return _model; }
		}
	}
}