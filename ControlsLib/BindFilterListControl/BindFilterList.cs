﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LaRenzo.ControlsLib.BindFilterListControl
{
    public partial class BindFilterList : UserControl
    {
        private List<BindListRow> _data = new List<BindListRow>();
        private List<BindListRow> _visibleData = new List<BindListRow>();
        private string _filter = string.Empty;

        private BindListRow _lastSelected;


        private bool _doGenerateSelectedRowChangedEvent = true;

        public event EventHandler<SelectedRowChangedEventArgs>
            SelectedRowChanged;

        public void OnSelectedRowChanged(SelectedRowChangedEventArgs e)
        {
            EventHandler<SelectedRowChangedEventArgs> handler =
                SelectedRowChanged;
            if (handler != null) handler(this, e);
        }


        private void SetData(IEnumerable<BindListRow> list)
        {
            _data = list.OrderBy(x => x.Weight).ThenBy(x => x.Value).ToList();
            SetVisibleData();
        }

        private void SetVisibleData()
        {
            _visibleData = string.IsNullOrWhiteSpace(Filter)
                               ? _data
                               : _data.Where(
                                   x =>
                                   x.Value.ToUpper().Contains(Filter.ToUpper()))
                                     .
                                     ToList();
            lbData.Items.Clear();
            var items = _visibleData.Select(x => x.Value).ToArray();
            lbData.Items.AddRange(items.Where(x=>x != null).ToArray());
        }


        public string Filter
        {
            get { return _filter; }
            set
            {
                _filter = value;
                SetVisibleData();
            }
        }

        public List<BindListRow> VisibleData
        {
            get { return _visibleData; }
        }

        public BindFilterList()
        {
            InitializeComponent();
            ResizeRedraw = false;
        }

        public void Clear()
        {
            SetData(new List<BindListRow>());
        }

        public void AddRange(List<BindListRow> rows)
        {
            SetData(rows);
        }

        public BindListRow GetLastSelected()
        {
            return _lastSelected;
        }

        public BindFilterList Bind(List<BindListRow> rows,
                                   bool saveSelection = true)
        {
            int selectedIndex = lbData.SelectedIndex;
            Clear();
            AddRange(rows);
            int maxIndex = lbData.Items.Count - 1;
            if (saveSelection && selectedIndex >= 0 && selectedIndex <= maxIndex )
            {
                _doGenerateSelectedRowChangedEvent = false;
                lbData.SelectedIndex = selectedIndex;
                _doGenerateSelectedRowChangedEvent = true;
            }

            return this;
        }

        public class BindListRow
        {
            public int ID { get; set; }
            public string Value { get; set; }
            public int Weight { get; set; }

            public BindListRow(int id, string value, int weight = 0)
            {
                ID = id;
                Value = value;
                Weight = weight;
            }

            public BindListRow()
            {
            }
        }

        private void tbFilter_TextChanged(object sender, EventArgs e)
        {
            Filter = tbFilter.Text;
        }

        private void lbData_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = lbData.SelectedIndex;

            if (index >= 0)
            {
                BindListRow obj = _visibleData.Skip(index).First();
                _lastSelected = obj;
                if (_doGenerateSelectedRowChangedEvent)
                {
                    OnSelectedRowChanged(new SelectedRowChangedEventArgs
                                             {
                                                 ID = obj.ID,
                                                 Value = obj.Value,
                                             });
                }
            }
        }


        private void tbFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Down)
            {
                if (_visibleData.Any()) lbData.SelectedIndex = 0;
                lbData.Focus();
                e.SuppressKeyPress = true;
            }

            if (e.KeyCode == Keys.Escape)
            {
                tbFilter.Text = string.Empty;
                e.SuppressKeyPress = true;
            }
        }

        public BindListRow GetSelected()
        {
            return lbData.SelectedIndex >= 0
                       ? _visibleData.Skip(lbData.SelectedIndex).FirstOrDefault()
                       : null;
        }

        public BindListRow SelectNext()
        {
            lbData.SelectedIndex++;
            if (lbData.SelectedIndex > lbData.Items.Count - 1)
                lbData.SelectedIndex = 0;
            return GetSelected();
        }

        public int GetFirstRowId()
        {
            return _data.First().ID;
        }

        public BindListRow SelectFirst()
        {
            if (lbData.Items.Count > 0)
            {
                lbData.SelectedIndex = 0;
                return GetSelected();
            }
            return null;
        }

        public void Deselect()
        {
            lbData.SelectedIndex = -1;
        }

        private void lbData_KeyDown(object sender, KeyEventArgs e)
        {
            if (lbData.SelectedIndex == 0 &&
                (e.KeyCode == Keys.Up || e.KeyCode == Keys.Left))
            {
                tbFilter.Focus();
                lbData.ClearSelected();
                e.SuppressKeyPress = true;
            }

            if (e.KeyCode == Keys.Escape)
            {
                tbFilter.Text = string.Empty;
                tbFilter.Focus();
                e.SuppressKeyPress = true;
            }
        }
    }

    public class SelectedRowChangedEventArgs : EventArgs
    {
        public int ID { get; set; }
        public string Value { get; set; }
    }
}

