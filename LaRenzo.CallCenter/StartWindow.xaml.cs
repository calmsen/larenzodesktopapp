﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LaRenzo.CallCenter.Authentification;
using LaRenzo.CallCenter.UserControls;

namespace LaRenzo.CallCenter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    public partial class StartWindow : Window, IView
    {
        public StartWindow(AuthenticationViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
        }

        public AuthenticationViewModel ViewModel { get; set; }


        public async Task Initialize()
        {
            await Task.Delay(2000);
            await OrdersTabControl.Initialization();
            var mainMenu = new MainMenu(ViewModel);
            Grid.SetColumn(mainMenu, 0);
            MainFormGrid.Children.Add(mainMenu);
        }
    }
}
