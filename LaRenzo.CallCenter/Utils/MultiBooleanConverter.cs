﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace LaRenzo.CallCenter.Utils
{
    [ValueConversion(typeof(bool), typeof(bool))]
    public class MultiBooleanConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            foreach (var value in values)
            {
                if (value != null && value != DependencyProperty.UnsetValue && !(bool)value)
                {
                    return false;
                }
            }
            return true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
