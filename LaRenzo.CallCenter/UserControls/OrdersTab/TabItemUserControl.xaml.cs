﻿using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.Forms.Documents.DeliveryOrderForms;

namespace LaRenzo.CallCenter.UserControls.OrdersTab
{
    /// <summary>
    /// Interaction logic for TabItemUserControl.xaml
    /// </summary>
    public partial class TabItemUserControl
    {
        public TabItemUserControl()
        {
            InitializeComponent();
            NewOrder.Click += async (s, e) => await NewOrder_Click();
        }

        public int BranchId { get; set; }
        public int BrandId { get; set; }
        public Brand Brand { get; set; }

        public async Task Initialize()
        {
            Brand = DataRepository.Repositories.Content.BrandManager.GetById(BrandId, false);
            DataGridOrders.ItemsSource = await Task.Run(() => LaRenzo.DataRepository.Repositories.Content.OrderManager.GetCurrentSessionList(BranchId, BrandId));
        }

        private async Task NewOrder_Click()
        {
            var dof = new DeliveryOrderForm
            {
                CurentBrand = Brand
            };
            await dof.Initialize();
            dof.ShowDialog();
            await Initialize();
        }

        private void PreOrders_Click(object sender, System.Windows.RoutedEventArgs e)
        {

        }
    }
}
