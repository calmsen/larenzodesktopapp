﻿using System.Windows;
using LaRenzo.CallCenter.Authentification;

namespace LaRenzo.CallCenter.UserControls
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu
    {
        public MainMenu(AuthenticationViewModel viewModel)
        {
            DataContext = viewModel;
            InitializeComponent();
        }
        
        private void Exit_OnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
