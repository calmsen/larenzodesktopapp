﻿using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Threading;
using DataLib.Models;
using LaRenzo.CallCenter.Authentification;
using NLog;

namespace LaRenzo.CallCenter
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            //Create a custom principal with an anonymous identity at startup
            CustomPrincipal customPrincipal = new CustomPrincipal();
            AppDomain.CurrentDomain.SetThreadPrincipal(customPrincipal);

            base.OnStartup(e);
            Database.SetInitializer<PizzaAppDB>(null);
            //Show the login view
            AuthenticationViewModel viewModel = new AuthenticationViewModel();
            IView loginWindow = new LoginWindow(viewModel);
            loginWindow.Show();
        }
        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Error(e.Exception);
            e.Handled = true;
        }
    }
}
