﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Controls;
using System.Security;
using System.Threading.Tasks;
using System.Windows;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories;

namespace LaRenzo.CallCenter.Authentification
{
    public interface IViewModel { }

    public class AuthenticationViewModel : IViewModel, INotifyPropertyChanged
    {
        private readonly DelegateCommand _loginCommand;
        private readonly DelegateCommand _logoutCommand;
        private string _username;
        private string _status;

        public AuthenticationViewModel()
        {
            _loginCommand = new DelegateCommand(Login, CanLogin);
            _logoutCommand = new DelegateCommand(Logout, CanLogout);
        }

        #region Properties
        public string Username
        {
            get { return _username; }
            set { _username = value; NotifyPropertyChanged("Username"); }
        }

        public string AuthenticatedUser
        {
            get
            {
                if (IsAuthenticated)
                    return string.Format("Вы успешно авторизовались: {0}.",
                          Thread.CurrentPrincipal.Identity.Name);
                else if (Authentication)
                {
                    return "Идёт авторизация...";
                }
                return "Введите логин и пароль:";
            }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; NotifyPropertyChanged("Status"); }
        }
        #endregion

        #region Commands
        public DelegateCommand LoginCommand { get { return _loginCommand; } }

        public DelegateCommand LogoutCommand { get { return _logoutCommand; } }

        #endregion

        private async Task Login(object parameter)
        {
            Authentication = true;
            NotifyPropertyChanged("Authentication");
            NotifyPropertyChanged("AuthenticatedUser");
            Status = String.Empty;
            if (parameter is PasswordBox passwordBox)
            {
                string clearTextPassword = passwordBox.Password;
                try
                {
                    //Validate credentials through the authentication service
                    var user = Content.UserManager.Login(Username, clearTextPassword);
                    if (user != null)
                    {
                        //Get the current principal object
                        CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
                        if (customPrincipal == null)
                            throw new ArgumentException(
                                "The application's default thread principal must be set to a CustomPrincipal object on startup.");

                        //Authenticate the user
                        customPrincipal.Identity = new CustomIdentity(user.Name, String.Empty, new string[] { });
                        //Update UI
                        NotifyPropertyChanged("AuthenticatedUser");
                        NotifyPropertyChanged("IsAuthenticated");
                        _loginCommand.RaiseCanExecuteChanged();
                        _logoutCommand.RaiseCanExecuteChanged();
                        Username = string.Empty; //reset
                        passwordBox.Password = string.Empty; //reset
                        Status = string.Empty;
                        Authentication = false;
                        await ShowView(new StartWindow(this));
                    }
                    else
                    {
                        Authentication = false;
                        Status = "Ошибка авторизации, такущая комбинация логин пароль не найдена";
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    Authentication = false;
                    Status = "Ошибка авторизации, такущая комбинация логин пароль не найдена";
                }
                catch (Exception ex)
                {
                    Authentication = false;
                    await Logout();
                    Status = string.Format("ERROR: {0}", ex.Message);
                }
            }

            NotifyPropertyChanged("AuthenticatedUser");
            NotifyPropertyChanged("Authentication");
        }

        private bool CanLogin()
        {
            return !IsAuthenticated;
        }

        private async Task Logout(object parameter = null)
        {
            if (Thread.CurrentPrincipal is CustomPrincipal customPrincipal)
            {
                customPrincipal.Identity = new AnonymousIdentity();
                NotifyPropertyChanged("AuthenticatedUser");
                NotifyPropertyChanged("IsAuthenticated");
                _loginCommand.RaiseCanExecuteChanged();
                _logoutCommand.RaiseCanExecuteChanged();
                Status = string.Empty;
                await ShowView(new LoginWindow(this));
            }
        }

        private bool CanLogout()
        {
            return IsAuthenticated;
        }

        public bool AllActionsDone
        {
            get;
            set;
        }

        public bool IsAuthenticated
        {
            get { return Thread.CurrentPrincipal.Identity.IsAuthenticated; }
        }

        public bool Authentication { get; set; }

        private async Task ShowView(Window window)
        {
            try
            {
                Status = string.Empty;
                await ((IView)window).Initialize();
                window.Show();
                AllActionsDone = true;
                Application.Current.Windows[0]?.Close();
            }
            catch (SecurityException)
            {
                Status = "Вы не авторизованны";
            }
        }


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
