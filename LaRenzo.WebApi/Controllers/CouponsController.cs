﻿using LaRenzo.WebApi.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.DataRepository.Repositories.Settings;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories.Catalogs.Dishes;
using DataLib.Models;

namespace LaRenzo.WebApi.Controllers
{
    [RoutePrefix("api/coupons")]
    public class CouponsController : ApiController
    {
        [Inject]
        public ICouponRepository CouponRepository { get; set; }
        [Inject]
        public IBrandManager BrandManager { get; set; }
        [Inject]
        public IOrderManager OrderManager { get; set; }
        [Inject]
        public IDishRepository DishRepository { get; set; }
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public CouponsController()
        {
        }

        [Route("apply")]
        [HttpPost]
        [Authorize(Roles = "InternalServices")]
        public async Task<ApplyCouponResponseDto> ApplyCoupon([FromBody] ApplyCouponRequestDto input)
        {
            try
            {

                string logMessage = "";
                _logger.Info(logMessage += $"Получение информации о купоне по коду {input.Code}. Входные данные: {JsonConvert.SerializeObject(input)}\r\n");
                if (string.IsNullOrEmpty(input.Code))
                {
                    _logger.Info(logMessage += $"Код купона не передан. Возвращаем сумму как есть {input.Sum}\r\n");
                    return new ApplyCouponResponseDto
                    {
                        Sum = input.Sum,
                        LogMessage = logMessage
                    };
                }
                var coupon = CouponRepository.GetCouponByCode(input.Code);
                if (coupon == null)
                {
                    _logger.Info(logMessage += $"Информация о купоне {input.Code} не найдена. Возвращаем сумму как есть {input.Sum}\r\n");
                    return new ApplyCouponResponseDto
                    {
                        Sum = input.Sum,
                        LogMessage = logMessage
                    }; ;
                }
                _logger.Info(logMessage += $"Получение информации о бренде {input.Brand}\r\n");
                var brand = BrandManager.GetByName(input.Brand ?? "Ларензо");
                if (brand == null)
                {
                    _logger.Info(logMessage += $"Информация о бренде {input.Brand} не найдена. Возвращаем сумму как есть {input.Sum}\r\n");
                    return new ApplyCouponResponseDto
                    {
                        Sum = input.Sum,
                        LogMessage = logMessage,
                        Description = OrderManager.RuleToString(coupon.CouponRule)
                    };
                }
                if (input.Items == null || input.Items.Length == 0)
                {
                    _logger.Info(logMessage += $"Позиции заказа не переданы. Возвращаем сумму как есть {input.Sum}\r\n");
                    return new ApplyCouponResponseDto
                    {
                        Sum = input.Sum,
                        LogMessage = logMessage,
                        Description = OrderManager.RuleToString(coupon.CouponRule)
                    };
                }
                var skuArray = input.Items.Distinct().ToArray();
                _logger.Info(logMessage += $"Получение информации о блюдах {input.Brand}\r\n");
                var items = DishRepository
                    .GetItems(new DishFilter { FilterBySKU = true, SKUArray = skuArray, BrandId = brand.ID }).ToList();
                if (items.Count() == 0)
                {
                    _logger.Info(logMessage += $"Позиции заказа не найдены. Возвращаем сумму как есть {input.Sum}\r\n");
                    return new ApplyCouponResponseDto
                    {
                        Sum = input.Sum,
                        LogMessage = logMessage,
                        Description = OrderManager.RuleToString(coupon.CouponRule)
                    };
                }
                items = input.Items.Select(x => items.First(y => y.SKU == x)).ToList();
                _logger.Info(logMessage += $"Применяем купон {input.Code} к сумме {input.Sum}\r\n");
                var sum = await OrderManager.ApplyCoupon(input.Sum, coupon, items);

                if (sum == input.Sum)
                    _logger.Info(logMessage += $"Сумма {input.Sum} после применения купона {input.Code} не изменилась.\r\n");
                else
                    _logger.Info(logMessage += $"Сумма {input.Sum} после применения купона {input.Code} изменилась на {sum}.\r\n");
                
                return new ApplyCouponResponseDto
                {
                    Sum = sum,
                    LogMessage = logMessage,
                    Description = OrderManager.RuleToString(coupon.CouponRule)
                };
            }
            catch (Exception e)
            {
                _logger.Error($"Произошла ошибка при обработке купона {input.Code}. Входные данные: {JsonConvert.SerializeObject(input)}");
                _logger.Error(e);
                throw;
            }
            
        }
    }
}
