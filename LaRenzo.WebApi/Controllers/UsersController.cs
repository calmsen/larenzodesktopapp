﻿using LaRenzo.WebApi.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.DataRepository.Repositories.Settings;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Repositories.Catalogs.Drivers;
using LaRenzo.DataRepository.Repositories.Others;
using DataLib.Models;

namespace LaRenzo.WebApi.Controllers
{
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IDriverManager DriverManager { get; set; }
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public UsersController()
        {
        }

        [Route("me")]
        [HttpGet]
        [Authorize]
        public UserDto GetUser()
        {
            try
            {
                _logger.Info($"Получение информации о пользователе {User.Identity.Name}\r\n");
                User user = UserManager.GetByName(User.Identity.Name);
                if (user != null)
                {
                    return new UserDto
                    {
                        Id = user.ID,
                        Name = user.Name
                    };
                }
                Driver driver = DriverManager.GetByName(User.Identity.Name);
                if (driver != null)
                {
                    return new UserDto
                    {
                        Id = driver.ID,
                        Name = driver.Name
                    };
                }
                _logger.Error("Пользователь не найден. Возможно был удален или было изменено имя.");
                throw new Exception("Пользователь не найден. Возможно был удален или было изменено имя.");

            }
            catch (Exception e)
            {
                _logger.Error($"Произошла ошибка при получении информации о пользователе {User.Identity.Name}");
                _logger.Error(e);
                throw;
            }
            
        }        

        [Route("me/password")]
        [HttpPut]
        [Authorize]
        public void ChangePassword(ChangePasswordRequestDto input)
        {
            if (string.IsNullOrEmpty(input.CurrentPassword))
            {
                ModelState.AddModelError("EmptyCurrentPassword", "currentPassword must not be null.");
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                throw new HttpResponseException(response);
            }

            if (string.IsNullOrEmpty(input.NewPassword))
            {
                ModelState.AddModelError("EmptyNewPassword", "newPassword must not be null.");
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                throw new HttpResponseException(response);
            }

            if (input.NewPassword.Length < 5)
            {
                ModelState.AddModelError("InvalidNewPassword", "newPassword must be at least 5 characters.");
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                throw new HttpResponseException(response);
            }

            string currentPassword = new Hasher().GetHashString(input.CurrentPassword);
            string newPassword = new Hasher().GetHashString(input.NewPassword);

            User user = UserManager.GetByName(User.Identity.Name);
            if (user != null)
            {
                ValidateCurrentPassword(currentPassword, user.Password);
                user.Password = newPassword;
                UserManager.Update(user);
                return;
            }

            Driver driver = DriverManager.GetByName(User.Identity.Name);
            if (driver != null)
            {
                ValidateCurrentPassword(currentPassword, driver.Password);
                driver.Password = newPassword;
                DriverManager.Update(driver);
                return;
            }
            ModelState.AddModelError("NotFoundUser", "User is not found");
            var notFoundUserResponse = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            throw new HttpResponseException(notFoundUserResponse);
        }

        private void ValidateCurrentPassword(string currentPassword,string dbPassword)
        {
            if (!string.Equals(currentPassword, dbPassword))
            {
                ModelState.AddModelError("InvalidCurrentPassword", "CurrentPassword is invalid");
                var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                throw new HttpResponseException(response);
            }
        }
    }
}
