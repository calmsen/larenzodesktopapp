﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LaRenzo.WebApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public ActionResult Index()
        {
            _logger.Debug("Home Page");
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
