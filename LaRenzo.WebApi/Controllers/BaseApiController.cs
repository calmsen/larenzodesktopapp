﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace LaRenzo.WebApi.Controllers
{
    public class BaseApiController: ApiController
    {
        /// <summary>
        /// Id текущего залогиненного пользователя
        /// </summary>
        protected int UserId
        {
            get
            {
                try
                {
                    var value = ((ClaimsIdentity)User.Identity).Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                    if (value == null)
                        return 0;
                    return Convert.ToInt32(value);
                }
                catch
                {
                    return 0;
                }
            }
        }
    }
}