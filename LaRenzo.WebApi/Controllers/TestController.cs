﻿using DataLib.Models;
using LaRenzo.DataRepository.Repositories.Settings;
using System;
using System.Web.Http;

namespace LaRenzo.WebApi.Controllers
{
    [RoutePrefix("api/test")]
    public class TestController : ApiController
    {
        public TestController()
        {
        }

        [Inject]
        public IProgrammVersionRepository ProgrammVersionRepository { get;set; }

        [Route("")]
        [HttpGet]
        public string Test()
        {
            return "TEST";
        }

        [Route("auth")]
        [HttpGet]
        [Authorize]
        public string TestAuth()
        {
            return $"TEST AUTH: {User.Identity.Name}";
        }

        [Route("internal-services")]
        [HttpGet]
        [Authorize(Roles = "InternalServices")]
        public string TestInternalServices()
        {
            return $"TEST INTERNAL SERVICES: {User.Identity.Name} has role InternalServices";
        }

        [Route("connect-to-db")]
        [HttpGet]
        public string TestConnectToDb()
        {
            try
            {
                ProgrammVersionRepository.GetItems();
                return "TEST SUCCESS";
            }
            catch (Exception e)
            {
                return $"TEST ERROR: {e.Message}: {e.StackTrace}";
            }            
        }
    }
}
