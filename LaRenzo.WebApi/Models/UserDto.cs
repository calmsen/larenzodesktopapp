﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaRenzo.WebApi.Models
{
    public class UserDto
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
    }
}