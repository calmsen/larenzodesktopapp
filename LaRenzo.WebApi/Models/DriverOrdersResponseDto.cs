﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaRenzo.WebApi.Models
{
    public class DriverOrdersResponseDto
    {
        public List<DriverOrderDto> Orders { get; set; } = new List<DriverOrderDto>();
    }
}