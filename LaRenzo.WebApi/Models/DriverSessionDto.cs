﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaRenzo.WebApi.Models
{
    public class DriverSessionDto
    {
        public int ID { get; set; }
        
        public string Opened { get; set; }

        public string Closed { get; set; }
    }
}