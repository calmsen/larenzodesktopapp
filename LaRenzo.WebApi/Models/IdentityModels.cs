﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace LaRenzo.WebApi.Models
{
    public class ApplicationUser : IUser
    {
        public string Id { get; set; }
        
        public string UserName { get; set; } 

        public List<string> Roles { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            if (Roles != null && Roles.Count > 0)
            {
                foreach (var role in Roles)
                {
                    userIdentity.AddClaim((new Claim(ClaimTypes.Role, role)));
                }
            }
            return userIdentity;
        }
    }
}