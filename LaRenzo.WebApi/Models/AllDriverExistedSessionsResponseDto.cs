﻿using LaRenzo.DataRepository.Entities.Documents.Sessions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaRenzo.WebApi.Models
{
    public class AllDriverExistedSessionsResponseDto
    {
        public List<DriverSessionDto> Sessions { get; set; }
    }
}