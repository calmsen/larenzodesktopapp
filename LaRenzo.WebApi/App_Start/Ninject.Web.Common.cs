[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(LaRenzo.WebApi.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(LaRenzo.WebApi.App_Start.NinjectWebCommon), "Stop")]

namespace LaRenzo.WebApi.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;
    using Ninject.Extensions.Conventions;
    using System.Reflection;
    using DataLib.Models;
    using LaRenzo.Interfaces;
    using LaRenzo.DataRepository;
    using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
    using Ninject.Web.WebApi;
    using System.Web.Http;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel(new NinjectSettings
            {
                InjectAttribute = typeof(DataLib.Models.InjectAttribute)
            });
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind(x => x
            .From(Assembly.GetAssembly(typeof(PizzaAppDB)))
            .SelectAllClasses()
            .EndingWith("Repository")
            .BindDefaultInterface()
            .Configure(b => b.InSingletonScope()));

            kernel.Bind(x => x
            .From(Assembly.GetAssembly(typeof(PizzaAppDB)))
            .SelectAllClasses()
            .EndingWith("Manager")
            .BindDefaultInterface()
            .Configure(b => b.InSingletonScope()));

            kernel.Bind<IThreadLocalStorage>().To<ThreadLocalStorage>();
            kernel.Bind<IBaseOptionsFunctions>().To<BaseOptionsFunctions>();
            kernel.Bind<ISignalProcessor>().To<SignalProcessor>();
        }        
    }
}