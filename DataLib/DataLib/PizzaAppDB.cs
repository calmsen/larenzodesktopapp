﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using LaRenzo.DataRepository;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Branch;
using LaRenzo.DataRepository.Entities.Calls;
using LaRenzo.DataRepository.Entities.Catalogs;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Entities.Catalogs.BlackList;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Client;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Entities.Catalogs.Printers;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Catalogs.Roles;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;
using LaRenzo.DataRepository.Entities.Catalogs.Tables;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.Inventories;
using LaRenzo.DataRepository.Entities.Documents.Moving;
using LaRenzo.DataRepository.Entities.Documents.Posting;
using LaRenzo.DataRepository.Entities.Documents.Returns;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Entities.Documents.Supply;
using LaRenzo.DataRepository.Entities.Documents.WebOrders;
using LaRenzo.DataRepository.Entities.Documents.Writeoff;
using LaRenzo.DataRepository.Entities.Documents.WriteoffDish;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Entities.Others.AmountFactories;
using LaRenzo.DataRepository.Entities.Others.DeliverySchedulies;
using LaRenzo.DataRepository.Entities.Others.GlobalOptions;
using LaRenzo.DataRepository.Entities.Others.HelpPages;
using LaRenzo.DataRepository.Entities.Others.TimeSheets;
using LaRenzo.DataRepository.Entities.PrintWorksModels;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Entities.Wirings;
using Address = LaRenzo.DataRepository.Entities.Catalogs.Addresses.Address;
using Message = LaRenzo.DataRepository.Entities.Others.Messages.Message;

// ReSharper disable CheckNamespace
namespace DataLib.Models
// ReSharper restore CheckNamespace
{
    // ReSharper disable InconsistentNaming
    public class PizzaAppDB : DbContext
    // ReSharper restore InconsistentNaming
    {
        public PizzaAppDB() : base("BasicCS")
        {
            Database.CommandTimeout = 20;
        }

        // This method is required for proper operation PizzaAppDB.Views.tt
        public PizzaAppDB(int timeout) : base("BasicCS")
        {
            Database.CommandTimeout = timeout;
            Database.Log = Logger;
        }

        /// <summary>
        /// Настройка логирования для дебага
        /// </summary>
        /// <param name="logString"></param>
        private void Logger(string logString)
        {
            Debug.WriteLine(logString);
        }

        public static PizzaAppDB GetNewContext(int timeount = 20)
        {
            var context = new PizzaAppDB(timeount);
            context.Configuration.ProxyCreationEnabled = false;
            context.Configuration.LazyLoadingEnabled = false;
            return context;
        }

        public DbSet<Brand> Brands { get; set; }
        public DbSet<CallData> CallData { get; set; }
        public DbSet<Branch> Branch { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ProgrammVersion> ProgrammVersion { get; set; }
        public DbSet<ClientPhone> ClientPhones { get; set; }
        public DbSet<ClientAddress> ClientAddresses { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<AdditionalOfferDish> AdditionalOfferDishes { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderState> OrderStates { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<GlobalOption> GlobalOptions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<WarehouseForDish> WarehouseForDishes { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<PrintWork> PrintWorks { get; set; }
        public DbSet<DetailPrintWork> DetailPrintWorks { get; set; }
        public DbSet<AddressGroup> AddressGroups { get; set; }
        public DbSet<DeliverySchedule> DeliverySchedules { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<SetParams> SetParamses { get; set; }
        public DbSet<SetOfDishes> SetOfDisheses { get; set; }
        public DbSet<ItemOfProductSet> ItemOfProductSets { get; set; }
        public DbSet<SupplyDocument> SupplyDocuments { get; set; }
        public DbSet<SupplyDocumentItem> SupplyDocumentItems { get; set; }
        public DbSet<TimeSheet> TimeSheets { get; set; }
        public DbSet<Printer> Printers { get; set; }
        public DbSet<PrinterTask> PrinterTasks { get; set; }
        public DbSet<AmountFactor> AmountFactors { get; set; }
        public DbSet<AmountFactorCollection> AmountFactorCollections { get; set; }
        public DbSet<WebOrder> WebOrders { get; set; }
        public DbSet<WebOrderItem> WebOrderItems { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<PreOrder> PreOrders { get; set; }
        public DbSet<DiscountCard> DiscountCards { get; set; }
        public DbSet<RemainsOfGoodWirings> RemainsOfGoodWirings { get; set; }
        public DbSet<ReturnDocument> ReturnDocuments { get; set; }
        public DbSet<ReturnDocumentItem> ReturnDocumentItems { get; set; }
        public DbSet<MovingDocument> MovingDocuments { get; set; }
        public DbSet<MovingDocumentItem> MovingDocumentItems { get; set; }
        public DbSet<WriteoffDocument> WriteoffDocuments { get; set; }
        public DbSet<WriteoffDocumentItem> WriteoffDocumentItems { get; set; }
        public DbSet<PostingDocument> PostingDocuments { get; set; }
        public DbSet<PostingDocumentItem> PostingDocumentItems { get; set; }
        public DbSet<ItemOfDishSet> ItemOfDishSets { get; set; }
        public DbSet<SessionDocumentItem> SessionDocumentItems { get; set; }
        public DbSet<InventoryDocument> InventoryDocuments { get; set; }
        public DbSet<InventoryDocumentItem> InventoryDocumentItems { get; set; }
        public DbSet<SettlsWithPartnerWirings> SettlsWithPartnerWirings { set; get; }
        public DbSet<AveragePriceWirings> AveragePriceWiringses { set; get; }
        public DbSet<WriteoffDishDocument> WriteoffDishDocuments { set; get; }
        public DbSet<WriteoffDishDocumentItem> WriteoffDishDocumentItems { set; get; }
        public DbSet<Table> Tables { set; get; }
        public DbSet<CafeOrderDocument> CaffeOrderDocuments { set; get; }
        public DbSet<CafeOrderDocumentItem> CafeOrderDocumentItems { set; get; }
        public DbSet<PartnerOfProduct> PartnerOfProducts { set; get; }
        public DbSet<IntermediateGoodWirings> IntermediateGoodWiringses { set; get; }
        public DbSet<BlackListItem> BlackListItems { set; get; }
        public DbSet<HelpPage> HelpPages { get; set; }
        public DbSet<UserAccess> UserAccesses { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaymentCategory> PaymentCategories { get; set; }
        public DbSet<BankInteraction> BankInteractions { get; set; }
        public DbSet<PointOfSale> PointOfSales { get; set; }
        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<CouponRule> CouponRules { get; set; }
        public DbSet<CouponRuleElement> CouponRuleElements { get; set; }
        public DbSet<CouponDiscount> CouponDiscounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            // Disable Cascade deleting on ProductCategory_DefaultMeasure FK.
            modelBuilder.Entity<ProductCategory>()
                 .HasRequired(x => x.DefaultMeasure)
                 .WithMany().WillCascadeOnDelete(false);

            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));
            modelBuilder.Entity<ItemOfDishSet>().Property(x => x.Amount).HasPrecision(18, 3);
            modelBuilder.Entity<ItemOfProductSet>().Property(x => x.Amount).HasPrecision(18, 3);

            modelBuilder.Entity<InventoryDocumentItem>().Property(x => x.Amount).HasPrecision(18, 3);
            modelBuilder.Entity<InventoryDocumentItem>().Property(x => x.AmountFact).HasPrecision(18, 3);
            modelBuilder.Entity<InventoryDocumentItem>().Property(x => x.Delta).HasPrecision(18, 3);

            modelBuilder.Entity<MovingDocumentItem>().Property(x => x.Amount).HasPrecision(18, 3);

            modelBuilder.Entity<PostingDocumentItem>().Property(x => x.Amount).HasPrecision(18, 3);

            modelBuilder.Entity<ReturnDocumentItem>().Property(x => x.Amount).HasPrecision(18, 3);

            modelBuilder.Entity<SupplyDocumentItem>().Property(x => x.Amount).HasPrecision(18, 3);

            modelBuilder.Entity<WriteoffDocumentItem>().Property(x => x.Amount).HasPrecision(18, 3);

            modelBuilder.Entity<RemainsOfGoodWirings>().Property(x => x.Amount).HasPrecision(18, 3);

            modelBuilder.Entity<Product>().Property(x => x.MinAmount).HasPrecision(18, 3);
        }
    }
}
