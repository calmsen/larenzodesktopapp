﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Settings;using System.Data.Entity;
using LaRenzo.DataRepository.Entities.Catalogs.Client;
using LaRenzo.DataRepository.Repositories.Reports;
using LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport;
using LaRenzo.DataRepository.Repositories.Reports.SalesReport;
using LaRenzo.DataRepository.Repositories.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.DataRepository.Repositories.Catalogs.Dishes;

namespace LaRenzo.DataRepository.Repositories.Others.Statistics
{
    public class StatisticManager: IStatisticManager
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IAddressManager AddressManager { get; set; }
        [Inject]
        public IDishRepository DishRepository { get; set; }
        [Inject]
        public IBrandManager BrandManager { get; set; }

        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Произвести отчёт по продаджам </summary>
        ////==================================================
        public List<SaleReportRecord> GetSalesReportData(DateTime start, DateTime end, int mode, bool isActiveDish)
        {
            start = start.AddMinutes(-1);
            end = end.AddMinutes(1);
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<SaleReportRecord>();

                var source = DishRepository.GetDishListWithCostPrice();

                var addresses = AddressManager.GetAddressList();

                // Если не кафе отчёт
                if (mode != 1)
                {
                    List<Brand> brandList = BrandManager.GetList();
                    var orderStates = db.OrderStates
                    .FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.Name == "Оплачен");
                    var orders = db.OrderItems
                        .Include(x => x.Order)
                        .Include(x => x.Order.Brand)
                        .Include(x => x.Order.PointOfSale)
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.Order.Session.Opened > start && x.Order.Session.Closed < end && x.Order.State.ID == orderStates.ID)
                        .ToList();
                    result.AddRange(
                            from order in orders 
                            join dishInfo in source on order.DishID equals dishInfo.Item1.ID
                            select new SaleReportRecord
                            {
                                Dish = dishInfo.Item1,
                                Price = dishInfo.Item1.Price,
                                Cost = dishInfo.Item2,
                                Amount = order.Amount,
                                Brand = order.Order.Brand,
                                PointOfSale = order.Order.PointOfSale.PointName,
                                CreatedDate = order.Order.Created,
                                CreatedMonth = order.Order.Created.ToString("yyyy MMMM"),
                                CreatedDayOfWeek = order.Order.Created.ToString("dddd"),
                                CreatedHour = order.Order.Created.ToString("HH ч."),
                                User = order.Order.User,
                                AddressGroup = addresses.FirstOrDefault(x => x.Name == order.Order.Street)?.AddressGroup?.Name
                            });
                    GC.Collect();
                }

                // Если кафе отчёт
                if (mode != 2)
                {
                    var cafeOrders = db.CafeOrderDocumentItems
                        .Include(x => x.CafeOrderDocument)
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId &&
                                    x.CafeOrderDocument.Created > start &&
                                    x.CafeOrderDocument.Created <= end && 
                                    x.CafeOrderDocument.IsProcessed)
                        .ToList();
                    result.AddRange(from order in cafeOrders
                                    join dishInfo in source on order.DishID equals dishInfo.Item1.ID
                                    select new SaleReportRecord
                                    {
                                        Dish = dishInfo.Item1,
                                        IsCafe = true,
                                        Price = dishInfo.Item1.CafePrice,
                                        Cost = dishInfo.Item2,
                                        Amount = order.Amount,
                                        CreatedDate = order.CafeOrderDocument.Created,
                                        CreatedMonth = order.CafeOrderDocument.Created.ToString("yyyy MMMM"),
                                        CreatedDayOfWeek = order.CafeOrderDocument.Created.ToString("dddd"),
                                        CreatedHour = order.CafeOrderDocument.Created.ToString("HH ч."),
                                        User = order.CafeOrderDocument.User
                                    });

                    GC.Collect();
                }
                if (!isActiveDish)
                {
                    var rest = source
                        .Where(x => !result.Any(y => y.Dish.ID == x.Item1.ID))
                        .Where(x => !x.Item1.IsDeleted)
                        .Where(x => x.Item1.Price > 0)
                        .Select(x => new SaleReportRecord
                        {
                            Dish = x.Item1,
                            Price = x.Item1.Price,
                            Cost = x.Item2
                        });
                    result.AddRange(rest);
                }
                
                return result.ToList();
            }
        }

        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Произвести отчёт по продаджам </summary>
        ////==================================================
        public List<SaleReportRecord> GetSimpleSalesReportData(int sessionId, ReportModeEnum mode)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<SaleReportRecord>();
                if (mode == ReportModeEnum.Delivery || mode == ReportModeEnum.All)
                {
                    var orderStates = db.OrderStates.FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.Name == "Оплачен");
                    var orders = db.OrderItems.Include(x => x.Dish).Include(x => x.Order).Include(x => x.Order.Brand).Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.Order.SessionID == sessionId && x.Order.State.ID == orderStates.ID).ToList();

                    foreach (var order in orders)
                    {
                        var sale = new SaleReportRecord
                        {
                            Dish = order.Dish,
                            Price = order.Dish.Price,
                            Amount = order.Amount,
                            Cost = order.Amount * order.Dish.Price,
                            Brand = order.Order.Brand
                        };
                        result.Add(sale);
                    }
                    GC.Collect();
                }

                if (mode == ReportModeEnum.Cafe || mode == ReportModeEnum.All)
                {
                    var cafeOrders = db.CafeOrderDocumentItems.Include(x => x.Dish).Include(x => x.CafeOrderDocument).Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.CafeOrderDocument.SessionID == sessionId && x.CafeOrderDocument.IsProcessed);
                    foreach (var order in cafeOrders)
                    {
                        var sale = new SaleReportRecord
                        {
                            Dish = order.Dish,
                            Price = order.Dish.Price,
                            Amount = order.Amount,
                            Cost = order.Amount * order.Dish.Price,
                            IsCafe = true
                        };
                        result.Add(sale);
                    }

                    GC.Collect();
                }
                var finalResult = new List<SaleReportRecord>();
                foreach (var groupResultElement in result.GroupBy(x => x.Dish))
                {
                    var tempResult = new SaleReportRecord();
                    foreach (var groupTempResult in groupResultElement.Where(x => x.IsCafe))
                    {
                        tempResult.Dish = groupTempResult.Dish;
                        tempResult.Brand = groupTempResult.Brand;
                        tempResult.Amount += groupTempResult.Amount;
                        tempResult.Cost += groupTempResult.Cost;
                        tempResult.IsCafe = groupTempResult.IsCafe;
                        tempResult.Price = groupTempResult.Price;
                    }

                    if (tempResult.Dish != null)
                    {
                        finalResult.Add(tempResult);
                    }

                    tempResult = new SaleReportRecord();
                    foreach (var groupTempResult in groupResultElement.Where(x => !x.IsCafe))
                    {
                        tempResult.Dish = groupTempResult.Dish;
                        tempResult.Brand = groupTempResult.Brand;
                        tempResult.Amount += groupTempResult.Amount;
                        tempResult.Cost += groupTempResult.Cost;
                        tempResult.IsCafe = groupTempResult.IsCafe;
                        tempResult.Price = groupTempResult.Price;
                    }

                    if (tempResult.Dish != null)
                    {
                        finalResult.Add(tempResult);
                    }
                }
                return finalResult;
            }
        }

        public List<ClientsReportByOrders> GetNotActiveClientReportData(DateTime from, DateTime to)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<ClientsReportByOrders>();
                var orders = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
                var allOrdersGroupedByPhone = orders.GroupBy(x => x.Phone).ToList();
                var ordersForPeriod = orders.Where(x => x.Created > from && x.Created < to);
                var ordersAfterPerios = orders.Where(x => x.Created > to).GroupBy(x => x.Phone).ToList();
                var groupedByPhone = ordersForPeriod.GroupBy(x => x.Phone);
                foreach (var groupedOrder in groupedByPhone)
                {
                    if (ordersAfterPerios.All(x => x.Key != groupedOrder.Key))
                    {
                        result.Add(
                            new ClientsReportByOrders
                            {
                                ClientName = groupedOrder.FirstOrDefault()?.Name,
                                ClientPhone = groupedOrder.Key,
                                OrdersCount = allOrdersGroupedByPhone.FirstOrDefault(x => x.Key == groupedOrder.Key)?.Count() ?? 0,
                                OrdersCountInPeriod = groupedOrder.Count(),
                                LastOrderData = groupedOrder.Max(x => x.Created)
                            });
                    }
                }
                return result;
            }

        }

        public List<ClientsReport> GetAllClients()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<ClientsReport>();
                var clients = db.Clients.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
                var clientPhones = db.ClientPhones.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
                var clientAddresses = db.ClientAddresses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
                foreach (var client in clients)
                {
                    var phones = clientPhones.Where(x => x.ClientId == client.ID).Select(x => x.Phone);
                    var address = clientAddresses.Where(x => x.ClientId == client.ID).FirstOrDefault();
                    var clientReport = new ClientsReport
                    {
                        ClientName = client.LastName + " " + client.FirstName + " " + client.MiddleName,
                        ClientPhone = string.Join(",", phones),
                        ClientAddress = GetAddressFromObject(address),
                        Email = client.Email,
                        Gender = client.Gender
                    };
                    if (client.BirthDate != null)
                    {
                        clientReport.BirthDate = client.BirthDate.Value;
                    }

                    result.Add(clientReport);
                }
                return result;
            }
        }

        private string GetAddressFromObject(ClientAddress address)
        {
            if (address != null)
            {
                var result = address.Street;
                result = result + " " + address.House + ",";
                result = result + " - " + address.Appartament + ";";
                return result;
            }
            return String.Empty;
        }

        public List<ClientsReportByOrders> GetNewClientReportData(DateTime from, DateTime to)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<ClientsReportByOrders>();
                var orders = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
                var allOrdersGroupedByPhone = orders.GroupBy(x => x.Phone).ToList();
                var ordersForPeriod = orders.Where(x => x.Created > from && x.Created < to);
                var ordersBeforePerios = orders.Where(x => x.Created < from).GroupBy(x => x.Phone).ToList();
                var groupedByPhone = ordersForPeriod.GroupBy(x => x.Phone);
                foreach (var groupedOrder in groupedByPhone)
                {
                    if (ordersBeforePerios.All(x => x.Key != groupedOrder.Key))
                    {
                        result.Add(
                            new ClientsReportByOrders
                            {
                                ClientName = groupedOrder.FirstOrDefault()?.Name,
                                ClientPhone = groupedOrder.Key,
                                OrdersCount = allOrdersGroupedByPhone.FirstOrDefault(x => x.Key == groupedOrder.Key)?.Count() ?? 0,
                                OrdersCountInPeriod = groupedOrder.Count(),
                                LastOrderData = groupedOrder.Max(x => x.Created)
                            });
                    }
                }
                return result;
            }
        }

        public DeliveryTimeReport GetDeliveryTimeReportData(DateTime from, DateTime to)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new DeliveryTimeReport();
                result.StreetsList = new List<DeliveryTimeReportGrid>();
                var orders = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId && !x.IsWindow);
                var ordersForPeriod = orders.Where(x => x.Created > from && x.Created < to && x.IsNearTime).ToList();
                result.CookingTime = 0;
                result.DeliveryTime = 0;
                int cookingCount = 0;
                int deliveryCount = 0;
                foreach (var order in ordersForPeriod)
                {
                    if (order.ChangeToOnWay != null)
                    {
                        cookingCount++;
                        var cookingTime = order.ChangeToOnWay.Value - order.Created;
                        result.CookingTime += (double)cookingTime.Seconds / 60 + cookingTime.Minutes + cookingTime.Hours * 60 + cookingTime.Days * 24 * 60;
                    }
                    if (order.ChangeToPaid != null && order.ChangeToOnWay != null)
                    {
                        deliveryCount++;
                        var deliveryTime = order.ChangeToPaid.Value - order.ChangeToOnWay.Value;
                        result.DeliveryTime += (double)deliveryTime.Seconds / 60 + deliveryTime.Minutes + deliveryTime.Hours * 60 + deliveryTime.Days * 24 * 60;
                    }
                }

                if (cookingCount > 0)
                {
                    result.CookingTimeCount = cookingCount;
                    result.CookingTime = Math.Round(result.CookingTime / cookingCount, 2);
                }

                if (deliveryCount > 0)
                {
                    result.DeliveryTimeCount = deliveryCount;
                    result.DeliveryTime = Math.Round(result.DeliveryTime / deliveryCount, 2);
                }

                var groupedByStreets = ordersForPeriod.GroupBy(x => x.Street);
                foreach (var groupedByStreet in groupedByStreets)
                {
                    var statByStreet = new DeliveryTimeReportGrid();
                    statByStreet.Steet = groupedByStreet.Key;
                    cookingCount = 0;
                    deliveryCount = 0;
                    foreach (var order in groupedByStreet)
                    {
                        if (order.ChangeToOnWay != null)
                        {
                            cookingCount++;
                            var cookingTime = order.ChangeToOnWay.Value - order.Created;
                            statByStreet.CookingTime += (double)cookingTime.Seconds / 60 + cookingTime.Minutes + cookingTime.Hours * 60 + cookingTime.Days * 24 * 60;
                        }
                        if (order.ChangeToPaid != null && order.ChangeToOnWay != null)
                        {
                            deliveryCount++;
                            var deliveryTime = order.ChangeToPaid.Value - order.ChangeToOnWay.Value;
                            statByStreet.DeliveryTime += (double)deliveryTime.Seconds / 60 + deliveryTime.Minutes + deliveryTime.Hours * 60 + deliveryTime.Days * 24 * 60;
                        }
                    }

                    if (cookingCount > 0)
                    {
                        statByStreet.CookingTimeCount = cookingCount;
                        statByStreet.CookingTime = Math.Round(statByStreet.CookingTime / cookingCount, 2);
                    }

                    if (deliveryCount > 0)
                    {
                        statByStreet.DeliveryTimeCount = deliveryCount;
                        statByStreet.DeliveryTime = Math.Round(statByStreet.DeliveryTime / deliveryCount, 2);
                    }
                    result.StreetsList.Add(statByStreet);
                }
                return result;
            }

        }

        public List<DishCombinationData> GetMostCompatibilityDishes()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<DishCombinationData>();
                var orderItems = db.OrderItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
                var dishes = db.Dishes.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
                var ordersGroupedByOrderId = orderItems.GroupBy(x => x.OrderID)
                    .Select(x => new
                    {
                        OrderID = x.Key,
                        DishList = x.Select(a => a.DishID).ToList()
                    }).ToList();
                foreach (var dish in dishes)
                {
                    var dishesResult = new Dictionary<int, int>();
                    var ordersWithDish = ordersGroupedByOrderId.Where(x => x.DishList.Contains(dish.ID)).ToList();
                    foreach (var group in ordersWithDish)
                    {
                        foreach (var dishItem in group.DishList)
                        {
                            if (dishesResult.ContainsKey(dishItem))
                            {
                                dishesResult[dishItem]++;
                            }
                            else
                            {
                                dishesResult.Add(dishItem, 1);
                            }
                        }
                    }

                    var topFive = dishesResult.OrderByDescending(x => x.Value).Take(6).ToList();
                    var data = new DishCombinationData();
                    data.MainDishName = dish.Name;
                    data.MainDishCount = ordersWithDish.Count;
                    if (topFive.Count >= 2)
                    {
                        data.Dish1Name = dishes.FirstOrDefault(x => x.ID == topFive[1].Key)?.Name;
                        data.Dish1Count = topFive[1].Value;
                    }

                    if (topFive.Count >= 3)
                    {
                        data.Dish2Name = dishes.FirstOrDefault(x => x.ID == topFive[2].Key)?.Name;
                        data.Dish2Count = topFive[2].Value;
                    }

                    if (topFive.Count >= 4)
                    {
                        data.Dish3Name = dishes.FirstOrDefault(x => x.ID == topFive[3].Key)?.Name;
                        data.Dish3Count = topFive[3].Value;
                    }

                    if (topFive.Count >= 5)
                    {
                        data.Dish4Name = dishes.FirstOrDefault(x => x.ID == topFive[4].Key)?.Name;
                        data.Dish4Count = topFive[4].Value;
                    }

                    if (topFive.Count >= 6)
                    {
                        data.Dish5Name = dishes.FirstOrDefault(x => x.ID == topFive[5].Key)?.Name;
                        data.Dish5Count = topFive[5].Value;
                    }
                    result.Add(data);
                }
                return result;
            }
        }

    }
}
