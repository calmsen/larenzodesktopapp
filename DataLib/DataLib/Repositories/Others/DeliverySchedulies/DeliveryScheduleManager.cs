﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Entities.Others.DeliverySchedulies;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Others.DeliverySchedulies
{
    public class DeliveryScheduleManager: IDeliveryScheduleManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        
        private const int PointOfSaleIdPriority = 1;
        
        public List<int> CheckDeliverySchedule(List<int> dayOfWeek, TimeSpan timeBegin, TimeSpan timeEnd, int groupId, int poinOfSaleId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var crossedPeriod = new List<int>();
                foreach (var day in dayOfWeek)
                {
                    var schedule = db.DeliverySchedules
                        .Where(x=>x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.DayOfWeekID == day && x.AddressGroupID == groupId && x.PointOfSaleId == poinOfSaleId)
                        .ToList();
                    crossedPeriod.AddRange(from deliverySchedule in schedule where TimePeriodOverlap(deliverySchedule.DeliveryBegin, deliverySchedule.DeliveryEnd, timeBegin, timeEnd) select day);
                }

                return crossedPeriod.Distinct().ToList();
            }
        }

        /// <summary>
        /// Tests if two given periods overlap each other.
        /// </summary>
        /// <param name="bs">Base period start</param>
        /// <param name="be">Base period end</param>
        /// <param name="ts">Test period start</param>
        /// <param name="te">Test period end</param>
        /// <returns>
        /// 	<c>true</c> if the periods overlap; otherwise, <c>false</c>.
        /// </returns>
        public bool TimePeriodOverlap(TimeSpan bs, TimeSpan be, TimeSpan ts, TimeSpan te)
        {
            return ((ts >= bs && ts < be) || (te <= be && te > bs) || (ts <= bs && te >= be));
        }

        public TimeSpan GetDeliveryTime(Address address, DateTime startTime, int pointOfSaleId)
        {
            if (address == null)
                return TimeSpan.Zero;

            var scheduleList = GetScheduleList(address, startTime);
            if (!scheduleList.Any())
                return TimeSpan.Zero;

            var schedule = scheduleList.FirstOrDefault(x => x.PointOfSaleId == pointOfSaleId);
            return schedule?.DeliveryTime ?? TimeSpan.Zero;
        }

        private int GetCurrentDay()
        {
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    {
                        return 1;
                    }
                case DayOfWeek.Tuesday:
                    {
                        return 2;
                    }
                case DayOfWeek.Wednesday:
                    {
                        return 3;
                    }
                case DayOfWeek.Thursday:
                    {
                        return 4;
                    }
                case DayOfWeek.Friday:
                    {
                        return 5;
                    }
                case DayOfWeek.Saturday:
                    {
                        return 6;
                    }
                case DayOfWeek.Sunday:
                    {
                        return 7;
                    }
            }

            return 1;
        }

        public DeliverySchedule GetSchedule(Address address, DateTime startTime)
        {
            var scheduleList = GetScheduleList(address, startTime);
            if (!scheduleList.Any())
                return null;
            var schedule = scheduleList.First();
            for (int i = 1; i < scheduleList.Count; i++)
            {
                if (scheduleList[i].DeliveryTime > schedule.DeliveryTime)
                    continue;

                if (scheduleList[i].DeliveryTime < schedule.DeliveryTime)
                {
                    schedule = scheduleList[i];
                    continue;
                }

                if (scheduleList[i].DeliveryTime == schedule.DeliveryTime
                    && scheduleList[i].Priority > schedule.Priority)
                    continue;

                if (scheduleList[i].DeliveryTime == schedule.DeliveryTime
                    && scheduleList[i].Priority < schedule.Priority)
                {
                    schedule = scheduleList[i];
                    continue;
                }

                if (scheduleList[i].DeliveryTime == schedule.DeliveryTime
                    && scheduleList[i].Priority == schedule.Priority 
                    && scheduleList[i].PointOfSaleId == PointOfSaleIdPriority
                    && schedule.PointOfSaleId != PointOfSaleIdPriority)
                {
                    schedule = scheduleList[i];
                    continue;
                }
            }
            return schedule;
        }

        public void AddDeliverySchedule(List<int> dayOfWeek, TimeSpan timeBegin, TimeSpan timeEnd,
                                               TimeSpan waiting, int groupId, int poinOfSaleId, int priority)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var day in dayOfWeek)
                {
                    var deliverySchedule = new DeliverySchedule
                    {
                        DayOfWeekID = day,
                        DeliveryBegin = timeBegin,
                        DeliveryEnd = timeEnd,
                        DeliveryTime = waiting,
                        AddressGroupID = groupId,
                        PointOfSaleId = poinOfSaleId,
                        Priority = priority
                    };
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        deliverySchedule.BranchId = BranchRepository.SelectedBranchId;
                    }
                    deliverySchedule.DateModified = DateTime.Now;
                    deliverySchedule.AuthorModified = UserManager.UserLogged.Name;
                    db.DeliverySchedules.Add(deliverySchedule);
                }

                db.SaveChanges();
            }
        }

        public void UpdateDeliverySchedule(int scheduleId, List<int> dayOfWeek, TimeSpan timeBegin, TimeSpan timeEnd,
                                               TimeSpan waiting, int groupId, int poinOfSaleId, int priority)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var day in dayOfWeek)
                {
                    var deliverySchedule = new DeliverySchedule
                    {
                        ID = scheduleId,
                        DayOfWeekID = day,
                        DeliveryBegin = timeBegin,
                        DeliveryEnd = timeEnd,
                        DeliveryTime = waiting,
                        AddressGroupID = groupId,
                        PointOfSaleId = poinOfSaleId,
                        Priority = priority
                    };
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        deliverySchedule.BranchId = BranchRepository.SelectedBranchId;
                    }
                    deliverySchedule.DateModified = DateTime.Now;
                    deliverySchedule.AuthorModified = UserManager.UserLogged.Name;

                    db.Entry(deliverySchedule).State = EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public void DeleteDeliverySchedule(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var deliverySchedule = db.DeliverySchedules.FirstOrDefault(x=>x.BranchId == BranchRepository.SelectedBranchId && x.ID == id);
                if (deliverySchedule != null)
                {
                    db.DeliverySchedules.Remove(deliverySchedule);
                    db.SaveChanges();
                }
            }
        }

        public void AddAddressGroup(AddressGroup addressGroup)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    addressGroup.BranchId = BranchRepository.SelectedBranchId;
                }
                addressGroup.DateModified = DateTime.Now;
                addressGroup.AuthorModified = UserManager.UserLogged.Name;
                db.AddressGroups.Add(addressGroup);
                db.SaveChanges();
            }
        }

        public void DeleteAddressGroup(AddressGroup addressGroup)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.AddressGroups.Remove(addressGroup);
                db.SaveChanges();
            }
        }

        public void DeleteAddressGroup(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var addressGroup = db.AddressGroups.FirstOrDefault(x=>x.BranchId == BranchRepository.SelectedBranchId && x.ID == id);
                foreach (var address in db.Addresses.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.AddressGroupID == id).ToList())
                {
                    address.AddressGroupID = null;
                    db.Entry(address).State = EntityState.Modified;
                }

                db.AddressGroups.Remove(addressGroup);
                db.SaveChanges();
            }
        }

        public void UpdateAddressGroup(AddressGroup addressGroup)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    addressGroup.BranchId = BranchRepository.SelectedBranchId;
                }
                addressGroup.DateModified = DateTime.Now;
                addressGroup.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(addressGroup).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void UpdateAddressGroup(int id, string value)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var addressGroup = db.AddressGroups.FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.ID == id);
                addressGroup.Name = value;
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    addressGroup.BranchId = BranchRepository.SelectedBranchId;
                }
                addressGroup.DateModified = DateTime.Now;
                addressGroup.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(addressGroup).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public List<AddressGroup> GetAddressGroupList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.AddressGroups.Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.Name).ToList();
            }
        }

        public List<DeliverySchedule> GetScheduleList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.DeliverySchedules.Include("AddressGroup").Include("PointOfSale").Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public List<DeliverySchedule> GetScheduleList(Address address, DateTime startTime)
        {
            var currentTime = startTime.TimeOfDay;

            //TODO Разобраться почему не работает обычное преобразование даты
            //var currentDay = (int) DateTime.Now.DayOfWeek;

            int currentDay = GetCurrentDay();

            using (var db = PizzaAppDB.GetNewContext())
            {
                var session = db.Sessions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Closed == null);
                if (session != null && session.IsVacationDelivery)
                    currentDay = 7;
                
                return
                    db.DeliverySchedules.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(
                        x =>
                        x.PointOfSale.Active && 
                        x.AddressGroupID == address.AddressGroupID &&
                        x.DayOfWeekID == currentDay &&
                        (currentTime <= x.DeliveryEnd && currentTime >= x.DeliveryBegin))
                        .ToList();
            }
        }
    }
}
