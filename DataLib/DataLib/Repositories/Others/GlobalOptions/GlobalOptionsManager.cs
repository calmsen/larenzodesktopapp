﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DataLib.Models;

namespace LaRenzo.DataRepository.Repositories.Others.GlobalOptions
{
	public class GlobalOptionsManager: IGlobalOptionsManager
    {
        [Inject]
        /// <summary> Основные функции для опций </summary>
        public IBaseOptionsFunctions BaseOptionsFunctions { get; set; }

		/// <summary> Текущий id бренда </summary>
		private string GetCurentBrandIdStr(int brandId)
		{
			return "_" + brandId;
		}

		public int GetCookTime(int brandId)
		{
			return int.Parse(BaseOptionsFunctions.GetValueOrZero("CookTime" + brandId));

		}
		public void SetCookTime(int value, int brandId)
		{

			BaseOptionsFunctions.SetOrCreateValue("CookTime" + brandId, value.ToString(CultureInfo.InvariantCulture));
		}

		public int GetOverheadTime(int brandId)
		{
			return int.Parse(BaseOptionsFunctions.GetValueOrZero("OverheadTime" + brandId));

		}
		public void SetOverheadTime(int value, int brandId)
		{
			BaseOptionsFunctions.SetOrCreateValue("OverheadTime" + brandId, value.ToString(CultureInfo.InvariantCulture));
		}

		public int GetReserveTime(int brandId)
		{
			return int.Parse(BaseOptionsFunctions.GetValueOrZero("ReserveTime" + GetCurentBrandIdStr(brandId)));

		}
		public void SetReserveTime(int value, int brandId)
		{

			BaseOptionsFunctions.SetOrCreateValue("ReserveTime" + GetCurentBrandIdStr(brandId), value.ToString(CultureInfo.InvariantCulture));
		}

		public int GetDeliveryPrice(int brandId)
		{
			return int.Parse(BaseOptionsFunctions.GetValueOrZero("DeliveryPrice" + GetCurentBrandIdStr(brandId)));

		}

		public void SetDeliveryPrice(int value, int brandId)
		{
			BaseOptionsFunctions.SetOrCreateValue("DeliveryPrice" + GetCurentBrandIdStr(brandId), value.ToString(CultureInfo.InvariantCulture));
		}

		public string GetOperName(int brandId)
		{
			return BaseOptionsFunctions.GetValueOrEmptyString("OperName" + GetCurentBrandIdStr(brandId));
		}

	    public string GetVlCancelUrl(int brandId, int webOrderId)
	    {
	        var url = BaseOptionsFunctions.GetValueOrEmptyString("CancelVlru" + GetCurentBrandIdStr(brandId));
	        if (webOrderId != 0)
	        {
	            try
	            {
	                var result = String.Format(url, webOrderId.ToString());
	                return result;
	            }
	            catch (Exception e)
	            {
	                Console.WriteLine(e);
	                return String.Empty;
	            }
	        }
            return String.Empty;
	    }

		public void SetOperName(int value, int brandId)
		{

			BaseOptionsFunctions.SetOrCreateValue("OperName" + GetCurentBrandIdStr(brandId), value.ToString(CultureInfo.InvariantCulture));
		}

		public int GetFreeDeliveryFrom(int brandId)
		{
			return int.Parse(BaseOptionsFunctions.GetValueOrZero("FreeDeliveryFrom" + GetCurentBrandIdStr(brandId)));

		}

		public void SetFreeDeliveryFrom(int value, int brandId)
		{

			BaseOptionsFunctions.SetOrCreateValue("FreeDeliveryFrom" + GetCurentBrandIdStr(brandId), value.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary> Максимальное время между цехами </summary>
		public int GetMaxTimeGap(int brandId)
		{
			return int.Parse(BaseOptionsFunctions.GetValueOrZero("MaxTimeGap" + GetCurentBrandIdStr(brandId)));

		}
		public void SetMaxTimeGap(int value, int brandId)
		{

			BaseOptionsFunctions.SetOrCreateValue("MaxTimeGap" + GetCurentBrandIdStr(brandId), value.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary> Первая строка чека </summary>
		public string GetFirstStringCheck(int brandId)
		{
			return BaseOptionsFunctions.GetValueOrEmptyString("FirstStringCheck" + GetCurentBrandIdStr(brandId));

		}

		public void SetFirstStringCheck(string value, int brandId)
		{

			BaseOptionsFunctions.SetOrCreateValue("FirstStringCheck" + GetCurentBrandIdStr(brandId), value.ToString(CultureInfo.InvariantCulture));
		}
        

		/// <summary> Сайты </summary>
		public string GetSites(int brandId)
		{
			return BaseOptionsFunctions.GetValueOrEmptyString("Sites" + GetCurentBrandIdStr(brandId));

		}

		public void SetSites(string value, int brandId)
		{

			BaseOptionsFunctions.SetOrCreateValue("Sites" + GetCurentBrandIdStr(brandId), value.ToString(CultureInfo.InvariantCulture));
		}


		/// <summary> Первая строка чека </summary>
		public string GetCheckFooterByBrandId(int orderBrandId)
		{
			return BaseOptionsFunctions.GetValueOrEmptyString("CheckFooter_" + orderBrandId.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary> Низ чека </summary>
		public string GetCheckFooter(int brandId)
		{
			return BaseOptionsFunctions.GetValueOrEmptyString("CheckFooter" + GetCurentBrandIdStr(brandId));

		}

		public void SetCheckFooter(string value, int brandId)
		{
			BaseOptionsFunctions.SetOrCreateValue("CheckFooter" + GetCurentBrandIdStr(brandId), value.ToString(CultureInfo.InvariantCulture));
		}

		public List<string> GetSitesList(int brandId)
		{
			var list = BaseOptionsFunctions.GetValueOrEmptyString("Sites" + GetCurentBrandIdStr(brandId)).Split(';').Select(x => x.Trim()).ToList();
			return string.IsNullOrWhiteSpace(list[0]) ? new List<string>() : list;

		}


		/// <summary> eMail разработчика программы </summary>
		public string AppDevEmail
		{
			get { return BaseOptionsFunctions.GetValueOrEmptyString("AppDevEmail"); }
			set { BaseOptionsFunctions.SetOrCreateValue("AppDevEmail", value.ToString(CultureInfo.InvariantCulture)); }
		}

		/// <summary> eMail разработчика сайта </summary>
		public string SiteDevEmail
		{
			get { return BaseOptionsFunctions.GetValueOrEmptyString("SiteDevEmail"); }
			set { BaseOptionsFunctions.SetOrCreateValue("SiteDevEmail", value.ToString(CultureInfo.InvariantCulture)); }
		}

		/// <summary> eMail через который отправляется почта  </summary>
		public string EmailAddress
		{
			get { return BaseOptionsFunctions.GetValueOrEmptyString("EmailAddress"); }
			set { BaseOptionsFunctions.SetOrCreateValue("EmailAddress", value.ToString(CultureInfo.InvariantCulture)); }
		}

		/// <summary> Пользователь для eMail через который отправляется почта  </summary>
		public string EmailUser
		{
			get { return BaseOptionsFunctions.GetValueOrEmptyString("EmailUser"); }
			set { BaseOptionsFunctions.SetOrCreateValue("EmailUser", value.ToString(CultureInfo.InvariantCulture)); }
		}

		/// <summary> Пароль для eMail через который отправляется почта </summary>
		public string EmailPass
		{
			get { return BaseOptionsFunctions.GetValueOrEmptyString("EmailPass"); }
			set { BaseOptionsFunctions.SetOrCreateValue("EmailPass", value.ToString(CultureInfo.InvariantCulture)); }
		}

		/// <summary> Адрес SMTP сервера для отправки почты из программы </summary>
		public string EmailSMTPServer
		{
			get { return BaseOptionsFunctions.GetValueOrEmptyString("EmailSMTPServer"); }
			set { BaseOptionsFunctions.SetOrCreateValue("EmailSMTPServer", value.ToString(CultureInfo.InvariantCulture)); }
		}

		/// <summary> SMTP порт сервера для отправки почты из программы </summary>
		public int EmailSMTPPort
		{
			get
			{
				string val = BaseOptionsFunctions.GetValueOrEmptyString("EmailSMTPPort");
				int intVal;
				int.TryParse(val, out intVal);

				return intVal;
			}
			set { BaseOptionsFunctions.SetOrCreateValue("EmailSMTPPort", value.ToString(CultureInfo.InvariantCulture)); }
		}

		/// <summary> SMTP порт сервера для отправки почты из программы (строка, для удобства) </summary>
		public string EmailSMTPPortStr
		{
			set { BaseOptionsFunctions.SetOrCreateValue("EmailSMTPPort", value.ToString(CultureInfo.InvariantCulture)); }
		}

		/// <summary> Использовать ли SSL для передачи почты </summary>
		public bool EmailUseSSL
		{
			get { return BaseOptionsFunctions.GetValueOrEmptyString("EmailUseSSL").ToLower() == "true"; }
			set { BaseOptionsFunctions.SetOrCreateValue("EmailUseSSL", value.ToString(CultureInfo.InvariantCulture)); }
		}
	}
}
