﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Others.GlobalOptions;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Others.GlobalOptions
{
    public class BaseOptionsFunctions: IBaseOptionsFunctions
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        
        private IQueryable<GlobalOption> GetItems(PizzaAppDB db, bool withBranch = true)
        {
            var result = db.Set<GlobalOption>().AsQueryable();
            return result;
        }

        public List<GlobalOption> GetItems(bool withBranch = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return GetItems(db, withBranch).ToList();
            }
        }

        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Вернуть значение по имени </summary>
        ////==============================================
        private string GetValue(string optionName)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var option = GetItems(db, false).Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name == optionName);

                if (option != null)
                {
                    return option.Value;
                }
                else
                {
                    throw new Exception(String.Format("Настройки с именем `{0}` не существует в базе данных", optionName));
                }
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Вернуть значение по имени или пустую строку </summary>
        ////================================================================
        public virtual string GetValueOrEmptyString(string optionName)
        {
            string retVal = "";

            try
            {
                retVal = GetValue(optionName);
            }
            catch
            {

            }

            return retVal;
        }


        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Вернуть значение по имени или 0 </summary>
        ////====================================================
        public virtual string GetValueOrZero(string optionName)
        {
            string retVal = "0";

            try
            {
                retVal = GetValue(optionName);
            }
            catch
            {

            }

            return retVal;
        }


        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Устновить значение по имени </summary>
        ////================================================
        private void SetValue(string optionName, string value)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var option = db.GlobalOptions.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name == optionName);
                if (option != null)
                {
                    option.DateModified = DateTime.Now;
                    option.AuthorModified = UserManager.UserLogged.Name;
                    option.Value = value;
                    db.SaveChanges();
                }
                else
                {
                    throw new Exception(String.Format("Настройки с именем `{0}` не существует в базе данных", optionName));
                }
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Устновить или создать значение, если такого нет </summary>
        ////====================================================================
        public void SetOrCreateValue(string optionName, string value)
        {
            try
            {
                SetValue(optionName, value);
            }
            catch
            {
                using (var db = PizzaAppDB.GetNewContext())
                {
                    var newOptions = new GlobalOption
                    {
                        Name = optionName,
                        Value = value
                    };
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        newOptions.BranchId = BranchRepository.SelectedBranchId;
                    }
                    newOptions.DateModified = DateTime.Now;
                    newOptions.AuthorModified = UserManager.UserLogged.Name;
                    db.GlobalOptions.Add(newOptions);
                    db.SaveChanges();
                }
            }
        }

    }
}
