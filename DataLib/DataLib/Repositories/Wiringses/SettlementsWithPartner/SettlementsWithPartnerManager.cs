﻿using System;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Wirings;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Wiringses.SettlementsWithPartner
{
    public class SettlementsWithPartnerManager: ISettlementsWithPartnerManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public void ProcessWirings(int documentId, SettlsWithPartnerWirings wiring)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.SupplyDocuments.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                if (doc != null)
                {
                    doc.IsProcessed = true;
                    doc.IsMarkToDelete = false;
                    if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                    {
                        wiring.BranchId = BranchRepository.SelectedBranchId;
                    }
                    wiring.DateModified = DateTime.Now;
                    wiring.AuthorModified = UserManager.UserLogged.Name;
                    db.SettlsWithPartnerWirings.Add(wiring);
                }
                db.SaveChanges();
            }
        }

        public void UnProcessWirings(int documentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.SupplyDocuments.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == documentId);

                if (doc != null)
                {
                    doc.IsProcessed = false;

                    var wirings = db.SettlsWithPartnerWirings.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SupplyDocumentID == documentId).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.SettlsWithPartnerWirings.Remove(wiring);
                    }
                }
                db.SaveChanges();
            }
        }
    }
}