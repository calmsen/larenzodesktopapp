﻿using System;
using System.Linq;
using System.Collections.Generic;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Wirings;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Wiringses.IntermadiatesGood
{
    public class ItermadiateGoodWirningManager: IItermadiateGoodWirningManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IProductManager ProductManager { get; set; }
        [Inject]
        public IWarehouseManager WarehouseManager { get; set; }
        public  List<IntermediateGoodWirings> GetList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.IntermediateGoodWiringses.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public  void DeleteWirning(IntermediateGoodWirings selectedRow)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var wiring = db.IntermediateGoodWiringses.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == selectedRow.ID);

                var wirings = db.RemainsOfGoodWirings.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId == selectedRow.ID).ToList();

                foreach (var remainsOfGoodWiringse in wirings)
                {
                    db.RemainsOfGoodWirings.Remove(remainsOfGoodWiringse);
                }

                if (wiring != null)
                {
                    db.IntermediateGoodWiringses.Remove(wiring);
                }

                db.SaveChanges();
            }
        }

        public  void AddWirning(DateTime dateTime)
        {
            var date = new DateTime(dateTime.Year, dateTime.Month, 1);
            var intermediate = new IntermediateGoodWirings { DateTime = date, IsActual = true };

            using (var db = PizzaAppDB.GetNewContext())
            {
                var intrmdt = db.IntermediateGoodWiringses.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.DateTime == date);

                if (intrmdt == null)
                {
                    if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                    {
                        intermediate.BranchId = BranchRepository.SelectedBranchId;
                    }
                    intermediate.DateModified = DateTime.Now;
                    intermediate.AuthorModified = UserManager.UserLogged.Name;
                    db.IntermediateGoodWiringses.Add(intermediate);
                }
                else
                {
                    var wirings = db.RemainsOfGoodWirings.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.IntermediateGoodWiringsId == intrmdt.ID).ToList();

                    foreach (var remainsOfGoodWiringse in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(remainsOfGoodWiringse);
                    }
                }

                db.SaveChanges();
            }

            using (var db = PizzaAppDB.GetNewContext())
            {
                var products = ProductManager.GetList();

                var warehouses = WarehouseManager.GetWarehouses();

                foreach (var product in products)
                {
                    if (product == null || product.IsMarkToDelete)
                        continue;

                    foreach (var warehouse in warehouses)
                    {
                        if (warehouse == null)
                            continue;

                        var intrmd =
                            db.RemainsOfGoodWirings
                            .Where(x=>x.BranchId == BranchRepository.SelectedBranchId)
                            .Where(
                                x =>
                                x.IntermediateGoodWiringsId != null && x.ProductID == product.ID && x.Date < date &&
                                x.WarehouseID == warehouse.ID)
                              .OrderByDescending(x => x.Date).FirstOrDefault();

                        List<RemainsOfGoodWirings> buffer;
                        decimal ammount = 0;

                        if (intrmd != null)
                        {
                            buffer = db.RemainsOfGoodWirings
                                .Where(x=>x.BranchId == BranchRepository.SelectedBranchId)
                                .Where(
                                x =>
                                x.ProductID == product.ID && x.Date <= date && x.Date >= intrmd.Date &&
                                x.WarehouseID == warehouse.ID && x.IntermediateGoodWiringsId == null).ToList();
                            ammount += intrmd.Amount;
                        }
                        else
                        {
                            buffer = db.RemainsOfGoodWirings
                                .Where(x=>x.BranchId == BranchRepository.SelectedBranchId)
                                .Where(
                                x =>
                                x.ProductID == product.ID && x.Date <= date &&
                                x.WarehouseID == warehouse.ID && x.IntermediateGoodWiringsId == null).ToList();
                        }

                        var orderRecord = new RemainsOfGoodWirings
                        {
                            ProductID = product.ID,
                            IntermediateGoodWiringsId = intermediate.ID,
                            WarehouseID = warehouse.ID,
                            Date = date,
                            Amount = (buffer.Any()) ? buffer.Sum(x => x.Amount) : 0,
                        };
                        orderRecord.Amount += ammount;
                        if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                        {
                            orderRecord.BranchId = BranchRepository.SelectedBranchId;
                        }
                        orderRecord.DateModified = DateTime.Now;
                        orderRecord.AuthorModified = UserManager.UserLogged.Name;
                        db.RemainsOfGoodWirings.Add(orderRecord);
                    }
                }

                db.SaveChanges();
            }
        }
    }
}