﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataLib.Models;
using DataRepository.Entities;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.Interfaces;

namespace LaRenzo.DataRepository.Repositories.Settings
{
    public static class ExtensionsBrand
    {
        public static IQueryable<Brand> IncludeComplete(this IQueryable<Brand> context)
        {
            return context;
        }
    }

    public class BrandManager: IBrandManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IThreadLocalStorage ThreadLocalStorage { get; set; }

        /// <summary>
        /// Выбранный бренд при входе
        /// </summary>
        public Brand SelectedBrand
        {
            get => ThreadLocalStorage.SelectedBrand;
            set => ThreadLocalStorage.SelectedBrand = value;
        }

        /// <summary>
        ///  Название таблицы
        /// </summary>
        private const string TableName = "Brands";

        /// <summary>
        /// Обновление
        /// </summary>
        /// <param name="item"></param>
        private void Update(object item)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }
        }


        /// <summary>
        /// Удалить элемент
        /// </summary>
        /// <param name="id"></param>
        public void Remove(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                try
                {
                    db.Database.ExecuteSqlCommand("delete from " + TableName + " where id = " + id.ToString());
                }
                catch (Exception ex)
                {
                    UserMessage.Show($"Ошибка удаления элемента {typeof(Brand).Name} с идентификатором {id}.", ex);
                }

            }
        }
               
        /// <summary>
        /// Сохранить элемент
        /// </summary>
        /// <param name="item"></param>
        public void Save(Brand item)
        {
            if (item.ID > 0)
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    item.BranchId = BranchRepository.SelectedBranchId;
                }
                Update(item);
            }
            else
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    item.BranchId = BranchRepository.SelectedBranchId;
                }
                Add(item);
            }
        }


        /// <summary>
        /// Вернуть состояния для сохранения объекта
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private EntityState GetStateSaveSubItem(Brand item)
        {
            var state = EntityState.Unchanged;

            if (item.ID == 0)
            {
                state = EntityState.Added;
            }

            return state;
        }

        
        /// <summary>
        /// Возвращает список платежей по фильтру
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public List<Brand> GetList(FilterModel filter = null, int? branchId = null)
        {
            List<Brand> listItems;
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Brands.IncludeComplete().Where(x => x.BranchId == (branchId??BranchRepository.SelectedBranchId));
                var value = filter as BrandFilter;
                if (value != null)
                {
                    if (!string.IsNullOrEmpty(value.BrandName))
                        result = result.Where(x => x.Name == value.BrandName);
                    listItems = result.ToList();
                }
                else
                {
                    listItems = result.ToList();
                }
            }

            return listItems;
        }

        /// <summary>
        /// Добавить платёж
        /// </summary>
        /// <param name="item"></param>
        public void Add(Brand item)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    item.BranchId = BranchRepository.SelectedBranchId;
                }
                db.Configuration.AutoDetectChangesEnabled = false; // Увеличение производительности
                
                db.Configuration.AutoDetectChangesEnabled = true;
                item.DateModified = DateTime.Now;
                item.AuthorModified = UserManager.UserLogged.Name;
                db.Brands.Add(item);
                db.SaveChanges();
            }
        }

        public Brand GetById(int id, bool withBranch = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (withBranch)
                {
                    return db.Brands.IncludeComplete()
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .FirstOrDefault(x => x.ID == id);
                }
                else
                {
                    return db.Brands.IncludeComplete().FirstOrDefault(x => x.ID == id);
                }
            }
        }

        public Brand GetByName(string name)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Brands.IncludeComplete().FirstOrDefault(x => x.Name.ToUpper() == name.ToUpper());
            }
        }

        public string Validate(Brand brand)
        {
            string errors = string.Empty;

            if (string.IsNullOrEmpty(brand.Name))
            {
                errors = "Название не может быть пустым";
            }

            return errors;
        }
    }
}
