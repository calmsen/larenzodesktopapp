﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Finance
{
    public class WalletRepository: IWalletRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public virtual IEnumerable<Wallet> GetItems()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Wallets.Include("Payments").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }



        public Wallet GetCashWallet()
        {
            return GetDefaultWalletOrCreate(WalletType.Cash);
        }

        public Wallet GetBankWallet()
        {
            return GetDefaultWalletOrCreate(WalletType.Bank);
        }

        public Wallet GetDebtWallet()
        {
            return GetDefaultWalletOrCreate(WalletType.Debt);
        }

        public Wallet GetDefaultWalletOrCreate(WalletType type)
        {
            var wallet = GetDefaultWallet(type);
            if (wallet == null)
            {
                wallet = new Wallet
                {
                    Type = type,
                    Name = GetDefaultWalletName(type)
                };
                InsertItem(wallet);
            }

            return wallet;
        }

        private string GetDefaultWalletName(WalletType type)
        {
            switch (type)
            {
                case WalletType.Cash:
                    return "Касса";

                case WalletType.Debt:
                    return "Заемные средства";

                case WalletType.Bank:
                    return "Банк";

                default:
                    throw new ArgumentException("Имена кошельков 'по-умолчанию' определены только для стандартных кошельков");
            }
        }

        public virtual Wallet GetDefaultWallet(WalletType type)
        {
            if (type == WalletType.Custom) throw new ArgumentException("Функция GetWalletType работает только со стандартными кошельками");

            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Wallets.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Type == type);
            }
        }

        public virtual void CreateDefaultWallets()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var wallets = new List<Wallet>();

                wallets.Add(new Wallet
                {
                    Name = "Касса",
                    Type = WalletType.Cash
                });

                wallets.Add(new Wallet
                {
                    Name = "Банк",
                    Type = WalletType.Bank
                });

                wallets.Add(new Wallet
                {
                    Name = "Заемные средства",
                    Type = WalletType.Debt
                });

                wallets.ForEach(InsertItem);
            }
        }

        public virtual IEnumerable<Wallet> GetItems(FilterModel filter)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Wallets.Where(x=>x.BranchId == BranchRepository.SelectedBranchId);
            }
        }

        public virtual Wallet GetItem(Wallet entity)
        {
            return null;
        }

        public virtual Wallet GetItem(FilterModel filter)
        {
            return null;
        }

        public virtual void UpdateItem(Wallet entity)
        {
        }

        public virtual void DeleteItem(Wallet entity)
        {
        }

        public virtual void InsertItem(Wallet entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (entity.Type != WalletType.Custom && db.Wallets.Any(x => x.Type == entity.Type))
                {
                    throw new ArgumentException("Нельзя создать два кошелька с типом, отлшичным от Custom");
                }
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Wallets.Add(entity);
                db.SaveChanges();
            }
        }
    }
}
