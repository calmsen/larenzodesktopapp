﻿using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Repositories.Finance;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Entities.Finance
{
    public class PaymentDashboardRepository: IPaymentDashboardRepository
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IWalletRepository WalletRepository { get; set; }
        [Inject]
        public IPaymentRepository PaymentRepository { get; set; }

        private PaymentCategoryReferences _paymentCategoryReferences;

        public PaymentDashboardRepository()
        {
            _paymentCategoryReferences = new PaymentCategoryReferences();
        }

        public List<PaymentDashboardCategoryInfo> GetCategoriesWithSummaries()
        {
            var wallets = WalletRepository.GetItems().ToList();
            if (wallets == null) return null;
            return GetCategoriesWithSummaries(0);
        }

        private List<PaymentDashboardCategoryInfo> GetCategoriesWithSummaries(int categoryId = 0)
        {
            var result = new List<PaymentDashboardCategoryInfo>();

            using (var db = PizzaAppDB.GetNewContext())
            {
                var categories = db.PaymentCategories.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).ToList();

                if (categoryId > 0)
                {
                    var references = _paymentCategoryReferences.GetCategoryReferences(categoryId, categories);
                    categories = categories.Where(x => references.Contains(x.ID)).ToList();
                }
                else
                {
                    categories.Add(new PaymentCategory
                    {
                        Name = "Все",
                        ID = 0,
                        Parent = -1
                    });
                }

                foreach (var cat in categories)
                {
                    var references = _paymentCategoryReferences.GetCategoryReferences(cat.ID, categories);

                    var payments = PaymentRepository.GetItems();
                    
                    var earn = payments.Any(x => references.Contains(x.CategoryID) && x.Total > 0) ? db.Payments.Where(x => references.Contains(x.CategoryID) && x.Total > 0).Sum(x => x.Total) : 0;
                    var spend = payments.Any(x => references.Contains(x.CategoryID) && x.Total < 0) ? db.Payments.Where(x => references.Contains(x.CategoryID) && x.Total < 0).Sum(x => x.Total) : 0;
                    
                    result.Add(new PaymentDashboardCategoryInfo
                    {
                        Category = cat,
                        EarningTotal = earn,
                        SpendTotal = spend,
                    });
                }

                var headNodeInfo = result.First(x => x.Category.ID == categoryId);

                foreach (var info in result)
                {
                    info.SpendPercent = headNodeInfo.SpendTotal != 0 ? info.SpendTotal / headNodeInfo.SpendTotal : 0;
                    info.EarningPercent = headNodeInfo.EarningTotal != 0 ? info.EarningTotal / headNodeInfo.EarningTotal : 0;
                }

            }

            return result;
        }
    }
}
