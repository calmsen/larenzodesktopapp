﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Finance
{
    public class PaymentRepository: IPaymentRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IWalletRepository WalletRepository { get; set; }
        public decimal GetTotalBalance()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Payments.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Sum(x => x.Total);
            }
        }

        private void AddDefaultPayment(int total, string comment, int categoryId, int walletId, DateTime date)
        {
            InsertItem(new Payment
            {
                CategoryID = categoryId,
                Total = total,
                UserID = UserManager.UserLogged.ID,
                WalletID = walletId,
                Comment = comment,
                Created = date
            });
        }

        public void AddBankPayment(int total, string comment, int categoryId, DateTime date)
        {
            var walletId = WalletRepository.GetBankWallet().ID;
            AddDefaultPayment(total, comment, categoryId, walletId, date);
        }

        public void AddBankPayment(int total, string comment, int categoryId)
        {
            AddBankPayment(total, comment, categoryId, DateTime.Now);
        }

        public void AddCashPayment(int total, string comment, int categoryId, DateTime date)
        {
            var walletId = WalletRepository.GetCashWallet().ID;
            AddDefaultPayment(total, comment, categoryId, walletId, date);
        }

        public void AddCashPayment(int total, string comment, int categoryId)
        {
            AddCashPayment(total, comment, categoryId, DateTime.Now);
        }

        public void AddDebtPayment(int total, string comment, int categoryId)
        {
            var walletId = WalletRepository.GetDebtWallet().ID;
            AddDefaultPayment(total, comment, categoryId, walletId, DateTime.Now);
        }

        public void AddTransfer(Wallet from, Wallet to, decimal amount, PaymentCategory category, string comment)
        {
            if (from == null || to == null) throw new ArgumentNullException("Не указана одна или обе стороны перевода");

            var transferId = Guid.NewGuid();

            var fromPayment = new Payment
            {
                WalletID = from.ID,
                Total = -amount,
                CategoryID = category.ID,
                Comment = comment,
                TransferID = transferId
            };

            var toPayment = new Payment
            {
                WalletID = to.ID,
                Total = amount,
                CategoryID = category.ID,
                Comment = comment,
                TransferID = transferId
            };

            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    fromPayment.BranchId = BranchRepository.SelectedBranchId;
                    toPayment.BranchId = BranchRepository.SelectedBranchId;
                }
                fromPayment.DateModified = DateTime.Now;
                fromPayment.AuthorModified = UserManager.UserLogged.Name;
                toPayment.DateModified = DateTime.Now;
                toPayment.AuthorModified = UserManager.UserLogged.Name;
                db.Payments.Add(fromPayment);
                db.Payments.Add(toPayment);
                db.SaveChanges();
            }

        }

        public virtual List<Payment> GetItems()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Payments.Include("Category").Include("Wallet").Include("User")
                    .Where(x=>x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public void InsertItem(Payment entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Payments.Add(entity);
                db.SaveChanges();
            }
        }
    }
}
