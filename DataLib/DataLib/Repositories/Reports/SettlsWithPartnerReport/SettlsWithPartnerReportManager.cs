﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Reports.SettlsWithPartnerReport;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.Interfaces;
using LaRenzo.Interfaces.SignalMessages;

namespace LaRenzo.DataRepository.Repositories.Reports.SettlsWithPartnerReport
{
    public class SettlsWithPartnerReportManager: ISettlsWithPartnerReportManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public ISignalProcessor SignalProcessor { get; set; }

        public List<SettlsWithPartnerRepRecord> GetReportDate(DateTime end)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<SettlsWithPartnerRepRecord>();
                var source = db.SettlsWithPartnerWirings.Include("Partner").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => !(Math.Abs(x.Payment - x.Sum) < 0.01) && x.Date < end).ToList();

                var max = source.Count;
                var current = 0;

                foreach (var wirings in source)
                {
                    if (wirings == null)
                        continue;

                    var reportRecord = new SettlsWithPartnerRepRecord
                        {
                            SupplyDocument = string.Format("Документ поступления №{0}", wirings.SupplyDocumentID),
                            Partner = wirings.Partner,
                            PartnerID = wirings.PartnerID,
                        };

                    if (wirings.Sum > wirings.Payment)
                    {
                        var deficitPayment = wirings.Sum - wirings.Payment;

                        if (deficitPayment < 0.01)
                            continue;

                        reportRecord.DeficitPayment = Math.Round(deficitPayment, 3);
                    }

                    if (wirings.Sum < wirings.Payment)
                    {
                        var overPayment = wirings.Payment - wirings.Sum;

                        if (overPayment < 0.01)
                            continue;

                        reportRecord.OverPayment = Math.Round(overPayment, 3);
                    }

                    result.Add(reportRecord);

                    var taskCompleted = (float)++current / max;
                    SignalProcessor?.Raise(new ReportDateProgressChangedSignalMessage
                    {
                        Percentage = (int)(taskCompleted * 100)
                    });
                    GC.Collect();
                }

                return result;
            }
        }

        public delegate void GenerationProgressReport(object sender, int percentage);
    }
}
