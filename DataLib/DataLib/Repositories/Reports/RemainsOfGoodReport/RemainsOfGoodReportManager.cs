﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using DataLib.Models;
using System.Linq;
using LaRenzo.DataRepository.Entities.Documents.Supply;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Reports.RemainsOfGoodReport
{
    public class RemainsOfGoodReportManager: IRemainsOfGoodReportManager
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IProductManager ProductManager { get; set; }
        [Inject]
        public IRemainsOfGoodWiringsManager RemainsOfGoodWiringsManager { get; set; }

        public List<RemainsOfGoodReportRecord> GetReportDate(DateTime end, int warehouseId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<RemainsOfGoodReportRecord>();
                var products = ProductManager.GetList();

                var productsAmountData = RemainsOfGoodWiringsManager.GetRemainsOnDate(warehouseId, end).ProductRemains;
                var productsAmountDataAllWH = RemainsOfGoodWiringsManager.GetRemainsOnDate(end).ProductRemains;

                var allSupplyItems = db.SupplyDocumentItems.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SupplyDocument.IsProcessed && x.Price > 0).GroupBy(x => x.ProductID).Select(x => new
                {
                    productId = x.Key,
                    items = x.OrderByDescending(a => a.SupplyDocument.DocumentDate).Select(a => new
                    {
                        a.Price,
                        a.Amount,
                        Date = a.SupplyDocument.DocumentDate
                    }).Take(10)
                }).ToList();

                var TimerCollection = new List<Stopwatch>();

                foreach (var product in products)
                {
                    if (product == null)
                        continue;

                    TimerCollection.Add(Stopwatch.StartNew());
                    
                    var orderRecord = new RemainsOfGoodReportRecord
                    {
                        ProductId = product.ID,
                        ProductName = product.Name,
                        ProductMinCount = product.MinAmount,
                        ProductMeasure = product.Measure.ShortName,
                        ProductCategory = product.ProductCategory.Name
                    };

                    var prdct = product;

                    orderRecord.ProductCount = productsAmountData.Any(x => x.ProductID == prdct.ID)
                        ? productsAmountData.First(x => x.ProductID == prdct.ID).Amount
                        : 0;

                    orderRecord.Price = (decimal)prdct.FixPrice;

                    orderRecord.Sum = orderRecord.ProductCount*orderRecord.Price;

                    orderRecord.ProductDelta = orderRecord.ProductCount - orderRecord.ProductMinCount;

                    //Получение данных о средней и последний цене (из данных по оприходыванию)
                    if (allSupplyItems.Any(x => x.productId == prdct.ID))
                    {
                        var recordSupplyItems = allSupplyItems.First(x => x.productId == prdct.ID);
                        if (recordSupplyItems.items.Any())
                        {
                            orderRecord.LastPrice = recordSupplyItems.items.First().Price;
                            orderRecord.AveragePrice = recordSupplyItems.items.Average(x => x.Price);
                            orderRecord.Median =
                                GetMedian(recordSupplyItems.items.Select(x => x.Price).ToList());

                            //**************
                            //Расчет максимального скачка цен
                            //
                            //**************

                            var items = recordSupplyItems.items.ToList();
                            double maxGap = 0;
                            
                            if (items.Count() > 2)
                            {
                                for (int i = 1; i < items.Count(); i++)
                                {
                                    var first = items[i].Price;
                                    var second = items[i - 1].Price;

                                    var max = Math.Max(first, second);
                                    var min = Math.Min(first, second);

                                    var gap = (max - min) / min;

                                    if (gap > maxGap) maxGap = gap;
                                }
                            }
                            
                            orderRecord.MaxPriceGapPercantage = maxGap;

                            //*************
                            //Рассчет средневзвешенной цены. Если на складе нет остатков
                            //
                            //*************

                            //Находим остатки товара на ВСЕХ складах
                            var productAmountOnAllWh = productsAmountDataAllWH.Any(x => x.ProductID == prdct.ID)
                                ? productsAmountDataAllWH.First(x => x.ProductID == prdct.ID).Amount
                                : 0;

                            if (productAmountOnAllWh <= 0) orderRecord.WeightedPrice = orderRecord.AveragePrice;
                            else
                            {
                                var edgeTime = DateTime.Now.AddMonths(-1);

                                //Разделяем поставки на те, что были за последний месяц (календарный, не зависит от периода отчета) и до этого
                                var thisMonthRecords = recordSupplyItems.items.Where(x => x.Date >= edgeTime).ToList();
                                var oldRecords = recordSupplyItems.items.Where(x => x.Date < edgeTime).ToList();

                                //Находим средние цены за эти два периода
                                var thisMonthAveragePrice = thisMonthRecords.Any() ? thisMonthRecords.Average(x => x.Price) : 0;
                                var oldRecordsAveragePrice = oldRecords.Any() ? oldRecords.Average(x => x.Price) : 0;

                                //Находим количество остатков, которые нужно учитывать в разных периодах
                                var thisMonthTotal = thisMonthRecords.Any() ? thisMonthRecords.Sum(x => x.Amount) : 0;

                                

                                var oldSuppliedTotal = productAmountOnAllWh - thisMonthTotal;
                                if (oldSuppliedTotal < 0)
                                {
                                    thisMonthTotal = productAmountOnAllWh;
                                    oldSuppliedTotal = 0;
                                }


                                orderRecord.WeightedPrice = ((double)thisMonthTotal * thisMonthAveragePrice +
                                                             (double)oldSuppliedTotal * oldRecordsAveragePrice) /
                                                            (double)(thisMonthTotal + oldSuppliedTotal);
                                
                            }
                            
                            
                        } 
                    }
                    
                    result.Add(orderRecord);

                    TimerCollection.FindLast(x => x.IsRunning).Stop();
                }

                var averageTime = TimerCollection.Average(x => x.Elapsed.TotalMilliseconds);
                return result;
            }
        }

        private double GetMedian(List<double> prices)
        {
            
            if (!prices.Any()) return 0;
            if (prices.Count() == 1) return prices[0];

            var result = 0d;

            var sorted = prices.OrderBy(x => x).ToList();
            
            if (sorted.Count()%2 == 0)
            {
                var mid = (sorted.Count()/2);
                result = ((sorted[mid] + sorted[mid - 1])/2);
            }
            else
            {
                var mid = ((sorted.Count() - 1) / 2);
                result = sorted[mid];
            }

            return result;
        }
    }
}
