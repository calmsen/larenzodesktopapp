﻿using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.PrintWorksModels
{
    public class PrintingRepository: IPrintingRepository
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public List<OrderItem> GetOrderItems(Order order)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var orderitems = db.OrderItems.Include("Dish").Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.OrderID == order.ID).ToList();

                return orderitems;
            }
        }

        public Category GetCategory(int categoryID)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var category = db.Categories.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == categoryID);
                return category;
            }
        }

        public CardsData GetCardsData(int sessionID)
        {
            var cardsData = new CardsData();
            using (var db = PizzaAppDB.GetNewContext())
            {
                var query = db.CaffeOrderDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => !x.IsMarkToDelete && x.SessionID == sessionID);

                cardsData.onlineCafeCount = query.Count(x => x.PaymentType == PaymentType.NoMoney);
                cardsData.offlineCafeCount = query.Count(x => x.PaymentType == PaymentType.Money);
                cardsData.cardsCafeCount = query.Count(x => x.PaymentType == PaymentType.Card);
                cardsData.onlineCafeSum = (cardsData.onlineCafeCount > 0)
                    ? query.Where(x => x.PaymentType == PaymentType.NoMoney).Select(x => x.DocumentSum).DefaultIfEmpty(0).Sum() : 0;
                cardsData.offlineCafeSum = (cardsData.offlineCafeCount > 0)
                    ? query.Where(x => x.PaymentType == PaymentType.Money).Select(x => x.DocumentSum).DefaultIfEmpty(0).Sum() : 0;
                cardsData.cardsCafeSum = (cardsData.offlineCafeCount > 0)
                    ? query.Where(x => x.PaymentType == PaymentType.Card).Select(x => x.DocumentSum).DefaultIfEmpty(0).Sum() : 0;
            }
            return cardsData;
        }
    }
}
