﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Linq;
using System.Xml.Serialization;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Transfers;
using LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using Product = LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer.Product;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Transfers
{
    public class ChiefExpertTransferManager: IChiefExpertTransferManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public List<ProductConformity> GetIngridients(string path)
        {
            ChefExpert chefExpert;
            LoadDataFromFile(path, out chefExpert);
            var productConformities = new List<ProductConformity>();

            using (var db = PizzaAppDB.GetNewContext())
            {
                productConformities.AddRange(from ingridient in chefExpert.GetIngridients()
                                             let product =
                                                 db.Products.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ChefExpertCode == ingridient.Fid) ??
                                                 db.Products.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(
                                                     x => x.Name.ToLower() == ingridient.Name.ToLower())
                                             select new ProductConformity { ChefProduct = ingridient, Product = product });
            }

            return productConformities;
        }

        public List<DishConformity> GetProducts(string path)
        {
            var chefExpert = new ChefExpert();
            try
            {
                if (path != null)
                {
                    var serializer = new XmlSerializer(typeof(ChefExpert));
                    using (
                        XmlReader reader = XmlReader.Create(new FileStream(path, FileMode.Open),
                                                            new XmlReaderSettings { CheckCharacters = true }))
                    {
                        chefExpert = (ChefExpert)serializer.Deserialize(reader);
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                UserMessage.Show(string.Format("При загрузке из файла возникли ошибки: {0}", ex.Message));
            }

            var dishConformity = new List<DishConformity>();
            try
            {
                using (var context = PizzaAppDB.GetNewContext())
                {
                    dishConformity.AddRange(from dish in chefExpert.GetDishes()
                                            let product =
                                                context.Dishes.Include("Category").FirstOrDefault(x => x.ChefExpertCode == dish.Fid) ??
                                                context.Dishes.Include("Category").FirstOrDefault(
                                                    x => x.Name.ToLower() == dish.Name.ToLower())
                                            select new DishConformity { DishChef = dish, Dish = product });
                }

            }
            catch (Exception ex)
            {
                UserMessage.Show(string.Format("{0} {1}", ex.Message, ex.StackTrace));
            }
            return dishConformity;
        }

        private void LoadDataFromFile(string path, out ChefExpert chefExpert)
        {
            chefExpert = new ChefExpert();
            try
            {
                if (path != null)
                {
                    var serializer = new XmlSerializer(typeof(ChefExpert));
                    using (
                        XmlReader reader = XmlReader.Create(new FileStream(path, FileMode.Open),
                                                            new XmlReaderSettings { CheckCharacters = true }))
                    {
                        chefExpert = (ChefExpert)serializer.Deserialize(reader);
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                UserMessage.Show(string.Format("При загрузке из файла возникли ошибки: {0}", ex.Message));
            }
        }

        public List<NewItemAction> GetActionList()
        {
            return new List<NewItemAction> { new NewItemAction { ID = 1, Name = "Пропустить" }, new NewItemAction { ID = 2, Name = "Создать" } };
        }

        public bool SaveIngridientList(List<ProductConformity> conformities, int actionId, bool fromIng, ProductCategory folder, bool isSet = false)
        {
            if (folder == null)
            {
                CreateProductCategories(actionId == 1
                                        ? conformities.Where(
                                            x =>
                                            x.ChefProduct != null && x.Product != null).Select(x => x.ChefProduct).ToList()
                                        : conformities.Where(
                                            x =>
                                            x.ChefProduct != null && (!fromIng || x.Product != null)).Select(x => x.ChefProduct).ToList());
            }

            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var conformity in conformities)
                {
                    if (conformity.ChefProduct == null)
                        continue;

                    var chefProductId = conformity.ChefProduct.ID.ToString(CultureInfo.InvariantCulture);

                    var foundProduct =
                        db.Products.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(
                            x =>
                            x.Name.ToLower() == conformity.ChefProduct.Name.Trim().ToLower() || (x.ChefExpertCode != null &&
                            x.ChefExpertCode.ToLower() == chefProductId.ToLower()));

                    if (foundProduct == null)
                    {
                        if (conformity.Product == null && actionId == 1)
                            continue;

                        ProductCategory productFolder = folder;
                        if (productFolder == null)
                        {
                            if (fromIng)
                            {
                                if (conformity.Product == null)
                                    continue;

                                productFolder = conformity.Product.ProductCategory;
                            }
                            else
                            {
                                productFolder =
                                    db.ProductCategories.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).First(
                                        x => x.Name.ToLower() == conformity.ChefProduct.ProductCategory.Name.ToLower());
                            }
                        }

                        var measure =
                            db.Measures.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name.ToLower() == conformity.ChefProduct.Unit.ToLower());

                        var product = new Entities.Catalogs.Products.Product
                        {
                            Name = conformity.ChefProduct.Name,
                            ProductCategoryID = productFolder.ID,
                            FixPrice = Convert.ToDouble(conformity.ChefProduct.Cost, CultureInfo.CurrentUICulture),
                            ChefExpertCode = conformity.ChefProduct.ID.ToString(CultureInfo.InvariantCulture),
                            MeasureID = measure != null ? measure.ID : productFolder.DefaultMeasureID,
                            IsSetProduct = isSet
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            product.BranchId = BranchRepository.SelectedBranchId;
                        }
                        product.DateModified = DateTime.Now;
                        product.AuthorModified = UserManager.UserLogged.Name;
                        db.Products.Add(product);
                    }
                    else
                    {
                        foundProduct.FixPrice = Convert.ToDouble(conformity.ChefProduct.Cost, CultureInfo.CurrentUICulture);
                        foundProduct.ChefExpertCode = conformity.ChefProduct.ID.ToString(CultureInfo.InvariantCulture);
                        foundProduct.IsSetProduct = isSet;
                    }
                }

                db.SaveChanges();
            }

            return true;
        }

        public bool SaveProductList(List<DishConformity> conformities, int actionId, bool fromDish, Category category, string path)
        {
            SaveSemiproducts(path);

            if (category == null)
            {
                CreateDishCategory(actionId == 1
                                   ? conformities.Where(x => x.DishChef != null && x.Dish != null).Select(x => x.DishChef).ToList()
                                   : conformities.Where(x => x.DishChef != null && (!fromDish || x.Dish != null)).Select(x => x.DishChef).ToList());
            }

            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var conformity in conformities)
                {
                    if (conformity.DishChef == null)
                        continue;

                    var chefDishId = conformity.DishChef.ID.ToString(CultureInfo.InvariantCulture);

                    var foundDish =
                        db.Dishes.Where(x=>x.BranchId == BranchRepository.SelectedBranchId)
                            .FirstOrDefault(
                            x =>
                            x.Name.Trim().ToLower() == conformity.DishChef.Name.Trim().ToLower() ||
                            x.InternalName.Trim().ToLower() == conformity.DishChef.Name.Trim().ToLower() ||
                            (x.ChefExpertCode != null && x.ChefExpertCode.ToLower() == chefDishId.ToLower()));

                    if (foundDish == null)
                    {
                        if (conformity.Dish == null && actionId == 1)
                            continue;

                        Category dishCategory = category;
                        if (dishCategory == null)
                        {
                            if (fromDish)
                            {
                                if (conformity.Dish == null)
                                    continue;

                                dishCategory = conformity.Dish.Category;
                            }
                            else
                            {
                                dishCategory =
                                    db.Categories.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).First(
                                        x => x.Name.ToLower() == conformity.DishChef.DishCategory.Name.ToLower());

                            }
                        }

                        var dish = new Dish
                        {
                            Name = conformity.DishChef.Name,
                            CategoryID = dishCategory.ID,
                            ChefExpertCode = conformity.DishChef.ID.ToString(CultureInfo.InvariantCulture),
                            Price = Convert.ToInt32(Convert.ToDouble(conformity.DishChef.Cost, CultureInfo.InvariantCulture)),
                            Weight = 0,
                            DoCountWhenDeliveryCostCalculated = false,
                            IsDeleted = false,
                            CookingTime = 0,
                            Cutlery = 1,
                            SKU = 0,
                            CalculateByCoefficient = false,
                            UseCustomWarehouse = false,
                            MarkGOST = false,
                            MarkEAC = false,
                            Protein = 0,
                            Fat = 0,
                            Carbohydrates = 0,
                            Netto = 0,
                            CoeffEnergy = 0,
                            FitHour = 0,
                            StartTemperature = 0,
                            EndTemperature = 0,
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            dish.BranchId = BranchRepository.SelectedBranchId;
                        }
                        dish.DateModified = DateTime.Now;
                        dish.AuthorModified = UserManager.UserLogged.Name;
                        db.Dishes.Add(dish);
                    }
                }

                db.SaveChanges();
            }

            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var conformity in conformities)
                {
                    if (conformity.DishChef == null)
                        continue;

                    var dishChefId = conformity.DishChef.ID.ToString(CultureInfo.InvariantCulture);

                    var dishid = conformity.Dish == null ? -1 : conformity.Dish.ID;

                    var dish = db.Dishes.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ChefExpertCode.ToLower() == dishChefId.ToLower()) ??
                               db.Dishes.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == dishid);

                    if (dish == null || conformity.DishChef.Recipe == null || conformity.DishChef.Recipe.RecipeDetails.Count == 0)
                        continue;

                    var itemsOfDishSet = db.ItemOfDishSets.Include("Product").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DishId == dish.ID).ToList();

                    foreach (var dishSet in itemsOfDishSet)
                    {
                        db.ItemOfDishSets.Remove(dishSet);
                    }

                    db.SaveChanges();

                    var foundedSetItems = conformity.DishChef.Recipe.RecipeDetails.ToList();

                    foreach (var detail in foundedSetItems)
                    {
                        if (string.IsNullOrWhiteSpace(detail.Prod))
                            continue;

                        var detailId = detail.Prod;
                        var product =
                            db.Products.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(
                                x => x.ChefExpertCode.ToLower() == detailId.ToLower());

                        if (product == null)
                            continue;

                        if (product.IsSetProduct)
                        {
                            var products = db.ItemOfProductSets.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ParentProductId == product.ID).ToList();

                            foreach (var itemOfProductSet in products)
                            {
                                var itemOfDishSet = new ItemOfDishSet
                                {
                                    DishId = dish.ID,
                                    Amount = (Convert.ToDecimal(detail.Brutto, CultureInfo.InvariantCulture) / 1000) * itemOfProductSet.Amount,
                                    ProductId = itemOfProductSet.ProductId,
                                    MeasureId = itemOfProductSet.MeasureId
                                };
                                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                                {
                                    itemOfDishSet.BranchId = BranchRepository.SelectedBranchId;
                                }
                                itemOfDishSet.DateModified = DateTime.Now;
                                itemOfDishSet.AuthorModified = UserManager.UserLogged.Name;
                                db.ItemOfDishSets.Add(itemOfDishSet);
                            }

                            db.SaveChanges();
                        }
                        else
                        {
                            var itemOfDishSet = new ItemOfDishSet
                            {
                                DishId = dish.ID,
                                Amount = (Convert.ToDecimal(detail.Brutto, CultureInfo.InvariantCulture) / 1000),
                                ProductId = product.ID,
                                MeasureId = product.MeasureID
                            };
                            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                            {
                                itemOfDishSet.BranchId = BranchRepository.SelectedBranchId;
                            }
                            itemOfDishSet.DateModified = DateTime.Now;
                            itemOfDishSet.AuthorModified = UserManager.UserLogged.Name;
                            db.ItemOfDishSets.Add(itemOfDishSet);

                            db.SaveChanges();
                        }
                    }
                }

                db.SaveChanges();
            }

            return true;
        }

        public void SaveSemiproducts(string path)
        {
            try
            {
                ChefExpert chefExpert;
                LoadDataFromFile(path, out chefExpert);
                var conformities = new List<ProductConformity>();

                using (var db = PizzaAppDB.GetNewContext())
                {
                    conformities.AddRange(from ingridient in chefExpert.GetSemiProducts()
                                          let product =
                                              db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ChefExpertCode == ingridient.Fid) ??
                                              db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(
                                                  x => x.Name.ToLower() == ingridient.Name.ToLower())
                                          select new ProductConformity { ChefProduct = ingridient, Product = product });
                }

                SaveIngridientList(conformities, 2, false, null, true);

                using (var db = PizzaAppDB.GetNewContext())
                {
                    foreach (var conformity in conformities)
                    {
                        if (conformity.ChefProduct == null)
                            continue;

                        var chefProductId = conformity.ChefProduct.ID.ToString(CultureInfo.InvariantCulture);

                        var product = db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ChefExpertCode.ToLower() == chefProductId.ToLower());


                        if (product == null)
                            continue;

                        product.ChefExpertCode = chefProductId;
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            product.BranchId = BranchRepository.SelectedBranchId;
                        }
                        product.DateModified = DateTime.Now;
                        product.AuthorModified = UserManager.UserLogged.Name;
                        db.Entry(product).State = EntityState.Modified;
                    }

                    db.SaveChanges();
                }

                using (var db = PizzaAppDB.GetNewContext())
                {
                    foreach (var conformity in conformities)
                    {
                        if (conformity.ChefProduct == null)
                            continue;

                        var chefProductId = conformity.ChefProduct.ID.ToString(CultureInfo.InvariantCulture);

                        var product = db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ChefExpertCode.ToLower() == chefProductId.ToLower());


                        if (product == null || conformity.ChefProduct.Recipe == null ||
                            conformity.ChefProduct.Recipe.RecipeDetails.Count == 0)
                            continue;

                        var itemsOfProductSet = db.ItemOfProductSets.Include("Product").Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ParentProductId == product.ID).ToList();

                        var itemsOfProductToDelete = new List<ItemOfProductSet>();
                        var itemsOfProductAdd = new List<ItemOfProductSet>();

                        foreach (var ofProductSet in itemsOfProductSet)
                        {
                            if (ofProductSet.Product.IsSetProduct)
                            {
                                itemsOfProductAdd.AddRange(db.ItemOfProductSets.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ParentProductId == ofProductSet.ParentProductId));
                                itemsOfProductToDelete.Add(ofProductSet);
                            }
                        }

                        foreach (var ofProductSet in itemsOfProductToDelete)
                        {
                            itemsOfProductSet.Remove(ofProductSet);
                        }

                        itemsOfProductSet.AddRange(itemsOfProductAdd);

                        var foundedSetItems = conformity.ChefProduct.Recipe.RecipeDetails.ToList();
                        var itemsToDelete = new List<ItemOfProductSet>();

                        foreach (var ofProductSet in itemsOfProductSet)
                        {
                            var parentProduct = db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == ofProductSet.ParentProductId);
                            if (parentProduct == null)
                                continue;

                            var recipe =
                                foundedSetItems.FirstOrDefault(
                                    x => x.Prod != null &&
                                    x.Prod.ToLower() ==
                                    parentProduct.ChefExpertCode.ToLower());

                            if (recipe == null)
                            {
                                itemsToDelete.Add(ofProductSet);
                            }
                            else
                            {
                                foundedSetItems.Remove(recipe);
                            }
                        }

                        foreach (var itemOfDishSet in itemsToDelete)
                        {
                            db.ItemOfProductSets.Remove(itemOfDishSet);
                        }

                        foreach (var detail in foundedSetItems)
                        {
                            if (string.IsNullOrWhiteSpace(detail.Prod))
                                continue;

                            var detailId = detail.Prod;
                            var detailProduct =
                                db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(
                                    x => x.ChefExpertCode.ToLower() == detailId.ToLower());

                            if (detailProduct == null)
                                continue;
                            var itemOfProductSet = new ItemOfProductSet
                            {
                                ParentProductId = product.ID,
                                MeasureId = product.MeasureID,
                                ProductId = detailProduct.ID,
                                Amount = Convert.ToDecimal(detail.Brutto, CultureInfo.InvariantCulture) / 1000
                            };
                            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                            {
                                itemOfProductSet.BranchId = BranchRepository.SelectedBranchId;
                            }
                            itemOfProductSet.DateModified = DateTime.Now;
                            itemOfProductSet.AuthorModified = UserManager.UserLogged.Name;
                            db.ItemOfProductSets.Add(itemOfProductSet);
                        }
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                UserMessage.Show(string.Format("{0}, {1}", ex.Message, ex.StackTrace));
            }
        }

        private void CreateDishCategory(IEnumerable<Product> conformities)
        {
            CreateDefaultSection();

            var categories = conformities.Select(x => new { x.DishCategory });

            var createdCategory = new Dictionary<string, Category>();

            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var conform in categories)
                {
                    var category = db.Categories.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name.ToLower() == conform.DishCategory.Name.ToLower());

                    if (conform.DishCategory.ID != -1 || category != null ||
                        createdCategory.ContainsKey(conform.DishCategory.Name.ToLower()))
                        continue;

                    var section = db.Sections.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name.ToLower() == "Загружен из Шеф Эксперт".ToLower());

                    var dishCategory = new Category
                    {
                        Name = conform.DishCategory.Name,
                        SectionID = section != null ? section.ID : db.Sections.Where(x => x.BranchId == BranchRepository.SelectedBranchId).First().ID,
                        Weight = 666
                    };
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        dishCategory.BranchId = BranchRepository.SelectedBranchId;
                    }
                    dishCategory.DateModified = DateTime.Now;
                    dishCategory.AuthorModified = UserManager.UserLogged.Name;
                    db.Categories.Add(dishCategory);
                    createdCategory.Add(conform.DishCategory.Name.ToLower(), dishCategory);
                }
                db.SaveChanges();
            }
        }

        private void CreateProductCategories(IEnumerable<Product> conformities)
        {
            CreateDefaultMeasure();

            var categories = conformities.Select(x => new { x.ProductCategory, x.Unit }).ToList();

            var createdCategory = new Dictionary<string, ProductCategory>();

            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var conform in categories)
                {
                    var category = db.ProductCategories.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name.ToLower() == conform.ProductCategory.Name.ToLower());

                    if (conform.ProductCategory.ID != -1 || category != null ||
                        createdCategory.ContainsKey(conform.ProductCategory.Name.ToLower()))
                        continue;

                    var measure = db.Measures.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name.ToLower() == conform.Unit.ToLower() || x.ShortName.ToLower() == conform.Unit.ToLower()) ??
                                      (db.Measures.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.IsBaseMeasure) ?? db.Measures.Where(x => x.BranchId == BranchRepository.SelectedBranchId).First());

                    var prodCategory = new ProductCategory
                    {
                        Name = conform.ProductCategory.Name,
                        ParentCategoryID = 0,
                        DefaultMeasureID = measure.ID,
                    };
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        prodCategory.BranchId = BranchRepository.SelectedBranchId;
                    }
                    prodCategory.DateModified = DateTime.Now;
                    prodCategory.AuthorModified = UserManager.UserLogged.Name;
                    db.ProductCategories.Add(prodCategory);

                    createdCategory.Add(conform.ProductCategory.Name.ToLower(), prodCategory);

                }
                db.SaveChanges();
            }
        }

        private void CreateDefaultMeasure()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (!db.Measures.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any())
                {
                    var measure = new Measure
                    {
                        Name = "кг",
                        ShortName = "кг",
                        IsBaseMeasure = true,
                        ForeignName = "kg"
                    };
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        measure.BranchId = BranchRepository.SelectedBranchId;
                    }
                    measure.DateModified = DateTime.Now;
                    measure.AuthorModified = UserManager.UserLogged.Name;
                    db.Measures.Add(measure);

                    db.SaveChanges();
                }
            }
        }

        private void CreateDefaultSection()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (!db.Sections.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Any() ||
                    db.Sections.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name.ToLower() == "Загружен из Шеф Эксперт") == null)
                {
                    var section = new Section
                    {
                        Name = "Загружен из Шеф Эксперт",
                        DoPrintCheck = false,
                        PeopleWorks = 0,
                        EquipmentWorks = 0
                    };
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        section.BranchId = BranchRepository.SelectedBranchId;
                    }
                    section.DateModified = DateTime.Now;
                    section.AuthorModified = UserManager.UserLogged.Name;
                    db.Sections.Add(section);
                    db.SaveChanges();
                }
            }
        }
    }
}
