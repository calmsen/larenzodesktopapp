﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public class CouponRepository: ICouponRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public Coupon GetItem(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Set<Coupon>().Find(id);
            }
                
        }

        public List<Coupon> GetList(string couponCode, bool? activated, int? couponRuleId, bool withBranch = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<Coupon>().AsQueryable();
                if (!string.IsNullOrEmpty(couponCode))
                    result = result.Where(x => x.Code == couponCode);
                if (activated != null)
                    result = result.Where(x => x.Activated == activated);
                if (couponRuleId != null)
                    result = result.Where(x => x.CouponRuleId == couponRuleId);
                if (withBranch)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        return result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToList();
                    }
                }
                return result.ToList();
            }
                
        }

        public Coupon GetCouponByCode(string code)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<Coupon>().Include(x => x.CouponRule).FirstOrDefault(x => x.Code.ToLower() == code.ToLower());
                return result;
            }
                
        }
        public Coupon GetCouponById(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<Coupon>().Include(x => x.CouponRule).FirstOrDefault(x => x.ID == id);
                return result;
            }                
        }

        public void DeleteItem(Coupon entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<Coupon>().Remove(entity);
                db.SaveChanges();
            }
        }

        private async Task UpdateItemAsync(Coupon entity)
        {
            if (entity.ID != 0)
            {
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                using (var db = PizzaAppDB.GetNewContext())
                {
                    db.Entry(entity).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
            }
        }

        private async Task<int> InsertItemAsync(Coupon entity)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                entity.BranchId = BranchRepository.SelectedBranchId;
            }
            entity.DateModified = DateTime.Now;
            entity.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<Coupon>().Add(entity);
                await db.SaveChangesAsync();
                return entity.ID;
            }
        }

        public async Task<int> InsertOrUpdateAsync(Coupon entity)
        {
            if (entity.ID == 0)
            {
                await InsertItemAsync(entity);
            }
            else
            {
                await UpdateItemAsync(entity);
            }
            return entity.ID;
        }
    }
}
