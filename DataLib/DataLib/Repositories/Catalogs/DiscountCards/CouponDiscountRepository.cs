﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public class CouponDiscountRepository: ICouponDiscountRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public List<CouponDiscount> GetCouponDiscountsByCouponRuleId(int couponRuleId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Set<CouponDiscount>().Where(x => x.CouponRuleId == couponRuleId).ToList();
            }
        }

        public void DeleteItem(CouponDiscount entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<CouponDiscount>().Remove(entity);
                db.SaveChanges();
            }
        }

        private void UpdateItem(CouponDiscount entity)
        {
            if (entity.ID != 0)
            {
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                using (var db = PizzaAppDB.GetNewContext())
                {
                    db.Entry(entity).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        private int InsertItem(CouponDiscount entity)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                entity.BranchId = BranchRepository.SelectedBranchId;
            }
            entity.DateModified = DateTime.Now;
            entity.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<CouponDiscount>().Add(entity);
                db.SaveChanges();
                return entity.ID;
            }                
        }

        public int InsertOrUpdate(CouponDiscount entity)
        {
            if (entity.ID == 0)
            {
                InsertItem(entity);
            }
            else
            {
                UpdateItem(entity);
            }
            return entity.ID;
        }
    }
}
