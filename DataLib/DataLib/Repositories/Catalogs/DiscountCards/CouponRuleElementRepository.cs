﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public class CouponRuleElementRepository: ICouponRuleElementRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        private async Task<List<CouponRuleElement>> GetListAsync(int couponRuleId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<CouponRuleElement>().Where(x => x.CouponRuleId == couponRuleId);
                
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    return await result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToListAsync();
                }
                return await result.ToListAsync();
            }
                
        }

        public async Task<List<CouponRuleElement>> GetByCouponRuleId(int couponRuleId)
        {
            var resultAsync = GetListAsync(couponRuleId);
            var result = await resultAsync;
            return result ?? new List<CouponRuleElement>();
        }

        public List<CouponRuleElement> GetByCouponRuleIdSync(int couponRuleId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<CouponRuleElement>().Where(x => x.CouponRuleId == couponRuleId);
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    return result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToList();
                }
                return result.ToList();
            }
        }

        private void DeleteItem(CouponRuleElement entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<CouponRuleElement>().Attach(entity);
                db.Set<CouponRuleElement>().Remove(entity);
                db.SaveChanges();
            }
        }

        public async Task DeleteByCouponRuleId(int couponRuleId)
        {
            var resultAsync = GetListAsync(couponRuleId);
            var result = await resultAsync;
            foreach (var item in result)
            {
                DeleteItem(item);
            }
        }

        private async Task UpdateItemAsync(CouponRuleElement entity)
        {
            if (entity.ID != 0)
            {
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                using (var db = PizzaAppDB.GetNewContext())
                {
                    db.Entry(entity).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                    
            }
        }

        private async Task<int> InsertItemAsync(CouponRuleElement entity)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                entity.BranchId = BranchRepository.SelectedBranchId;
            }
            entity.DateModified = DateTime.Now;
            entity.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<CouponRuleElement>().Add(entity);
                await db.SaveChangesAsync();
                return entity.ID;
            }                
        }

        public async Task<int> InsertOrUpdateAsync(CouponRuleElement entity)
        {
            if (entity.ID == 0)
            {
                await InsertItemAsync(entity);
            }
            else
            {
                await UpdateItemAsync(entity);
            }
            return entity.ID;
        }
    }
}
