﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Others;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Drivers
{
    public class DriverManager: IDriverManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public List<Driver> GetList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Drivers.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
        }

        public void Add(Driver driver)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    driver.BranchId = BranchRepository.SelectedBranchId;
                }
                driver.DateModified = DateTime.Now;
                driver.AuthorModified = UserManager.UserLogged.Name;
                db.Drivers.Add(driver);
                db.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var driver = db.Drivers.FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.ID == id);
                db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DriverID == id).ToList().ForEach(x => x.DriverID = null);
                db.Drivers.Remove(driver);
                db.SaveChanges();
            }
        }

        public void Update(Driver driver)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    driver.BranchId = BranchRepository.SelectedBranchId;
                }
                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public Driver GetById(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Drivers.FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.ID == id);
            }
        }

        public Driver GetByName(string name)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Drivers.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name == name);
            }
        }

        public string Validate(Driver driver, bool requiredPass)
        {
            string errors = string.Empty;

            if (driver.Name.Length == 0) errors += "Укажите имя" + Environment.NewLine;
            if((requiredPass && driver.Password.Length < 5) || (!requiredPass && driver.Password.Length > 0 && driver.Password.Length < 5)) errors += "Пароль должен быть длиннее 4 символов.\n";
            return errors;
        }

        /// <summary>
        /// Авторизация водителя
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Driver Login(string name, string password)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                password = new Hasher().GetHashString(password);
                var driver = db.Drivers.FirstOrDefault(x => x.BranchId == BranchRepository.SelectedBranchId && x.Name == name && x.Password == password);
                return driver;
            }
        }
        public Task<Driver> LoginAsync(string name, string password)
        {
            return Task.Run(() => Login(name, password));
        }
    }
}
