﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Categories
{
    public class CategoryRepository: ICategoryRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public virtual List<Category> GetItems(bool withBranch = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Categories.Include(x => x.Brand).Include(x => x.Section);
                if (withBranch)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        result = result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0);
                    }
                }

                return result.ToList();
            }
        }

        public virtual Category GetItem(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Categories.Include(x => x.Brand).Include(x => x.Section).FirstOrDefault(x => x.ID == id);
            }
        }

        public virtual void UpdateItem(Category entity)
        {
            if (entity.ID != 0)
            {
                GetItems();
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                using (var db = PizzaAppDB.GetNewContext())
                {
                    db.Entry(entity).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public virtual void DeleteItem(Category entity)
        {
            var item = GetItem(entity.ID);
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<Category>().Remove(item);
                db.SaveChanges();
            }
        }

        public virtual int InsertItem(Category entity)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                entity.BranchId = BranchRepository.SelectedBranchId;
            }
            entity.DateModified = DateTime.Now;
            entity.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<Category>().Add(entity);
                db.SaveChanges();
            }
            return entity.ID;
        }
    }
}
