﻿using System.Collections.Generic;
using System.Linq;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Categories
{
    public class CategoryCachedRepository : CategoryRepository
    {

        private List<Category> _cachedItemList;
        private List<Category> GetAllItemsFromCache()
        {
            if (_cachedItemList == null)
            {
                _cachedItemList = base.GetItems(false);
            }
            return _cachedItemList;
        }

        public override List<Category> GetItems(bool withBranch = true)
        {
            var result = GetAllItemsFromCache();
            if (withBranch)
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    var resultNew = result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToList();
                    return resultNew;
                }
            }
            else
            {
                return result.ToList();
            }
            return result.ToList();
        }

        public override Category GetItem(int id)
        {
            return GetAllItemsFromCache().FirstOrDefault(x => x.ID == id);
        }

        public override void UpdateItem(Category entity)
        {
            base.UpdateItem(entity);
            _cachedItemList = null;
        }

        public override void DeleteItem(Category entity)
        {
            base.DeleteItem(entity);
            _cachedItemList = null;
        }

        public override int InsertItem(Category entity)
        {
            base.InsertItem(entity);
            _cachedItemList = null;
            return entity.ID;
        }
    }
}
