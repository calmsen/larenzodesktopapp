﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Others.Times;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Sections
{
    public class SectionRepository: ISectionRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }        
        [Inject]
        public IBrandManager BrandManager { get; set; }
        [Inject]
        public IPointOfSaleRepository PointOfSaleRepository { get; set; }
        [Inject]
        public ITimeSheetManager TimeSheetManager { get; set; }

        public List<Section> GetItems(bool withBranch = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<Section>();
                if (withBranch)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        return result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToList();
                    }
                }
                else
                {
                    return result.ToList();
                }
                return result.ToList();
            }
            
        }

        public Section GetById(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Sections.Include(x => x.EquipmentFactorCollection)
                    .Include(x => x.PeopleFactorCollection).Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(
                    x => x.ID == id);
            }                
        }

        public List<Section> GetList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Sections.Include(x => x.EquipmentFactorCollection)
                .Include(x => x.PeopleFactorCollection).Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();
            }
                
        }

        private int GetSectionId(string sectionName)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var buffer = db.Sections.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.Name == sectionName);
                if (buffer == null)
                {
                    return 0;
                }
                return buffer.ID;
            }
            
        }


        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Добавление цеха </summary>
        ////====================================
        public void Add(Section section)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                section.BranchId = BranchRepository.SelectedBranchId;
            }
            section.DateModified = DateTime.Now;
            section.AuthorModified = UserManager.UserLogged.Name;
            DateTime minDate;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Sections.Attach(section);
                db.Entry(section).State = EntityState.Added;
                db.SaveChanges();
                // Получить минимальную дату из существующих в таблице, именно она и будет время начала сесии
                minDate = db.TimeSheets.Min(x => x.Time);
            }              
                        
            var brands = BrandManager.GetList();
            var pointOfSales = PointOfSaleRepository.GetItems();
            foreach (var brand in brands)
            {
                foreach (var pointOfSale in pointOfSales)
                {
                    // Добавить упоминание о цехе в таблицу времени работы
                    TimeSheetManager.AddSectionToTimeSheetTable(section, minDate, brand.ID, pointOfSale.ID);
                }
            }
        }


        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Удаление цеха </summary>
        ////==================================
        public void Remove(int sectionId)
        {
            Section section;
            using (var db = PizzaAppDB.GetNewContext())
            {
                // Получить цех с заданным Id
                section = db.Sections.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == sectionId);

                // Цех не найден
                if (section == null)
                {
                    throw new NullReferenceException(string.Format("Цех с ID {0} не найден", sectionId));
                }

                // Удалить цех
                db.Sections.Remove(section);
                db.SaveChanges();
            }

            // Удалить упоминание о цехе из таблицы работы
            TimeSheetManager.RemoveSectionFromTimeSheetTable(section);
        }

        public void Update(Section section)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                section.BranchId = BranchRepository.SelectedBranchId;
            }
            section.DateModified = DateTime.Now;
            section.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Entry(section).State = EntityState.Modified;
                db.SaveChanges();
            }
               
        }

        private bool DoExists(string name)
        {
            return GetSectionId(name) != 0;
        }

        public string Validate(Section section, bool isUnique = true)
        {
            string errors = string.Empty;

            if (section.Name.Length == 0) errors += "Укажите название цеха. \n";
            else if (isUnique && DoExists(section.Name)) errors += "Цех с таким именем уже существует. \n";

            return errors;
        }

    }
}
