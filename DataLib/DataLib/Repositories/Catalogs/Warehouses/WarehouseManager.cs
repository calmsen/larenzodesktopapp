﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Warehouses
{
    public class WarehouseManager: IWarehouseManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public List<Warehouse> GetWarehouses()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Warehouses.Include(x=>x.PointOfSale).Include(x=>x.WarehouseForDish).Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.ID).ToList();
            }
        }

        public Warehouse GetWarehouse(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Warehouses.Include(x=>x.PointOfSale).Include(x=>x.WarehouseForDish).Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }

        public void AddWarehouse(Warehouse entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Warehouses.Add(entity);
                db.SaveChanges();
            }
        }

        public void UpdateWarehouse(Warehouse entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void DeleteWarehouse(Warehouse entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var warehouse = db.Warehouses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == entity.ID);
                if (warehouse != null)
                {
                    db.Warehouses.Remove(warehouse);
                    db.SaveChanges();
                }
            }
        }
    }
}
