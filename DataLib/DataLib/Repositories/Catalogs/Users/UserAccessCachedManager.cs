﻿using System.Collections.Generic;
using System.Linq;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Users
{
    public class UserAccessCahcedManager: UserAccessManager
    {
        private List<UserRightsCacheItem> _cachedRights = new List<UserRightsCacheItem>();
        public override List<AccessRightInfo> GetForUser(int userId, bool doIgnoreAdminFlag = false)
        {
            var userRightsCacheItem = _cachedRights.FirstOrDefault(x => x.UserId == userId && x.DoIgnoreAdminFlag == doIgnoreAdminFlag);
            if (userRightsCacheItem == null)
            {
                userRightsCacheItem = new UserRightsCacheItem
                {
                    UserId = userId,
                    DoIgnoreAdminFlag = doIgnoreAdminFlag,
                    UserRights = base.GetForUser(userId, doIgnoreAdminFlag)
                };
                _cachedRights.Add(userRightsCacheItem);
            }
            return userRightsCacheItem.UserRights;
        }

        public override void SetForUser(int userId, List<AccessRightInfo> rights)
        {
            base.SetForUser(userId, rights);
            _cachedRights.Where(x => x.UserId != userId);
        }

        public override bool GroupContainsRight(List<AccessRightInfo> list, AccessRightAlias rightAlias)
        {
            return list.Where(x => x.Allowed).Any(x => x.Code == (int)rightAlias);
        }
    }

}
