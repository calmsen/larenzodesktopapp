﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Users;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Users
{
    public class UserCachedManager: UserManager
    {
        private Dictionary<int, bool> _cachedIsAdmin = new Dictionary<int, bool>();

        public override void Add(User user)
        {
            base.Add(user);
            if (_cachedIsAdmin.ContainsKey(user.ID))
                _cachedIsAdmin.Remove(user.ID);
        }

        public override void Remove(int id)
        {
            base.Remove(id);
            if (_cachedIsAdmin.ContainsKey(id))
                _cachedIsAdmin.Remove(id);
        }

        public override void Update(User user)
        {
            base.Update(user);
            if (_cachedIsAdmin.ContainsKey(user.ID))
                _cachedIsAdmin.Remove(user.ID);
        }
        
        /// <summary>
        /// Авторизация пользователя
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override User Login(string name, string password)
        {
            UserLogged = base.Login(name, password);
            return UserLogged;
        }

        public override bool IsAdmin()
        {
            return UserLogged != null && UserLogged.Role != null && UserLogged.Role.Name == "Admin"; ;
        }

        public override bool IsAdmin(int userId)
        {
            if(_cachedIsAdmin.ContainsKey(userId))
                return _cachedIsAdmin[userId];
            bool isAdmin = this.IsAdmin(userId);
            _cachedIsAdmin.Add(userId, isAdmin);
            return isAdmin;
        }

        public override bool IsAdmin(User user)
        {
            if (_cachedIsAdmin.ContainsKey(user.ID))
                return _cachedIsAdmin[user.ID];
            bool isAdmin = this.IsAdmin(user);
            _cachedIsAdmin.Add(user.ID, isAdmin);
            return isAdmin;
        }

        public override bool IsWarehouseManager()
        {
            return UserLogged != null && UserLogged.Role != null && UserLogged.Role.Name == "WarehouseManager";
        }


        public override void Logout()
        {
            UserLogged = null;
        }

        public override bool IsLogged()
        {
            return UserLogged != null;
        }

        public override string GetUserNameOrEmptyString()
        {
            return this.IsLogged() ? UserLogged.Name : string.Empty;
        }
    }
}
