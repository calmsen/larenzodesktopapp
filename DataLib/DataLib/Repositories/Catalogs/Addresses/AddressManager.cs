﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using DataLib.Models;
using System.Data.Entity;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Addresses
{
    public class AddressManager: IAddressManager
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }

        public object[] GetAddressArray()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Addresses.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Select(x => x.Name).ToArray();
            }
        }

        public List<Address> GetAddressList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Addresses.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Include(x=>x.AddressGroup).OrderBy(x => x.Name).ToList();
            }
        }

        public List<Address> GetAddressList(int groupId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Addresses.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.AddressGroupID == groupId).ToList();
            }
        }

        public async Task UpdateAddress(Address address)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                address.BranchId = BranchRepository.SelectedBranchId;
                db.Entry(address).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
        }

        public async Task AddAddress(Address address)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    address.BranchId = BranchRepository.SelectedBranchId;
                }
                db.Addresses.Add(address);
                await db.SaveChangesAsync();
            }
        }

        public void DeleteAddress(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var address = db.Addresses.Find(id);
                db.Addresses.Remove(address);
                db.SaveChanges();
            }
        }

        public void BindToGroup(int addressId, int groupId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var address = db.Addresses.FirstOrDefault(x=>x.BranchId == BranchRepository.SelectedBranchId && x.ID == addressId);
                address.AddressGroupID = groupId;
                db.Entry(address).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void ClearGroupBind(int addressId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var address = db.Addresses.FirstOrDefault(x=>x.BranchId == BranchRepository.SelectedBranchId && x.ID == addressId);
                address.AddressGroupID = null;
                db.Entry(address).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
