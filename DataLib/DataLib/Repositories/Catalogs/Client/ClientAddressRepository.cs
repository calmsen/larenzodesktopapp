﻿using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Client;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Client
{
    public class ClientAddressRepository: IClientAddressRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public async Task<List<ClientAddress>> GetListAsync(int clientId, string street, string house, bool checkDateModified = false)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<ClientAddress>().Where(x =>
                        x.ClientId == clientId && x.Street == street && x.House == house);
                if (!string.IsNullOrEmpty(street))
                    result = result.Where(x => x.Street == street);
                if (!string.IsNullOrEmpty(house))
                    result = result.Where(x => x.House == house);
                if (checkDateModified)
                    result = result.Where(x => x.DateModified != null);
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    return await result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToListAsync();
                }
                return await result.ToListAsync();
            }
            
        }

        public async Task UpdateItemAsync(ClientAddress entity)
        {
            if (entity.ID != 0)
            {
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                using (var db = PizzaAppDB.GetNewContext())
                {
                    db.Entry(entity).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                    
            }
        }

        public async Task<int> InsertItemAsync(ClientAddress entity)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                entity.BranchId = BranchRepository.SelectedBranchId;
            }
            entity.DateModified = DateTime.Now;
            entity.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<ClientAddress>().Add(entity);
                await db.SaveChangesAsync();
                return entity.ID;
            }
                
        }

        public async Task<int> InsertOrUpdateAsync(ClientAddress entity)
        {
            if (entity.ID == 0)
            {
                await InsertItemAsync(entity);
            }
            else
            {
                await UpdateItemAsync(entity);
            }
            return entity.ID;
        }
    }
}
