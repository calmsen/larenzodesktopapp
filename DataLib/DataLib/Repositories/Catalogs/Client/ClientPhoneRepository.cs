﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Client;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Client
{
    public class ClientPhoneRepository: IClientPhoneRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        private async Task<List<ClientPhone>> GetListAsync(string phone, bool withBranch = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.Set<ClientPhone>().Where(x => x.Phone == phone);
                if (withBranch)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        return await result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToListAsync();
                    }
                }
                return await result.ToListAsync();

            }
                
        }
        public async Task<int> GetClientIdByPhone(string phone)
        {
            var resultAsync = GetListAsync(phone);
            var result = await resultAsync;
            return result.FirstOrDefault()?.ClientId??0;
        }

        private async Task UpdateItemAsync(ClientPhone entity)
        {
            if (entity.ID != 0)
            {
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                using (var db = PizzaAppDB.GetNewContext())
                {
                    db.Entry(entity).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                    
            }
        }

        private async Task<int> InsertItemAsync(ClientPhone entity)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                entity.BranchId = BranchRepository.SelectedBranchId;
            }
            entity.DateModified = DateTime.Now;
            entity.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<ClientPhone>().Add(entity);
                await db.SaveChangesAsync();
                return entity.ID;
            }
                
        }

        public async Task<int> InsertOrUpdateAsync(ClientPhone entity)
        {
            if (entity.ID == 0)
            {
                await InsertItemAsync(entity);
            }
            else
            {
                await UpdateItemAsync(entity);
            }
            return entity.ID;
        }
    }
}
