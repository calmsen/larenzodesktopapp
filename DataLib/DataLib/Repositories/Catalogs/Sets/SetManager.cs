﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using DataLib.Models;
using System.Linq;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Sets
{
    public class SetManager: ISetManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public List<Set> GetSets()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Sets.Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.ID).ToList();
            }
        }

        public List<SetOfDishes> GetDetailDishes(int setId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.SetOfDisheses.Include("Dish").Include("Set").Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SetID == setId).ToList();
            }
        }

        public void AddDetailDishes(int setId, Dish dish)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var sod = new SetOfDishes { SetID = setId, DishID = dish.ID };
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    sod.BranchId = BranchRepository.SelectedBranchId;
                }
                sod.DateModified = DateTime.Now;
                sod.AuthorModified = UserManager.UserLogged.Name;
                db.SetOfDisheses.Add(sod);
                db.SaveChanges();
            }
        }

        public void DeleteDetailDishes(SetOfDishes setOfDishes)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.SetOfDisheses.Remove(setOfDishes);
                db.SaveChanges();
            }
        }

        public Set GetSet(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Sets.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }

        public void AddSet(Set entity, List<SetOfDishes> setOfDishes)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Sets.Add(entity);

                foreach (var ofDish in setOfDishes)
                {
                    var setItem = new SetOfDishes
                    {
                        DishID = ofDish.DishID,
                        SetID = entity.ID,
                    };
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        setItem.BranchId = BranchRepository.SelectedBranchId;
                    }
                    setItem.DateModified = DateTime.Now;
                    setItem.AuthorModified = UserManager.UserLogged.Name;
                    db.SetOfDisheses.Add(setItem);
                }

                db.SaveChanges();
            }
        }

        public void AddSet(Set entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Sets.Add(entity);
                db.SaveChanges();
            }
        }

        public void UpdateSet(Set entity, List<SetOfDishes> setOfDishes)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(entity).State = EntityState.Modified;

                var existsItem = db.SetOfDisheses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SetID == entity.ID).ToList();

                foreach (var setOfDishese in setOfDishes)
                {
                    if (setOfDishese.ID == -1)
                    {
                        var sod = new SetOfDishes { DishID = setOfDishese.DishID, SetID = setOfDishese.SetID };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            sod.BranchId = BranchRepository.SelectedBranchId;
                        }
                        sod.DateModified = DateTime.Now;
                        sod.AuthorModified = UserManager.UserLogged.Name;
                        db.SetOfDisheses.Add(sod);
                    }
                    else
                    {
                        if (existsItem.Any(x => x.ID == setOfDishese.ID))
                        {
                            var item = existsItem.FirstOrDefault(x => x.ID == setOfDishese.ID);
                            existsItem.Remove(item);
                        }
                    }
                }

                foreach (var ofDishese in existsItem)
                {
                    db.SetOfDisheses.Remove(ofDishese);
                }

                db.SaveChanges();
            }
        }

        public void UpdateSet(Set entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void DeleteSet(Set entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var coefficient = db.Sets.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == entity.ID);
                if (coefficient != null)
                {
                    db.Sets.Remove(coefficient);
                    db.SaveChanges();
                }
            }
        }

        public void UpdateDetailSetItem(int setId, List<SetOfDishes> setOfDish)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var existsItem = db.SetOfDisheses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SetID == setId).ToList();

                foreach (var setOfDishese in setOfDish)
                {
                    if (setOfDishese.ID == -1)
                    {
                        var sod = new SetOfDishes { DishID = setOfDishese.DishID, SetID = setOfDishese.SetID };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            sod.BranchId = BranchRepository.SelectedBranchId;
                        }
                        sod.DateModified = DateTime.Now;
                        sod.AuthorModified = UserManager.UserLogged.Name;
                        db.SetOfDisheses.Add(sod);
                    }
                    else
                    {
                        if (existsItem.Any(x => x.ID == setOfDishese.ID))
                        {
                            var item = existsItem.FirstOrDefault(x => x.ID == setOfDishese.ID);
                            existsItem.Remove(item);
                        }
                    }
                }

                foreach (var ofDishese in existsItem)
                {
                    db.SetOfDisheses.Remove(ofDishese);
                }

                db.SaveChanges();
            }
        }
    }
}