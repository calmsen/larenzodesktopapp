﻿using System.Collections.Generic;
using DataLib.Models;
using System.Linq;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Sets
{
    public class SetCalculationManager: ISetCalculationManager
    {
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public List<OrderItem> GetSets(List<OrderItem> orderItems, bool detail = false)
        {
            var items = new List<OrderItem>();

            var dicOfSets = new Dictionary<int, int>();

            foreach (var orderItem in orderItems)
            {
                if (orderItem.Dish.CalculateByCoefficient)
                {
                    if (orderItem.Dish.CoefficientId == null)
                        continue;
                    
                    if (dicOfSets.ContainsKey(orderItem.Dish.CoefficientId.Value))
                    {
                        dicOfSets[orderItem.Dish.CoefficientId.Value] += orderItem.Amount;
                    }
                    else
                    {
                        dicOfSets.Add(orderItem.Dish.CoefficientId.Value, orderItem.Amount);
                    }
                }
            }

            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var sets in dicOfSets)
                {
                    var coeffParams = db.SetParamses.Include("Set").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SetID == sets.Key).ToList();

                    KeyValuePair<int, int> setAmmountPair = sets;

                    var findInterval =
                        coeffParams.FirstOrDefault(x => x.MinValue <= setAmmountPair.Value && setAmmountPair.Value <= x.MaxValue);

                    if (findInterval != null)
                    {
                        if (detail)
                        {
                            var setitems = db.SetOfDisheses.Include("Dish").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SetID == findInterval.SetID);

                            foreach (var setitem in setitems)
                            {
                                items.Add(new OrderItem
                                    {
                                        Amount = findInterval.DishValue,
                                        Dish = new Dish{Name = setitem.Dish.Name}
                                    });
                            }
                        }
                        else
                        {
                            items.Add(new OrderItem
                                {
                                    Amount = findInterval.DishValue,
                                    Dish = new Dish {Name = findInterval.Set.Name}
                                });
                        }
                    }
                    else
                    {
                        var maxMinElement = coeffParams.Where(x => x.MaxValue <= setAmmountPair.Value)
                                   .OrderByDescending(x => x.MinValue)
                                   .FirstOrDefault();

                        if (maxMinElement != null)
                        {
                            int counter = maxMinElement.MaxValue;
                            int itemcCount = maxMinElement.DishValue;

                            while (setAmmountPair.Value >= counter)
                            {
                                counter += maxMinElement.IncrementValue;
                                itemcCount += maxMinElement.DishValue;
                            }

                            if (maxMinElement.Round)
                            {
                                if (maxMinElement.RoundDirection == 1)
                                {
                                    if (itemcCount - maxMinElement.DishValue > 0)
                                    {
                                        itemcCount -= maxMinElement.DishValue;
                                    }
                                }
                            }

                            if (detail)
                            {
                                var setitems = db.SetOfDisheses.Include("Dish").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SetID == maxMinElement.SetID);
                                foreach (var setitem in setitems)
                                {
                                    items.Add(new OrderItem
                                        {
                                            Amount = itemcCount,
                                            Dish = new Dish{Name = setitem.Dish.Name}
                                        });
                                }
                            }
                            else
                            {
                                items.Add(new OrderItem
                                    {
                                        Amount = itemcCount,
                                        Dish = new Dish { Name = maxMinElement.Set.Name}
                                    });
                            }
                        }
                    }
                }
            }

            return items;
        }
    }
}
