﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Sets
{
    public class SetParamsManager: ISetParamsManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public List<SetParams> GetSetParamses()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.SetParamses.Include("Set").Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.ID).ToList();
            }
        }

        public SetParams GetSetParams(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.SetParamses.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
            }
        }

        public void AddSetParams(SetParams entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }                    
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.SetParamses.Attach(entity);
                db.Entry(entity).State = EntityState.Added;
                db.SaveChanges();
            }
        }

        public void UpdateSetParams(SetParams entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    entity.BranchId = BranchRepository.SelectedBranchId;
                }
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void DeleteSetParams(SetParams entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var coefficientParams = db.SetParamses.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == entity.ID);
                if (coefficientParams != null)
                {
                    db.SetParamses.Remove(coefficientParams);
                    db.SaveChanges();
                }
            }
        }
    }
}