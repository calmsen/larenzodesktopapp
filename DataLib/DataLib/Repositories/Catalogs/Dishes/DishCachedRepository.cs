﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Dishes
{
    public class DishCachedRepository: DishRepository
    {

        protected List<Dish> _cachedItemList;
        
        private List<Dish> GetItems(bool withBranch = true)
        {
            var result = GetAllItemsFromCache();
            if (withBranch)
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    result = result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToList();
                    return result;
                }
            }
            else
            {
                return result.ToList();
            }
            return result.ToList();
        }

        public override Dish GetItem(int id)
        {
            return GetAllItemsFromCache().FirstOrDefault(x => x.ID == id);
        }
        
        private List<Dish> GetAllItemsFromCache()
        {
            if (_cachedItemList == null)
            {
                using (var db = PizzaAppDB.GetNewContext())
                {
                    var result = db.Dishes.Include(x => x.Category).Include(x => x.Category.Brand).Include(x => x.FactorCollection).Include(x => x.Set).Include(x => x.Brand);
                    _cachedItemList = result.ToList();
                }
            }
            return _cachedItemList;
        }
        
        public override IEnumerable<Dish> GetItems(FilterModel filter, bool withBranch = true)
        {
            var items = GetItems(withBranch);
            var value = filter as DishFilter;
            if (value == null)
            {
                return items.ToList();
            }
            if (value.FilterByBrandId)
            {
                return items.Where(x => x.Brand.ID == value.BrandId).OrderBy(x => x.Name).ToList();
            }
            if (value.FilterByCategoryName)
            {
                if (value.FilterByIsDeleteMark)
                {
                    return items.Where(x => x.Category.Name == value.CategoryName && x.Brand.ID == value.BrandId)
                      .ToList();
                }

                return items.Where(x => x.Category.Name == value.CategoryName && x.IsDeleted == false && x.Brand.ID == value.BrandId)
                     .ToList();
            }

            if (value.FilterByIsDeleteMark)
            {
                return items.Where(x => x.Brand.ID == value.BrandId).ToList();
            }

            if (value.FilterBySKU)
            {
                return items.Where(x => value.SKUArray.Contains(x.SKU) && x.Brand.ID == value.BrandId);
            }

            var dishes = items.Where(x => x.IsDeleted == false).ToList();
            dishes = dishes.Where(x => x.Brand.ID == value.BrandId).ToList();
            return dishes.ToList();
        }

        public override Dish GetItem(FilterModel filter)
        {
            var value = (DishFilter)filter;
            Dish dish = null;

            if (value.FilterBySKU)
            {
                dish = GetItems().FirstOrDefault(x => x.SKU == value.SKU && x.Brand.ID == value.BrandId);
            }

            if (value.FilterByDishId)
            {
                dish = GetItems().FirstOrDefault(x => x.ID == value.DishId && x.Brand.ID == value.BrandId);
            }

            if (dish != null)
            {
                using (var db = PizzaAppDB.GetNewContext())
                {
                    dish.ItemsOfDishSets = db.ItemOfDishSets
                        .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                        .Where(x => x.DishId == dish.ID)
                        .Include(x => x.Product).Include(x => x.Measure).ToList();
                }
            }

            return dish;
        }


        public override void UpdateItem(Dish entity)
        {
            base.UpdateItem(entity);
            _cachedItemList = null;
        }


        public override void DeleteItem(Dish entity)
        {
            base.DeleteItem(entity);
            _cachedItemList = null;
        }


        public override void InsertItem(Dish entity)
        {
            base.InsertItem(entity);
            _cachedItemList = null;
        }
    }
}