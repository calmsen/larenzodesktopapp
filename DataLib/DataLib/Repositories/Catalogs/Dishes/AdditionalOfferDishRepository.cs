﻿using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System;
using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public class AdditionalOfferDishRepository: IAdditionalOfferDishRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        public void DeleteItem(AdditionalOfferDish entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<AdditionalOfferDish>().Remove(entity);
                db.SaveChanges();
            }
        }

        private void UpdateItem(AdditionalOfferDish entity)
        {
            if (entity.ID != 0)
            {
                entity.DateModified = DateTime.Now;
                entity.AuthorModified = UserManager.UserLogged.Name;
                using (var db = PizzaAppDB.GetNewContext())
                {
                    db.Entry(entity).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        private int InsertItem(AdditionalOfferDish entity)
        {
            if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
            {
                entity.BranchId = BranchRepository.SelectedBranchId;
            }
            entity.DateModified = DateTime.Now;
            entity.AuthorModified = UserManager.UserLogged.Name;
            using (var db = PizzaAppDB.GetNewContext())
            {
                db.Set<AdditionalOfferDish>().Add(entity);
                db.SaveChanges();
                return entity.ID;
            }
        }

        public int InsertOrUpdate(AdditionalOfferDish entity)
        {
            if (entity.ID == 0)
            {
                InsertItem(entity);
            }
            else
            {
                UpdateItem(entity);
            }
            return entity.ID;
        }

        public List<AdditionalOfferDish> GetItems(int dishId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.AdditionalOfferDishes
                .Include(x => x.Dish)
                .Where(x => x.Dish.IsDeleted == false)
                .Where(x => x.MainDishId == dishId)
                .ToList();
                return result;
            }
        }

        public List<AdditionalOfferDish> GetItems(IEnumerable<int> dishIds)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.AdditionalOfferDishes
                               .Include(x => x.Dish)
                               .Where(x => x.Dish.IsDeleted == false)
                               .Where(x => dishIds.Contains(x.MainDishId))
                               .ToList();
                return result;
            }               
        }
    }
}
