﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Entity;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Documents.Orders
{
    public class PreOrderManager: IPreOrderManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IRemainsOfGoodWiringsManager RemainsOfGoodWiringsManager { get; set; }
        public void Update(PreOrder preOrder)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    preOrder.BranchId = BranchRepository.SelectedBranchId;
                }
                preOrder.DateModified = DateTime.Now;
                preOrder.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(preOrder).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var entity = db.PreOrders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
                if (entity == null) throw new ArgumentException("Предзаказ с таким ID не  найден");
                entity.IsDeleted = true;
                db.SaveChanges();
            }
        }

        public void Restore(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var entity = db.PreOrders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
                if (entity == null) throw new ArgumentException("Предзаказ с таким ID не  найден");
                entity.IsDeleted = false;
                db.SaveChanges();
            }
        }

        public void Create(int orderId, DateTime date)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var preOrder = new PreOrder
                {
                    OrderID = orderId,
                    ReservationDateTime = date,
                };
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    preOrder.BranchId = BranchRepository.SelectedBranchId;
                }
                preOrder.DateModified = DateTime.Now;
                preOrder.AuthorModified = UserManager.UserLogged.Name;
                db.PreOrders.Add(preOrder);
                db.SaveChanges();
            }
        }

        public List<PreOrder> GetFullList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.PreOrders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Include(x => x.Order).ToList();
            }
        }


        public List<PreOrder> GetList(DateTime date, bool takeAllAfterDate = false)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var startTime = new DateTime(date.Year, date.Month, date.Day, 6, 0, 0);
                var endTime = startTime.AddHours(21);

                var orders = db.PreOrders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.Order.SessionID == null && x.ReservationDateTime >= startTime).Include(x => x.Order).Include(x => x.Order.Brand);
                if (!takeAllAfterDate)
                {
                    orders = orders.Where(x => x.ReservationDateTime <= endTime);
                }

                return orders.Include(x => x.Order).ToList();
            }
        }

        public List<PreOrderProduct> GetNeededProductList(DateTime date, bool isChecked)
        {
            List<PreOrder> preOrders = GetList(date, isChecked);
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = new List<PreOrderProduct>();

                var preOrderIds = preOrders.Select(x => x.ID);

                var allDishesInfo = db.PreOrders
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => preOrderIds.Contains(x.ID)).
                    SelectMany(x => x.Order.OrderItems).
                    GroupBy(x => x.DishID).
                    Select(x => new
                    {
                        DishID = x.Key,
                        Amount = x.Sum(a => a.Amount),
                    }).ToList();


                var dishIds = allDishesInfo.Select(x => x.DishID);

                var itemsOfDishSets = db.ItemOfDishSets.Include(x => x.Product).Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => dishIds.Contains(x.DishId)).ToList();

                var data = itemsOfDishSets.Select(x => new
                {
                    x.ProductId,
                    Amount = x.Amount * allDishesInfo.First(a => a.DishID == x.DishId).Amount
                }).GroupBy(x => x.ProductId).
                    Select(x => new PreOrderProduct
                    {
                        ProductID = x.Key,
                        NeededAmount = x.Sum(a => a.Amount)
                    }).ToList();


                var productsAmountData = RemainsOfGoodWiringsManager.GetRemainsOnDate(DateTime.Now).ProductRemains;
                var productNames = db.Products.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Select(x => new
                {
                    x.ID,
                    x.Name
                }).ToList();

                data.ForEach(x => result.Add(new PreOrderProduct
                {
                    ProductName = productNames.First(a => a.ID == x.ProductID).Name,
                    NeededAmount = x.NeededAmount,
                    StockAmount = productsAmountData.Any(a => a.ProductID == x.ProductID)
                        ? productsAmountData.First(a => a.ProductID == x.ProductID).Amount
                        : 0
                }));

                return result;
            }
        }
    }

    
}
