﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Others;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.DataRepository.Repositories.Others.Times;
using PaymentMethod = LaRenzo.TransferLibrary.PaymentMethod;
using LaRenzo.DataRepository.Repositories.Settings;
using LaRenzo.DataRepository.Repositories.Call;
using LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories.Catalogs.Categories;
using LaRenzo.DataRepository.Repositories.Catalogs.Dishes;
using LaRenzo.Interfaces;
using LaRenzo.DataRepository.Entities.Documents.Sessions;

namespace LaRenzo.DataRepository.Repositories.Documents.Orders
{
    
    public static class OrderManagerExtension
    {
        public static IEnumerable<Order> IncludeAll(this IQueryable<Order> set)
        {
            return set.Include(x => x.Session)
                      .Include(x => x.Brand)
                      .Include(x => x.DiscountCard)
                      .Include(x => x.Driver)
                      .Include(x => x.State)
                      .Include(x => x.OrderItems.Select(z => z.Dish.Category.Section));
        }

        public static IEnumerable<OrderItem> IncludeAll(this IQueryable<OrderItem> set)
        {
            return set.Include(x => x.Dish.Category.Section);
        }
    }
    public class OrderManager:IOrderManager, IOrderCodeChecker
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public ISessionManager SessionManager { get; set; }
        [Inject]
        public IStateManager StateManager { get; set; }
        [Inject]
        public ICouponDiscountRepository CouponDiscountRepository { get; set; }
        [Inject]
        public ICouponRuleElementRepository CouponRuleElementRepository { get; set; }
        [Inject]
        public ICategoryRepository CategoryRepository { get; set; }
        [Inject]
        public IDishRepository DishRepository { get; set; }
        [Inject]
        public ITimeSheetManager TimeSheetManager { get; set; }
        [Inject]
        public IGlobalOptionsManager GlobalOptionsManager { get; set; }
        public List<Order> GetOrdersByCouponRuleId(int couponRuleId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Orders
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => x.Coupon.CouponRuleId == couponRuleId)
                    .Include(x => x.Brand)
                    .Include(x => x.State)
                    .Include(x => x.Client)
                    .ToList();

            }
        }


        public List<Order> GetFullList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).ToList();

            }
        }

        public List<StatsOrderCount> GetStatsList()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).GroupBy(x => x.Phone).Select(x => new StatsOrderCount { Phone = x.Key, OrdersCount = x.Count() }).ToList();

            }
        }

        public List<Order> GetLastN(int n = 1000)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).OrderByDescending(x => x.ID).
                          Take(n).
                          Include(x => x.Driver).
                          Include(x => x.Brand).
                          Include(x => x.State).
                          Include(x => x.Session).ToList();
            }
        }

        public List<Order> GetByPeriod(DateTime begin, DateTime end, bool web)
        {
            begin = begin.Date;
            end = end.Date.AddDays(1).AddMilliseconds(-1);

            using (var db = PizzaAppDB.GetNewContext(180))
            {
                if (web)
                {
                    return db.Orders.OrderByDescending(x => x.ID)
                      .Where(x => x.Created >= begin && x.Created <= end && x.IsWebRequest)
                      .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                      .Include(x => x.Driver)
                      .Include(x => x.Brand)
                      .Include(x => x.State)
                      .Include(x => x.Session)
                      .Include(x => x.PointOfSale)
                      .ToList();
                }

                return
                    db.Orders.OrderByDescending(x => x.ID)
                      .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                      .Where(x => x.Created >= begin && x.Created <= end)
                      .Include(x => x.Driver)
                      .Include(x => x.Brand)
                      .Include(x => x.State)
                      .Include(x => x.Session)
                      .Include(x => x.PointOfSale)
                      .Include(x => x.OrderItems)
                      .Include(x => x.OrderItems.Select(y => y.Dish))
                      .ToList();
            }
        }


        public Order GetByGuid(Guid guid)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ReservationGuid == guid);
            }
        }

        public async Task<List<Order>> GetCurrentSessionList(int? branchId = null, int? brandId = null)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var session = await SessionManager.GetOpened(branchId);
                if (session == null) return new List<Order>();
                if (brandId == null)
                {
                    var query = db.Orders.Include(x => x.PointOfSale)
                        .Where(x => x.BranchId == (branchId ?? BranchRepository.SelectedBranchId))
                        .Where(x => x.SessionID == session.ID);
                    return query.IncludeAll().ToList();
                }
                else
                {
                    var query = db.Orders.Include(x => x.PointOfSale)
                        .Where(x => x.BranchId == (branchId ?? BranchRepository.SelectedBranchId)
                                    && x.IdBrand == brandId)
                        .Where(x => x.SessionID == session.ID);
                    return query.IncludeAll().ToList();
                }
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Вернуть список заказов по телефону </summary>
        ////=======================================================
        public async Task<List<Order>> GetOrdersByPhone(string phoneNumber, bool isCurentSession = false)
        {

            if (!string.IsNullOrEmpty(phoneNumber) && phoneNumber.Length == 11)
            {
                phoneNumber = phoneNumber.Substring(1);
            }

            using (var db = PizzaAppDB.GetNewContext())
            {
                List<Order> selectedOrders;

                if (isCurentSession)
                {
                    selectedOrders = await db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.Session.Closed == null && x.SessionID != null && x.Phone == phoneNumber).Include(x => x.DiscountCard).ToListAsync();
                }
                else
                {
                    selectedOrders = await db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.Session.Closed != null && x.SessionID != null && x.Phone == phoneNumber).Include(x => x.DiscountCard).ToListAsync();
                }

                return selectedOrders;

            }
        }


        public void UpdatePreOrderTime(int id, DateTime dateTime)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var preorder = db.PreOrders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);

                if (preorder != null)
                {
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        preorder.BranchId = BranchRepository.SelectedBranchId;
                    }
                    preorder.ReservationDateTime = dateTime;
                    preorder.DateModified = DateTime.Now;
                    preorder.AuthorModified = UserManager.UserLogged.Name;
                    db.Entry(preorder).State = EntityState.Modified;

                    db.SaveChanges();
                }
            }
        }

        public void Update(Order order)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    order.BranchId = BranchRepository.SelectedBranchId;
                }
                order.DateModified = DateTime.Now;
                if (UserManager.UserLogged != null)
                    order.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
            }
        }


        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Обновить инфу по заказу </summary>
        ////============================================
        public void Update(Order order, List<OrderItem> items)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    order.BranchId = BranchRepository.SelectedBranchId;
                }
                order.DiscountCard = null;
                order.DateModified = DateTime.Now;
                order.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
            }

            // Для всех элементов заказа установить обновлённый id заказа
            items.ForEach(x => x.OrderID = order.ID);
            // 
            UpdateOrderItems(order.OrderItems.ToList(), items);
        }


        private void UpdateOrderItems(List<OrderItem> oldItems, List<OrderItem> newItems)
        {

            using (var db = PizzaAppDB.GetNewContext())
            {
                newItems.Intersect(oldItems, new OrderItemIdComparer()).ToList().ForEach(x => db.Entry(x).State = EntityState.Modified);

                newItems.Where(x => x.ID == 0).ToList().ForEach(x =>
                    {
                        x.Dish = null;
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            x.BranchId = BranchRepository.SelectedBranchId;
                        }
                        x.DateModified = DateTime.Now;
                        x.AuthorModified = UserManager.UserLogged.Name;
                        db.OrderItems.Add(x);
                    });
                oldItems.Except(newItems, new OrderItemIdComparer()).ToList().ForEach(x => db.OrderItems.Remove(db.OrderItems.First(z => x.ID == z.ID)));

                db.SaveChanges();
            }
        }

        public void ChangePaymentType(int orderId, PaymentMethod method)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var order = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == orderId);
                if (order == null) throw new ArgumentException("Заказ с таким ID не найден");
                if (order.PaymentMethod == method)
                    return;
                order.DateModified = DateTime.Now;
                order.AuthorModified = UserManager.UserLogged.Name;
                order.PaymentMethod = method;
                order.IsOnlinePayment = method == PaymentMethod.OfflineCard || method == PaymentMethod.OnlineCard;

                db.SaveChanges();
            }
        }

        public void ChangeSmsSendedStatus(int orderId, bool status)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var order = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == orderId);
                if (order == null)
                {
                    throw new ArgumentException("Заказ с таким ID не найден");
                }
                order.DateModified = DateTime.Now;
                order.AuthorModified = UserManager.UserLogged.Name;
                order.SmsSended = status;
                db.SaveChanges();
            }
        }

        public void ChangePrintToNalogPrinterStatus(int orderId, bool status)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var order = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == orderId);
                if (order == null)
                {
                    throw new ArgumentException("Заказ с таким ID не найден");
                }
                order.DateModified = DateTime.Now;
                order.AuthorModified = UserManager.UserLogged.Name;
                order.FiscalCheckPrinted = status;
                db.SaveChanges();
            }
        }

        public string ValidateOrder(Order order)
        {
            var result = string.Empty;


            result += (string.IsNullOrEmpty(order.Name)) ? "Заполните поле 'Имя'\n" : string.Empty;
            result += (string.IsNullOrEmpty(order.Phone)) ? "Заполните поле 'Телефон'\n" : string.Empty;

            if (!order.IsWindow)
            {
                result += (string.IsNullOrEmpty(order.Street)) ? "Заполните поле 'Улица'\n" : string.Empty;
                result += (string.IsNullOrEmpty(order.House)) ? "Заполните поле 'Дом'\n" : string.Empty;
                result += (string.IsNullOrEmpty(order.Appartament)) ? "Значение в поле 'Квартира' должно быть указано числом\n" : string.Empty;
                result += (!string.IsNullOrEmpty(order.Porch) && !int.TryParse(order.Porch, out _)) ? "Значение в поле 'Подъезд' должно быть указано числом\n" : string.Empty;
            }

            return result;
        }

        public List<OrderItem> GetItemList(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.OrderItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.OrderID == id).IncludeAll().ToList();
            }
        }

        public OrderItem GetOrderItemByDishId(int dishID)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.OrderItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.DishID == dishID).FirstOrDefault();
            }
        }

        public void Add(Order order, List<OrderItem> items)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                order.DiscountCard = null;
                order.Code = new CodeGenerator(this).GenerateCode(BranchRepository.SelectedBranchId);
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    order.BranchId = BranchRepository.SelectedBranchId;
                }
                order.DateModified = DateTime.Now;
                order.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(order.Brand).State = EntityState.Unchanged;
                db.Orders.Add(order);
                db.SaveChanges();

                foreach (var x in items)
                {
                    x.OrderID = order.ID;
                    AddItem(x);
                }
            }
        }

        public List<Order> GetListForDriver(int driverID, int sessionID)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var query = db.Orders
                    .Include(x => x.State)
                    .Include(x => x.Client)
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => x.DriverID == driverID);

                if (sessionID == 0)
                    query = query.Where(x => x.State.Name == "В пути" || x.State.Name == "Доставлен");
                else
                    query = query.Where(x => x.SessionID == sessionID && x.State.Name != "В пути" && x.State.Name != "Доставлен");

                return query.ToList();
            }
        }

        public List<Session> GetAllExistedSessionsForDriver(int driverID)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var sessions = db.Sessions
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => x.Orders.Any(y => y.DriverID == driverID))
                    .ToList();               

                return sessions.OrderByDescending(x => x.ID).ToList();
            }
        }

        public void SetOrderStateToDelivered(int orderId)
        {
            var fromState = StateManager.GetByName("В пути");
            var toState = StateManager.GetByName("Доставлен");
            var order = GetById(orderId, false);
            if (order.StateID != fromState.ID)
                return;
            order.ChangeToDelivered = DateTime.Now;
            order.StateID = toState.ID;
            Update(order);
        }

        public List<Order> GetListOfAllOrdersInSession(int sessionId)
        {
            return GetAllOrdersInSession(sessionId).ToList();
        }

        public List<Order> GetAllOrdersInSession(int sessionId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Orders.IncludeAll().Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SessionID == sessionId).ToList();
            }
        }

        public List<Order> GetSessionInfo(int sessionId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Orders.Include(x => x.State).Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SessionID == sessionId).ToList();
            }
        }

        private void AddItem(OrderItem item)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    item.Dish.BranchId = BranchRepository.SelectedBranchId;
                    item.BranchId = BranchRepository.SelectedBranchId;
                }
                item.DateModified = DateTime.Now;
                item.AuthorModified = UserManager.UserLogged.Name;
                item.Dish.DateModified = DateTime.Now;
                item.Dish.AuthorModified = UserManager.UserLogged.Name;
                db.Dishes.Attach(item.Dish);
                db.OrderItems.Add(item);
                db.SaveChanges();
            }
        }

        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Возвраащет массив блюд (по цехам (null - все блюда)) и их количества (OrderItem) в заказе </summary>
        /// <param name="order">Заказ, для которого вычисляется массив</param>
        /// <param name="section">Цех, к которому принадлежат возвращаемые блюда</param>
        /// <returns>Массив OrderItem</returns>
        ////==============================================================================================================
        public OrderItem[] GetOrderItemsArray(Order order, Section section = null)
        {
            var buffer = new List<OrderItem>();

            if (section == null)
            {
                foreach (OrderItem item in order.OrderItems)
                {
                    using (var db = PizzaAppDB.GetNewContext())
                    {
                        OrderItem itm = item;
                        var foundItem = db.OrderItems.Where(x => x.ID == itm.ID).Include(x => x.Dish).FirstOrDefault();
                        if (foundItem != null)
                        {
                            buffer.Add(foundItem);
                        }
                    }
                }
            }
            else
            {
                foreach (OrderItem item in order.OrderItems)
                {
                    using (var db = PizzaAppDB.GetNewContext())
                    {
                        OrderItem itm = item;
                        var foundItem = db.OrderItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ID == itm.ID).Include("Dish").Include("Dish.Category").FirstOrDefault();
                        if (foundItem != null && foundItem.Dish.Category.SectionID == section.ID)
                        {
                            buffer.Add(foundItem);
                        }
                    }
                }
            }

            var items = new OrderItem[buffer.Count];
            buffer.CopyTo(items, 0);

            return items;
        }

        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary>  Возвраащет массив блюд по заданному цеху и их количества (OrderItem) в заказе </summary>
        /// <param name="cafeOrderDocumentItemsList">Текущий список заказов </param>
        /// <param name="section">Цех, к которому принадлежат возвращаемые блюда</param>
        /// <returns>Массив OrderItem</returns>
        ////====================================================================================================
        public List<OrderItem> GetOrderItemsArrayByKitchen(List<CafeOrderDocumentItem> cafeOrderDocumentItemsList, Section section)
        {

            var orderItems = new List<OrderItem>();

            using (var db = PizzaAppDB.GetNewContext())
            {
                foreach (var cafeOrderDocumentItem in cafeOrderDocumentItemsList)
                {
                    // Если позиция заказа "свежая"
                    if (cafeOrderDocumentItem.IsEditable)
                    {
                        int cafeOrderDishId = cafeOrderDocumentItem.Dish.ID;

                        // Перезагрузка позиций блюда, ибо изначально в нём не все поля загружены
                        Dish foundDish = db.Dishes.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.ID == cafeOrderDishId).Include("Category").FirstOrDefault();

                        // Если элемент загружен и цех кухни соответствует заданному
                        if (foundDish != null && foundDish.Category.SectionID == section.ID)
                        {
                            // Создать элемент заказа, для дальнейшей совместимости
                            var orderItem = new OrderItem
                            {
                                Amount = cafeOrderDocumentItem.Amount,
                                Dish = foundDish,
                                DishID = foundDish.ID,
                                IsEditable = cafeOrderDocumentItem.IsEditable
                            };

                            orderItems.Add(orderItem);
                        }
                    }
                }
            }

            return orderItems;
        }

        public async Task<int> GetDeliveryPrice(ICollection<OrderItem> items, int brandId, DiscountCard card = null, Coupon coupon = null)
        {
            int deliveryPrice = 0;

            if (items == null) items = new Collection<OrderItem>();
            decimal sum = items.Where(x => x.ID != -1 && x.Dish.DoCountWhenDeliveryCostCalculated).Sum(x => x.Amount * x.Dish.Price);
            if (card != null)
            {
                sum = sum * GetDiscountRate(card.Discount);
            }
            else if (coupon != null)
            {
                sum = await ApplyCoupon(sum, coupon, MapOrderItemsToDishes(items));
            }
            if (sum < GlobalOptionsManager.GetFreeDeliveryFrom(brandId))
            {
                deliveryPrice = GlobalOptionsManager.GetDeliveryPrice(brandId);
            }

            return deliveryPrice;
        }

        public decimal ApplyCouponSync(decimal totalCost, Coupon coupon, IEnumerable<ICouponDish> ordeItems)
        {
            var result = totalCost;
            var rule = coupon?.CouponRule;
            if (rule != null)
            {
                var ruleElements = CouponRuleElementRepository.GetByCouponRuleIdSync(rule.ID);                
                var couponDiscount = GetCouponDiscountForCurrentTime(totalCost, rule.ID);
                var ruleElementsRequired = ruleElements.Where(x =>
                    x.ElementApplyDirection == ElementApplyDirection.ElementNeededToApplyRule);
                var ruleElementsToApply = ruleElements.Where(x =>
                    x.ElementApplyDirection == ElementApplyDirection.RuleAppliedToElement);
                if (CheckRequiredDishes(ruleElementsRequired, ordeItems, rule.AllDishRequired, rule.GiftDish))
                {
                    result = ApplyRule(totalCost, rule, ruleElementsToApply, couponDiscount, ordeItems);
                }
            }

            return result;
        }

        private decimal ApplyRule(decimal totalCost, CouponRule rule,
            IEnumerable<CouponRuleElement> ruleElementsToApply, CouponDiscount couponDiscount, IEnumerable<ICouponDish> ordeItems)
        {
            decimal result = totalCost;
            if (rule.SumToApply != null && totalCost < rule.SumToApply)
                return result;

            var couponRuleElements = ruleElementsToApply.ToList();
            if (couponRuleElements.Count == 0)
            {
                if (rule.GiftDish != null)
                    return result;
                
                result = GetSum(result, rule, couponDiscount);
            }
            else
            {
                foreach (var ruleElement in couponRuleElements)
                {
                    if (ruleElement.ElementContext == CouponRuleElementEnum.Dish)
                    {
                        var dishesFromOrder = ordeItems.Where(x => x.ID == ruleElement.ElementId).ToList();
                        if (dishesFromOrder.Any())
                        {
                            if (rule.ApplyToAllDishes && rule.GiftDish == null)
                            {
                                foreach (var dishFromOrder in dishesFromOrder)
                                {
                                    result = result - dishFromOrder.Price + GetSum(dishFromOrder.Price, rule, couponDiscount);
                                }
                            }
                            else
                            {
                                var dishFromOrder = dishesFromOrder.OrderBy(x => x.Price).FirstOrDefault();
                                if (dishFromOrder != null)
                                    result = result - dishFromOrder.Price + GetSum(dishFromOrder.Price, rule, couponDiscount);
                            }
                        }
                    }
                    else if (ruleElement.ElementContext == CouponRuleElementEnum.Category)
                    {
                        var dishesFromOrder = ordeItems.Where(x => x.CategoryID == ruleElement.ElementId).ToList();
                        if (dishesFromOrder.Any())
                        {
                            if (rule.ApplyToAllDishes)
                            {
                                foreach (var dishFromOrder in dishesFromOrder)
                                {
                                    result = result - dishFromOrder.Price + GetSum(dishFromOrder.Price, rule, couponDiscount);
                                }
                            }
                            else
                            {
                                var dishFromOrder = dishesFromOrder.OrderBy(x => x.Price).FirstOrDefault();
                                if (dishFromOrder != null)
                                    result = result - dishFromOrder.Price + GetSum(dishFromOrder.Price, rule, couponDiscount);
                            }
                        }
                    }
                }
            }

            return result;
        }
        private DaysOfWeekEnum GetCurrentDay()
        {
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return DaysOfWeekEnum.Monday;
                case DayOfWeek.Tuesday:
                    return DaysOfWeekEnum.Tuesday;
                case DayOfWeek.Wednesday:
                    return DaysOfWeekEnum.Wednesday;
                case DayOfWeek.Thursday:
                    return DaysOfWeekEnum.Thursday;
                case DayOfWeek.Friday:
                    return DaysOfWeekEnum.Friday;
                case DayOfWeek.Saturday:
                    return DaysOfWeekEnum.Saturday;
                case DayOfWeek.Sunday:
                    return DaysOfWeekEnum.Sunday;
                default:
                    return DaysOfWeekEnum.All;
            }
        }
        public CouponDiscount GetCouponDiscountForCurrentTime(decimal totalCost, int couponRuleId)
        {
            var currentDay = GetCurrentDay();
            var currentTime = DateTime.Now.TimeOfDay;
            var couponDiscounts = CouponDiscountRepository.GetCouponDiscountsByCouponRuleId(couponRuleId);
            var couponDiscount = couponDiscounts.Where(
                x => x.DaysOfWeek.HasFlag(currentDay)
                    && totalCost >= x.SumToApply 
                    && currentTime <= x.TimeEnd   
                    && currentTime >= x.TimeBegin  
            )
            .OrderByDescending(x => x.SumToApply)
            .FirstOrDefault();
            return couponDiscount;
        }

        private decimal GetSum(decimal result, CouponRule rule, CouponDiscount couponDiscount)
        {
            if (rule.FixDiscount != null)
            {
                result = (decimal)(result - rule.FixDiscount);
            }
            else if (rule.Percent != null)
            {
                int discountInPercent = rule.Percent ?? 0;
                if (couponDiscount != null)
                    discountInPercent = couponDiscount.DiscountInPercent;
                var discount = result / 100 * discountInPercent;
                result = result - discount;

            }
            else if (rule.Price != null)
            {
                result = (decimal) rule.Price;
            }
            else if (rule.GiftDish != null)
            {
                result = 0;
            }

            return result;
        }

        private bool CheckRequiredDishes(IEnumerable<CouponRuleElement> ruleElementsRequired, IEnumerable<ICouponDish> ordeItems, bool ruleAllDishRequired, int? ruleCountDishRequired)
        {
            var existElements = 0;
            var requiredElements = 0;
            var couponRuleElements = ruleElementsRequired.ToList();
            if (couponRuleElements.Count > 0)
            {
                foreach (var ruleElement in couponRuleElements)
                {
                    if (ruleElement.ElementContext == CouponRuleElementEnum.Dish)
                    {
                        int findedElements = ordeItems.Count(x => x.ID == ruleElement.ElementId);
                        if (findedElements > 0)
                        {
                            existElements++;
                            requiredElements += findedElements;
                        }

                    }
                    else if (ruleElement.ElementContext == CouponRuleElementEnum.Category)
                    {
                        int findedElements = ordeItems.Count(x => x.CategoryID == ruleElement.ElementId);
                        if (findedElements > 0)
                        {
                            existElements++;
                            requiredElements += findedElements;
                        }
                    }
                }

                if (ruleAllDishRequired)
                {
                    if (existElements != couponRuleElements.Count)
                    {
                        return false;
                    }
                    if (ruleCountDishRequired != null && requiredElements < ruleCountDishRequired)
                    {
                        return false;
                    }
                }
                else if (existElements == 0)
                {
                    return false;
                }
            }

            return true;
        }
        public string RuleToString(CouponRule couponRule)
        {
            var result = $"Купон: {couponRule.Name}, действует с {couponRule.From} по {couponRule.To}";
            if (couponRule.Price != null)
            {
                result += $"; Фиксированная цена - {couponRule.Price} рублей";

            }
            else if (couponRule.Percent != null)
            {
                result += $"; Скидка в процентах - {couponRule.Percent}%";
            }
            else if (couponRule.FixDiscount != null)
            {
                result += $"; Фиксированная скидка - {couponRule.FixDiscount} рублей"; result += $"; Скидка в процентах - {couponRule.Percent}%";
            }

            if (couponRule.SumToApply != null)
            {
                result += $" при заказе от {couponRule.SumToApply} рублей";
            }
            if (couponRule.Percent != null)
            {
                var couponDiscounts = CouponDiscountRepository.GetCouponDiscountsByCouponRuleId(couponRule.ID);

                foreach (var couponDiscount in couponDiscounts)
                    result += $"; Скидка в процентах - {couponDiscount.DiscountInPercent}% при заказе от {couponDiscount.SumToApply} рублей действут с {couponDiscount.TimeBegin.Hours}:{couponDiscount.TimeBegin.Minutes} по {couponDiscount.TimeEnd.Hours}:{couponDiscount.TimeEnd.Minutes}";
            }
            if (couponRule.AllDishRequired)
            {
                result += $"; Для применения купона требуется, чтобы все блюда были в заказе.";
            }

            var rulesElements = CouponRuleElementRepository.GetByCouponRuleIdSync(couponRule.ID);
            var directions = rulesElements.GroupBy(x => x.ElementApplyDirection);
            foreach (var rulesElement in directions)
            {
                if (rulesElement.Key == ElementApplyDirection.ElementNeededToApplyRule)
                {
                    result += $"; Блюда и/или категории при которых применяется скидка: ";
                    foreach (var element in rulesElement)
                    {
                        if (element.ElementContext == CouponRuleElementEnum.Category)
                        {
                            var category = CategoryRepository.GetItem(element.ElementId);
                            if (category == null)
                                continue;
                            result += $"Категория: {category.Name}; ";
                        }
                        else
                        {
                            var dish = DishRepository.GetItem(element.ElementId);
                            if (dish == null)
                                continue;
                            result += $"Блюдо: {dish.Name}; ";
                        }
                    }
                }
                else if (rulesElement.Key == ElementApplyDirection.RuleAppliedToElement)
                {
                    result += $"; Блюда и/или категории к которым применяется скидка: ";
                    foreach (var element in rulesElement)
                    {
                        if (element.ElementContext == CouponRuleElementEnum.Category)
                        {
                            var category = CategoryRepository.GetItem(element.ElementId);
                            if (category == null)
                                continue;
                            result += $"Категория: {category.Name}; ";
                        }
                        else
                        {
                            var dish = DishRepository.GetItem(element.ElementId);
                            if (dish == null)
                                continue;
                            result += $"Блюдо: {dish.Name}; ";
                        }
                    }
                }
            }
            return result;
        }

        public IEnumerable<ICouponDish> MapOrderItemsToDishes(IEnumerable<OrderItem> orderItems)
        {
            var dishes = new List<Dish>();
            foreach(var orderItem in orderItems) 
            {
                int itemAmount = orderItem.Amount;
                while(itemAmount-- > 0)
                {
                    dishes.Add(orderItem.Dish);
                }
            }
            return dishes;
        }

        public async Task<decimal> ApplyCoupon(decimal totalCost, Coupon coupon, IEnumerable<ICouponDish> ordeItems)
        {
            var result = totalCost;
            var rule = coupon?.CouponRule;
            if (rule != null)
            {
                var ruleElements = await CouponRuleElementRepository.GetByCouponRuleId(rule.ID);
                var couponDiscount = GetCouponDiscountForCurrentTime(totalCost, rule.ID);
                var ruleElementsRequired = ruleElements.Where(x =>
                    x.ElementApplyDirection == ElementApplyDirection.ElementNeededToApplyRule);
                var ruleElementsToApply = ruleElements.Where(x =>
                    x.ElementApplyDirection == ElementApplyDirection.RuleAppliedToElement);
                if (CheckRequiredDishes(ruleElementsRequired, ordeItems, rule.AllDishRequired, rule.GiftDish))
                {
                    result = ApplyRule(totalCost, rule, ruleElementsToApply, couponDiscount, ordeItems);
                }
            }

            return result;
        }

        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Получить коэффициент скидки </summary>
        ////================================================
        public decimal GetDiscountRate(int discount)
        {
            var result = (100 - Convert.ToDecimal(discount)) / 100;
            return result;
        }

        public Order GetById(int id, bool includeAll = true)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var query = db.Orders
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => x.ID == id);
                if (includeAll)
                    return query.IncludeAll().FirstOrDefault();
                else
                    return query.FirstOrDefault();
            }
        }


        public void Remove(int id)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var order = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == id);
                if (order != null)
                {
                    db.Orders.Remove(order);
                    db.SaveChanges();
                    TimeSheetManager.ClearReservation(order.ReservationGuid);
                }

            }
        }

        public void SetNewDriver(int orderId, Driver driver)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var order = db.Orders.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == orderId);
                if (order != null)
                {
                    order.DateModified = DateTime.Now;
                    order.AuthorModified = UserManager.UserLogged.Name;
                    order.DriverID = driver.ID;
                    db.SaveChanges();
                }
            }
        }

        public async Task<List<DishFavorite>> GetDisheByFavorite(List<Order> lastOrders)
        {
            List<DishFavorite> dishFavorites = new List<DishFavorite>();

            foreach (Order order in lastOrders)
            {
                using (var db = PizzaAppDB.GetNewContext())
                {
                    List<OrderItem> orderItems = await db.OrderItems.Include(x => x.Dish).Where(x => x.OrderID == order.ID && x.BranchId == BranchRepository.SelectedBranchId).ToListAsync();

                    foreach (OrderItem orderItem in orderItems)
                    {
                        bool isAdd = false;

                        foreach (DishFavorite dishFavorite in dishFavorites)
                        {
                            if (dishFavorite.Dish.Name == orderItem.Dish.Name)
                            {
                                dishFavorite.Count += orderItem.Amount;

                                isAdd = true;

                                break;
                            }
                        }

                        if (!isAdd && orderItem.Dish.Price > 0 && !orderItem.Dish.IsDeleted)
                        {
                            dishFavorites.Add(new DishFavorite { Dish = orderItem.Dish, Count = orderItem.Amount });
                        }
                    }
                }
            }

            return dishFavorites.OrderByDescending(x => x.Count).ToList();
        }

        public List<Order> GetFailureOrders(DateTime dateFrom, DateTime dateTo)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var failureState = StateManager.GetByName("Отказ");
                var orders = db.Orders
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => x.StateID == failureState.ID)
                    .Where(x => x.Created >= dateFrom && x.Created <= dateTo)
                    .ToList();
                return orders;
            }
        }


        public bool DoCodeAlreadyExists(string uid, int selectedBranchId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.Orders.Where(x => x.BranchId == selectedBranchId).Any(x => x.Code == uid);
            }
        }
    }
}
