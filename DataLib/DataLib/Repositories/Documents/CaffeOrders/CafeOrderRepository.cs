﻿
using System;
using System.Collections.Generic;

using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataLib.Models;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Documents.CaffeOrders
{
    public class CafeOrderDocumentRepository: ICafeOrderDocumentRepository
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public ISessionManager SessionManager { get; set; }
        public List<CafeOrderDocument> GetList(int? sessionId, bool? isMarkToDelete, PaymentType? paymentType)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var result = db.CaffeOrderDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId);
                if (sessionId != null)
                    result = result.Where(x => x.SessionID == sessionId);
                if (isMarkToDelete != null)
                    result = result.Where(x => x.IsMarkToDelete == isMarkToDelete);
                if (paymentType != null)
                    result = result.Where(x => x.PaymentType == paymentType);
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    return result.Where(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0).ToList();
                }
                return result.ToList();
            }
        }

        public int Count(int? sessionId, bool? isMarkToDelete)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    var result = db.CaffeOrderDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId);
                    if (sessionId != null)
                        result = result.Where(x => x.SessionID == sessionId);
                    if (isMarkToDelete != null)
                        result = result.Where(x => x.IsMarkToDelete == isMarkToDelete);
                    return result
                        .Count(x => x.BranchId == BranchRepository.SelectedBranchId || x.BranchId == 0);
                } 
                else
                {
                    var result = db.CaffeOrderDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId);//.Count(predicate);
                    if (sessionId != null)
                        result = result.Where(x => x.SessionID == sessionId);
                    if (isMarkToDelete != null)
                        result = result.Where(x => x.IsMarkToDelete == isMarkToDelete);
                    return result.Count();
                }
            }
        }

        public IEnumerable<CafeOrderDocument> GetItems(DateTime start, DateTime end)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.CaffeOrderDocuments
                    .Include(document => document.Table)
                    .Include(document => document.CafeOrderItems)
                    .Include(document => document.CafeOrderItems.Select(y => y.Dish))
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                    .Where(x => x.Created >= start && x.Created <= end)
                    .ToList();
            }
        }

        public IEnumerable<CafeOrderDocument> GetItems(FilterModel filter)
        {
            var value = (CafeOrderDocumentFilter)filter;

            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.CaffeOrderDocuments.Where(x => !x.IsMarkToDelete && x.SessionID == value.SessionId)
                      .Include(document => document.Table).Where(x => x.BranchId == BranchRepository.SelectedBranchId)
                      .ToList();
            }
        }

        public CafeOrderDocument GetItem(FilterModel filter)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var value = (CafeOrderDocumentFilter)filter;
                CafeOrderDocument document = null;

                var query = db.CaffeOrderDocuments.Include("Table").Include("DiscountCard").Where(x => x.BranchId == BranchRepository.SelectedBranchId);

                if (value.IsPositionFilter)
                {
                    document = query.FirstOrDefault(x => !x.IsProcessed && !x.IsMarkToDelete && x.TableID == value.CurrentTable.ID && x.PlaceIndex == value.Position);

                    if (document != null)
                    {
                        document.CafeOrderItems = db.CafeOrderDocumentItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.CafeOrderDocumentID == document.ID).Include("Dish").ToList();
                    }
                }
                else
                {

                    if (value.CurrentTable != null && value.CurrentTable.Name == "Возьми с собой")
                    {
                        value.IsTableFilter = false;
                        value.IsIDFilter = true;
                    }

                    if (value.IsTableFilter)
                    {
                        document = query.FirstOrDefault(x => !x.IsProcessed && !x.IsMarkToDelete && x.TableID == value.CurrentTable.ID);
                    }

                    if (value.IsIDFilter)
                    {
                        document = query.FirstOrDefault(x => x.ID == value.DocumentID);
                    }

                    if (document == null)
                        return null;

                    document.CafeOrderItems = db.CafeOrderDocumentItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.CafeOrderDocumentID == document.ID).Include("Dish").ToList();
                }

                return document;
            }
        }


        public async Task InsertItemAsync(CafeOrderDocument entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var session = await SessionManager.GetOpened();

                if (session == null)
                {
                    UserMessage.Show("Не найдена открытая сессия создание документа невозможно!");
                    return;
                }


                var code = db.CaffeOrderDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Count(x => x.SessionID == session.ID) + 1;

                entity.SessionID = session.ID;
                entity.Code = code.ToString(CultureInfo.InvariantCulture);

                var doc = new CafeOrderDocument
                {
                    Code = entity.Code,
                    Comment = entity.Comment,
                    Created = entity.Created,
                    DocumentSum = entity.DocumentSum,
                    SessionID = entity.SessionID,
                    TableID = entity.TableID == -1 ? null : entity.TableID,
                    User = UserManager.UserLogged?.Name,
                    IsMarkToDelete = entity.IsMarkToDelete,
                    IsProcessed = entity.IsProcessed,
                    DiscountCardID = entity.DiscountCardID,
                    IsOnlinePayment = entity.IsOnlinePayment,
                    PersonsCount = entity.PersonsCount,
                    OnlinePayCardName = entity.OnlinePayCardName,
                    PlaceIndex = entity.PlaceIndex,
                    PaymentType = entity.PaymentType,
                    PointOfSaleId = entity.PointOfSaleId
                };
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    doc.BranchId = BranchRepository.SelectedBranchId;
                }
                doc.DateModified = DateTime.Now;
                doc.AuthorModified = UserManager.UserLogged.Name;
                db.CaffeOrderDocuments.Add(doc);

                db.SaveChanges();
                entity.ID = doc.ID;

                foreach (var item in entity.CafeOrderItems.ToList())
                {
                    if (item.IsNew)
                    {
                        var documentItem = new CafeOrderDocumentItem
                        {
                            Amount = item.Amount,
                            CafeOrderDocumentID = doc.ID,
                            DishID = item.DishID,
                            Price = item.Price,
                            Sum = item.Sum
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            documentItem.BranchId = BranchRepository.SelectedBranchId;
                        }
                        documentItem.DateModified = DateTime.Now;
                        documentItem.AuthorModified = UserManager.UserLogged.Name;
                        db.CafeOrderDocumentItems.Add(documentItem);
                        db.SaveChanges();
                        item.ID = documentItem.ID;
                    }
                }
            }
        }

        public void UpdateItem(CafeOrderDocument entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.CaffeOrderDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == entity.ID);

                if (doc != null)
                {
                    doc.Code = entity.Code;
                    doc.Comment = entity.Comment;
                    doc.Created = entity.Created;
                    doc.DocumentSum = entity.DocumentSum;
                    doc.SessionID = entity.SessionID;
                    doc.TableID = entity.TableID;
                    doc.IsMarkToDelete = entity.IsMarkToDelete;
                    doc.IsProcessed = entity.IsProcessed;
                    doc.DiscountCardID = entity.DiscountCardID;
                    doc.IsOnlinePayment = entity.IsOnlinePayment;
                    doc.OnlinePayCardName = entity.OnlinePayCardName;
                    doc.PersonsCount = entity.PersonsCount;
                    doc.PlaceIndex = entity.PlaceIndex;
                    doc.PaymentType = entity.PaymentType;
                    if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                    {
                        doc.BranchId = BranchRepository.SelectedBranchId;
                    }
                    doc.DateModified = DateTime.Now;
                    doc.AuthorModified = UserManager.UserLogged.Name;
                    db.Entry(doc).State = EntityState.Modified;
                }

                var existsItem = db.CafeOrderDocumentItems.Where(x => x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.CafeOrderDocumentID == entity.ID).ToList();

                foreach (var cafeOrderItem in entity.CafeOrderItems)
                {
                    if (cafeOrderItem.IsNew)
                    {
                        var cafeItem = new CafeOrderDocumentItem
                        {
                            Amount = cafeOrderItem.Amount,
                            CafeOrderDocumentID = entity.ID,
                            DishID = cafeOrderItem.DishID,
                            Price = cafeOrderItem.Price,
                            Sum = cafeOrderItem.Sum,
                            IsEditable = false
                        };
                        if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                        {
                            cafeItem.BranchId = BranchRepository.SelectedBranchId;
                        }
                        cafeItem.DateModified = DateTime.Now;
                        cafeItem.AuthorModified = UserManager.UserLogged.Name;
                        db.CafeOrderDocumentItems.Add(cafeItem);
                        db.SaveChanges();
                        cafeOrderItem.ID = cafeItem.ID;
                    }
                    else
                    {
                        if (existsItem.Any(x => x.ID == cafeOrderItem.ID))
                        {
                            var item = existsItem.FirstOrDefault(x => x.ID == cafeOrderItem.ID);

                            if (item != null && cafeOrderItem.IsEdit)
                            {
                                item.Amount = cafeOrderItem.Amount;
                                item.Sum = cafeOrderItem.Sum;
                                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                                {
                                    item.BranchId = BranchRepository.SelectedBranchId;
                                }
                                db.Entry(item).State = EntityState.Modified;

                                existsItem.Remove(item);
                            }
                        }
                    }
                }

                foreach (var cafeOrderItem in existsItem)
                {
                    db.CafeOrderDocumentItems.Remove(cafeOrderItem);
                }

                db.SaveChanges();
            }
        }

        public void DeleteItem(CafeOrderDocument entity)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var doc = db.CaffeOrderDocuments.Where(x => x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == entity.ID);

                if (doc != null)
                {
                    doc.IsMarkToDelete = !doc.IsMarkToDelete;
                    doc.IsProcessed = false;

                    db.Entry(doc).State = EntityState.Modified;

                    db.SaveChanges();
                }
            }
        }
    }
}
