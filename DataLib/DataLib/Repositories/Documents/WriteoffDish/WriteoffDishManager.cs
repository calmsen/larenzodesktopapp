﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using DataLib.Models;
using System.Linq;
using LaRenzo.DataRepository.Entities.Documents.WriteoffDish;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Inventory;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Documents.WriteoffDish
{
    public class WriteoffDishManager: IWriteoffDishManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IInventoryManager InventoryManager { get; set; }
        public List<WriteoffDishDocument> GetWriteoffDishDocuments()
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.WriteoffDishDocuments.Include("Warehouse").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).OrderBy(x => x.ID).ToList();
            }
        }

        public WriteoffDishDocument GetWriteoffDishDocument(int writeoffDishId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.WriteoffDishDocuments.Include("Warehouse").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == writeoffDishId);
            }
        }

        public void DeleteWriteoffDocument(WriteoffDishDocument document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (!document.IsMarkToDelete)
                {
                    document.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.WriteoffDishDocumentID == document.ID).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                document.IsMarkToDelete = !document.IsMarkToDelete;

                db.Entry(document).State = EntityState.Modified;

                db.SaveChanges();

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, document.DocumentDate);

            }
        }

        public List<WriteoffDishDocumentItem> GetWriteoffDocumentItems(int writeoffDishDocumentId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return
                    db.WriteoffDishDocumentItems.Include("Dish")
                      .Where(x=>x.BranchId == BranchRepository.SelectedBranchId)
                      .Where(x => x.WriteoffDishDocumentID == writeoffDishDocumentId)
                      .ToList();
            }
        }

        public void UpdateWriteoffDishDocument(WriteoffDishData writeoffDishData)
        {
            WriteoffDishDocument document = writeoffDishData.Document;
            List<WriteoffDishDocumentItem> documentItems = writeoffDishData.DocumentItems;
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    document.BranchId = BranchRepository.SelectedBranchId;
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(document).State = EntityState.Modified;
                var existsItem = db.WriteoffDishDocumentItems.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.WriteoffDishDocumentID == document.ID).ToList();

                var oldDocument = db.WriteoffDishDocuments.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == document.ID);

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, new DocumentWithDateHelper().GetMinDate(document, oldDocument));

                foreach (var documentItem in documentItems)
                {
                    if (documentItem.ID == -1)
                    {
                        var docItem = new WriteoffDishDocumentItem
                        {
                            Amount = documentItem.Amount,
                            Sum = documentItem.Sum,
                            Price = documentItem.Price,
                            DishID = documentItem.DishID,
                            WriteoffDishDocumentID = document.ID
                        };
                        if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                        {
                            docItem.BranchId = BranchRepository.SelectedBranchId;
                        }
                        docItem.DateModified = DateTime.Now;
                        docItem.AuthorModified = UserManager.UserLogged.Name;
                        db.WriteoffDishDocumentItems.Add(docItem);
                    }
                    else
                    {
                        if (existsItem.Any(x => x.ID == documentItem.ID))
                        {
                            var item = existsItem.FirstOrDefault(x => x.ID == documentItem.ID);
                            if (item != null)
                            {
                                item.Amount = documentItem.Amount;
                                item.Price = documentItem.Price;
                                item.Sum = documentItem.Sum;
                                existsItem.Remove(item);
                            }
                        }
                    }
                }

                foreach (var documentItem in existsItem)
                {
                    db.WriteoffDishDocumentItems.Remove(documentItem);
                }

                db.SaveChanges();
            }
        }

        public int AddWriteoffDishDocument(WriteoffDishDocument document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    document.BranchId = BranchRepository.SelectedBranchId;
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                db.WriteoffDishDocuments.Add(document);

                db.SaveChanges();

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, document.DocumentDate);

                return document.ID;
            }
        }
    }
}