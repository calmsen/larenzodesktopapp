﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using DataLib.Models;
using LaRenzo.DataRepository.Entities.Documents.Supply;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Inventory;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Documents.Supply
{
    public class SupplyManager: ISupplyManager
    {
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IBranchRepository BranchRepository { get; set; }
        [Inject]
        public IInventoryManager InventoryManager { get; set; }

        public List<SupplyDocument> GetSupplyDocuments(int countOfRows = 100, bool notPaid = false)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                var query = db.SupplyDocuments
                    .Include("Partner")
                    .Include("Warehouse")
                    .Where(x => x.BranchId == BranchRepository.SelectedBranchId);
                if (notPaid)
                    query = query.Where(x => x.PaySum == 0);
                return query
                    .OrderByDescending(x => x.ID)
                    .Take(countOfRows)
                    .ToList();
            }
        }

        public SupplyDocument GetSupplyDocument(int supplyDocId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.SupplyDocuments.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == supplyDocId);
            }
        }

        public void UpdateSupplyDocument(SupplyData supplyData)
        {
            SupplyDocument document = supplyData.Document;
            List<SupplyDocumentItem> documentItems = supplyData.DocumentItems;
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId != (int)BranchIdEnum.AllBranchId)
                {
                    document.BranchId = BranchRepository.SelectedBranchId;
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                db.Entry(document).State = EntityState.Modified;

                var existsItem = db.SupplyDocumentItems.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SupplyDocumentID == document.ID).ToList();
                var oldDocument = db.SupplyDocuments.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).FirstOrDefault(x => x.ID == document.ID);

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, new DocumentWithDateHelper().GetMinDate(document, oldDocument));

                foreach (var documentItem in documentItems)
                {
                    if (documentItem.ID == -1)
                    {
                        var docItem = new SupplyDocumentItem
                        {
                            Amount = documentItem.Amount,
                            Sum = documentItem.Sum,
                            Price = documentItem.Price,
                            ProductID = documentItem.ProductID,
                            SupplyDocumentID = document.ID
                        };
                        if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                        {
                            docItem.BranchId = BranchRepository.SelectedBranchId;
                        }
                        docItem.DateModified = DateTime.Now;
                        docItem.AuthorModified = UserManager.UserLogged.Name;
                        db.SupplyDocumentItems.Add(docItem);
                    }
                    else
                    {
                        if (existsItem.Any(x => x.ID == documentItem.ID))
                        {
                            var item = existsItem.FirstOrDefault(x => x.ID == documentItem.ID);
                            if (item != null)
                            {
                                item.Amount = documentItem.Amount;
                                item.Price = documentItem.Price;
                                item.Sum = documentItem.Sum;
                                existsItem.Remove(item);
                            }
                        }
                    }
                }

                foreach (var documentItem in existsItem)
                {
                    db.SupplyDocumentItems.Remove(documentItem);
                }

                db.SaveChanges();
            }
        }

        private DateTime GetMinDate(SupplyDocument document, SupplyDocument oldDocument)
        {
            return oldDocument == null
                ? document.DocumentDate
                : document.DocumentDate < oldDocument.DocumentDate
                    ? document.DocumentDate
                    : oldDocument.DocumentDate;
        }

        public int AddSupplyDocument(SupplyDocument document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (BranchRepository.SelectedBranchId  != (int)BranchIdEnum.AllBranchId)
                {
                    document.BranchId = BranchRepository.SelectedBranchId;
                }
                document.DateModified = DateTime.Now;
                document.AuthorModified = UserManager.UserLogged.Name;
                db.SupplyDocuments.Add(document);

                db.SaveChanges();

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, document.DocumentDate);

                return document.ID;
            }
        }

        public void DeleteSupplyDocument(SupplyDocument document)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                if (!document.IsMarkToDelete)
                {
                    document.IsProcessed = false;

                    var wirings = db.RemainsOfGoodWirings.Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SupplyDocumentID == document.ID).ToList();

                    foreach (var wiring in wirings)
                    {
                        db.RemainsOfGoodWirings.Remove(wiring);
                    }
                }

                document.IsMarkToDelete = !document.IsMarkToDelete;

                db.Entry(document).State = EntityState.Modified;

                db.SaveChanges();

                InventoryManager.MarkInventoryDocumentRecalculate(document.WarehouseID, document.DocumentDate);
            }
        }

        public List<SupplyDocumentItem> GetSupplyDocumentItems(int supplyId)
        {
            using (var db = PizzaAppDB.GetNewContext())
            {
                return db.SupplyDocumentItems.Include("Product").Include("Product.Measure").Where(x=>x.BranchId == BranchRepository.SelectedBranchId).Where(x => x.SupplyDocumentID == supplyId).ToList();
            }
        }
    }
}