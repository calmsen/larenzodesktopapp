namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OnWayTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ChangeToOnWay", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Orders", "ChangeToPaid", c => c.DateTime(precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "ChangeToPaid");
            DropColumn("dbo.Orders", "ChangeToOnWay");
        }
    }
}
