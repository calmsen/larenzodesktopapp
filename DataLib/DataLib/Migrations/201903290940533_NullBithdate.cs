namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullBithdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clients", "BirthDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clients", "BirthDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
    }
}
