namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Add_In_WebOrder_CreateDate_Fild : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WebOrders", "CreateDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WebOrders", "CreateDate");
        }
    }
}
