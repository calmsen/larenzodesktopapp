namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class filds_2017_10_21 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SessionDocumentItems", "IdBrand", c => c.Int(nullable: false));
            AddColumn("dbo.SessionDocumentItems", "PayTypeIndex", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SessionDocumentItems", "PayTypeIndex");
            DropColumn("dbo.SessionDocumentItems", "IdBrand");
        }
    }
}
