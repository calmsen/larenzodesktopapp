namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DishSection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dishes", "SectionID", c => c.Int());
            CreateIndex("dbo.Dishes", "SectionID");
            AddForeignKey("dbo.Dishes", "SectionID", "dbo.Sections", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dishes", "SectionID", "dbo.Sections");
            DropIndex("dbo.Dishes", new[] { "SectionID" });
            DropColumn("dbo.Dishes", "SectionID");
        }
    }
}
