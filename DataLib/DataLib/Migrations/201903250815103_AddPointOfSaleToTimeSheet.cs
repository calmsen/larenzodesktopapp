namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPointOfSaleToTimeSheet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TimeSheets", "PointOfSaleId", c => c.Int(nullable: false, defaultValue:1));
            AddColumn("dbo.TimeSheets", "BrandId", c => c.Int(nullable: false, defaultValue:1));
            CreateIndex("dbo.TimeSheets", "PointOfSaleId");
            CreateIndex("dbo.TimeSheets", "BrandId");
            AddForeignKey("dbo.TimeSheets", "BrandId", "dbo.Brands", "ID", cascadeDelete: true);
            AddForeignKey("dbo.TimeSheets", "PointOfSaleId", "dbo.PointOfSales", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TimeSheets", "PointOfSaleId", "dbo.PointOfSales");
            DropForeignKey("dbo.TimeSheets", "BrandId", "dbo.Brands");
            DropIndex("dbo.TimeSheets", new[] { "BrandId" });
            DropIndex("dbo.TimeSheets", new[] { "PointOfSaleId" });
            DropColumn("dbo.TimeSheets", "BrandId");
            DropColumn("dbo.TimeSheets", "PointOfSaleId");
        }
    }
}
