namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPointOfSaleToCafe : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CafeOrderDocuments", "PointOfSaleId", c => c.Int(nullable: false, defaultValue: 1));
            CreateIndex("dbo.CafeOrderDocuments", "PointOfSaleId");
            AddForeignKey("dbo.CafeOrderDocuments", "PointOfSaleId", "dbo.PointOfSales", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CafeOrderDocuments", "PointOfSaleId", "dbo.PointOfSales");
            DropIndex("dbo.CafeOrderDocuments", new[] { "PointOfSaleId" });
            DropColumn("dbo.CafeOrderDocuments", "PointOfSaleId");
        }
    }
}
