namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class IsOnlinePaymentAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "IsOnlinePayment", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "IsOnlinePayment");
        }
    }
}
