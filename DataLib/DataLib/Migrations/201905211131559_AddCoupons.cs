namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCoupons : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CouponRuleElements",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuthorModified = c.String(),
                        DateModified = c.DateTime(precision: 7, storeType: "datetime2"),
                        BranchId = c.Int(nullable: false),
                        CouponRuleId = c.Int(nullable: false),
                        ElementId = c.Int(nullable: false),
                        ElementContext = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CouponRules", t => t.CouponRuleId, cascadeDelete: true)
                .Index(t => t.CouponRuleId);
            
            CreateTable(
                "dbo.CouponRules",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuthorModified = c.String(),
                        DateModified = c.DateTime(precision: 7, storeType: "datetime2"),
                        BranchId = c.Int(nullable: false),
                        BrandId = c.Int(nullable: false),
                        Name = c.String(),
                        Price = c.Int(nullable: false),
                        Percent = c.Int(nullable: false),
                        SumToApply = c.Int(nullable: false),
                        AllDishRequired = c.Boolean(nullable: false),
                        From = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        To = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Coupons",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuthorModified = c.String(),
                        DateModified = c.DateTime(precision: 7, storeType: "datetime2"),
                        BranchId = c.Int(nullable: false),
                        CouponRuleId = c.Int(nullable: false),
                        Activated = c.Boolean(nullable: false),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CouponRules", t => t.CouponRuleId, cascadeDelete: true)
                .Index(t => t.CouponRuleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Coupons", "CouponRuleId", "dbo.CouponRules");
            DropForeignKey("dbo.CouponRuleElements", "CouponRuleId", "dbo.CouponRules");
            DropIndex("dbo.Coupons", new[] { "CouponRuleId" });
            DropIndex("dbo.CouponRuleElements", new[] { "CouponRuleId" });
            DropTable("dbo.Coupons");
            DropTable("dbo.CouponRules");
            DropTable("dbo.CouponRuleElements");
        }
    }
}
