namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddApplyToAllDishes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CouponRules", "ApplyToAllDishes", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CouponRules", "ApplyToAllDishes");
        }
    }
}
