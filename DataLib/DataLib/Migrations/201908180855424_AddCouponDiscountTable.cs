namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCouponDiscountTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CouponDiscounts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuthorModified = c.String(),
                        DateModified = c.DateTime(precision: 7, storeType: "datetime2"),
                        BranchId = c.Int(nullable: false),
                        DiscountInPercent = c.Int(nullable: false),
                        SumToApply = c.Int(nullable: false),
                        DaysOfWeek = c.Int(nullable: false),
                        TimeBegin = c.Time(nullable: false, precision: 7),
                        TimeEnd = c.Time(nullable: false, precision: 7),
                        CouponRuleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CouponRules", t => t.CouponRuleId, cascadeDelete: true)
                .Index(t => t.CouponRuleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CouponDiscounts", "CouponRuleId", "dbo.CouponRules");
            DropIndex("dbo.CouponDiscounts", new[] { "CouponRuleId" });
            DropTable("dbo.CouponDiscounts");
        }
    }
}
