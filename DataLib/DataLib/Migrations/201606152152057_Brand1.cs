namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Brand1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Brands",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SiteAddress = c.String(),
                        Check = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Orders", "IdBrand", c => c.Int(nullable: false));
            AddColumn("dbo.Orders", "Brand_ID", c => c.Int());
            CreateIndex("dbo.Orders", "Brand_ID");
            AddForeignKey("dbo.Orders", "Brand_ID", "dbo.Brands", "ID");}
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "Brand_ID", "dbo.Brands");
            DropIndex("dbo.Orders", new[] { "Brand_ID" });
            DropColumn("dbo.Orders", "Brand_ID");
            DropColumn("dbo.Orders", "IdBrand");
            DropTable("dbo.Brands");
        }
    }
}
