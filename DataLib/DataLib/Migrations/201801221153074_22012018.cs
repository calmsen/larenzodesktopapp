namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class _22012018 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BranchName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.CallDatas", "BranchId", c => c.Int(nullable: false, defaultValue:2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallDatas", "BranchId");
            DropTable("dbo.Branches");
        }
    }
}
