namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddIsFastDriver_Order : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "IsFastDriver", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "IsFastDriver");
        }
    }
	// ALTER TABLE [dbo].[Orders] ADD [IsFastDriver] [bit] NOT NULL DEFAULT 0
}
