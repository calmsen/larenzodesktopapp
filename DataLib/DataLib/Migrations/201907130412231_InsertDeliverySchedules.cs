namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsertDeliverySchedules : DbMigration
    {
        public override void Up()
        {
            Sql(@"with d2 as (
    select d1.DayOfWeekID, 
		d1.DeliveryBegin,
		d1.DeliveryEnd,
		d1.DeliveryTime,
		d1.AddressGroupID,
		d1.BranchId,
		(case d1.PointOfSaleId when 1 then 2 else 1 end) as PointOfSaleId
    from DeliverySchedules d1  
	where d1.BranchId = 2
)
insert into DeliverySchedules (DayOfWeekID, DeliveryBegin, DeliveryEnd, DeliveryTime, AddressGroupID, BranchId, PointOfSaleId, Priority)
select DayOfWeekID, DeliveryBegin, DeliveryEnd, DeliveryTime, AddressGroupID, BranchId, PointOfSaleId, 2 from d2");
        }
        
        public override void Down()
        {
        }
    }
}
