namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateClient : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientAddresses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ClientId = c.Int(nullable: false),
                        Street = c.String(),
                        House = c.String(),
                        Appartament = c.String(),
                        Porch = c.String(),
                        Additional = c.String(),
                        BranchId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        BirthDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Gender = c.Int(nullable: false),
                        BranchId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientPhones",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ClientId = c.Int(nullable: false),
                        Phone = c.String(nullable: false),
                        BranchId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
            AddColumn("dbo.Users", "CashboxPassword", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClientPhones", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.ClientAddresses", "ClientId", "dbo.Clients");
            DropIndex("dbo.ClientPhones", new[] { "ClientId" });
            DropIndex("dbo.ClientAddresses", new[] { "ClientId" });
            DropColumn("dbo.Users", "CashboxPassword");
            DropTable("dbo.ClientPhones");
            DropTable("dbo.Clients");
            DropTable("dbo.ClientAddresses");
        }
    }
}
