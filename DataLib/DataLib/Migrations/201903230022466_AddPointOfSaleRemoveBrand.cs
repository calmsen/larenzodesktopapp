namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPointOfSaleRemoveBrand : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PointOfSales", "BrandId", "dbo.Brands");
            DropIndex("dbo.PointOfSales", new[] { "BrandId" });
            DropColumn("dbo.PointOfSales", "BrandId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PointOfSales", "BrandId", c => c.Int(nullable: false));
            CreateIndex("dbo.PointOfSales", "BrandId");
            AddForeignKey("dbo.PointOfSales", "BrandId", "dbo.Brands", "ID", cascadeDelete: true);
        }
    }
}
