namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSms : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "SmsSended", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "SmsSended");
        }
    }
}
