namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DiscountCardchanges : DbMigration
    {
        public override void Up()
        {
            /*AddColumn("dbo.CafeOrderDocuments", "DiscountCardID", c => c.Int());
            AddForeignKey("dbo.CafeOrderDocuments", "DiscountCardID", "dbo.DiscountCards", "ID");
            CreateIndex("dbo.CafeOrderDocuments", "DiscountCardID");*/
        }
        
        public override void Down()
        {
            DropIndex("dbo.CafeOrderDocuments", new[] { "DiscountCardID" });
            DropForeignKey("dbo.CafeOrderDocuments", "DiscountCardID", "dbo.DiscountCards");
            DropColumn("dbo.CafeOrderDocuments", "DiscountCardID");
        }
    }
}
