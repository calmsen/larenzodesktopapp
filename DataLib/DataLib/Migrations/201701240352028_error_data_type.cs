namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class error_data_type : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "PersonsCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "PersonsCount", c => c.Boolean(nullable: false));
        }
    }
}
