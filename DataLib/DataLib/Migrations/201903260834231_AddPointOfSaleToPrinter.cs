using System.Data.Entity.Migrations;
namespace LaRenzo.DataRepository.Migrations
{
    public partial class AddPointOfSaleToPrinter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "PointOfSaleId", c => c.Int(nullable: false, defaultValue: 1));
            AddColumn("dbo.Printers", "PointOfSaleId", c => c.Int(nullable: false, defaultValue: 1));
            CreateIndex("dbo.Orders", "PointOfSaleId");
            CreateIndex("dbo.Printers", "PointOfSaleId");
            AddForeignKey("dbo.Orders", "PointOfSaleId", "dbo.PointOfSales", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Printers", "PointOfSaleId", "dbo.PointOfSales", "ID", cascadeDelete: true);
        }

        public override void Down()
        {
            DropForeignKey("dbo.Printers", "PointOfSaleId", "dbo.PointOfSales");
            DropForeignKey("dbo.Orders", "PointOfSaleId", "dbo.PointOfSales");
            DropIndex("dbo.Printers", new[] { "PointOfSaleId" });
            DropIndex("dbo.Orders", new[] { "PointOfSaleId" });
            DropColumn("dbo.Printers", "PointOfSaleId");
            DropColumn("dbo.Orders", "PointOfSaleId");
        }
    }
}
