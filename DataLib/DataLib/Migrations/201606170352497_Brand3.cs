namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Brand3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WebOrders", "IdBrand", c => c.Int(nullable: false));
            AddColumn("dbo.WebOrders", "Brand_ID", c => c.Int());
            CreateIndex("dbo.WebOrders", "Brand_ID");
            AddForeignKey("dbo.WebOrders", "Brand_ID", "dbo.Brands", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WebOrders", "Brand_ID", "dbo.Brands");
            DropIndex("dbo.WebOrders", new[] { "Brand_ID" });
            DropColumn("dbo.WebOrders", "Brand_ID");
            DropColumn("dbo.WebOrders", "IdBrand");
        }
    }
}
