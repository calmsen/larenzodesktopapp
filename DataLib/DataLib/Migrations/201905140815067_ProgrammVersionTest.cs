namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProgrammVersionTest : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProgrammVersions", "TestVersion", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProgrammVersions", "TestVersion");
        }
    }
}
