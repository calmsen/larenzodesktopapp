namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class IsNearTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "IsNearTime", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "IsNearTime");
        }
    }
	// ALTER TABLE [dbo].[Orders] ADD [IsNearTime] [bit] NOT NULL DEFAULT 0
}
