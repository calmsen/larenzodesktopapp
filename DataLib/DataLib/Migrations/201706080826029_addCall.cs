namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class addCall : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CallDatas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PhoneNumber = c.String(),
                        IdCall = c.String(),
                        Name = c.String(),
                        Created = c.DateTime(nullable: false),
                        Answed = c.DateTime(nullable: false),
                        Droped = c.DateTime(nullable: false),
                        IsUnAnswed = c.Boolean(nullable: false),
                        IsOnline = c.Boolean(nullable: false),
                        IsEnter = c.Boolean(nullable: false),
                        Session_ID = c.Int(),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Sessions", t => t.Session_ID)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.Session_ID)
                .Index(t => t.User_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CallDatas", "User_ID", "dbo.Users");
            DropForeignKey("dbo.CallDatas", "Session_ID", "dbo.Sessions");
            DropIndex("dbo.CallDatas", new[] { "User_ID" });
            DropIndex("dbo.CallDatas", new[] { "Session_ID" });
            DropTable("dbo.CallDatas");
        }
    }
}
