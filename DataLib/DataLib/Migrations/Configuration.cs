using System.Data.Entity.Migrations;
using DataLib.Models;

namespace LaRenzo.DataRepository.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<PizzaAppDB>
    {
        public Configuration()
        {
            CommandTimeout = 60 * 10;
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(PizzaAppDB context)
        {
            // Microsoft comment says "This method will be called after migrating to the latest version."
            // However my testing shows that it is called every time the software starts

            // Apply changes to database
            context.SaveChanges();
            base.Seed(context);
        }
    }
}
