namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DiscountOnDish : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlackListItems", "Comment", c => c.String());
            AddColumn("dbo.Dishes", "DiscountAppliсability", c => c.Boolean(nullable: false));
            AddColumn("dbo.Categories", "IdBrand", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Categories", "IdBrand");
            DropColumn("dbo.Dishes", "DiscountAppliсability");
            DropColumn("dbo.BlackListItems", "Comment");
        }
    }
}
