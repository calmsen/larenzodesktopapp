namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class IsOrderPaid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "IsPaid", c => c.Boolean(nullable: false));
            AddColumn("dbo.WebOrders", "IsPaid", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WebOrders", "IsPaid");
            DropColumn("dbo.Orders", "IsPaid");
        }
    }
}
