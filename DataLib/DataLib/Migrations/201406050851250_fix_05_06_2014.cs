namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix_05_06_2014 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.GlobalOptions", "Value", c => c.String(maxLength: 400));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.GlobalOptions", "Value", c => c.String(maxLength: 100));
        }
    }
}
