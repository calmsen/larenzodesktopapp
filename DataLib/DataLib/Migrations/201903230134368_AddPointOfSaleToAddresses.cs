namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPointOfSaleToAddresses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "PointOfSaleId", c => c.Int(nullable: false, defaultValue:1));
            CreateIndex("dbo.Addresses", "PointOfSaleId");
            AddForeignKey("dbo.Addresses", "PointOfSaleId", "dbo.PointOfSales", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Addresses", "PointOfSaleId", "dbo.PointOfSales");
            DropIndex("dbo.Addresses", new[] { "PointOfSaleId" });
            DropColumn("dbo.Addresses", "PointOfSaleId");
        }
    }
}
