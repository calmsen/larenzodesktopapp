namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MultiChanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BankInteractions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        OrderId = c.Int(nullable: false),
                        OrderGuid = c.Guid(nullable: false),
                        CommandCreated = c.DateTime(nullable: false),
                        CommandSent = c.DateTime(nullable: false),
                        Comment = c.String(),
                        Status = c.Int(nullable: false),
                        Command = c.Int(nullable: false),
                        BankResponse = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.OrderId);
            
            AddColumn("dbo.Orders", "WebOrderGuid", c => c.Guid());
            AddColumn("dbo.OrderStates", "BankInteractionMode", c => c.Int(nullable: false));

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BankInteractions", "OrderId", "dbo.Orders");
            DropIndex("dbo.BankInteractions", new[] { "OrderId" });
            DropColumn("dbo.OrderStates", "BankInteractionMode");
            DropColumn("dbo.Orders", "WebOrderGuid");
            DropTable("dbo.BankInteractions");
        }
    }
}
