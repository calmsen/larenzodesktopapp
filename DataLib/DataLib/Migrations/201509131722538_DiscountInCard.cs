namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DiscountInCard : DbMigration
    {
		public override void Up()
		{
			AddColumn("dbo.DiscountCards", "Discount", c => c.Int(nullable: false));
		}

		public override void Down()
		{
			DropColumn("dbo.DiscountCards", "Discount");
		}
    }
}
