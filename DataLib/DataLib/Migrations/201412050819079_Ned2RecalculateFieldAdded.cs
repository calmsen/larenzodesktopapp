namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Ned2RecalculateFieldAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InventoryDocuments", "NeedToRecalculate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InventoryDocuments", "NeedToRecalculate");
        }
    }
}
