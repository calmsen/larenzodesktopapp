namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ReInit : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.InventoryDocuments", "NeedToRecalculate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InventoryDocuments", "NeedToRecalculate", c => c.Boolean(nullable: false));
        }
    }
}
