namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdditionalOfferDishMainDishColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdditionalOfferDishes", "MainDishId", c => c.Int(nullable: false));
            CreateIndex("dbo.AdditionalOfferDishes", "MainDishId");
            AddForeignKey("dbo.AdditionalOfferDishes", "MainDishId", "dbo.Dishes", "ID", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdditionalOfferDishes", "MainDishId", "dbo.Dishes");
            DropIndex("dbo.AdditionalOfferDishes", new[] { "MainDishId" });
            DropColumn("dbo.AdditionalOfferDishes", "MainDishId");
        }
    }
}
