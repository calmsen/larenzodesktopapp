namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TimeSheetWorkingWithPoints : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.TimeSheets");
            AddPrimaryKey("dbo.TimeSheets", new[] { "Time", "SectionID", "BranchId", "PointOfSaleId", "BrandId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.TimeSheets");
            AddPrimaryKey("dbo.TimeSheets", new[] { "Time", "SectionID", "BranchId" });
        }
    }
}
