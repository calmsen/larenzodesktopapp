namespace LaRenzo.DataRepository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGiftDishToCoupon : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CouponRules", "GiftDish", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CouponRules", "GiftDish");
        }
    }
}
