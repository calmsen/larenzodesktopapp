namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Brand2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Dishes", "IdBrand", c => c.Int(nullable: false));
            AddColumn("dbo.Dishes", "Brand_ID", c => c.Int());
            AddColumn("dbo.Categories", "IdBrand", c => c.Int(nullable: false));
            AddColumn("dbo.Categories", "Brand_ID", c => c.Int());
            AddColumn("dbo.Sections", "IdBrand", c => c.Int(nullable: false));
            AddColumn("dbo.Sections", "Brand_ID", c => c.Int());
            CreateIndex("dbo.Dishes", "Brand_ID");
            CreateIndex("dbo.Categories", "Brand_ID");
            CreateIndex("dbo.Sections", "Brand_ID");
            AddForeignKey("dbo.Dishes", "Brand_ID", "dbo.Brands", "ID");
            AddForeignKey("dbo.Categories", "Brand_ID", "dbo.Brands", "ID");
            AddForeignKey("dbo.Sections", "Brand_ID", "dbo.Brands", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sections", "Brand_ID", "dbo.Brands");
            DropForeignKey("dbo.Categories", "Brand_ID", "dbo.Brands");
            DropForeignKey("dbo.Dishes", "Brand_ID", "dbo.Brands");
            DropIndex("dbo.Sections", new[] { "Brand_ID" });
            DropIndex("dbo.Categories", new[] { "Brand_ID" });
            DropIndex("dbo.Dishes", new[] { "Brand_ID" });
            DropColumn("dbo.Sections", "Brand_ID");
            DropColumn("dbo.Sections", "IdBrand");
            DropColumn("dbo.Categories", "Brand_ID");
            DropColumn("dbo.Categories", "IdBrand");
            DropColumn("dbo.Dishes", "Brand_ID");
            DropColumn("dbo.Dishes", "IdBrand");
        }
    }
}
