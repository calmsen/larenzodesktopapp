namespace DataLib.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class fix_01_01_2014 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RemainsOfGoodWirings", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RemainsOfGoodWirings", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
