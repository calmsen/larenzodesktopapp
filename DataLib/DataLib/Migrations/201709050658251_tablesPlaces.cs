namespace LaRenzo.DataRepository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class tablesPlaces : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tables", "IsOpenP1", c => c.Boolean(nullable: false));
            AddColumn("dbo.Tables", "IsOpenP2", c => c.Boolean(nullable: false));
            AddColumn("dbo.Tables", "IsOpenP3", c => c.Boolean(nullable: false));
            AddColumn("dbo.Tables", "IsOpenP4", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tables", "IsOpenP4");
            DropColumn("dbo.Tables", "IsOpenP3");
            DropColumn("dbo.Tables", "IsOpenP2");
            DropColumn("dbo.Tables", "IsOpenP1");}
    }
}
