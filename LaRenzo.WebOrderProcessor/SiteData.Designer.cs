﻿namespace LaRenzo.WebOrderProcessor
{
    partial class SiteData
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.labelDateValue = new System.Windows.Forms.Label();
            this.labelProcessed = new System.Windows.Forms.Label();
            this.labelNewValue = new System.Windows.Forms.Label();
            this.labelSite = new System.Windows.Forms.Label();
            this.labelSiteValue = new System.Windows.Forms.Label();
            this.labelNew = new System.Windows.Forms.Label();
            this.labelProcessedValue = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.labelErrorValue = new System.Windows.Forms.Label();
            this.buttonSite = new System.Windows.Forms.Button();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.labelDateValue, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.labelProcessed, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.labelNewValue, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.labelSite, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.labelSiteValue, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.labelNew, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.labelProcessedValue, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.labelDate, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.labelError, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.labelErrorValue, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.buttonSite, 1, 5);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel.RowCount = 6;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(1603, 638);
            this.tableLayoutPanel.TabIndex = 7;
            // 
            // labelDateValue
            // 
            this.labelDateValue.AutoSize = true;
            this.labelDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDateValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDateValue.Location = new System.Drawing.Point(528, 179);
            this.labelDateValue.Name = "labelDateValue";
            this.labelDateValue.Padding = new System.Windows.Forms.Padding(10);
            this.labelDateValue.Size = new System.Drawing.Size(1049, 49);
            this.labelDateValue.TabIndex = 7;
            this.labelDateValue.Text = "0";
            // 
            // labelProcessed
            // 
            this.labelProcessed.AutoSize = true;
            this.labelProcessed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelProcessed.Location = new System.Drawing.Point(23, 23);
            this.labelProcessed.Margin = new System.Windows.Forms.Padding(0);
            this.labelProcessed.Name = "labelProcessed";
            this.labelProcessed.Padding = new System.Windows.Forms.Padding(10);
            this.labelProcessed.Size = new System.Drawing.Size(499, 49);
            this.labelProcessed.TabIndex = 0;
            this.labelProcessed.Text = "Количество записанных в базе заказов:";
            // 
            // labelNewValue
            // 
            this.labelNewValue.AutoSize = true;
            this.labelNewValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelNewValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNewValue.Location = new System.Drawing.Point(528, 127);
            this.labelNewValue.Name = "labelNewValue";
            this.labelNewValue.Padding = new System.Windows.Forms.Padding(10);
            this.labelNewValue.Size = new System.Drawing.Size(1049, 49);
            this.labelNewValue.TabIndex = 5;
            this.labelNewValue.Text = "0";
            // 
            // labelSite
            // 
            this.labelSite.AutoSize = true;
            this.labelSite.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSite.Location = new System.Drawing.Point(23, 75);
            this.labelSite.Margin = new System.Windows.Forms.Padding(0);
            this.labelSite.Name = "labelSite";
            this.labelSite.Padding = new System.Windows.Forms.Padding(10);
            this.labelSite.Size = new System.Drawing.Size(381, 49);
            this.labelSite.TabIndex = 2;
            this.labelSite.Text = "Количество заказов на сайте:";
            // 
            // labelSiteValue
            // 
            this.labelSiteValue.AutoSize = true;
            this.labelSiteValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSiteValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSiteValue.Location = new System.Drawing.Point(528, 75);
            this.labelSiteValue.Name = "labelSiteValue";
            this.labelSiteValue.Padding = new System.Windows.Forms.Padding(10);
            this.labelSiteValue.Size = new System.Drawing.Size(1049, 49);
            this.labelSiteValue.TabIndex = 3;
            this.labelSiteValue.Text = "0";
            // 
            // labelNew
            // 
            this.labelNew.AutoSize = true;
            this.labelNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNew.Location = new System.Drawing.Point(23, 127);
            this.labelNew.Margin = new System.Windows.Forms.Padding(0);
            this.labelNew.Name = "labelNew";
            this.labelNew.Padding = new System.Windows.Forms.Padding(10);
            this.labelNew.Size = new System.Drawing.Size(457, 49);
            this.labelNew.TabIndex = 4;
            this.labelNew.Text = "Количество новых заказов на сайте:";
            // 
            // labelProcessedValue
            // 
            this.labelProcessedValue.AutoSize = true;
            this.labelProcessedValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProcessedValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelProcessedValue.Location = new System.Drawing.Point(528, 23);
            this.labelProcessedValue.Name = "labelProcessedValue";
            this.labelProcessedValue.Padding = new System.Windows.Forms.Padding(10);
            this.labelProcessedValue.Size = new System.Drawing.Size(1049, 49);
            this.labelProcessedValue.TabIndex = 1;
            this.labelProcessedValue.Text = "0";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDate.Location = new System.Drawing.Point(23, 179);
            this.labelDate.Margin = new System.Windows.Forms.Padding(0);
            this.labelDate.Name = "labelDate";
            this.labelDate.Padding = new System.Windows.Forms.Padding(10);
            this.labelDate.Size = new System.Drawing.Size(386, 49);
            this.labelDate.TabIndex = 6;
            this.labelDate.Text = "Дата последнего обновления:";
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelError.Location = new System.Drawing.Point(23, 231);
            this.labelError.Margin = new System.Windows.Forms.Padding(0);
            this.labelError.Name = "labelError";
            this.labelError.Padding = new System.Windows.Forms.Padding(10);
            this.labelError.Size = new System.Drawing.Size(334, 49);
            this.labelError.TabIndex = 8;
            this.labelError.Text = "Ошибки при обновлении:";
            // 
            // labelErrorValue
            // 
            this.labelErrorValue.AutoSize = true;
            this.labelErrorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelErrorValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelErrorValue.Location = new System.Drawing.Point(528, 231);
            this.labelErrorValue.Name = "labelErrorValue";
            this.labelErrorValue.Padding = new System.Windows.Forms.Padding(10);
            this.labelErrorValue.Size = new System.Drawing.Size(1049, 49);
            this.labelErrorValue.TabIndex = 9;
            this.labelErrorValue.Text = "Нет ошибок";
            // 
            // buttonSite
            // 
            this.buttonSite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSite.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSite.Location = new System.Drawing.Point(528, 286);
            this.buttonSite.Name = "buttonSite";
            this.buttonSite.Size = new System.Drawing.Size(1049, 326);
            this.buttonSite.TabIndex = 10;
            this.buttonSite.Text = "Перейти на сайт";
            this.buttonSite.UseVisualStyleBackColor = true;
            this.buttonSite.Click += new System.EventHandler(this.buttonSite_Click);
            // 
            // SiteData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "SiteData";
            this.Size = new System.Drawing.Size(1603, 638);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label labelDateValue;
        private System.Windows.Forms.Label labelProcessed;
        private System.Windows.Forms.Label labelNewValue;
        private System.Windows.Forms.Label labelSite;
        private System.Windows.Forms.Label labelSiteValue;
        private System.Windows.Forms.Label labelNew;
        private System.Windows.Forms.Label labelProcessedValue;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Label labelErrorValue;
        private System.Windows.Forms.Button buttonSite;
    }
}
