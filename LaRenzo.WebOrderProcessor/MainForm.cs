﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using LaRenzo.WebOrderProcessor.Properties;
using NLog;

namespace LaRenzo.WebOrderProcessor
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            Text = "Загрузка Интернет заказов. Версия: " + Settings.Default.Version;
            var logger = LogManager.GetCurrentClassLogger();
            logger.Info("Инициализация WebOrderProcessor");
            tabControlSites.TabPages.Clear();
            tabControlSites.DrawMode = TabDrawMode.OwnerDrawFixed;
            tabControlSites.DrawItem += tabControlSites_DrawItem;
            var branches = ServiceSelector.BranchRepository.GetItems();
            foreach (var branch in branches)
            {
                ServiceSelector.BranchRepository.SelectedBranchId = branch.ID;
                var brands = ServiceSelector.BrandManager.GetList();
                foreach (var brand in brands)
                {
                    var sites = ServiceSelector.GlobalOptionsManager.GetSitesList(brand.ID);
                    foreach (var site in sites)
                    {
                        var page = new TabPage(site);
                        var siteData = new SiteData { SiteAddress = site, BrandId = brand.ID, BranchId = branch.ID, Dock = DockStyle.Fill };
                        page.Controls.Add(siteData);
                        tabControlSites.TabPages.Add(page);
                        TabColors.Add(page, Color.Green);
                        siteData.TabColors = TabColors;
                        await siteData.DoWorkAsyncInfiniteLoop();
                    }
                }
            }
            logger.Info("Инициализация WebOrderProcessor завершена");
        }
        public Dictionary<TabPage, Color> TabColors = new Dictionary<TabPage, Color>();
        private void tabControlSites_DrawItem(object sender, DrawItemEventArgs e)
        {
            using (Brush br = new SolidBrush(TabColors[tabControlSites.TabPages[e.Index]]))
            {
                e.Graphics.FillRectangle(br, e.Bounds);
                SizeF sz = e.Graphics.MeasureString(tabControlSites.TabPages[e.Index].Text, e.Font);
                e.Graphics.DrawString(tabControlSites.TabPages[e.Index].Text, e.Font, Brushes.Black, e.Bounds.Left + (e.Bounds.Width - sz.Width) / 2, e.Bounds.Top + (e.Bounds.Height - sz.Height) / 2 + 1);

                Rectangle rect = e.Bounds;
                rect.Offset(0, 1);
                rect.Inflate(0, -1);
                e.Graphics.DrawRectangle(Pens.DarkGray, rect);
                e.DrawFocusRectangle();
            }
        }
    }
}
