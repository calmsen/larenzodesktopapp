﻿namespace LaRenzo.WebOrderProcessor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPageSites = new System.Windows.Forms.TabPage();
            this.tabControlSites = new System.Windows.Forms.TabControl();
            this.tabControlSites.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1809, 417);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPageSites
            // 
            this.tabPageSites.Location = new System.Drawing.Point(4, 29);
            this.tabPageSites.Name = "tabPageSites";
            this.tabPageSites.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSites.Size = new System.Drawing.Size(1809, 417);
            this.tabPageSites.TabIndex = 0;
            this.tabPageSites.Text = "tabPage1";
            this.tabPageSites.UseVisualStyleBackColor = true;
            // 
            // tabControlSites
            // 
            this.tabControlSites.Controls.Add(this.tabPageSites);
            this.tabControlSites.Controls.Add(this.tabPage2);
            this.tabControlSites.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlSites.Location = new System.Drawing.Point(0, 0);
            this.tabControlSites.Name = "tabControlSites";
            this.tabControlSites.SelectedIndex = 0;
            this.tabControlSites.Size = new System.Drawing.Size(1817, 450);
            this.tabControlSites.TabIndex = 7;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1817, 450);
            this.Controls.Add(this.tabControlSites);
            this.Name = "MainForm";
            this.Text = "Загрузка Интернет заказов";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tabControlSites.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPageSites;
        private System.Windows.Forms.TabControl tabControlSites;
    }
}

