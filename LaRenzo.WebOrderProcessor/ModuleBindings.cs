﻿using Ninject.Modules;
using Ninject.Extensions.Conventions;
using DataLib.Models;
using System.Reflection;
using LaRenzo.Interfaces;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.DataRepository;

namespace LaRenzo.DataHelpers
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(x => x
            .From(Assembly.GetAssembly(typeof(PizzaAppDB)))
            .SelectAllClasses()
            .EndingWith("Repository")
            .BindDefaultInterface()
            .Configure(b => b.InSingletonScope()));

            Kernel.Bind(x => x
            .From(Assembly.GetAssembly(typeof(PizzaAppDB)))
            .SelectAllClasses()
            .EndingWith("Manager")
            .BindDefaultInterface()
            .Configure(b => b.InSingletonScope()));

            Bind<IThreadLocalStorage>().To<ThreadLocalStorage>();
            Bind<IBaseOptionsFunctions>().To<BaseOptionsFunctions>();
            Bind<ISignalProcessor>().To<SignalProcessor>();
        }
    }
}
