﻿using LaRenzo.DataRepository.Repositories.Documents.WebOrders;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.DataRepository.Repositories.Settings;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.WebOrderProcessor
{
    public class ServiceSelector
    {
        public static IBranchRepository BranchRepository { get; set; }
        public static IBrandManager BrandManager { get; set; }
        public static IGlobalOptionsManager GlobalOptionsManager { get; set; }
        public static IWebOrderManager WebOrderManager { get; set; }
        public static void RersolveRepositories()
        {
            IKernel kernel = new StandardKernel(new NinjectSettings
            {
                InjectAttribute = typeof(DataLib.Models.InjectAttribute)
            });
            kernel.Load(Assembly.GetExecutingAssembly());

            BranchRepository = kernel.Get<IBranchRepository>();
            BrandManager = kernel.Get<IBrandManager>();
            GlobalOptionsManager = kernel.Get<IGlobalOptionsManager>();
            WebOrderManager = kernel.Get<IWebOrderManager>();
        }
    }
}
