﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories.Documents.WebOrders;
using LaRenzo.WebOrderProcessor.Properties;
using NLog;

namespace LaRenzo.WebOrderProcessor
{
    public partial class SiteData : UserControl
    {
        public string SiteAddress { get; set; }
        public int BrandId { get; set; }

        public int BranchId { get; set; }
        public SiteData()
        {
            InitializeComponent();
        }

        public async Task DoWorkAsyncInfiniteLoop()
        {
            var logger = LogManager.GetCurrentClassLogger();
            while (true)
            {
                logger.Info("Обработка заказов с сайта: " + SiteAddress + "; Дата: " + DateTime.Now.ToString("F"));
                Random rand = new Random();
                var pause = rand.Next(20000, 40000);
                TabPage tabPage = null;
                if (Parent is TabPage)
                {
                    tabPage = (TabPage)Parent;
                }
                try
                {
                    labelDateValue.Text = DateTime.Now.ToString("F");
                    var result = await GrabWebOrder();
                    labelNewValue.Text = result.NewCount.ToString();
                    labelSiteValue.Text = result.WebOrderCount.ToString();
                    if (result.ExistCount > result.WebOrderCount)
                    {
                        labelProcessedValue.ForeColor = Color.Red;
                        labelProcessedValue.Text = result.ExistCount + ": в базе есть дубли";
                    }
                    else
                    {
                        labelProcessedValue.ForeColor = Color.Green;
                        labelProcessedValue.Text = result.ExistCount + ": в базе нет дублей";
                    }
                    SetTabHeader(tabPage, Color.Green);
                    labelErrorValue.ForeColor = Color.Green;
                    logger.Info("Обработка заказов с сайта: " + SiteAddress + "; " +
                                "На сайте: " + result.WebOrderCount + "; " +
                                "В базе: " + result.ExistCount + "; " +
                                "Новых: " + result.NewCount);
                }
                catch (Exception e)
                {
                    logger.Error(e);
                    labelErrorValue.Text = e.Message;
                    SetTabHeader(tabPage, Color.Red);
                    labelErrorValue.ForeColor = Color.Red;
                }
                logger.Info("Обработка заказов с сайта: " + SiteAddress + " завершена");
                await Task.Delay(pause);
            }
        }

        public Dictionary<TabPage, Color> TabColors { get; set; }

        private void SetTabHeader(TabPage page, Color color)
        {
            TabColors[page] = color;
            page.Invalidate();
        }

        private async Task<GrabOrdersResult> GrabWebOrder()
        {
            var result = await ServiceSelector.WebOrderManager.GrabOrders(SiteAddress, BrandId, Settings.Default.SiteProtocol, BranchId);
            return result;
        }

        private void buttonSite_Click(object sender, EventArgs e)
        {
            Process.Start(string.Format($"{Settings.Default.SiteProtocol}://{SiteAddress}"));
        }
    }
}


