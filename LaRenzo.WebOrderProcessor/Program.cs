﻿using System;
using System.Data.Entity;
using System.Threading;
using System.Windows.Forms;
using NLog;

namespace LaRenzo.WebOrderProcessor
{
    static class Program
    {
        static Logger _logger;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            _logger = LogManager.GetCurrentClassLogger();

            ServiceSelector.RersolveRepositories();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //My initialization, enable to migrate DB to latest version
            //Database.SetInitializer<PizzaAppDB>(null);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        static void Application_ThreadException(
            object sender, ThreadExceptionEventArgs e)
        {
            _logger.Error(e.Exception);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            _logger.Error(e.ExceptionObject);
        }
    }
}
