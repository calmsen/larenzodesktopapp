﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.Interfaces
{
    public interface IOrderCodeChecker
    {
        bool DoCodeAlreadyExists(string uid, int selectedBranchId);
    }
}
