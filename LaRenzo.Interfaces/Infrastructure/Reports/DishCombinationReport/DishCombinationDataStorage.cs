﻿using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport
{
    /// <summary>
    /// В данном классе хранится информация, необходимая для поиска комбинаций
    /// </summary>
    public class DishCombinationDataStorage
    {
        private readonly int _lastDemension;

        public DishCombinationDataLevel OriginalData { get; set; }
        public List<DishCombinationDataLevel> LevelData { get; set; }
        public List<int> DishIdList { get; set; }

        private List<Dish> ActualDishList { get; set; }

        public int CurrentAmount { get; set; }
        public int MaxAmount { get; set; }

        public bool CanBeCalculated { get; set; }

        public DishCombinationDataStorage(int dimensionCount, List<DishCombinationSource> data, IEnumerable<int> categories, List<Dish> actualDishList)
        {
            DishIdList = data.SelectMany(x => x.DishList).Distinct().OrderBy(x => x).ToList();
            ActualDishList = actualDishList;
            RemoveDeletedDishesFromList(categories);

            CanBeCalculated = DishIdList.Count >= dimensionCount;

            OriginalData = new DishCombinationDataLevel
            {
                LevelId = -1,
                Data = data
            };

            LevelData = new List<DishCombinationDataLevel>();

            for (int i = 0; i < dimensionCount; i++)
            {
                LevelData.Add(new DishCombinationDataLevel
                {
                    LevelId = i,

                    MaxIndex = DishIdList.Count - dimensionCount + i,
                    CurrentIndex = i,
                    Data = GetLevelData(i, i)
                });
            }

            _lastDemension = dimensionCount - 1;
        }

        private void RemoveDeletedDishesFromList(IEnumerable<int> categories)
        {
            DishIdList = DishIdList.Intersect(ActualDishList.Select(x => x.ID)).ToList();
        }

        private DishCombinationDataLevel GetLevel(int levelId)
        {
            return levelId == -1 ? OriginalData : LevelData.FirstOrDefault(x => x.LevelId == levelId);
        }

        /// <summary>
        /// Возвращает данные по выборке
        /// </summary>
        /// <param name="currentIndex">Текущий индекс элемента</param>
        /// <param name="currentLevel">Текущий уровень</param>
        /// <returns></returns>
        private List<DishCombinationSource> GetLevelData(int currentIndex, int currentLevel)
        {
            var parentLevel = GetLevel(currentLevel - 1);

            if (parentLevel == null) return new List<DishCombinationSource>();

            if (DishIdList.Count - 1 <= currentIndex) return new List<DishCombinationSource>();

            var dishId = DishIdList[currentIndex];

            return parentLevel.Data.Where(x => x.DishList.Contains(dishId)).ToList();
        }


        public int[] GetIndexes()
        {
            var result = new int[_lastDemension + 1];

            for (var i = 0; i <= _lastDemension; i++)
            {
                result[i] = LevelData[i].CurrentIndex;
            }

            return result;
        }


        internal int[] GetLimits()
        {
            var result = new int[_lastDemension + 1];

            for (var i = 0; i <= _lastDemension; i++)
            {
                result[i] = LevelData[i].MaxIndex;
            }

            return result;
        }



        public int GetCurrentElementCount()
        {
            return LevelData[_lastDemension].Data.Count;
        }

        public bool GoToNextElement()
        {
            var currentDimension = _lastDemension;

            // Смещаемся по измерениям назад, до тех пор, пока не найдем то, где еще нет максимального значения
            while (LevelData[currentDimension].CurrentIndex == LevelData[currentDimension].MaxIndex)
            {
                currentDimension--;
                if (currentDimension < 0) return false;
            }

            // Если таких измерений не нашлось, значит идет в проверку условия while, где мы должны выйти из цикла


            // Всем измерениям, включая текущее и последующие устанавливаем новое значение
            // Сохраняем текущее значение в выбранном измерении
            var currentIndex = LevelData[currentDimension].CurrentIndex;

            var caretMovedBack = false;

            for (int i = currentDimension; i <= _lastDemension; i++)
            {


                List<DishCombinationSource> data;

                do
                {
                    if (!caretMovedBack)
                    {
                        LevelData[i].CurrentIndex = currentIndex;
                    }

                    while (LevelData[i].CurrentIndex == LevelData[i].MaxIndex)
                    {
                        i--;
                        if (i < 0) return false;
                        currentIndex = LevelData[i].CurrentIndex;
                        caretMovedBack = true;
                    }

                    var index = ++currentIndex;
                    LevelData[i].CurrentIndex = index;
                    data = GetLevelData(index, i);

                } while (data.Count <= MaxAmount);

                if (i >= 0) LevelData[i].Data = data;

                caretMovedBack = false;

                /*while (IsLastElement(i))
                {
                    i--;
                    if (i < 0) return false;
                    currentIndex = LevelData[i].CurrentIndex + 1;
                }*/
            }

            return true;
        }

        public List<Dish> GetDishList(int[] indexes)
        {
            var predicate = PredicateBuilder.False<Dish>();
            foreach (var index in indexes)
            {
                var dishId = DishIdList[index];
                predicate = predicate.Or(x => x.ID == dishId);
            }

            return ActualDishList.Where(predicate.Compile()).ToList();
        }

        public int TotalPercent()
        {
            return (LevelData[0].CurrentIndex * 100) / LevelData[0].MaxIndex;
        }
    }
}
