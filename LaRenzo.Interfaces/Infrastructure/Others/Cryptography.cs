﻿using LaRenzo.Interfaces;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace LaRenzo.DataRepository.Repositories.Others
{
    public class CodeGenerator
    {
        private readonly IOrderCodeChecker _codeChecker;

        public CodeGenerator(IOrderCodeChecker codeChecker)
        {
            _codeChecker = codeChecker;
        }

        public string GenerateCode(int selectedBranchId)
        {
            var randomBytes = new byte[6];
            var rng = new RNGCryptoServiceProvider();
            const string chars = "БГДЕИЖКМПХУЯ";
            StringBuilder uid;
            
            do
            {
                rng.GetBytes(randomBytes);

                int seed = (randomBytes[0] & 0x7f) << 40 |randomBytes[1] << 32 |
                           randomBytes[2] << 24 |
                           randomBytes[3] << 16| randomBytes[4] << 8 | randomBytes[5];

                var rnd = new Random(seed);

                uid = new StringBuilder();

                uid.Append(chars[rnd.Next(12)]);
                uid.Append(String.Format("{0:000000}", rnd.Next(999999)));

            } while (_codeChecker.DoCodeAlreadyExists(uid.ToString(), selectedBranchId));
            
            return uid.ToString();
        }
    }
}
