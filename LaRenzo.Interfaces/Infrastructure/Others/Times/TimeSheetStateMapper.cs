﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Others.Times
{
    public class TimeSheetStateMapper
    {
        private readonly Dictionary<string, string> Pairs = new Dictionary<string, string>
                                                              {
                                                                  {"Cooking", "Приготовление"},
                                                                  {"Reserved", "Резерв"},
                                                                  {"Blocked", "Перерыв"},
                                                              };

        public string GetEng(string rus)
        {
            return Pairs.FirstOrDefault(x => x.Value == rus).Key;
        }

        public string GetRus(string eng)
        {
            return Pairs.FirstOrDefault(x => x.Key == eng).Value;
        }
    }
}
