﻿using System;
using LaRenzo.DataRepository.Repositories.Documents.Interface;

namespace LaRenzo.DataRepository.Repositories.Documents
{
    public class DocumentWithDateHelper
    {

        public DateTime GetMinDate(IDocumentWithDate document, IDocumentWithDate oldDocument)
        {
            return oldDocument == null
                ? document.DocumentDate
                : document.DocumentDate < oldDocument.DocumentDate
                    ? document.DocumentDate
                    : oldDocument.DocumentDate;
        }
    }
}
