﻿using System;
using System.Globalization;
using System.Text;

namespace LaRenzo.DataRepository.Repositories.Barcodes
{
    public class Ean13
    {
        private const string BarcodeName = "EAN13";

        private const float FMinimumAllowableScale = .8f;
        private const float FMaximumAllowableScale = 2.0f;

        private const float FWidth = 37.29f;
        private const float FHeight = 25.93f;
        private const float FFontSize = 8.0f;
        private float _fScale = 1.0f;

        private readonly string[] _aOddLeft = { "0001101", "0011001", "0010011", "0111101", 
										  "0100011", "0110001", "0101111", "0111011", 
										  "0110111", "0001011" };

        private readonly string[] _aEvenLeft = { "0100111", "0110011", "0011011", "0100001", 
										   "0011101", "0111001", "0000101", "0010001", 
										   "0001001", "0010111" };

        private readonly string[] _aRight = { "1110010", "1100110", "1101100", "1000010", 
										"1011100", "1001110", "1010000", "1000100", 
										"1001000", "1110100" };

        private const string SQuiteZone = "000000000";

        private const string SLeadTail = "101";

        private const string SSeparator = "01010";

        private string _sCountryCode = "00";
        private string _sChecksumDigit;

        public Ean13()
        {
        }

        public Ean13(string mfgNumber, string productId)
        {
            CountryCode = "00";
            ManufacturerCode = mfgNumber;
            ProductCode = productId;
            CalculateChecksumDigit();
        }

        public Ean13(string countryCode, string mfgNumber, string productId)
        {
            CountryCode = countryCode;
            ManufacturerCode = mfgNumber;
            ProductCode = productId;
            CalculateChecksumDigit();
        }

        public Ean13(string countryCode, string mfgNumber, string productId, string checkDigit)
        {
            CountryCode = countryCode;
            ManufacturerCode = mfgNumber;
            ProductCode = productId;
            ChecksumDigit = checkDigit;
        }

        public void DrawEan13Barcode(System.Drawing.Graphics g, System.Drawing.Point pt)
        {
            float width = Width * Scale;
            float height = Height * Scale;

            float lineWidth = width / 113f;

            System.Drawing.Drawing2D.GraphicsState gs = g.Save();

            g.PageUnit = System.Drawing.GraphicsUnit.Millimeter;

            g.PageScale = 1;

            var brush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);

            float xPosition = 0;

            var strbEan13 = new StringBuilder();
            var sbTemp = new StringBuilder();

            float xStart = pt.X;
            float yStart = pt.Y;

            var font = new System.Drawing.Font("Arial", FFontSize * Scale);

            CalculateChecksumDigit();

            sbTemp.AppendFormat("{0}{1}{2}{3}", CountryCode, ManufacturerCode, ProductCode, ChecksumDigit);

            string sTemp = sbTemp.ToString();

            string sLeftPattern = ConvertLeftPattern(sTemp.Substring(0, 7));

            strbEan13.AppendFormat("{0}{1}{2}{3}{4}{1}{0}",
                                   SQuiteZone, SLeadTail,
                                   sLeftPattern,
                                   SSeparator,
                                   ConvertToDigitPatterns(sTemp.Substring(7), _aRight));

            string sTempUpc = strbEan13.ToString();

            float fTextHeight = g.MeasureString(sTempUpc, font).Height;

            for (int i = 0; i < strbEan13.Length; i++)
            {
                if (sTempUpc.Substring(i, 1) == "1")
                {
                    if (Math.Abs(xStart - pt.X) < 0.01)
                        xStart = xPosition;

                    if ((i > 12 && i < 55) || (i > 57 && i < 101))
                        g.FillRectangle(brush, xPosition, yStart, lineWidth, height - fTextHeight);
                    else
                        g.FillRectangle(brush, xPosition, yStart, lineWidth, height);
                }

                xPosition += lineWidth;
            }

            xPosition = xStart - g.MeasureString(CountryCode.Substring(0, 1), font).Width;
            float yPosition = yStart + (height - fTextHeight);

            g.DrawString(sTemp.Substring(0, 1), font, brush, new System.Drawing.PointF(xPosition, yPosition));

            xPosition += (g.MeasureString(sTemp.Substring(0, 1), font).Width + 43 * lineWidth) -
                (g.MeasureString(sTemp.Substring(1, 6), font).Width);

            g.DrawString(sTemp.Substring(1, 6), font, brush, new System.Drawing.PointF(xPosition, yPosition));

            xPosition += g.MeasureString(sTemp.Substring(1, 6), font).Width + (11 * lineWidth);

            g.DrawString(sTemp.Substring(7), font, brush, new System.Drawing.PointF(xPosition, yPosition));

            g.Restore(gs);
        }

        public System.Drawing.Bitmap CreateBitmap()
        {
            float tempWidth = (Width*Scale)*3.8f;
            float tempHeight = (Height*Scale)*3.8f;

            var bmp = new System.Drawing.Bitmap((int)tempWidth, (int)tempHeight);

            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmp);
            DrawEan13Barcode(g, new System.Drawing.Point(0, 0));
            
            g.Dispose();
            
            return bmp;
        }


        private string ConvertLeftPattern(string sLeft)
        {
            switch (sLeft.Substring(0, 1))
            {
                case "0":
                    return CountryCode0(sLeft.Substring(1));

                case "1":
                    return CountryCode1(sLeft.Substring(1));

                case "2":
                    return CountryCode2(sLeft.Substring(1));

                case "3":
                    return CountryCode3(sLeft.Substring(1));

                case "4":
                    return CountryCode4(sLeft.Substring(1));

                case "5":
                    return CountryCode5(sLeft.Substring(1));

                case "6":
                    return CountryCode6(sLeft.Substring(1));

                case "7":
                    return CountryCode7(sLeft.Substring(1));

                case "8":
                    return CountryCode8(sLeft.Substring(1));

                case "9":
                    return CountryCode9(sLeft.Substring(1));

                default:
                    return "";
            }
        }


        private string CountryCode0(string sLeft)
        {
            return ConvertToDigitPatterns(sLeft, _aOddLeft);
        }


        private string CountryCode1(string sLeft)
        {
            var sReturn = new StringBuilder();
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(0, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(1, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(2, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(3, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(4, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(5, 1), _aEvenLeft));
            return sReturn.ToString();
        }


        private string CountryCode2(string sLeft)
        {
            var sReturn = new StringBuilder();
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(0, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(1, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(2, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(3, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(4, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(5, 1), _aEvenLeft));
            return sReturn.ToString();
        }


        private string CountryCode3(string sLeft)
        {
            var sReturn = new StringBuilder();
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(0, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(1, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(2, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(3, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(4, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(5, 1), _aOddLeft));
            return sReturn.ToString();
        }


        private string CountryCode4(string sLeft)
        {
            var sReturn = new StringBuilder();
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(0, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(1, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(2, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(4, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(5, 1), _aEvenLeft));
            return sReturn.ToString();
        }


        private string CountryCode5(string sLeft)
        {
            var sReturn = new StringBuilder();
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(0, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(1, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(2, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(3, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(4, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(5, 1), _aEvenLeft));
            return sReturn.ToString();
        }


        private string CountryCode6(string sLeft)
        {
            var sReturn = new StringBuilder();
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(0, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(1, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(2, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(3, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(4, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(5, 1), _aOddLeft));
            return sReturn.ToString();
        }


        private string CountryCode7(string sLeft)
        {
            var sReturn = new StringBuilder();
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(0, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(1, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(2, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(3, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(4, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(5, 1), _aEvenLeft));
            return sReturn.ToString();
        }


        private string CountryCode8(string sLeft)
        {
            var sReturn = new StringBuilder();
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(0, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(1, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(2, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(3, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(4, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(5, 1), _aOddLeft));
            return sReturn.ToString();
        }

        private string CountryCode9(string sLeft)
        {
            var sReturn = new StringBuilder();
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(0, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(1, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(2, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(3, 1), _aOddLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(4, 1), _aEvenLeft));
            sReturn.Append(ConvertToDigitPatterns(sLeft.Substring(5, 1), _aOddLeft));
            return sReturn.ToString();
        }

        private string ConvertToDigitPatterns(string inputNumber, string[] patterns)
        {
            var sbTemp = new StringBuilder();
            for (int i = 0; i < inputNumber.Length; i++)
            {
                int iIndex = Convert.ToInt32(inputNumber.Substring(i, 1));
                sbTemp.Append(patterns[iIndex]);
            }
            return sbTemp.ToString();
        }


        public void CalculateChecksumDigit()
        {
            string sTemp = CountryCode + ManufacturerCode + ProductCode;
            int iSum = 0;

            for (int i = sTemp.Length; i >= 1; i--)
            {
                int iDigit = Convert.ToInt32(sTemp.Substring(i - 1, 1));
                if (i % 2 == 0)
                {	
                    iSum += iDigit * 3;
                }
                else
                {	
                    iSum += iDigit * 1;
                }
            }

            int iCheckSum = (10 - (iSum % 10)) % 10;
            ChecksumDigit = iCheckSum.ToString(CultureInfo.InvariantCulture);

        }

        public string Name
        {
            get
            {
                return BarcodeName;
            }
        }

        public float MinimumAllowableScale
        {
            get
            {
                return FMinimumAllowableScale;
            }
        }

        public float MaximumAllowableScale
        {
            get
            {
                return FMaximumAllowableScale;
            }
        }

        public float Width
        {
            get
            {
                return FWidth;
            }
        }

        public float Height
        {
            get
            {
                return FHeight;
            }
        }

        public float FontSize
        {
            get
            {
                return FFontSize;
            }
        }

        public float Scale
        {
            get
            {
                return _fScale;
            }

            set
            {
                if (value < FMinimumAllowableScale || value > FMaximumAllowableScale)
                    throw new Exception("Scale value out of allowable range.  Value must be between " +
                        FMinimumAllowableScale.ToString(CultureInfo.InvariantCulture) + " and " +
                        FMaximumAllowableScale.ToString(CultureInfo.InvariantCulture));
                _fScale = value;
            }
        }

        public string CountryCode
        {
            get
            {
                return _sCountryCode;
            }

            set
            {
                while (value.Length < 2)
                {
                    value = "0" + value;
                }
                _sCountryCode = value;
            }
        }

        public string ManufacturerCode { get; set; }

        public string ProductCode { get; set; }

        public string ChecksumDigit
        {
            get
            {
                return _sChecksumDigit;
            }

            set
            {
                int iValue = Convert.ToInt32(value);
                if (iValue < 0 || iValue > 9)
                    throw new Exception("The Check Digit mst be between 0 and 9.");
                _sChecksumDigit = value;
            }
        }
    }
}
