﻿using System;
using System.ComponentModel;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Users
{
    public class AccessRightInfo: INotifyPropertyChanged
    {
        private bool _allowed;
        
        public int Code { get; set; }
        public int ParentCode { get; set; }
        public string Name { get; set; }
        
        public AccessRightInfo(string name, int code, int parentCode = 0)
        {
            Code = code;
            Name = name;
            ParentCode = parentCode;
            AllowedWithoutAuthoChanges = false;
        }

        public bool Allowed
        {
            get { return _allowed; }
            set
            {
                if (value.Equals(_allowed)) return;
                _allowed = value;
                OnPropertyChanged("Allowed");
                Console.WriteLine("Code {0}, Allowed: {1}", Code, _allowed);
            }
        }

        public bool AllowedWithoutAuthoChanges
        {
            get { return _allowed; }
            set
            {
                if (value.Equals(_allowed)) return;
                _allowed = value;
                OnPropertyChanged("Allowed", true);
                Console.WriteLine("Code*** {0}, Allowed: {1}", Code, _allowed);
            }
        }

        public void SetAllowedWithoutNotification(bool value)
        {
            _allowed = value;
        }

        public event PropertyChangedEventHandler PropertyChanged;


        //[NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName = null, bool onlyUpwardUpdate = false)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new RightPropertyChangedEventArgs(propertyName, onlyUpwardUpdate));
        }

        public class RightPropertyChangedEventArgs : PropertyChangedEventArgs
        {
            public bool OnlyUpwardUpdate;

            public RightPropertyChangedEventArgs(string propertyName, bool onlyUpwardUpdate)
                : base(propertyName)
            {
                OnlyUpwardUpdate = onlyUpwardUpdate;
            }
        }
    }
    
}
