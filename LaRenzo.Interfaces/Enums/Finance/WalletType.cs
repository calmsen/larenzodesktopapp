﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Entities.Finance
{
    public enum WalletType
    {
        Cash,
        Bank,
        Debt,
        Custom
    }
}
