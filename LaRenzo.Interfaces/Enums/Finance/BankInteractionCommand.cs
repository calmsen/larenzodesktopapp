﻿namespace LaRenzo.DataRepository.Entities.Finance
{
    public enum BankInteractionCommand
    {
        Hold = 0,
        ApproveOrder = 1,
        CancelOrder = 2
    }
}
