﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Entities.Catalogs.DiscountCards
{
    [Flags]
    public enum DaysOfWeekEnum
    {
        None = 0,
        Monday = 1,
        Tuesday = 2,
        Wednesday = 4,
        Thursday = 8,
        Friday = 16,
        Weekdays = Monday | Tuesday | Wednesday | Thursday | Friday,
        Saturday = 32,
        Sunday = 64,
        Weekends = Saturday | Sunday,
        All = Weekdays | Weekends
    }
}
