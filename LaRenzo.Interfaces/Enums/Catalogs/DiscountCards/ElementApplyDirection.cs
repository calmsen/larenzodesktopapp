﻿
namespace LaRenzo.DataRepository.Entities.Catalogs.DiscountCards
{  
    public enum ElementApplyDirection
    {
        ElementNeededToApplyRule = 0,
        RuleAppliedToElement = 1
    }
}
