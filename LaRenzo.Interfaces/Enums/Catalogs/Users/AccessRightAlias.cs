﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Users
{
    public enum AccessRightAlias
    {
        DelivaryTab = 311,
        CafeTab = 321,
        SessionButton = 20,
        EditOrder = 313,
        EditOrderStatus = 314,
        EditOrderPaymentType = 315,
        ViewDeliveryOrdersSumCount = 316,
        BlackList = 1372,
        PrintCheckOneMoreTime = 41,
        EditDocuments = 44,
        Popups = 42,
        PreOrderWatchTomorrow = 3171,
        PreOrderCreate = 3172,
        PreOrderEdit = 3173,
        PreOrderRemove = 3174,
        PreOrderRestore = 3175,
        DeliveryOrderPaymentMethod = 318,

    }
}
