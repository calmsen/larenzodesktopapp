﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Entities.Documents.CafeOrders
{
    public enum PaymentType
    {
        /// <summary>
        /// Наличные
        /// </summary>
        Money = 0,

        /// <summary>
        /// Карта
        /// </summary>
        Card = 1,

        /// <summary>
        /// Безналичный расчёт
        /// </summary>
        NoMoney = 2
    }
}
