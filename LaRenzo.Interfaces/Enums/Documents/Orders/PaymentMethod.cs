﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LaRenzo.TransferLibrary
{
    public enum PaymentMethod
    {
        [XmlEnum("")]
        [Description("Не указан")]
        Undefined = 0,
        [XmlEnum("1")]
        [Description("Наличные")]
        Cash = 1,
        [XmlEnum("2")]
        [Description("Онлайн карта")]
        OnlineCard = 2,
        [XmlEnum("3")]
        [Description("Офлайн карта")]
        OfflineCard = 3
    }
}
