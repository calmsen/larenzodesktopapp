﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Sessions
{
    /// <summary> Результат попытки закрыть сессию </summary>
    public enum CloseSessionResult
    {
        Closed,
        OrdersOpened,
        CafeOrdersOpened,
        WebOrdersInProcessing
    }
}
