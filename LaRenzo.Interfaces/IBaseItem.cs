﻿using System;

namespace LaRenzo.DataRepository.Repositories.Base
{
	public interface IBaseItem : IBaseBranchItem
	{
		int ID { get; set; }

	    string AuthorModified { get; set; }
        
	    DateTime? DateModified { get; set; }
	}
}
