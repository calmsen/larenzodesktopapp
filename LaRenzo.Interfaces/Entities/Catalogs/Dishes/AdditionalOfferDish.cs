﻿using LaRenzo.DataRepository.Repositories.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Entities.Catalogs.Dishes
{
    public class AdditionalOfferDish : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public int BranchId { get; set; }
        public int DishId { get; set; }
        public Dish Dish { get; set; }
        public int MainDishId { get; set; }
        public Dish MainDish { get; set; }

        [NotMapped]
        public string Name
        {
            get => _name ?? Dish?.Name ?? string.Empty;
            set => _name = value;
        }

        private string _name;
    }
}
