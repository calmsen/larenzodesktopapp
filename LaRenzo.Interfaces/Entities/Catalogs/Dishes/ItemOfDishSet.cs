﻿using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Entities.Catalogs.Products;

namespace LaRenzo.DataRepository.Entities.Catalogs.Dishes
{
    // ReSharper disable InconsistentNaming
    public class ItemOfDishSet : Entity
    {
        [ForeignKey("Dish")]
        public int DishId { get; set; }

        public Dish Dish { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }

        public Product Product { get; set; }

        [ForeignKey("Measure")]
        public int MeasureId { get; set; }

        public Measure Measure { get; set; }

        public decimal Amount { get; set; }

        public new int BranchId { set; get; }
        
        [NotMapped]
        public string Comment{ get; set; }

        [NotMapped]
        public string Name{ get; set; }
    }
}
