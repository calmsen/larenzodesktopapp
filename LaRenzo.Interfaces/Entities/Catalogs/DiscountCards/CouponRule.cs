﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.DiscountCards
{
    public class CouponRule: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public int BranchId { set; get; }
        public int BrandId { get; set; }

        public string Name { get; set; }
        public bool AllDishRequired { get; set; }
        public bool ApplyToAllDishes { get; set; }

        public int? Price { get; set; }
        public int? Percent { get; set; }
        public int? FixDiscount { get; set; }
        public int? GiftDish { get; set; }
        public int? SumToApply { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
