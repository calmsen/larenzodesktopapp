﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.DiscountCards
{
    public class Coupon : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public int BranchId { set; get; }
        public int CouponRuleId { get; set; }
        public CouponRule CouponRule { get; set; }
        public int ActivationCount { get; set; }
        public int ActivatedCount { get; set; }
        public bool Activated { get; set; }
        public string Code { get; set; }
    }
}
