﻿using System;

namespace LaRenzo.DataRepository.Entities.Catalogs.BlackList
{
    public class BlackListItem : Entity
    {
        public string Name { get; set; }
        public string Street { get; set; }

        public string House { get; set; }

        public string Appartment { get; set; }

        public string Phone { get; set; }

        public string Reason { get; set; }

        public DateTime Created { get; set; }

        public int BranchId { set; get; }

        public string Comment { get; set; }
    }
}
