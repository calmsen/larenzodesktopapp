﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Addresses
{
    public class Address : IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public string Name { set; get; }

        [ForeignKey("AddressGroup")]
        public int? AddressGroupID { set; get; }

        public AddressGroup AddressGroup { get; set; }

        public int BranchId { set; get; }


        public override string ToString()
        {
            return Name;
        }
    }
}