﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
namespace LaRenzo.DataRepository.Entities.Catalogs.Addresses
{
    // ReSharper disable InconsistentNaming
    public class AddressGroup : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public string Name { get; set; }

        public int BranchId { set; get; }
    }
}
