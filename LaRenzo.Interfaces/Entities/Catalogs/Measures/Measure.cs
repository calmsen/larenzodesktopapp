﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaRenzo.DataRepository.Entities.Catalogs.Measures
{
    public class Measure: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string ForeignName { get; set; }

        [ForeignKey("ParentMeasure")]
        public int? ParentMeasureID { get; set; }

        public Measure ParentMeasure { get; set; }

        public int ParentMeasureValue { get; set; }

        public bool IsBaseMeasure { get; set; }
        public int BranchId { set; get; }
    }
}
