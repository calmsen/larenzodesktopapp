﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaRenzo.DataRepository.Entities.Catalogs.Sets
{
    public class SetParams: IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("Set")]
        public int SetID { set; get; }
        
        public Set Set { set; get; }

        public int MinValue { set; get; }

        public int MaxValue { set; get; }

        public int DishValue { set; get; }

        public bool IsIncremental { set; get; }

        public int IncrementValue { set; get; }

        public bool Round { set; get; }

        public int RoundDirection { set; get; }
        public int BranchId { set; get; }
    }
}
