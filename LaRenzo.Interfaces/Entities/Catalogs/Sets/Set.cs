﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Sets
{
    public class Set: IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public string Name { set; get; }
        public int BranchId { set; get; }
    }
}
