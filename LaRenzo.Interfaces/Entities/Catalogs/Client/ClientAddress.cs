﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Client
{
    public class ClientAddress: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [Required]
        [ForeignKey("Client")]
        public int ClientId { get; set; }
        public Client Client { get; set; }

        public string Street { get; set; }
        
        public string House { get; set; }
        
        public string Appartament { get; set; }
        
        public string Porch { get; set; }
        
        public string Additional { get; set; }

        public int BranchId { set; get; }
    }
}
