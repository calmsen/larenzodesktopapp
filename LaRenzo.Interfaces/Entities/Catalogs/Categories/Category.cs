﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Settings;

namespace LaRenzo.DataRepository.Entities.Catalogs.Categories
{
    // ReSharper disable InconsistentNaming
    public class Category : Entity
    {
        [NotMapped]
        public string Comment{ get; set; }

        [Required]
        public string Name{ get; set; }

        [ForeignKey("Section")]
        public int SectionID { get; set; }

        public int Weight { get; set; }

        public Section Section { get; set; }

        /// <summary> Id бренда </summary>
        [ForeignKey("Brand")]
        public int Brand_ID { get; set; }

        /// <summary> Id бренда </summary>
        public int IdBrand { get; set; }

        /// <summary> Бренд </summary>
        public Brand Brand { get; set; }
    }
}
