﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.ComponentModel.DataAnnotations;

namespace LaRenzo.DataRepository.Entities.Catalogs.Roles
{
    public class Role: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [Required]
        public string Name { get; set; }
        public int BranchId { set; get; }

    }
}
