﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Catalogs.Products
{
    public class ProductCategory: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public string Name { get; set; }

        [Required]
        public int ParentCategoryID { get; set; }

        [ForeignKey("DefaultMeasure")]
        public int DefaultMeasureID { get; set; }

        public Measure DefaultMeasure { get; set; }
        public int BranchId { set; get; }
    }
}
