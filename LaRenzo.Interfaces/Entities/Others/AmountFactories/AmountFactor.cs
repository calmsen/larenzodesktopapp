﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaRenzo.DataRepository.Entities.Others.AmountFactories
{
    public class AmountFactor : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public decimal Amount { get; set; }

        public float Factor { get; set; }

        [ForeignKey("Parent")]
        public int ParentID { get; set; }

        public AmountFactorCollection Parent { get; set; }

        public int BranchId { set; get; }
    }
}
