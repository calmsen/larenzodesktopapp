﻿using LaRenzo.DataRepository.Repositories.Base;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Settings;

namespace LaRenzo.DataRepository.Entities.Others.TimeSheets
{
    public class TimeSheet : IEquatable<TimeSheet>, IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [Key, Column(Order = 0)]
        public DateTime Time { get; set; }

        [Key, Column(Order = 1)]
        public int SectionID { get; set; }

        public string Status { get; set; }

        public Guid? ReservationGuid { get; set; }

        public DateTime? ReservationDate { get; set; }

        public PointOfSale PointOfSale { get; set; }
        [Key, Column(Order = 3)]
        public int PointOfSaleId { get; set; }

        public Brand Brand { get; set; }
        [Key, Column(Order = 4)]
        public int BrandId { get; set; }

        [Key, Column(Order = 2)]
        public int BranchId { set; get; }
        public bool Equals(TimeSheet other)
        {
            return Time == other.Time
                   && SectionID == other.SectionID
                   && Status == other.Status
                   && ReservationGuid == other.ReservationGuid
                   && ReservationDate == other.ReservationDate
                   && BranchId == other.BranchId
                && PointOfSaleId == other.PointOfSaleId
                && BrandId == other.BrandId;
        }
    }


}
