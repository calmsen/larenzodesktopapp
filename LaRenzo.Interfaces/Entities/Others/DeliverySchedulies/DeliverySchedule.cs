﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Others.DeliverySchedulies
{
    public class DeliverySchedule : IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public int DayOfWeekID { set; get; }

        [ForeignKey("AddressGroup")]
        public int? AddressGroupID { set; get; }

        public virtual AddressGroup AddressGroup { get; set; }

        public int? PointOfSaleId { get; set; }
        public PointOfSale PointOfSale { get; set; }

        public int Priority { get; set; }

        public TimeSpan DeliveryBegin { set; get; }

        public TimeSpan DeliveryEnd { set; get; }

        public TimeSpan DeliveryTime { set; get; }

        public int BranchId { set; get; }
    }
}
