﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.Collections.Generic;
using System.Linq;

namespace LaRenzo.DataRepository.Entities.Finance
{
    public class Wallet: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public string Name { get; set; }
        public WalletType Type { get; set; }

        public List<Payment> Payments { get; set; }

        public decimal Total
        {
            get
            {
                return Payments != null && Payments.Any() ? Payments.Sum(x => x.Total) : 0;
            }
        }

        public int BranchId { set; get; }

    }
    
}
