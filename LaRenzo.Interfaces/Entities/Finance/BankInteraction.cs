﻿using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Finance
{
    public class BankInteraction: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }
        public int UserID { get; set; }

        public Guid OrderGuid { get; set; }

        public DateTime CommandCreated { get; set; }
        public DateTime? CommandSent { get; set; }
        public string Comment { get; set; }

        public BankInteractionStatus Status { get; set; }
        public BankInteractionCommand Command { get; set; }

        public string BankResponse { get; set; }

        public int BranchId { set; get; }
    }

}
