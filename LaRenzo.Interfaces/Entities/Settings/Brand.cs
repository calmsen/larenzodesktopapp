﻿
using System;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Settings
{
    public class Brand : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public string Name { get; set; }

        public string SiteAddress { get; set; }

        public string Check { get; set; }

        public int BranchId { set; get; }
    }
}
