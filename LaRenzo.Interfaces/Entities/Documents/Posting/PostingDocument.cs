﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Base;
using LaRenzo.DataRepository.Repositories.Documents.Interface;

namespace LaRenzo.DataRepository.Entities.Documents.Posting
{
    public class PostingDocument : IDocumentWithDate, IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public DateTime DocumentDate { set; get; }

        [ForeignKey("Warehouse")]
        public int WarehouseID { set; get; }

        public Warehouse Warehouse { set; get; }

        public double DocumentSum { set; get; }

        public bool IsProcessed { set; get; }

        public bool IsMarkToDelete { set; get; }

        public string User { set; get; }

        public string Note { set; get; }
        public int BranchId { set; get; }
    }
}
