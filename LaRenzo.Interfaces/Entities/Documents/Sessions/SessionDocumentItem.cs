﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Documents.Sessions
{
    public class SessionDocumentItem:IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("Session")]
        public int SessionID { set; get; }

        public Session Session { set; get; }

        [ForeignKey("Dish")]
        public int DishID { set; get; }

        public Dish Dish { set; get; }

        public int Amount { set; get; }

        public double Sum { set; get; }/// <summary> id брэнда </summary>
		public int IdBrand { set; get; }

		/// <summary> тип оплаты  1-нал, 2-оплата какой-то картой, 3-со счёта на счёт </summary>
		public int PayTypeIndex { set; get; }
        
        public int BranchId { set; get; }
    }
}