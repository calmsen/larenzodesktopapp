﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories.Base;
using LaRenzo.TransferLibrary;

namespace LaRenzo.DataRepository.Entities.Documents.WebOrders
{
    public class WebOrder : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string Time { get; set; }

        /// <summary> Источник заказа </summary>
        public string Source { get; set; }
        
        /// <summary>
        /// Комментарий к заказу
        /// </summary>
        public string Comment { get; set; }

        public int WebOrderId { get; set; }

        public Guid Guid { get; set; }

        public int IsNewSiteOrder { get; set; }
        
        [NotMapped]
        public int BrandId { get; set; }

        public bool IsCallback { get; set; }

        public bool IsPaid { get; set; }

        public int DeliveryMethod { get; set; }

        /// <summary> Дата создания заявки </summary>
        public DateTime CreateDate { get; set; }

        public string DeliveryMethodName
        {
            get
            {
                string retValue = "";
                switch (DeliveryMethod)
                {
                    case 1:
                        retValue = "Cамовывоз";
                        break;
                    case 2:
                        retValue = "Курьер";
                        break;
                }

                return retValue;
            }
        }

        [DefaultValue("Acepted")]
        public string Status { get; set; }

        public ICollection<WebOrderItem> Items { get; set; }
        public PaymentMethod PaymentMethod { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();

            if (IsCallback)
            {
                result.Append("ЗАКАЗАЛИ ОБРАТНЫЙ ЗВОНОК");
                result.Append(Environment.NewLine);
                result.Append(Environment.NewLine);
            }
            if (BrandId == 1)
            {
                result.Append(string.Format("Заказ сделан с сайта {0}", IsNewSiteOrder == 1
                                                                            ? "http://larenzo.ru/"
                                                                            : IsNewSiteOrder == 2
                                                                                  ? "http://new.larenzo.ru/"
                                                                                  : "http://www.pizzalarenzo.ru/"));
            }
            else if (BrandId == 2)
            {
                result.Append("Заказ сделан с сайта http://pizzarambo.ru/");
            }
            result.Append(Environment.NewLine);
            result.Append(@"Имя: ");
            result.Append(Name);
            result.Append(Environment.NewLine);

            result.Append(@"Телефон:");
            result.Append(Phone);
            result.Append(Environment.NewLine);

            result.Append(@"Адрес: ");
            result.Append(Address);
            result.Append(Environment.NewLine);

            result.Append(@"Приготовить заказ к: ");
            result.Append(Time);
            result.Append(Environment.NewLine);
            result.Append(Environment.NewLine);
            result.Append("*********************");
            result.Append(Environment.NewLine);

            foreach (var item in Items)
            {
                result.Append(item.Name);
                result.Append(" > ");
                result.Append(item.Amount);
                result.Append("шт");
                result.Append(Environment.NewLine);
            }
            return result.ToString();

        }

        /// <summary> Id бренда </summary>
        public int IdBrand { get; set; }

        /// <summary>
        /// Номер скидочной картыы
        /// </summary>
        public string DiscountCard { get; set; }

        /// <summary> Бренд </summary>
        public Brand Brand { get; set; }

        public int BranchId { set; get; }
        
        public string CouponCode { get; set; }
    }

}
