﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaRenzo.DataRepository.Entities.Documents.WebOrders
{
    public class WebOrderItem: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public int SKU { get; set; }

        public string Name { get; set; }

        public int Amount { get; set; }

        [ForeignKey("WebOrder")]
        public int WebOrderID { get; set; }

        public WebOrder WebOrder { get; set; }
        public int BranchId { set; get; }
    }
}
