﻿using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;

namespace LaRenzo.DataRepository.Entities.Documents.CafeOrders
{
    // ReSharper disable InconsistentNaming
    public class CafeOrderDocumentItem : Entity
    {
        [NotMapped]
        public string Name{ get; set; }

        [NotMapped]
        public string Comment{ get; set; }

        [ForeignKey("Dish")]
        public int DishID { get; set; }

        public Dish Dish { get; set; }

        public int Amount { get; set; }

        public int Price { get; set; }

        public int Sum { get; set; }

        public bool IsEditable { get; set; }

        [ForeignKey("CafeOrderDocument")]
        public int CafeOrderDocumentID { get; set; }

        public CafeOrderDocument CafeOrderDocument { set; get; }

        public new int BranchId { set; get; }
    }
}
