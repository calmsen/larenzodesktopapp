﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Documents.Inventories
{
    public class InventoryDocumentItem : IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [ForeignKey("InventoryDocument")]
        public int InventoryDocumentID { set; get; }

        public InventoryDocument InventoryDocument { set; get; }

        [ForeignKey("Product")]
        public int ProductID { set; get; }

        public Product Product { set; get; }

        public decimal Amount { set; get; }

        public decimal AmountFact { set; get; }

        public decimal Delta { set; get; }

        [NotMapped]
        public double Price { set; get; }

        [NotMapped]
        public decimal DeltaFin { set; get; }

        [NotMapped]
        public decimal AmmountFactFin { set; get; }
        public int BranchId { set; get; }
    }
}