﻿using LaRenzo.DataRepository.Repositories.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaRenzo.DataRepository.Entities.Documents.DeliveryOrders
{
    public class PreOrder : IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public DateTime ReservationDateTime { get; set; }

        public bool IsDeleted { get; set; }

        [ForeignKey("Order")]
        public int OrderID { get; set; }

        public Order Order { get; set; }

        public int BranchId { set; get; }
    }
}
