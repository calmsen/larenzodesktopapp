﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Client;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories.Base;
using LaRenzo.TransferLibrary;

namespace LaRenzo.DataRepository.Entities.Documents.DeliveryOrders
{
    public class Order: IBaseItem, IBaseBranchItem
    {
        public int ID { get; set; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        [MaxLength(7)]
        public string Code { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(50)]
        [Required]
        public string Phone { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime TimeClient { get; set; }
        
        public DateTime? ChangeToOnWay { get; set; }

        public DateTime? ChangeToDelivered { get; set; }

        public DateTime? ChangeToPaid { get; set; }

        public DateTime? ChangeToFailure { get; set; }

        [Required]
        public DateTime TimeKitchen { get; set; }
        public string User { get; set; }

        /// <summary> Источник  </summary>
        public string Source { get; set; }

        /// <summary>
        /// Цена заказа на момент закрытия. Вычислить может быть невозможно, так как цены могут меняться, а для сатистики важны реальные цены
        /// </summary>
        public int TotalCost { get; set; }

        /// <summary>
        /// Цена заказа на момент закрытия без учета скидки.
        /// </summary>
        public int TotalCostWithoutDiscount { get; set; }

        /// <summary>
        /// Название устарело. Флаг, указывающий что чек был уже как минимум один раз разпечатан
        /// </summary>
        public bool DoPrintedByOperator { get; set; }

        /// <summary>
        /// Название устарело. Флаг, указывающий есть ли доставка в заказе
        /// </summary>
        public bool DoCalculateDelivery { get; set; }

        public bool IsWindow { get; set; }

        public bool IsOnlinePayment { get; set; }

        public bool IsPaid { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public Guid? WebOrderGuid { get; set; }

        /// <summary>
        /// Состояние списания (частичное / полное / etc)
        /// </summary>
        public int CreditStatus { get; set; }

        [ForeignKey("State")]
        public int StateID { get; set; }
        /*public**virtual*/
        public OrderState State { get; set; }

        [ForeignKey("Driver")]
        public int? DriverID { get; set; }
        /*public**virtual*/
        public Driver Driver { get; set; }


        [ForeignKey("Session")]
        public int? SessionID { get; set; }
        public Session Session { get; set; }

        [DisplayName("Список блюд")]
        public ICollection<OrderItem> OrderItems { get; set; }

        public bool IsWebRequest { set; get; }

        #region Address


        [MaxLength(100)]
        public string Street { get; set; }

        [MaxLength(20)]
        public string House { get; set; }
        public string Appartament { get; set; }
        public string Porch { get; set; }
        [MaxLength(255)]
        public string Additional { get; set; }

        public int WebOrderId { get; set; }

        #endregion

        [ForeignKey("DiscountCard")]
        public int? DiscountCardID { get; set; }
        public DiscountCard DiscountCard { get; set; }

        [ForeignKey("Coupon")]
        public int? CouponId { get; set; }
        public Coupon Coupon { get; set; }

        public Guid? ReservationGuid { get; set; }
        public int OrderReserveType { get; set; }

        [NotMapped]
        public OrderReserveTypes ReserveType
        {
            get { return (OrderReserveTypes)OrderReserveType; }
            set { OrderReserveType = (int)value; }
        }

        /// <summary> Id бренда </summary>
        public int IdBrand { get; set; }

        /// <summary> Бренд </summary>
        public Brand Brand { get; set; }

        public bool Is5k { get; set; }

        /// <summary>  </summary>
        public bool IsUnCash { get; set; }

        /// <summary> Клиент оплатил заказ </summary>
        public bool IsClientPaid { get; set; }

        /// <summary> Количество персон </summary>
        public int PersonsCount { get; set; }

        /// <summary> Быстрая доставка </summary>
        public bool IsFastDriver { get; set; }

        /// <summary> Ближйшее время </summary>
        public bool IsNearTime { get; set; }

        public int BranchId { set; get; }
        /// <summary>
        /// Признак того, что фискальный чек напечатан
        /// </summary>
        public bool FiscalCheckPrinted { set; get; }

        /// <summary>
        /// Признак того, что смс клиенту отправлена
        /// </summary>
        public bool SmsSended { set; get; }

        /// <summary>
        /// Связь с клиентом
        /// </summary>
        [ForeignKey("Client")]
        public int? ClientId { set; get; }

        public Client Client { get; set; }

        public PointOfSale PointOfSale { get; set; }

        public int PointOfSaleId { get;set; }
        public string FailureReason { set; get; }
    }
}
