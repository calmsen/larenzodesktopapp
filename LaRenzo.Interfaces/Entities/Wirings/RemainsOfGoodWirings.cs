﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Documents.Inventories;
using LaRenzo.DataRepository.Entities.Documents.Moving;
using LaRenzo.DataRepository.Entities.Documents.Posting;
using LaRenzo.DataRepository.Entities.Documents.Returns;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Entities.Documents.Supply;
using LaRenzo.DataRepository.Entities.Documents.Writeoff;
using LaRenzo.DataRepository.Entities.Documents.WriteoffDish;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Wirings
{
    public class RemainsOfGoodWirings : IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public DateTime Date { set; get; }

        [ForeignKey("Product")]
        public int ProductID { set; get; }

        public Product Product { set; get; }

        public decimal Amount { set; get; }

        [ForeignKey("Warehouse")]
        public int WarehouseID { set; get; }

        public Warehouse Warehouse { set; get; }

        [ForeignKey("SupplyDocument")]
        public int? SupplyDocumentID { set; get; }

        public SupplyDocument SupplyDocument { set; get; }

        [ForeignKey("ReturnDocument")]
        public int? ReturnDocumentID { set; get; }

        public ReturnDocument ReturnDocument { set; get; }

        [ForeignKey("MovingDocument")]
        public int? MovingDocumentID { set; get; }

        public MovingDocument MovingDocument { set; get; }

        [ForeignKey("WriteoffDocument")]
        public int? WriteoffDocumentID { set; get; }

        public WriteoffDocument WriteoffDocument { set; get; }

        [ForeignKey("PostingDocument")]
        public int? PostingDocumentID { set; get; }

        public PostingDocument PostingDocument { set; get; }

        [ForeignKey("Session")]
        public int? SessionDocumentID { set; get; }

        public Session Session { set; get; }

        [ForeignKey("InventoryDocument")]
        public int? InventoryDocumentID { set; get; }

        public InventoryDocument InventoryDocument { set; get; }

        [ForeignKey("WriteoffDishDocument")]
        public int? WriteoffDishDocumentID { set; get; }

        public WriteoffDishDocument WriteoffDishDocument { set; get; }

        public int? IntermediateGoodWiringsId { set; get; }

        public IntermediateGoodWirings IntermediateGoodWirings { set; get; }

        public int BranchId { set; get; }
    }
}
