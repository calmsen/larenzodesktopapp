﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaRenzo.DataRepository.Entities.Branch
{
    /// <summary>
    /// Филиал
    /// </summary>
    public class Branch : IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        /// <summary>
        /// Название филиала
        /// </summary>
        public string BranchName { get; set; }

        /// <summary>
        /// Поле по сути дублирующее ID, добавлено для обнообразия
        /// </summary>        
        [NotMapped]
        public int BranchId { get; set; }
    }
}
