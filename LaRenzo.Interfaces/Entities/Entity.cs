﻿using System;
using LaRenzo.DataRepository.Repositories.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace LaRenzo.DataRepository.Entities
{
    // ReSharper disable InconsistentNaming
    public abstract class Entity : IBaseItem, IBaseBranchItem
    {
        protected Entity()
        {
            ID = -1;
        }
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        public int BranchId { get; set; }

        [NotMapped]
        public bool IsNew
        {
            get { return ID == -1; }
        }

        [NotMapped]
        public bool IsEdit { set; get; } = true;
    }
}
