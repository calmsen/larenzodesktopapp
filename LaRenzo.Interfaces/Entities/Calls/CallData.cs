﻿using System;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Base;

namespace LaRenzo.DataRepository.Entities.Calls
{
    public class CallData : IBaseItem, IBaseBranchItem
    {
        public int ID { set; get; }
        public string AuthorModified { get; set; }
        public DateTime? DateModified { get; set; }

        /// <summary> Номер телефона </summary>
        public string PhoneNumber { get; set; }

        /// <summary> Id вызова </summary>
        public string IdCall { get; set; }

        /// <summary> Имя </summary>
        public string Name { get; set; }

        /// <summary> Создан </summary>
        public DateTime Created { get; set; }

        /// <summary> Ответи </summary>
        public DateTime Answed { get; set; }

        /// <summary> Отключились </summary>
        public DateTime Droped { get; set; }

        /// <summary> Не отвечен </summary>
        public bool IsUnAnswed { get; set; }

        /// <summary> Сессия звонка </summary>
        public Session Session { get; set; }

        /// <summary> Принявший пользователь </summary>
        public User User { get; set; }

        /// <summary> Звонок сейчас на линии </summary>
        public bool IsOnline { get; set; }

        /// <summary>  </summary>
        public bool IsEnter { get; set; }

        /// <summary> Название статуса </summary>
        public string StatusName { get; set; }

        /// <summary> Является VIP </summary>
        public bool IsVip { get; set; }
        
        public int BranchId { set; get; }
    }
}
