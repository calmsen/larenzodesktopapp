﻿using System;

namespace LaRenzo.DataRepository.Repositories.Call
{
	public class CallEvent : EventArgs
	{
		public LaRenzo.DataRepository.Repositories.Call.CallInfo CurentCall;

		public CallEvent(CallInfo curentCall)
		{
			CurentCall = curentCall;
		}
	}
}
