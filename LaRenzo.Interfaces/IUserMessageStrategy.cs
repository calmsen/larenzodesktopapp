﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.Interfaces
{
    public interface IUserMessageStrategy
    {
        void Show(string message);
        void Show(string message, Exception innerException);
    }
}
