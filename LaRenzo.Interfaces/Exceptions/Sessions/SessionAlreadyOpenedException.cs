﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Sessions
{
    public class SessionAlreadyOpenedException : Exception
    {
        private const string Msg = "В базе данных имеется открытая сессия. Попробуйте перезагрузить программу, если вы ее не видите.";

        public SessionAlreadyOpenedException() : base(Msg)
        {
        }

        public SessionAlreadyOpenedException(Exception innerException)
            : base(Msg, innerException)
        {
        }
    }
}
