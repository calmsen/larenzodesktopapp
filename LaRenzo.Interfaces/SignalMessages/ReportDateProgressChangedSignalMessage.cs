﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.Interfaces.SignalMessages
{
    public class ReportDateProgressChangedSignalMessage
    {
        public int Percentage { get; set; }
        public Guid Guid { get; set; }
    }
}
