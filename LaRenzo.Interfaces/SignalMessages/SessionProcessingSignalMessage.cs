﻿using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.Interfaces.SignalMessages
{
    public class SessionProcessingSignalMessage
    {
        public int TotalOrders { get; set; }
        public int OrdersProcessed { get; set; }
        public int TotalCafeOrders { get; set; }
        public int CafeOrdersProcessed { get; set; }
        public SessionProcessingStatus Status { get; set; }
    }
}
