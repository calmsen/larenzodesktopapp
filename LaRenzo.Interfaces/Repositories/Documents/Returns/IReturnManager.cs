﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.Returns;

namespace LaRenzo.DataRepository.Repositories.Documents.Returns
{
    public interface IReturnManager
    {
        int AddReturnDocument(ReturnDocument document);
        void DeleteReturnDocument(ReturnDocument document);
        ReturnDocument GetReturnDocument(int returnDocId);
        List<ReturnDocumentItem> GetReturnDocumentItems(int returnDocId);
        List<ReturnDocument> GetReturnDocuments();
        void UpdateReturnDocument(ReturnData returnData);
    }
}