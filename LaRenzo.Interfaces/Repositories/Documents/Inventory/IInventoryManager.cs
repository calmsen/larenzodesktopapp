﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.Inventories;

namespace LaRenzo.DataRepository.Repositories.Documents.Inventory
{
    public interface IInventoryManager
    {
        int AddInventoryDocument(InventoryDocument document);
        void DeleteInventoryDocument(InventoryDocument document);
        List<InventoryDocumentItem> FillInventoryItems(int warehouseId, DateTime docDate);
        InventoryDocument GetInventoryDocument(int inventoryId);
        List<InventoryDocumentItem> GetInventoryDocumentItems(int inventoryId);
        object GetInventoryDocuments();
        InventoryDocument MarkInventoryDocumentRecalculate(int warehouseId, DateTime changesDate, string reasonForNotify = null);
        
        void UpdateInventoryDocument(InventoryData inventoryData);
    }
}