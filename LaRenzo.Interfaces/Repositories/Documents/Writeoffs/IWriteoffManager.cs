﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.Writeoff;

namespace LaRenzo.DataRepository.Repositories.Documents.Writeoffs
{
    public interface IWriteoffManager
    {
        int AddWriteoffDocument(WriteoffDocument document);
        void DeleteWriteoffDocument(WriteoffDocument document);
        WriteoffDocument GetWriteoffDocument(int writeoffDocId);
        List<WriteoffDocumentItem> GetWriteoffDocumentItems(int writeoffDocumentId);
        List<WriteoffDocument> GetWriteoffDocuments();
        void UpdateWriteoffDocument(WriteoffData writeoffData);
    }
}