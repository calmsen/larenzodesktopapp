﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Documents.WebOrders;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.TransferLibrary;

namespace LaRenzo.DataRepository.Repositories.Documents.WebOrders
{
    public interface IWebOrderManager
    {
        void AddMultiple(List<WebOrder> list, int selectedBranchId);
        void ChangePaymentMethod(int id, PaymentMethod method);
        string CheckIfOrderCanBeProcessed(WebOrder order, int brandId, string siteProtocol);
        WebOrder GetById(int id);
        List<WebOrder> GetUnhandledWebOrders(bool onlyNewWebOrders);
        int GetUnhandledWebOrdersCount();
        Task<GrabOrdersResult> GrabOrders(string site, int brandId, string siteProtocol, int selectedBranchId);
        Task SetOrderProcessed(WebOrder order, int brandId, string siteProtocol);
        void SetOrderStatus(int id, string status);
        void StartProcessing(WebOrder order);
        void StopProcessing(WebOrder order);
        string ToString(WebOrder order);
    }
}