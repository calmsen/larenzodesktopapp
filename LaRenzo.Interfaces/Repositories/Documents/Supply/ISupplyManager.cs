﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.Supply;

namespace LaRenzo.DataRepository.Repositories.Documents.Supply
{
    public interface ISupplyManager
    {
        int AddSupplyDocument(SupplyDocument document);
        void DeleteSupplyDocument(SupplyDocument document);
        SupplyDocument GetSupplyDocument(int supplyDocId);
        List<SupplyDocumentItem> GetSupplyDocumentItems(int supplyId);
        List<SupplyDocument> GetSupplyDocuments(int countOfRows = 100, bool notPaid = false);
        void UpdateSupplyDocument(SupplyData supplyData);
    }
}