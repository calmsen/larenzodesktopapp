﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;

namespace LaRenzo.DataRepository.Repositories.Documents.Orders
{
    public interface IPreOrderManager
    {
        void Create(int orderId, DateTime date);
        List<PreOrder> GetFullList();
        List<PreOrder> GetList(DateTime date, bool takeAllAfterDate = false);
        List<PreOrderProduct> GetNeededProductList(DateTime date, bool isChecked);
        void Remove(int id);
        void Restore(int id);
        void Update(PreOrder preOrder);
    }
}