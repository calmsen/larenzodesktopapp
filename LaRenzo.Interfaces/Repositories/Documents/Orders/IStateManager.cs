﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;

namespace LaRenzo.DataRepository.Repositories.Documents.Orders
{
    public interface IStateManager
    {
        OrderState GetByName(string name);
        List<OrderState> GetList();
    }
}