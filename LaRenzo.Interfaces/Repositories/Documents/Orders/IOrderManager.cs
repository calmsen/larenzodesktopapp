﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Call;
using LaRenzo.TransferLibrary;

namespace LaRenzo.DataRepository.Repositories.Documents.Orders
{
    /// <summary>
    /// TODO: Problem
    /// </summary>
    public interface IOrderManager
    {
        void Add(Order order, List<OrderItem> items);
        Task<decimal> ApplyCoupon(decimal totalCost, Coupon coupon, IEnumerable<ICouponDish> ordeItems);
        decimal ApplyCouponSync(decimal totalCost, Coupon coupon, IEnumerable<ICouponDish> ordeItems);
        void ChangePaymentType(int orderId, PaymentMethod method);
        void ChangePrintToNalogPrinterStatus(int orderId, bool status);
        void ChangeSmsSendedStatus(int orderId, bool status);
        bool DoCodeAlreadyExists(string uid, int selectedBranchId);
        List<Order> GetAllOrdersInSession(int sessionId);
        Order GetByGuid(Guid guid);
        Order GetById(int id, bool includeAll = true);
        List<Order> GetByPeriod(DateTime begin, DateTime end, bool web);
        CouponDiscount GetCouponDiscountForCurrentTime(decimal totalCost, int couponRuleId);
        Task<List<Order>> GetCurrentSessionList(int? branchId = null, int? brandId = null);
        Task<int> GetDeliveryPrice(ICollection<OrderItem> items, int brandId, DiscountCard card = null, Coupon coupon = null);
        decimal GetDiscountRate(int discount);
        Task<List<DishFavorite>> GetDisheByFavorite(List<Order> lastOrders);
        List<Order> GetFailureOrders(DateTime dateFrom, DateTime dateTo);
        List<Order> GetFullList();
        List<OrderItem> GetItemList(int id);
        List<Order> GetLastN(int n = 1000);
        List<Order> GetListForDriver(int driverID, int sessionID);
        List<Session> GetAllExistedSessionsForDriver(int driverID);
        List<Order> GetListOfAllOrdersInSession(int sessionId);
        OrderItem[] GetOrderItemsArray(Order order, Section section = null);
        List<OrderItem> GetOrderItemsArrayByKitchen(List<CafeOrderDocumentItem> cafeOrderDocumentItemsList, Section section);
        List<Order> GetOrdersByCouponRuleId(int couponRuleId);
        Task<List<Order>> GetOrdersByPhone(string phoneNumber, bool isCurentSession = false);
        List<Order> GetSessionInfo(int sessionId);
        List<StatsOrderCount> GetStatsList();
        IEnumerable<ICouponDish> MapOrderItemsToDishes(IEnumerable<OrderItem> orderItems);
        void Remove(int id);
        string RuleToString(CouponRule couponRule);
        void SetNewDriver(int orderId, Driver driver);
        void SetOrderStateToDelivered(int orderId);
        void Update(Order order);
        void Update(Order order, List<OrderItem> items);
        void UpdatePreOrderTime(int id, DateTime dateTime);
        string ValidateOrder(Order order);
    }
}