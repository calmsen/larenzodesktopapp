﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.WriteoffDish;

namespace LaRenzo.DataRepository.Repositories.Documents.WriteoffDish
{
    public interface IWriteoffDishManager
    {

        int AddWriteoffDishDocument(WriteoffDishDocument document);
        void DeleteWriteoffDocument(WriteoffDishDocument document);
        WriteoffDishDocument GetWriteoffDishDocument(int writeoffDishId);
        List<WriteoffDishDocument> GetWriteoffDishDocuments();
        List<WriteoffDishDocumentItem> GetWriteoffDocumentItems(int writeoffDishDocumentId);
        void UpdateWriteoffDishDocument(WriteoffDishData writeoffDishData);
    }
}