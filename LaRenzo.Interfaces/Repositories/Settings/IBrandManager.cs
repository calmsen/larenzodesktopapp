﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Settings;

namespace LaRenzo.DataRepository.Repositories.Settings
{
    public interface IBrandManager
    {
        Brand SelectedBrand { get; set; }

        void Add(Brand item);
        Brand GetById(int id, bool withBranch = true);
        Brand GetByName(string name);
        List<Brand> GetList(FilterModel filter = null, int? branchId = null);
        void Remove(int id);
        void Save(Brand item);
        string Validate(Brand brand);
    }
}