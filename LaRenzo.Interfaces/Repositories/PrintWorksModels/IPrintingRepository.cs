﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;

namespace LaRenzo.DataRepository.Repositories.PrintWorksModels
{
    public interface IPrintingRepository
    {
        CardsData GetCardsData(int sessionID);
        Category GetCategory(int categoryID);
        List<OrderItem> GetOrderItems(Order order);
    }
}