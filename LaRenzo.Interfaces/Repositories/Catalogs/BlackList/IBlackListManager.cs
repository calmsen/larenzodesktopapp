﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.BlackList;

namespace LaRenzo.DataRepository.Repositories.Catalogs.BlackList
{
    public interface IBlackListManager
    {
        void Add(BlackListItem blackListItem);
        BlackListItem GetById(int id);
        BlackListItem GetByName(string name);
        List<BlackListItem> GetList();
        List<BlackListItem> GetListByFilter(string phone, string street, string house, string appartment);
        bool IsAddressBlack(string street, string house, string appartment);
        bool IsNumberBlack(string phoneNumber);
        void Remove(int id);
        void Update(BlackListItem blackListItem);
    }
}