﻿using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public interface ICouponRuleRepository
    {
        List<CouponRule> GetItems(bool withBranch = true);
        void DeleteItem(CouponRule entity);
        Task<int> InsertOrUpdateAsync(CouponRule entity);
    }
}
