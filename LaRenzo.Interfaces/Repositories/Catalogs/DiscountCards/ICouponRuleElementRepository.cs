﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public interface ICouponRuleElementRepository
    {
        Task DeleteByCouponRuleId(int couponRuleId);
        Task<List<CouponRuleElement>> GetByCouponRuleId(int couponRuleId);
        List<CouponRuleElement> GetByCouponRuleIdSync(int couponRuleId);
        Task<int> InsertOrUpdateAsync(CouponRuleElement entity);
    }
}