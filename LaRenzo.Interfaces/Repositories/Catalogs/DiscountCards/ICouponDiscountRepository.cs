﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public interface ICouponDiscountRepository
    {
        List<CouponDiscount> GetCouponDiscountsByCouponRuleId(int couponRuleId);
        void DeleteItem(CouponDiscount entity);
        int InsertOrUpdate(CouponDiscount entity);
    }
}