﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;

namespace LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards
{
    public interface IAdditionalOfferDishRepository
    {
        void DeleteItem(AdditionalOfferDish entity);
        int InsertOrUpdate(AdditionalOfferDish entity);
        List<AdditionalOfferDish> GetItems(IEnumerable<int> dishIds);
        List<AdditionalOfferDish> GetItems(int dishId);
    }
}