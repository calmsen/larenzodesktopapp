﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Booklets
{
    public interface IBookletManager
    {
        List<OrderItem> GetBooklets(int totalSum);
    }
}