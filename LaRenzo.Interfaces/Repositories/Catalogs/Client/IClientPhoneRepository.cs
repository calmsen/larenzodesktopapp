﻿using LaRenzo.DataRepository.Entities.Catalogs.Client;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Client
{
    public interface IClientPhoneRepository
    {
        Task<int> GetClientIdByPhone(string phone);
        Task<int> InsertOrUpdateAsync(ClientPhone entity);
    }
}