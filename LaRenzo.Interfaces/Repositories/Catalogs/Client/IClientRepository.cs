﻿
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Client
{
    public interface IClientRepository
    {
        Task<Entities.Catalogs.Client.Client> GetItemAsync(int id);
        Task<int> InsertOrUpdateAsync(Entities.Catalogs.Client.Client entity);
    }
}
