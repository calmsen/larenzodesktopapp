﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
namespace LaRenzo.DataRepository.Repositories.Catalogs.Sections
{
    public interface ISectionRepository
    {
        List<Section> GetItems(bool withBranch = true);
        void Add(Section section);
        Section GetById(int id);
        List<Section> GetList();
        void Remove(int sectionId);
        void Update(Section section);
        string Validate(Section section, bool isUnique = true);
    }
}