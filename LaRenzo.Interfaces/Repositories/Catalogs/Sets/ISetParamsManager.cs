﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Sets
{
    public interface ISetParamsManager
    {
        void AddSetParams(SetParams entity);
        void DeleteSetParams(SetParams entity);
        SetParams GetSetParams(int id);
        List<SetParams> GetSetParamses();
        void UpdateSetParams(SetParams entity);
    }
}