﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Sets
{
    public interface ISetCalculationManager
    {
        List<OrderItem> GetSets(List<OrderItem> orderItems, bool detail = false);
    }
}