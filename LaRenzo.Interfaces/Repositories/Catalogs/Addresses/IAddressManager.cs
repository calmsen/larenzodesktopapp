﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
namespace LaRenzo.DataRepository.Repositories.Catalogs.Addresses
{
    public interface IAddressManager
    {
        Task AddAddress(Address address);
        void BindToGroup(int addressId, int groupId);
        void ClearGroupBind(int addressId);
        void DeleteAddress(int id);
        object[] GetAddressArray();
        List<Address> GetAddressList();
        List<Address> GetAddressList(int groupId);
        Task UpdateAddress(Address address);
    }
}