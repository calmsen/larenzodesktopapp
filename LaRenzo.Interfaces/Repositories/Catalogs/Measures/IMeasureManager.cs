﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Measures
{
    public interface IMeasureManager
    {
        void AddMeasure(Measure entity);
        void DeleteMeasure(Measure entity);
        Measure GetMeasure(int measureId);
        Measure GetMeasure(Measure entity);
        List<Measure> GetMeasures();
        void UpdateMeasure(Measure entity);
    }
}