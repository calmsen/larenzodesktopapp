﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
namespace LaRenzo.DataRepository.Repositories.Catalogs.Warehouses
{
    public interface IWarehouseManager
    {
        void AddWarehouse(Warehouse entity);
        void DeleteWarehouse(Warehouse entity);
        Warehouse GetWarehouse(int id);
        List<Warehouse> GetWarehouses();
        void UpdateWarehouse(Warehouse entity);
    }
}