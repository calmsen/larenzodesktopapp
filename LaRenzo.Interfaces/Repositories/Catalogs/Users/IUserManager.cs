﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.Users;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Users
{
    public interface IUserManager
    {
        User UserLogged { get; }
        void Add(User user);
        bool DoExist(string name);
        User GetById(int id);
        User GetByName(string name);
        List<User> GetList();
        string GetUserNameOrEmptyString();
        bool IsAdmin();
        bool IsAdmin(int userId);
        bool IsAdmin(User user);
        bool IsLogged();
        bool IsWarehouseManager();
        User Login(string name, string password);
        void Logout();
        void Remove(int id);
        void Update(User user);
        string Validate(User user, bool isUnique, bool requiredPass);
    }
}