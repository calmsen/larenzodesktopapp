﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Settings;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Partners
{
    public interface IPartnerManager
    {
        void AddPartner(Partner entity);
        void DeletePartner(Partner entity);
        Partner GetPartner(int partnerId);
        List<PartnerOfProduct> GetPartnerByProductId(int prodId);
        List<Partner> GetPartners();
        void UpdatePartner(Partner entity);
    }
}