﻿using System;
using System.Collections.Generic;

namespace LaRenzo.DataRepository.Repositories.Reports.RemainsOfGoodReport
{
    public interface IRemainsOfGoodReportManager
    {
        List<RemainsOfGoodReportRecord> GetReportDate(DateTime end, int warehouseId);
    }
}