﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Reports.SettlsWithPartnerReport;
using LaRenzo.DataRepository.Repositories.Reports.PartnersProduct;

namespace LaRenzo.DataRepository.Repositories.Reports.SettlsWithPartnerReport
{
    public interface ISettlsWithPartnerReportManager
    {
        List<SettlsWithPartnerRepRecord> GetReportDate(DateTime end);
    }
}