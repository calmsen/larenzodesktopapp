﻿using LaRenzo.DataRepository.Entities.Wirings;

namespace LaRenzo.DataRepository.Repositories.Wiringses.SettlementsWithPartner
{
    public interface ISettlementsWithPartnerManager
    {
        void ProcessWirings(int documentId, SettlsWithPartnerWirings wiring);
        void UnProcessWirings(int documentId);
    }
}