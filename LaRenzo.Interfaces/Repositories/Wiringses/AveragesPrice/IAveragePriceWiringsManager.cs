﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Wirings;

namespace LaRenzo.DataRepository.Repositories.Wiringses.AveragesPrice
{
    /// <summary>
    /// TODO: Problem
    /// </summary>
    public interface IAveragePriceWiringsManager
    {
        void ProcessWirings(int documentId, List<AveragePriceWirings> wirings);
        void UnProcessWirings(int documentId);
    }
}