﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Wirings;

namespace LaRenzo.DataRepository.Repositories.Wiringses.IntermadiatesGood
{
    public interface IItermadiateGoodWirningManager
    {
        void AddWirning(DateTime dateTime);
        void DeleteWirning(IntermediateGoodWirings selectedRow);
        List<IntermediateGoodWirings> GetList();
    }
}