﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Documents.WriteoffDish;
using LaRenzo.DataRepository.Entities.Wirings;
namespace LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood
{
    /// <summary>
    /// TODO: Problem
    /// </summary>
    public interface IRemainsOfGoodWiringsManager
    {
        RemainsOfGoodWiringsOnDate GetRemainsOnDate(DateTime date);
        RemainsOfGoodWiringsOnDate GetRemainsOnDate(int warehouseId, DateTime date);
        List<RemainsOfGoodWirings> GroupByProductAndWarehouse(List<RemainsOfGoodWirings> list);
        void ProcessInventoryDocument(int documentId, List<RemainsOfGoodWirings> wiringses);
        void ProcessMovingDocument(int documentId, List<RemainsOfGoodWirings> wiringses, Warehouse toWarehouse);
        void ProcessPostingDocument(int documentId, List<RemainsOfGoodWirings> wiringses);
        void ProcessReturnDocument(int documentId, List<RemainsOfGoodWirings> wiringses);
        void ProcessSessionDocument(int documentId, List<RemainsOfGoodWirings> wiringses);
        void ProcessSupplyDocument(int documentId, List<RemainsOfGoodWirings> wiringses);
        void ProcessWriteoffDishDocument(int documentId, List<RemainsOfGoodWirings> wiringses);
        void ProcessWriteoffDishDocument(int documentId, List<WriteoffDishDocumentItem> documentItems);
        void ProcessWriteoffDocument(int documentId, List<RemainsOfGoodWirings> wiringses);
        void UnProcessInventoryDocument(int documentId);
        void UnProcessMovingDocument(int documentId);
        void UnProcessPostingDocument(int documentId);
        void UnProcessReturnDocument(int documentId);
        void UnProcessSessionDocument(int documentId);
        void UnProcessSupplyDocument(int documentId);
        void UnProcessWriteoffDishDocument(int documentId);
        void UnProcessWriteoffDocument(int documentId);
    }
}