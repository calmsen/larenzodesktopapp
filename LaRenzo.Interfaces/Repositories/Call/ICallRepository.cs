﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Calls;
namespace LaRenzo.DataRepository.Repositories.Call
{
    public interface ICallRepository
    {
        void Add(CallData item);
        Task<List<CallData>> ListCalls();
        void Remove(int id);
        void Save(CallData item);
    }
}