﻿using System;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Finance;

namespace LaRenzo.DataRepository.Repositories.Finance
{
    public interface IBankManager
    {
        void AddBankInteraction(Guid orderGuid, BankInteractionCommand command, string comment, int orderId = 0);
        Task<bool> ProcessAll(int brandId, string siteProtocol);
    }
}