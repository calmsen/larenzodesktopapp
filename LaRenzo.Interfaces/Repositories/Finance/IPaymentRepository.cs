﻿using System;
using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Finance;

namespace LaRenzo.DataRepository.Repositories.Finance
{
    public interface IPaymentRepository
    {
        void AddBankPayment(int total, string comment, int categoryId);
        void AddBankPayment(int total, string comment, int categoryId, DateTime date);
        void AddCashPayment(int total, string comment, int categoryId);
        void AddCashPayment(int total, string comment, int categoryId, DateTime date);
        void AddDebtPayment(int total, string comment, int categoryId);
        void AddTransfer(Wallet from, Wallet to, decimal amount, PaymentCategory category, string comment);
        List<Payment> GetItems();
        decimal GetTotalBalance();
        void InsertItem(Payment entity);
    }
}