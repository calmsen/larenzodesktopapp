﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Finance;

namespace LaRenzo.DataRepository.Repositories.Finance
{
    public interface IPaymentCategoryRepository
    {
        void CreateTestCategories();
        void DeleteItem(PaymentCategory entity);
        IEnumerable<PaymentCategory> GetItems();
        List<Payment> GetPayments(int categoryId);
        PaymentCategory InsertItem(PaymentCategory entity);
        void RemoveAllWithPayments();
        void UpdateItem(PaymentCategory entity);
    }
}