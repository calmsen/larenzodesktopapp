﻿using System.Collections.Generic;

namespace LaRenzo.DataRepository.Entities.Finance
{
    public interface IPaymentDashboardRepository
    {
        List<PaymentDashboardCategoryInfo> GetCategoriesWithSummaries();
    }
}