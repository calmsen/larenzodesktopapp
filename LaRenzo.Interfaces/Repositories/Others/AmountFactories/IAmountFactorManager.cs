﻿using System.Collections.Generic;
using LaRenzo.DataRepository.Entities.Others.AmountFactories;

namespace LaRenzo.DataRepository.Repositories.Others.AmountFactories
{
    public interface IAmountFactorManager
    {
        void Add(string name);
        void AddOrUpdateFactor(AmountFactor factor);
        AmountFactorCollection GetCollectionById(int id);
        List<AmountFactor> GetFactors(int collectionId);
        List<AmountFactorCollection> GetList();
        void RemoveCollection(int id);
        void RemoveFactor(int factorId);
    }
}