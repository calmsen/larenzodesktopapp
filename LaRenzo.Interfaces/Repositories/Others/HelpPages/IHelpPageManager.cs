﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Others.HelpPages;

namespace LaRenzo.DataRepository.Repositories.Others.HelpPages
{
    public interface IHelpPageManager
    {
        void DeleteItem(HelpPage entity);
        HelpPage GetExistingOrCreate(string internalName, string name);
        HelpPage GetItem(FilterModel filter);
        HelpPage GetItem(HelpPage entity);
        IEnumerable<HelpPage> GetItems();
        IEnumerable<HelpPage> GetItems(FilterModel filter);
        IEnumerable<HelpPage> GetItems(int t);
        void InsertItem(HelpPage entity);
        Task InsertItemAsync(HelpPage entity);
        void InsertOrUpdateItem(HelpPage entity);
        bool InternalNameAllowed(HelpPage entity);
        void UpdateItem(HelpPage entity);
    }
}