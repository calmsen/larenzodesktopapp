﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Others.TimeSheets;
namespace LaRenzo.DataRepository.Repositories.Others.Times
{
    /// <summary>
    /// TODO: Problem
    /// </summary>
    public interface ITimeSheetManager
    {
        bool AddOrderToTimeSheet(Guid orderGuid);
        void AddSectionToTimeSheetTable(Section section, DateTime startSessionTime, int brandId, int pointOfSaleId);
        int CalculateMaxTimeGap(IEnumerable<TimeSheet> list);
        void ClearAllStatuses();
        void ClearReservation(Guid? orderGuid, bool withCooking = true);
        int ContainsImmutableBlocks(List<TimeSheet> list, DateTime currentTime, bool shiftDownward);
        Dictionary<Section, int> ConvertOrderItemsToTimeCooking(Dictionary<Section, List<OrderItem>> separatedList, int brandId);
        int GetAddDayCountByTime(DateTime dateTime);
        List<TimeSheet> GetList();
        DateTime GetNormalizedDateTime(TimeSpan shift, int addDayCount = 0);
        List<TimeSheet> GetNoTrackingList();
        List<TimeSheet> GetSheetsForReserveInSection(int sectionId, int minutesForSection, int shift, DateTime targetTime, bool isNearTime, bool shiftDownward, int addDayCount, int pointOfSaleId, int brandId);
        DataTable GetTimeSheets(List<TimeSheet> data);
        void MarkReservedAndSave(Guid orderGuid, List<TimeSheet> timeSheetsToUpdate, int brandId);
        void RemoveSectionFromTimeSheetTable(Section section);
        Dictionary<Section, List<OrderItem>> SeparateToSections(List<OrderItem> orderItems);
        void SetStartTime(DateTime startSessionTime);
        void SetTimeCells(List<KeyValuePair<DateTime, int>> cells, string value, int pointOfSaleId, int brandId);
        Task TransferOpenOrdersToTimeSheets(List<TimeSheet> timeSheets);
    }
}