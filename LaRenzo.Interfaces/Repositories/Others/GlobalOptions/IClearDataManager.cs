﻿using System;

namespace LaRenzo.DataRepository.Repositories.Others.GlobalOptions
{
    public interface IClearDataManager
    {

        void RemoveHistory(DateTime from);
    }
}