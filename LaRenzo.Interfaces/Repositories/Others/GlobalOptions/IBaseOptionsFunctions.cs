﻿
using LaRenzo.DataRepository.Entities.Others.GlobalOptions;
using System.Collections.Generic;

namespace LaRenzo.DataRepository.Repositories.Others.GlobalOptions
{
    public interface IBaseOptionsFunctions
    {
        List<GlobalOption> GetItems(bool withBranch = true);
        string GetValueOrEmptyString(string optionName);
        string GetValueOrZero(string optionName);
        void SetOrCreateValue(string optionName, string value);
    }
}