﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Others.GlobalOptions
{
    /// <summary> Название применяемости шрифтов для Принтера </summary>
	public struct PrinterFontUseNames
    {
        public const string HEADER = "header";
        public const string BODY = "body";
        public const string ITEMS = "items";
        public const string SMALL = "small";
        public const string STICKY = "sticky";
        public const string SESSION_BODY = "sessionBody";
    }
}
