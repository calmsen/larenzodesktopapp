﻿using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.Interfaces;
using System.Collections.Generic;

namespace LaRenzo.DataRepository
{
    public class ThreadLocalStorage : IThreadLocalStorage
    {
        public ThreadLocalStorage()
        {
            SelectedBrand = new Brand { Name = "", Check = "", SiteAddress = "" };
            SelectedBranchId = 2;
        }

        public Brand SelectedBrand { get; set; }
        public User UserLogged { get; set; }
        public int SelectedBranchId { get; set; }
        public List<AccessRightInfo> UserRights { get; set; }
    }
}
