﻿namespace LaRenzo.DataRepository.Repositories.Reports.TransferOfGoodReport
{
    public class TransferOfGoodDetailReportRecord
    {
        public string DocumentName { set; get; }

        public decimal ProductAmmountAdded { set; get; }

        public decimal ProductAmmountDeleted { set; get; }
    }
}
