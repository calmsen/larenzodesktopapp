﻿namespace LaRenzo.DataRepository.Repositories.Reports.TransferOfGoodReport
{
    public class TransferOfGoodReportRecord
    {
        public int ProductId { set; get; }

        public string ProductName { set; get; }

        public decimal ProductAmmountStart { set; get; }

        public decimal ProductAmmountAdded { set; get; }

        public decimal ProductAmmountDeleted { set; get; }

        public decimal ProductAmmountEnd { set; get; }

        public string ProductMeasure { set; get; }

        public decimal Price { set; get; }

        public decimal Sum { set; get; }
    }
}
