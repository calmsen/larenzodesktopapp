﻿using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Settings;
using System;

namespace LaRenzo.DataRepository.Repositories.Reports.SalesReport
{
    public class SaleReportRecord
    {
        public Dish Dish { get; set; }

        public int Amount { get; set; }

        public int Price { get; set; }

        public double Cost { get; set; }

        public bool IsCafe { get; set; }

		public Brand Brand { get; set; }

		public string PointOfSale { get; set; }

		public DateTime CreatedDate { get; set; }
		public string CreatedMonth { get; set; }
		public string CreatedDayOfWeek { get; set; }
		public string CreatedHour { get; set; }
		public string User { get; set; }
        public string AddressGroup { get; set; }
    }
}
