﻿using System;

namespace LaRenzo.DataRepository.Repositories.Reports
{
    public class ClientsReportByOrders
    {
        public string ClientName { get; set; }
        public string ClientPhone { get; set; }
        public int OrdersCount { get; set; }
        public int OrdersCountInPeriod { get; set; }
        public DateTime LastOrderData { get; set; }
    }
}
