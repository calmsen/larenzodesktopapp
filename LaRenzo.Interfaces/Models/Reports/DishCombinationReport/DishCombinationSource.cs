﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport
{
    public class DishCombinationSource
    {
        public int OrderId { get; set; }
        public List<int> DishList { get; set; }
    }
}
