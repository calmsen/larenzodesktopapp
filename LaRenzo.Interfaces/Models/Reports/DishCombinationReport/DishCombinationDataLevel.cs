﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport
{
    public class DishCombinationDataLevel
    {
        private int _currentIndex;
        public int LevelId { get; set; }
        public List<DishCombinationSource> Data { get; set; }
        public int MaxIndex { get; set; }

        public int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                _currentIndex = value > MaxIndex ? MaxIndex : value;
            }
        }
    }
}
