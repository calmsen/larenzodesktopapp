﻿using LaRenzo.DataRepository.Entities.Documents.WriteoffDish;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.WriteoffDish
{
    public class WriteoffDishData
    {
        public WriteoffDishDocument Document { get; set; }
        public List<WriteoffDishDocumentItem> DocumentItems { get; set; }
    }
}
