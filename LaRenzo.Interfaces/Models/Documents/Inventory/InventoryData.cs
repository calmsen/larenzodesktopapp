﻿using LaRenzo.DataRepository.Entities.Documents.Inventories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Inventory
{
    public class InventoryData
    {
        public InventoryDocument Document { get; set; }
        public List<InventoryDocumentItem> DocumentItems { get; set; }
    }
}
