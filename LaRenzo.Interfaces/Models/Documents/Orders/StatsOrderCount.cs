﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Orders
{
    public class StatsOrderCount
    {
        public string Phone { get; set; }
        public int OrdersCount { get; set; }
    }
}
