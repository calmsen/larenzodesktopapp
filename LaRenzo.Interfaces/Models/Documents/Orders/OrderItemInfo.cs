﻿namespace LaRenzo.TransferLibrary
{
    public class OrderItemInfo
    {
        public int? SKU { get; set; }
        
        public string Name { get; set; }
        
        public int Amount { get; set; }
    }
}
