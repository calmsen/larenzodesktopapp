﻿using LaRenzo.DataRepository.Entities.Documents.Supply;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Supply
{
    public class SupplyData
    {
        public SupplyDocument Document { get; set; }
        public List<SupplyDocumentItem> DocumentItems { get; set; }
    }
}
