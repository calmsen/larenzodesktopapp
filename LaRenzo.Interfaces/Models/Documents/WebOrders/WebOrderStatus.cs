﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.WebOrders
{
    public class WebOrderStatus
    {
        public const string New = "New";
        public const string Processing = "Processing";
        public const string Processed = "Processed";
    }
}
