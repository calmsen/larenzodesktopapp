﻿using LaRenzo.DataRepository.Entities.Documents.Returns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Documents.Returns
{
    public class ReturnData
    {
        public ReturnDocument Document { get; set; }
        public List<ReturnDocumentItem> DocumentItems { get; set; }
    }
}
