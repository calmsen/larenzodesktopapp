﻿using LaRenzo.DataRepository.Entities;
namespace DataRepository.Entities
{
    public class BrandFilter: FilterModel
    {
        public string BrandName { get; set; }
    }
}
