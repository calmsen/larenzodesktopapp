﻿using System.Collections.Generic;
using System.Xml.Serialization;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.Products;

namespace LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer
{
    [XmlRoot(ElementName = "prod")]
    public class Product
    {
        [XmlElement(ElementName = "proc")]
        public List<Proc> Procs { set; get; }

        [XmlElement(ElementName = "recipe")]
        public Recipe Recipe { set; get; }

        [XmlAttribute(AttributeName = "id")]
        public int ID { set; get; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { set; get; }

        [XmlAttribute(AttributeName = "unit")]
        public string Unit { set; get; }

        [XmlAttribute(AttributeName = "grm")]
        public string GRM { set; get; }

        [XmlAttribute(AttributeName = "cost")]
        public string Cost { set; get; }

        [XmlAttribute(AttributeName = "wh")]
        public string WH { set; get; }

        [XmlAttribute(AttributeName = "gr")]
        public string GR { set; get; }

        [XmlAttribute(AttributeName = "ch")]
        public string CH { set; get; }

        [XmlAttribute(AttributeName = "dry")]
        public string DRY { set; get; }

        [XmlAttribute(AttributeName = "sug")]
        public string Sug { set; get; }

        [XmlAttribute(AttributeName = "fat")]
        public string Fat { set; get; }

        [XmlAttribute(AttributeName = "alko")]
        public string Alko { set; get; }

        [XmlAttribute(AttributeName = "rem")]
        public string Rem { set; get; }

        [XmlAttribute(AttributeName = "state")]
        public string State { set; get; }

        [XmlAttribute(AttributeName = "fid")]
        public string Fid { set; get; }

        [XmlIgnore]
        public ProductCategory ProductCategory { set; get; }

        [XmlIgnore]
        public Category DishCategory { set; get; }
    }
}
