﻿namespace LaRenzo.DataRepository.Entities.Transfers.ChefExpertTransfer
{
    public class ProductConformity
    {
        public Catalogs.Products.Product Product { set; get; }

        public Product ChefProduct { set; get; }
    }
}
