﻿namespace LaRenzo.DataRepository.Entities.Transfers
{
    public class NewItemAction
    {
        public int ID { set; get; }

        public string Name { set; get; }

        public override string ToString()
        {
            return Name;
        }
    }
}
