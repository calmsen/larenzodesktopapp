﻿using System;

namespace LaRenzo.DataRepository.Entities.PrintWorksModels
{
    public class PrintMonitorRecord
    {
        public int ID { set; get; }

        public int? OrderId { set; get; }

        public string OrderName { set; get; }

        public string State { set; get; }

        public DateTime? OrderTime { set; get; }

        public DateTime? Created { set; get; }

        public DateTime? LastTry { set; get; }
    }
}
