﻿using System.Collections.Generic;
using System.Linq;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.TransferLibrary;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Printers
{
    public class PaymentSessionSummaryInfo
    {
        public List<PaymentMethodSessionSummary> SummaryList { get; set; }

        public int TotalCount
        {
            get { return Orders.Any() ? Orders.Count : 0; }
        }

        public int TotalSum
        {
            get { return Orders.Any() ? Orders.Sum(x => x.TotalCost) : 0; }
        }

        public int TotalCanceledCount
        {
            get { return CanceledOrders.Any() ? CanceledOrders.Count : 0; }
        }

        public int TotalCanceledSum
        {
            get { return CanceledOrders.Any() ? CanceledOrders.Sum(x => x.TotalCost) : 0; }
        }

        public List<Order> Orders { get; set; }
        public List<Order> CanceledOrders { get; set; }
        

        public static PaymentSessionSummaryInfo CreateWithSiteOnlineCard(List<Order> orders)
        {
            var obj = new PaymentSessionSummaryInfo();
            obj.Orders = orders
                .Where(x => x.State.Name != "Отказ" && x.Source != "vlru")
                .Where(x => x.WebOrderGuid != null && x.PaymentMethod == PaymentMethod.OnlineCard)
                .ToList();
            obj.CanceledOrders = orders
                .Where(x => x.State.Name == "Отказ" && x.Source != "vlru")
                .Where(x => x.WebOrderGuid != null && x.PaymentMethod == PaymentMethod.OnlineCard)
                .ToList();

            obj.SummaryList = new List<PaymentMethodSessionSummary>
                {
                    new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.OnlineCard, "Картой онлайн")
                };
            return obj;
        }

        public static PaymentSessionSummaryInfo CreateWithoutSiteOnlineCard(List<Order> orders)
        {
            var obj = new PaymentSessionSummaryInfo();
            obj.Orders = orders
                .Where(x => x.State.Name != "Отказ" && x.Source != "vlru")
                .Where(x => x.WebOrderGuid == null || (x.WebOrderGuid != null && x.PaymentMethod != PaymentMethod.OnlineCard))
                .ToList();
            obj.CanceledOrders = orders
                .Where(x => x.State.Name == "Отказ" && x.Source != "vlru")
                .Where(x => x.WebOrderGuid == null || (x.WebOrderGuid != null && x.PaymentMethod != PaymentMethod.OnlineCard))
                .ToList();

            obj.SummaryList = new List<PaymentMethodSessionSummary>
                {
                    new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.Cash, "Наличные"),
                    new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.OfflineCard, "Картой курьеру"),
                    new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.OnlineCard, "Картой онлайн"),
                    new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.Undefined, "Способ не указан")
                };
            return obj;
        }

        public static PaymentSessionSummaryInfo CreateWithVlruSite(List<Order> orders)
        {
            var obj = new PaymentSessionSummaryInfo();
            obj.Orders = orders.Where(x => x.State.Name != "Отказ" && x.Source == "vlru").ToList();
            obj.CanceledOrders = orders.Where(x => x.State.Name == "Отказ" && x.Source == "vlru").ToList();

            obj.SummaryList = new List<PaymentMethodSessionSummary>
            {
                new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.Cash, "Наличные", true),
                new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.OfflineCard, "Картой курьеру", true),
                new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.OnlineCard, "Картой онлайн", true)
            };
            return obj;
        }

        public static PaymentSessionSummaryInfo CreateDefault(List<Order> orders)
        {
            var obj = new PaymentSessionSummaryInfo();
            obj.Orders = orders.Where(x => x.State.Name != "Отказ" && x.Source != "vlru").ToList();
            obj.CanceledOrders = orders.Where(x => x.State.Name == "Отказ" && x.Source != "vlru").ToList();

            obj.SummaryList = new List<PaymentMethodSessionSummary>
            {
                new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.Cash, "Наличные"),
                new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.OfflineCard, "Картой курьеру"),
                new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.OnlineCard, "Картой онлайн"),
                new PaymentMethodSessionSummary(obj.Orders, PaymentMethod.Undefined, "Способ не указан")
            };
            return obj;
        }

        public PaymentSessionSummaryInfo()
        {
            
        }
    }

}
