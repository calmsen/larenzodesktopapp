﻿using System.Collections.Generic;
using System.Linq;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.TransferLibrary;

namespace LaRenzo.DataRepository.Repositories.Catalogs.Printers
{
    public class PaymentMethodSessionSummary
    {
        public PaymentMethod? PaymentMethod { get; set; }
        public int Count { get; set; }
        public int Sum { get; set; }
        public string Description { get; set; }

        public bool IsVlru { get; set; }

        public List<Order> Orders { get; set; }

        public PaymentMethodSessionSummary(IEnumerable<Order> orders, PaymentMethod? method = null, string description = null, bool isVlru = false)
        {
            IsVlru = isVlru;
            PaymentMethod = method;
            if (PaymentMethod != null)
            {
                Orders = orders.Where(x => x.PaymentMethod == method).ToList();
            }
            else
            {
                Orders = orders.ToList();
            }
            Count = Orders.Count;
            Sum = Orders.Any() ? Orders.Sum(x => x.TotalCost) : 0;
            Description = description;
        }
    }
}
