﻿namespace LaRenzo.DataRepository.Entities.Catalogs.Addresses
{
    public class AddressRecord
    {
        public bool IsChecked { set; get; }

        public string Address { set; get; }
    }
}
