﻿namespace LaRenzo.DataRepository.Entities.Catalogs.BlackList
{
    public class BlackListFilter: FilterModel
    {
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Appartment { get; set; }
    }
}
