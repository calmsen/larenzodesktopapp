﻿namespace LaRenzo.DataRepository.Entities.Catalogs.Tables
{
    public class TableFilter : FilterModel
    {
        public int TableID { set; get; }
    }
}
