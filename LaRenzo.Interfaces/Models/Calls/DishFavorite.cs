﻿using LaRenzo.DataRepository.Entities.Catalogs.Dishes;

namespace LaRenzo.DataRepository.Repositories.Call
{
	public class DishFavorite
	{
		/// <summary> Блюдо </summary>
		public Dish Dish;

		/// <summary> Сколько раз было куплено </summary>
		public int Count;

	}
}
