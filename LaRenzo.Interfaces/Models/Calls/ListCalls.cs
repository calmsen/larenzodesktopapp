﻿namespace LaRenzo.DataRepository.Repositories.Call
{
    public class ListCalls
	{
		public string ClientName { get; set; }
		public string Status { get; set; }

		public string PhoneNumber { get; set; }

		public string WaitTime { get; set; }
		public string OperatorName { get; set; }
		public string IsVip { get; set; }
	}
}
