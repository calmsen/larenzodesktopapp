﻿using LaRenzo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository
{
    public class UserMessage
    {
        public static IUserMessageStrategy UserMessageStrategy { get; set; }
        public static void Show(string message)
        {
            UserMessageStrategy?.Show(message);
        }
        public static void Show(string message, Exception innerException)
        {
            UserMessageStrategy?.Show(message, innerException);
        }
    }
}
