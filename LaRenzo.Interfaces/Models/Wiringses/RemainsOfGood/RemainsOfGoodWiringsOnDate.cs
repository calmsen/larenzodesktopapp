﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood
{
    public class RemainsOfGoodWiringsOnDate
    {
        public int WarehouseID { get; set; }
        public DateTime DateTime { get; set; }

        public bool HasBrokenInventarisationsDueDate { get; set; }

        public List<ProductRemain> ProductRemains { get; set; }
    }
}
