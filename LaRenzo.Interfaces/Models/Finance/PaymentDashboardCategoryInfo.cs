﻿namespace LaRenzo.DataRepository.Entities.Finance
{
    public class PaymentDashboardCategoryInfo
    {
        public int ID {
            get { return Category.ID; }
        }
        public int ParentID {
            get { return Category.Parent; }
        }

        public PaymentCategory Category { get; set; }
        
        public decimal SpendTotal { get; set; }
        public decimal SpendPercent { get; set; }

        public decimal EarningTotal { get; set; }
        public decimal EarningPercent { get; set; }

    }
}
