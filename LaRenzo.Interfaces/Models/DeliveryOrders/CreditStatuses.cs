﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LaRenzo.DataRepository.Entities.Documents.DeliveryOrders
{
    [NotMapped]
    public class CreditStatuses
    {
        /// <summary>
        /// Полностью списан
        /// </summary>
        public const int FULL_CREDITED = 0;

        /// <summary>
        /// Частично списан
        /// </summary>
        public const int PARTIAL_CREDITED = 1;

        /// <summary>
        /// Полностью восстановлен (на складе)
        /// </summary>
        public const int FULL_RESTORED = 2;
    }
}
