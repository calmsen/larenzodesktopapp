﻿using System;
using DevExpress.XtraBars.Alerter;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Forms.Documents.InventoryForms;
using LaRenzo.FormsProvider;
using LaRenzo.Interfaces.SignalMessages;

namespace LaRenzo.Tools
{
    public static class NotifyManager
    {
        /// <summary>
        ///     Метод подписывает NotifyManager на события в InventoryManager 
        ///     (инвентаризация была помечена как некорректная)
        /// </summary>
        public static void SubscribeToInventoryChanges()
        {
            Content.SignalProcessor.Bind<BrokenInventorySignalMessage>(OnBrokenInventory);
        }

        private static void OnBrokenInventory(BrokenInventorySignalMessage message)
        {
            var info = new AlertInfo("Инвентаризация была помечена, как некорректная",
                    string.Format("Инвентаризая №{0} от {1} требует перепроводки",
                        message.DocumentId, message.DocumentDate, message.Reason));

            ShowNotify(info);
        }

        public static void UnsubsctibeFromInventoryChanges()
        {
            Content.SignalProcessor.UnBind<BrokenInventorySignalMessage>();
        }

        /// <summary>
        ///     Показывает всплывающее уведомление в новом окне
        /// </summary>
        /// <param name="info"></param>
        private static void ShowNotify(AlertInfo info)
        {
            var alert = new AlertControl();
            
            Action notifyAction = () =>
                {
                    SingletonFormProvider<InventoryListForm>.ActivateForm(() => new InventoryListForm());
                    alert.AlertFormList.ForEach(x => x.Close());
                };

            alert.AlertClick += (sender, args) => { if (notifyAction != null) notifyAction.Invoke(); };

            alert.Show(null, info);
        }
    }
}
