﻿namespace LaRenzo.Tools
{
    public static class IntExtension
    {
        public static bool IsEven(this int item)
        {
            return item%2 != 1;
        }
    }
}
