﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace LaRenzo.Tools
{
    public struct MailSendParams
	{
		/// <summary> Параметы от кого </summary>
		public MailSendParamsFrom FromParam;

		/// <summary> Кому </summary>
		public string ToMailAddress;

		/// <summary> Заголовок </summary>
		public string MailTitle;

		/// <summary> Сообщение </summary>
		public string Message;

		/// <summary> Список имён файлов на отправку </summary>
		public List<string> AttachListFileNames;

		/// <summary> Список файлов на отправку </summary>
		public List<string> AttachListFileBodys;
	}

	public struct MailSendParamsFrom
	{
		/// <summary> Адрес почты </summary>
		public string Address;

		/// <summary> SMTP сервер</summary>
		public string SmtpServer;
		/// <summary> SMTP порт</summary>
		public int SmtpServerPort;

		/// <summary> Пользователь </summary>
		public string User;
		/// <summary> Пароль </summary>
		public string Pass;

		/// <summary> Использовать SSL </summary>
		public bool IsEnabledSSL;
	}

	
	public class SendMailManager
	{
		public void SendMail(MailSendParams mailSendParams)
		{
			var ms = new List<MemoryStream>();
			
			// Сформировать
			var msg = new MailMessage(mailSendParams.FromParam.Address, mailSendParams.ToMailAddress, mailSendParams.MailTitle, mailSendParams.Message);
			var smtpClient = new SmtpClient(mailSendParams.FromParam.SmtpServer, mailSendParams.FromParam.SmtpServerPort)
			{
				Credentials = new NetworkCredential(mailSendParams.FromParam.User, mailSendParams.FromParam.Pass),
				EnableSsl = mailSendParams.FromParam.IsEnabledSSL
			};
			
			// Добавить вложения из названий файлов
			if (mailSendParams.AttachListFileNames != null && mailSendParams.AttachListFileNames.Count > 0)
			{
				foreach (string attachFileName in mailSendParams.AttachListFileNames)
				{
					var attachment = new Attachment(attachFileName);msg.Attachments.Add(attachment);
				}
			}

			// Добавить вложения из тел файлов
			if (mailSendParams.AttachListFileNames != null && mailSendParams.AttachListFileBodys.Count > 0)
			{
				
				foreach (string attachFileBody in mailSendParams.AttachListFileBodys)
				{
					var memStream = new MemoryStream();
					
					var writer = new StreamWriter(memStream);
					writer.Write(attachFileBody);
					//writer.Flush();
					//writer.Dispose();

					var attachment = new Attachment("ExecutionTime.txt");
					
					
					
					attachment.ContentDisposition.FileName = "ExecutionTime.txt";
					msg.Attachments.Add(attachment);
					ms.Add(memStream);
				}
			}
			
			// отослать
			smtpClient.Send(msg);

			//foreach (var memoryStream in ms)
			//{
			//	memoryStream.Close();
			//}

			//ms.Clear();
		}

	}
}
