﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;

namespace LaRenzo.Tools
{
    public partial class ExecutionTimeLoggerForm : Form
    {
        public ExecutionTimeLoggerForm()
        {
            InitializeComponent();
        }

        private void ExecutionTimeLoggerForm_Load(object sender, EventArgs e)
        {
            gridControl.DataSource = ExecutionTimeLogger.Logs;
        }

        private void timerDataSource_Tick(object sender, EventArgs e)
        {
            gridControl.RefreshDataSource();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            gridControl.RefreshDataSource();
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            saveFileDialog1.DefaultExt = "cvs";
            saveFileDialog1.ShowDialog(this);

        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            File.WriteAllLines(saveFileDialog1.FileName, ExecutionTimeLogger.Logs.Select(x => string.Join(",", x)));
        }

        private void gridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var obj = (ExucutionTimeLogEntry)view.GetRow(e.RowHandle);

            if (obj.TooLong) e.Appearance.BackColor = Color.Firebrick;

        }
    }
}
