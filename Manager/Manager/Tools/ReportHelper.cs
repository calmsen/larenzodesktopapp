﻿using System.Drawing;
using System.Windows.Forms;
using LaRenzo.Properties;

namespace LaRenzo.Tools
{
    public static class ReportHelper
    {
        /// <summary>
        /// Настраивает внешний вид кнопки, пока генерируется отчет
        /// </summary>
        /// <param name="btn"></param>
        public static void SetUpButtonOnStart(Button btn)
        {
            btn.Text = Resources.ReportHelper_Button_ReportIsGenerating;
            btn.AutoSize = true;
            btn.BackColor = Settings.Default.ButtonInProgressColor;
        }

        /// <summary>
        /// Выставляет стандартные настройки для кнопки создания отчета
        /// </summary>
        /// <param name="btn"></param>
        public static void SetUpButtonDefault(Button btn)
        {
            btn.Text = Resources.ReportHelper_Button_CreateReport;
            btn.BackColor = SystemColors.Control;
        }
    }
}
