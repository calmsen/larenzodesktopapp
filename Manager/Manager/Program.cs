using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Forms.MainFormAndIM;
using LaRenzo.Properties;
using Ninject;
using NLog;

namespace LaRenzo
{
    static class Program
    {
        static Logger _logger;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            _logger = LogManager.GetCurrentClassLogger();

            Content.RersolveRepositories();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var applicationClosed = false;
            try
            {
                var version = Content.ProgrammVersionRepository.GetItems().FirstOrDefault();
                if (version != null)
                {
                    var supportedVersion = new Version(version.SupportedVersion);
                    var currentVersion = new Version(version.CurrentVersion);
                    var appCurrentVersion = new Version(Settings.Default.CurrentVersion);
                    if (appCurrentVersion < supportedVersion)
                    {
                        applicationClosed = true;
                        MessageBox.Show(
                            "� ��� ����������� �� ��������� ������ ���������, ����������, �������� ��������� ��� ���������� ������.");
                    }
                    else if (appCurrentVersion < currentVersion)
                    {
                        MessageBox.Show(
                            "� ��� ����������� �� ��������� ������ ���������, ����������, �������� ���������.");
                    }
                }
            }
            catch (Exception ex)
            {
                applicationClosed = true;
                MessageBox.Show($"������ �������� ������ ���������. ��������� ����������� � ��������� � ����������� ���� ������. {ex.Message}");
            }
            if (!applicationClosed)
            {
                var mainForm = new MainForm();
                Application.Run(mainForm);
            }
        }

        static void Application_ThreadException(
            object sender, ThreadExceptionEventArgs e)
        {
            _logger.Error(e.Exception);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            _logger.Error(e.ExceptionObject);
        }
    }
}

