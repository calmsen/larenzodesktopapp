﻿namespace LaRenzo.Forms.Catalog.SetsForms
{
    partial class SetParamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblMinValue = new System.Windows.Forms.Label();
            this.btnSelectCoefficient = new System.Windows.Forms.Button();
            this.lblCoefficient = new System.Windows.Forms.Label();
            this.numMinValue = new System.Windows.Forms.NumericUpDown();
            this.lblMaxValue = new System.Windows.Forms.Label();
            this.numMaxValue = new System.Windows.Forms.NumericUpDown();
            this.chkIsIncrement = new System.Windows.Forms.CheckBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.numValue = new System.Windows.Forms.NumericUpDown();
            this.lblValue = new System.Windows.Forms.Label();
            this.numIncrement = new System.Windows.Forms.NumericUpDown();
            this.lblIncrement = new System.Windows.Forms.Label();
            this.txtCoefficient = new System.Windows.Forms.TextBox();
            this.chkRounded = new System.Windows.Forms.CheckBox();
            this.cmbRoundedValue = new System.Windows.Forms.ComboBox();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMinValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIncrement)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 168);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(344, 24);
            this.pnlBottom.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(168, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Записать";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(255, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblMinValue
            // 
            this.lblMinValue.AutoSize = true;
            this.lblMinValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMinValue.Location = new System.Drawing.Point(26, 40);
            this.lblMinValue.Name = "lblMinValue";
            this.lblMinValue.Size = new System.Drawing.Size(59, 13);
            this.lblMinValue.TabIndex = 13;
            this.lblMinValue.Text = "Блюд от:";
            // 
            // btnSelectCoefficient
            // 
            this.btnSelectCoefficient.Location = new System.Drawing.Point(304, 12);
            this.btnSelectCoefficient.Name = "btnSelectCoefficient";
            this.btnSelectCoefficient.Size = new System.Drawing.Size(24, 20);
            this.btnSelectCoefficient.TabIndex = 26;
            this.btnSelectCoefficient.Text = "...";
            this.btnSelectCoefficient.UseVisualStyleBackColor = true;
            this.btnSelectCoefficient.Click += new System.EventHandler(this.btnSelectCoefficient_Click);
            // 
            // lblCoefficient
            // 
            this.lblCoefficient.AutoSize = true;
            this.lblCoefficient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCoefficient.Location = new System.Drawing.Point(37, 15);
            this.lblCoefficient.Name = "lblCoefficient";
            this.lblCoefficient.Size = new System.Drawing.Size(48, 13);
            this.lblCoefficient.TabIndex = 13;
            this.lblCoefficient.Text = "Набор:";
            // 
            // numMinValue
            // 
            this.numMinValue.Location = new System.Drawing.Point(98, 38);
            this.numMinValue.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numMinValue.Name = "numMinValue";
            this.numMinValue.Size = new System.Drawing.Size(97, 20);
            this.numMinValue.TabIndex = 27;
            this.numMinValue.Tag = "MinValue";
            // 
            // lblMaxValue
            // 
            this.lblMaxValue.AutoSize = true;
            this.lblMaxValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMaxValue.Location = new System.Drawing.Point(201, 40);
            this.lblMaxValue.Name = "lblMaxValue";
            this.lblMaxValue.Size = new System.Drawing.Size(25, 13);
            this.lblMaxValue.TabIndex = 13;
            this.lblMaxValue.Text = "до:";
            // 
            // numMaxValue
            // 
            this.numMaxValue.Location = new System.Drawing.Point(234, 38);
            this.numMaxValue.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numMaxValue.Name = "numMaxValue";
            this.numMaxValue.Size = new System.Drawing.Size(94, 20);
            this.numMaxValue.TabIndex = 27;
            this.numMaxValue.Tag = "MaxValue";
            // 
            // chkIsIncrement
            // 
            this.chkIsIncrement.AutoSize = true;
            this.chkIsIncrement.Location = new System.Drawing.Point(98, 90);
            this.chkIsIncrement.Name = "chkIsIncrement";
            this.chkIsIncrement.Size = new System.Drawing.Size(174, 17);
            this.chkIsIncrement.TabIndex = 28;
            this.chkIsIncrement.Tag = "IsIncremental";
            this.chkIsIncrement.Text = "Инкрементально возрастает";
            this.chkIsIncrement.UseVisualStyleBackColor = true;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(31, 87);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(54, 20);
            this.txtId.TabIndex = 29;
            this.txtId.Tag = "id";
            this.txtId.Visible = false;
            // 
            // numValue
            // 
            this.numValue.Location = new System.Drawing.Point(98, 64);
            this.numValue.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numValue.Name = "numValue";
            this.numValue.Size = new System.Drawing.Size(230, 20);
            this.numValue.TabIndex = 27;
            this.numValue.Tag = "DishValue";
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblValue.Location = new System.Drawing.Point(12, 66);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(80, 13);
            this.lblValue.TabIndex = 13;
            this.lblValue.Text = "Количество:";
            // 
            // numIncrement
            // 
            this.numIncrement.Location = new System.Drawing.Point(98, 111);
            this.numIncrement.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numIncrement.Name = "numIncrement";
            this.numIncrement.Size = new System.Drawing.Size(230, 20);
            this.numIncrement.TabIndex = 27;
            this.numIncrement.Tag = "IncrementValue";
            // 
            // lblIncrement
            // 
            this.lblIncrement.AutoSize = true;
            this.lblIncrement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblIncrement.Location = new System.Drawing.Point(18, 113);
            this.lblIncrement.Name = "lblIncrement";
            this.lblIncrement.Size = new System.Drawing.Size(67, 13);
            this.lblIncrement.TabIndex = 13;
            this.lblIncrement.Text = "Инкремент:";
            // 
            // txtCoefficient
            // 
            this.txtCoefficient.Location = new System.Drawing.Point(98, 12);
            this.txtCoefficient.Name = "txtCoefficient";
            this.txtCoefficient.ReadOnly = true;
            this.txtCoefficient.Size = new System.Drawing.Size(202, 20);
            this.txtCoefficient.TabIndex = 29;
            this.txtCoefficient.Tag = "SetID";
            // 
            // chkRounded
            // 
            this.chkRounded.AutoSize = true;
            this.chkRounded.Location = new System.Drawing.Point(15, 141);
            this.chkRounded.Name = "chkRounded";
            this.chkRounded.Size = new System.Drawing.Size(79, 17);
            this.chkRounded.TabIndex = 28;
            this.chkRounded.Tag = "Round";
            this.chkRounded.Text = "Округлять";
            this.chkRounded.UseVisualStyleBackColor = true;
            this.chkRounded.CheckedChanged += new System.EventHandler(this.chkRounded_CheckedChanged);
            // 
            // cmbRoundedValue
            // 
            this.cmbRoundedValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRoundedValue.Enabled = false;
            this.cmbRoundedValue.FormattingEnabled = true;
            this.cmbRoundedValue.Items.AddRange(new object[] {
            "В большую сторону",
            "В меньшую сторону"});
            this.cmbRoundedValue.Location = new System.Drawing.Point(98, 139);
            this.cmbRoundedValue.Name = "cmbRoundedValue";
            this.cmbRoundedValue.Size = new System.Drawing.Size(230, 21);
            this.cmbRoundedValue.TabIndex = 30;
            this.cmbRoundedValue.Tag = "RoundDirection";
            // 
            // SetParamForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(344, 192);
            this.Controls.Add(this.cmbRoundedValue);
            this.Controls.Add(this.txtCoefficient);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.chkRounded);
            this.Controls.Add(this.chkIsIncrement);
            this.Controls.Add(this.numIncrement);
            this.Controls.Add(this.numValue);
            this.Controls.Add(this.numMaxValue);
            this.Controls.Add(this.numMinValue);
            this.Controls.Add(this.btnSelectCoefficient);
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.lblCoefficient);
            this.Controls.Add(this.lblMaxValue);
            this.Controls.Add(this.lblIncrement);
            this.Controls.Add(this.lblMinValue);
            this.Controls.Add(this.pnlBottom);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(360, 230);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(360, 230);
            this.Name = "SetParamForm";
            this.Text = "Правила для набора";
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numMinValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIncrement)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblMinValue;
        private System.Windows.Forms.Button btnSelectCoefficient;
        private System.Windows.Forms.Label lblCoefficient;
        private System.Windows.Forms.NumericUpDown numMinValue;
        private System.Windows.Forms.Label lblMaxValue;
        private System.Windows.Forms.NumericUpDown numMaxValue;
        private System.Windows.Forms.CheckBox chkIsIncrement;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.NumericUpDown numValue;
        private System.Windows.Forms.Label lblValue;
        private System.Windows.Forms.NumericUpDown numIncrement;
        private System.Windows.Forms.Label lblIncrement;
        private System.Windows.Forms.TextBox txtCoefficient;
        private System.Windows.Forms.CheckBox chkRounded;
        private System.Windows.Forms.ComboBox cmbRoundedValue;
    }
}