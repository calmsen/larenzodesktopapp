﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Dishes;

namespace LaRenzo.Forms.Catalog.SetsForms
{
    public partial class SelectDishesListForm : Form
    {
        public Dish Dish { set; get; }

        public SelectDishesListForm()
        {
            InitializeComponent();
            LoadDishesList();
        }

        private void LoadDishesList()
        {
            mainGridControl.DataSource = Content.DishRepository.GetItems(null);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Dish = (Dish) mainGridView.GetFocusedRow();
            if (Dish == null)
                return;
            
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }
    }
}