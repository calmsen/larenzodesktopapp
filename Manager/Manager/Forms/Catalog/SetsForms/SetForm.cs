﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Sets;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.SetsForms
{
    public partial class SetForm : Form
    {
        private List<SetOfDishes> _setOfDisheses; 

        private readonly bool _isEditWindow;

        public SetForm()
        {
            InitializeComponent();
            _setOfDisheses = new List<SetOfDishes>();
        }

        public SetForm(bool isEdit, Set set) : this()
        {
            _isEditWindow = isEdit;

            WindowsFormProvider.FillControlsFromModel(this, set);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text))
            {
                var ep = new ErrorProvider();
                ep.SetError(txtName, Resources.CoefficientNameIsNull);
                return;
            }

            var coefficient = new Set
                {
                    ID = string.IsNullOrEmpty(txtID.Text) ? 0 : Convert.ToInt32(txtID.Text.Trim()),
                    Name = txtName.Text.Trim()
                };

            if (_isEditWindow)
            {
                Content.SetManager.UpdateSet(coefficient, _setOfDisheses);
            }
            else
            {
                Content.SetManager.AddSet(coefficient, _setOfDisheses);
            }

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void SetForm_Load(object sender, EventArgs e)
        {
            LoadDetailItems();
        }

        private void LoadDetailItems()
        {
            if (!string.IsNullOrEmpty(txtID.Text))
            {
                _setOfDisheses = Content.SetManager.GetDetailDishes(Convert.ToInt32(txtID.Text));
                mainGridControl.DataSource = _setOfDisheses;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            mainGridControl.BeginUpdate();
            var selectDishesList = new SelectDishesListForm();

            if (selectDishesList.ShowDialog() == DialogResult.OK && selectDishesList.Dish != null)
            {
                _setOfDisheses.Add(new SetOfDishes
                    {
                        ID = -1,
                        SetID = string.IsNullOrEmpty(txtID.Text) ? 0 : Convert.ToInt32(txtID.Text),
                        Dish = selectDishesList.Dish,
                        DishID = selectDishesList.Dish.ID,
                    });
            }
            mainGridControl.DataSource = _setOfDisheses;
            mainGridControl.EndUpdate();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DelItemFromSetQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                mainGridControl.BeginUpdate();

                var selectedRow = (SetOfDishes) mainGridView.GetFocusedRow();
                if (selectedRow!= null)
                {
                    _setOfDisheses.Remove(selectedRow);
                }

                mainGridControl.EndUpdate();
            }
        }
    }
}