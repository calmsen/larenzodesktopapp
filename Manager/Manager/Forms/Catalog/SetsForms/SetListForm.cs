﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Sets;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.SetsForms
{
    public partial class SetListForm : Form
    {
        public SetListForm()
        {
            InitializeComponent();
            LoadCoefficientList();
        }

        private void LoadCoefficientList()
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            mainGridControl.DataSource = Content.SetManager.GetSets();
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var coefficient = new SetForm();
            if (coefficient.ShowDialog() == DialogResult.OK)
            {
                LoadCoefficientList();
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (Set)mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetCoefficientForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.CoefficientIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void GetCoefficientForm(bool isEdit, int coefficientId)
        {
            var coefficient = Content.SetManager.GetSet(coefficientId);

            var coefficientForm = new SetForm(isEdit, coefficient);

            if (coefficientForm.ShowDialog() == DialogResult.OK)
            {
                LoadCoefficientList();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (Set)mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetCoefficientForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.CoefficientIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteCoefficientQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var selectedRow = (Set)mainGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    Content.SetManager.DeleteSet(new Set{ID = selectedRow.ID});
                    LoadCoefficientList();
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadCoefficientList();
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var coefficientId = (int) view.GetRowCellValue(info.RowHandle, columnId);
                GetCoefficientForm(true, coefficientId);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}