﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Catalogs.Sets;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Cutlery;
using LaRenzo.DataRepository.Repositories.Catalogs.Sets;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.SetsForms
{
    public partial class SetParamsListForm : Form
    {
        public SetParamsListForm()
        {
            InitializeComponent();
            LoadCoefficientParamsList();
        }

        private void LoadCoefficientParamsList()
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            mainGridControl.DataSource = Content.SetParamsManager.GetSetParamses();
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var coefficientForm = new SetParamForm();
            if (coefficientForm.ShowDialog() == DialogResult.OK)
            {
                LoadCoefficientParamsList();
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (SetParams) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetCoefficientForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.CoefficientIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                               MessageBoxIcon.Warning);
            }
        }

        private void GetCoefficientForm(bool isEdit, int coefficientId)
        {
            var coefficient = Content.SetParamsManager.GetSetParams(coefficientId);

            var coefficientForm = new SetParamForm(isEdit, coefficient);

            if (coefficientForm.ShowDialog() == DialogResult.OK)
            {
                LoadCoefficientParamsList();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (SetParams) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetCoefficientForm(true, selectedRow.ID);
            }
            else
            {
                 MessageBox.Show(Resources.CoefficientIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                               MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteCoefficientQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var selectedRow = (SetParams)mainGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    Content.SetParamsManager.DeleteSetParams(new SetParams { ID = selectedRow.ID });
                    LoadCoefficientParamsList();
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadCoefficientParamsList();
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var coefficientId = (int)view.GetRowCellValue(info.RowHandle, columnId);
                GetCoefficientForm(true, coefficientId);
            }
        }

        private void mainGridView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column.FieldName == "CutleryID")
            {
                var value = (int) mainGridView.GetRowCellValue(e.RowHandle, "CutleryID");
                var cuttlery = Content.CutleryRepository.GetCutleryList().FirstOrDefault(x => x.ID == value);
                if (cuttlery != null) e.DisplayText = cuttlery.Name;
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}