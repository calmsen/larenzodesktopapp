﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Partners;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.Properties;
using Font = System.Drawing.Font;

namespace LaRenzo.Forms.Catalog.PartnersForms
{
    public partial class PartnerListForm : Form
    {
		/// <summary> Структура отображения информации по партнёру </summary>
	    private struct PartnerWithProducts
	    {
		    /// <summary> Партнёр </summary>
			public Partner Partner1 { get; set; }
		    /// <summary> Список постовляемых продуктов </summary>
			public List<PartnerOfProduct> Products { get; set; }
			/// <summary> Постовляемые продукты в строку </summary>
			public string AllProductsNames { get; set; }
	    }
		
		public PartnerListForm()
        {
            InitializeComponent();
            LoadPartnerList();
        }

        private void LoadPartnerList()
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
			mainGridControl.DataSource = GetPartnerWithProducts();
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var partnerForm = new PartnerForm();
            if (partnerForm.ShowDialog() == DialogResult.OK)
            {
                LoadPartnerList();
            }
        }


		//____________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить список партнёров с продуктами </summary>
		////===========================================================
	    private List<PartnerWithProducts> GetPartnerWithProducts()
	    {
			var partnerWithProducts = new List<PartnerWithProducts>();

			// Получить список партнёров
			List<Partner> partners = Content.PartnerManager.GetPartners();

			// Пройтись по каждому
		    foreach (var partner in partners)
		    {
				// Получить для каждого список продуктов
				var onePartpartnerWithProducts = new PartnerWithProducts
			    {
				    Partner1 = partner,
				    Products = Content.ProductManager.GetAllProductsByPartnerId(partner.ID)
			    };

				// Все продукты засунуть в строку
				foreach (var product in onePartpartnerWithProducts.Products)
				{
					onePartpartnerWithProducts.AllProductsNames += product.Product.Name + " ";
				}

				// Добавить к списку
				partnerWithProducts.Add(onePartpartnerWithProducts);
		    }

			return partnerWithProducts;
	    }



        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadPartnerList();
        }
        
        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (Partner) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetPartnerForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.PartnerIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void GetPartnerForm(bool isEdit, int partnerId)
        {
            Partner partner = Content.PartnerManager.GetPartner(partnerId);

            var partnerForm = new PartnerForm(isEdit, partner);

            if (partnerForm.ShowDialog() == DialogResult.OK)
            {
                LoadPartnerList();
            }
        }

        private void MainGridViewDoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var partnerId = (int) view.GetRowCellValue(info.RowHandle, idColumn);
                GetPartnerForm(true, partnerId);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (Partner)mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetPartnerForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.PartnerIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(Resources.DeletePartnerQuestion, Resources.WarningTitle,
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var selectedRow = (Partner)mainGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    Content.PartnerManager.DeletePartner(new Partner {ID = selectedRow.ID});
                    LoadPartnerList();
                }
            }

        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}
