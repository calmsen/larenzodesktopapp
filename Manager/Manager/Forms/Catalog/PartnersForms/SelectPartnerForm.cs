﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Partners;

namespace LaRenzo.Forms.Catalog.PartnersForms
{
    public partial class SelectPartnerForm : Form
    {
        public Partner Partner { set; get; }

        public SelectPartnerForm()
        {
            InitializeComponent();
            LoadPartnerList();
        }

        private void LoadPartnerList()
        {
            mainGridControl.DataSource = Content.PartnerManager.GetPartners();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            Partner = (Partner) mainGridView.GetFocusedRow();

            if (Partner == null)
                return;
            
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }
    }
}