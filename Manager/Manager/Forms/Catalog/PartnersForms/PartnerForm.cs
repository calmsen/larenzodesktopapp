﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Partners;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.Forms.StaffForms;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.PartnersForms
{
    public partial class PartnerForm : Form
    {
        private readonly bool _isEditWindow;

        private const string EmailRegex = @"\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}\b";

        public PartnerForm()
        {
            InitializeComponent();
            SetControlsParams();
        }

        private void SetControlsParams()
        {
            btnFillFactAddress.Tag = txtFactAddress;
            btnFillAddress.Tag = txtAddress;
            btnPostAddress.Tag = txtPostAddress;
        }

        public PartnerForm(bool isEdit, Partner partner):this()
        {
            _isEditWindow = isEdit;

            WindowsFormProvider.FillControlsFromModel(this, partner);

            cmbPartnerType.SelectedIndex = !partner.PartnerIsIndividual ? 0 : 1;

	        productsGridControl.DataSource = Content.ProductManager.GetAllProductsByPartnerId(partner.ID);

        }private void btnSave_Click(object sender, EventArgs e)
        {
	        if (!CheckRequireFields())
	        {
		        return;
	        }

	        var partner = GetPartnerByControl();

            if (_isEditWindow)
            {
                Content.PartnerManager.UpdatePartner(partner);
            }
            else
            {
                Content.PartnerManager.AddPartner(partner);
            }

            DialogResult = DialogResult.OK;
        }

        private Partner GetPartnerByControl()
        {
            var partner = new Partner
                {
                    ID = string.IsNullOrEmpty(txtID.Text) ? -1 : Convert.ToInt32(txtID.Text.Trim()),
                    PartnerName = txtName.Text,
                    PartnerShortName = txtShortName.Text,
                    PartnerIsIndividual = cmbPartnerType.SelectedIndex == 1,
                    PartnerInn = txtInn.Text,
                    PartnerKpp = txtKpp.Text,
                    PartnerOKPO = txtOkpo.Text,
                    PartnerFactAddress = txtFactAddress.Text,
                    PartnerPostAddress = txtPostAddress.Text,
                    PartnerAddress = txtAddress.Text,
                    PartnerPhone = txtPhone.Text,
                    PartnerFax = txtFax.Text,
                    PartnerEmail = txtEMail.Text,
                    PartnerOtherInfo = txtOtherInfo.Text,
                    PartnerOptionInfo = txtOptionsInfo.Text,
					PartnerRemarkDayOrder = remarkDayOrderTextBox.Text,
					PartnerContactFace = contactFaceTextBox.Text
                };

            return partner;
        }

        private bool CheckRequireFields()
        {
            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                SetError(txtName, Resources.PartnerNameIsNull);
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtShortName.Text))
            {
                SetError(txtShortName, Resources.PartnerNameIsNull);
                return false;
            }

            return true;
        }

        private void SetError(Control control, string message)
        {
            var ep = new ErrorProvider();
            ep.SetError(control, message);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            var regex = new Regex(EmailRegex);
            MessageBox.Show(regex.Match(txtEMail.Text).Success ? Resources.EMailIsValid : Resources.EMailIsNotValid);
        }

        private void btnFillKPP_Click(object sender, EventArgs e)
        {
            if (txtInn.Text.Length >= 4)
            {
                txtKpp.Text = txtInn.Text.Substring(0,4)+ @"01001";
            }
        }

        private void FillAddress_Click(object sender, EventArgs e)
        {
            var btn = sender as Control;
            if (btn != null)
            {
                var txtBox = (TextBox) btn.Tag;

                var addressForm = new FillAddressForm();
                if (addressForm.ShowDialog() == DialogResult.OK)
                {
                    txtBox.Text = addressForm.FullAddress;
                }
            }
        }
    }
}
