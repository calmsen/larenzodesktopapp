﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.Forms.Catalog.Classifier.MeasuresForms;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.ProductForms
{
    public partial class ProductCategoryForm : Form
    {
        private readonly bool _isEditWindow;

        private Measure _measure;

        private Measure Measure
        {
            set
            {
                _measure = value;
                txtMeasure.Text = value.Name;
            }

            get { return _measure; }
        }

        private ProductCategory _parentCategory;

        private ProductCategory ParentCategory
        {
            set 
            { 
                _parentCategory = value;
                txtParent.Text = value.Name;
            }

            get { return _parentCategory; }
        }

        public ProductCategoryForm()
        {
            InitializeComponent();
        }

        public ProductCategoryForm(bool isEdit, ProductCategory category):this()
        {
            _isEditWindow = isEdit;

            WindowsFormProvider.FillControlsFromModel(this, category);

            Measure = Content.MeasureManager.GetMeasure(category.DefaultMeasureID);

            if (category.ParentCategoryID != 0)
                ParentCategory = Content.ProductManager.GetCategoryById(category.ParentCategoryID);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {if (!CheckRequireFields())
                return;

            var productCategory = GetProductCategoryByControl();

            if (_isEditWindow)
            {
                Content.ProductManager.UpdateCategory(productCategory);
            }
            else
            {
                Content.ProductManager.AddCategory(productCategory);
            }

            DialogResult = DialogResult.OK;
        }

        private ProductCategory GetProductCategoryByControl()
        {
            var productCategory = new ProductCategory
                {
                    ID = string.IsNullOrEmpty(txtId.Text) ? -1 : Convert.ToInt32(txtId.Text.Trim()),
                    Name = txtName.Text,
                    DefaultMeasureID = Measure.ID,
                    ParentCategoryID = ParentCategory == null ? 0 : ParentCategory.ID
                };

            return productCategory;
        }

        private bool CheckRequireFields()
        {
            if (Measure == null)
            {
                SetError(btnSelectMeasure, Resources.DefaultMeasureIsNotSelected);
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                SetError(txtName, Resources.CategoryNameIsNotAdded);
                return false;
            }

            return true;
        }

        private void SetError(Control control, string message)
        {
            var ep = new ErrorProvider();
            ep.SetError(control, message);
        }

        private void btnSelectMeasure_Click(object sender, EventArgs e)
        {
            var measureSelectForm = new SelectMeasureForm();

            if (measureSelectForm.ShowDialog() == DialogResult.OK)
            {
                Measure = measureSelectForm.Measure;
            }
        }

        private void btnSelectParent_Click(object sender, EventArgs e)
        {
            var selectCategoryForm = new SelectProductCategoryForm();

            if (selectCategoryForm.ShowDialog() == DialogResult.OK)
            {
                ParentCategory = selectCategoryForm.ProductCategory;
            }
        }
    }
}