﻿namespace LaRenzo.Forms.Catalog.ProductForms
{
    partial class SelectProductForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
			this.treeListCategories = new DevExpress.XtraTreeList.TreeList();
			this.NameCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.IdCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
			this.mainGridControl = new DevExpress.XtraGrid.GridControl();
			this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnCategory = new DevExpress.XtraGrid.Columns.GridColumn();
			this.pnlBottom = new System.Windows.Forms.Panel();
			this.btnSelect = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.horizontSplitContainer = new System.Windows.Forms.SplitContainer();
			this.productsGridControl = new DevExpress.XtraGrid.GridControl();
			this.productGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
			((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
			this.mainSplitContainer.Panel1.SuspendLayout();
			this.mainSplitContainer.Panel2.SuspendLayout();
			this.mainSplitContainer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.treeListCategories)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
			this.pnlBottom.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.horizontSplitContainer)).BeginInit();
			this.horizontSplitContainer.Panel1.SuspendLayout();
			this.horizontSplitContainer.Panel2.SuspendLayout();
			this.horizontSplitContainer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.productsGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.productGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// mainSplitContainer
			// 
			this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.mainSplitContainer.Name = "mainSplitContainer";
			// 
			// mainSplitContainer.Panel1
			// 
			this.mainSplitContainer.Panel1.Controls.Add(this.treeListCategories);
			// 
			// mainSplitContainer.Panel2
			// 
			this.mainSplitContainer.Panel2.Controls.Add(this.mainGridControl);
			this.mainSplitContainer.Size = new System.Drawing.Size(668, 366);
			this.mainSplitContainer.SplitterDistance = 222;
			this.mainSplitContainer.TabIndex = 1;
			// 
			// treeListCategories
			// 
			this.treeListCategories.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.NameCol,
            this.IdCol});
			this.treeListCategories.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeListCategories.DragNodesMode = DevExpress.XtraTreeList.TreeListDragNodesMode.Standard;
			this.treeListCategories.Location = new System.Drawing.Point(0, 0);
			this.treeListCategories.Name = "treeListCategories";
			this.treeListCategories.OptionsBehavior.DragNodes = true;
			this.treeListCategories.OptionsBehavior.Editable = false;
			this.treeListCategories.OptionsBehavior.EnableFiltering = true;
			this.treeListCategories.ParentFieldName = "ParentCategoryID";
			this.treeListCategories.Size = new System.Drawing.Size(222, 366);
			this.treeListCategories.TabIndex = 8;
			this.treeListCategories.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListCategories_FocusedNodeChanged);
			// 
			// NameCol
			// 
			this.NameCol.Caption = "Категории";
			this.NameCol.FieldName = "Name";
			this.NameCol.MinWidth = 37;
			this.NameCol.Name = "NameCol";
			this.NameCol.Visible = true;
			this.NameCol.VisibleIndex = 0;
			// 
			// IdCol
			// 
			this.IdCol.Caption = "ID";
			this.IdCol.FieldName = "ID";
			this.IdCol.Name = "IdCol";
			// 
			// mainGridControl
			// 
			this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainGridControl.Location = new System.Drawing.Point(0, 0);
			this.mainGridControl.MainView = this.mainGridView;
			this.mainGridControl.Name = "mainGridControl";
			this.mainGridControl.Size = new System.Drawing.Size(442, 366);
			this.mainGridControl.TabIndex = 8;
			this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
			this.mainGridControl.DoubleClick += new System.EventHandler(this.mainGridControl_DoubleClick);
			// 
			// mainGridView
			// 
			this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnName,
            this.columnCategory});
			this.mainGridView.GridControl = this.mainGridControl;
			this.mainGridView.Name = "mainGridView";
			this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
			this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
			this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
			// 
			// columnId
			// 
			this.columnId.Caption = "ИД";
			this.columnId.FieldName = "ID";
			this.columnId.Name = "columnId";
			this.columnId.OptionsColumn.AllowEdit = false;
			this.columnId.OptionsColumn.ReadOnly = true;
			this.columnId.Visible = true;
			this.columnId.VisibleIndex = 0;
			this.columnId.Width = 67;
			// 
			// columnName
			// 
			this.columnName.Caption = "Наименование";
			this.columnName.FieldName = "Name";
			this.columnName.Name = "columnName";
			this.columnName.OptionsColumn.AllowEdit = false;
			this.columnName.OptionsColumn.ReadOnly = true;
			this.columnName.Visible = true;
			this.columnName.VisibleIndex = 1;
			this.columnName.Width = 419;
			// 
			// columnCategory
			// 
			this.columnCategory.Caption = "Категория";
			this.columnCategory.FieldName = "Category.Name";
			this.columnCategory.Name = "columnCategory";
			this.columnCategory.Visible = true;
			this.columnCategory.VisibleIndex = 2;
			// 
			// pnlBottom
			// 
			this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlBottom.Controls.Add(this.btnSelect);
			this.pnlBottom.Controls.Add(this.btnCancel);
			this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottom.Location = new System.Drawing.Point(0, 544);
			this.pnlBottom.Name = "pnlBottom";
			this.pnlBottom.Size = new System.Drawing.Size(668, 24);
			this.pnlBottom.TabIndex = 10;
			// 
			// btnSelect
			// 
			this.btnSelect.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnSelect.Location = new System.Drawing.Point(492, 0);
			this.btnSelect.Name = "btnSelect";
			this.btnSelect.Size = new System.Drawing.Size(87, 22);
			this.btnSelect.TabIndex = 3;
			this.btnSelect.Text = "Выбрать";
			this.btnSelect.UseVisualStyleBackColor = true;
			this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
			this.btnCancel.Location = new System.Drawing.Point(579, 0);
			this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(87, 22);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "Отмена";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// horizontSplitContainer
			// 
			this.horizontSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.horizontSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.horizontSplitContainer.Name = "horizontSplitContainer";
			this.horizontSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// horizontSplitContainer.Panel1
			// 
			this.horizontSplitContainer.Panel1.Controls.Add(this.mainSplitContainer);
			// 
			// horizontSplitContainer.Panel2
			// 
			this.horizontSplitContainer.Panel2.Controls.Add(this.productsGridControl);
			this.horizontSplitContainer.Size = new System.Drawing.Size(668, 544);
			this.horizontSplitContainer.SplitterDistance = 366;
			this.horizontSplitContainer.TabIndex = 11;
			// 
			// productsGridControl
			// 
			this.productsGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.productsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.productsGridControl.Location = new System.Drawing.Point(0, 0);
			this.productsGridControl.MainView = this.productGridView;
			this.productsGridControl.Name = "productsGridControl";
			this.productsGridControl.Size = new System.Drawing.Size(668, 174);
			this.productsGridControl.TabIndex = 9;
			this.productsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.productGridView});
			this.productsGridControl.DoubleClick += new System.EventHandler(this.productsGridControl_DoubleClick);
			// 
			// productGridView
			// 
			this.productGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
			this.productGridView.GridControl = this.productsGridControl;
			this.productGridView.Name = "productGridView";
			this.productGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
			this.productGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
			// 
			// gridColumn1
			// 
			this.gridColumn1.Caption = "ИД";
			this.gridColumn1.FieldName = "ID";
			this.gridColumn1.Name = "gridColumn1";
			this.gridColumn1.OptionsColumn.AllowEdit = false;
			this.gridColumn1.OptionsColumn.ReadOnly = true;
			this.gridColumn1.Visible = true;
			this.gridColumn1.VisibleIndex = 0;
			this.gridColumn1.Width = 67;
			// 
			// gridColumn2
			// 
			this.gridColumn2.Caption = "Наименование";
			this.gridColumn2.FieldName = "Name";
			this.gridColumn2.Name = "gridColumn2";
			this.gridColumn2.OptionsColumn.AllowEdit = false;
			this.gridColumn2.OptionsColumn.ReadOnly = true;
			this.gridColumn2.Visible = true;
			this.gridColumn2.VisibleIndex = 1;
			this.gridColumn2.Width = 419;
			// 
			// gridColumn3
			// 
			this.gridColumn3.Caption = "Категория";
			this.gridColumn3.FieldName = "Category.Name";
			this.gridColumn3.Name = "gridColumn3";
			this.gridColumn3.Visible = true;
			this.gridColumn3.VisibleIndex = 2;
			// 
			// SelectProductForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(668, 568);
			this.Controls.Add(this.horizontSplitContainer);
			this.Controls.Add(this.pnlBottom);
			this.Name = "SelectProductForm";
			this.Text = "Выберите продукт";
			this.Load += new System.EventHandler(this.SelectProductForm_Load);
			this.mainSplitContainer.Panel1.ResumeLayout(false);
			this.mainSplitContainer.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
			this.mainSplitContainer.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.treeListCategories)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
			this.pnlBottom.ResumeLayout(false);
			this.horizontSplitContainer.Panel1.ResumeLayout(false);
			this.horizontSplitContainer.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.horizontSplitContainer)).EndInit();
			this.horizontSplitContainer.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.productsGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.productGridView)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private DevExpress.XtraTreeList.TreeList treeListCategories;
        private DevExpress.XtraTreeList.Columns.TreeListColumn NameCol;
        private DevExpress.XtraTreeList.Columns.TreeListColumn IdCol;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnCategory;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.SplitContainer horizontSplitContainer;
		private DevExpress.XtraGrid.GridControl productsGridControl;
		private DevExpress.XtraGrid.Views.Grid.GridView productGridView;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    }
}