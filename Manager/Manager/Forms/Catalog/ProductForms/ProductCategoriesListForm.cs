﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.ProductForms
{
    public partial class ProductCategoriesListForm : Form
    {
        public ProductCategoriesListForm()
        {
            InitializeComponent();
        }

        private void LoadCategoriesTree()
        {
            var node = treeListCategories.FocusedNode;
            treeListCategories.DataSource = Content.ProductManager.GetCategoriesList();
            if (node!= null)
                treeListCategories.FocusedNode = treeListCategories.FindNodeByID(node.Id);    
            
        }

        private void ProductCategoriesListForm_Load(object sender, EventArgs e)
        {
            LoadCategoriesTree();
        }

        private void treeListCategories_DragDrop(object sender, DragEventArgs e)
        {
            var treeList = sender as TreeList;
            if (treeList != null)
            {
                Point point = treeList.PointToClient(new Point(e.X, e.Y));

                var dragNode = e.Data.GetData(typeof(TreeListNode)) as TreeListNode;
                var targetNode = treeList.CalcHitInfo(point).Node;

                if (treeList.CalcHitInfo(point).HitInfoType == HitInfoType.Empty)
                {
                    targetNode = null;
                }

                treeList.MoveNode(dragNode, targetNode);

                if (dragNode != null)
                {
                    var changeCatId = (int)dragNode.GetValue(IdCol);
                    var parentCatId = (targetNode == null) ? 0 : (int)targetNode.GetValue(IdCol);

                    Content.ProductManager.ChangeCategoryParent(changeCatId, parentCatId);
                }
            }

            e.Effect = DragDropEffects.Move;
        }

        private void treeListCategories_DragOver(object sender, DragEventArgs e)
        {
            var treeList = sender as TreeList;
            var point = new Point(e.X, e.Y);
            if (treeList != null)
            {
                point = treeList.PointToClient(point);
                if (treeList.CalcHitInfo(point).HitInfoType == HitInfoType.Empty)
                    e.Effect = DragDropEffects.Move;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var productCategory = new ProductCategoryForm();
            if (productCategory.ShowDialog() == DialogResult.OK)
            {
                LoadCategoriesTree();
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedValue = (int?)treeListCategories.FocusedNode.GetValue(IdCol);
            if (selectedValue != null)
            {
                GetCategoryForm(false, selectedValue.Value);
            }
            else
            {
                MessageBox.Show(Resources.ProductCategoryIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void GetCategoryForm(bool isEdit, int id)
        {
            var category = Content.ProductManager.GetCategoryById(id);

            var categoryForm = new ProductCategoryForm(isEdit, category);

            if (categoryForm.ShowDialog() == DialogResult.OK)
            {
                LoadCategoriesTree();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedValue = (int?)treeListCategories.FocusedNode.GetValue(IdCol);
            if (selectedValue != null)
            {
                GetCategoryForm(true, selectedValue.Value);
            }
            else
            {
                MessageBox.Show(Resources.ProductCategoryIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadCategoriesTree();
        }

        private void treeListCategories_DoubleClick(object sender, EventArgs e)
        {
            var selectedValue = (int?)treeListCategories.FocusedNode.GetValue(IdCol);
            if (selectedValue != null)
            {
                GetCategoryForm(true, selectedValue.Value);
            }
            else
            {
                MessageBox.Show(Resources.ProductCategoryIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteProductCategoriesQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
                var selectedValue = (int?)treeListCategories.FocusedNode.GetValue(IdCol);
                if (selectedValue != null)
                {
                    try
                    {
                        Content.ProductManager.RemoveCategory(selectedValue.Value);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format(Resources.CantDeleteProductCategory, ex.Message),
                                        Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    
                    LoadCategoriesTree();
                } 
            }
        }
    }
}