﻿using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.WarehouseForms
{
    public partial class WarehouseListForm : Form
    {
        public WarehouseListForm()
        {
            InitializeComponent();
            LoadWarehouseList();
        }

        private void LoadWarehouseList()
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            const int ChurkinPointOfSaleId = 2;
            mainGridControl.DataSource = Content.WarehouseManager.GetWarehouses().Where(x => x.PointOfSaleId != ChurkinPointOfSaleId);
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            var warehouseForm = new WarehouseForm();
            if (warehouseForm.ShowDialog() == DialogResult.OK)
            {
                LoadWarehouseList();
            }
        }

        private void btnCopy_Click(object sender, System.EventArgs e)
        {
            var selectedRow = (Warehouse) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetWarehouseForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.WarehouseIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnRefresh_Click(object sender, System.EventArgs e)
        {
            LoadWarehouseList();
        }

        private void GetWarehouseForm(bool isEdit, int warehouseId)
        {
            Warehouse warehouse = Content.WarehouseManager.GetWarehouse(warehouseId);

            var warehouseForm = new WarehouseForm(isEdit, warehouse);

            if (warehouseForm.ShowDialog()== DialogResult.OK)
            {
                LoadWarehouseList();
            }
        }

        private void mainGridView_DoubleClick(object sender, System.EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var warehouseId = (int) view.GetRowCellValue(info.RowHandle, columnId);
                GetWarehouseForm(true, warehouseId);
            }
        }

        private void btnEdit_Click(object sender, System.EventArgs e)
        {
            var selectedRow = (Warehouse) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
               GetWarehouseForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.WarehouseIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, System.EventArgs e)
        {
            if (
                MessageBox.Show(Resources.DeleteWarehouseQuestions, Resources.WarningTitle, 
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var selectedRow = (Warehouse) mainGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    Content.WarehouseManager.DeleteWarehouse(new Warehouse {ID = selectedRow.ID});
                    LoadWarehouseList();
                }
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, System.EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}