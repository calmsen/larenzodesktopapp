﻿namespace LaRenzo.Forms.Catalog.WarehouseForms
{
    partial class WarehouseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.lblFullName = new System.Windows.Forms.Label();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chbIsBase = new System.Windows.Forms.CheckBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxPointOfSale = new System.Windows.Forms.ComboBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxWarehouseForDish = new System.Windows.Forms.ComboBox();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtFullName
            // 
            this.txtFullName.Location = new System.Drawing.Point(164, 12);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(248, 20);
            this.txtFullName.TabIndex = 10;
            this.txtFullName.Tag = "name";
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = true;
            this.lblFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFullName.Location = new System.Drawing.Point(3, 15);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(145, 13);
            this.lblFullName.TabIndex = 9;
            this.lblFullName.Text = "Наименование полное:";
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 157);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(424, 24);
            this.pnlBottom.TabIndex = 11;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(248, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Записать";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(335, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chbIsBase
            // 
            this.chbIsBase.AutoSize = true;
            this.chbIsBase.Location = new System.Drawing.Point(164, 116);
            this.chbIsBase.Name = "chbIsBase";
            this.chbIsBase.Size = new System.Drawing.Size(109, 17);
            this.chbIsBase.TabIndex = 12;
            this.chbIsBase.Tag = "IsBase";
            this.chbIsBase.Text = "Основной склад";
            this.chbIsBase.UseVisualStyleBackColor = true;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(9, 113);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(122, 20);
            this.txtID.TabIndex = 10;
            this.txtID.Tag = "id";
            this.txtID.Visible = false;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(6, 41);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(72, 13);
            this.labelControl4.TabIndex = 2044;
            this.labelControl4.Text = "Точка продаж";
            // 
            // comboBoxPointOfSale
            // 
            this.comboBoxPointOfSale.FormattingEnabled = true;
            this.comboBoxPointOfSale.Location = new System.Drawing.Point(164, 38);
            this.comboBoxPointOfSale.Name = "comboBoxPointOfSale";
            this.comboBoxPointOfSale.Size = new System.Drawing.Size(248, 21);
            this.comboBoxPointOfSale.TabIndex = 2043;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 68);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(85, 13);
            this.labelControl1.TabIndex = 2046;
            this.labelControl1.Text = "Склад для блюд";
            // 
            // comboBoxWarehouseForDish
            // 
            this.comboBoxWarehouseForDish.FormattingEnabled = true;
            this.comboBoxWarehouseForDish.Location = new System.Drawing.Point(164, 65);
            this.comboBoxWarehouseForDish.Name = "comboBoxWarehouseForDish";
            this.comboBoxWarehouseForDish.Size = new System.Drawing.Size(248, 21);
            this.comboBoxWarehouseForDish.TabIndex = 2045;
            // 
            // WarehouseForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(424, 181);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.comboBoxWarehouseForDish);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.chbIsBase);
            this.Controls.Add(this.comboBoxPointOfSale);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.txtFullName);
            this.Controls.Add(this.lblFullName);
            this.MaximumSize = new System.Drawing.Size(440, 220);
            this.MinimumSize = new System.Drawing.Size(440, 170);
            this.Name = "WarehouseForm";
            this.Text = "Склад";
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFullName;
        private System.Windows.Forms.Label lblFullName;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chbIsBase;
        private System.Windows.Forms.TextBox txtID;
        protected DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.ComboBox comboBoxPointOfSale;
        protected DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.ComboBox comboBoxWarehouseForDish;
    }
}