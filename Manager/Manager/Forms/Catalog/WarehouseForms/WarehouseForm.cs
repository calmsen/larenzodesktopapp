﻿using System;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.WarehouseForms
{
    public partial class WarehouseForm : Form
    {
        private readonly bool _isEditWindow;

        public WarehouseForm()
        {
            InitializeComponent();
            var points = Content.PointOfSaleRepository.GetItems();
            comboBoxPointOfSale.DataSource = points;
            comboBoxPointOfSale.DisplayMember = "PointName";
            comboBoxPointOfSale.ValueMember = "Id";
            comboBoxPointOfSale.SelectedValue = points.FirstOrDefault()?.ID;

            var warehouseForDishes = Content.WarehouseForDishManager.GetWarehouses();
            comboBoxWarehouseForDish.DataSource = warehouseForDishes;
            comboBoxWarehouseForDish.DisplayMember = "Name";
            comboBoxWarehouseForDish.ValueMember = "Id";
            comboBoxWarehouseForDish.SelectedValue = warehouseForDishes.FirstOrDefault()?.ID;
        }

        public WarehouseForm(bool isEditWindow, Warehouse warehouse) : this()
        {
            _isEditWindow = isEditWindow;
            WindowsFormProvider.FillControlsFromModel(this, warehouse);
            comboBoxPointOfSale.SelectedValue = warehouse.PointOfSaleId;
            if (warehouse.WarehouseForDishId != null)
            {
                comboBoxWarehouseForDish.SelectedValue = warehouse.WarehouseForDishId;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFullName.Text))
            {
                var ep = new ErrorProvider();
                ep.SetError(txtFullName, Resources.NameIsEmpty);
                return;
            }

            var warehouse = new Warehouse
            {
                ID = string.IsNullOrEmpty(txtID.Text) ? 0 : Convert.ToInt32(txtID.Text.Trim()),
                Name = txtFullName.Text.Trim(),
                IsBase = chbIsBase.Checked,
                PointOfSaleId = (int)comboBoxPointOfSale.SelectedValue,
                WarehouseForDishId = (int)comboBoxWarehouseForDish.SelectedValue,
            };

            if (_isEditWindow)
            {
                Content.WarehouseManager.UpdateWarehouse(warehouse);
            }
            else
            {
                Content.WarehouseManager.AddWarehouse(warehouse);
            }

            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}