﻿namespace LaRenzo.Forms.Catalog.WarehouseForms
{
    partial class WarehouseListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPointOfSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIsBase = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainToolstrip = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.separator = new System.Windows.Forms.ToolStripSeparator();
            this.btnCopy = new System.Windows.Forms.ToolStripButton();
            this.btnEdit = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.searchButton = new System.Windows.Forms.ToolStripButton();
            this.gridColumnWarehouseForDish = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.mainToolstrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGridControl
            // 
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 31);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(754, 431);
            this.mainGridControl.TabIndex = 8;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnWarehouseForDish,
            this.columnId,
            this.columnName,
            this.gridColumnPointOfSale,
            this.columnIsBase});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
            this.mainGridView.DoubleClick += new System.EventHandler(this.mainGridView_DoubleClick);
            // 
            // columnId
            // 
            this.columnId.Caption = "ИД";
            this.columnId.FieldName = "ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 0;
            // 
            // columnName
            // 
            this.columnName.Caption = "Наименование";
            this.columnName.FieldName = "Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 1;
            this.columnName.Width = 204;
            // 
            // gridColumnPointOfSale
            // 
            this.gridColumnPointOfSale.Caption = "Точка продаж";
            this.gridColumnPointOfSale.FieldName = "PointOfSale.PointName";
            this.gridColumnPointOfSale.Name = "gridColumnPointOfSale";
            this.gridColumnPointOfSale.Visible = true;
            this.gridColumnPointOfSale.VisibleIndex = 3;
            this.gridColumnPointOfSale.Width = 119;
            // 
            // columnIsBase
            // 
            this.columnIsBase.Caption = "Основной";
            this.columnIsBase.FieldName = "IsBase";
            this.columnIsBase.Name = "columnIsBase";
            this.columnIsBase.OptionsColumn.AllowEdit = false;
            this.columnIsBase.OptionsColumn.ReadOnly = true;
            this.columnIsBase.Visible = true;
            this.columnIsBase.VisibleIndex = 4;
            this.columnIsBase.Width = 84;
            // 
            // mainToolstrip
            // 
            this.mainToolstrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.separator,
            this.btnCopy,
            this.btnEdit,
            this.btnDelete,
            this.separator2,
            this.btnRefresh,
            this.toolStripSeparator1,
            this.searchButton});
            this.mainToolstrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolstrip.Name = "mainToolstrip";
            this.mainToolstrip.Size = new System.Drawing.Size(754, 31);
            this.mainToolstrip.TabIndex = 7;
            this.mainToolstrip.Text = "toolStrip1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(28, 28);
            this.btnAdd.Text = "Создать поставщика";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // separator
            // 
            this.separator.Name = "separator";
            this.separator.Size = new System.Drawing.Size(6, 31);
            // 
            // btnCopy
            // 
            this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopy.Image = global::LaRenzo.Properties.Resources.copy;
            this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(28, 28);
            this.btnCopy.Text = "Скопировать поставщика";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEdit.Image = global::LaRenzo.Properties.Resources.edit_wh;
            this.btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(28, 28);
            this.btnEdit.Text = "Редактировать поставщика";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(28, 28);
            this.btnDelete.Text = "Пометить на удаление";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(6, 31);
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 28);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // searchButton
            // 
            this.searchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchButton.Image = global::LaRenzo.Properties.Resources.search;
            this.searchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(28, 28);
            this.searchButton.Text = "Найти";
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // gridColumnWarehouseForDish
            // 
            this.gridColumnWarehouseForDish.Caption = "Склад для Блюд";
            this.gridColumnWarehouseForDish.FieldName = "WarehouseForDish.Name";
            this.gridColumnWarehouseForDish.Name = "gridColumnWarehouseForDish";
            this.gridColumnWarehouseForDish.Visible = true;
            this.gridColumnWarehouseForDish.VisibleIndex = 2;
            this.gridColumnWarehouseForDish.Width = 254;
            // 
            // WarehouseListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 462);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.mainToolstrip);
            this.Name = "WarehouseListForm";
            this.Text = "Склады";
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.mainToolstrip.ResumeLayout(false);
            this.mainToolstrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private System.Windows.Forms.ToolStrip mainToolstrip;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripSeparator separator;
        private System.Windows.Forms.ToolStripButton btnCopy;
        private System.Windows.Forms.ToolStripButton btnEdit;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.ToolStripSeparator separator2;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsBase;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton searchButton;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPointOfSale;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnWarehouseForDish;
    }
}