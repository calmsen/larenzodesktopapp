﻿namespace LaRenzo.Forms.Catalog.Dishes
{
    partial class CollapseIngridinetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbMain = new System.Windows.Forms.ProgressBar();
            this.tbLog = new System.Windows.Forms.RichTextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // pbMain
            // 
            this.pbMain.Location = new System.Drawing.Point(28, 55);
            this.pbMain.Name = "pbMain";
            this.pbMain.Size = new System.Drawing.Size(365, 37);
            this.pbMain.TabIndex = 0;
            // 
            // tbLog
            // 
            this.tbLog.Location = new System.Drawing.Point(28, 110);
            this.tbLog.Name = "tbLog";
            this.tbLog.Size = new System.Drawing.Size(365, 237);
            this.tbLog.TabIndex = 1;
            this.tbLog.Text = "";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(27, 18);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(359, 25);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "Нажмите кнопку \"Начать объединение\"";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(28, 363);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(365, 50);
            this.button1.TabIndex = 3;
            this.button1.Text = "Начать объеденение";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CollapseIngridinetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 439);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.pbMain);
            this.Name = "CollapseIngridinetForm";
            this.Text = "Объединение ингридиентов";
            this.Load += new System.EventHandler(this.CollapseIngridinetForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar pbMain;
        private System.Windows.Forms.RichTextBox tbLog;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button button1;
    }
}