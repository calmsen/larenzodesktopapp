﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;

namespace LaRenzo.Forms.Catalog.Dishes
{
    public partial class CollapseIngridinetForm : Form
    {
        List<ItemOfDishSet> AllItemsToUpdate = new List<ItemOfDishSet>();

        public CollapseIngridinetForm()
        {
            InitializeComponent();
        }

        private void CollapseIngridinetForm_Load(object sender, EventArgs e)
        {
            
        }

        private void Finish()
        {
            tbLog.Text = "Сохранение...";

            Content.ProductManager.CollapseItemsOfDishSet(AllItemsToUpdate);
            
            tbLog.Text = AllItemsToUpdate.Any()
                ? string.Join(Environment.NewLine,
                    AllItemsToUpdate.Select(x => x.Dish.Name + " >> " + x.Product.Name + " >> " + x.Amount))
                : "Дубликатов не найдено!";

            lblStatus.Text = "Готово";
            button1.Text = "Закрыть";
            button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "Закрыть")
            {
                Close();
                return;
            }

            button1.Enabled = false;
            lblStatus.Text = "Идет анализ ингридиентов...";
            var items = Content.ProductManager.GetAllItemsOfDishSets().GroupBy(x => x.DishId).Select(x => new
            {
                DishID = x.Key,
                Products = x.GroupBy(y => y.ProductId).Select(y => new
                {
                    ID = y.Key,
                    Amount = y.Count(),
                    Data = y.ToList()
                }).ToList()
            }).ToList();

            if (!items.Any())
            {
                Finish();
                return;
            }

            pbMain.Minimum = 0;
            pbMain.Maximum = items.Count();

            foreach (var item in items)
            {
                foreach (var product in item.Products)
                {
                    if (product.Amount < 2) continue;

                    var itemToUpdate = product.Data.First();
                    itemToUpdate.Amount = product.Data.Sum(x => x.Amount);
                    AllItemsToUpdate.Add(itemToUpdate);
                }

                pbMain.Value++;
            }

            Finish();
        }
    }
}
