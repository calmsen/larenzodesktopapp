﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Forms.Catalog.Dishes.Controls;

namespace LaRenzo.Forms.Catalog.Dishes
{
    public partial class DishCategoryListForm : Form
    {
        public DishCategoryListForm()
        {
            InitializeComponent();
        }

        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Загрузить табы брэндов </summary>
        ////===========================================
        private void LoadBrandTabs()
        {
            List<Brand> brands = Content.BrandManager.GetList();

            foreach (Brand brand in brands)
            {
                if (!string.IsNullOrEmpty(brand.Name))
                {
                    DishCategoryControl listByBrandControl = new DishCategoryControl(brand)
                    {
                        Dock = DockStyle.Fill
                    };

                    TabPage tp = new TabPage
                    {
                        Text = brand.Name,
                        TabIndex = brand.ID,
                        Visible = true
                    };

                    tp.Controls.Add(listByBrandControl);

                    tabControl.TabPages.Add(tp);
                }
            }
        }

        private void DishCategoryListForm_Load(object sender, EventArgs e)
        {
            LoadBrandTabs();
        }
    }
}