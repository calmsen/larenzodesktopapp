﻿namespace LaRenzo.Forms.Catalog.Dishes
{
    partial class DishCategoryForm
    {
        /// <summary>
        /// Required designer variable. </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblWeight = new System.Windows.Forms.Label();
            this.txtBoxWeigth = new System.Windows.Forms.TextBox();
            this.cmbBoxCatSection = new System.Windows.Forms.ComboBox();
            this.lblCategoryName = new System.Windows.Forms.Label();
            this.txtCatName = new System.Windows.Forms.TextBox();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblSection = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWeight.Location = new System.Drawing.Point(12, 68);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(139, 13);
            this.lblWeight.TabIndex = 15;
            this.lblWeight.Text = "Вес (для сортировки):";
            // 
            // txtBoxWeigth
            // 
            this.txtBoxWeigth.Location = new System.Drawing.Point(158, 65);
            this.txtBoxWeigth.Name = "txtBoxWeigth";
            this.txtBoxWeigth.Size = new System.Drawing.Size(294, 20);
            this.txtBoxWeigth.TabIndex = 14;
            this.txtBoxWeigth.Tag = "weight";
            // 
            // cmbBoxCatSection
            // 
            this.cmbBoxCatSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxCatSection.FormattingEnabled = true;
            this.cmbBoxCatSection.Location = new System.Drawing.Point(158, 38);
            this.cmbBoxCatSection.Name = "cmbBoxCatSection";
            this.cmbBoxCatSection.Size = new System.Drawing.Size(294, 21);
            this.cmbBoxCatSection.TabIndex = 13;
            this.cmbBoxCatSection.Tag = "sectionId";
            // 
            // lblCategoryName
            // 
            this.lblCategoryName.AutoSize = true;
            this.lblCategoryName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCategoryName.Location = new System.Drawing.Point(48, 15);
            this.lblCategoryName.Name = "lblCategoryName";
            this.lblCategoryName.Size = new System.Drawing.Size(99, 13);
            this.lblCategoryName.TabIndex = 12;
            this.lblCategoryName.Text = "Наименование:";
            // 
            // txtCatName
            // 
            this.txtCatName.Location = new System.Drawing.Point(158, 12);
            this.txtCatName.Name = "txtCatName";
            this.txtCatName.Size = new System.Drawing.Size(294, 20);
            this.txtCatName.TabIndex = 11;
            this.txtCatName.Tag = "name";
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 98);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(464, 24);
            this.pnlBottom.TabIndex = 41;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(288, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Записать";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(375, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblSection
            // 
            this.lblSection.AutoSize = true;
            this.lblSection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSection.Location = new System.Drawing.Point(114, 41);
            this.lblSection.Name = "lblSection";
            this.lblSection.Size = new System.Drawing.Size(33, 13);
            this.lblSection.TabIndex = 42;
            this.lblSection.Text = "Цех:";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(396, 15);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(56, 20);
            this.txtId.TabIndex = 43;
            this.txtId.Tag = "id";
            this.txtId.Visible = false;
            // 
            // DishCategoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 122);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.lblSection);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.txtBoxWeigth);
            this.Controls.Add(this.cmbBoxCatSection);
            this.Controls.Add(this.lblCategoryName);
            this.Controls.Add(this.txtCatName);
            this.Name = "DishCategoryForm";
            this.Text = "Категория продуктов";
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWeight;
        public System.Windows.Forms.TextBox txtBoxWeigth;
        private System.Windows.Forms.ComboBox cmbBoxCatSection;
        private System.Windows.Forms.Label lblCategoryName;
        public System.Windows.Forms.TextBox txtCatName;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblSection;
        public System.Windows.Forms.TextBox txtId;
    }
}