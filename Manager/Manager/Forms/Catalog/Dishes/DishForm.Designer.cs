﻿namespace LaRenzo.Forms.Catalog.Dishes
{
    partial class DishForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DishForm));
            this.Container = new System.Windows.Forms.TabControl();
            this.tbMainPage = new System.Windows.Forms.TabPage();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AmountColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MeasureColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.mainToolstrip = new System.Windows.Forms.ToolStrip();
            this.btnAddIngridient = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDeleteIngridient = new System.Windows.Forms.ToolStripButton();
            this.tabCheck = new System.Windows.Forms.TabPage();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.txtCTO = new System.Windows.Forms.TextBox();
            this.lblCTO = new System.Windows.Forms.Label();
            this.txtCarbohydrates = new System.Windows.Forms.TextBox();
            this.txtProtein = new System.Windows.Forms.TextBox();
            this.txtFat = new System.Windows.Forms.TextBox();
            this.lblProtein = new System.Windows.Forms.Label();
            this.lblFat = new System.Windows.Forms.Label();
            this.lblCarbohydrates = new System.Windows.Forms.Label();
            this.lblEndTemperature2 = new System.Windows.Forms.Label();
            this.lblEndTemperature = new System.Windows.Forms.Label();
            this.lblStartTemperature = new System.Windows.Forms.Label();
            this.lblFitHour = new System.Windows.Forms.Label();
            this.lblAddressProduction = new System.Windows.Forms.Label();
            this.lblManufactured = new System.Windows.Forms.Label();
            this.lblNetto = new System.Windows.Forms.Label();
            this.txtAddressProduction = new System.Windows.Forms.TextBox();
            this.txtManufactured = new System.Windows.Forms.TextBox();
            this.txtEndTemperature = new System.Windows.Forms.TextBox();
            this.txtStartTemperature = new System.Windows.Forms.TextBox();
            this.txtFitHour = new System.Windows.Forms.TextBox();
            this.txtNetto = new System.Windows.Forms.TextBox();
            this.txtCoeffEnergy = new System.Windows.Forms.TextBox();
            this.lblCoeffEnergy = new System.Windows.Forms.Label();
            this.txtStructure = new System.Windows.Forms.TextBox();
            this.lblStructure = new System.Windows.Forms.Label();
            this.chkMarkEAC = new System.Windows.Forms.CheckBox();
            this.chkMarkGOST = new System.Windows.Forms.CheckBox();
            this.tabOthers = new System.Windows.Forms.TabPage();
            this.transferPanel = new System.Windows.Forms.Panel();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.txtExternalCode = new System.Windows.Forms.TextBox();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.lblExternalCode = new System.Windows.Forms.Label();
            this.tabTopDishes = new System.Windows.Forms.TabPage();
            this.gridTopDishes = new DevExpress.XtraGrid.GridControl();
            this.viewTopDishes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dishName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dishCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sharedOrders = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dishAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.additionalOffer = new System.Windows.Forms.TabPage();
            this.listBoxSelected = new System.Windows.Forms.ListBox();
            this.listBoxDish = new System.Windows.Forms.ListBox();
            this.listBoxCategory = new System.Windows.Forms.ListBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.separator = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lblClientName = new System.Windows.Forms.Label();
            this.txtInternalName = new System.Windows.Forms.TextBox();
            this.lblCategory = new System.Windows.Forms.Label();
            this.cmbDishCat = new System.Windows.Forms.ComboBox();
            this.lblCookingTime = new System.Windows.Forms.Label();
            this.txtCook = new System.Windows.Forms.TextBox();
            this.lblCoef = new System.Windows.Forms.Label();
            this.cmbAFCollections = new System.Windows.Forms.ComboBox();
            this.lblSku = new System.Windows.Forms.Label();
            this.txtSku = new System.Windows.Forms.TextBox();
            this.lblSet = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblCutlery = new System.Windows.Forms.Label();
            this.cmbCutlery = new System.Windows.Forms.ComboBox();
            this.btnSelectCoefficient = new System.Windows.Forms.Button();
            this.chkCalculateCoefficient = new System.Windows.Forms.CheckBox();
            this.cmbCutleryCoefficient = new System.Windows.Forms.ComboBox();
            this.chckDelivery = new System.Windows.Forms.CheckBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.cmbWarehouse = new System.Windows.Forms.ComboBox();
            this.chkCustomWarehouse = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCafePrice = new System.Windows.Forms.TextBox();
            this.chkAlcohol = new System.Windows.Forms.CheckBox();
            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::LaRenzo.Forms.WaitFormGeneral), true, true);
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxDiscountApplicability = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxSection = new System.Windows.Forms.ComboBox();
            this.Container.SuspendLayout();
            this.tbMainPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.mainToolstrip.SuspendLayout();
            this.tabCheck.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.tabOthers.SuspendLayout();
            this.transferPanel.SuspendLayout();
            this.tabTopDishes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTopDishes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewTopDishes)).BeginInit();
            this.additionalOffer.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // Container
            // 
            this.Container.Controls.Add(this.tbMainPage);
            this.Container.Controls.Add(this.tabCheck);
            this.Container.Controls.Add(this.tabOthers);
            this.Container.Controls.Add(this.tabTopDishes);
            this.Container.Controls.Add(this.additionalOffer);
            this.Container.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Container.Location = new System.Drawing.Point(0, 380);
            this.Container.Margin = new System.Windows.Forms.Padding(4);
            this.Container.Name = "Container";
            this.Container.SelectedIndex = 0;
            this.Container.Size = new System.Drawing.Size(785, 274);
            this.Container.TabIndex = 52;
            // 
            // tbMainPage
            // 
            this.tbMainPage.Controls.Add(this.mainGridControl);
            this.tbMainPage.Controls.Add(this.mainToolstrip);
            this.tbMainPage.Location = new System.Drawing.Point(4, 25);
            this.tbMainPage.Margin = new System.Windows.Forms.Padding(4);
            this.tbMainPage.Name = "tbMainPage";
            this.tbMainPage.Padding = new System.Windows.Forms.Padding(4);
            this.tbMainPage.Size = new System.Drawing.Size(777, 245);
            this.tbMainPage.TabIndex = 1;
            this.tbMainPage.Text = "Ингридиенты";
            this.tbMainPage.UseVisualStyleBackColor = true;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Location = new System.Drawing.Point(4, 35);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(769, 206);
            this.mainGridControl.TabIndex = 9;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView,
            this.gridView1});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IdColumn,
            this.NameColumn,
            this.AmountColumn,
            this.MeasureColumn});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            // 
            // IdColumn
            // 
            this.IdColumn.Caption = "ИД";
            this.IdColumn.FieldName = "Product.ID";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.OptionsColumn.AllowEdit = false;
            this.IdColumn.OptionsColumn.AllowFocus = false;
            this.IdColumn.OptionsColumn.ReadOnly = true;
            this.IdColumn.Visible = true;
            this.IdColumn.VisibleIndex = 0;
            this.IdColumn.Width = 100;
            // 
            // NameColumn
            // 
            this.NameColumn.Caption = "Наименование";
            this.NameColumn.FieldName = "Product.Name";
            this.NameColumn.Name = "NameColumn";
            this.NameColumn.OptionsColumn.AllowEdit = false;
            this.NameColumn.OptionsColumn.AllowFocus = false;
            this.NameColumn.OptionsColumn.ReadOnly = true;
            this.NameColumn.Visible = true;
            this.NameColumn.VisibleIndex = 1;
            this.NameColumn.Width = 166;
            // 
            // AmountColumn
            // 
            this.AmountColumn.Caption = "Количество";
            this.AmountColumn.FieldName = "Amount";
            this.AmountColumn.Name = "AmountColumn";
            this.AmountColumn.Visible = true;
            this.AmountColumn.VisibleIndex = 2;
            this.AmountColumn.Width = 166;
            // 
            // MeasureColumn
            // 
            this.MeasureColumn.Caption = "Ед.Изм.";
            this.MeasureColumn.FieldName = "Measure.ShortName";
            this.MeasureColumn.Name = "MeasureColumn";
            this.MeasureColumn.OptionsColumn.AllowEdit = false;
            this.MeasureColumn.OptionsColumn.AllowFocus = false;
            this.MeasureColumn.OptionsColumn.ReadOnly = true;
            this.MeasureColumn.Visible = true;
            this.MeasureColumn.VisibleIndex = 3;
            this.MeasureColumn.Width = 125;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.mainGridControl;
            this.gridView1.Name = "gridView1";
            // 
            // mainToolstrip
            // 
            this.mainToolstrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddIngridient,
            this.toolStripSeparator1,
            this.btnDeleteIngridient});
            this.mainToolstrip.Location = new System.Drawing.Point(4, 4);
            this.mainToolstrip.Name = "mainToolstrip";
            this.mainToolstrip.Size = new System.Drawing.Size(769, 31);
            this.mainToolstrip.TabIndex = 10;
            this.mainToolstrip.Text = "toolStrip1";
            // 
            // btnAddIngridient
            // 
            this.btnAddIngridient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddIngridient.Image = ((System.Drawing.Image)(resources.GetObject("btnAddIngridient.Image")));
            this.btnAddIngridient.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddIngridient.Name = "btnAddIngridient";
            this.btnAddIngridient.Size = new System.Drawing.Size(28, 28);
            this.btnAddIngridient.Text = "Создать коэфициент";
            this.btnAddIngridient.Click += new System.EventHandler(this.AddIngridientButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // btnDeleteIngridient
            // 
            this.btnDeleteIngridient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeleteIngridient.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteIngridient.Image")));
            this.btnDeleteIngridient.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeleteIngridient.Name = "btnDeleteIngridient";
            this.btnDeleteIngridient.Size = new System.Drawing.Size(28, 28);
            this.btnDeleteIngridient.Text = "Пометить на удаление";
            this.btnDeleteIngridient.Click += new System.EventHandler(this.DeleteIngridientButton_Click);
            // 
            // tabCheck
            // 
            this.tabCheck.Controls.Add(this.mainPanel);
            this.tabCheck.Location = new System.Drawing.Point(4, 25);
            this.tabCheck.Margin = new System.Windows.Forms.Padding(4);
            this.tabCheck.Name = "tabCheck";
            this.tabCheck.Padding = new System.Windows.Forms.Padding(4);
            this.tabCheck.Size = new System.Drawing.Size(777, 245);
            this.tabCheck.TabIndex = 2;
            this.tabCheck.Text = "Маркировочный чек";
            this.tabCheck.UseVisualStyleBackColor = true;
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.SystemColors.Control;
            this.mainPanel.Controls.Add(this.txtCTO);
            this.mainPanel.Controls.Add(this.lblCTO);
            this.mainPanel.Controls.Add(this.txtCarbohydrates);
            this.mainPanel.Controls.Add(this.txtProtein);
            this.mainPanel.Controls.Add(this.txtFat);
            this.mainPanel.Controls.Add(this.lblProtein);
            this.mainPanel.Controls.Add(this.lblFat);
            this.mainPanel.Controls.Add(this.lblCarbohydrates);
            this.mainPanel.Controls.Add(this.lblEndTemperature2);
            this.mainPanel.Controls.Add(this.lblEndTemperature);
            this.mainPanel.Controls.Add(this.lblStartTemperature);
            this.mainPanel.Controls.Add(this.lblFitHour);
            this.mainPanel.Controls.Add(this.lblAddressProduction);
            this.mainPanel.Controls.Add(this.lblManufactured);
            this.mainPanel.Controls.Add(this.lblNetto);
            this.mainPanel.Controls.Add(this.txtAddressProduction);
            this.mainPanel.Controls.Add(this.txtManufactured);
            this.mainPanel.Controls.Add(this.txtEndTemperature);
            this.mainPanel.Controls.Add(this.txtStartTemperature);
            this.mainPanel.Controls.Add(this.txtFitHour);
            this.mainPanel.Controls.Add(this.txtNetto);
            this.mainPanel.Controls.Add(this.txtCoeffEnergy);
            this.mainPanel.Controls.Add(this.lblCoeffEnergy);
            this.mainPanel.Controls.Add(this.txtStructure);
            this.mainPanel.Controls.Add(this.lblStructure);
            this.mainPanel.Controls.Add(this.chkMarkEAC);
            this.mainPanel.Controls.Add(this.chkMarkGOST);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(4, 4);
            this.mainPanel.Margin = new System.Windows.Forms.Padding(4);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(769, 237);
            this.mainPanel.TabIndex = 0;
            // 
            // txtCTO
            // 
            this.txtCTO.Location = new System.Drawing.Point(488, 206);
            this.txtCTO.Margin = new System.Windows.Forms.Padding(4);
            this.txtCTO.Name = "txtCTO";
            this.txtCTO.Size = new System.Drawing.Size(271, 22);
            this.txtCTO.TabIndex = 16;
            this.txtCTO.Tag = "CTO";
            // 
            // lblCTO
            // 
            this.lblCTO.AutoSize = true;
            this.lblCTO.Location = new System.Drawing.Point(447, 209);
            this.lblCTO.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCTO.Name = "lblCTO";
            this.lblCTO.Size = new System.Drawing.Size(41, 17);
            this.lblCTO.TabIndex = 15;
            this.lblCTO.Text = "СТО:";
            // 
            // txtCarbohydrates
            // 
            this.txtCarbohydrates.Location = new System.Drawing.Point(627, 110);
            this.txtCarbohydrates.Margin = new System.Windows.Forms.Padding(4);
            this.txtCarbohydrates.Name = "txtCarbohydrates";
            this.txtCarbohydrates.Size = new System.Drawing.Size(132, 22);
            this.txtCarbohydrates.TabIndex = 12;
            this.txtCarbohydrates.Tag = "Carbohydrates";
            this.txtCarbohydrates.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // txtProtein
            // 
            this.txtProtein.Location = new System.Drawing.Point(176, 110);
            this.txtProtein.Margin = new System.Windows.Forms.Padding(4);
            this.txtProtein.Name = "txtProtein";
            this.txtProtein.Size = new System.Drawing.Size(132, 22);
            this.txtProtein.TabIndex = 13;
            this.txtProtein.Tag = "Protein";
            this.txtProtein.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // txtFat
            // 
            this.txtFat.Location = new System.Drawing.Point(384, 110);
            this.txtFat.Margin = new System.Windows.Forms.Padding(4);
            this.txtFat.Name = "txtFat";
            this.txtFat.Size = new System.Drawing.Size(132, 22);
            this.txtFat.TabIndex = 14;
            this.txtFat.Tag = "Fat";
            this.txtFat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // lblProtein
            // 
            this.lblProtein.AutoSize = true;
            this.lblProtein.Location = new System.Drawing.Point(113, 113);
            this.lblProtein.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProtein.Name = "lblProtein";
            this.lblProtein.Size = new System.Drawing.Size(52, 17);
            this.lblProtein.TabIndex = 9;
            this.lblProtein.Text = "Белки:";
            // 
            // lblFat
            // 
            this.lblFat.AutoSize = true;
            this.lblFat.Location = new System.Drawing.Point(321, 113);
            this.lblFat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFat.Name = "lblFat";
            this.lblFat.Size = new System.Drawing.Size(51, 17);
            this.lblFat.TabIndex = 10;
            this.lblFat.Text = "Жиры:";
            // 
            // lblCarbohydrates
            // 
            this.lblCarbohydrates.AutoSize = true;
            this.lblCarbohydrates.Location = new System.Drawing.Point(528, 113);
            this.lblCarbohydrates.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCarbohydrates.Name = "lblCarbohydrates";
            this.lblCarbohydrates.Size = new System.Drawing.Size(75, 17);
            this.lblCarbohydrates.TabIndex = 11;
            this.lblCarbohydrates.Text = "Углеводы:";
            // 
            // lblEndTemperature2
            // 
            this.lblEndTemperature2.AutoSize = true;
            this.lblEndTemperature2.Location = new System.Drawing.Point(736, 177);
            this.lblEndTemperature2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEndTemperature2.Name = "lblEndTemperature2";
            this.lblEndTemperature2.Size = new System.Drawing.Size(17, 17);
            this.lblEndTemperature2.TabIndex = 8;
            this.lblEndTemperature2.Text = "C";
            // 
            // lblEndTemperature
            // 
            this.lblEndTemperature.AutoSize = true;
            this.lblEndTemperature.Location = new System.Drawing.Point(583, 177);
            this.lblEndTemperature.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEndTemperature.Name = "lblEndTemperature";
            this.lblEndTemperature.Size = new System.Drawing.Size(37, 17);
            this.lblEndTemperature.TabIndex = 8;
            this.lblEndTemperature.Text = "C до";
            // 
            // lblStartTemperature
            // 
            this.lblStartTemperature.AutoSize = true;
            this.lblStartTemperature.Location = new System.Drawing.Point(283, 177);
            this.lblStartTemperature.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStartTemperature.Name = "lblStartTemperature";
            this.lblStartTemperature.Size = new System.Drawing.Size(183, 17);
            this.lblStartTemperature.TabIndex = 8;
            this.lblStartTemperature.Text = "часов при температуре от";
            // 
            // lblFitHour
            // 
            this.lblFitHour.AutoSize = true;
            this.lblFitHour.Location = new System.Drawing.Point(45, 177);
            this.lblFitHour.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFitHour.Name = "lblFitHour";
            this.lblFitHour.Size = new System.Drawing.Size(122, 17);
            this.lblFitHour.TabIndex = 7;
            this.lblFitHour.Text = "Годен в течении:";
            // 
            // lblAddressProduction
            // 
            this.lblAddressProduction.AutoSize = true;
            this.lblAddressProduction.Location = new System.Drawing.Point(15, 241);
            this.lblAddressProduction.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddressProduction.Name = "lblAddressProduction";
            this.lblAddressProduction.Size = new System.Drawing.Size(147, 17);
            this.lblAddressProduction.TabIndex = 6;
            this.lblAddressProduction.Text = "Адрес производства:";
            // 
            // lblManufactured
            // 
            this.lblManufactured.AutoSize = true;
            this.lblManufactured.Location = new System.Drawing.Point(60, 209);
            this.lblManufactured.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblManufactured.Name = "lblManufactured";
            this.lblManufactured.Size = new System.Drawing.Size(102, 17);
            this.lblManufactured.TabIndex = 6;
            this.lblManufactured.Text = "Изготовитель:";
            // 
            // lblNetto
            // 
            this.lblNetto.AutoSize = true;
            this.lblNetto.Location = new System.Drawing.Point(73, 145);
            this.lblNetto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNetto.Name = "lblNetto";
            this.lblNetto.Size = new System.Drawing.Size(91, 17);
            this.lblNetto.TabIndex = 5;
            this.lblNetto.Text = "Масса нетто";
            // 
            // txtAddressProduction
            // 
            this.txtAddressProduction.Location = new System.Drawing.Point(176, 238);
            this.txtAddressProduction.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddressProduction.Name = "txtAddressProduction";
            this.txtAddressProduction.Size = new System.Drawing.Size(583, 22);
            this.txtAddressProduction.TabIndex = 1;
            this.txtAddressProduction.Tag = "AddressProduction";
            // 
            // txtManufactured
            // 
            this.txtManufactured.Location = new System.Drawing.Point(176, 206);
            this.txtManufactured.Margin = new System.Windows.Forms.Padding(4);
            this.txtManufactured.Name = "txtManufactured";
            this.txtManufactured.Size = new System.Drawing.Size(264, 22);
            this.txtManufactured.TabIndex = 1;
            this.txtManufactured.Tag = "Manufacturer";
            // 
            // txtEndTemperature
            // 
            this.txtEndTemperature.Location = new System.Drawing.Point(629, 174);
            this.txtEndTemperature.Margin = new System.Windows.Forms.Padding(4);
            this.txtEndTemperature.Name = "txtEndTemperature";
            this.txtEndTemperature.Size = new System.Drawing.Size(97, 22);
            this.txtEndTemperature.TabIndex = 1;
            this.txtEndTemperature.Tag = "EndTemperature";
            this.txtEndTemperature.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // txtStartTemperature
            // 
            this.txtStartTemperature.Location = new System.Drawing.Point(476, 174);
            this.txtStartTemperature.Margin = new System.Windows.Forms.Padding(4);
            this.txtStartTemperature.Name = "txtStartTemperature";
            this.txtStartTemperature.Size = new System.Drawing.Size(97, 22);
            this.txtStartTemperature.TabIndex = 1;
            this.txtStartTemperature.Tag = "StartTemperature";
            this.txtStartTemperature.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // txtFitHour
            // 
            this.txtFitHour.Location = new System.Drawing.Point(176, 174);
            this.txtFitHour.Margin = new System.Windows.Forms.Padding(4);
            this.txtFitHour.Name = "txtFitHour";
            this.txtFitHour.Size = new System.Drawing.Size(97, 22);
            this.txtFitHour.TabIndex = 1;
            this.txtFitHour.Tag = "FitHour";
            this.txtFitHour.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // txtNetto
            // 
            this.txtNetto.Location = new System.Drawing.Point(176, 142);
            this.txtNetto.Margin = new System.Windows.Forms.Padding(4);
            this.txtNetto.Name = "txtNetto";
            this.txtNetto.Size = new System.Drawing.Size(97, 22);
            this.txtNetto.TabIndex = 1;
            this.txtNetto.Tag = "Netto";
            this.txtNetto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // txtCoeffEnergy
            // 
            this.txtCoeffEnergy.Location = new System.Drawing.Point(476, 142);
            this.txtCoeffEnergy.Margin = new System.Windows.Forms.Padding(4);
            this.txtCoeffEnergy.Name = "txtCoeffEnergy";
            this.txtCoeffEnergy.Size = new System.Drawing.Size(283, 22);
            this.txtCoeffEnergy.TabIndex = 1;
            this.txtCoeffEnergy.Tag = "CoeffEnergy";
            this.txtCoeffEnergy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // lblCoeffEnergy
            // 
            this.lblCoeffEnergy.AutoSize = true;
            this.lblCoeffEnergy.Location = new System.Drawing.Point(283, 145);
            this.lblCoeffEnergy.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCoeffEnergy.Name = "lblCoeffEnergy";
            this.lblCoeffEnergy.Size = new System.Drawing.Size(184, 17);
            this.lblCoeffEnergy.TabIndex = 4;
            this.lblCoeffEnergy.Text = "Энергетическая ценность:";
            // 
            // txtStructure
            // 
            this.txtStructure.Location = new System.Drawing.Point(176, 43);
            this.txtStructure.Margin = new System.Windows.Forms.Padding(4);
            this.txtStructure.Multiline = true;
            this.txtStructure.Name = "txtStructure";
            this.txtStructure.Size = new System.Drawing.Size(583, 58);
            this.txtStructure.TabIndex = 2;
            this.txtStructure.Tag = "Structure";
            // 
            // lblStructure
            // 
            this.lblStructure.AutoSize = true;
            this.lblStructure.Location = new System.Drawing.Point(107, 63);
            this.lblStructure.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStructure.Name = "lblStructure";
            this.lblStructure.Size = new System.Drawing.Size(58, 17);
            this.lblStructure.TabIndex = 1;
            this.lblStructure.Text = "Состав:";
            // 
            // chkMarkEAC
            // 
            this.chkMarkEAC.AutoSize = true;
            this.chkMarkEAC.Location = new System.Drawing.Point(384, 15);
            this.chkMarkEAC.Margin = new System.Windows.Forms.Padding(4);
            this.chkMarkEAC.Name = "chkMarkEAC";
            this.chkMarkEAC.Size = new System.Drawing.Size(161, 21);
            this.chkMarkEAC.TabIndex = 0;
            this.chkMarkEAC.Tag = "MarkEAC";
            this.chkMarkEAC.Text = "Маркировка по EAC";
            this.chkMarkEAC.UseVisualStyleBackColor = true;
            // 
            // chkMarkGOST
            // 
            this.chkMarkGOST.AutoSize = true;
            this.chkMarkGOST.Location = new System.Drawing.Point(176, 15);
            this.chkMarkGOST.Margin = new System.Windows.Forms.Padding(4);
            this.chkMarkGOST.Name = "chkMarkGOST";
            this.chkMarkGOST.Size = new System.Drawing.Size(184, 21);
            this.chkMarkGOST.TabIndex = 0;
            this.chkMarkGOST.Tag = "MarkGOST";
            this.chkMarkGOST.Text = "Маркировка по ГОСТ Р";
            this.chkMarkGOST.UseVisualStyleBackColor = true;
            // 
            // tabOthers
            // 
            this.tabOthers.Controls.Add(this.transferPanel);
            this.tabOthers.Location = new System.Drawing.Point(4, 25);
            this.tabOthers.Margin = new System.Windows.Forms.Padding(4);
            this.tabOthers.Name = "tabOthers";
            this.tabOthers.Padding = new System.Windows.Forms.Padding(4);
            this.tabOthers.Size = new System.Drawing.Size(777, 245);
            this.tabOthers.TabIndex = 3;
            this.tabOthers.Text = "Дополнительно";
            this.tabOthers.UseVisualStyleBackColor = true;
            // 
            // transferPanel
            // 
            this.transferPanel.BackColor = System.Drawing.SystemColors.Control;
            this.transferPanel.Controls.Add(this.txtBarcode);
            this.transferPanel.Controls.Add(this.txtExternalCode);
            this.transferPanel.Controls.Add(this.lblBarcode);
            this.transferPanel.Controls.Add(this.lblExternalCode);
            this.transferPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.transferPanel.Location = new System.Drawing.Point(4, 4);
            this.transferPanel.Margin = new System.Windows.Forms.Padding(4);
            this.transferPanel.Name = "transferPanel";
            this.transferPanel.Size = new System.Drawing.Size(769, 237);
            this.transferPanel.TabIndex = 0;
            // 
            // txtBarcode
            // 
            this.txtBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBarcode.Location = new System.Drawing.Point(249, 41);
            this.txtBarcode.Margin = new System.Windows.Forms.Padding(4);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(511, 22);
            this.txtBarcode.TabIndex = 1;
            this.txtBarcode.Tag = "Barcode";
            // 
            // txtExternalCode
            // 
            this.txtExternalCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExternalCode.Location = new System.Drawing.Point(249, 9);
            this.txtExternalCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtExternalCode.Name = "txtExternalCode";
            this.txtExternalCode.Size = new System.Drawing.Size(511, 22);
            this.txtExternalCode.TabIndex = 1;
            this.txtExternalCode.Tag = "ChefExpertCode";
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.Location = new System.Drawing.Point(163, 44);
            this.lblBarcode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(75, 17);
            this.lblBarcode.TabIndex = 0;
            this.lblBarcode.Text = "Штрихкод:";
            // 
            // lblExternalCode
            // 
            this.lblExternalCode.AutoSize = true;
            this.lblExternalCode.Location = new System.Drawing.Point(16, 12);
            this.lblExternalCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExternalCode.Name = "lblExternalCode";
            this.lblExternalCode.Size = new System.Drawing.Size(215, 17);
            this.lblExternalCode.TabIndex = 0;
            this.lblExternalCode.Text = "Код в программе Шеф Эксперт:";
            // 
            // tabTopDishes
            // 
            this.tabTopDishes.Controls.Add(this.gridTopDishes);
            this.tabTopDishes.Location = new System.Drawing.Point(4, 25);
            this.tabTopDishes.Margin = new System.Windows.Forms.Padding(4);
            this.tabTopDishes.Name = "tabTopDishes";
            this.tabTopDishes.Size = new System.Drawing.Size(777, 245);
            this.tabTopDishes.TabIndex = 4;
            this.tabTopDishes.Text = "Блюда, которые покупают вместе с этим";
            this.tabTopDishes.UseVisualStyleBackColor = true;
            this.tabTopDishes.Enter += new System.EventHandler(this.tabTopDishes_Enter);
            // 
            // gridTopDishes
            // 
            this.gridTopDishes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTopDishes.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridTopDishes.Location = new System.Drawing.Point(0, 0);
            this.gridTopDishes.MainView = this.viewTopDishes;
            this.gridTopDishes.Margin = new System.Windows.Forms.Padding(4);
            this.gridTopDishes.Name = "gridTopDishes";
            this.gridTopDishes.Size = new System.Drawing.Size(777, 245);
            this.gridTopDishes.TabIndex = 0;
            this.gridTopDishes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewTopDishes});
            // 
            // viewTopDishes
            // 
            this.viewTopDishes.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.dishName,
            this.dishCategory,
            this.sharedOrders,
            this.dishAmount});
            this.viewTopDishes.GridControl = this.gridTopDishes;
            this.viewTopDishes.Name = "viewTopDishes";
            // 
            // dishName
            // 
            this.dishName.Caption = "Блюдо";
            this.dishName.FieldName = "Item1.Name";
            this.dishName.Name = "dishName";
            this.dishName.Visible = true;
            this.dishName.VisibleIndex = 0;
            // 
            // dishCategory
            // 
            this.dishCategory.Caption = "Категория";
            this.dishCategory.FieldName = "Item1.Category.Name";
            this.dishCategory.Name = "dishCategory";
            this.dishCategory.Visible = true;
            this.dishCategory.VisibleIndex = 1;
            // 
            // sharedOrders
            // 
            this.sharedOrders.Caption = "Общих заказов";
            this.sharedOrders.FieldName = "Item3";
            this.sharedOrders.Name = "sharedOrders";
            this.sharedOrders.Visible = true;
            this.sharedOrders.VisibleIndex = 2;
            // 
            // dishAmount
            // 
            this.dishAmount.Caption = "Куплено позиций";
            this.dishAmount.FieldName = "Item2";
            this.dishAmount.Name = "dishAmount";
            this.dishAmount.Visible = true;
            this.dishAmount.VisibleIndex = 3;
            // 
            // additionalOffer
            // 
            this.additionalOffer.Controls.Add(this.listBoxSelected);
            this.additionalOffer.Controls.Add(this.listBoxDish);
            this.additionalOffer.Controls.Add(this.listBoxCategory);
            this.additionalOffer.Location = new System.Drawing.Point(4, 25);
            this.additionalOffer.Name = "additionalOffer";
            this.additionalOffer.Padding = new System.Windows.Forms.Padding(3);
            this.additionalOffer.Size = new System.Drawing.Size(777, 245);
            this.additionalOffer.TabIndex = 5;
            this.additionalOffer.Text = "Доп. предложение";
            this.additionalOffer.UseVisualStyleBackColor = true;
            // 
            // listBoxSelected
            // 
            this.listBoxSelected.FormattingEnabled = true;
            this.listBoxSelected.ItemHeight = 16;
            this.listBoxSelected.Location = new System.Drawing.Point(500, 0);
            this.listBoxSelected.Name = "listBoxSelected";
            this.listBoxSelected.Size = new System.Drawing.Size(277, 244);
            this.listBoxSelected.TabIndex = 2;
            this.listBoxSelected.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxSelected_MouseDoubleClick);
            // 
            // listBoxDish
            // 
            this.listBoxDish.FormattingEnabled = true;
            this.listBoxDish.ItemHeight = 16;
            this.listBoxDish.Location = new System.Drawing.Point(250, 0);
            this.listBoxDish.Name = "listBoxDish";
            this.listBoxDish.Size = new System.Drawing.Size(250, 244);
            this.listBoxDish.TabIndex = 1;
            this.listBoxDish.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxDish_MouseDoubleClick);
            // 
            // listBoxCategory
            // 
            this.listBoxCategory.FormattingEnabled = true;
            this.listBoxCategory.ItemHeight = 16;
            this.listBoxCategory.Location = new System.Drawing.Point(0, 0);
            this.listBoxCategory.Name = "listBoxCategory";
            this.listBoxCategory.Size = new System.Drawing.Size(250, 244);
            this.listBoxCategory.TabIndex = 0;
            this.listBoxCategory.SelectedIndexChanged += new System.EventHandler(this.listBoxCategory_SelectedIndexChanged);
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(657, 10);
            this.txtId.Margin = new System.Windows.Forms.Padding(4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(59, 22);
            this.txtId.TabIndex = 44;
            this.txtId.Tag = "id";
            this.txtId.Visible = false;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(229, 10);
            this.txtName.Margin = new System.Windows.Forms.Padding(4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(540, 22);
            this.txtName.TabIndex = 45;
            this.txtName.Tag = "name";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblName.Location = new System.Drawing.Point(91, 14);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(123, 17);
            this.lblName.TabIndex = 43;
            this.lblName.Text = "Наименование:";
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 654);
            this.pnlBottom.Margin = new System.Windows.Forms.Padding(4);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(785, 29);
            this.pnlBottom.TabIndex = 40;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(551, 0);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(116, 27);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Записать";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(667, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 27, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(116, 27);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(28, 28);
            this.btnAdd.Text = "Создать коэфициент";
            // 
            // separator
            // 
            this.separator.Name = "separator";
            this.separator.Size = new System.Drawing.Size(6, 31);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(28, 28);
            this.btnDelete.Text = "Пометить на удаление";
            // 
            // columnId
            // 
            this.columnId.Caption = "ИД";
            this.columnId.FieldName = "Product.ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 0;
            this.columnId.Width = 58;
            // 
            // columnName
            // 
            this.columnName.Caption = "Наименование";
            this.columnName.FieldName = "Product.Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 1;
            this.columnName.Width = 150;
            // 
            // columnAmount
            // 
            this.columnAmount.Caption = "Количество";
            this.columnAmount.FieldName = "Amount";
            this.columnAmount.Name = "columnAmount";
            this.columnAmount.Visible = true;
            this.columnAmount.VisibleIndex = 2;
            // 
            // columnMeasure
            // 
            this.columnMeasure.Caption = "Ед. Изм.";
            this.columnMeasure.FieldName = "Measure.ShortName";
            this.columnMeasure.Name = "columnMeasure";
            this.columnMeasure.OptionsColumn.AllowEdit = false;
            this.columnMeasure.OptionsColumn.ReadOnly = true;
            this.columnMeasure.Visible = true;
            this.columnMeasure.VisibleIndex = 3;
            // 
            // lblClientName
            // 
            this.lblClientName.AutoSize = true;
            this.lblClientName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblClientName.Location = new System.Drawing.Point(36, 46);
            this.lblClientName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblClientName.Name = "lblClientName";
            this.lblClientName.Size = new System.Drawing.Size(177, 17);
            this.lblClientName.TabIndex = 43;
            this.lblClientName.Text = "Наименование(внутр):";
            // 
            // txtInternalName
            // 
            this.txtInternalName.Location = new System.Drawing.Point(229, 42);
            this.txtInternalName.Margin = new System.Windows.Forms.Padding(4);
            this.txtInternalName.Name = "txtInternalName";
            this.txtInternalName.Size = new System.Drawing.Size(540, 22);
            this.txtInternalName.TabIndex = 45;
            this.txtInternalName.Tag = "internalname";
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCategory.Location = new System.Drawing.Point(125, 143);
            this.lblCategory.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(91, 17);
            this.lblCategory.TabIndex = 43;
            this.lblCategory.Text = "Категория:";
            // 
            // cmbDishCat
            // 
            this.cmbDishCat.FormattingEnabled = true;
            this.cmbDishCat.Location = new System.Drawing.Point(231, 139);
            this.cmbDishCat.Margin = new System.Windows.Forms.Padding(4);
            this.cmbDishCat.Name = "cmbDishCat";
            this.cmbDishCat.Size = new System.Drawing.Size(235, 24);
            this.cmbDishCat.TabIndex = 53;
            this.cmbDishCat.Tag = "CategoryID";
            // 
            // lblCookingTime
            // 
            this.lblCookingTime.AutoSize = true;
            this.lblCookingTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCookingTime.Location = new System.Drawing.Point(33, 110);
            this.lblCookingTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCookingTime.Name = "lblCookingTime";
            this.lblCookingTime.Size = new System.Drawing.Size(177, 17);
            this.lblCookingTime.TabIndex = 43;
            this.lblCookingTime.Text = "Время приготовления:";
            // 
            // txtCook
            // 
            this.txtCook.Location = new System.Drawing.Point(231, 106);
            this.txtCook.Margin = new System.Windows.Forms.Padding(4);
            this.txtCook.Name = "txtCook";
            this.txtCook.Size = new System.Drawing.Size(235, 22);
            this.txtCook.TabIndex = 45;
            this.txtCook.Tag = "CookingTime";
            this.txtCook.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // lblCoef
            // 
            this.lblCoef.AutoSize = true;
            this.lblCoef.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCoef.Location = new System.Drawing.Point(77, 218);
            this.lblCoef.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCoef.Name = "lblCoef";
            this.lblCoef.Size = new System.Drawing.Size(138, 17);
            this.lblCoef.TabIndex = 43;
            this.lblCoef.Text = "Категория коэф.:";
            // 
            // cmbAFCollections
            // 
            this.cmbAFCollections.FormattingEnabled = true;
            this.cmbAFCollections.Location = new System.Drawing.Point(233, 214);
            this.cmbAFCollections.Margin = new System.Windows.Forms.Padding(4);
            this.cmbAFCollections.Name = "cmbAFCollections";
            this.cmbAFCollections.Size = new System.Drawing.Size(536, 24);
            this.cmbAFCollections.TabIndex = 54;
            this.cmbAFCollections.Tag = "FactorCollectionID";
            // 
            // lblSku
            // 
            this.lblSku.AutoSize = true;
            this.lblSku.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSku.Location = new System.Drawing.Point(144, 78);
            this.lblSku.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSku.Name = "lblSku";
            this.lblSku.Size = new System.Drawing.Size(74, 17);
            this.lblSku.TabIndex = 43;
            this.lblSku.Text = "Артикул:";
            // 
            // txtSku
            // 
            this.txtSku.Location = new System.Drawing.Point(229, 74);
            this.txtSku.Margin = new System.Windows.Forms.Padding(4);
            this.txtSku.Name = "txtSku";
            this.txtSku.Size = new System.Drawing.Size(236, 22);
            this.txtSku.TabIndex = 45;
            this.txtSku.Tag = "sku";
            this.txtSku.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // lblSet
            // 
            this.lblSet.AutoSize = true;
            this.lblSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSet.Location = new System.Drawing.Point(161, 279);
            this.lblSet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSet.Name = "lblSet";
            this.lblSet.Size = new System.Drawing.Size(54, 17);
            this.lblSet.TabIndex = 43;
            this.lblSet.Text = "Набор:";
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(583, 74);
            this.txtPrice.Margin = new System.Windows.Forms.Padding(4);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(187, 22);
            this.txtPrice.TabIndex = 45;
            this.txtPrice.Tag = "price";
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckAvailableChar);
            // 
            // lblCutlery
            // 
            this.lblCutlery.AutoSize = true;
            this.lblCutlery.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCutlery.Location = new System.Drawing.Point(501, 110);
            this.lblCutlery.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCutlery.Name = "lblCutlery";
            this.lblCutlery.Size = new System.Drawing.Size(69, 17);
            this.lblCutlery.TabIndex = 43;
            this.lblCutlery.Text = "Прибор:";
            // 
            // cmbCutlery
            // 
            this.cmbCutlery.FormattingEnabled = true;
            this.cmbCutlery.Location = new System.Drawing.Point(583, 106);
            this.cmbCutlery.Margin = new System.Windows.Forms.Padding(4);
            this.cmbCutlery.Name = "cmbCutlery";
            this.cmbCutlery.Size = new System.Drawing.Size(187, 24);
            this.cmbCutlery.TabIndex = 55;
            this.cmbCutlery.Tag = "Cutlery";
            // 
            // btnSelectCoefficient
            // 
            this.btnSelectCoefficient.Enabled = false;
            this.btnSelectCoefficient.Location = new System.Drawing.Point(743, 273);
            this.btnSelectCoefficient.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelectCoefficient.Name = "btnSelectCoefficient";
            this.btnSelectCoefficient.Size = new System.Drawing.Size(32, 27);
            this.btnSelectCoefficient.TabIndex = 60;
            this.btnSelectCoefficient.Text = "...";
            this.btnSelectCoefficient.UseVisualStyleBackColor = true;
            this.btnSelectCoefficient.Click += new System.EventHandler(this.btnSelectCoefficient_Click);
            // 
            // chkCalculateCoefficient
            // 
            this.chkCalculateCoefficient.AutoSize = true;
            this.chkCalculateCoefficient.Location = new System.Drawing.Point(233, 247);
            this.chkCalculateCoefficient.Margin = new System.Windows.Forms.Padding(4);
            this.chkCalculateCoefficient.Name = "chkCalculateCoefficient";
            this.chkCalculateCoefficient.Size = new System.Drawing.Size(195, 21);
            this.chkCalculateCoefficient.TabIndex = 59;
            this.chkCalculateCoefficient.Tag = "CalculateByCoefficient";
            this.chkCalculateCoefficient.Text = "Поставляется с набором";
            this.chkCalculateCoefficient.UseVisualStyleBackColor = true;
            this.chkCalculateCoefficient.CheckedChanged += new System.EventHandler(this.chkCalculateCoefficient_CheckedChanged);
            // 
            // cmbCutleryCoefficient
            // 
            this.cmbCutleryCoefficient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCutleryCoefficient.Enabled = false;
            this.cmbCutleryCoefficient.FormattingEnabled = true;
            this.cmbCutleryCoefficient.Location = new System.Drawing.Point(232, 276);
            this.cmbCutleryCoefficient.Margin = new System.Windows.Forms.Padding(4);
            this.cmbCutleryCoefficient.Name = "cmbCutleryCoefficient";
            this.cmbCutleryCoefficient.Size = new System.Drawing.Size(499, 24);
            this.cmbCutleryCoefficient.TabIndex = 58;
            this.cmbCutleryCoefficient.Tag = "CoefficientId";
            // 
            // chckDelivery
            // 
            this.chckDelivery.AutoSize = true;
            this.chckDelivery.Location = new System.Drawing.Point(447, 247);
            this.chckDelivery.Margin = new System.Windows.Forms.Padding(4);
            this.chckDelivery.Name = "chckDelivery";
            this.chckDelivery.Size = new System.Drawing.Size(251, 21);
            this.chckDelivery.TabIndex = 56;
            this.chckDelivery.Tag = "DoCountWhenDeliveryCostCalculated";
            this.chckDelivery.Text = "Учитывать при расчете доставки";
            this.chckDelivery.UseVisualStyleBackColor = true;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPrice.Location = new System.Drawing.Point(520, 78);
            this.lblPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(52, 17);
            this.lblPrice.TabIndex = 43;
            this.lblPrice.Text = "Цена:";
            // 
            // cmbWarehouse
            // 
            this.cmbWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWarehouse.Enabled = false;
            this.cmbWarehouse.FormattingEnabled = true;
            this.cmbWarehouse.Location = new System.Drawing.Point(461, 309);
            this.cmbWarehouse.Margin = new System.Windows.Forms.Padding(4);
            this.cmbWarehouse.Name = "cmbWarehouse";
            this.cmbWarehouse.Size = new System.Drawing.Size(312, 24);
            this.cmbWarehouse.TabIndex = 62;
            this.cmbWarehouse.Tag = "WarehouseForDishID";
            // 
            // chkCustomWarehouse
            // 
            this.chkCustomWarehouse.AutoSize = true;
            this.chkCustomWarehouse.Location = new System.Drawing.Point(232, 311);
            this.chkCustomWarehouse.Margin = new System.Windows.Forms.Padding(4);
            this.chkCustomWarehouse.Name = "chkCustomWarehouse";
            this.chkCustomWarehouse.Size = new System.Drawing.Size(212, 21);
            this.chkCustomWarehouse.TabIndex = 63;
            this.chkCustomWarehouse.Tag = "UseCustomWarehouse";
            this.chkCustomWarehouse.Text = "Склад списания продуктов:";
            this.chkCustomWarehouse.UseVisualStyleBackColor = true;
            this.chkCustomWarehouse.CheckedChanged += new System.EventHandler(this.chkCustomWarehouse_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(475, 149);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 17);
            this.label1.TabIndex = 64;
            this.label1.Text = "Цена кафе:";
            // 
            // txtCafePrice
            // 
            this.txtCafePrice.Location = new System.Drawing.Point(583, 143);
            this.txtCafePrice.Margin = new System.Windows.Forms.Padding(4);
            this.txtCafePrice.Name = "txtCafePrice";
            this.txtCafePrice.Size = new System.Drawing.Size(187, 22);
            this.txtCafePrice.TabIndex = 65;
            this.txtCafePrice.Tag = "CafePrice";
            // 
            // chkAlcohol
            // 
            this.chkAlcohol.AutoSize = true;
            this.chkAlcohol.Location = new System.Drawing.Point(715, 247);
            this.chkAlcohol.Margin = new System.Windows.Forms.Padding(4);
            this.chkAlcohol.Name = "chkAlcohol";
            this.chkAlcohol.Size = new System.Drawing.Size(55, 21);
            this.chkAlcohol.TabIndex = 66;
            this.chkAlcohol.Tag = "IsAlcohole";
            this.chkAlcohol.Text = "Бар";
            this.chkAlcohol.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(31, 338);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 17);
            this.label2.TabIndex = 67;
            this.label2.Text = "Применимость скидки:";
            // 
            // checkBoxDiscountApplicability
            // 
            this.checkBoxDiscountApplicability.AutoSize = true;
            this.checkBoxDiscountApplicability.Location = new System.Drawing.Point(232, 338);
            this.checkBoxDiscountApplicability.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxDiscountApplicability.Name = "checkBoxDiscountApplicability";
            this.checkBoxDiscountApplicability.Size = new System.Drawing.Size(168, 21);
            this.checkBoxDiscountApplicability.TabIndex = 68;
            this.checkBoxDiscountApplicability.Tag = "DiscountAppliсability";
            this.checkBoxDiscountApplicability.Text = "Скидка применяется";
            this.checkBoxDiscountApplicability.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(173, 180);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 17);
            this.label3.TabIndex = 69;
            this.label3.Text = "Цех:";
            // 
            // comboBoxSection
            // 
            this.comboBoxSection.FormattingEnabled = true;
            this.comboBoxSection.Location = new System.Drawing.Point(231, 176);
            this.comboBoxSection.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxSection.Name = "comboBoxSection";
            this.comboBoxSection.Size = new System.Drawing.Size(539, 24);
            this.comboBoxSection.TabIndex = 70;
            this.comboBoxSection.Tag = "SectionID";
            // 
            // DishForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 683);
            this.Controls.Add(this.comboBoxSection);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.checkBoxDiscountApplicability);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chkAlcohol);
            this.Controls.Add(this.txtCafePrice);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkCustomWarehouse);
            this.Controls.Add(this.cmbWarehouse);
            this.Controls.Add(this.btnSelectCoefficient);
            this.Controls.Add(this.chkCalculateCoefficient);
            this.Controls.Add(this.cmbCutleryCoefficient);
            this.Controls.Add(this.chckDelivery);
            this.Controls.Add(this.cmbCutlery);
            this.Controls.Add(this.cmbAFCollections);
            this.Controls.Add(this.cmbDishCat);
            this.Controls.Add(this.Container);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.txtPrice);
            this.Controls.Add(this.txtSku);
            this.Controls.Add(this.txtCook);
            this.Controls.Add(this.txtInternalName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblClientName);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.lblSet);
            this.Controls.Add(this.lblCutlery);
            this.Controls.Add(this.lblSku);
            this.Controls.Add(this.lblCoef);
            this.Controls.Add(this.lblCookingTime);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.pnlBottom);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DishForm";
            this.Text = "Редактирование блюда";
            this.Container.ResumeLayout(false);
            this.tbMainPage.ResumeLayout(false);
            this.tbMainPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.mainToolstrip.ResumeLayout(false);
            this.mainToolstrip.PerformLayout();
            this.tabCheck.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.tabOthers.ResumeLayout(false);
            this.transferPanel.ResumeLayout(false);
            this.transferPanel.PerformLayout();
            this.tabTopDishes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTopDishes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewTopDishes)).EndInit();
            this.additionalOffer.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl Container;
        public System.Windows.Forms.TextBox txtId;
        public System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TabPage tbMainPage;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripSeparator separator;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnMeasure;
        private System.Windows.Forms.Label lblClientName;
        public System.Windows.Forms.TextBox txtInternalName;
        private System.Windows.Forms.Label lblCategory;
        public System.Windows.Forms.ComboBox cmbDishCat;
        private System.Windows.Forms.Label lblCookingTime;
        public System.Windows.Forms.TextBox txtCook;
        private System.Windows.Forms.Label lblCoef;
        public System.Windows.Forms.ComboBox cmbAFCollections;
        private System.Windows.Forms.Label lblSku;
        public System.Windows.Forms.TextBox txtSku;
        private System.Windows.Forms.Label lblSet;
        public System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblCutlery;
        public System.Windows.Forms.ComboBox cmbCutlery;
        private System.Windows.Forms.Button btnSelectCoefficient;
        private System.Windows.Forms.CheckBox chkCalculateCoefficient;
        public System.Windows.Forms.ComboBox cmbCutleryCoefficient;
        private System.Windows.Forms.CheckBox chckDelivery;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TabPage tabCheck;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.TextBox txtCarbohydrates;
        private System.Windows.Forms.TextBox txtProtein;
        private System.Windows.Forms.TextBox txtFat;
        private System.Windows.Forms.Label lblProtein;
        private System.Windows.Forms.Label lblFat;
        private System.Windows.Forms.Label lblCarbohydrates;
        private System.Windows.Forms.Label lblEndTemperature2;
        private System.Windows.Forms.Label lblEndTemperature;
        private System.Windows.Forms.Label lblStartTemperature;
        private System.Windows.Forms.Label lblFitHour;
        private System.Windows.Forms.Label lblAddressProduction;
        private System.Windows.Forms.Label lblManufactured;
        private System.Windows.Forms.Label lblNetto;
        private System.Windows.Forms.TextBox txtAddressProduction;
        private System.Windows.Forms.TextBox txtManufactured;
        private System.Windows.Forms.TextBox txtEndTemperature;
        private System.Windows.Forms.TextBox txtStartTemperature;
        private System.Windows.Forms.TextBox txtFitHour;
        private System.Windows.Forms.TextBox txtNetto;
        private System.Windows.Forms.TextBox txtCoeffEnergy;
        private System.Windows.Forms.Label lblCoeffEnergy;
        private System.Windows.Forms.TextBox txtStructure;
        private System.Windows.Forms.Label lblStructure;
        private System.Windows.Forms.CheckBox chkMarkEAC;
        private System.Windows.Forms.CheckBox chkMarkGOST;
        public System.Windows.Forms.ComboBox cmbWarehouse;
        private System.Windows.Forms.CheckBox chkCustomWarehouse;
        private System.Windows.Forms.ToolStrip mainToolstrip;
        private System.Windows.Forms.ToolStripButton btnAddIngridient;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnDeleteIngridient;
        private System.Windows.Forms.TabPage tabOthers;
        private System.Windows.Forms.Panel transferPanel;
        private System.Windows.Forms.TextBox txtExternalCode;
        private System.Windows.Forms.Label lblExternalCode;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblBarcode;
        private DevExpress.XtraGrid.Columns.GridColumn IdColumn;
        private DevExpress.XtraGrid.Columns.GridColumn NameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn AmountColumn;
        private DevExpress.XtraGrid.Columns.GridColumn MeasureColumn;
        private System.Windows.Forms.TextBox txtCTO;
        private System.Windows.Forms.Label lblCTO;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtCafePrice;
        private System.Windows.Forms.CheckBox chkAlcohol;
        private System.Windows.Forms.TabPage tabTopDishes;
        private DevExpress.XtraGrid.GridControl gridTopDishes;
        private DevExpress.XtraGrid.Views.Grid.GridView viewTopDishes;
        private DevExpress.XtraGrid.Columns.GridColumn dishName;
        private DevExpress.XtraGrid.Columns.GridColumn dishAmount;
        private DevExpress.XtraGrid.Columns.GridColumn sharedOrders;
        private DevExpress.XtraGrid.Columns.GridColumn dishCategory;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxDiscountApplicability;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox comboBoxSection;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager;
        private System.Windows.Forms.TabPage additionalOffer;
        private System.Windows.Forms.ListBox listBoxCategory;
        private System.Windows.Forms.ListBox listBoxDish;
        private System.Windows.Forms.ListBox listBoxSelected;
    }
}