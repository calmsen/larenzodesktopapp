﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Sections;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.Dishes
{
    public partial class DishCategoryForm : Form
    {
        #region Category

        private readonly bool _isEditWindow;

        private readonly Category _category;
        
        private readonly int _brandId;

        #endregion Category

        #region Ctor

        public DishCategoryForm()
        {
            InitializeComponent();
        }

        public DishCategoryForm(bool isEdit, Category category, int brandId)
            : this()
        {
            _isEditWindow = isEdit;

            _brandId = brandId;

            _category = category ?? new Category();

            SetControlsParams();

            WindowsFormProvider.FillControlsFromModel(this, category);
        }

        #endregion Ctor

        #region Private Methods

        private void SetControlsParams()
        {
            var sections = Content.SectionRepository.GetList();
            cmbBoxCatSection.DataSource = sections;
            cmbBoxCatSection.DisplayMember = "Name";
            cmbBoxCatSection.ValueMember = "ID";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!CheckRequireFields())
                return;

            FillEntityProperties();
            _category.Brand_ID = _brandId;
            if (_isEditWindow)
            {
                Content.CategoryRepository.UpdateItem(_category);
            }
            else
            {
                Content.CategoryRepository.InsertItem(_category);
            }

            DialogResult = DialogResult.OK;
        }

        private void FillEntityProperties()
        {
            _category.Name = txtCatName.Text;

            var section = Convert.ToInt32(cmbBoxCatSection.SelectedValue);
            _category.SectionID = section;
            
            var weight = Convert.ToInt32(txtBoxWeigth.Text);
            _category.Weight = weight;
        }

        private bool CheckRequireFields()
        {
            if (string.IsNullOrWhiteSpace(txtCatName.Text))
            {
                SetError(txtCatName, Resources.DishCategoryNameIsNotFill);
                return false;
            }

            if (!int.TryParse(txtBoxWeigth.Text, out _))
            {
                SetError(txtBoxWeigth, Resources.WeightMastBeInt);
                return false;
            }

            return true;
        }

        private void SetError(Control control, string message)
        {
            var ep = new ErrorProvider();
            ep.SetError(control, message);
        }

        #endregion Private Methods
    }
}