﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Measures;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.Classifier.MeasuresForms
{
    public partial class MeasureListForm : Form
    {
        public MeasureListForm()
        {
            InitializeComponent();
            LoadMeasureList();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var measureForm = new MeasureForm();
            if (measureForm.ShowDialog() == DialogResult.OK)
            {
                LoadMeasureList();
            }
        }

        private void GetMeasureForm(bool isEdit, int measureId)
        {
            Measure measure = Content.MeasureManager.GetMeasure(measureId);

            var measureForm = new MeasureForm(isEdit, measure);

            if (measureForm.ShowDialog() == DialogResult.OK)
            {
                LoadMeasureList();
            }
        }

        private void LoadMeasureList()
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            mainGridControl.DataSource = Content.MeasureManager.GetMeasures();
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadMeasureList();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selecterRow = (Measure) mainGridView.GetFocusedRow();
            if (selecterRow != null)
            {
                GetMeasureForm(false, selecterRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.MeasureIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (Measure) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetMeasureForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.MeasureIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteMeasureQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var selectedRow = (Measure) mainGridView.GetFocusedRow();
                if (selectedRow != null)
                {
                    Content.MeasureManager.DeleteMeasure(new Measure {ID = selectedRow.ID});
                    LoadMeasureList();
                }
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var measureId = (int) view.GetRowCellValue(info.RowHandle, columnId);
                GetMeasureForm(true, measureId);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}
