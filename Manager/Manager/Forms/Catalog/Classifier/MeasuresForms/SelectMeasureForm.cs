﻿using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Measures;

namespace LaRenzo.Forms.Catalog.Classifier.MeasuresForms
{
    public partial class SelectMeasureForm : Form
    {
        public Measure Measure { set; get; }

        public SelectMeasureForm()
        {
            InitializeComponent();
            LoadMeasureList();
        }

        private void LoadMeasureList()
        {
            mainGridControl.DataSource = Content.MeasureManager.GetMeasures();
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSelect_Click(object sender, System.EventArgs e)
        {
            Measure = (Measure)mainGridView.GetFocusedRow();
            if (Measure == null)
                return;

            DialogResult = DialogResult.OK;
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }
    }
}
