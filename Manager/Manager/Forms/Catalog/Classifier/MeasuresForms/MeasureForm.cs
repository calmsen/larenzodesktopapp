﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Measures;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Measures;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Catalog.Classifier.MeasuresForms
{
    public partial class MeasureForm : Form
    {
        private readonly bool _isEditWindow;

        private Measure _parentMeasure;

        public Measure ParentMeasure
        {
            set
            {
                _parentMeasure = value;
                txtParentMeasure.Text = value.Name;
            }
            get { return _parentMeasure; }
        }

        public MeasureForm()
        {
            InitializeComponent();
        }

        public MeasureForm(bool isEditWindow, Measure measure) : this()
        {
            _isEditWindow = isEditWindow;

            WindowsFormProvider.FillControlsFromModel(this, measure);

            if (measure.ParentMeasureID != null)
            {
                var parentMeasure = Content.MeasureManager.GetMeasure(measure.ParentMeasureID.Value);
                
                if (parentMeasure != null)
                {
                    ParentMeasure = parentMeasure;
                }
            }
        }

        private void txtMeasureValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnSelectMeasure_Click(object sender, EventArgs e)
        {
            var selectMeasureForm = new SelectMeasureForm();

            if (selectMeasureForm.ShowDialog() == DialogResult.OK)
            {
                ParentMeasure = selectMeasureForm.Measure;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!CheckRequireFields())
                return;

            var measure = GetMeasureByControl();

            if (_isEditWindow)
            {
                Content.MeasureManager.UpdateMeasure(measure);
            }
            else
            {
                Content.MeasureManager.AddMeasure(measure);
            }

            DialogResult = DialogResult.OK;
        }

        private Measure GetMeasureByControl()
        {
            var measure = new Measure
                {
                    ID = string.IsNullOrEmpty(txtId.Text) ? 0 : Convert.ToInt32(txtId.Text.Trim()),
                    ForeignName = txtForeignName.Text.Trim(),
                    Name = txtFullName.Text.Trim(),
                    ShortName = txtShortName.Text.Trim(),
                    IsBaseMeasure = chbIsBase.Checked,
                    ParentMeasureValue = Convert.ToInt32(txtMeasureValue.Text.Trim()),
                    ParentMeasureID = ParentMeasure != null ? ParentMeasure.ID : (int?) null
                };

            return measure;
        }

        private bool CheckRequireFields()
        {
            if (string.IsNullOrWhiteSpace(txtFullName.Text))
            {
                SetError(txtFullName, Resources.NameIsEmpty);

                return false;
            }

            if (string.IsNullOrWhiteSpace(txtShortName.Text))
            {
                SetError(txtShortName, Resources.NameIsEmpty);

                return false;
            }

            return true;
        }

        private void SetError(Control control, string message)
        {
            var ep = new ErrorProvider();
            ep.SetError(control, message);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
