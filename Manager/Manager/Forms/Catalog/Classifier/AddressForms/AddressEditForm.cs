﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Addresses;

namespace LaRenzo.Forms.Catalog.Classifier.AddressForms
{
    public partial class AddressEditForm : Form
    {
        private Address _address;
		/// <summary> Значение контрола </summary>
		private Address ItemValue
		{
			get
			{
			    _address.Name = nameTextBox.Text;
				return _address;
			}
			set
			{
			    _address = value;
				nameTextBox.Text = value.Name ?? "";
			}
		}

        //___________________________________________________________________________________________________________________________________________________________________________________
		public AddressEditForm(Address editItem = null)
		{
			InitializeComponent();
            okButton.Click += async(s,e) => await OkButtonClick();
		    if (editItem != null)
		    {
		        ItemValue = editItem;
		    }
		    else
		    {
		        ItemValue = new Address();
		    }
		}
		
		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Сохранить" </summary>
		////=======================================
        private async Task OkButtonClick()
        {
			if (ItemValue != null)
			{
			    if (ItemValue.ID == 0)
			    {
			        await Content.AddressManager.AddAddress(ItemValue);
			    }
			    else
			    {
			        await Content.AddressManager.UpdateAddress(ItemValue);
			    }

			    DialogResult = DialogResult.OK;
			}
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Удалить" </summary>
		////=======================================
		private void CancelButtonClick(object sender, EventArgs e)
        {
			DialogResult = DialogResult.Cancel;
        }
    }
}
