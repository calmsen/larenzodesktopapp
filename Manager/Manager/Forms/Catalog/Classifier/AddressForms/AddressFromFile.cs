﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories.Catalogs.Addresses;
using LaRenzo.Properties;
using LaRenzo.DataRepository.Repositories;

namespace LaRenzo.Forms.Catalog.Classifier.AddressForms
{
    public partial class AddressFromFile : Form
    {
        private readonly BindingList<AddressRecord> _addressRecords = new BindingList<AddressRecord>();

        public AddressFromFile()
        {
            InitializeComponent();
            loadedGridView.AutoGenerateColumns = false;
            SaveToDataBase.DoWork += async(s,e) => await SaveToDataBase_DoWork();
        }

        private void AddressFromFile_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (LoadFromFile.IsBusy)
                e.Cancel = true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtPath.Text = string.Empty;
        }

        private void select_Click(object sender, EventArgs e)
        {
            if (AddressDlg.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = AddressDlg.FileName;
            }
        }

        private void LoadAddressWizard_NextClick(object sender, DevExpress.XtraWizard.WizardCommandButtonClickEventArgs e)
        {
            if (e.Page.Name == "Start")
            {
                LoadFromFile.RunWorkerAsync();
            }
            else
            {
               SaveToDataBase.RunWorkerAsync();
            }

            
        }

        private void LoadFromFile_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!File.Exists(txtPath.Text))
            {
                LoadAddressWizard.SetPreviousPage();
                MessageBox.Show(Resources.FileWithAddressNotFound);
            }

            using (var sr = new StreamReader(txtPath.Text))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Length == 0)
                        continue;

                    _addressRecords.Add(new AddressRecord {IsChecked = false, Address = line});
                }

                mainDataGrid.BeginInvoke(new Action(() => { mainDataGrid.DataSource = _addressRecords; }));
            }
        }

        private void LoadFromFile_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LoadAddressWizard.SetNextPage();
        }

        private void selectAll_Click(object sender, EventArgs e)
        {
            foreach (var record in _addressRecords)
            {
                record.IsChecked = true;
            }

            mainDataGrid.Refresh();
        }

        private void unSelectAll_Click(object sender, EventArgs e)
        {
            foreach (var record in _addressRecords)
            {
                record.IsChecked = false;
            }

            mainDataGrid.Refresh();
        }

        private async Task SaveToDataBase_DoWork()
        {
            foreach (var addressRecord in _addressRecords)
            {
                if (addressRecord.IsChecked)
                {
                    await Content.AddressManager.AddAddress(new Address { Name = addressRecord.Address });
                }
            }
        }

        private void SaveToDataBase_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            mainDataGrid.BeginInvoke(
                new Action(
                    () =>
                        {
                            loadedGridView.DataSource =
                                loadedGridView.DataSource = _addressRecords.Where(x => x.IsChecked).ToList();
                        }));

            LoadAddressWizard.SetNextPage();
        }

        private void mainDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                _addressRecords[e.RowIndex].IsChecked = true;
            }
        }
    }
}
