﻿using DevExpress.XtraWaitForm;

namespace LaRenzo.Forms
{
    public partial class WaitFormGeneral : WaitForm
    {
        public WaitFormGeneral()
        {
            InitializeComponent();
            progressPanel.AutoHeight = true;
        }

        #region Overrides

        public override void SetCaption(string caption)
        {
            base.SetCaption(caption);
            progressPanel.Caption = caption;
        }
        public override void SetDescription(string description)
        {
            base.SetDescription(description);
            progressPanel.Description = description;
        }

        #endregion

        public enum WaitFormCommand
        {
        }
    }
}