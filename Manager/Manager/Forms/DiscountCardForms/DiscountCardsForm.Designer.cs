﻿namespace LaRenzo.Forms.DiscountCardForms
{
    partial class DiscountCardsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.btnAdd = new System.Windows.Forms.ToolStripButton();
			this.btnRefresh = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.mainGridControl = new DevExpress.XtraGrid.GridControl();
			this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.columnID = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnCardNumber = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnFatherName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnEmail = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnOrdersTotal = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnOrdersCost = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnOrdersCostWithoutDisc = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnOrdersCostDiff = new DevExpress.XtraGrid.Columns.GridColumn();
			this.сolumnDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.toolStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// toolStrip1
			// 
			this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.btnRefresh,
            this.toolStripSeparator1,
            this.toolStripButton1});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(1091, 31);
			this.toolStrip1.TabIndex = 0;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// btnAdd
			// 
			this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnAdd.Image = global::LaRenzo.Properties.Resources.plus;
			this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(28, 28);
			this.btnAdd.Text = "Добавить карту";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// btnRefresh
			// 
			this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
			this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new System.Drawing.Size(28, 28);
			this.btnRefresh.Text = "Обновить";
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
			// 
			// toolStripButton1
			// 
			this.toolStripButton1.Image = global::LaRenzo.Properties.Resources._1427714162_flow_merge_128;
			this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton1.Name = "toolStripButton1";
			this.toolStripButton1.Size = new System.Drawing.Size(167, 28);
			this.toolStripButton1.Text = "Объеденить дубли";
			this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
			// 
			// mainGridControl
			// 
			this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.mainGridControl.Location = new System.Drawing.Point(0, 31);
			this.mainGridControl.MainView = this.mainGridView;
			this.mainGridControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.mainGridControl.Name = "mainGridControl";
			this.mainGridControl.Size = new System.Drawing.Size(1091, 591);
			this.mainGridControl.TabIndex = 1;
			this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
			// 
			// mainGridView
			// 
			this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnID,
            this.columnCardNumber,
            this.columnName,
            this.columnFatherName,
            this.columnPhone,
            this.columnEmail,
            this.columnOrdersTotal,
            this.columnOrdersCost,
            this.columnOrdersCostWithoutDisc,
            this.columnOrdersCostDiff,
            this.сolumnDiscount});
			this.mainGridView.GridControl = this.mainGridControl;
			this.mainGridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrdersCostDiffCol", null, "")});
			this.mainGridView.Name = "mainGridView";
			this.mainGridView.OptionsView.ShowAutoFilterRow = true;
			this.mainGridView.OptionsView.ShowFooter = true;
			this.mainGridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
			this.mainGridView.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
			// 
			// columnID
			// 
			this.columnID.Caption = "Ид";
			this.columnID.FieldName = "ID";
			this.columnID.Name = "columnID";
			this.columnID.OptionsColumn.AllowEdit = false;
			this.columnID.OptionsColumn.ReadOnly = true;
			// 
			// columnCardNumber
			// 
			this.columnCardNumber.Caption = "Номер карты";
			this.columnCardNumber.FieldName = "CardNumber";
			this.columnCardNumber.Name = "columnCardNumber";
			this.columnCardNumber.OptionsColumn.AllowEdit = false;
			this.columnCardNumber.OptionsColumn.ReadOnly = true;
			this.columnCardNumber.Visible = true;
			this.columnCardNumber.VisibleIndex = 0;
			// 
			// columnName
			// 
			this.columnName.Caption = "Имя";
			this.columnName.FieldName = "Name";
			this.columnName.Name = "columnName";
			this.columnName.OptionsColumn.AllowEdit = false;
			this.columnName.OptionsColumn.ReadOnly = true;
			this.columnName.Visible = true;
			this.columnName.VisibleIndex = 1;
			// 
			// columnFatherName
			// 
			this.columnFatherName.Caption = "Отчество";
			this.columnFatherName.FieldName = "FatherName";
			this.columnFatherName.Name = "columnFatherName";
			this.columnFatherName.OptionsColumn.AllowEdit = false;
			this.columnFatherName.OptionsColumn.ReadOnly = true;
			this.columnFatherName.Visible = true;
			this.columnFatherName.VisibleIndex = 2;
			// 
			// columnPhone
			// 
			this.columnPhone.Caption = "Телефон";
			this.columnPhone.FieldName = "Phone";
			this.columnPhone.Name = "columnPhone";
			this.columnPhone.OptionsColumn.AllowEdit = false;
			this.columnPhone.OptionsColumn.ReadOnly = true;
			this.columnPhone.Visible = true;
			this.columnPhone.VisibleIndex = 3;
			// 
			// columnEmail
			// 
			this.columnEmail.Caption = "Email";
			this.columnEmail.FieldName = "Email";
			this.columnEmail.Name = "columnEmail";
			this.columnEmail.OptionsColumn.AllowEdit = false;
			this.columnEmail.OptionsColumn.ReadOnly = true;
			this.columnEmail.Visible = true;
			this.columnEmail.VisibleIndex = 4;
			// 
			// columnOrdersTotal
			// 
			this.columnOrdersTotal.Caption = "Всего заказов";
			this.columnOrdersTotal.FieldName = "OrdersTotal";
			this.columnOrdersTotal.Name = "columnOrdersTotal";
			this.columnOrdersTotal.OptionsColumn.AllowEdit = false;
			this.columnOrdersTotal.OptionsColumn.ReadOnly = true;
			this.columnOrdersTotal.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
			this.columnOrdersTotal.Visible = true;
			this.columnOrdersTotal.VisibleIndex = 5;
			// 
			// columnOrdersCost
			// 
			this.columnOrdersCost.Caption = "Сумма заказов";
			this.columnOrdersCost.FieldName = "OrdersCost";
			this.columnOrdersCost.Name = "columnOrdersCost";
			this.columnOrdersCost.OptionsColumn.AllowEdit = false;
			this.columnOrdersCost.OptionsColumn.ReadOnly = true;
			this.columnOrdersCost.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
			this.columnOrdersCost.Visible = true;
			this.columnOrdersCost.VisibleIndex = 6;
			// 
			// columnOrdersCostWithoutDisc
			// 
			this.columnOrdersCostWithoutDisc.Caption = "Сумма заказов без скидки";
			this.columnOrdersCostWithoutDisc.FieldName = "OrdersCostWithoutDisc";
			this.columnOrdersCostWithoutDisc.Name = "columnOrdersCostWithoutDisc";
			this.columnOrdersCostWithoutDisc.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
			this.columnOrdersCostWithoutDisc.Visible = true;
			this.columnOrdersCostWithoutDisc.VisibleIndex = 7;
			// 
			// columnOrdersCostDiff
			// 
			this.columnOrdersCostDiff.Caption = "Потеряно на скидках";
			this.columnOrdersCostDiff.FieldName = "OrdersCostDiff";
			this.columnOrdersCostDiff.Name = "columnOrdersCostDiff";
			this.columnOrdersCostDiff.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
			this.columnOrdersCostDiff.Visible = true;
			this.columnOrdersCostDiff.VisibleIndex = 8;
			// 
			// сolumnDiscount
			// 
			this.сolumnDiscount.Caption = "% скидки";
			this.сolumnDiscount.FieldName = "Discount";
			this.сolumnDiscount.Name = "сolumnDiscount";
			this.сolumnDiscount.OptionsColumn.AllowEdit = false;
			this.сolumnDiscount.OptionsColumn.ReadOnly = true;
			this.сolumnDiscount.Visible = true;
			this.сolumnDiscount.VisibleIndex = 9;
			// 
			// DiscountCardsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1091, 622);
			this.Controls.Add(this.mainGridControl);
			this.Controls.Add(this.toolStrip1);
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Name = "DiscountCardsForm";
			this.Text = "Скидочные карты";
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn columnID;
        private DevExpress.XtraGrid.Columns.GridColumn columnCardNumber;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
        private DevExpress.XtraGrid.Columns.GridColumn columnOrdersTotal;
        private DevExpress.XtraGrid.Columns.GridColumn columnOrdersCost;
        private DevExpress.XtraGrid.Columns.GridColumn columnOrdersCostWithoutDisc;
        private DevExpress.XtraGrid.Columns.GridColumn columnOrdersCostDiff;
        private DevExpress.XtraGrid.Columns.GridColumn columnFatherName;
        private DevExpress.XtraGrid.Columns.GridColumn columnEmail;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
		private DevExpress.XtraGrid.Columns.GridColumn сolumnDiscount;
    }
}