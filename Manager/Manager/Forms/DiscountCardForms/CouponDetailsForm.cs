﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Categories;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories;

namespace LaRenzo.Forms.DiscountCardForms
{
    public partial class CouponDetailsForm : Form
    {
        private CouponRule _couponRule;
        private bool _isFormEnabled;
        private List<CouponDiscount> _couponDiscountList;

        public CouponDetailsForm(CouponRule couponRule = null)
        {
            _couponRule = couponRule;

            InitializeComponent();
        }

        private async void CouponDetailsForm_Load(object sender, EventArgs e)
        {
            listBoxCategory.DisplayMember = "Name";
            listBoxDish.DisplayMember = "Name";
            listBoxSelected.DisplayMember = "Name";
            listBoxSelectedFor.DisplayMember = "Name";


            FillBrandComboBox();
            FillCategoryList();
            FillDishList();
            FillDiscountOverlap();
            FillDiscountTypeComboBox();
            await FillCouponRule();
            IsFormEnabled(true);
        }

        private void FillDiscountTypeComboBox()
        {
            comboBoxDiscountType.Items.Clear();
            comboBoxDiscountType.Items.Add("Фиксированная цена блюда");
            comboBoxDiscountType.Items.Add("Скидка в процентах");
            comboBoxDiscountType.Items.Add("Фиксированная скидка");
            comboBoxDiscountType.Items.Add("Следующее блюдо в подарок");
            if (_couponRule?.Price != null)
                comboBoxDiscountType.SelectedIndex = 0;
            else if (_couponRule?.Percent != null)
                comboBoxDiscountType.SelectedIndex = 1;
            else if (_couponRule?.FixDiscount != null)
                comboBoxDiscountType.SelectedIndex = 2;
            else if (_couponRule?.GiftDish != null)
                comboBoxDiscountType.SelectedIndex = 3;
            else
                comboBoxDiscountType.SelectedIndex = 0;
        }

        private void IsFormEnabled(bool enabled)
        {
            buttonGenerate.Enabled = enabled;
            buttonSave.Enabled = enabled;

            _isFormEnabled = enabled;
        }

        private async Task FillCouponRule()
        {
            if (_couponRule == null)
                return;

            textBoxName.Text = _couponRule.Name;
            textBoxDiscountValue.Text = (_couponRule.Price ?? _couponRule.FixDiscount ?? _couponRule.Percent ?? _couponRule.GiftDish)?.ToString();
            textBoxSumToApply.Text = _couponRule.SumToApply?.ToString();
            
            textBoxSumToApply.Enabled = _couponRule.SumToApply != null;

            checkBoxSumToApply.Checked = _couponRule.SumToApply != null;

            checkBoxAllRequired.Checked = _couponRule.AllDishRequired;
            checkBoxApplyToAll.Checked = _couponRule.ApplyToAllDishes;
            dateTimePickerFrom.Value = _couponRule.From;
            dateTimePickerTo.Value = _couponRule.To;

            var couponRuleElements = await Content.CouponRuleElementRepository.GetByCouponRuleId(_couponRule.ID);
            foreach (var couponRuleElement in couponRuleElements.Where(x => x.ElementApplyDirection == ElementApplyDirection.ElementNeededToApplyRule))
            {
                if (couponRuleElement.ElementContext == CouponRuleElementEnum.Category)
                {
                    var item = Content.CategoryRepository.GetItem(couponRuleElement.ElementId);
                    listBoxSelectedFor.Items.Add(item);
                }
                else if (couponRuleElement.ElementContext == CouponRuleElementEnum.Dish)
                {
                    var item = Content.DishRepository.GetItem(couponRuleElement.ElementId);
                    listBoxSelectedFor.Items.Add(item);
                }
            }
            foreach (var couponRuleElement in couponRuleElements.Where(x => x.ElementApplyDirection == ElementApplyDirection.RuleAppliedToElement))
            {
                if (couponRuleElement.ElementContext == CouponRuleElementEnum.Category)
                {
                    var item = Content.CategoryRepository.GetItem(couponRuleElement.ElementId);
                    if (item == null)
                        continue;
                    listBoxSelected.Items.Add(item);
                }
                else if (couponRuleElement.ElementContext == CouponRuleElementEnum.Dish)
                {
                    var item = Content.DishRepository.GetItem(couponRuleElement.ElementId);
                    if (item == null)
                        continue;
                    listBoxSelected.Items.Add(item);
                }
            }
        }

        private void FillBrandComboBox()
        {
            var brands = Content.BrandManager.GetList();
            comboBoxBrand.DisplayMember = "Name";
            comboBoxBrand.DataSource = brands;

            if (_couponRule == null)
                return;
            for(var i = 0; i < brands.Count; i++)
            {
                if (brands[i].ID == _couponRule.BrandId)
                {
                    comboBoxBrand.SelectedIndex = i;
                    return;
                }
            }
            
        }

        private void FillCategoryList()
        {
            listBoxCategory.Items.Clear();
            var list = Content.CategoryRepository.GetItems().OrderBy(y => y.Weight);
            listBoxCategory.Items.Add("Все");

            var brand = (Brand)comboBoxBrand.SelectedItem;
            if (brand == null)
                return;
            foreach (var category in list)
            {
                if (category.Brand != null && category.Brand.ID == brand.ID)
                {
                    listBoxCategory.Items.Add(category);
                }
            }
        }

        private void FillDishList()
        {
            var brand = (Brand)comboBoxBrand.SelectedItem;
            if (brand == null)
                return;
            var dishes = Content.DishRepository.GetItems(new DishFilter { FilterByBrandId = true, BrandId = brand.ID });
            dishes = dishes.OrderBy(x => x.Name);

            listBoxDish.Items.Clear();
            if (listBoxCategory.SelectedIndex > 0)
            {
                var category = (Category)listBoxCategory.SelectedItem;
                dishes = dishes.Where(x => x.CategoryID == category.ID && !x.IsDeleted);
            }

            foreach (var dish in dishes)
            {
                listBoxDish.Items.Add(dish);
            }
        }

        private void FillDiscountOverlap()
        {
            listViewDiscountOverlap.Items.Clear();

            _couponDiscountList = _couponDiscountList 
                ?? (_couponRule != null 
                    ? Content.CouponDiscountRepository.GetCouponDiscountsByCouponRuleId(_couponRule.ID)
                    : new List<CouponDiscount>());

            for (int i = 0; i < _couponDiscountList.Count; i++)
            {
                var discountItem = _couponDiscountList[i];
                var listViewItem = new ListViewItem(
                    new[]
                    {
                        (i + 1).ToString(),
                        discountItem.DiscountInPercent.ToString(),
                        discountItem.SumToApply.ToString(),
                        discountItem.DaysOfWeek.ToString(),
                        discountItem.TimeBegin.ToString(),
                        discountItem.TimeEnd.ToString(),
                    }
                );
                listViewDiscountOverlap.Items.Add(listViewItem);
            }
        }

        private void comboBoxBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            FillCategoryList();
            FillDishList();
        }

        private void listBoxCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            FillDishList();
        }

        private void listBoxCategory_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (!_isFormEnabled)
                return;
            _isFormEnabled = false;
            int index = listBoxCategory.IndexFromPoint(e.Location);
            if (index == ListBox.NoMatches)
            {
                _isFormEnabled = true;
                return;
            }
               

            var item = (Category)listBoxCategory.Items[index];
            if (checkBoxSelectedFor.Checked)
            {
                if (!listBoxSelectedFor.Items.Contains(item))
                {
                    listBoxSelectedFor.Items.Add(item);
                    for(int i = 0; i < listBoxSelectedFor.Items.Count; i++)
                    {
                        if ((listBoxSelectedFor.Items[i] as Dish)?.CategoryID == item.ID)
                        {
                            listBoxSelectedFor.Items.RemoveAt(i);
                            i--;
                        }
                    }
                }                    
            }
            else if (checkBoxSelected.Checked)
            {
                if (!listBoxSelected.Items.Contains(item))
                {
                    listBoxSelected.Items.Add(item);
                    for (int i = 0; i < listBoxSelected.Items.Count; i++)
                    {
                        if ((listBoxSelected.Items[i] as Dish)?.CategoryID == item.ID)
                        {
                            listBoxSelected.Items.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Выберите для применения или для применимости скидки добавить категорию");
            }
            _isFormEnabled = true;
        }

        private void listBoxDish_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (!_isFormEnabled)
                return;
            _isFormEnabled = false;

            int index = listBoxDish.IndexFromPoint(e.Location);
            if (index == ListBox.NoMatches)
            {
                _isFormEnabled = true;
                return;
            }

            var item = (Dish)listBoxDish.Items[index];
            if (checkBoxSelectedFor.Checked)
            {
                if (!listBoxSelectedFor.Items.Contains(item))
                {
                    bool isCategoryFound = false;
                    for (int i = 0; i < listBoxSelectedFor.Items.Count; i++)
                    {
                        if ((listBoxSelectedFor.Items[i] as Category)?.ID == item.CategoryID)
                        {
                            isCategoryFound = true;
                            break;
                        }
                    }
                    if (!isCategoryFound)
                        listBoxSelectedFor.Items.Add(item);
                    else
                        MessageBox.Show("Уже была добавлена категория");
                }                
            }
            else if (checkBoxSelected.Checked)
            {
                if (!listBoxSelected.Items.Contains(item))
                {
                    bool isCategoryFound = false;
                    for (int i = 0; i < listBoxSelected.Items.Count; i++)
                    {
                        if ((listBoxSelected.Items[i] as Category)?.ID == item.CategoryID)
                        {
                            isCategoryFound = true;
                            break;
                        }
                    }
                    if (!isCategoryFound)
                        listBoxSelected.Items.Add(item);
                    else
                        MessageBox.Show("Уже была добавлена категория");
                }
            }
            else
            {
                MessageBox.Show("Выберите для применения или для применимости скидки добавить блюдо");
            }
            _isFormEnabled = true;
        }

        private void listBoxSelected_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = listBoxSelected.IndexFromPoint(e.Location);
            if (index != ListBox.NoMatches)
            {
                var item = listBoxSelected.Items[index];
                listBoxSelected.Items.Remove(item);
            }
        }

        private void checkBoxSelectedFor_CheckedChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            if (checkBoxSelectedFor.Checked)
            {
                if (checkBoxSelected.Checked)
                {
                    checkBoxSelected.Checked = false;
                }
            }
            else
            {
                if (!checkBoxSelected.Checked)
                {
                    checkBoxSelected.Checked = true;
                }
            }
        }

        private void checkBoxSelected_CheckedChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            if (checkBoxSelected.Checked)
            {
                if (checkBoxSelectedFor.Checked)
                {
                    checkBoxSelectedFor.Checked = false;
                }
            }
            else
            {
                if (!checkBoxSelectedFor.Checked)
                {
                    checkBoxSelectedFor.Checked = true;
                }
            }
        }

        private void listBoxSelectedFor_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = listBoxSelectedFor.IndexFromPoint(e.Location);
            if (index != ListBox.NoMatches)
            {
                var item = listBoxSelectedFor.Items[index];
                listBoxSelectedFor.Items.Remove(item);
            }
        }
        
        private async Task<bool> SaveCouponRule()
        {
            if (_couponRule == null)
            {
                _couponRule = new CouponRule();
            }
            if (comboBoxBrand.SelectedItem == null)
            {
                MessageBox.Show("Не выбран бренд");
                return false;
            }
            _couponRule.BrandId = (comboBoxBrand.SelectedItem as Brand).ID;

            if (string.IsNullOrEmpty(textBoxName.Text))
            {
                MessageBox.Show("Введите название купона");
                return false;
            }
            _couponRule.Name = textBoxName.Text;

            if (comboBoxDiscountType.SelectedIndex == 0)
            {
                if (string.IsNullOrEmpty(textBoxDiscountValue.Text))
                {
                    MessageBox.Show("Введите фиксированную цену блюда");
                    return false;
                }
                if (int.TryParse(textBoxDiscountValue.Text, out int price))
                {
                    _couponRule.Price = price;
                } 
                else
                {
                    MessageBox.Show("Некорректно введена фиксированная цена блюда");
                    return false;
                }
            }
            else
            {
                _couponRule.Price = null;
            }

            if (comboBoxDiscountType.SelectedIndex == 1)
            {
                if (string.IsNullOrEmpty(textBoxDiscountValue.Text))
                {
                    MessageBox.Show("Введите скидку в процентах");
                    return false;
                }
                if (int.TryParse(textBoxDiscountValue.Text, out int percent))
                {
                    _couponRule.Percent = percent;
                }
                else
                {
                    MessageBox.Show("Некорректно введена скидка в процентах");
                    return false;
                }
            }
            else
            {
                _couponRule.Percent = null;
            }

            if (comboBoxDiscountType.SelectedIndex == 2)
            {
                if (string.IsNullOrEmpty(textBoxDiscountValue.Text))
                {
                    MessageBox.Show("Введите фиксированную скидку");
                    return false;
                }
                if (int.TryParse(textBoxDiscountValue.Text, out int fixDiscount))
                {
                    _couponRule.FixDiscount = fixDiscount;
                }
                else
                {
                    MessageBox.Show("Некорректно введена фиксированная скидка");
                    return false;
                }
            }
            else
            {
                _couponRule.FixDiscount = null;
            }

            if (comboBoxDiscountType.SelectedIndex == 3)
            {
                if (string.IsNullOrEmpty(textBoxDiscountValue.Text))
                {
                    MessageBox.Show("Введите номер блюда которое будет в подарок");
                    return false;
                }
                if (int.TryParse(textBoxDiscountValue.Text, out int giftDish))
                {
                    _couponRule.GiftDish = giftDish;
                }
                else
                {
                    MessageBox.Show("Некорректно введено номер блюда");
                    return false;
                }
            }
            else
            {
                _couponRule.GiftDish = null;
            }

            if (checkBoxSumToApply.Checked)
            {
                if (string.IsNullOrEmpty(textBoxSumToApply.Text))
                {
                    MessageBox.Show("Введите текст в поле \"Скидка применяется от суммы\"");
                    return false;
                }
                if (int.TryParse(textBoxSumToApply.Text, out int sumToApply))
                {
                    _couponRule.SumToApply = sumToApply;
                }
                else
                {
                    MessageBox.Show("Некорректно заполнено поле \"Скидка применяется от суммы\"");
                    return false;
                }
            }
            else
            {
                _couponRule.SumToApply = null;
            }

            _couponRule.AllDishRequired = checkBoxAllRequired.Checked;
            _couponRule.ApplyToAllDishes = checkBoxApplyToAll.Checked;
            _couponRule.From = dateTimePickerFrom.Value;
            _couponRule.To = dateTimePickerTo.Value;
            await Content.CouponRuleRepository.InsertOrUpdateAsync(_couponRule);
            await Content.CouponRuleElementRepository.DeleteByCouponRuleId(_couponRule.ID);
            var couponRuleElements = new List<CouponRuleElement>();
            foreach (var selectedFor in listBoxSelectedFor.Items)
            {
                var couponRuleElement = new CouponRuleElement { CouponRuleId = _couponRule.ID };
                couponRuleElement.ElementApplyDirection = ElementApplyDirection.ElementNeededToApplyRule;
                if (selectedFor is Category category)
                {
                    couponRuleElement.ElementContext = CouponRuleElementEnum.Category;
                    couponRuleElement.ElementId = category.ID;
                }
                else if (selectedFor is Dish dish)
                {
                    couponRuleElement.ElementContext = CouponRuleElementEnum.Dish;
                    couponRuleElement.ElementId = dish.ID;
                }
                couponRuleElements.Add(couponRuleElement);
            }

            foreach (var selected in listBoxSelected.Items)
            {
                var couponRuleElement = new CouponRuleElement { CouponRuleId = _couponRule.ID };
                couponRuleElement.ElementApplyDirection = ElementApplyDirection.RuleAppliedToElement;
                if (selected is Category category)
                {
                    couponRuleElement.ElementContext = CouponRuleElementEnum.Category;
                    couponRuleElement.ElementId = category.ID;
                }
                else if (selected is Dish dish)
                {
                    couponRuleElement.ElementContext = CouponRuleElementEnum.Dish;
                    couponRuleElement.ElementId = dish.ID;
                }
                couponRuleElements.Add(couponRuleElement);
            }

            foreach (var couponRuleElement in couponRuleElements)
            {
                await Content.CouponRuleElementRepository.InsertOrUpdateAsync(couponRuleElement);
            }
            SaveCouponDiscounts();
            return true;
        }

        private void SaveCouponDiscounts()
        {
            var oldCouponDiscounts = Content.CouponDiscountRepository
                .GetCouponDiscountsByCouponRuleId(_couponRule.ID);
            var couponDiscountsToDelete = oldCouponDiscounts.Where(x => !_couponDiscountList.Any(y => y.ID == x.ID)).ToList();
            foreach(var couponDiscount in couponDiscountsToDelete)
            {
                Content.CouponDiscountRepository.DeleteItem(couponDiscount);
            }
            foreach (var couponDiscount in _couponDiscountList)
            {
                couponDiscount.CouponRuleId = _couponRule.ID;
                Content.CouponDiscountRepository.InsertOrUpdate(couponDiscount);
            }
        }

        private async Task GenerateCoupons()
        {
            if (string.IsNullOrEmpty(textBoxCount.Text))
            {
                MessageBox.Show("Введите текст в поле \"Сколько купонов сгенерировать\"");
                return;
            }
            int couponsCountToGenerate;
            if (!int.TryParse(textBoxCount.Text, out couponsCountToGenerate))
            {
                MessageBox.Show("Некорректно заполнено поле \"Сколько купонов сгенерировать\"");
                return;
            }
            
            if (string.IsNullOrEmpty(textBoxLength.Text))
            {
                MessageBox.Show("Введите текст в поле \"Количество символов в купоне\"");
                return;
            }
            int couponCodeSymbolsCount;
            if (!int.TryParse(textBoxLength.Text, out couponCodeSymbolsCount))
            {
                MessageBox.Show("Некорректно заполнено поле \"Количество символов в купоне\"");
                return;
            }

            if (string.IsNullOrEmpty(textBoxLength.Text))
            {
                MessageBox.Show("Введите текст в поле \"Количество применений купона\"");
                return;
            }
            int applyCount;
            if (!int.TryParse(textBoxLength.Text, out applyCount))
            {
                MessageBox.Show("Некорректно заполнено поле \"Количество применений купона\"");
                return;
            }

            for (int a = 0; a < couponsCountToGenerate; a++)
            {
                var newCoupon = new Coupon
                {
                    Activated = false,
                    CouponRuleId = _couponRule.ID,
                    ActivationCount = applyCount,
                    Code = GetCoupon(couponCodeSymbolsCount)
                };

                await Content.CouponRepository.InsertOrUpdateAsync(newCoupon);
            }
        }

        private string GetCoupon(int length)
        {
            var coupon = string.Empty;

            Random random = new Random();
            for (int i = 0; i <= length; i++)
            {
                coupon += GetRandomCharFromString(CouponString, random);
            }
            var couponToCheck = coupon;
            var isExist = Content.CouponRepository.GetList(couponToCheck, false);
            if (isExist.Count > 0)
            {
                coupon = GetCoupon(length);
            }
            return coupon;
        }

        private char GetRandomCharFromString(string input, Random random)
        {
            int index = random.Next(0, input.Length);
            var result = input[index];
            return result;
        }

        private const string CouponString = "0123456789АБВГЕЗИКЛМНОПРСТХ";
        
        private void checkBoxSumToApply_CheckedChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            textBoxSumToApply.Enabled = checkBoxSumToApply.Checked;
            if (!textBoxSumToApply.Enabled)
                textBoxSumToApply.Text = "";
            else
                textBoxSumToApply.Focus();
        }

        private async void buttonSave_Click(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
            {
                MessageBox.Show("Подождите выполняется другое действие");
                return;
            }
            if (!_isFormEnabled)
                return;
            IsFormEnabled(false);
            try
            {
                await SaveCouponRule();
            }
            catch
            {
                MessageBox.Show("Произошла ошибка при сохранении");
            }
            IsFormEnabled(true);
        }

        private async void buttonGenerate_Click(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
            {
                MessageBox.Show("Подождите выполняется другое действие");
                return;
            }
            IsFormEnabled(false);
            try
            {
                if (await SaveCouponRule())
                {
                    await GenerateCoupons();
                }
            }
            catch
            {
                MessageBox.Show("Произошла ошибка при сохранении");
            }

            IsFormEnabled(true);
        }



        private void labelName_Click(object sender, EventArgs e)
        {
            textBoxName.Focus();
        }

        private void labelBrand_Click(object sender, EventArgs e)
        {
            comboBoxBrand.Focus();
        }

        private void labelCouponCount_Click(object sender, EventArgs e)
        {
            textBoxCount.Focus();
        }

        private void labelLegth_Click(object sender, EventArgs e)
        {
            textBoxLength.Focus();
        }

        private void labelApplyCount_Click(object sender, EventArgs e)
        {
            textBoxApplyCount.Focus();
        }

        private void labelFrom_Click(object sender, EventArgs e)
        {
            dateTimePickerFrom.Focus();
        }

        private void labelTo_Click(object sender, EventArgs e)
        {
            dateTimePickerTo.Focus();
        }

        private void buttonAddCouponDiscount_Click(object sender, EventArgs e)
        {
            ShowCouponDiscountEditForm();
        }

        private void listViewDiscountOverlap_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            foreach (int index in listViewDiscountOverlap.SelectedIndices)
            {
                ShowCouponDiscountEditForm(_couponDiscountList.ElementAt(index));
                break;
            }
        }

        private void ShowCouponDiscountEditForm(CouponDiscount couponDiscount = null)
        {
            var couponDiscointEditForm = new CouponDiscountEditForm(couponDiscount);
            var result = couponDiscointEditForm.ShowDialog();
            
            if (result == DialogResult.OK)
            {
                if (!_couponDiscountList.Contains(couponDiscointEditForm.CouponDiscount))
                {
                    _couponDiscountList.Add(couponDiscointEditForm.CouponDiscount);
                }
            }
            else if (result == DialogResult.Abort)
            {
                _couponDiscountList.Remove(couponDiscointEditForm.CouponDiscount);
            }
            else
            {
                return;
            }
            FillDiscountOverlap();
        }

        private void comboBoxDiscountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxDiscountType.SelectedIndex == 3)
            {
                checkBoxApplyToAll.Enabled = false;
                checkBoxApplyToAll.Checked = false;
            }
            else
            {                               
                checkBoxApplyToAll.Enabled = true;
                checkBoxApplyToAll.Checked = _couponRule?.ApplyToAllDishes ?? false;
            }
        }
    }
}