﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.DiscountCards;
using LaRenzo.FormsProvider;

namespace LaRenzo.Forms.DiscountCardForms
{
    public partial class DiscountCardsForm : Form
    {
        public DiscountCardsForm()
        {
            InitializeComponent();
            Bind();
        }

        private void Bind()
        {
            mainGridControl.DataSource = null;
            mainGridControl.DataSource = Content.DiscountCardManager.GetList();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var form = new DiscountCardDetails();
            form.Closed += delegate { Bind(); };
            SingletonFormProvider<DiscountCardDetails>.ActivateForm(() => form);
            
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Bind();
        }

        private void gridView1_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column.Name == "columnOrdersTotal")
            {
                var discountCard = e.Row as DiscountCard;
                if (discountCard != null) e.Value = discountCard.Orders.Count;
            }
            if (e.Column.Name == "columnOrdersCost")
            {
                var card = e.Row as DiscountCard;
                if (card != null) e.Value = card.Orders.Sum(x => x.TotalCost);
            }
            if (e.Column.Name == "columnOrdersCostWithoutDisc")
            {
                var dCard = e.Row as DiscountCard;
                if (dCard != null)
                    e.Value = dCard.Orders.Sum(x => x.TotalCostWithoutDiscount);
            }

            if (e.Column.Name == "columnOrdersCostDiff")
            {
                var discountCard = e.Row as DiscountCard;
                if (discountCard != null)
                    e.Value = discountCard.Orders.Sum(x => x.TotalCostWithoutDiscount - x.TotalCost);
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView)sender;

            Point pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var form = new DiscountCardDetails((int)mainGridView.GetRowCellValue(info.RowHandle, columnID));
                form.Closed += delegate { Bind(); };
                form.Show();
            }}

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Будут объеденены скидочный карты с одинковыми номерами. Вы уверены?", "Скидочные карты",MessageBoxButtons.YesNo) == DialogResult.No) return;

            try
            {
                Content.DiscountCardManager.MergeAll();
                Bind();
            }
            catch (DiscountCardMergeException ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            
        }
    }
}
