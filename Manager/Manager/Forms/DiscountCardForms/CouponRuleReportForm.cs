﻿using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaymentMethod = LaRenzo.TransferLibrary.PaymentMethod;

namespace LaRenzo.Forms.DiscountCardForms
{
    public partial class CouponRuleReportForm : Form
    {
        private readonly CouponRule _couponRule;
        private List<Order> _orders;

        public CouponRuleReportForm(CouponRule couponRule)
        {
            InitializeComponent();
            _couponRule = couponRule;
            FillCouponRuleReportView();
        }

        private void FillCouponRuleReportView()
        {
            CouponRuleReportView.Items.Clear();
            Text = $"Отчет по группе купонов - {_couponRule.Name}";
            _orders = Content.OrderManager.GetOrdersByCouponRuleId(_couponRule.ID);
            foreach (var order in _orders)
            {
                var listViewItem = new ListViewItem(new[]
                {
                    order.ID.ToString(),
                    order.Created.ToString(),
                    order.Phone,
                    $"{order.Client.LastName} {order.Client.FirstName} {order.Client.MiddleName}".TrimEnd(),
                    order.TotalCost.ToString(),
                    order.TotalCostWithoutDiscount.ToString(),
                    order.Source
                });
                CouponRuleReportView.Items.Add(listViewItem);
            }
        }

        private async void CouponRuleReportView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int orderIdAsInt = 0;
            foreach (var item in CouponRuleReportView.SelectedItems)
            {
                ListViewItem.ListViewSubItem orderId = ((ListViewItem)item).SubItems[0];
                orderIdAsInt = Convert.ToInt32(orderId.Text);
                break;
            }
            if (orderIdAsInt == 0)
                return;
            await ShowOrderDetails(orderIdAsInt);
        }

        private async Task ShowOrderDetails(int orderId)
        {
            if (Content.UserManager.UserLogged != null)
            {
                var orderInfo = _orders.FirstOrDefault(x => x.ID == orderId);

                if (orderInfo == null) return;

                var mode = DetailsFormModes.ViewProcessed;

                if (!orderInfo.State.DoSessionCanBeClosed)
                {
                    var userRights = Content.UserAccessManager.GetForUser(Content.UserManager.UserLogged.ID);
                    List<AccessRightInfo> rights = userRights.OrderBy(x => x.Code).ToList();
                    mode = Content.UserAccessManager.GroupContainsRight(rights, AccessRightAlias.EditOrder) ? DetailsFormModes.EditOrder : DetailsFormModes.ViewByOperator;
                }

                DeliveryOrderFormOptions formOptions = null;

                if (orderInfo.PaymentMethod == PaymentMethod.OnlineCard)
                {
                    formOptions = new DeliveryOrderFormOptions
                    {
                        SaveButtonText = "Заказы, оплаченные онлайн, редактировать запрещено",
                        DoBlockSaveButton = true,
                        DoBlockDishArea = true,
                        DoBlockReservationArea = true
                    };
                }

                await EditDetails(orderInfo, mode, formOptions);
            }
            
        }


        public async Task EditDetails(Order order, DetailsFormModes state = DetailsFormModes.EditOrder, DeliveryOrderFormOptions formOptions = null)
        {
            try
            {
                var form = new DeliveryOrderForm(state, order.ID, formOptions)
                {
                    CurentBrand = order.Brand
                };
                await form.Initialize();
                form.Show();
                form.Activate();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
