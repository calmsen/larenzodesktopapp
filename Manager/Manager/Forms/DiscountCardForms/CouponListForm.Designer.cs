﻿namespace LaRenzo.Forms.DiscountCardForms
{
    partial class CouponListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDeleteGroups = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDeleteCoupons = new System.Windows.Forms.ToolStripButton();
            this.toolStripAddCoupon = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.listBoxCoupons = new System.Windows.Forms.ListBox();
            this.listViewCodes = new System.Windows.Forms.ListView();
            this.Code = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ActivationCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Activated = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ActivatedCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonShowReport = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.btnRefresh,
            this.toolStripSeparator1,
            this.toolStripButtonDeleteGroups,
            this.toolStripButtonDeleteCoupons,
            this.toolStripAddCoupon,
            this.toolStripSeparator2,
            this.toolStripButtonShowReport});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1091, 31);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::LaRenzo.Properties.Resources.plus;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(28, 28);
            this.btnAdd.Text = "Добавить Купон";
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 28);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButtonDeleteGroups
            // 
            this.toolStripButtonDeleteGroups.Image = global::LaRenzo.Properties.Resources.cross_48;
            this.toolStripButtonDeleteGroups.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteGroups.Name = "toolStripButtonDeleteGroups";
            this.toolStripButtonDeleteGroups.Size = new System.Drawing.Size(144, 28);
            this.toolStripButtonDeleteGroups.Text = "Удалить группу";
            this.toolStripButtonDeleteGroups.Click += new System.EventHandler(this.toolStripButtonDeleteGroups_Click);
            // 
            // toolStripButtonDeleteCoupons
            // 
            this.toolStripButtonDeleteCoupons.Image = global::LaRenzo.Properties.Resources.cross_48;
            this.toolStripButtonDeleteCoupons.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteCoupons.Name = "toolStripButtonDeleteCoupons";
            this.toolStripButtonDeleteCoupons.Size = new System.Drawing.Size(149, 28);
            this.toolStripButtonDeleteCoupons.Text = "Удалить купоны";
            this.toolStripButtonDeleteCoupons.Click += new System.EventHandler(this.toolStripButtonDeleteCoupons_Click);
            // 
            // toolStripAddCoupon
            // 
            this.toolStripAddCoupon.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.toolStripAddCoupon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAddCoupon.Name = "toolStripAddCoupon";
            this.toolStripAddCoupon.Size = new System.Drawing.Size(149, 28);
            this.toolStripAddCoupon.Text = "Добавить купон";
            this.toolStripAddCoupon.Click += new System.EventHandler(this.toolStripButtonAddCoupon_Click);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.52979F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.47021F));
            this.tableLayoutPanel.Controls.Add(this.listBoxCoupons, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.listViewCodes, 1, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 31);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(1091, 591);
            this.tableLayoutPanel.TabIndex = 1;
            // 
            // listBoxCoupons
            // 
            this.listBoxCoupons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxCoupons.FormattingEnabled = true;
            this.listBoxCoupons.ItemHeight = 16;
            this.listBoxCoupons.Location = new System.Drawing.Point(4, 4);
            this.listBoxCoupons.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listBoxCoupons.Name = "listBoxCoupons";
            this.listBoxCoupons.Size = new System.Drawing.Size(455, 583);
            this.listBoxCoupons.TabIndex = 0;
            this.listBoxCoupons.SelectedIndexChanged += new System.EventHandler(this.listBoxCoupons_SelectedIndexChanged);
            // 
            // listViewCodes
            // 
            this.listViewCodes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Code,
            this.ActivationCount,
            this.Activated,
            this.ActivatedCount});
            this.listViewCodes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewCodes.Location = new System.Drawing.Point(467, 4);
            this.listViewCodes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listViewCodes.Name = "listViewCodes";
            this.listViewCodes.Size = new System.Drawing.Size(620, 583);
            this.listViewCodes.TabIndex = 1;
            this.listViewCodes.UseCompatibleStateImageBehavior = false;
            this.listViewCodes.View = System.Windows.Forms.View.Details;
            this.listViewCodes.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewCodes_MouseDoubleClick);
            // 
            // Code
            // 
            this.Code.Name = "Code";
            this.Code.Text = "Код";
            this.Code.Width = 31;
            // 
            // ActivationCount
            // 
            this.ActivationCount.Name = "ActivationCount";
            this.ActivationCount.Text = "Количество активаций";
            this.ActivationCount.Width = 127;
            // 
            // Activated
            // 
            this.Activated.Name = "Activated";
            this.Activated.Text = "Активирован";
            this.Activated.Width = 98;
            // 
            // ActivatedCount
            // 
            this.ActivatedCount.Text = "Сколько раз активирован";
            this.ActivatedCount.Width = 141;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripButtonShowReport
            // 
            this.toolStripButtonShowReport.Image = global::LaRenzo.Properties.Resources.credit_card_2008;
            this.toolStripButtonShowReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonShowReport.Name = "toolStripButtonShowReport";
            this.toolStripButtonShowReport.Size = new System.Drawing.Size(142, 28);
            this.toolStripButtonShowReport.Text = "Показать отчет";
            this.toolStripButtonShowReport.Click += new System.EventHandler(this.toolStripButtonShowReport_Click);
            // 
            // CouponListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 622);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.toolStrip1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "CouponListForm";
            this.Text = "Работа с купонами";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.ListBox listBoxCoupons;
        private System.Windows.Forms.ListView listViewCodes;
        private System.Windows.Forms.ColumnHeader columnHeaderCode;
        private System.Windows.Forms.ColumnHeader columnHeaderCount;
        private System.Windows.Forms.ColumnHeader columnHeaderActivated;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteGroups;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteCoupons;
        private System.Windows.Forms.ColumnHeader Code;
        private System.Windows.Forms.ColumnHeader ActivationCount;
        private System.Windows.Forms.ColumnHeader Activated;
        private System.Windows.Forms.ToolStripButton toolStripAddCoupon;
        private System.Windows.Forms.ColumnHeader ActivatedCount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButtonShowReport;
    }
}