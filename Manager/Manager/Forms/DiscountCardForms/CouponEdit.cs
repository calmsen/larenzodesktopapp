﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.DataRepository.Repositories;

namespace LaRenzo.Forms.DiscountCardForms
{
    public partial class CouponEdit : Form
    {
        private Coupon _coupon;
        private readonly int _couponRuleId;


        //___________________________________________________________________________________________________________________________________________________________________________________
        public CouponEdit(int coupoRuleId)
        {
            _couponRuleId = coupoRuleId;
            InitializeComponent();
            okButton.Click += async (s, e) => await OkButtonClick();
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        public CouponEdit(Coupon coupon)
        {
            _coupon = coupon;
            InitializeComponent();
            okButton.Click += async (s, e) => await OkButtonClick();
            nameTextBox.Text = _coupon.Code;
            textBoxActivation.Text = _coupon.ActivationCount.ToString();
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнопка "Сохранить" </summary>
        ////=======================================
        private async Task OkButtonClick()
        {
            if (_coupon == null)
            {
                _coupon = new Coupon { CouponRuleId = _couponRuleId };
            }
            _coupon.Code = nameTextBox.Text;
            _coupon.ActivationCount = Convert.ToInt32(textBoxActivation.Text);
            await Content.CouponRepository.InsertOrUpdateAsync(_coupon);
            DialogResult = DialogResult.OK;
            Close();
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнопка "Удалить" </summary>
        ////=======================================
        private void CancelButtonClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
