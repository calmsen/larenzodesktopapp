﻿namespace LaRenzo.Forms.DiscountCardForms
{
	partial class DiscountCardDetails
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.pnlCardNumber = new System.Windows.Forms.Panel();
			this.numCardNumber = new System.Windows.Forms.NumericUpDown();
			this.lblCardNumber = new System.Windows.Forms.Label();
			this.pnlName = new System.Windows.Forms.Panel();
			this.txtName = new System.Windows.Forms.TextBox();
			this.lblName = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.txtFatherName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.pnlPhone = new System.Windows.Forms.Panel();
			this.txtPhone = new System.Windows.Forms.MaskedTextBox();
			this.lblPhone = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.txtEmail = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.pnlStreet = new System.Windows.Forms.Panel();
			this.cmbStreet = new System.Windows.Forms.ComboBox();
			this.lblStreet = new System.Windows.Forms.Label();
			this.pnlHouse = new System.Windows.Forms.Panel();
			this.txtHouse = new System.Windows.Forms.TextBox();
			this.lblHouse = new System.Windows.Forms.Label();
			this.pnlFlat = new System.Windows.Forms.Panel();
			this.txtFlat = new System.Windows.Forms.TextBox();
			this.lblFlat = new System.Windows.Forms.Label();
			this.pnlPorch = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtPorch = new System.Windows.Forms.TextBox();
			this.lblPorch = new System.Windows.Forms.Label();
			this.pnlDiscounting = new System.Windows.Forms.Panel();
			this.discComboBox = new System.Windows.Forms.ComboBox();
			this.discLabel = new System.Windows.Forms.Label();
			this.pnlBtn = new System.Windows.Forms.Panel();
			this.btnSave = new System.Windows.Forms.Button();
			this.mainGridControl = new DevExpress.XtraGrid.GridControl();
			this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.columnID = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnDateCreating = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnOrderSum = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnSumWithout = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnOrderCostDiff = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnStreet = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnHouse = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnFlat = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
			this.сolumnDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::LaRenzo.Forms.WaitFormGeneral), true, true);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.pnlCardNumber.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numCardNumber)).BeginInit();
			this.pnlName.SuspendLayout();
			this.panel2.SuspendLayout();
			this.pnlPhone.SuspendLayout();
			this.panel1.SuspendLayout();
			this.pnlStreet.SuspendLayout();
			this.pnlHouse.SuspendLayout();
			this.pnlFlat.SuspendLayout();
			this.pnlPorch.SuspendLayout();
			this.panel3.SuspendLayout();
			this.pnlDiscounting.SuspendLayout();
			this.pnlBtn.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.mainGridControl);
			this.splitContainer1.Size = new System.Drawing.Size(1038, 613);
			this.splitContainer1.SplitterDistance = 138;
			this.splitContainer1.TabIndex = 0;
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Controls.Add(this.pnlCardNumber);
			this.flowLayoutPanel1.Controls.Add(this.pnlName);
			this.flowLayoutPanel1.Controls.Add(this.panel2);
			this.flowLayoutPanel1.Controls.Add(this.pnlPhone);
			this.flowLayoutPanel1.Controls.Add(this.panel1);
			this.flowLayoutPanel1.Controls.Add(this.pnlStreet);
			this.flowLayoutPanel1.Controls.Add(this.pnlHouse);
			this.flowLayoutPanel1.Controls.Add(this.pnlFlat);
			this.flowLayoutPanel1.Controls.Add(this.pnlPorch);
			this.flowLayoutPanel1.Controls.Add(this.pnlDiscounting);
			this.flowLayoutPanel1.Controls.Add(this.pnlBtn);
			this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(138, 613);
			this.flowLayoutPanel1.TabIndex = 0;
			// 
			// pnlCardNumber
			// 
			this.pnlCardNumber.Controls.Add(this.numCardNumber);
			this.pnlCardNumber.Controls.Add(this.lblCardNumber);
			this.pnlCardNumber.Location = new System.Drawing.Point(3, 3);
			this.pnlCardNumber.Name = "pnlCardNumber";
			this.pnlCardNumber.Size = new System.Drawing.Size(134, 45);
			this.pnlCardNumber.TabIndex = 0;
			// 
			// numCardNumber
			// 
			this.numCardNumber.Location = new System.Drawing.Point(4, 20);
			this.numCardNumber.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
			this.numCardNumber.Minimum = new decimal(new int[] {
            301,
            0,
            0,
            0});
			this.numCardNumber.Name = "numCardNumber";
			this.numCardNumber.Size = new System.Drawing.Size(127, 20);
			this.numCardNumber.TabIndex = 1;
			this.numCardNumber.Value = new decimal(new int[] {
            301,
            0,
            0,
            0});
			// 
			// lblCardNumber
			// 
			this.lblCardNumber.AutoSize = true;
			this.lblCardNumber.Location = new System.Drawing.Point(4, 4);
			this.lblCardNumber.Name = "lblCardNumber";
			this.lblCardNumber.Size = new System.Drawing.Size(75, 13);
			this.lblCardNumber.TabIndex = 0;
			this.lblCardNumber.Text = "Номер карты";
			// 
			// pnlName
			// 
			this.pnlName.Controls.Add(this.txtName);
			this.pnlName.Controls.Add(this.lblName);
			this.pnlName.Location = new System.Drawing.Point(3, 54);
			this.pnlName.Name = "pnlName";
			this.pnlName.Size = new System.Drawing.Size(134, 45);
			this.pnlName.TabIndex = 1;
			// 
			// txtName
			// 
			this.txtName.Location = new System.Drawing.Point(4, 21);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(127, 20);
			this.txtName.TabIndex = 2;
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(4, 4);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(29, 13);
			this.lblName.TabIndex = 0;
			this.lblName.Text = "Имя";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.txtFatherName);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Location = new System.Drawing.Point(3, 105);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(134, 45);
			this.panel2.TabIndex = 9;
			// 
			// txtFatherName
			// 
			this.txtFatherName.Location = new System.Drawing.Point(3, 25);
			this.txtFatherName.Name = "txtFatherName";
			this.txtFatherName.Size = new System.Drawing.Size(127, 20);
			this.txtFatherName.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(4, 4);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(54, 13);
			this.label2.TabIndex = 0;
			this.label2.Text = "Отчество";
			// 
			// pnlPhone
			// 
			this.pnlPhone.Controls.Add(this.txtPhone);
			this.pnlPhone.Controls.Add(this.lblPhone);
			this.pnlPhone.Location = new System.Drawing.Point(3, 156);
			this.pnlPhone.Name = "pnlPhone";
			this.pnlPhone.Size = new System.Drawing.Size(134, 45);
			this.pnlPhone.TabIndex = 7;
			// 
			// txtPhone
			// 
			this.txtPhone.Location = new System.Drawing.Point(4, 20);
			this.txtPhone.Mask = "(000) 000-00-00";
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Size = new System.Drawing.Size(126, 20);
			this.txtPhone.TabIndex = 4;
			this.txtPhone.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
			this.txtPhone.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.TextPhone_MaskInputRejected);
			this.txtPhone.TextChanged += new System.EventHandler(this.TextPhone_TextChanged);
			// 
			// lblPhone
			// 
			this.lblPhone.AutoSize = true;
			this.lblPhone.Location = new System.Drawing.Point(4, 4);
			this.lblPhone.Name = "lblPhone";
			this.lblPhone.Size = new System.Drawing.Size(52, 13);
			this.lblPhone.TabIndex = 0;
			this.lblPhone.Text = "Телефон";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.txtEmail);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(3, 207);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(134, 45);
			this.panel1.TabIndex = 8;
			// 
			// txtEmail
			// 
			this.txtEmail.Location = new System.Drawing.Point(3, 25);
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Size = new System.Drawing.Size(127, 20);
			this.txtEmail.TabIndex = 5;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(4, 4);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(32, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Email";
			// 
			// pnlStreet
			// 
			this.pnlStreet.Controls.Add(this.cmbStreet);
			this.pnlStreet.Controls.Add(this.lblStreet);
			this.pnlStreet.Location = new System.Drawing.Point(3, 258);
			this.pnlStreet.Name = "pnlStreet";
			this.pnlStreet.Size = new System.Drawing.Size(134, 45);
			this.pnlStreet.TabIndex = 2;
			// 
			// cmbStreet
			// 
			this.cmbStreet.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.cmbStreet.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
			this.cmbStreet.FormattingEnabled = true;
			this.cmbStreet.Location = new System.Drawing.Point(4, 20);
			this.cmbStreet.Name = "cmbStreet";
			this.cmbStreet.Size = new System.Drawing.Size(126, 21);
			this.cmbStreet.TabIndex = 6;
			// 
			// lblStreet
			// 
			this.lblStreet.AutoSize = true;
			this.lblStreet.Location = new System.Drawing.Point(4, 4);
			this.lblStreet.Name = "lblStreet";
			this.lblStreet.Size = new System.Drawing.Size(39, 13);
			this.lblStreet.TabIndex = 0;
			this.lblStreet.Text = "Улица";
			// 
			// pnlHouse
			// 
			this.pnlHouse.Controls.Add(this.txtHouse);
			this.pnlHouse.Controls.Add(this.lblHouse);
			this.pnlHouse.Location = new System.Drawing.Point(3, 309);
			this.pnlHouse.Name = "pnlHouse";
			this.pnlHouse.Size = new System.Drawing.Size(134, 45);
			this.pnlHouse.TabIndex = 3;
			// 
			// txtHouse
			// 
			this.txtHouse.Location = new System.Drawing.Point(4, 21);
			this.txtHouse.Name = "txtHouse";
			this.txtHouse.Size = new System.Drawing.Size(127, 20);
			this.txtHouse.TabIndex = 7;
			// 
			// lblHouse
			// 
			this.lblHouse.AutoSize = true;
			this.lblHouse.Location = new System.Drawing.Point(4, 4);
			this.lblHouse.Name = "lblHouse";
			this.lblHouse.Size = new System.Drawing.Size(30, 13);
			this.lblHouse.TabIndex = 0;
			this.lblHouse.Text = "Дом";
			// 
			// pnlFlat
			// 
			this.pnlFlat.Controls.Add(this.txtFlat);
			this.pnlFlat.Controls.Add(this.lblFlat);
			this.pnlFlat.Location = new System.Drawing.Point(3, 360);
			this.pnlFlat.Name = "pnlFlat";
			this.pnlFlat.Size = new System.Drawing.Size(134, 45);
			this.pnlFlat.TabIndex = 4;
			// 
			// txtFlat
			// 
			this.txtFlat.Location = new System.Drawing.Point(4, 21);
			this.txtFlat.Name = "txtFlat";
			this.txtFlat.Size = new System.Drawing.Size(127, 20);
			this.txtFlat.TabIndex = 8;
			// 
			// lblFlat
			// 
			this.lblFlat.AutoSize = true;
			this.lblFlat.Location = new System.Drawing.Point(4, 4);
			this.lblFlat.Name = "lblFlat";
			this.lblFlat.Size = new System.Drawing.Size(55, 13);
			this.lblFlat.TabIndex = 0;
			this.lblFlat.Text = "Квартира";
			// 
			// pnlPorch
			// 
			this.pnlPorch.Controls.Add(this.panel3);
			this.pnlPorch.Controls.Add(this.txtPorch);
			this.pnlPorch.Controls.Add(this.lblPorch);
			this.pnlPorch.Location = new System.Drawing.Point(3, 411);
			this.pnlPorch.Name = "pnlPorch";
			this.pnlPorch.Size = new System.Drawing.Size(134, 45);
			this.pnlPorch.TabIndex = 5;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.textBox1);
			this.panel3.Controls.Add(this.label3);
			this.panel3.Location = new System.Drawing.Point(2, 46);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(134, 45);
			this.panel3.TabIndex = 7;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(4, 21);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(127, 20);
			this.textBox1.TabIndex = 6;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(4, 4);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 13);
			this.label3.TabIndex = 0;
			this.label3.Text = "Подъезд";
			// 
			// txtPorch
			// 
			this.txtPorch.Location = new System.Drawing.Point(4, 21);
			this.txtPorch.Name = "txtPorch";
			this.txtPorch.Size = new System.Drawing.Size(127, 20);
			this.txtPorch.TabIndex = 9;
			// 
			// lblPorch
			// 
			this.lblPorch.AutoSize = true;
			this.lblPorch.Location = new System.Drawing.Point(4, 4);
			this.lblPorch.Name = "lblPorch";
			this.lblPorch.Size = new System.Drawing.Size(52, 13);
			this.lblPorch.TabIndex = 0;
			this.lblPorch.Text = "Подъезд";
			// 
			// pnlDiscounting
			// 
			this.pnlDiscounting.Controls.Add(this.discComboBox);
			this.pnlDiscounting.Controls.Add(this.discLabel);
			this.pnlDiscounting.Location = new System.Drawing.Point(3, 462);
			this.pnlDiscounting.Name = "pnlDiscounting";
			this.pnlDiscounting.Size = new System.Drawing.Size(134, 45);
			this.pnlDiscounting.TabIndex = 7;
			// 
			// discComboBox
			// 
			this.discComboBox.FormattingEnabled = true;
			this.discComboBox.Items.AddRange(new object[] {
            "0",
            "3",
            "5",
            "7",
            "10",
            "15",
            "20"});
			this.discComboBox.Location = new System.Drawing.Point(4, 20);
			this.discComboBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.discComboBox.Name = "discComboBox";
			this.discComboBox.Size = new System.Drawing.Size(126, 21);
			this.discComboBox.TabIndex = 10;
			// 
			// discLabel
			// 
			this.discLabel.AutoSize = true;
			this.discLabel.Location = new System.Drawing.Point(5, 4);
			this.discLabel.Name = "discLabel";
			this.discLabel.Size = new System.Drawing.Size(44, 13);
			this.discLabel.TabIndex = 1;
			this.discLabel.Text = "Скидка";
			// 
			// pnlBtn
			// 
			this.pnlBtn.Controls.Add(this.btnSave);
			this.pnlBtn.Location = new System.Drawing.Point(3, 513);
			this.pnlBtn.Name = "pnlBtn";
			this.pnlBtn.Size = new System.Drawing.Size(134, 45);
			this.pnlBtn.TabIndex = 6;
			// 
			// btnSave
			// 
			this.btnSave.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnSave.Location = new System.Drawing.Point(0, 0);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(134, 45);
			this.btnSave.TabIndex = 11;
			this.btnSave.Text = "Сохранить";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// mainGridControl
			// 
			this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainGridControl.Location = new System.Drawing.Point(0, 0);
			this.mainGridControl.MainView = this.mainGridView;
			this.mainGridControl.Name = "mainGridControl";
			this.mainGridControl.Size = new System.Drawing.Size(896, 613);
			this.mainGridControl.TabIndex = 999;
			this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
			// 
			// mainGridView
			// 
			this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnID,
            this.columnDateCreating,
            this.columnOrderSum,
            this.columnSumWithout,
            this.columnOrderCostDiff,
            this.columnName,
            this.columnStreet,
            this.columnHouse,
            this.columnFlat,
            this.columnPhone,
            this.сolumnDiscount});
			this.mainGridView.GridControl = this.mainGridControl;
			this.mainGridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrderCostDiffCol", this.columnOrderCostDiff, "")});
			this.mainGridView.Name = "mainGridView";
			this.mainGridView.OptionsView.ShowAutoFilterRow = true;
			this.mainGridView.OptionsView.ShowFooter = true;
			this.mainGridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
			// 
			// columnID
			// 
			this.columnID.Caption = "Ид";
			this.columnID.FieldName = "ID";
			this.columnID.Name = "columnID";
			this.columnID.OptionsColumn.AllowEdit = false;
			this.columnID.OptionsColumn.ReadOnly = true;
			// 
			// columnDateCreating
			// 
			this.columnDateCreating.Caption = "Дата создания";
			this.columnDateCreating.FieldName = "Created";
			this.columnDateCreating.Name = "columnDateCreating";
			this.columnDateCreating.OptionsColumn.AllowEdit = false;
			this.columnDateCreating.OptionsColumn.ReadOnly = true;
			this.columnDateCreating.Visible = true;
			this.columnDateCreating.VisibleIndex = 5;
			this.columnDateCreating.Width = 214;
			// 
			// columnOrderSum
			// 
			this.columnOrderSum.Caption = "Сумма заказа";
			this.columnOrderSum.FieldName = "TotalCost";
			this.columnOrderSum.Name = "columnOrderSum";
			this.columnOrderSum.OptionsColumn.AllowEdit = false;
			this.columnOrderSum.OptionsColumn.ReadOnly = true;
			this.columnOrderSum.Visible = true;
			this.columnOrderSum.VisibleIndex = 6;
			this.columnOrderSum.Width = 163;
			// 
			// columnSumWithout
			// 
			this.columnSumWithout.Caption = "Сумма заказа без скидки";
			this.columnSumWithout.FieldName = "TotalCostWithoutDiscount";
			this.columnSumWithout.Name = "columnSumWithout";
			this.columnSumWithout.OptionsColumn.AllowEdit = false;
			this.columnSumWithout.OptionsColumn.ReadOnly = true;
			this.columnSumWithout.Visible = true;
			this.columnSumWithout.VisibleIndex = 7;
			this.columnSumWithout.Width = 430;
			// 
			// columnOrderCostDiff
			// 
			this.columnOrderCostDiff.Caption = "Скидка";
			this.columnOrderCostDiff.FieldName = "gridColumn5";
			this.columnOrderCostDiff.Name = "columnOrderCostDiff";
			this.columnOrderCostDiff.OptionsColumn.AllowEdit = false;
			this.columnOrderCostDiff.OptionsColumn.ReadOnly = true;
			this.columnOrderCostDiff.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
			this.columnOrderCostDiff.Visible = true;
			this.columnOrderCostDiff.VisibleIndex = 8;
			this.columnOrderCostDiff.Width = 192;
			// 
			// columnName
			// 
			this.columnName.Caption = "Имя";
			this.columnName.FieldName = "Name";
			this.columnName.Name = "columnName";
			this.columnName.OptionsColumn.AllowEdit = false;
			this.columnName.OptionsColumn.ReadOnly = true;
			this.columnName.Visible = true;
			this.columnName.VisibleIndex = 0;
			this.columnName.Width = 207;
			// 
			// columnStreet
			// 
			this.columnStreet.Caption = "Улица";
			this.columnStreet.FieldName = "Street";
			this.columnStreet.Name = "columnStreet";
			this.columnStreet.OptionsColumn.AllowEdit = false;
			this.columnStreet.OptionsColumn.ReadOnly = true;
			this.columnStreet.Visible = true;
			this.columnStreet.VisibleIndex = 2;
			this.columnStreet.Width = 205;
			// 
			// columnHouse
			// 
			this.columnHouse.Caption = "Дом";
			this.columnHouse.FieldName = "House";
			this.columnHouse.Name = "columnHouse";
			this.columnHouse.OptionsColumn.AllowEdit = false;
			this.columnHouse.OptionsColumn.ReadOnly = true;
			this.columnHouse.Visible = true;
			this.columnHouse.VisibleIndex = 3;
			this.columnHouse.Width = 88;
			// 
			// columnFlat
			// 
			this.columnFlat.Caption = "Квартира";
			this.columnFlat.FieldName = "Appartament";
			this.columnFlat.Name = "columnFlat";
			this.columnFlat.OptionsColumn.AllowEdit = false;
			this.columnFlat.OptionsColumn.ReadOnly = true;
			this.columnFlat.Visible = true;
			this.columnFlat.VisibleIndex = 4;
			this.columnFlat.Width = 90;
			// 
			// columnPhone
			// 
			this.columnPhone.Caption = "Телефон";
			this.columnPhone.FieldName = "Phone";
			this.columnPhone.Name = "columnPhone";
			this.columnPhone.OptionsColumn.AllowEdit = false;
			this.columnPhone.OptionsColumn.ReadOnly = true;
			this.columnPhone.Visible = true;
			this.columnPhone.VisibleIndex = 1;
			this.columnPhone.Width = 145;
			// 
			// сolumnDiscount
			// 
			this.сolumnDiscount.Caption = "% скидки";
			this.сolumnDiscount.FieldName = "Discount";
			this.сolumnDiscount.Name = "сolumnDiscount";
			this.сolumnDiscount.OptionsColumn.AllowEdit = false;
			this.сolumnDiscount.OptionsColumn.ReadOnly = true;
			this.сolumnDiscount.Visible = true;
			this.сolumnDiscount.VisibleIndex = 9;
			// 
			// DiscountCardDetails
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1038, 613);
			this.Controls.Add(this.splitContainer1);
			this.Name = "DiscountCardDetails";
			this.Text = "Скидочные карты";
			this.Load += new System.EventHandler(this.DiscountCardDetails_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.flowLayoutPanel1.ResumeLayout(false);
			this.pnlCardNumber.ResumeLayout(false);
			this.pnlCardNumber.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numCardNumber)).EndInit();
			this.pnlName.ResumeLayout(false);
			this.pnlName.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.pnlPhone.ResumeLayout(false);
			this.pnlPhone.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.pnlStreet.ResumeLayout(false);
			this.pnlStreet.PerformLayout();
			this.pnlHouse.ResumeLayout(false);
			this.pnlHouse.PerformLayout();
			this.pnlFlat.ResumeLayout(false);
			this.pnlFlat.PerformLayout();
			this.pnlPorch.ResumeLayout(false);
			this.pnlPorch.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.pnlDiscounting.ResumeLayout(false);
			this.pnlDiscounting.PerformLayout();
			this.pnlBtn.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private DevExpress.XtraGrid.GridControl mainGridControl;
		private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Panel pnlCardNumber;
		private System.Windows.Forms.Label lblCardNumber;
		private System.Windows.Forms.Panel pnlName;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.Panel pnlStreet;
		private System.Windows.Forms.Label lblStreet;
		private System.Windows.Forms.Panel pnlHouse;
		private System.Windows.Forms.Label lblHouse;
		private System.Windows.Forms.Panel pnlFlat;
		private System.Windows.Forms.Label lblFlat;
		private System.Windows.Forms.Panel pnlPorch;
		private System.Windows.Forms.Panel pnlDiscounting;
		private System.Windows.Forms.Label lblPorch;
		private System.Windows.Forms.Panel pnlBtn;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Panel pnlPhone;
		private System.Windows.Forms.Label lblPhone;
		private System.Windows.Forms.ComboBox cmbStreet;
		private System.Windows.Forms.MaskedTextBox txtPhone;
		private System.Windows.Forms.NumericUpDown numCardNumber;
		private System.Windows.Forms.TextBox txtHouse;
		private System.Windows.Forms.TextBox txtFlat;
		private System.Windows.Forms.TextBox txtPorch;
		private DevExpress.XtraGrid.Columns.GridColumn columnID;
		private DevExpress.XtraGrid.Columns.GridColumn columnDateCreating;
		private DevExpress.XtraGrid.Columns.GridColumn columnOrderSum;
		private DevExpress.XtraGrid.Columns.GridColumn columnSumWithout;
		private DevExpress.XtraGrid.Columns.GridColumn columnOrderCostDiff;
		private DevExpress.XtraGrid.Columns.GridColumn columnName;
		private DevExpress.XtraGrid.Columns.GridColumn columnStreet;
		private DevExpress.XtraGrid.Columns.GridColumn columnHouse;
		private DevExpress.XtraGrid.Columns.GridColumn columnFlat;
		private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TextBox txtEmail;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox txtFatherName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox discComboBox;
		private System.Windows.Forms.Label discLabel;
		private DevExpress.XtraGrid.Columns.GridColumn сolumnDiscount;
		private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
	}
}