﻿using LaRenzo.DataRepository.Entities.Catalogs.DiscountCards;
using LaRenzo.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaRenzo.Forms.DiscountCardForms
{
    public partial class CouponDiscountEditForm : Form
    {
        public CouponDiscount CouponDiscount { get; private set; }
        private bool _isFormEnabled;
        private ErrorProvider _errorProvider;

        public CouponDiscountEditForm(CouponDiscount couponDiscount = null)
        {
            InitializeComponent();
            CouponDiscount = couponDiscount;
            
            FillForm();

            _errorProvider = new ErrorProvider();
            SetFormEnabled(true);
        }
                
        private void SetFormEnabled(bool enabled)
        {
            buttonSave.Enabled = true;
            buttonDelete.Enabled = true;
            _isFormEnabled = true;
        }

        private void FillForm()
        {
            if (CouponDiscount == null)
                return;
            textBoxDiscountInPercent.Text = CouponDiscount.DiscountInPercent.ToString();
            textBoxSumToApply.Text = CouponDiscount.SumToApply.ToString();
            pickerTimeBegin.Value = pickerTimeBegin.MinDate.AddTicks(CouponDiscount.TimeBegin.Ticks);
            pickerTimeEnd.Value = pickerTimeEnd.MinDate.AddTicks(CouponDiscount.TimeEnd.Ticks);
            FillCheckedListBoxDaysOfWeek(CouponDiscount.DaysOfWeek);
            FillGroupListDaysOfWeek(CouponDiscount.DaysOfWeek);
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            DialogResult = DialogResult.Abort;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            if (!ValidateForm())
                return;
            CouponDiscount = CouponDiscount ?? new CouponDiscount();
            CouponDiscount.DiscountInPercent = int.Parse(textBoxDiscountInPercent.Text.Trim());
            CouponDiscount.SumToApply = int.Parse(textBoxSumToApply.Text.Trim());
            CouponDiscount.TimeBegin = new TimeSpan(pickerTimeBegin.Value.Hour, pickerTimeBegin.Value.Minute, pickerTimeBegin.Value.Second);
            CouponDiscount.TimeEnd = new TimeSpan(pickerTimeEnd.Value.Hour, pickerTimeEnd.Value.Minute, pickerTimeEnd.Value.Second);
            CouponDiscount.DaysOfWeek = GetDaysOfWeek();
            if (CouponDiscount.DaysOfWeek == DaysOfWeekEnum.None)
                CouponDiscount.DaysOfWeek = DaysOfWeekEnum.All;
            DialogResult = DialogResult.OK;
        }

        private bool ValidateTextBoxDiscountInPercent()
        {
            string discountInPercent = textBoxDiscountInPercent.Text.Trim();
            if (string.IsNullOrEmpty(discountInPercent))
            {
                _errorProvider.SetError(textBoxDiscountInPercent, "Введите значение");
                return false;
            }
            if (!int.TryParse(discountInPercent, out int discount))
            {
                _errorProvider.SetError(textBoxDiscountInPercent, "Некорректно введено значение");
                return false;
            }
            _errorProvider.SetError(textBoxDiscountInPercent, "");
            return true;
        }

        private bool ValidateTextBoxSumToApply()
        {
            string sumToApply = textBoxSumToApply.Text.Trim();
            if (string.IsNullOrEmpty(sumToApply))
            {
                _errorProvider.SetError(textBoxSumToApply, "Введите значение");
                return false;
            }
            if (!int.TryParse(sumToApply, out int sum))
            {
                _errorProvider.SetError(textBoxSumToApply, "Некорректно введено значение");
                return false;
            }
            _errorProvider.SetError(textBoxSumToApply, "");
            return true;
        }

        private bool ValidatePickerTimeEnd()
        {
            if (pickerTimeBegin.Value > pickerTimeEnd.Value)
            {
                _errorProvider.SetError(pickerTimeEnd, Resources.TimeBeginMatchMore);
                return false;
            }
            _errorProvider.SetError(pickerTimeEnd, "");
            return true;
        }

        private bool ValidateForm()
        {
            return ValidateTextBoxDiscountInPercent() && ValidateTextBoxSumToApply() && ValidatePickerTimeEnd();
        }

        private void radioButtonAllDays_CheckedChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            if (!(sender as RadioButton).Checked)
                return;
            FillCheckedListBoxDaysOfWeek(DaysOfWeekEnum.All);
        }

        private void radioButtonWeekdays_CheckedChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            if (!(sender as RadioButton).Checked)
                return;
            FillCheckedListBoxDaysOfWeek(DaysOfWeekEnum.Weekdays);
        }

        private void radioButtonWeekends_CheckedChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            if (!(sender as RadioButton).Checked)
                return;
            FillCheckedListBoxDaysOfWeek(DaysOfWeekEnum.Weekends);
        }

        private void FillCheckedListBoxDaysOfWeek(DaysOfWeekEnum daysOfWeek)
        {
            checkedListBoxDaysOfWeek.SetItemChecked(0, daysOfWeek.HasFlag(DaysOfWeekEnum.Monday));
            checkedListBoxDaysOfWeek.SetItemChecked(1, daysOfWeek.HasFlag(DaysOfWeekEnum.Tuesday));
            checkedListBoxDaysOfWeek.SetItemChecked(2, daysOfWeek.HasFlag(DaysOfWeekEnum.Wednesday));
            checkedListBoxDaysOfWeek.SetItemChecked(3, daysOfWeek.HasFlag(DaysOfWeekEnum.Thursday));
            checkedListBoxDaysOfWeek.SetItemChecked(4, daysOfWeek.HasFlag(DaysOfWeekEnum.Friday));
            checkedListBoxDaysOfWeek.SetItemChecked(5, daysOfWeek.HasFlag(DaysOfWeekEnum.Saturday));
            checkedListBoxDaysOfWeek.SetItemChecked(6, daysOfWeek.HasFlag(DaysOfWeekEnum.Sunday));
        }

        private void FillGroupListDaysOfWeek(DaysOfWeekEnum daysOfWeek)
        {
            switch (daysOfWeek)
            {
                case DaysOfWeekEnum.All:
                    radioButtonAllDays.Checked = true;
                    break;
                case DaysOfWeekEnum.Weekdays:
                    radioButtonWeekdays.Checked = true;
                    break;
                case DaysOfWeekEnum.Weekends:
                    radioButtonWeekends.Checked = true;
                    break;
                default:
                    radioButtonAllDays.Checked = false;
                    radioButtonWeekdays.Checked = false;
                    radioButtonWeekends.Checked = false;
                    break;
            }
        }

        private List<int> GetCheckedIndices(CheckedListBox checkedListBox)
        {
            var checkedIndices = new List<int>();
            foreach (int index in checkedListBox.CheckedIndices)
                checkedIndices.Add(index);
            return checkedIndices;
        }

        private DaysOfWeekEnum GetDaysOfWeek(List<int> checkedIndices = null)
        {
            checkedIndices = checkedIndices ?? GetCheckedIndices(checkedListBoxDaysOfWeek);
            var daysOfWeek = DaysOfWeekEnum.None;
            if (checkedIndices.Contains(0))
                daysOfWeek |= DaysOfWeekEnum.Monday;
            if (checkedIndices.Contains(1))
                daysOfWeek |= DaysOfWeekEnum.Tuesday;
            if (checkedIndices.Contains(2))
                daysOfWeek |= DaysOfWeekEnum.Wednesday;
            if (checkedIndices.Contains(3))
                daysOfWeek |= DaysOfWeekEnum.Thursday;
            if (checkedIndices.Contains(4))
                daysOfWeek |= DaysOfWeekEnum.Friday;
            if (checkedIndices.Contains(5))
                daysOfWeek |= DaysOfWeekEnum.Saturday;
            if (checkedIndices.Contains(6))
                daysOfWeek |= DaysOfWeekEnum.Sunday;
            return daysOfWeek;
        }

        private void textBoxDiscountInPercent_TextChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            ValidateTextBoxDiscountInPercent();
        }

        private void textBoxSumToApply_TextChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            ValidateTextBoxSumToApply();
        }

        private void pickerTimeEnd_ValueChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            ValidatePickerTimeEnd();
        }

        private void pickerTimeBegin_ValueChanged(object sender, EventArgs e)
        {
            if (!_isFormEnabled)
                return;
            ValidatePickerTimeEnd();
        }
        
        private void checkedListBoxDaysOfWeek_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (!_isFormEnabled)
                return;
            List<int> checkedIndices = GetCheckedIndices(checkedListBoxDaysOfWeek);
            if (e.NewValue == CheckState.Checked)
                checkedIndices.Add(e.Index);
            else
                checkedIndices.Remove(e.Index);

            DaysOfWeekEnum daysOfWeek = GetDaysOfWeek(checkedIndices);
            _isFormEnabled = false;
            FillGroupListDaysOfWeek(daysOfWeek);
            _isFormEnabled = true;
        }
    }
}
