﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.ControlsLib.BindFilterListControl;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Others.AmountFactories;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.AmountFactories;

namespace LaRenzo.Forms.DataForms.Controls
{
    public partial class SectionControl : UserControl
	{
		private Brand brand;

		public SectionControl(Brand brandLocal)
		{
			brand = brandLocal;

			InitializeComponent();
		}

		

		public void ChangeState(int state, int id = -1)
		{
			switch (state)
			{
				case 0:
					btnAdd.Enabled = true;
					btnDelete.Enabled = false;
					btnUpdate.Enabled = false;
					break;
				case 1:
					btnAdd.Enabled = false;
					btnDelete.Enabled = true;
					btnUpdate.Enabled = true;
					break;
			}

			if (id > 0)
			{
				var section = Content.SectionRepository.GetById(id);
				txtName.Text = section.Name;
				txtIdToEdit.Text = id.ToString(CultureInfo.InvariantCulture);
				cmbDoPrintCheck.Checked = section.DoPrintCheck;
				cmbAFPeople.SelectedValue = section.PeopleFactorCollectionID.HasValue ? section.PeopleFactorCollectionID.Value : 0;
				cmbAFEquipment.SelectedValue = section.EquipmentFactorCollectionID.HasValue ? section.EquipmentFactorCollectionID.Value : 0;
			}
			else
			{
				cmbAFEquipment.SelectedValue = cmbAFPeople.SelectedValue = 0;
				txtName.Text = txtIdToEdit.Text = string.Empty;
				cmbDoPrintCheck.Checked = false;
				bFilterListSections.Deselect();
			}


			var sections = Content.SectionRepository.GetList().Where(x=>x.IdBrand == brand.ID || x.IdBrand == 0);
			bFilterListSections.Bind(sections.Select(x => new BindFilterList.BindListRow(x.ID, x.Name)).ToList());

		}

		public Section ParseSection(bool forUpdate)
		{
			var section = new Section
			{
                IdBrand =  brand.ID,
				Name = txtName.Text,
				DoPrintCheck = cmbDoPrintCheck.Checked,
				PeopleFactorCollectionID = ((int)cmbAFPeople.SelectedValue == 0) ? (int?)null : (int)cmbAFPeople.SelectedValue,
				EquipmentFactorCollectionID = ((int)cmbAFEquipment.SelectedValue == 0) ? (int?)null : (int)cmbAFEquipment.SelectedValue
			};

			string errors = Content.SectionRepository.Validate(section, !forUpdate);
			if (errors.Length > 0)
			{
				MessageBox.Show(errors);
				return null;
			}

			return section;
		}


		//_______________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Добавить" </summary>
		////======================================
		private void BtnAddClick(object sender, EventArgs e)
		{
			// Получить выбранный цех
			Section section = ParseSection(false);

			if (section != null)
			{
			    Content.SectionRepository.Add(section);
				ChangeState(0);
			}
		}


		//_______________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Обновить" </summary>
		////======================================
		private void BtnUpdateClick(object sender, EventArgs e)
		{
			Section section = ParseSection(true);
			if (section != null)
			{
				section.ID = int.Parse(txtIdToEdit.Text);
			    Content.SectionRepository.Update(section);
				ChangeState(0);
			}
		}


		//_______________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Удалить" </summary>
		////=====================================
		private void BtnDeleteClick(object sender, EventArgs e)
		{
			try
			{
			    Content.SectionRepository.Remove(int.Parse(txtIdToEdit.Text));
				ChangeState(0);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}


		//__________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Сбросить" </summary>
		////======================================
		private void BtnDropClick(object sender, EventArgs e)
		{
			ChangeState(0);
		}

		

		private void bFilterListSections_SelectedRowChanged(object sender, SelectedRowChangedEventArgs e)
		{
			ChangeState(1, e.ID);
		}

		private void SectionControl_Load(object sender, EventArgs e)
		{
			var amountFactorCollections = Content.AmountFactorManager.GetList();

			amountFactorCollections.Add(new AmountFactorCollection { Name = string.Empty });

			cmbAFEquipment.DataSource = amountFactorCollections.OrderBy(x => x.Name).ToList();
			cmbAFEquipment.DisplayMember = "Name";
			cmbAFEquipment.ValueMember = "ID";

			cmbAFPeople.DataSource = amountFactorCollections.OrderBy(x => x.Name).ToList();
			cmbAFPeople.DisplayMember = "Name";
			cmbAFPeople.ValueMember = "ID";

			ChangeState(0);
		}

		
	}
}
