﻿namespace LaRenzo.Forms.DataForms.Controls
{
	partial class SectionControl
	{
		/// <summary> 
		/// Требуется переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором компонентов

		/// <summary> 
		/// Обязательный метод для поддержки конструктора - не изменяйте 
		/// содержимое данного метода при помощи редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnDrop = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.txtName = new System.Windows.Forms.TextBox();
			this.bFilterListSections = new LaRenzo.ControlsLib.BindFilterListControl.BindFilterList();
			this.cmbAFEquipment = new System.Windows.Forms.ComboBox();
			this.lblAFEquipment = new System.Windows.Forms.Label();
			this.cmbAFPeople = new System.Windows.Forms.ComboBox();
			this.cmbDoPrintCheck = new System.Windows.Forms.CheckBox();
			this.txtIdToEdit = new System.Windows.Forms.TextBox();
			this.btnUpdate = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
			this.lblAFPeople = new System.Windows.Forms.Label();
			this.lblSection = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
			this.splitContainerControl2.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnDrop
			// 
			this.btnDrop.Image = global::LaRenzo.Properties.Resources.cross;
			this.btnDrop.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnDrop.Location = new System.Drawing.Point(129, 31);
			this.btnDrop.Name = "btnDrop";
			this.btnDrop.Size = new System.Drawing.Size(84, 24);
			this.btnDrop.TabIndex = 6;
			this.btnDrop.Text = "Сбросить";
			this.btnDrop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnDrop.UseVisualStyleBackColor = true;
			this.btnDrop.Click += new System.EventHandler(this.BtnDropClick);
			// 
			// btnDelete
			// 
			this.btnDelete.Location = new System.Drawing.Point(14, 247);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(100, 23);
			this.btnDelete.TabIndex = 4;
			this.btnDelete.Text = "Удалить";
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Visible = false;
			this.btnDelete.Click += new System.EventHandler(this.BtnDeleteClick);
			// 
			// txtName
			// 
			this.txtName.Location = new System.Drawing.Point(14, 34);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(109, 20);
			this.txtName.TabIndex = 0;
			// 
			// bFilterListSections
			// 
			this.bFilterListSections.AutoSize = true;
			this.bFilterListSections.BackColor = System.Drawing.SystemColors.Control;
			this.bFilterListSections.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.bFilterListSections.Dock = System.Windows.Forms.DockStyle.Fill;
			this.bFilterListSections.Filter = "";
			this.bFilterListSections.Location = new System.Drawing.Point(0, 0);
			this.bFilterListSections.Name = "bFilterListSections";
			this.bFilterListSections.Size = new System.Drawing.Size(169, 419);
			this.bFilterListSections.TabIndex = 2;
			this.bFilterListSections.SelectedRowChanged += new System.EventHandler<LaRenzo.ControlsLib.BindFilterListControl.SelectedRowChangedEventArgs>(this.bFilterListSections_SelectedRowChanged);
			// 
			// cmbAFEquipment
			// 
			this.cmbAFEquipment.FormattingEnabled = true;
			this.cmbAFEquipment.Location = new System.Drawing.Point(14, 143);
			this.cmbAFEquipment.Name = "cmbAFEquipment";
			this.cmbAFEquipment.Size = new System.Drawing.Size(121, 21);
			this.cmbAFEquipment.TabIndex = 44;
			// 
			// lblAFEquipment
			// 
			this.lblAFEquipment.AutoSize = true;
			this.lblAFEquipment.Location = new System.Drawing.Point(11, 127);
			this.lblAFEquipment.Name = "lblAFEquipment";
			this.lblAFEquipment.Size = new System.Drawing.Size(145, 13);
			this.lblAFEquipment.TabIndex = 43;
			this.lblAFEquipment.Text = "Коэффицент оборудования";
			// 
			// cmbAFPeople
			// 
			this.cmbAFPeople.FormattingEnabled = true;
			this.cmbAFPeople.Location = new System.Drawing.Point(14, 102);
			this.cmbAFPeople.Name = "cmbAFPeople";
			this.cmbAFPeople.Size = new System.Drawing.Size(121, 21);
			this.cmbAFPeople.TabIndex = 42;
			// 
			// cmbDoPrintCheck
			// 
			this.cmbDoPrintCheck.AutoSize = true;
			this.cmbDoPrintCheck.Location = new System.Drawing.Point(14, 61);
			this.cmbDoPrintCheck.Name = "cmbDoPrintCheck";
			this.cmbDoPrintCheck.Size = new System.Drawing.Size(171, 17);
			this.cmbDoPrintCheck.TabIndex = 9;
			this.cmbDoPrintCheck.Text = "Печатать чек для этого цеха";
			this.cmbDoPrintCheck.UseVisualStyleBackColor = true;
			// 
			// txtIdToEdit
			// 
			this.txtIdToEdit.Location = new System.Drawing.Point(14, 170);
			this.txtIdToEdit.Name = "txtIdToEdit";
			this.txtIdToEdit.Size = new System.Drawing.Size(100, 20);
			this.txtIdToEdit.TabIndex = 5;
			this.txtIdToEdit.Visible = false;
			// 
			// btnUpdate
			// 
			this.btnUpdate.Location = new System.Drawing.Point(14, 216);
			this.btnUpdate.Name = "btnUpdate";
			this.btnUpdate.Size = new System.Drawing.Size(100, 23);
			this.btnUpdate.TabIndex = 3;
			this.btnUpdate.Text = "Сохранить";
			this.btnUpdate.UseVisualStyleBackColor = true;
			this.btnUpdate.Click += new System.EventHandler(this.BtnUpdateClick);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(14, 187);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(100, 23);
			this.btnAdd.TabIndex = 2;
			this.btnAdd.Text = "Добавить";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.BtnAddClick);
			// 
			// splitContainerControl2
			// 
			this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
			this.splitContainerControl2.Name = "splitContainerControl2";
			this.splitContainerControl2.Panel1.Controls.Add(this.bFilterListSections);
			this.splitContainerControl2.Panel1.Text = "Panel1";
			this.splitContainerControl2.Panel2.Controls.Add(this.cmbAFEquipment);
			this.splitContainerControl2.Panel2.Controls.Add(this.lblAFEquipment);
			this.splitContainerControl2.Panel2.Controls.Add(this.cmbAFPeople);
			this.splitContainerControl2.Panel2.Controls.Add(this.lblAFPeople);
			this.splitContainerControl2.Panel2.Controls.Add(this.cmbDoPrintCheck);
			this.splitContainerControl2.Panel2.Controls.Add(this.btnDrop);
			this.splitContainerControl2.Panel2.Controls.Add(this.txtIdToEdit);
			this.splitContainerControl2.Panel2.Controls.Add(this.btnDelete);
			this.splitContainerControl2.Panel2.Controls.Add(this.btnUpdate);
			this.splitContainerControl2.Panel2.Controls.Add(this.btnAdd);
			this.splitContainerControl2.Panel2.Controls.Add(this.lblSection);
			this.splitContainerControl2.Panel2.Controls.Add(this.txtName);
			this.splitContainerControl2.Panel2.Text = "Panel2";
			this.splitContainerControl2.Size = new System.Drawing.Size(371, 419);
			this.splitContainerControl2.SplitterPosition = 169;
			this.splitContainerControl2.TabIndex = 3;
			this.splitContainerControl2.Text = "splitContainerControl2";
			// 
			// lblAFPeople
			// 
			this.lblAFPeople.AutoSize = true;
			this.lblAFPeople.Location = new System.Drawing.Point(11, 86);
			this.lblAFPeople.Name = "lblAFPeople";
			this.lblAFPeople.Size = new System.Drawing.Size(106, 13);
			this.lblAFPeople.TabIndex = 41;
			this.lblAFPeople.Text = "Коэффицент людей";
			// 
			// lblSection
			// 
			this.lblSection.AutoSize = true;
			this.lblSection.Location = new System.Drawing.Point(11, 18);
			this.lblSection.Name = "lblSection";
			this.lblSection.Size = new System.Drawing.Size(83, 13);
			this.lblSection.TabIndex = 1;
			this.lblSection.Text = "Название цеха";
			// 
			// SectionControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.splitContainerControl2);
			this.Name = "SectionControl";
			this.Size = new System.Drawing.Size(371, 419);
			this.Load += new System.EventHandler(this.SectionControl_Load);
			((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
			this.splitContainerControl2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnDrop;
		public System.Windows.Forms.Button btnDelete;
		public System.Windows.Forms.TextBox txtName;
		private ControlsLib.BindFilterListControl.BindFilterList bFilterListSections;
		public System.Windows.Forms.ComboBox cmbAFEquipment;
		private System.Windows.Forms.Label lblAFEquipment;
		public System.Windows.Forms.ComboBox cmbAFPeople;
		private System.Windows.Forms.CheckBox cmbDoPrintCheck;
		public System.Windows.Forms.TextBox txtIdToEdit;
		public System.Windows.Forms.Button btnUpdate;
		public System.Windows.Forms.Button btnAdd;
		private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
		private System.Windows.Forms.Label lblAFPeople;
		private System.Windows.Forms.Label lblSection;
	}
}
