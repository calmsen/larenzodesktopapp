﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Sections;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Others.Times;
using LaRenzo.Forms.MainFormAndIM;

namespace LaRenzo.Forms
{
    public partial class SessionOptionsForm : Form
    {
        public delegate void OpenSessionSuccessHandler(object sender);

        public event OpenSessionSuccessHandler OpenSessionSuccess;

        //________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Открыть успешную сессию </summary>
        ////============================================
        public void OnOpenSessionSuccess()
        {
            OpenSessionSuccessHandler handler = OpenSessionSuccess;
            if (handler != null)
            {
                handler(this);
            }
        }

        private bool IsNew { get; set; }

        private Session Session { get; set; }

        public SessionOptionsForm(bool isNew = true)
        {
            InitializeComponent();
            IsNew = isNew;
            if (isNew)
            {
                tEditSession.Time = Content.TimeSheetManager.GetNormalizedDateTime(new TimeSpan(10, 0, 0));
                btnAccept.Enabled = false;
                btnCancel.Enabled = false;
            }
            else
            {
                tEditSession.Enabled = false;
                btnOpenSessoion.Enabled = false;
                btnAccept.Enabled = true;
                btnCancel.Enabled = true;
            }
            btnOpenSessoion.Click += async (s, e) => await BtnOpenSessoionClick();
            KeyDown += async (s, e) => await SessionOptionsForm_KeyDown(e);
        }

        public async Task Initialization()
        {
            if (!IsNew)
            {
                Session = await Content.SessionManager.GetOpened();
                if (Session != null)
                {
                    tEditDelta.Time = tEditDelta.Time.Add(Session.Delta);
                    tEditDeltaWindow.Time = tEditDeltaWindow.Time.Add(Session.DeltaWindow);
                    cmbIsVacationDelivery.Checked = Session.IsVacationDelivery;
                    chkCanEditTime.Checked = Session.CanEditTime;
                }
            }
        }


        //____________________________________________________________________________________________________________________________________
        /// <summary> Кнопка "Открыть сессию" </summary>
        ////============================================
        private async Task BtnOpenSessoionClick()
        {
            await Process();
        }


        //____________________________________________________________________________________________________________________________________
        /// <summary> Обработать открытие сессии </summary>
        ////===============================================
        private async Task Process()
        {
            var deltaWindow = new TimeSpan(tEditDeltaWindow.Time.Hour, tEditDeltaWindow.Time.Minute, tEditDeltaWindow.Time.Second);
            var delta = new TimeSpan(tEditDelta.Time.Hour, tEditDelta.Time.Minute, tEditDelta.Time.Second);
 
            await Content.SessionManager.Open(tEditSession.Time.TimeOfDay, cmbIsVacationDelivery.Checked, delta, deltaWindow, chkCanEditTime.Checked);
            SavePointOfSales();
            OnOpenSessionSuccess();

            Close();
        }

        private void OpenSessionForm_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void SessionOptionsForm_Load(object sender, EventArgs e)
        {
            FillPointOfSales();
            mainGridControl.DataSource = Content.SectionRepository.GetList();
        }

        private void FillPointOfSales()
        {
            var pointOfSales = Content.PointOfSaleRepository.GetItems();

            chlbPointOfSales.DisplayMember = "PointName";
            for (int i = 0; i < pointOfSales.Count; i++)
            {
                chlbPointOfSales.Items.Add(pointOfSales[i]);
                if (pointOfSales[i].Active)
                {
                    chlbPointOfSales.SetItemChecked(i, true);
                }
            }
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            var obj = e.Row as Section;
            Content.SectionRepository.Update(obj);
        }

        private async Task SessionOptionsForm_KeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                await Process();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            var deltaWindow = new TimeSpan(tEditDeltaWindow.Time.Hour, tEditDeltaWindow.Time.Minute,
                                           tEditDeltaWindow.Time.Second);
            var delta = new TimeSpan(tEditDelta.Time.Hour, tEditDelta.Time.Minute, tEditDelta.Time.Second);
            Content.SessionManager.UpdateSessionState(delta, deltaWindow, cmbIsVacationDelivery.Checked, chkCanEditTime.Checked);
            SavePointOfSales();
            Close();
        }

        private void SavePointOfSales()
        {
            var pointOfSales = Content.PointOfSaleRepository.GetItems();
            var checkedPointOfSales = chlbPointOfSales.CheckedItems.OfType<PointOfSale>().Select(x => x.ID).ToList();
            foreach (var pointOfSale in pointOfSales)
            {
                pointOfSale.Active = checkedPointOfSales.Contains(pointOfSale.ID);
            }
            Content.PointOfSaleRepository.UpdateItems(pointOfSales);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tEditDelta_EditValueChanged(object sender, EventArgs e)
        {
            var inputCodeForm = new InputCodeForm();
            inputCodeForm.ShowDialog();

            // Если окну "понравился" код, показать секретное меню
            if (inputCodeForm.DialogResult != DialogResult.OK)
            {
                tEditDelta.Time = new DateTime().Add(Session.Delta);
            }
        }
    }
}