﻿using LaRenzo.ControlsLib.BindFilterListControl;

namespace LaRenzo.Forms.AmountFactorForms
{
    partial class AmountFactorCollectionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bFilterCollections = new LaRenzo.ControlsLib.BindFilterListControl.BindFilterList();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.AddCollectionMenuItem = new System.Windows.Forms.ToolStripButton();
            this.RemoveCollectionMenuItem = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.mainToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // bFilterCollections
            // 
            this.bFilterCollections.AutoSize = true;
            this.bFilterCollections.BackColor = System.Drawing.SystemColors.Control;
            this.bFilterCollections.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bFilterCollections.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bFilterCollections.Filter = "";
            this.bFilterCollections.Location = new System.Drawing.Point(0, 0);
            this.bFilterCollections.Name = "bFilterCollections";
            this.bFilterCollections.Size = new System.Drawing.Size(181, 318);
            this.bFilterCollections.TabIndex = 0;
            this.bFilterCollections.SelectedRowChanged += new System.EventHandler<LaRenzo.ControlsLib.BindFilterListControl.SelectedRowChangedEventArgs>(this.flCollections_SelectedRowChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 31);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.bFilterCollections);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.mainGridControl);
            this.splitContainer1.Size = new System.Drawing.Size(483, 318);
            this.splitContainer1.SplitterDistance = 181;
            this.splitContainer1.TabIndex = 1;
            // 
            // mainGridControl
            // 
            this.mainGridControl.ContextMenuStrip = this.contextMenuStrip1;
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(298, 318);
            this.mainGridControl.TabIndex = 0;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалитьToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(119, 26);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnAmount,
            this.columnFactor});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.mainGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.mainGridView.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.mainGridView.OptionsView.ShowGroupPanel = false;
            // 
            // columnAmount
            // 
            this.columnAmount.Caption = "Количество";
            this.columnAmount.DisplayFormat.FormatString = "{0:0.0}";
            this.columnAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnAmount.FieldName = "Amount";
            this.columnAmount.Name = "columnAmount";
            this.columnAmount.Visible = true;
            this.columnAmount.VisibleIndex = 0;
            // 
            // columnFactor
            // 
            this.columnFactor.Caption = "Коэффицент";
            this.columnFactor.FieldName = "Factor";
            this.columnFactor.Name = "columnFactor";
            this.columnFactor.Visible = true;
            this.columnFactor.VisibleIndex = 1;
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddCollectionMenuItem,
            this.RemoveCollectionMenuItem});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(483, 31);
            this.mainToolStrip.TabIndex = 2;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // AddCollectionMenuItem
            // 
            this.AddCollectionMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AddCollectionMenuItem.Image = global::LaRenzo.Properties.Resources.plus;
            this.AddCollectionMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddCollectionMenuItem.Name = "AddCollectionMenuItem";
            this.AddCollectionMenuItem.Size = new System.Drawing.Size(28, 28);
            this.AddCollectionMenuItem.Text = "Добавить";
            this.AddCollectionMenuItem.Click += new System.EventHandler(this.toolStripButtonAddCollection_Click);
            // 
            // RemoveCollectionMenuItem
            // 
            this.RemoveCollectionMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RemoveCollectionMenuItem.Image = global::LaRenzo.Properties.Resources.cross_48;
            this.RemoveCollectionMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RemoveCollectionMenuItem.Name = "RemoveCollectionMenuItem";
            this.RemoveCollectionMenuItem.Size = new System.Drawing.Size(28, 28);
            this.RemoveCollectionMenuItem.Text = "Удалить";
            this.RemoveCollectionMenuItem.Click += new System.EventHandler(this.toolStripButtonRemoveCollection_Click);
            // 
            // AmountFactorCollectionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 349);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.mainToolStrip);
            this.Name = "AmountFactorCollectionsForm";
            this.Text = "Управление коэффицентами";
            this.Load += new System.EventHandler(this.AmountFactorCollectionsForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BindFilterList bFilterCollections;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton AddCollectionMenuItem;
        private System.Windows.Forms.ToolStripButton RemoveCollectionMenuItem;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnFactor;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
    }
}