﻿namespace LaRenzo.Forms.BlackList
{
    partial class BlackListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnStreet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnHouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAppartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGridControl
            // 
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 31);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(884, 408);
            this.mainGridControl.TabIndex = 3;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnID,
            this.columnName,
            this.columnPhone,
            this.columnStreet,
            this.columnHouse,
            this.columnAppartment,
            this.columnReason,
            this.columnCreated});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrdersCostDiffCol", null, "")});
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsView.ShowAutoFilterRow = true;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.DoubleClick += new System.EventHandler(this.MainGridViewDoubleClick);
            // 
            // columnID
            // 
            this.columnID.Caption = "Ид";
            this.columnID.FieldName = "ID";
            this.columnID.Name = "columnID";
            this.columnID.OptionsColumn.AllowEdit = false;
            this.columnID.OptionsColumn.ReadOnly = true;
            // 
            // columnName
            // 
            this.columnName.Caption = "Имя";
            this.columnName.FieldName = "Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 0;
            // 
            // columnPhone
            // 
            this.columnPhone.Caption = "Телефон";
            this.columnPhone.FieldName = "Phone";
            this.columnPhone.Name = "columnPhone";
            this.columnPhone.OptionsColumn.AllowEdit = false;
            this.columnPhone.OptionsColumn.ReadOnly = true;
            this.columnPhone.Visible = true;
            this.columnPhone.VisibleIndex = 1;
            // 
            // columnStreet
            // 
            this.columnStreet.Caption = "Улица";
            this.columnStreet.FieldName = "Street";
            this.columnStreet.Name = "columnStreet";
            this.columnStreet.OptionsColumn.AllowEdit = false;
            this.columnStreet.OptionsColumn.ReadOnly = true;
            this.columnStreet.Visible = true;
            this.columnStreet.VisibleIndex = 2;
            // 
            // columnHouse
            // 
            this.columnHouse.Caption = "Дом";
            this.columnHouse.FieldName = "House";
            this.columnHouse.Name = "columnHouse";
            this.columnHouse.OptionsColumn.AllowEdit = false;
            this.columnHouse.OptionsColumn.ReadOnly = true;
            this.columnHouse.Visible = true;
            this.columnHouse.VisibleIndex = 3;
            // 
            // columnAppartment
            // 
            this.columnAppartment.Caption = "Квартира";
            this.columnAppartment.FieldName = "Appartment";
            this.columnAppartment.Name = "columnAppartment";
            this.columnAppartment.OptionsColumn.AllowEdit = false;
            this.columnAppartment.OptionsColumn.ReadOnly = true;
            this.columnAppartment.Visible = true;
            this.columnAppartment.VisibleIndex = 4;
            // 
            // columnReason
            // 
            this.columnReason.Caption = "Причина добавления";
            this.columnReason.FieldName = "Reason";
            this.columnReason.Name = "columnReason";
            this.columnReason.OptionsColumn.AllowEdit = false;
            this.columnReason.OptionsColumn.ReadOnly = true;
            this.columnReason.Visible = true;
            this.columnReason.VisibleIndex = 5;
            // 
            // columnCreated
            // 
            this.columnCreated.Caption = "Дата добавления";
            this.columnCreated.FieldName = "Created";
            this.columnCreated.Name = "columnCreated";
            this.columnCreated.OptionsColumn.AllowEdit = false;
            this.columnCreated.OptionsColumn.ReadOnly = true;
            this.columnCreated.Visible = true;
            this.columnCreated.VisibleIndex = 6;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.btnRefresh});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(884, 31);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::LaRenzo.Properties.Resources.plus;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(28, 28);
            this.btnAdd.Text = "Добавить карту";
            this.btnAdd.Click += new System.EventHandler(this.BtnAddClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 28);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.BtnRefreshClick);
            // 
            // BlackListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 439);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.toolStrip1);
            this.Name = "BlackListForm";
            this.Text = "Чёрный список";
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnID;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPhone;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn columnStreet;
        private DevExpress.XtraGrid.Columns.GridColumn columnHouse;
        private DevExpress.XtraGrid.Columns.GridColumn columnAppartment;
        private DevExpress.XtraGrid.Columns.GridColumn columnReason;
        private DevExpress.XtraGrid.Columns.GridColumn columnCreated;
    }
}