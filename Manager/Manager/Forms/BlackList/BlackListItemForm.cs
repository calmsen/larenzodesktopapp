﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.BlackList;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.BlackList;
using LaRenzo.FormsProvider;

namespace LaRenzo.Forms.BlackList
{
    public partial class BlackListItemForm : Form
    {
        /// <summary> Редактируемый ли элемент </summary>
		private readonly bool isEditingForm;

	    private int itemId = 0;

		//___________________________________________________________________________________________________________________________________________________________________________________
		public BlackListItemForm()
        {
            InitializeComponent();
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		public BlackListItemForm(BlackListItem blackListItem, bool isEdit = false): this()
        {
            isEditingForm = isEdit;

			itemId = blackListItem.ID;

			WindowsFormProvider.FillControlsFromModel(this, blackListItem);

            btnSave.Text = isEditingForm ? "Сохранить" : "Добавить";
            btnDelete.Visible = isEditingForm;
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить элемент из интерфейса </summary>
		////===================================================
		private BlackListItem GetEntityProperies()
        {
			var blackItem = new BlackListItem
			{
				Name = textName.Text, 
				Street = textStreet.Text, 
				House = textHouse.Text, 
				Appartment = textAppartment.Text, 
				Phone = textPhone.Text, 
				Reason = textReason.Text,
				ID = itemId,
                BranchId = Content.BranchRepository.SelectedBranchId
			};

			return blackItem;
        }

		
		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Сохранить" </summary>
		////=======================================
        private void BtnSaveClick(object sender, EventArgs e)
        {
	        BlackListItem blackItem = GetEntityProperies();

	        if (isEditingForm)
	        {
                Content.BlackListManager.Update(blackItem);
	        }
	        else
	        {
                Content.BlackListManager.Add(blackItem);
	        }
            
            Close();
        }


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Удалить" </summary>
		////=======================================
		private void BtnDeleteClick(object sender, EventArgs e)
        {
			BlackListItem blackItem = GetEntityProperies();
            Content.BlackListManager.Remove(blackItem.ID);

            Close();
        }
    }
}
