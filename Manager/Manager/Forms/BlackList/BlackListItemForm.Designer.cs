﻿namespace LaRenzo.Forms.BlackList
{
    partial class BlackListItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.textName = new System.Windows.Forms.TextBox();
			this.textHouse = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textStreet = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textPhone = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.textAppartment = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.textReason = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(29, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Имя";
			// 
			// textName
			// 
			this.textName.Location = new System.Drawing.Point(12, 29);
			this.textName.Name = "textName";
			this.textName.Size = new System.Drawing.Size(168, 20);
			this.textName.TabIndex = 1;
			this.textName.Tag = "Name";
			this.textName.Text = "-";
			// 
			// textHouse
			// 
			this.textHouse.Location = new System.Drawing.Point(12, 148);
			this.textHouse.Name = "textHouse";
			this.textHouse.Size = new System.Drawing.Size(168, 20);
			this.textHouse.TabIndex = 4;
			this.textHouse.Tag = "House";
			this.textHouse.Text = "-";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 132);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(30, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Дом";
			// 
			// textStreet
			// 
			this.textStreet.Location = new System.Drawing.Point(12, 108);
			this.textStreet.Name = "textStreet";
			this.textStreet.Size = new System.Drawing.Size(168, 20);
			this.textStreet.TabIndex = 3;
			this.textStreet.Tag = "Street";
			this.textStreet.Text = "-";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 92);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(39, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Улица";
			// 
			// textPhone
			// 
			this.textPhone.Location = new System.Drawing.Point(12, 67);
			this.textPhone.Name = "textPhone";
			this.textPhone.Size = new System.Drawing.Size(168, 20);
			this.textPhone.TabIndex = 2;
			this.textPhone.Tag = "Phone";
			this.textPhone.Text = "-";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 51);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 13);
			this.label4.TabIndex = 6;
			this.label4.Text = "Телефон";
			// 
			// textAppartment
			// 
			this.textAppartment.Location = new System.Drawing.Point(12, 189);
			this.textAppartment.Name = "textAppartment";
			this.textAppartment.Size = new System.Drawing.Size(168, 20);
			this.textAppartment.TabIndex = 5;
			this.textAppartment.Tag = "Appartment";
			this.textAppartment.Text = "-";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 173);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(55, 13);
			this.label5.TabIndex = 8;
			this.label5.Text = "Квартира";
			// 
			// textReason
			// 
			this.textReason.Location = new System.Drawing.Point(12, 233);
			this.textReason.Name = "textReason";
			this.textReason.Size = new System.Drawing.Size(168, 20);
			this.textReason.TabIndex = 6;
			this.textReason.Tag = "Reason";
			this.textReason.Text = "-";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 217);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(113, 13);
			this.label6.TabIndex = 10;
			this.label6.Text = "Причина добавления";
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(12, 260);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(168, 42);
			this.btnSave.TabIndex = 99;
			this.btnSave.Text = "Добавить";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
			// 
			// btnDelete
			// 
			this.btnDelete.BackColor = System.Drawing.Color.Salmon;
			this.btnDelete.Location = new System.Drawing.Point(12, 307);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(168, 31);
			this.btnDelete.TabIndex = 100;
			this.btnDelete.Text = "Удалить";
			this.btnDelete.UseVisualStyleBackColor = false;
			this.btnDelete.Click += new System.EventHandler(this.BtnDeleteClick);
			// 
			// BlackListItemForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(198, 348);
			this.Controls.Add(this.btnDelete);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.textReason);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.textAppartment);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.textPhone);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.textStreet);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.textHouse);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textName);
			this.Controls.Add(this.label1);
			this.Name = "BlackListItemForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Чёрный список";
			this.TopMost = true;
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.TextBox textHouse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textStreet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textPhone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textAppartment;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textReason;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
    }
}