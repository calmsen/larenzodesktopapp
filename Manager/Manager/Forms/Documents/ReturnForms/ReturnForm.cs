﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Entities.Catalogs.Products;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Entities.Documents.Returns;
using LaRenzo.DataRepository.Entities.Wirings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Partners;
using LaRenzo.DataRepository.Repositories.Catalogs.Products;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Documents.Returns;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;
using LaRenzo.Forms.Catalog.PartnersForms;
using LaRenzo.Forms.Catalog.WarehouseForms;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.ReturnForms
{
    public partial class ReturnForm : Form
    {
        private bool _isEditWindow;

        private string _userName;

        private bool DocumentIsProcessed { set; get; }

        private readonly List<ReturnDocumentItem> _documentItems;

        public ReturnForm()
        {
            InitializeComponent();
            _documentItems = new List<ReturnDocumentItem>();
            lstProduct.DisplayMember = "Name";

            var partners = Content.PartnerManager.GetPartners();
            cmbPartner.DataSource = partners;
            cmbPartner.DisplayMember = "PartnerShortName";
            cmbPartner.ValueMember = "ID";

            var warehouses = Content.WarehouseManager.GetWarehouses();
            cmbWarehouse.DataSource = warehouses;
            cmbWarehouse.DisplayMember = "Name";
            cmbWarehouse.ValueMember = "ID";

            var defaultWarehouse =
                Content.WarehouseManager.GetWarehouses().FirstOrDefault(x => x.IsBase);
            if (defaultWarehouse != null)
                cmbWarehouse.SelectedValue = defaultWarehouse.ID;
        }

        public ReturnForm(bool isEdit, ReturnDocument document):this()
        {
            _isEditWindow = isEdit;

            _userName = document.User;

            WindowsFormProvider.FillControlsFromModel(this, document);

            DocumentIsProcessed = document.IsProcessed;

            dtnDocDate.Value = document.DocumentDate;

            if (!string.IsNullOrEmpty(txtNumber.Text))
                _documentItems = Content.ReturnManager.GetReturnDocumentItems(document.ID);

            mainGridControl.DataSource = _documentItems;
        }

        private void FillCategoties()
        {
            treeListCategories.DataSource = Content.ProductManager.GetCategoriesList();

            var nodeAll = treeListCategories.AppendNode(null, null);
            nodeAll.SetValue(NameCol, "Все");
            nodeAll.SetValue(IdCol, 0);
            treeListCategories.SetNodeIndex(nodeAll, 0);
            treeListCategories.FocusedNode = nodeAll;
        }

        private void ReturnForm_Load(object sender, EventArgs e)
        {
            FillCategoties();

            if (DocumentIsProcessed)
            {
                btnProcess.Text = Resources.Unprocess;
                btnProcess.ForeColor = Color.DarkRed;
            }
            else
            {
                btnProcess.Text = Resources.Process;
                btnProcess.ForeColor = Color.Green;
            }
        }

        private void treeListCategories_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            if (e.Node != null)
            {
                var id = (int)e.Node.GetValue(IdCol);
                FillProducts(id);
            }
        }

        private void FillProducts(int catId)
        {
            var products = Content.ProductManager.GetAllProductsInCategory(catId).OrderBy(x => x.Name).ToList();
            if (products.Count != 0)
            {
                lstProduct.DataSource = products;
            }
        }

        private void btnSelectParent_Click(object sender, EventArgs e)
        {
            var selectPartnerForm = new SelectPartnerForm();

            if (selectPartnerForm.ShowDialog() == DialogResult.OK)
            {
                cmbPartner.SelectedValue = selectPartnerForm.Partner.ID;
            }
        }

        private void btnSelectWarehouse_Click(object sender, EventArgs e)
        {
            var selectWarehouseForm = new SelectWarehousezForm();

            if (selectWarehouseForm.ShowDialog() == DialogResult.OK)
            {
                cmbWarehouse.SelectedValue = selectWarehouseForm.Warehouse.ID;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lstProduct_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mainGridView.BeginDataUpdate();
            var product = (Product)lstProduct.SelectedItem;

            if (product == null)
                return;

            _documentItems.Add(new ReturnDocumentItem
            {
                ID = -1,
                Product = product,
                ProductID = product.ID,
                Amount = 0,
                Price = 0,
                Sum = 0
            });

            mainGridControl.DataSource = _documentItems;
            mainGridView.EndDataUpdate();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
			if (IsCanSave)
			{
				SaveDoc();
			}
			else
			{
				MessageBox.Show(@"У вас нет прав");
			}
        }


		//____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Можно ли сохранить документ </summary>
		///=================================================
	    private bool IsCanSave
		{
			get
			{
				bool isCanSave = true;

				if (_isEditWindow && Content.UserManager.UserLogged.Role.Name != "Admin")
				{
					isCanSave = false;
				}

				return isCanSave;
			}
		}


		//____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Сохранить документ </summary>
		////=======================================
	    private void SaveDoc()
	    {
			var documentSum = _documentItems.Sum(x => x.Sum);
			
			var returnDocument = new ReturnDocument
			{
				ID = string.IsNullOrEmpty(txtNumber.Text) ? 0 : Convert.ToInt32(txtNumber.Text.Trim()),
				DocumentDate = dtnDocDate.Value,
				PartnerID = ((Partner)cmbPartner.SelectedItem).ID,
				WarehouseID = ((Warehouse)cmbWarehouse.SelectedItem).ID,
				IsProcessed = DocumentIsProcessed,
				DocumentSum = documentSum,
				Note = richOtherInfo.Text
			};

			if (_isEditWindow)
			{
				returnDocument.User = _userName;
                Content.ReturnManager.UpdateReturnDocument(new ReturnData
                {
                    Document = returnDocument,
                    DocumentItems = _documentItems
                });

				if (DocumentIsProcessed)
				{
					UnProcessDocument();
					ProcessDocument();
				}
			}
			else
			{
				returnDocument.User = _userName = Content.UserManager.GetUserNameOrEmptyString();
				int docId = Content.ReturnManager.AddReturnDocument(returnDocument);
                Content.ReturnManager.UpdateReturnDocument(new ReturnData { 
                    Document = returnDocument,
                    DocumentItems = _documentItems
                });
				txtNumber.Text = docId.ToString(CultureInfo.InvariantCulture);
				_isEditWindow = true;
			}
	    }

	    private void mainGridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.Name == "columnAmmount" || e.Column.Name == "columnPrice")
            {
                var view = (GridView)sender;
                var currentRow = (ReturnDocumentItem)view.GetRow(e.RowHandle);

                currentRow.Price = Math.Round(currentRow.Price, 3);
                currentRow.Amount = Math.Round(currentRow.Amount, 3);

                currentRow.Sum = Math.Round(currentRow.Price *  Convert.ToDouble(currentRow.Amount), 3);
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            btnSave_Click(sender, e);

	        if (IsCanSave)
	        {
		        if (DocumentIsProcessed)
		        {
			        UnProcessDocument();
		        }
		        else
		        {
			        ProcessDocument();
		        }
	        }
	        else
	        {
		        MessageBox.Show(@"У вас нет прав");
	        }
        }

        private void UnProcessDocument()
        {
            Content.RemainsOfGoodWiringsManager.UnProcessReturnDocument(Convert.ToInt32(txtNumber.Text));

            DocumentIsProcessed = false;

            btnProcess.Text = Resources.Process;
            btnProcess.ForeColor = Color.Green;
        }

        private void ProcessDocument()
        {
            if (string.IsNullOrEmpty(txtNumber.Text))
            {
                MessageBox.Show(Resources.DocIsNotProcessed, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            var wiringses = _documentItems.Select(documentItem => new RemainsOfGoodWirings
            {
                Amount = -documentItem.Amount,
                Date = dtnDocDate.Value,
                WarehouseID = ((Warehouse)cmbWarehouse.SelectedItem).ID,
                ProductID = documentItem.ProductID,
                ReturnDocumentID = Convert.ToInt32(txtNumber.Text) 
            }).ToList();

            Content.RemainsOfGoodWiringsManager.ProcessReturnDocument(Convert.ToInt32(txtNumber.Text), wiringses);

            DocumentIsProcessed = true;

            btnProcess.Text = Resources.Unprocess;
            btnProcess.ForeColor = Color.DarkRed;
        }

        private void mainGridControl_DoubleClick(object sender, EventArgs e)
        {
            mainGridView.BeginDataUpdate();
            var item = (ReturnDocumentItem)mainGridView.GetRow(mainGridView.FocusedRowHandle);

            if (item == null)
                return;

            _documentItems.Remove(item);

            mainGridControl.DataSource = _documentItems;

            mainGridView.EndDataUpdate();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            var brick =
                e.Graph.DrawString(
                    string.Format("Поставщик: {0}\nСклад: {1}", cmbPartner.Text, cmbWarehouse.Text), Color.Navy,
                    new RectangleF(0, 0, 560, 50), BorderSide.None);

            brick.Font = new Font("Arial", 12);
            brick.HorzAlignment = HorzAlignment.Near;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }

        private void printableComponentLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            var brick =
                e.Graph.DrawString(
                    string.Format("Возврат товаров поставщику №{0} от {1:dd.MM.yyyy}.",
                                  txtNumber.Text, dtnDocDate.Value), Color.Navy,
                    new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 16);
            brick.HorzAlignment = HorzAlignment.Near;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }
    }
}