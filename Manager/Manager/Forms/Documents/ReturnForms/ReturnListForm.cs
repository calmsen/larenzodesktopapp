﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.Returns;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Returns;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.ReturnForms
{
    public partial class ReturnListForm : Form
    {
        public ReturnListForm()
        {
            InitializeComponent();
            LoadReturnList(new object(), new EventArgs());
        }

        private void LoadReturnList(object sender, EventArgs args)
        {
            mainGridControl.DataSource = Content.ReturnManager.GetReturnDocuments();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var returnForm = new ReturnForm();
            returnForm.FormClosed += UpdateDataGrid;
            returnForm.Show();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateDataGrid(sender, e);
        }

        private void UpdateDataGrid(object sender, EventArgs e)
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            LoadReturnList(sender, e);
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (ReturnDocument) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetReturnForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.ReturnDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void GetReturnForm(bool isEdit, int returnId)
        {
            var returnDocument = Content.ReturnManager.GetReturnDocument(returnId);

            var returnForm = new ReturnForm(isEdit, returnDocument);

            returnForm.FormClosed += UpdateDataGrid;
            returnForm.Show();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (ReturnDocument) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetReturnForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.ReturnDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var selectedRow = (ReturnDocument) mainGridView.GetFocusedRow();

            if (selectedRow == null)
            {
                MessageBox.Show(Resources.ReturnDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string msg = selectedRow.IsMarkToDelete ? Resources.UnMarkDeleteDocument : Resources.MarkDeleteDocument;

            if (MessageBox.Show(msg, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Content.ReturnManager.DeleteReturnDocument(selectedRow);
                UpdateDataGrid(sender, e);
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            var info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var supplyId = (int) view.GetRowCellValue(info.RowHandle, columnNumber);
                GetReturnForm(true, supplyId);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (ReturnDocument)view.GetRow(e.RowHandle);

            if (currentRow.IsMarkToDelete)
                e.Appearance.BackColor = Color.Salmon;

            if (currentRow.IsProcessed)
                e.Appearance.BackColor = Color.LightGreen;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}