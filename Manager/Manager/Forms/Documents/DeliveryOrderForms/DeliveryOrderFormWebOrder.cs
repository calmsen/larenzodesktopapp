﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.WebOrders;
using LaRenzo.DataRepository.Entities.Settings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Call;
using LaRenzo.DataRepository.Repositories.Catalogs.Dishes;
using LaRenzo.DataRepository.Repositories.Documents.WebOrders;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.DeliveryOrderForms
{
    public partial class DeliveryOrderFormWebOrder : DeliveryOrderForm
    {
        public DeliveryOrderFormWebOrder(WebOrder webOrder, DetailsFormModes mode, Brand brand):base(mode, 0)
        {
	        CurentBrand = brand;

	        if (webOrder == null)
	        {
		        throw new ArgumentNullException();
	        }
            webOrder.BrandId = brand.ID;
	        if (mode != DetailsFormModes.NewOrder && mode != DetailsFormModes.ViewWebOrder)
	        {
		        throw new ArgumentException("Данный режим просмотра не поддерживается");
	        }
			
			InitializeComponent();
            WebOrder = webOrder;
			

            WebOrderGuid = webOrder.Guid;
            PaymentMethod = webOrder.PaymentMethod;

            richWebOrder.Text = WebOrder.ToString();

	        if (webOrder.DeliveryMethod == 1)
	        {
		        chkIsWindow.Checked = true;
                chkNeedDelivery.Checked = false;

            }

            if (mode == DetailsFormModes.NewOrder)
            {
                Content.WebOrderManager.StartProcessing(WebOrder);
                FormClosing += (sender, args) => Content.WebOrderManager.StopProcessing(WebOrder);
                OrderProcessed += async (state, order) => await Content.WebOrderManager.SetOrderProcessed(WebOrder, CurentBrand.ID, Settings.Default.SiteProtocol);
            }

            if (mode == DetailsFormModes.ViewWebOrder)
            {
                FormOptions = new DeliveryOrderFormOptions
                {
                    SaveButtonText = "Этот онлайн заказ еще не оплачен." + Environment.NewLine + "Подождите или смените тип оплаты",
                    DoBlockSaveButton = true,
                    DoBlockReservationArea = true
                };

                UpdateFormOptions();
            }
        }

        public async Task Initialize()
        {
            await base.Initialize();
            // Заполнить поля заказа из web заказа
            await FillFormFromWebOrder();
        }


		//___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Заполнить поля заказа из web заказа </summary>
		////========================================================
        private async Task FillFormFromWebOrder()
        {
            foreach (var item in WebOrder.Items)
            {
                // Найти по SKU
				var dish = Content.DishRepository.GetItem(new DishFilter { FilterBySKU = true, SKU = item.SKU, BrandId = CurentBrand.ID });
	            
				// Если не распозналось
				if (dish == null)
	            {
		            richWebOrder.Text += string.Format("{0}{0}Блюдо с артикулом {1} [{2}(id={3})] не найдено в базе", Environment.NewLine, item.SKU, item.Name, item.ID);
	            }
	            else
	            {
		            var orderItem = new OrderItem
		            {
			            Amount = item.Amount,
			            DishID = dish.ID,
						Dish = dish,
                        BranchId = item.BranchId
		            };

		            OrderItems.Add(orderItem);

					if (orderItem.Dish != null && orderItem.Dish.IsDeleted)
		            {
						MessageBox.Show("Блюдо " + orderItem.Dish.Name + " значится удалённым");
		            }
	            }
            }

            textEditFirstName.Text = WebOrder.Name;
           
            var phone = new string(WebOrder.Phone.Where(Char.IsDigit).ToArray());
			
			if (phone.StartsWith("8") || phone.StartsWith("7"))
			{
				phone = phone.Remove(0, 1);
			}

			if (phone.StartsWith("+7"))
			{
				phone = phone.Remove(0, 2);
			}

            if (!String.IsNullOrEmpty(WebOrder.DiscountCard))
            {
                numCardNumber.Value = Convert.ToDecimal(WebOrder.DiscountCard);
            }
            txtAdditional.Text = WebOrder.Comment;
            textBoxWebOrderId.Text = WebOrder.WebOrderId.ToString();
            string clearPhone = phone.Replace(" ", "").Replace("-", "");
			CallInfo ci = await CallManager.Instance.GetCallInfoByPhone("8" + clearPhone);
			await SetFildsByCallInfo(ci, true);

            txtPhone.Text = phone;

            //применим купон если надо. Вызывается перед тем как установливается событие textBoxCouponCode.TextChanged в base.Initialize                        
            ApplyCouponIfNeed();

            await ManualUpdateOnGrid(CurentBrand.ID);
            
			SetEnableAllEdit();
        }

        private void ApplyCouponIfNeed()
        {
            if (string.IsNullOrEmpty(WebOrder.CouponCode))
                return;
            SetTextBoxCouponCode(WebOrder.CouponCode);
        }
    }
}
