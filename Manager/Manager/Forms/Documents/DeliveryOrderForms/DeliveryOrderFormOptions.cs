﻿namespace LaRenzo.Forms.Documents.DeliveryOrderForms
{
    /// <summary>
    /// Этот класс хранит различные настройки для формы доставки пиццы
    /// </summary>
    public class DeliveryOrderFormOptions
    {
        public bool DoBlockDishArea { get; set; }
        public bool DoBlockReservationArea { get; set; }
        public bool DoBlockSaveButton { get; set; }
        public string SaveButtonText { get; set; }

        public DeliveryOrderFormOptions()
        {
            SaveButtonText = "Кнопка заблокирована"; 
        }
    }
}
