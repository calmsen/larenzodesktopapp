﻿namespace LaRenzo.Forms.Documents.DeliveryOrderForms
{
    partial class DeliveryOrderForm
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeliveryOrderForm));
            this.mainSplit = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.lstCategory = new DevExpress.XtraEditors.ListBoxControl();
            this.offPanel = new System.Windows.Forms.Panel();
            this.onPanel = new System.Windows.Forms.Panel();
            this.lstDishListIDs = new DevExpress.XtraEditors.ListBoxControl();
            this.lstDish = new DevExpress.XtraEditors.ListBoxControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.rightSplit = new DevExpress.XtraEditors.SplitContainerControl();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NameCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AmountCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PriceCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CostCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelCheckOut = new DevExpress.XtraEditors.PanelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonMale = new System.Windows.Forms.RadioButton();
            this.radioButtonFemale = new System.Windows.Forms.RadioButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxPointOfSale = new System.Windows.Forms.ComboBox();
            this.dateTimePickerBirthDate = new System.Windows.Forms.DateTimePicker();
            this.labelControlBirthDate = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEditSecondName = new DevExpress.XtraEditors.TextEdit();
            this.textEditLastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlEmail = new DevExpress.XtraEditors.LabelControl();
            this.textEditEmail = new DevExpress.XtraEditors.TextEdit();
            this.textBoxWebOrderId = new System.Windows.Forms.TextBox();
            this.callButton = new System.Windows.Forms.Button();
            this.fastDriverCheckBox = new System.Windows.Forms.CheckBox();
            this.personComboBox = new System.Windows.Forms.ComboBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.nonRadioButton = new System.Windows.Forms.RadioButton();
            this.isPaidRadioButton = new System.Windows.Forms.RadioButton();
            this.is5kRadioButton = new System.Windows.Forms.RadioButton();
            this.isUnCashRadioButton = new System.Windows.Forms.RadioButton();
            this.mruEditStreets = new DevExpress.XtraEditors.MRUEdit();
            this.discountLabel = new System.Windows.Forms.Label();
            this.panelPaymentMethod = new System.Windows.Forms.Panel();
            this.paymentMethodList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numCardNumber = new System.Windows.Forms.NumericUpDown();
            this.lblCardStatus = new DevExpress.XtraEditors.LabelControl();
            this.lblCard = new DevExpress.XtraEditors.LabelControl();
            this.orderTypeAndReservationPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.panelRealTimeOrder = new System.Windows.Forms.Panel();
            this.secondDayRadioButton = new System.Windows.Forms.RadioButton();
            this.firstDayRadioButton = new System.Windows.Forms.RadioButton();
            this.lblTimeKitchen = new DevExpress.XtraEditors.LabelControl();
            this.tEditKitchen = new DevExpress.XtraEditors.TimeEdit();
            this.tEditClient = new DevExpress.XtraEditors.TimeEdit();
            this.gBoxReserv = new System.Windows.Forms.GroupBox();
            this.reCalculate = new System.Windows.Forms.Button();
            this.btnReserve = new System.Windows.Forms.Button();
            this.rbSelectedTime = new System.Windows.Forms.RadioButton();
            this.rbNearestTime = new System.Windows.Forms.RadioButton();
            this.lblTimeClient = new DevExpress.XtraEditors.LabelControl();
            this.panelPreOrder = new System.Windows.Forms.Panel();
            this.lblTime = new DevExpress.XtraEditors.LabelControl();
            this.tEditPreOrder = new DevExpress.XtraEditors.TimeEdit();
            this.lblDate = new DevExpress.XtraEditors.LabelControl();
            this.datePreOrder = new System.Windows.Forms.DateTimePicker();
            this.panelFlags = new System.Windows.Forms.Panel();
            this.chkIsWindow = new System.Windows.Forms.CheckBox();
            this.chkPreOrder = new System.Windows.Forms.CheckBox();
            this.chkNeedDelivery = new System.Windows.Forms.CheckBox();
            this.txtPhone = new System.Windows.Forms.MaskedTextBox();
            this.lblOtherInfo = new DevExpress.XtraEditors.LabelControl();
            this.txtAdditional = new System.Windows.Forms.TextBox();
            this.txtPorch = new DevExpress.XtraEditors.TextEdit();
            this.lblPorch = new DevExpress.XtraEditors.LabelControl();
            this.txtAppartament = new DevExpress.XtraEditors.TextEdit();
            this.lblFlat = new DevExpress.XtraEditors.LabelControl();
            this.txtHouse = new DevExpress.XtraEditors.TextEdit();
            this.lblHouse = new DevExpress.XtraEditors.LabelControl();
            this.btnDone = new DevExpress.XtraEditors.SimpleButton();
            this.lblStreet = new DevExpress.XtraEditors.LabelControl();
            this.lblPhone = new DevExpress.XtraEditors.LabelControl();
            this.textEditFirstName = new DevExpress.XtraEditors.TextEdit();
            this.lblName = new DevExpress.XtraEditors.LabelControl();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.richWebOrder = new System.Windows.Forms.RichTextBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.textBoxCouponCode = new System.Windows.Forms.TextBox();
            this.labelCoupon = new System.Windows.Forms.Label();
            this.labelCouponInfo = new System.Windows.Forms.Label();
            this.buttonApplyCoupon = new System.Windows.Forms.Button();
            this.favDishesListBoxControl = new DevExpress.XtraEditors.ListBoxControl();
            this.buttonFillFromPhone = new System.Windows.Forms.Button();
            this.favDishesIdListBoxControl = new DevExpress.XtraEditors.ListBoxControl();
            this.toolTipPaymentMethod = new System.Windows.Forms.ToolTip(this.components);
            this.enabledTimer = new System.Windows.Forms.Timer(this.components);
            this.testCallTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplit)).BeginInit();
            this.mainSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstDishListIDs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstDish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rightSplit)).BeginInit();
            this.rightSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCheckOut)).BeginInit();
            this.panelCheckOut.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSecondName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEditStreets.Properties)).BeginInit();
            this.panelPaymentMethod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCardNumber)).BeginInit();
            this.orderTypeAndReservationPanel.SuspendLayout();
            this.panelRealTimeOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tEditKitchen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tEditClient.Properties)).BeginInit();
            this.gBoxReserv.SuspendLayout();
            this.panelPreOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tEditPreOrder.Properties)).BeginInit();
            this.panelFlags.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPorch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAppartament.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.favDishesListBoxControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.favDishesIdListBoxControl)).BeginInit();
            this.SuspendLayout();
            // 
            // mainSplit
            // 
            this.mainSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplit.Location = new System.Drawing.Point(0, 0);
            this.mainSplit.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.mainSplit.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.mainSplit.Name = "mainSplit";
            this.mainSplit.Panel1.Controls.Add(this.splitContainerControl1);
            this.mainSplit.Panel1.Text = "Panel1";
            this.mainSplit.Panel2.Controls.Add(this.splitContainerControl2);
            this.mainSplit.Panel2.Text = "Panel2";
            this.mainSplit.Size = new System.Drawing.Size(1443, 682);
            this.mainSplit.SplitterPosition = 308;
            this.mainSplit.TabIndex = 0;
            this.mainSplit.Text = "splitContainerControl1";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.lstCategory);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.offPanel);
            this.splitContainerControl1.Panel2.Controls.Add(this.onPanel);
            this.splitContainerControl1.Panel2.Controls.Add(this.lstDishListIDs);
            this.splitContainerControl1.Panel2.Controls.Add(this.lstDish);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(308, 682);
            this.splitContainerControl1.SplitterPosition = 128;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // lstCategory
            // 
            this.lstCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstCategory.Location = new System.Drawing.Point(0, 0);
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(128, 682);
            this.lstCategory.TabIndex = 0;
            this.lstCategory.ToolTip = "Категории";
            this.lstCategory.MouseClick += new System.Windows.Forms.MouseEventHandler(this.categoryList_MouseClick);
            // 
            // offPanel
            // 
            this.offPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("offPanel.BackgroundImage")));
            this.offPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.offPanel.Location = new System.Drawing.Point(27, 114);
            this.offPanel.Name = "offPanel";
            this.offPanel.Size = new System.Drawing.Size(48, 46);
            this.offPanel.TabIndex = 5;
            this.offPanel.Visible = false;
            // 
            // onPanel
            // 
            this.onPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("onPanel.BackgroundImage")));
            this.onPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.onPanel.Location = new System.Drawing.Point(27, 43);
            this.onPanel.Name = "onPanel";
            this.onPanel.Size = new System.Drawing.Size(48, 51);
            this.onPanel.TabIndex = 4;
            this.onPanel.Visible = false;
            // 
            // lstDishListIDs
            // 
            this.lstDishListIDs.Location = new System.Drawing.Point(6, 357);
            this.lstDishListIDs.Name = "lstDishListIDs";
            this.lstDishListIDs.Size = new System.Drawing.Size(180, 148);
            this.lstDishListIDs.TabIndex = 3;
            this.lstDishListIDs.Visible = false;
            // 
            // lstDish
            // 
            this.lstDish.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDish.Location = new System.Drawing.Point(0, 0);
            this.lstDish.Name = "lstDish";
            this.lstDish.Size = new System.Drawing.Size(175, 682);
            this.lstDish.TabIndex = 1;
            this.lstDish.ToolTip = "Блюда";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.rightSplit);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainerControl2.Panel2.Controls.Add(this.favDishesIdListBoxControl);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1130, 682);
            this.splitContainerControl2.SplitterPosition = 607;
            this.splitContainerControl2.TabIndex = 2006;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // rightSplit
            // 
            this.rightSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightSplit.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.rightSplit.Horizontal = false;
            this.rightSplit.Location = new System.Drawing.Point(0, 0);
            this.rightSplit.Name = "rightSplit";
            this.rightSplit.Panel1.Controls.Add(this.mainGridControl);
            this.rightSplit.Panel1.Text = "Panel1";
            this.rightSplit.Panel2.Controls.Add(this.panelCheckOut);
            this.rightSplit.Panel2.Text = "Panel2";
            this.rightSplit.Size = new System.Drawing.Size(607, 682);
            this.rightSplit.SplitterPosition = 416;
            this.rightSplit.TabIndex = 0;
            this.rightSplit.Text = "splitContainerControl1";
            // 
            // mainGridControl
            // 
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(607, 261);
            this.mainGridControl.TabIndex = 2;
            this.mainGridControl.Tag = "1";
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.NameCol,
            this.AmountCol,
            this.PriceCol,
            this.CostCol});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewOrderItems_CustomColumnDisplayText);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "№";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 29;
            // 
            // NameCol
            // 
            this.NameCol.Caption = "Название";
            this.NameCol.FieldName = "Dish.Name";
            this.NameCol.Name = "NameCol";
            this.NameCol.OptionsColumn.AllowEdit = false;
            this.NameCol.OptionsColumn.AllowFocus = false;
            this.NameCol.OptionsColumn.ReadOnly = true;
            this.NameCol.Visible = true;
            this.NameCol.VisibleIndex = 1;
            this.NameCol.Width = 208;
            // 
            // AmountCol
            // 
            this.AmountCol.Caption = "Количество";
            this.AmountCol.FieldName = "Amount";
            this.AmountCol.Name = "AmountCol";
            this.AmountCol.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.AmountCol.Visible = true;
            this.AmountCol.VisibleIndex = 2;
            this.AmountCol.Width = 84;
            // 
            // PriceCol
            // 
            this.PriceCol.Caption = "Цена";
            this.PriceCol.FieldName = "Dish.Price";
            this.PriceCol.Name = "PriceCol";
            this.PriceCol.OptionsColumn.AllowEdit = false;
            this.PriceCol.OptionsColumn.AllowFocus = false;
            this.PriceCol.OptionsColumn.ReadOnly = true;
            this.PriceCol.Visible = true;
            this.PriceCol.VisibleIndex = 3;
            this.PriceCol.Width = 67;
            // 
            // CostCol
            // 
            this.CostCol.Caption = "Сумма";
            this.CostCol.DisplayFormat.FormatString = "c2 руб.";
            this.CostCol.FieldName = "Cost";
            this.CostCol.Name = "CostCol";
            this.CostCol.OptionsColumn.AllowEdit = false;
            this.CostCol.OptionsColumn.AllowFocus = false;
            this.CostCol.OptionsColumn.ReadOnly = true;
            this.CostCol.ShowUnboundExpressionMenu = true;
            this.CostCol.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom)});
            this.CostCol.UnboundExpression = "[Amount] * [Dish.Price]";
            this.CostCol.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.CostCol.Visible = true;
            this.CostCol.VisibleIndex = 4;
            this.CostCol.Width = 222;
            // 
            // panelCheckOut
            // 
            this.panelCheckOut.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.panelCheckOut.Controls.Add(this.groupBox1);
            this.panelCheckOut.Controls.Add(this.labelControl4);
            this.panelCheckOut.Controls.Add(this.comboBoxPointOfSale);
            this.panelCheckOut.Controls.Add(this.dateTimePickerBirthDate);
            this.panelCheckOut.Controls.Add(this.labelControlBirthDate);
            this.panelCheckOut.Controls.Add(this.labelControl3);
            this.panelCheckOut.Controls.Add(this.textEditSecondName);
            this.panelCheckOut.Controls.Add(this.textEditLastName);
            this.panelCheckOut.Controls.Add(this.labelControl2);
            this.panelCheckOut.Controls.Add(this.labelControlEmail);
            this.panelCheckOut.Controls.Add(this.textEditEmail);
            this.panelCheckOut.Controls.Add(this.textBoxWebOrderId);
            this.panelCheckOut.Controls.Add(this.callButton);
            this.panelCheckOut.Controls.Add(this.fastDriverCheckBox);
            this.panelCheckOut.Controls.Add(this.personComboBox);
            this.panelCheckOut.Controls.Add(this.labelControl1);
            this.panelCheckOut.Controls.Add(this.nonRadioButton);
            this.panelCheckOut.Controls.Add(this.isPaidRadioButton);
            this.panelCheckOut.Controls.Add(this.is5kRadioButton);
            this.panelCheckOut.Controls.Add(this.isUnCashRadioButton);
            this.panelCheckOut.Controls.Add(this.mruEditStreets);
            this.panelCheckOut.Controls.Add(this.discountLabel);
            this.panelCheckOut.Controls.Add(this.panelPaymentMethod);
            this.panelCheckOut.Controls.Add(this.numCardNumber);
            this.panelCheckOut.Controls.Add(this.lblCardStatus);
            this.panelCheckOut.Controls.Add(this.lblCard);
            this.panelCheckOut.Controls.Add(this.orderTypeAndReservationPanel);
            this.panelCheckOut.Controls.Add(this.txtPhone);
            this.panelCheckOut.Controls.Add(this.lblOtherInfo);
            this.panelCheckOut.Controls.Add(this.txtAdditional);
            this.panelCheckOut.Controls.Add(this.txtPorch);
            this.panelCheckOut.Controls.Add(this.lblPorch);
            this.panelCheckOut.Controls.Add(this.txtAppartament);
            this.panelCheckOut.Controls.Add(this.lblFlat);
            this.panelCheckOut.Controls.Add(this.txtHouse);
            this.panelCheckOut.Controls.Add(this.lblHouse);
            this.panelCheckOut.Controls.Add(this.btnDone);
            this.panelCheckOut.Controls.Add(this.lblStreet);
            this.panelCheckOut.Controls.Add(this.lblPhone);
            this.panelCheckOut.Controls.Add(this.textEditFirstName);
            this.panelCheckOut.Controls.Add(this.lblName);
            this.panelCheckOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCheckOut.Location = new System.Drawing.Point(0, 0);
            this.panelCheckOut.Name = "panelCheckOut";
            this.panelCheckOut.Size = new System.Drawing.Size(607, 416);
            this.panelCheckOut.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonMale);
            this.groupBox1.Controls.Add(this.radioButtonFemale);
            this.groupBox1.Location = new System.Drawing.Point(7, 195);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(328, 39);
            this.groupBox1.TabIndex = 2043;
            this.groupBox1.TabStop = false;
            // 
            // radioButtonMale
            // 
            this.radioButtonMale.AutoSize = true;
            this.radioButtonMale.Location = new System.Drawing.Point(6, 13);
            this.radioButtonMale.Name = "radioButtonMale";
            this.radioButtonMale.Size = new System.Drawing.Size(70, 17);
            this.radioButtonMale.TabIndex = 2039;
            this.radioButtonMale.TabStop = true;
            this.radioButtonMale.Text = "Мужчина";
            this.radioButtonMale.UseVisualStyleBackColor = true;
            // 
            // radioButtonFemale
            // 
            this.radioButtonFemale.AutoSize = true;
            this.radioButtonFemale.Location = new System.Drawing.Point(82, 12);
            this.radioButtonFemale.Name = "radioButtonFemale";
            this.radioButtonFemale.Size = new System.Drawing.Size(75, 17);
            this.radioButtonFemale.TabIndex = 2040;
            this.radioButtonFemale.TabStop = true;
            this.radioButtonFemale.Text = "Женщина";
            this.radioButtonFemale.UseVisualStyleBackColor = true;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(352, 47);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(72, 13);
            this.labelControl4.TabIndex = 2042;
            this.labelControl4.Text = "Точка продаж";
            // 
            // comboBoxPointOfSale
            // 
            this.comboBoxPointOfSale.FormattingEnabled = true;
            this.comboBoxPointOfSale.Location = new System.Drawing.Point(430, 44);
            this.comboBoxPointOfSale.Name = "comboBoxPointOfSale";
            this.comboBoxPointOfSale.Size = new System.Drawing.Size(166, 21);
            this.comboBoxPointOfSale.TabIndex = 2041;
            // 
            // dateTimePickerBirthDate
            // 
            this.dateTimePickerBirthDate.CustomFormat = " ";
            this.dateTimePickerBirthDate.Location = new System.Drawing.Point(94, 169);
            this.dateTimePickerBirthDate.Name = "dateTimePickerBirthDate";
            this.dateTimePickerBirthDate.Size = new System.Drawing.Size(122, 20);
            this.dateTimePickerBirthDate.TabIndex = 2038;
            // 
            // labelControlBirthDate
            // 
            this.labelControlBirthDate.Location = new System.Drawing.Point(6, 172);
            this.labelControlBirthDate.Name = "labelControlBirthDate";
            this.labelControlBirthDate.Size = new System.Drawing.Size(80, 13);
            this.labelControlBirthDate.TabIndex = 2037;
            this.labelControlBirthDate.Text = "День рождения";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(118, 118);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(49, 13);
            this.labelControl3.TabIndex = 2036;
            this.labelControl3.Text = "Отчество";
            // 
            // textEditSecondName
            // 
            this.textEditSecondName.Location = new System.Drawing.Point(118, 138);
            this.textEditSecondName.Name = "textEditSecondName";
            this.textEditSecondName.Size = new System.Drawing.Size(109, 20);
            this.textEditSecondName.TabIndex = 2035;
            // 
            // textEditLastName
            // 
            this.textEditLastName.Location = new System.Drawing.Point(233, 138);
            this.textEditLastName.Name = "textEditLastName";
            this.textEditLastName.Size = new System.Drawing.Size(109, 20);
            this.textEditLastName.TabIndex = 2034;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(234, 118);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(44, 13);
            this.labelControl2.TabIndex = 2033;
            this.labelControl2.Text = "Фамилия";
            // 
            // labelControlEmail
            // 
            this.labelControlEmail.Location = new System.Drawing.Point(352, 20);
            this.labelControlEmail.Name = "labelControlEmail";
            this.labelControlEmail.Size = new System.Drawing.Size(24, 13);
            this.labelControlEmail.TabIndex = 2032;
            this.labelControlEmail.Text = "Email";
            // 
            // textEditEmail
            // 
            this.textEditEmail.Location = new System.Drawing.Point(382, 17);
            this.textEditEmail.Name = "textEditEmail";
            this.textEditEmail.Size = new System.Drawing.Size(215, 20);
            this.textEditEmail.TabIndex = 2031;
            // 
            // textBoxWebOrderId
            // 
            this.textBoxWebOrderId.Location = new System.Drawing.Point(492, 356);
            this.textBoxWebOrderId.Name = "textBoxWebOrderId";
            this.textBoxWebOrderId.Size = new System.Drawing.Size(100, 20);
            this.textBoxWebOrderId.TabIndex = 1;
            this.textBoxWebOrderId.Visible = false;
            // 
            // callButton
            // 
            this.callButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("callButton.BackgroundImage")));
            this.callButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.callButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.callButton.Location = new System.Drawing.Point(297, 59);
            this.callButton.Name = "callButton";
            this.callButton.Size = new System.Drawing.Size(38, 39);
            this.callButton.TabIndex = 2030;
            this.callButton.UseVisualStyleBackColor = true;
            this.callButton.Click += new System.EventHandler(this.callButton_Click);
            // 
            // fastDriverCheckBox
            // 
            this.fastDriverCheckBox.AutoSize = true;
            this.fastDriverCheckBox.Location = new System.Drawing.Point(366, 353);
            this.fastDriverCheckBox.Name = "fastDriverCheckBox";
            this.fastDriverCheckBox.Size = new System.Drawing.Size(120, 17);
            this.fastDriverCheckBox.TabIndex = 2029;
            this.fastDriverCheckBox.Text = "Быстрая доставка";
            this.fastDriverCheckBox.UseVisualStyleBackColor = true;
            // 
            // personComboBox
            // 
            this.personComboBox.FormattingEnabled = true;
            this.personComboBox.Items.AddRange(new object[] {
            "- - -",
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "25",
            "30",
            "35",
            "40",
            "50"});
            this.personComboBox.Location = new System.Drawing.Point(457, 329);
            this.personComboBox.MaxDropDownItems = 10;
            this.personComboBox.Name = "personComboBox";
            this.personComboBox.Size = new System.Drawing.Size(77, 21);
            this.personComboBox.TabIndex = 2028;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(364, 332);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(83, 13);
            this.labelControl1.TabIndex = 2027;
            this.labelControl1.Text = "На число персон";
            // 
            // nonRadioButton
            // 
            this.nonRadioButton.AutoSize = true;
            this.nonRadioButton.Checked = true;
            this.nonRadioButton.Location = new System.Drawing.Point(7, 336);
            this.nonRadioButton.Name = "nonRadioButton";
            this.nonRadioButton.Size = new System.Drawing.Size(44, 17);
            this.nonRadioButton.TabIndex = 2025;
            this.nonRadioButton.TabStop = true;
            this.nonRadioButton.Text = "Нет";
            this.nonRadioButton.UseVisualStyleBackColor = true;
            // 
            // isPaidRadioButton
            // 
            this.isPaidRadioButton.AutoSize = true;
            this.isPaidRadioButton.Location = new System.Drawing.Point(222, 336);
            this.isPaidRadioButton.Name = "isPaidRadioButton";
            this.isPaidRadioButton.Size = new System.Drawing.Size(74, 17);
            this.isPaidRadioButton.TabIndex = 2024;
            this.isPaidRadioButton.Text = "Оплачено";
            this.isPaidRadioButton.UseVisualStyleBackColor = true;
            // 
            // is5kRadioButton
            // 
            this.is5kRadioButton.AutoSize = true;
            this.is5kRadioButton.Location = new System.Drawing.Point(125, 336);
            this.is5kRadioButton.Name = "is5kRadioButton";
            this.is5kRadioButton.Size = new System.Drawing.Size(91, 17);
            this.is5kRadioButton.TabIndex = 2023;
            this.is5kRadioButton.Text = "Сдача с 5000";
            this.is5kRadioButton.UseVisualStyleBackColor = true;
            this.is5kRadioButton.CheckedChanged += new System.EventHandler(this.is5kRadioButton_CheckedChanged);
            // 
            // isUnCashRadioButton
            // 
            this.isUnCashRadioButton.AutoSize = true;
            this.isUnCashRadioButton.Location = new System.Drawing.Point(57, 336);
            this.isUnCashRadioButton.Name = "isUnCashRadioButton";
            this.isUnCashRadioButton.Size = new System.Drawing.Size(62, 17);
            this.isUnCashRadioButton.TabIndex = 2022;
            this.isUnCashRadioButton.Text = "Безнал";
            this.isUnCashRadioButton.UseVisualStyleBackColor = true;
            this.isUnCashRadioButton.CheckedChanged += new System.EventHandler(this.isUnCashRadioButton_CheckedChanged);
            // 
            // mruEditStreets
            // 
            this.mruEditStreets.Location = new System.Drawing.Point(6, 266);
            this.mruEditStreets.Name = "mruEditStreets";
            this.mruEditStreets.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.True;
            this.mruEditStreets.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.mruEditStreets.Properties.AllowRemoveMRUItems = false;
            this.mruEditStreets.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.mruEditStreets.Properties.SelectedValueChanged += new System.EventHandler(this.mruEditStreets_Properties_SelectedValueChanged);
            this.mruEditStreets.Size = new System.Drawing.Size(155, 20);
            this.mruEditStreets.TabIndex = 5;
            // 
            // discountLabel
            // 
            this.discountLabel.AutoSize = true;
            this.discountLabel.Location = new System.Drawing.Point(231, 58);
            this.discountLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.discountLabel.Name = "discountLabel";
            this.discountLabel.Size = new System.Drawing.Size(61, 13);
            this.discountLabel.TabIndex = 2018;
            this.discountLabel.Text = "Скидка 0%";
            // 
            // panelPaymentMethod
            // 
            this.panelPaymentMethod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPaymentMethod.Controls.Add(this.paymentMethodList);
            this.panelPaymentMethod.Controls.Add(this.label1);
            this.panelPaymentMethod.Location = new System.Drawing.Point(5, 7);
            this.panelPaymentMethod.Name = "panelPaymentMethod";
            this.panelPaymentMethod.Size = new System.Drawing.Size(331, 40);
            this.panelPaymentMethod.TabIndex = 2017;
            // 
            // paymentMethodList
            // 
            this.paymentMethodList.FormattingEnabled = true;
            this.paymentMethodList.Location = new System.Drawing.Point(86, 9);
            this.paymentMethodList.Name = "paymentMethodList";
            this.paymentMethodList.Size = new System.Drawing.Size(199, 21);
            this.paymentMethodList.TabIndex = 2018;
            this.paymentMethodList.SelectedIndexChanged += new System.EventHandler(this.paymentMethodList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 2017;
            this.label1.Text = "Метод оплаты";
            // 
            // numCardNumber
            // 
            this.numCardNumber.Location = new System.Drawing.Point(118, 56);
            this.numCardNumber.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numCardNumber.Name = "numCardNumber";
            this.numCardNumber.Size = new System.Drawing.Size(100, 20);
            this.numCardNumber.TabIndex = 2014;
            this.numCardNumber.Validating += new System.ComponentModel.CancelEventHandler(this.numCardNumber_Validating);
            // 
            // lblCardStatus
            // 
            this.lblCardStatus.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblCardStatus.LineColor = System.Drawing.Color.White;
            this.lblCardStatus.Location = new System.Drawing.Point(5, 59);
            this.lblCardStatus.Name = "lblCardStatus";
            this.lblCardStatus.Size = new System.Drawing.Size(93, 13);
            this.lblCardStatus.TabIndex = 2013;
            this.lblCardStatus.Text = "Карта не выбрана";
            // 
            // lblCard
            // 
            this.lblCard.Location = new System.Drawing.Point(6, 58);
            this.lblCard.Name = "lblCard";
            this.lblCard.Size = new System.Drawing.Size(105, 13);
            this.lblCard.TabIndex = 2012;
            this.lblCard.Text = "Скидочная карта №";
            this.lblCard.Click += new System.EventHandler(this.labelControl12_Click);
            // 
            // orderTypeAndReservationPanel
            // 
            this.orderTypeAndReservationPanel.Controls.Add(this.panelRealTimeOrder);
            this.orderTypeAndReservationPanel.Controls.Add(this.panelPreOrder);
            this.orderTypeAndReservationPanel.Controls.Add(this.panelFlags);
            this.orderTypeAndReservationPanel.Location = new System.Drawing.Point(349, 77);
            this.orderTypeAndReservationPanel.Name = "orderTypeAndReservationPanel";
            this.orderTypeAndReservationPanel.Size = new System.Drawing.Size(261, 249);
            this.orderTypeAndReservationPanel.TabIndex = 2010;
            // 
            // panelRealTimeOrder
            // 
            this.panelRealTimeOrder.Controls.Add(this.secondDayRadioButton);
            this.panelRealTimeOrder.Controls.Add(this.firstDayRadioButton);
            this.panelRealTimeOrder.Controls.Add(this.lblTimeKitchen);
            this.panelRealTimeOrder.Controls.Add(this.tEditKitchen);
            this.panelRealTimeOrder.Controls.Add(this.tEditClient);
            this.panelRealTimeOrder.Controls.Add(this.gBoxReserv);
            this.panelRealTimeOrder.Controls.Add(this.lblTimeClient);
            this.panelRealTimeOrder.Location = new System.Drawing.Point(3, 3);
            this.panelRealTimeOrder.Name = "panelRealTimeOrder";
            this.panelRealTimeOrder.Size = new System.Drawing.Size(250, 151);
            this.panelRealTimeOrder.TabIndex = 2007;
            // 
            // secondDayRadioButton
            // 
            this.secondDayRadioButton.AutoSize = true;
            this.secondDayRadioButton.Enabled = false;
            this.secondDayRadioButton.Location = new System.Drawing.Point(13, 115);
            this.secondDayRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.secondDayRadioButton.Name = "secondDayRadioButton";
            this.secondDayRadioButton.Size = new System.Drawing.Size(81, 17);
            this.secondDayRadioButton.TabIndex = 2007;
            this.secondDayRadioButton.Text = "SecondDay";
            this.secondDayRadioButton.UseVisualStyleBackColor = true;
            // 
            // firstDayRadioButton
            // 
            this.firstDayRadioButton.AutoSize = true;
            this.firstDayRadioButton.Checked = true;
            this.firstDayRadioButton.Enabled = false;
            this.firstDayRadioButton.Location = new System.Drawing.Point(13, 93);
            this.firstDayRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.firstDayRadioButton.Name = "firstDayRadioButton";
            this.firstDayRadioButton.Size = new System.Drawing.Size(63, 17);
            this.firstDayRadioButton.TabIndex = 2006;
            this.firstDayRadioButton.TabStop = true;
            this.firstDayRadioButton.Text = "FirstDay";
            this.firstDayRadioButton.UseVisualStyleBackColor = true;
            // 
            // lblTimeKitchen
            // 
            this.lblTimeKitchen.Location = new System.Drawing.Point(13, 7);
            this.lblTimeKitchen.Name = "lblTimeKitchen";
            this.lblTimeKitchen.Size = new System.Drawing.Size(63, 13);
            this.lblTimeKitchen.TabIndex = 11;
            this.lblTimeKitchen.Text = "Время кухня";
            // 
            // tEditKitchen
            // 
            this.tEditKitchen.CausesValidation = false;
            this.tEditKitchen.EditValue = new System.DateTime(2012, 1, 16, 0, 0, 0, 0);
            this.tEditKitchen.Location = new System.Drawing.Point(13, 23);
            this.tEditKitchen.Name = "tEditKitchen";
            this.tEditKitchen.Properties.AllowFocused = false;
            this.tEditKitchen.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.tEditKitchen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tEditKitchen.Properties.DisplayFormat.FormatString = "t";
            this.tEditKitchen.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditKitchen.Properties.EditFormat.FormatString = "t";
            this.tEditKitchen.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditKitchen.Properties.EditValueChangedDelay = 2500;
            this.tEditKitchen.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.tEditKitchen.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.tEditKitchen.Properties.Mask.EditMask = "HH:mm";
            this.tEditKitchen.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.tEditKitchen.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.tEditKitchen.Properties.NullText = "Время кухни";
            this.tEditKitchen.Size = new System.Drawing.Size(76, 20);
            this.tEditKitchen.TabIndex = 10;
            this.tEditKitchen.EditValueChanged += new System.EventHandler(this.timeKitchen_EditValueChanged);
            // 
            // tEditClient
            // 
            this.tEditClient.EditValue = new System.DateTime(2012, 1, 16, 0, 0, 0, 0);
            this.tEditClient.Location = new System.Drawing.Point(13, 68);
            this.tEditClient.Name = "tEditClient";
            this.tEditClient.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tEditClient.Properties.DisplayFormat.FormatString = "t";
            this.tEditClient.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditClient.Properties.EditFormat.FormatString = "t";
            this.tEditClient.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditClient.Properties.EditValueChangedDelay = 2500;
            this.tEditClient.Properties.Mask.EditMask = "HH:mm";
            this.tEditClient.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.tEditClient.Size = new System.Drawing.Size(76, 20);
            this.tEditClient.TabIndex = 11;
            // 
            // gBoxReserv
            // 
            this.gBoxReserv.Controls.Add(this.reCalculate);
            this.gBoxReserv.Controls.Add(this.btnReserve);
            this.gBoxReserv.Controls.Add(this.rbSelectedTime);
            this.gBoxReserv.Controls.Add(this.rbNearestTime);
            this.gBoxReserv.Location = new System.Drawing.Point(105, 7);
            this.gBoxReserv.Name = "gBoxReserv";
            this.gBoxReserv.Size = new System.Drawing.Size(131, 137);
            this.gBoxReserv.TabIndex = 2005;
            this.gBoxReserv.TabStop = false;
            this.gBoxReserv.Text = "Резерв";
            // 
            // reCalculate
            // 
            this.reCalculate.Location = new System.Drawing.Point(8, 103);
            this.reCalculate.Name = "reCalculate";
            this.reCalculate.Size = new System.Drawing.Size(118, 26);
            this.reCalculate.TabIndex = 2007;
            this.reCalculate.Text = "Пересчитать время";
            this.reCalculate.UseVisualStyleBackColor = true;
            this.reCalculate.Visible = false;
            this.reCalculate.Click += new System.EventHandler(this.reCalculate_Click);
            // 
            // btnReserve
            // 
            this.btnReserve.Enabled = false;
            this.btnReserve.Location = new System.Drawing.Point(7, 66);
            this.btnReserve.Name = "btnReserve";
            this.btnReserve.Size = new System.Drawing.Size(118, 33);
            this.btnReserve.TabIndex = 2006;
            this.btnReserve.Text = "Зарезервировать";
            this.btnReserve.UseVisualStyleBackColor = true;
            this.btnReserve.Click += new System.EventHandler(this.BtnReserveClick);
            // 
            // rbSelectedTime
            // 
            this.rbSelectedTime.AutoSize = true;
            this.rbSelectedTime.Location = new System.Drawing.Point(6, 43);
            this.rbSelectedTime.Name = "rbSelectedTime";
            this.rbSelectedTime.Size = new System.Drawing.Size(119, 17);
            this.rbSelectedTime.TabIndex = 1;
            this.rbSelectedTime.TabStop = true;
            this.rbSelectedTime.Text = "Рассчетное время";
            this.rbSelectedTime.UseVisualStyleBackColor = true;
            this.rbSelectedTime.CheckedChanged += new System.EventHandler(this.rbSelectedTime_CheckedChanged);
            // 
            // rbNearestTime
            // 
            this.rbNearestTime.AutoSize = true;
            this.rbNearestTime.Location = new System.Drawing.Point(7, 20);
            this.rbNearestTime.Name = "rbNearestTime";
            this.rbNearestTime.Size = new System.Drawing.Size(119, 17);
            this.rbNearestTime.TabIndex = 0;
            this.rbNearestTime.TabStop = true;
            this.rbNearestTime.Text = "Ближайшее время";
            this.rbNearestTime.UseVisualStyleBackColor = true;
            // 
            // lblTimeClient
            // 
            this.lblTimeClient.Location = new System.Drawing.Point(13, 50);
            this.lblTimeClient.Name = "lblTimeClient";
            this.lblTimeClient.Size = new System.Drawing.Size(69, 13);
            this.lblTimeClient.TabIndex = 12;
            this.lblTimeClient.Text = "Время клиент";
            // 
            // panelPreOrder
            // 
            this.panelPreOrder.Controls.Add(this.lblTime);
            this.panelPreOrder.Controls.Add(this.tEditPreOrder);
            this.panelPreOrder.Controls.Add(this.lblDate);
            this.panelPreOrder.Controls.Add(this.datePreOrder);
            this.panelPreOrder.Location = new System.Drawing.Point(3, 160);
            this.panelPreOrder.Name = "panelPreOrder";
            this.panelPreOrder.Size = new System.Drawing.Size(250, 52);
            this.panelPreOrder.TabIndex = 2009;
            this.panelPreOrder.Visible = false;
            // 
            // lblTime
            // 
            this.lblTime.Location = new System.Drawing.Point(13, 3);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(30, 13);
            this.lblTime.TabIndex = 14;
            this.lblTime.Text = "Время";
            // 
            // tEditPreOrder
            // 
            this.tEditPreOrder.CausesValidation = false;
            this.tEditPreOrder.EditValue = new System.DateTime(2012, 1, 16, 0, 0, 0, 0);
            this.tEditPreOrder.Location = new System.Drawing.Point(13, 19);
            this.tEditPreOrder.Name = "tEditPreOrder";
            this.tEditPreOrder.Properties.AllowFocused = false;
            this.tEditPreOrder.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.tEditPreOrder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tEditPreOrder.Properties.DisplayFormat.FormatString = "t";
            this.tEditPreOrder.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditPreOrder.Properties.EditFormat.FormatString = "t";
            this.tEditPreOrder.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditPreOrder.Properties.EditValueChangedDelay = 2500;
            this.tEditPreOrder.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.tEditPreOrder.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.tEditPreOrder.Properties.Mask.EditMask = "HH:mm";
            this.tEditPreOrder.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.tEditPreOrder.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.tEditPreOrder.Properties.NullText = "Время кухни";
            this.tEditPreOrder.Size = new System.Drawing.Size(76, 20);
            this.tEditPreOrder.TabIndex = 13;
            // 
            // lblDate
            // 
            this.lblDate.Location = new System.Drawing.Point(105, 3);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(26, 13);
            this.lblDate.TabIndex = 12;
            this.lblDate.Text = "Дата";
            // 
            // datePreOrder
            // 
            this.datePreOrder.Location = new System.Drawing.Point(105, 19);
            this.datePreOrder.Name = "datePreOrder";
            this.datePreOrder.Size = new System.Drawing.Size(127, 20);
            this.datePreOrder.TabIndex = 0;
            // 
            // panelFlags
            // 
            this.panelFlags.Controls.Add(this.chkIsWindow);
            this.panelFlags.Controls.Add(this.chkPreOrder);
            this.panelFlags.Controls.Add(this.chkNeedDelivery);
            this.panelFlags.Location = new System.Drawing.Point(3, 218);
            this.panelFlags.Name = "panelFlags";
            this.panelFlags.Size = new System.Drawing.Size(250, 26);
            this.panelFlags.TabIndex = 2008;
            // 
            // chkIsWindow
            // 
            this.chkIsWindow.AutoSize = true;
            this.chkIsWindow.Location = new System.Drawing.Point(193, 5);
            this.chkIsWindow.Name = "chkIsWindow";
            this.chkIsWindow.Size = new System.Drawing.Size(52, 17);
            this.chkIsWindow.TabIndex = 2007;
            this.chkIsWindow.Text = "Окно";
            this.chkIsWindow.UseVisualStyleBackColor = true;
            // 
            // chkPreOrder
            // 
            this.chkPreOrder.AutoSize = true;
            this.chkPreOrder.Location = new System.Drawing.Point(105, 5);
            this.chkPreOrder.Name = "chkPreOrder";
            this.chkPreOrder.Size = new System.Drawing.Size(82, 17);
            this.chkPreOrder.TabIndex = 2006;
            this.chkPreOrder.Text = "Предзаказ";
            this.chkPreOrder.UseVisualStyleBackColor = true;
            this.chkPreOrder.CheckedChanged += new System.EventHandler(this.checkBoxPreOrder_CheckedChanged);
            // 
            // chkNeedDelivery
            // 
            this.chkNeedDelivery.AutoSize = true;
            this.chkNeedDelivery.Checked = true;
            this.chkNeedDelivery.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkNeedDelivery.Location = new System.Drawing.Point(13, 3);
            this.chkNeedDelivery.Name = "chkNeedDelivery";
            this.chkNeedDelivery.Size = new System.Drawing.Size(76, 17);
            this.chkNeedDelivery.TabIndex = 2001;
            this.chkNeedDelivery.Text = "Доставка";
            this.chkNeedDelivery.UseVisualStyleBackColor = true;
            // 
            // txtPhone
            // 
            this.txtPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPhone.Location = new System.Drawing.Point(83, 84);
            this.txtPhone.Mask = "(000) 000-00-00";
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(135, 26);
            this.txtPhone.TabIndex = 4;
            this.txtPhone.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.txtPhone.Enter += new System.EventHandler(this.textPhone_Enter);
            // 
            // lblOtherInfo
            // 
            this.lblOtherInfo.Location = new System.Drawing.Point(6, 292);
            this.lblOtherInfo.Name = "lblOtherInfo";
            this.lblOtherInfo.Size = new System.Drawing.Size(151, 13);
            this.lblOtherInfo.TabIndex = 1008;
            this.lblOtherInfo.Text = "Дополнительная информация";
            // 
            // txtAdditional
            // 
            this.txtAdditional.Location = new System.Drawing.Point(5, 311);
            this.txtAdditional.Name = "txtAdditional";
            this.txtAdditional.Size = new System.Drawing.Size(331, 20);
            this.txtAdditional.TabIndex = 12;
            // 
            // txtPorch
            // 
            this.txtPorch.Location = new System.Drawing.Point(295, 266);
            this.txtPorch.Name = "txtPorch";
            this.txtPorch.Size = new System.Drawing.Size(41, 20);
            this.txtPorch.TabIndex = 8;
            // 
            // lblPorch
            // 
            this.lblPorch.Location = new System.Drawing.Point(295, 247);
            this.lblPorch.Name = "lblPorch";
            this.lblPorch.Size = new System.Drawing.Size(45, 13);
            this.lblPorch.TabIndex = 1005;
            this.lblPorch.Text = "Подъезд";
            // 
            // txtAppartament
            // 
            this.txtAppartament.Location = new System.Drawing.Point(231, 266);
            this.txtAppartament.Name = "txtAppartament";
            this.txtAppartament.Size = new System.Drawing.Size(60, 20);
            this.txtAppartament.TabIndex = 7;
            // 
            // lblFlat
            // 
            this.lblFlat.Location = new System.Drawing.Point(232, 247);
            this.lblFlat.Name = "lblFlat";
            this.lblFlat.Size = new System.Drawing.Size(49, 13);
            this.lblFlat.TabIndex = 1003;
            this.lblFlat.Text = "Квартира";
            // 
            // txtHouse
            // 
            this.txtHouse.Location = new System.Drawing.Point(167, 266);
            this.txtHouse.Name = "txtHouse";
            this.txtHouse.Size = new System.Drawing.Size(60, 20);
            this.txtHouse.TabIndex = 6;
            // 
            // lblHouse
            // 
            this.lblHouse.Location = new System.Drawing.Point(168, 247);
            this.lblHouse.Name = "lblHouse";
            this.lblHouse.Size = new System.Drawing.Size(20, 13);
            this.lblHouse.TabIndex = 1001;
            this.lblHouse.Text = "Дом";
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(6, 358);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(330, 53);
            this.btnDone.TabIndex = 13;
            this.btnDone.Text = "Создать заказ";
            // 
            // lblStreet
            // 
            this.lblStreet.Location = new System.Drawing.Point(6, 247);
            this.lblStreet.Name = "lblStreet";
            this.lblStreet.Size = new System.Drawing.Size(31, 13);
            this.lblStreet.TabIndex = 6;
            this.lblStreet.Text = "Улица";
            // 
            // lblPhone
            // 
            this.lblPhone.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.lblPhone.Location = new System.Drawing.Point(6, 87);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(65, 19);
            this.lblPhone.TabIndex = 4;
            this.lblPhone.Text = "Телефон";
            // 
            // textEditFirstName
            // 
            this.textEditFirstName.Location = new System.Drawing.Point(6, 138);
            this.textEditFirstName.Name = "textEditFirstName";
            this.textEditFirstName.Size = new System.Drawing.Size(106, 20);
            this.textEditFirstName.TabIndex = 3;
            this.textEditFirstName.EditValueChanged += new System.EventHandler(this.textName_EditValueChanged);
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(6, 118);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(19, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Имя";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.richWebOrder);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Size = new System.Drawing.Size(518, 682);
            this.splitContainer1.SplitterDistance = 210;
            this.splitContainer1.TabIndex = 5;
            // 
            // richWebOrder
            // 
            this.richWebOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richWebOrder.Location = new System.Drawing.Point(4, 4);
            this.richWebOrder.Name = "richWebOrder";
            this.richWebOrder.Size = new System.Drawing.Size(510, 202);
            this.richWebOrder.TabIndex = 0;
            this.richWebOrder.Tag = "Note";
            this.richWebOrder.Text = "";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(4, 4);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.textBoxCouponCode);
            this.splitContainer2.Panel1.Controls.Add(this.labelCoupon);
            this.splitContainer2.Panel1.Controls.Add(this.labelCouponInfo);
            this.splitContainer2.Panel1.Controls.Add(this.buttonApplyCoupon);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.favDishesListBoxControl);
            this.splitContainer2.Panel2.Controls.Add(this.buttonFillFromPhone);
            this.splitContainer2.Size = new System.Drawing.Size(510, 460);
            this.splitContainer2.SplitterDistance = 174;
            this.splitContainer2.TabIndex = 4;
            // 
            // textBoxCouponCode
            // 
            this.textBoxCouponCode.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBoxCouponCode.Location = new System.Drawing.Point(48, 0);
            this.textBoxCouponCode.Name = "textBoxCouponCode";
            this.textBoxCouponCode.Size = new System.Drawing.Size(122, 20);
            this.textBoxCouponCode.TabIndex = 0;
            // 
            // labelCoupon
            // 
            this.labelCoupon.AutoSize = true;
            this.labelCoupon.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCoupon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCoupon.Location = new System.Drawing.Point(0, 0);
            this.labelCoupon.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.labelCoupon.Name = "labelCoupon";
            this.labelCoupon.Size = new System.Drawing.Size(48, 17);
            this.labelCoupon.TabIndex = 2;
            this.labelCoupon.Text = "Купон";
            // 
            // labelCouponInfo
            // 
            this.labelCouponInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelCouponInfo.Location = new System.Drawing.Point(0, 40);
            this.labelCouponInfo.Margin = new System.Windows.Forms.Padding(3, 30, 3, 0);
            this.labelCouponInfo.Name = "labelCouponInfo";
            this.labelCouponInfo.Size = new System.Drawing.Size(510, 111);
            this.labelCouponInfo.TabIndex = 3;
            this.labelCouponInfo.Text = "Информация по купону";
            // 
            // buttonApplyCoupon
            // 
            this.buttonApplyCoupon.AllowDrop = true;
            this.buttonApplyCoupon.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonApplyCoupon.Location = new System.Drawing.Point(0, 151);
            this.buttonApplyCoupon.Name = "buttonApplyCoupon";
            this.buttonApplyCoupon.Size = new System.Drawing.Size(510, 23);
            this.buttonApplyCoupon.TabIndex = 1;
            this.buttonApplyCoupon.Text = "Применить купон";
            this.buttonApplyCoupon.UseVisualStyleBackColor = true;
            // 
            // favDishesListBoxControl
            // 
            this.favDishesListBoxControl.Appearance.BackColor = System.Drawing.Color.White;
            this.favDishesListBoxControl.Appearance.Options.UseBackColor = true;
            this.favDishesListBoxControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.favDishesListBoxControl.Location = new System.Drawing.Point(0, 0);
            this.favDishesListBoxControl.Name = "favDishesListBoxControl";
            this.favDishesListBoxControl.Size = new System.Drawing.Size(510, 259);
            this.favDishesListBoxControl.TabIndex = 2;
            // 
            // buttonFillFromPhone
            // 
            this.buttonFillFromPhone.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonFillFromPhone.Location = new System.Drawing.Point(0, 259);
            this.buttonFillFromPhone.Name = "buttonFillFromPhone";
            this.buttonFillFromPhone.Size = new System.Drawing.Size(510, 23);
            this.buttonFillFromPhone.TabIndex = 3;
            this.buttonFillFromPhone.Text = "Заполнить все поля по телефону";
            this.buttonFillFromPhone.UseVisualStyleBackColor = true;
            // 
            // favDishesIdListBoxControl
            // 
            this.favDishesIdListBoxControl.Location = new System.Drawing.Point(33, 230);
            this.favDishesIdListBoxControl.Name = "favDishesIdListBoxControl";
            this.favDishesIdListBoxControl.Size = new System.Drawing.Size(180, 148);
            this.favDishesIdListBoxControl.TabIndex = 4;
            this.favDishesIdListBoxControl.Visible = false;
            // 
            // toolTipPaymentMethod
            // 
            this.toolTipPaymentMethod.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // testCallTimer
            // 
            this.testCallTimer.Enabled = true;
            this.testCallTimer.Interval = 1500;
            this.testCallTimer.Tick += new System.EventHandler(this.testCallTimer_Tick);
            // 
            // DeliveryOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1443, 682);
            this.Controls.Add(this.mainSplit);
            this.KeyPreview = true;
            this.Name = "DeliveryOrderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Заказ на доставку";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DetailsForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DeliveryOrderForm_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DetailsForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplit)).EndInit();
            this.mainSplit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstDishListIDs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstDish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rightSplit)).EndInit();
            this.rightSplit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCheckOut)).EndInit();
            this.panelCheckOut.ResumeLayout(false);
            this.panelCheckOut.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSecondName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mruEditStreets.Properties)).EndInit();
            this.panelPaymentMethod.ResumeLayout(false);
            this.panelPaymentMethod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCardNumber)).EndInit();
            this.orderTypeAndReservationPanel.ResumeLayout(false);
            this.panelRealTimeOrder.ResumeLayout(false);
            this.panelRealTimeOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tEditKitchen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tEditClient.Properties)).EndInit();
            this.gBoxReserv.ResumeLayout(false);
            this.gBoxReserv.PerformLayout();
            this.panelPreOrder.ResumeLayout(false);
            this.panelPreOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tEditPreOrder.Properties)).EndInit();
            this.panelFlags.ResumeLayout(false);
            this.panelFlags.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPorch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAppartament.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirstName.Properties)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.favDishesListBoxControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.favDishesIdListBoxControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected DevExpress.XtraEditors.SplitContainerControl mainSplit;
        protected DevExpress.XtraEditors.SplitContainerControl rightSplit;
        protected DevExpress.XtraGrid.GridControl mainGridControl;
        protected DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        protected DevExpress.XtraGrid.Columns.GridColumn NameCol;
        protected DevExpress.XtraGrid.Columns.GridColumn AmountCol;
        protected DevExpress.XtraGrid.Columns.GridColumn PriceCol;
        protected DevExpress.XtraGrid.Columns.GridColumn CostCol;
        protected DevExpress.XtraEditors.PanelControl panelCheckOut;
        protected DevExpress.XtraEditors.SimpleButton btnDone;
        protected DevExpress.XtraEditors.LabelControl lblStreet;
        protected DevExpress.XtraEditors.LabelControl lblPhone;
        protected DevExpress.XtraEditors.TextEdit textEditFirstName;
        protected DevExpress.XtraEditors.LabelControl lblName;
        protected DevExpress.XtraEditors.TimeEdit tEditKitchen;
        protected DevExpress.XtraEditors.TimeEdit tEditClient;
        protected DevExpress.XtraEditors.LabelControl lblTimeClient;
        protected DevExpress.XtraEditors.LabelControl lblTimeKitchen;
        protected DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        protected DevExpress.XtraEditors.ListBoxControl lstCategory;
        protected DevExpress.XtraEditors.ListBoxControl lstDishListIDs;
        protected DevExpress.XtraEditors.ListBoxControl lstDish;
        protected DevExpress.XtraEditors.LabelControl lblOtherInfo;
        protected System.Windows.Forms.TextBox txtAdditional;
        protected DevExpress.XtraEditors.TextEdit txtPorch;
        protected DevExpress.XtraEditors.LabelControl lblPorch;
        protected DevExpress.XtraEditors.TextEdit txtAppartament;
        protected DevExpress.XtraEditors.LabelControl lblFlat;
        protected DevExpress.XtraEditors.TextEdit txtHouse;
        protected DevExpress.XtraEditors.LabelControl lblHouse;
        protected System.Windows.Forms.GroupBox gBoxReserv;
        protected System.Windows.Forms.RadioButton rbSelectedTime;
        protected System.Windows.Forms.RadioButton rbNearestTime;
        protected System.Windows.Forms.Button btnReserve;
        protected DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        protected System.Windows.Forms.RichTextBox richWebOrder;
        protected System.Windows.Forms.MaskedTextBox txtPhone;
        protected System.Windows.Forms.FlowLayoutPanel orderTypeAndReservationPanel;
        protected System.Windows.Forms.Panel panelRealTimeOrder;
        protected System.Windows.Forms.Panel panelPreOrder;
        protected DevExpress.XtraEditors.LabelControl lblDate;
        protected System.Windows.Forms.DateTimePicker datePreOrder;
        protected System.Windows.Forms.Panel panelFlags;
        protected System.Windows.Forms.CheckBox chkPreOrder;
        protected DevExpress.XtraEditors.LabelControl lblTime;
        protected DevExpress.XtraEditors.TimeEdit tEditPreOrder;
        protected DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        protected DevExpress.XtraEditors.LabelControl lblCardStatus;
        protected DevExpress.XtraEditors.LabelControl lblCard;
        protected System.Windows.Forms.NumericUpDown numCardNumber;
        protected System.Windows.Forms.Button reCalculate;
        protected System.Windows.Forms.CheckBox chkIsWindow;
        private System.Windows.Forms.ToolTip toolTipPaymentMethod;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Panel panelPaymentMethod;
        private System.Windows.Forms.ComboBox paymentMethodList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label discountLabel;
        private System.Windows.Forms.RadioButton secondDayRadioButton;
        private System.Windows.Forms.RadioButton firstDayRadioButton;
        private DevExpress.XtraEditors.MRUEdit mruEditStreets;
        private System.Windows.Forms.RadioButton nonRadioButton;
        private System.Windows.Forms.RadioButton isPaidRadioButton;
        private System.Windows.Forms.RadioButton is5kRadioButton;
        private System.Windows.Forms.RadioButton isUnCashRadioButton;
        protected System.Windows.Forms.CheckBox chkNeedDelivery;
        private System.Windows.Forms.ComboBox personComboBox;
        protected DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Timer enabledTimer;
        private System.Windows.Forms.CheckBox fastDriverCheckBox;
        private System.Windows.Forms.Button callButton;
        protected DevExpress.XtraEditors.ListBoxControl favDishesListBoxControl;
        protected DevExpress.XtraEditors.ListBoxControl favDishesIdListBoxControl;
        private System.Windows.Forms.Timer testCallTimer;
        private System.Windows.Forms.Panel offPanel;
        private System.Windows.Forms.Panel onPanel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        protected System.Windows.Forms.TextBox textBoxWebOrderId;
        protected DevExpress.XtraEditors.LabelControl labelControlEmail;
        protected DevExpress.XtraEditors.TextEdit textEditEmail;
        protected DevExpress.XtraEditors.TextEdit textEditLastName;
        protected DevExpress.XtraEditors.LabelControl labelControl2;
        protected DevExpress.XtraEditors.LabelControl labelControl3;
        protected DevExpress.XtraEditors.TextEdit textEditSecondName;
        private System.Windows.Forms.RadioButton radioButtonFemale;
        private System.Windows.Forms.RadioButton radioButtonMale;
        private System.Windows.Forms.DateTimePicker dateTimePickerBirthDate;
        protected DevExpress.XtraEditors.LabelControl labelControlBirthDate;
        protected DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.ComboBox comboBoxPointOfSale;
        private System.Windows.Forms.Button buttonFillFromPhone;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox textBoxCouponCode;
        private System.Windows.Forms.Label labelCoupon;
        private System.Windows.Forms.Label labelCouponInfo;
        private System.Windows.Forms.Button buttonApplyCoupon;
    }
}