﻿using System.Threading.Tasks;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.WebOrders;
using LaRenzo.DataRepository.Entities.Settings;

namespace LaRenzo.Forms.Documents.DeliveryOrderForms
{
    public class DeliveryOrderFormFactory
    {
        public static async Task<DeliveryOrderForm> GetForm(DetailsFormModes mode, int orderId = 0)
        {
            var form = new DeliveryOrderForm(mode, orderId);
            await form.Initialize();
            return form;
        }

		public static async Task<DeliveryOrderForm> GetForm(PreOrder preOrder, Brand brand, DetailsFormModes mode = DetailsFormModes.NewOrderFromPreOrder)
        {
            var form = new DeliveryOrderForm(preOrder, brand, mode == DetailsFormModes.EditOrder);
            await form.Initialize();
            return form;
        }

        public static async Task<DeliveryOrderForm> GetForm(WebOrder webOrder, Brand brand, DetailsFormModes mode = DetailsFormModes.NewOrder)
        {
            var form = new DeliveryOrderFormWebOrder(webOrder, mode, brand);
            await form.Initialize();
            return form;
        }
    }
}
