﻿namespace LaRenzo.Forms.Documents.CafeOrderForms
{
    partial class CafePlaceControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainGridControl1 = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.NameCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AmountCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PriceCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CostCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnTableState = new DevExpress.XtraEditors.SimpleButton();
            this.onlineRadioButton = new System.Windows.Forms.RadioButton();
            this.cardRadioButton = new System.Windows.Forms.RadioButton();
            this.moneyRadioButton = new System.Windows.Forms.RadioButton();
            this.numDiscountCard = new System.Windows.Forms.NumericUpDown();
            this.lblDiscountCard = new System.Windows.Forms.Label();
            this.btnSelectTable = new System.Windows.Forms.Button();
            this.txtTable = new System.Windows.Forms.TextBox();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.dtnDocDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.personNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.rtxtComment = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxPointOfSale = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDiscountCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personNumericUpDown)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGridControl1
            // 
            this.mainGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl1.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl1.MainView = this.mainGridView;
            this.mainGridControl1.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl1.Name = "mainGridControl1";
            this.mainGridControl1.Size = new System.Drawing.Size(652, 495);
            this.mainGridControl1.TabIndex = 3;
            this.mainGridControl1.Tag = "1";
            this.mainGridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.NameCol,
            this.AmountCol,
            this.PriceCol,
            this.CostCol});
            this.mainGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.mainGridView.GridControl = this.mainGridControl1;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
            this.mainGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.mainGridView_PopupMenuShowing);
            this.mainGridView.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.mainGridView_CustomSummaryCalculate);
            this.mainGridView.DoubleClick += new System.EventHandler(this.mainGridView_DoubleClick);
            // 
            // NameCol
            // 
            this.NameCol.Caption = "Название";
            this.NameCol.FieldName = "Dish.Name";
            this.NameCol.Name = "NameCol";
            this.NameCol.OptionsColumn.AllowEdit = false;
            this.NameCol.OptionsColumn.AllowFocus = false;
            this.NameCol.OptionsColumn.ReadOnly = true;
            this.NameCol.Visible = true;
            this.NameCol.VisibleIndex = 0;
            this.NameCol.Width = 594;
            // 
            // AmountCol
            // 
            this.AmountCol.Caption = "Количество";
            this.AmountCol.FieldName = "Amount";
            this.AmountCol.Name = "AmountCol";
            this.AmountCol.OptionsColumn.AllowEdit = false;
            this.AmountCol.OptionsColumn.AllowFocus = false;
            this.AmountCol.OptionsColumn.ReadOnly = true;
            this.AmountCol.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.AmountCol.Visible = true;
            this.AmountCol.VisibleIndex = 1;
            this.AmountCol.Width = 349;
            // 
            // PriceCol
            // 
            this.PriceCol.Caption = "Цена";
            this.PriceCol.FieldName = "Price";
            this.PriceCol.Name = "PriceCol";
            this.PriceCol.OptionsColumn.AllowEdit = false;
            this.PriceCol.OptionsColumn.AllowFocus = false;
            this.PriceCol.OptionsColumn.ReadOnly = true;
            this.PriceCol.Visible = true;
            this.PriceCol.VisibleIndex = 2;
            this.PriceCol.Width = 349;
            // 
            // CostCol
            // 
            this.CostCol.Caption = "Сумма";
            this.CostCol.DisplayFormat.FormatString = "c2 руб.";
            this.CostCol.FieldName = "Sum";
            this.CostCol.Name = "CostCol";
            this.CostCol.OptionsColumn.AllowEdit = false;
            this.CostCol.OptionsColumn.AllowFocus = false;
            this.CostCol.OptionsColumn.ReadOnly = true;
            this.CostCol.ShowUnboundExpressionMenu = true;
            this.CostCol.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom)});
            this.CostCol.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.CostCol.Visible = true;
            this.CostCol.VisibleIndex = 3;
            this.CostCol.Width = 358;
            // 
            // btnTableState
            // 
            this.btnTableState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTableState.Location = new System.Drawing.Point(27, 19);
            this.btnTableState.Margin = new System.Windows.Forms.Padding(4);
            this.btnTableState.Name = "btnTableState";
            this.btnTableState.Size = new System.Drawing.Size(337, 38);
            this.btnTableState.TabIndex = 1012;
            this.btnTableState.Text = "Закрыть столик";
            this.btnTableState.Visible = false;
            this.btnTableState.Click += new System.EventHandler(this.btnTableState_Click);
            // 
            // onlineRadioButton
            // 
            this.onlineRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.onlineRadioButton.AutoSize = true;
            this.onlineRadioButton.Location = new System.Drawing.Point(243, 68);
            this.onlineRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.onlineRadioButton.Name = "onlineRadioButton";
            this.onlineRadioButton.Size = new System.Drawing.Size(119, 21);
            this.onlineRadioButton.TabIndex = 1038;
            this.onlineRadioButton.Text = "Безналичный";
            this.onlineRadioButton.UseVisualStyleBackColor = true;
            // 
            // cardRadioButton
            // 
            this.cardRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cardRadioButton.AutoSize = true;
            this.cardRadioButton.Location = new System.Drawing.Point(147, 68);
            this.cardRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.cardRadioButton.Name = "cardRadioButton";
            this.cardRadioButton.Size = new System.Drawing.Size(69, 21);
            this.cardRadioButton.TabIndex = 1037;
            this.cardRadioButton.Text = "Карта";
            this.cardRadioButton.UseVisualStyleBackColor = true;
            // 
            // moneyRadioButton
            // 
            this.moneyRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.moneyRadioButton.AutoSize = true;
            this.moneyRadioButton.Checked = true;
            this.moneyRadioButton.Location = new System.Drawing.Point(27, 68);
            this.moneyRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.moneyRadioButton.Name = "moneyRadioButton";
            this.moneyRadioButton.Size = new System.Drawing.Size(97, 21);
            this.moneyRadioButton.TabIndex = 1036;
            this.moneyRadioButton.TabStop = true;
            this.moneyRadioButton.Text = "Наличные";
            this.moneyRadioButton.UseVisualStyleBackColor = true;
            // 
            // numDiscountCard
            // 
            this.numDiscountCard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numDiscountCard.Location = new System.Drawing.Point(559, 66);
            this.numDiscountCard.Margin = new System.Windows.Forms.Padding(4);
            this.numDiscountCard.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numDiscountCard.Name = "numDiscountCard";
            this.numDiscountCard.Size = new System.Drawing.Size(69, 22);
            this.numDiscountCard.TabIndex = 1035;
            this.numDiscountCard.ValueChanged += new System.EventHandler(this.numDiscountCard_ValueChanged);
            this.numDiscountCard.Leave += new System.EventHandler(this.numDiscountCard_Leave);
            // 
            // lblDiscountCard
            // 
            this.lblDiscountCard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDiscountCard.AutoSize = true;
            this.lblDiscountCard.Location = new System.Drawing.Point(499, 67);
            this.lblDiscountCard.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDiscountCard.Name = "lblDiscountCard";
            this.lblDiscountCard.Size = new System.Drawing.Size(52, 17);
            this.lblDiscountCard.TabIndex = 1034;
            this.lblDiscountCard.Text = "Карта:";
            // 
            // btnSelectTable
            // 
            this.btnSelectTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectTable.Enabled = false;
            this.btnSelectTable.Location = new System.Drawing.Point(797, 59);
            this.btnSelectTable.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelectTable.Name = "btnSelectTable";
            this.btnSelectTable.Size = new System.Drawing.Size(47, 30);
            this.btnSelectTable.TabIndex = 1033;
            this.btnSelectTable.Text = "...";
            this.btnSelectTable.UseVisualStyleBackColor = true;
            // 
            // txtTable
            // 
            this.txtTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtTable.Enabled = false;
            this.txtTable.Location = new System.Drawing.Point(703, 66);
            this.txtTable.Margin = new System.Windows.Forms.Padding(4);
            this.txtTable.Name = "txtTable";
            this.txtTable.Size = new System.Drawing.Size(87, 22);
            this.txtTable.TabIndex = 1032;
            this.txtTable.Tag = "Table";
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(636, 68);
            this.lblWarehouse.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(59, 17);
            this.lblWarehouse.TabIndex = 1031;
            this.lblWarehouse.Text = "Столик:";
            // 
            // txtNumber
            // 
            this.txtNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtNumber.Enabled = false;
            this.txtNumber.Location = new System.Drawing.Point(435, 25);
            this.txtNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(159, 22);
            this.txtNumber.TabIndex = 1027;
            this.txtNumber.Tag = "code";
            // 
            // dtnDocDate
            // 
            this.dtnDocDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dtnDocDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtnDocDate.Enabled = false;
            this.dtnDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtnDocDate.Location = new System.Drawing.Point(669, 25);
            this.dtnDocDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtnDocDate.Name = "dtnDocDate";
            this.dtnDocDate.Size = new System.Drawing.Size(175, 22);
            this.dtnDocDate.TabIndex = 1028;
            this.dtnDocDate.Tag = "Created";
            // 
            // lblDate
            // 
            this.lblDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(635, 30);
            this.lblDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(27, 17);
            this.lblDate.TabIndex = 1029;
            this.lblDate.Text = "от:";
            // 
            // lblNumber
            // 
            this.lblNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(369, 29);
            this.lblNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(55, 17);
            this.lblNumber.TabIndex = 1030;
            this.lblNumber.Text = "Номер:";
            // 
            // personNumericUpDown
            // 
            this.personNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.personNumericUpDown.Location = new System.Drawing.Point(435, 64);
            this.personNumericUpDown.Margin = new System.Windows.Forms.Padding(4);
            this.personNumericUpDown.Name = "personNumericUpDown";
            this.personNumericUpDown.Size = new System.Drawing.Size(57, 22);
            this.personNumericUpDown.TabIndex = 1040;
            this.personNumericUpDown.ValueChanged += new System.EventHandler(this.personNumericUpDown_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(369, 67);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 1039;
            this.label1.Text = "Персон:";
            // 
            // rtxtComment
            // 
            this.rtxtComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtxtComment.Location = new System.Drawing.Point(0, 0);
            this.rtxtComment.Margin = new System.Windows.Forms.Padding(4);
            this.rtxtComment.Name = "rtxtComment";
            this.rtxtComment.Size = new System.Drawing.Size(193, 495);
            this.rtxtComment.TabIndex = 1041;
            this.rtxtComment.Tag = "Comment";
            this.rtxtComment.Text = "";
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(145, 148);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(144, 24);
            this.toolStripMenuItem1.Text = "В место 1";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(144, 24);
            this.toolStripMenuItem2.Text = "В место 2";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(144, 24);
            this.toolStripMenuItem3.Text = "В место 3";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(144, 24);
            this.toolStripMenuItem4.Text = "В место 4";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(144, 24);
            this.toolStripMenuItem5.Text = "В место 5";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(144, 24);
            this.toolStripMenuItem6.Text = "В место 6";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.labelControl4);
            this.splitContainer1.Panel2.Controls.Add(this.comboBoxPointOfSale);
            this.splitContainer1.Panel2.Controls.Add(this.btnTableState);
            this.splitContainer1.Panel2.Controls.Add(this.lblDiscountCard);
            this.splitContainer1.Panel2.Controls.Add(this.lblNumber);
            this.splitContainer1.Panel2.Controls.Add(this.btnSelectTable);
            this.splitContainer1.Panel2.Controls.Add(this.personNumericUpDown);
            this.splitContainer1.Panel2.Controls.Add(this.numDiscountCard);
            this.splitContainer1.Panel2.Controls.Add(this.lblDate);
            this.splitContainer1.Panel2.Controls.Add(this.txtTable);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.moneyRadioButton);
            this.splitContainer1.Panel2.Controls.Add(this.dtnDocDate);
            this.splitContainer1.Panel2.Controls.Add(this.lblWarehouse);
            this.splitContainer1.Panel2.Controls.Add(this.onlineRadioButton);
            this.splitContainer1.Panel2.Controls.Add(this.cardRadioButton);
            this.splitContainer1.Panel2.Controls.Add(this.txtNumber);
            this.splitContainer1.Panel2MinSize = 100;
            this.splitContainer1.Size = new System.Drawing.Size(851, 601);
            this.splitContainer1.SplitterDistance = 497;
            this.splitContainer1.TabIndex = 1042;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.mainGridControl1);
            this.splitContainer2.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer2.Panel1MinSize = 400;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.rtxtComment);
            this.splitContainer2.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer2.Panel2MinSize = 150;
            this.splitContainer2.Size = new System.Drawing.Size(849, 495);
            this.splitContainer2.SplitterDistance = 652;
            this.splitContainer2.TabIndex = 0;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(407, 15);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(84, 16);
            this.labelControl4.TabIndex = 2046;
            this.labelControl4.Text = "Точка продаж";
            // 
            // comboBoxPointOfSale
            // 
            this.comboBoxPointOfSale.FormattingEnabled = true;
            this.comboBoxPointOfSale.Location = new System.Drawing.Point(511, 11);
            this.comboBoxPointOfSale.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxPointOfSale.Name = "comboBoxPointOfSale";
            this.comboBoxPointOfSale.Size = new System.Drawing.Size(333, 24);
            this.comboBoxPointOfSale.TabIndex = 2045;
            // 
            // CafePlaceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CafePlaceControl";
            this.Size = new System.Drawing.Size(851, 601);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDiscountCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personNumericUpDown)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl mainGridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn NameCol;
        private DevExpress.XtraGrid.Columns.GridColumn AmountCol;
        private DevExpress.XtraGrid.Columns.GridColumn PriceCol;
        private DevExpress.XtraGrid.Columns.GridColumn CostCol;
        private DevExpress.XtraEditors.SimpleButton btnTableState;
        private System.Windows.Forms.RadioButton onlineRadioButton;
        private System.Windows.Forms.RadioButton cardRadioButton;
        private System.Windows.Forms.RadioButton moneyRadioButton;
        private System.Windows.Forms.NumericUpDown numDiscountCard;
        private System.Windows.Forms.Label lblDiscountCard;
        private System.Windows.Forms.Button btnSelectTable;
        private System.Windows.Forms.TextBox txtTable;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.DateTimePicker dtnDocDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.NumericUpDown personNumericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtxtComment;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        protected DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.ComboBox comboBoxPointOfSale;
    }
}
