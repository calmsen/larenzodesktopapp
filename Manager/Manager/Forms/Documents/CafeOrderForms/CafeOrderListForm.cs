﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.CafeOrderForms
{
    public partial class CafeOrderListForm : Form
    {
        #region Ctor

        public CafeOrderListForm()
        {
            InitializeComponent();
            dateFrom.Value = DateTime.Now.AddDays(-30);
            mainGridView.OptionsBehavior.AlignGroupSummaryInGroupRow = DefaultBoolean.True;
        }

        #endregion Ctor

        #region Private Methods

        private void LoadOrderDocList()
        {
            mainGridControl.DataSource = Content.CafeOrderDocumentRepository.GetItems(dateFrom.Value.Date, dateTo.Value.AddDays(1).Date);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            GetDocumentForm(false, -1);
        }

        private void GetDocumentForm(bool isEdit, int tableId)
        {
            var table = Content.CafeOrderDocumentRepository.GetItem(new CafeOrderDocumentFilter
                {
                    DocumentID = tableId,
                    IsIDFilter = true
                });

            var caffeOrderForm = new CafeOrderForm(table);

            if (caffeOrderForm.ShowDialog() == DialogResult.OK)
            {
                UpdateDataGrid();
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (CafeOrderDocument) mainGridView.GetFocusedRow();

            if (selectedRow != null)
            {
                GetDocumentForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.DocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (CafeOrderDocument)mainGridView.GetFocusedRow();

            if (selectedRow != null)
            {
                GetDocumentForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.DocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.DeleteDocumentQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var selectedRow = (CafeOrderDocument)mainGridView.GetFocusedRow();

                if (selectedRow != null)
                {
                    Content.CafeOrderDocumentRepository.DeleteItem(new CafeOrderDocument {ID = selectedRow.ID});
                    UpdateDataGrid();
                }
            }
        }

        private void UpdateDataGrid()
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            LoadOrderDocList();
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateDataGrid();
        }

        private void CafeOrderListForm_Load(object sender, EventArgs e)
        {
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (CafeOrderDocument) view.GetRow(e.RowHandle);

            if (currentRow.IsMarkToDelete)
                e.Appearance.BackColor = Color.Salmon;

            if (currentRow.IsProcessed)
                e.Appearance.BackColor = Color.LightGreen;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView)sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            var info = view.CalcHitInfo(pt);

            if (view.IsDataRow(info.RowHandle))
            {
                var tableId = (int)view.GetRowCellValue(info.RowHandle, columnId);
                GetDocumentForm(true, tableId);
            }
        }

        #endregion Private Methods

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }

        private void mainGridView_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column.Name == "columnItemsAmount")
            {
                var order = e.Row as CafeOrderDocument;
                if (order != null) e.Value = order.CafeOrderItems.Sum(x => x.Amount);
            }
            else if (e.Column.Name == "columnDishesAmount")
            {
                var order = e.Row as CafeOrderDocument;
                if (order != null) e.Value = order.CafeOrderItems
                        .Where(x => x.Dish.CategoryID != 9 && x.Dish.CategoryID != 155 && x.Dish.CategoryID != 75)
                        .Sum(x => x.Amount);
            }
            else if (e.Column.Name == "columnDrinksAmount")
            {
                var order = e.Row as CafeOrderDocument;
                if (order != null) e.Value = order.CafeOrderItems
                        .Where(x => x.Dish.CategoryID == 9 || x.Dish.CategoryID == 155 || x.Dish.CategoryID == 75)
                        .Sum(x => x.Amount);
            }
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            LoadOrderDocList();
        }
    }
}
