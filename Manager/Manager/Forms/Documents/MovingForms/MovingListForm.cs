﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.Moving;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Moving;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.MovingForms
{
    public partial class MovingListForm : Form
    {
        public MovingListForm()
        {
            InitializeComponent();
            LoadMovingList(new object(), new EventArgs());
        }

        private void LoadMovingList(object sender, EventArgs args)
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            mainGridControl.DataSource = Content.MovingManager.GetMovingDocuments();
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var movingForm = new MovingForm();
            movingForm.FormClosed += UpdateDataGrid;
            movingForm.Show();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateDataGrid(sender, e);
        }

        private void UpdateDataGrid(object sender, EventArgs e)
        {
            LoadMovingList(sender, e);
            
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (MovingDocument) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetMovingForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.MovingDocIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void GetMovingForm(bool isEdit, int returnId)
        {
            var movingDocument = Content.MovingManager.GetMovingDocument(returnId);

            var movingForm = new MovingForm(isEdit, movingDocument);

            movingForm.FormClosed += LoadMovingList;
            movingForm.Show();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (MovingDocument) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetMovingForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.MovingDocIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var selectedRow = (MovingDocument) mainGridView.GetFocusedRow();

            if (selectedRow == null)
            {
                MessageBox.Show(Resources.MovingDocIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            string msg = selectedRow.IsMarkToDelete
                         ? Resources.UnMarkDeleteDocument
                         : Resources.MarkDeleteDocument;

            if (MessageBox.Show(msg, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Content.MovingManager.DeleteMovingDocument(selectedRow);
                LoadMovingList(sender, e);
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            var info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var documentId = (int)view.GetRowCellValue(info.RowHandle, columnNumber);
                GetMovingForm(true, documentId);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (MovingDocument) view.GetRow(e.RowHandle);

            if (currentRow.IsMarkToDelete)
                e.Appearance.BackColor = Color.Salmon;

            if (currentRow.IsProcessed)
                e.Appearance.BackColor = Color.LightGreen;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }

        private void MovingListForm_Load(object sender, EventArgs e)
        {
            
        }
    }
}