﻿namespace LaRenzo.Forms.Documents.MovingForms
{
    partial class MovingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MovingForm));
            this.lblNumber = new System.Windows.Forms.Label();
            this.columnBaseMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.spltMainCenter = new System.Windows.Forms.SplitContainer();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.fromWarehouseComboBox = new System.Windows.Forms.ComboBox();
            this.toWarehouseComboBox = new System.Windows.Forms.ComboBox();
            this.btnSelectWarehouse = new System.Windows.Forms.Button();
            this.btnSelectWarehouse2 = new System.Windows.Forms.Button();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.dtnDocDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.lblWarehouse2 = new System.Windows.Forms.Label();
            this.spltRight = new System.Windows.Forms.SplitContainer();
            this.richOtherInfo = new System.Windows.Forms.RichTextBox();
            this.lstProduct = new System.Windows.Forms.ListBox();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.IdCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.NameCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListCategories = new DevExpress.XtraTreeList.TreeList();
            this.spltLeft = new System.Windows.Forms.SplitContainer();
            this.spltMain = new System.Windows.Forms.SplitContainer();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spltMainCenter)).BeginInit();
            this.spltMainCenter.Panel1.SuspendLayout();
            this.spltMainCenter.Panel2.SuspendLayout();
            this.spltMainCenter.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltRight)).BeginInit();
            this.spltRight.Panel1.SuspendLayout();
            this.spltRight.Panel2.SuspendLayout();
            this.spltRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spltLeft)).BeginInit();
            this.spltLeft.Panel1.SuspendLayout();
            this.spltLeft.Panel2.SuspendLayout();
            this.spltLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltMain)).BeginInit();
            this.spltMain.Panel1.SuspendLayout();
            this.spltMain.Panel2.SuspendLayout();
            this.spltMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printableComponentLink.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(71, 18);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(44, 13);
            this.lblNumber.TabIndex = 46;
            this.lblNumber.Text = "Номер:";
            // 
            // columnBaseMeasure
            // 
            this.columnBaseMeasure.Caption = "Ед. Изм.";
            this.columnBaseMeasure.FieldName = "Product.Measure.Name";
            this.columnBaseMeasure.Name = "columnBaseMeasure";
            this.columnBaseMeasure.OptionsColumn.AllowEdit = false;
            this.columnBaseMeasure.OptionsColumn.ReadOnly = true;
            this.columnBaseMeasure.Visible = true;
            this.columnBaseMeasure.VisibleIndex = 2;
            this.columnBaseMeasure.Width = 70;
            // 
            // columnAmmount
            // 
            this.columnAmmount.Caption = "Количество";
            this.columnAmmount.FieldName = "Amount";
            this.columnAmmount.Name = "columnAmmount";
            this.columnAmmount.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.columnAmmount.Visible = true;
            this.columnAmmount.VisibleIndex = 1;
            this.columnAmmount.Width = 127;
            // 
            // columnName
            // 
            this.columnName.Caption = "Название";
            this.columnName.FieldName = "Product.Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 0;
            this.columnName.Width = 216;
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnName,
            this.columnAmmount,
            this.columnBaseMeasure});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.mainGridView_CellValueChanged);
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(546, 570);
            this.mainGridControl.TabIndex = 5;
            this.mainGridControl.Tag = "1";
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            this.mainGridControl.DoubleClick += new System.EventHandler(this.mainGridControl_DoubleClick);
            // 
            // spltMainCenter
            // 
            this.spltMainCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltMainCenter.Location = new System.Drawing.Point(0, 0);
            this.spltMainCenter.Name = "spltMainCenter";
            this.spltMainCenter.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltMainCenter.Panel1
            // 
            this.spltMainCenter.Panel1.Controls.Add(this.mainGridControl);
            // 
            // spltMainCenter.Panel2
            // 
            this.spltMainCenter.Panel2.Controls.Add(this.bottomPanel);
            this.spltMainCenter.Size = new System.Drawing.Size(546, 692);
            this.spltMainCenter.SplitterDistance = 570;
            this.spltMainCenter.TabIndex = 0;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.pnlBottom);
            this.bottomPanel.Controls.Add(this.fromWarehouseComboBox);
            this.bottomPanel.Controls.Add(this.toWarehouseComboBox);
            this.bottomPanel.Controls.Add(this.btnSelectWarehouse);
            this.bottomPanel.Controls.Add(this.btnSelectWarehouse2);
            this.bottomPanel.Controls.Add(this.txtNumber);
            this.bottomPanel.Controls.Add(this.dtnDocDate);
            this.bottomPanel.Controls.Add(this.lblDate);
            this.bottomPanel.Controls.Add(this.lblWarehouse);
            this.bottomPanel.Controls.Add(this.lblWarehouse2);
            this.bottomPanel.Controls.Add(this.lblNumber);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomPanel.Location = new System.Drawing.Point(0, 0);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(546, 118);
            this.bottomPanel.TabIndex = 6;
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnPrint);
            this.pnlBottom.Controls.Add(this.btnProcess);
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 94);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(546, 24);
            this.pnlBottom.TabIndex = 55;
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(183, 0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(87, 22);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnProcess.ForeColor = System.Drawing.Color.Green;
            this.btnProcess.Location = new System.Drawing.Point(270, 0);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(100, 22);
            this.btnProcess.TabIndex = 8;
            this.btnProcess.Text = "Провести";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(370, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Записать";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(457, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // fromWarehouseComboBox
            // 
            this.fromWarehouseComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fromWarehouseComboBox.FormattingEnabled = true;
            this.fromWarehouseComboBox.Location = new System.Drawing.Point(121, 41);
            this.fromWarehouseComboBox.Name = "fromWarehouseComboBox";
            this.fromWarehouseComboBox.Size = new System.Drawing.Size(252, 21);
            this.fromWarehouseComboBox.TabIndex = 3;
            this.fromWarehouseComboBox.Tag = "WarehouseID";
            // 
            // toWarehouseComboBox
            // 
            this.toWarehouseComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toWarehouseComboBox.FormattingEnabled = true;
            this.toWarehouseComboBox.Location = new System.Drawing.Point(121, 69);
            this.toWarehouseComboBox.Name = "toWarehouseComboBox";
            this.toWarehouseComboBox.Size = new System.Drawing.Size(252, 21);
            this.toWarehouseComboBox.TabIndex = 5;
            this.toWarehouseComboBox.Tag = "PurposeWarehouseID";
            // 
            // btnSelectWarehouse
            // 
            this.btnSelectWarehouse.Location = new System.Drawing.Point(379, 42);
            this.btnSelectWarehouse.Name = "btnSelectWarehouse";
            this.btnSelectWarehouse.Size = new System.Drawing.Size(24, 20);
            this.btnSelectWarehouse.TabIndex = 4;
            this.btnSelectWarehouse.Text = "...";
            this.btnSelectWarehouse.UseVisualStyleBackColor = true;
            this.btnSelectWarehouse.Click += new System.EventHandler(this.btnSelectWarehouse_Click);
            // 
            // btnSelectWarehouse2
            // 
            this.btnSelectWarehouse2.Location = new System.Drawing.Point(379, 68);
            this.btnSelectWarehouse2.Name = "btnSelectWarehouse2";
            this.btnSelectWarehouse2.Size = new System.Drawing.Size(24, 20);
            this.btnSelectWarehouse2.TabIndex = 6;
            this.btnSelectWarehouse2.Text = "...";
            this.btnSelectWarehouse2.UseVisualStyleBackColor = true;
            this.btnSelectWarehouse2.Click += new System.EventHandler(this.btnSelectParent_Click);
            // 
            // txtNumber
            // 
            this.txtNumber.Enabled = false;
            this.txtNumber.Location = new System.Drawing.Point(121, 15);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(120, 20);
            this.txtNumber.TabIndex = 1;
            this.txtNumber.Tag = "id";
            // 
            // dtnDocDate
            // 
            this.dtnDocDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtnDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtnDocDate.Location = new System.Drawing.Point(271, 15);
            this.dtnDocDate.Name = "dtnDocDate";
            this.dtnDocDate.Size = new System.Drawing.Size(132, 20);
            this.dtnDocDate.TabIndex = 2;
            this.dtnDocDate.Tag = "DocumentDate";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(244, 15);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(21, 13);
            this.lblDate.TabIndex = 42;
            this.lblDate.Text = "от:";
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(7, 44);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(108, 13);
            this.lblWarehouse.TabIndex = 44;
            this.lblWarehouse.Text = "Склад отправитель:";
            // 
            // lblWarehouse2
            // 
            this.lblWarehouse2.AutoSize = true;
            this.lblWarehouse2.Location = new System.Drawing.Point(14, 72);
            this.lblWarehouse2.Name = "lblWarehouse2";
            this.lblWarehouse2.Size = new System.Drawing.Size(101, 13);
            this.lblWarehouse2.TabIndex = 45;
            this.lblWarehouse2.Text = "Склад получатель:";
            // 
            // spltRight
            // 
            this.spltRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltRight.Location = new System.Drawing.Point(0, 0);
            this.spltRight.Name = "spltRight";
            // 
            // spltRight.Panel1
            // 
            this.spltRight.Panel1.Controls.Add(this.spltMainCenter);
            // 
            // spltRight.Panel2
            // 
            this.spltRight.Panel2.Controls.Add(this.richOtherInfo);
            this.spltRight.Size = new System.Drawing.Size(806, 692);
            this.spltRight.SplitterDistance = 546;
            this.spltRight.TabIndex = 0;
            // 
            // richOtherInfo
            // 
            this.richOtherInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richOtherInfo.Location = new System.Drawing.Point(0, 0);
            this.richOtherInfo.Name = "richOtherInfo";
            this.richOtherInfo.Size = new System.Drawing.Size(256, 692);
            this.richOtherInfo.TabIndex = 0;
            this.richOtherInfo.Tag = "Note";
            this.richOtherInfo.Text = "Дополнительная информация:";
            // 
            // lstProduct
            // 
            this.lstProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstProduct.FormattingEnabled = true;
            this.lstProduct.Location = new System.Drawing.Point(0, 0);
            this.lstProduct.Name = "lstProduct";
            this.lstProduct.Size = new System.Drawing.Size(211, 692);
            this.lstProduct.TabIndex = 0;
            this.lstProduct.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstProduct_MouseDoubleClick);
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "Deleket-Sleek-Xp-Basic-Folder.ico");
            // 
            // IdCol
            // 
            this.IdCol.Caption = "ID";
            this.IdCol.FieldName = "ID";
            this.IdCol.Name = "IdCol";
            // 
            // NameCol
            // 
            this.NameCol.Caption = "Категории";
            this.NameCol.FieldName = "Name";
            this.NameCol.MinWidth = 37;
            this.NameCol.Name = "NameCol";
            this.NameCol.Visible = true;
            this.NameCol.VisibleIndex = 0;
            // 
            // treeListCategories
            // 
            this.treeListCategories.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.NameCol,
            this.IdCol});
            this.treeListCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListCategories.DragNodesMode = DevExpress.XtraTreeList.TreeListDragNodesMode.Standard;
            this.treeListCategories.Location = new System.Drawing.Point(0, 0);
            this.treeListCategories.Name = "treeListCategories";
            this.treeListCategories.OptionsBehavior.DragNodes = true;
            this.treeListCategories.OptionsBehavior.Editable = false;
            this.treeListCategories.OptionsBehavior.EnableFiltering = true;
            this.treeListCategories.ParentFieldName = "ParentCategoryID";
            this.treeListCategories.SelectImageList = this.imgList;
            this.treeListCategories.Size = new System.Drawing.Size(191, 692);
            this.treeListCategories.TabIndex = 0;
            this.treeListCategories.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListCategories_FocusedNodeChanged);
            // 
            // spltLeft
            // 
            this.spltLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltLeft.Location = new System.Drawing.Point(0, 0);
            this.spltLeft.Name = "spltLeft";
            // 
            // spltLeft.Panel1
            // 
            this.spltLeft.Panel1.Controls.Add(this.treeListCategories);
            // 
            // spltLeft.Panel2
            // 
            this.spltLeft.Panel2.Controls.Add(this.lstProduct);
            this.spltLeft.Size = new System.Drawing.Size(406, 692);
            this.spltLeft.SplitterDistance = 191;
            this.spltLeft.TabIndex = 0;
            // 
            // spltMain
            // 
            this.spltMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltMain.Location = new System.Drawing.Point(0, 0);
            this.spltMain.Name = "spltMain";
            // 
            // spltMain.Panel1
            // 
            this.spltMain.Panel1.Controls.Add(this.spltLeft);
            // 
            // spltMain.Panel2
            // 
            this.spltMain.Panel2.Controls.Add(this.spltRight);
            this.spltMain.Size = new System.Drawing.Size(1216, 692);
            this.spltMain.SplitterDistance = 406;
            this.spltMain.TabIndex = 2;
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            // 
            // 
            // 
            this.printableComponentLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("printableComponentLink.ImageCollection.ImageStream")));
            this.printableComponentLink.Landscape = true;
            this.printableComponentLink.Margins = new System.Drawing.Printing.Margins(100, 100, 76, 100);
            this.printableComponentLink.MinMargins = new System.Drawing.Printing.Margins(20, 20, 50, 20);
            this.printableComponentLink.PrintingSystem = this.printingSystem;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateMarginalHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateMarginalHeaderArea);
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // MovingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 692);
            this.Controls.Add(this.spltMain);
            this.Name = "MovingForm";
            this.Text = "Документ перемещения";
            this.Load += new System.EventHandler(this.MovingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            this.spltMainCenter.Panel1.ResumeLayout(false);
            this.spltMainCenter.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltMainCenter)).EndInit();
            this.spltMainCenter.ResumeLayout(false);
            this.bottomPanel.ResumeLayout(false);
            this.bottomPanel.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.spltRight.Panel1.ResumeLayout(false);
            this.spltRight.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltRight)).EndInit();
            this.spltRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListCategories)).EndInit();
            this.spltLeft.Panel1.ResumeLayout(false);
            this.spltLeft.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltLeft)).EndInit();
            this.spltLeft.ResumeLayout(false);
            this.spltMain.Panel1.ResumeLayout(false);
            this.spltMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltMain)).EndInit();
            this.spltMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printableComponentLink.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblNumber;
        private DevExpress.XtraGrid.Columns.GridColumn columnBaseMeasure;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private System.Windows.Forms.SplitContainer spltMainCenter;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox fromWarehouseComboBox;
        private System.Windows.Forms.ComboBox toWarehouseComboBox;
        private System.Windows.Forms.Button btnSelectWarehouse;
        private System.Windows.Forms.Button btnSelectWarehouse2;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.DateTimePicker dtnDocDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Label lblWarehouse2;
        private System.Windows.Forms.SplitContainer spltRight;
        private System.Windows.Forms.RichTextBox richOtherInfo;
        private System.Windows.Forms.ListBox lstProduct;
        private System.Windows.Forms.ImageList imgList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn IdCol;
        private DevExpress.XtraTreeList.Columns.TreeListColumn NameCol;
        private DevExpress.XtraTreeList.TreeList treeListCategories;
        private System.Windows.Forms.SplitContainer spltLeft;
        private System.Windows.Forms.SplitContainer spltMain;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;

    }
}