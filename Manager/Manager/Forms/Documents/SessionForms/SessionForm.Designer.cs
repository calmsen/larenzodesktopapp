﻿namespace LaRenzo.Forms.Documents.SessionForms
{
    partial class SessionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionForm));
            this.spltRight = new System.Windows.Forms.SplitContainer();
            this.spltMainCenter = new System.Windows.Forms.SplitContainer();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.dtnCloseDocDate = new System.Windows.Forms.DateTimePicker();
            this.dtnOpenDocDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.richOtherInfo = new System.Windows.Forms.RichTextBox();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.spltRight)).BeginInit();
            this.spltRight.Panel1.SuspendLayout();
            this.spltRight.Panel2.SuspendLayout();
            this.spltRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltMainCenter)).BeginInit();
            this.spltMainCenter.Panel1.SuspendLayout();
            this.spltMainCenter.Panel2.SuspendLayout();
            this.spltMainCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.bottomPanel.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printableComponentLink.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // spltRight
            // 
            this.spltRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltRight.Location = new System.Drawing.Point(0, 0);
            this.spltRight.Name = "spltRight";
            // 
            // spltRight.Panel1
            // 
            this.spltRight.Panel1.Controls.Add(this.spltMainCenter);
            // 
            // spltRight.Panel2
            // 
            this.spltRight.Panel2.Controls.Add(this.richOtherInfo);
            this.spltRight.Size = new System.Drawing.Size(1216, 692);
            this.spltRight.SplitterDistance = 982;
            this.spltRight.TabIndex = 1;
            // 
            // spltMainCenter
            // 
            this.spltMainCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltMainCenter.Location = new System.Drawing.Point(0, 0);
            this.spltMainCenter.Name = "spltMainCenter";
            this.spltMainCenter.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltMainCenter.Panel1
            // 
            this.spltMainCenter.Panel1.Controls.Add(this.mainGridControl);
            // 
            // spltMainCenter.Panel2
            // 
            this.spltMainCenter.Panel2.Controls.Add(this.bottomPanel);
            this.spltMainCenter.Size = new System.Drawing.Size(982, 692);
            this.spltMainCenter.SplitterDistance = 623;
            this.spltMainCenter.TabIndex = 0;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(982, 623);
            this.mainGridControl.TabIndex = 5;
            this.mainGridControl.Tag = "1";
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnName,
            this.columnAmmount,
            this.columnPrice,
            this.columnSum});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.mainGridView_RowCellStyle);
            // 
            // columnName
            // 
            this.columnName.Caption = "Название";
            this.columnName.FieldName = "Dish.Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 0;
            this.columnName.Width = 216;
            // 
            // columnAmmount
            // 
            this.columnAmmount.Caption = "Количество";
            this.columnAmmount.FieldName = "Amount";
            this.columnAmmount.Name = "columnAmmount";
            this.columnAmmount.OptionsColumn.AllowEdit = false;
            this.columnAmmount.OptionsColumn.ReadOnly = true;
            this.columnAmmount.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.columnAmmount.Visible = true;
            this.columnAmmount.VisibleIndex = 1;
            this.columnAmmount.Width = 127;
            // 
            // columnPrice
            // 
            this.columnPrice.Caption = "Цена";
            this.columnPrice.FieldName = "Dish.Price";
            this.columnPrice.Name = "columnPrice";
            this.columnPrice.OptionsColumn.AllowEdit = false;
            this.columnPrice.OptionsColumn.ReadOnly = true;
            this.columnPrice.Visible = true;
            this.columnPrice.VisibleIndex = 2;
            this.columnPrice.Width = 105;
            // 
            // columnSum
            // 
            this.columnSum.Caption = "Сумма";
            this.columnSum.DisplayFormat.FormatString = "c2 руб.";
            this.columnSum.FieldName = "Sum";
            this.columnSum.Name = "columnSum";
            this.columnSum.OptionsColumn.AllowEdit = false;
            this.columnSum.OptionsColumn.AllowFocus = false;
            this.columnSum.OptionsColumn.ReadOnly = true;
            this.columnSum.ShowUnboundExpressionMenu = true;
            this.columnSum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnSum.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.columnSum.Visible = true;
            this.columnSum.VisibleIndex = 3;
            this.columnSum.Width = 112;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.pnlBottom);
            this.bottomPanel.Controls.Add(this.txtNumber);
            this.bottomPanel.Controls.Add(this.dtnCloseDocDate);
            this.bottomPanel.Controls.Add(this.dtnOpenDocDate);
            this.bottomPanel.Controls.Add(this.label1);
            this.bottomPanel.Controls.Add(this.lblDate);
            this.bottomPanel.Controls.Add(this.lblNumber);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomPanel.Location = new System.Drawing.Point(0, 0);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(982, 65);
            this.bottomPanel.TabIndex = 6;
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnPrint);
            this.pnlBottom.Controls.Add(this.btnProcess);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 41);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(982, 24);
            this.pnlBottom.TabIndex = 55;
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(706, 0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(87, 22);
            this.btnPrint.TabIndex = 4;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnProcess.ForeColor = System.Drawing.Color.Green;
            this.btnProcess.Location = new System.Drawing.Point(793, 0);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(100, 22);
            this.btnProcess.TabIndex = 5;
            this.btnProcess.Text = "Провести";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(893, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtNumber
            // 
            this.txtNumber.Enabled = false;
            this.txtNumber.Location = new System.Drawing.Point(62, 10);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(127, 20);
            this.txtNumber.TabIndex = 1;
            this.txtNumber.Tag = "id";
            // 
            // dtnCloseDocDate
            // 
            this.dtnCloseDocDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtnCloseDocDate.Enabled = false;
            this.dtnCloseDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtnCloseDocDate.Location = new System.Drawing.Point(517, 10);
            this.dtnCloseDocDate.Name = "dtnCloseDocDate";
            this.dtnCloseDocDate.Size = new System.Drawing.Size(127, 20);
            this.dtnCloseDocDate.TabIndex = 3;
            this.dtnCloseDocDate.Tag = "Closed";
            // 
            // dtnOpenDocDate
            // 
            this.dtnOpenDocDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtnOpenDocDate.Enabled = false;
            this.dtnOpenDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtnOpenDocDate.Location = new System.Drawing.Point(289, 11);
            this.dtnOpenDocDate.Name = "dtnOpenDocDate";
            this.dtnOpenDocDate.Size = new System.Drawing.Size(127, 20);
            this.dtnOpenDocDate.TabIndex = 2;
            this.dtnOpenDocDate.Tag = "Opened";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(422, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Смена закрыта:";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(195, 13);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(88, 13);
            this.lblDate.TabIndex = 42;
            this.lblDate.Text = "Смена открыта:";
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(12, 13);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(44, 13);
            this.lblNumber.TabIndex = 46;
            this.lblNumber.Text = "Номер:";
            // 
            // richOtherInfo
            // 
            this.richOtherInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richOtherInfo.Location = new System.Drawing.Point(0, 0);
            this.richOtherInfo.Name = "richOtherInfo";
            this.richOtherInfo.Size = new System.Drawing.Size(230, 692);
            this.richOtherInfo.TabIndex = 0;
            this.richOtherInfo.Text = "Дополнительная информация:";
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            // 
            // 
            // 
            this.printableComponentLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("printableComponentLink.ImageCollection.ImageStream")));
            this.printableComponentLink.Landscape = true;
            this.printableComponentLink.Margins = new System.Drawing.Printing.Margins(100, 100, 76, 100);
            this.printableComponentLink.MinMargins = new System.Drawing.Printing.Margins(20, 20, 50, 20);
            this.printableComponentLink.PrintingSystem = this.printingSystem;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateMarginalHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateMarginalHeaderArea);
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // SessionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 692);
            this.Controls.Add(this.spltRight);
            this.Name = "SessionForm";
            this.Text = "Закрытие смены";
            this.Load += new System.EventHandler(this.SessionForm_Load);
            this.spltRight.Panel1.ResumeLayout(false);
            this.spltRight.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltRight)).EndInit();
            this.spltRight.ResumeLayout(false);
            this.spltMainCenter.Panel1.ResumeLayout(false);
            this.spltMainCenter.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltMainCenter)).EndInit();
            this.spltMainCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.bottomPanel.ResumeLayout(false);
            this.bottomPanel.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printableComponentLink.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spltRight;
        private System.Windows.Forms.SplitContainer spltMainCenter;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrice;
        private DevExpress.XtraGrid.Columns.GridColumn columnSum;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.DateTimePicker dtnOpenDocDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.RichTextBox richOtherInfo;
        private System.Windows.Forms.DateTimePicker dtnCloseDocDate;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
    }
}