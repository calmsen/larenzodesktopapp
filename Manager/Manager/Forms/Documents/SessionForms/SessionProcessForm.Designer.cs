﻿namespace LaRenzo.Forms.Documents.SessionForms
{
    partial class SessionProcessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.totalProgress = new System.Windows.Forms.ProgressBar();
			this.Info = new System.Windows.Forms.RichTextBox();
			this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
			this.openNewSessionCheckBox = new System.Windows.Forms.CheckBox();
			this.shiftOpenOrdersCheckBox = new System.Windows.Forms.CheckBox();
			this.sendPaysToBankCheckBox = new System.Windows.Forms.CheckBox();
			this.StartProcessSession = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// totalProgress
			// 
			this.totalProgress.Location = new System.Drawing.Point(29, 427);
			this.totalProgress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.totalProgress.Name = "totalProgress";
			this.totalProgress.Size = new System.Drawing.Size(453, 42);
			this.totalProgress.TabIndex = 1;
			// 
			// Info
			// 
			this.Info.Location = new System.Drawing.Point(29, 171);
			this.Info.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Info.Name = "Info";
			this.Info.Size = new System.Drawing.Size(452, 248);
			this.Info.TabIndex = 2;
			this.Info.Text = "";
			// 
			// checkBox1
			// 
			this.openNewSessionCheckBox.AutoSize = true;
			this.openNewSessionCheckBox.Checked = true;
			this.openNewSessionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.openNewSessionCheckBox.Location = new System.Drawing.Point(29, 15);
			this.openNewSessionCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.openNewSessionCheckBox.Name = "openNewSessionCheckBox";
			this.openNewSessionCheckBox.Size = new System.Drawing.Size(283, 21);
			this.openNewSessionCheckBox.TabIndex = 3;
			this.openNewSessionCheckBox.Text = "Автоматисески открыть новую сессию";
			this.openNewSessionCheckBox.UseVisualStyleBackColor = true;
			this.openNewSessionCheckBox.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// checkBox2
			// 
			this.shiftOpenOrdersCheckBox.AutoSize = true;
			this.shiftOpenOrdersCheckBox.Checked = true;
			this.shiftOpenOrdersCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.shiftOpenOrdersCheckBox.Location = new System.Drawing.Point(53, 43);
			this.shiftOpenOrdersCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.shiftOpenOrdersCheckBox.Name = "shiftOpenOrdersCheckBox";
			this.shiftOpenOrdersCheckBox.Size = new System.Drawing.Size(376, 21);
			this.shiftOpenOrdersCheckBox.TabIndex = 4;
			this.shiftOpenOrdersCheckBox.Text = "Перенести необработанные заказы в новую сессию";
			this.shiftOpenOrdersCheckBox.UseVisualStyleBackColor = true;
			// 
			// checkBox3
			// 
			this.sendPaysToBankCheckBox.AutoSize = true;
			this.sendPaysToBankCheckBox.Checked = true;
			this.sendPaysToBankCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.sendPaysToBankCheckBox.Location = new System.Drawing.Point(29, 71);
			this.sendPaysToBankCheckBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.sendPaysToBankCheckBox.Name = "sendPaysToBankCheckBox";
			this.sendPaysToBankCheckBox.Size = new System.Drawing.Size(314, 21);
			this.sendPaysToBankCheckBox.TabIndex = 5;
			this.sendPaysToBankCheckBox.Text = "Отправить платежную информацию в банк";
			this.sendPaysToBankCheckBox.UseVisualStyleBackColor = true;
			// 
			// StartProcessSession
			// 
			this.StartProcessSession.Location = new System.Drawing.Point(29, 106);
			this.StartProcessSession.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.StartProcessSession.Name = "StartProcessSession";
			this.StartProcessSession.Size = new System.Drawing.Size(453, 47);
			this.StartProcessSession.TabIndex = 6;
			this.StartProcessSession.Text = "Начать обработку сессии";
			this.StartProcessSession.UseVisualStyleBackColor = true;
			this.StartProcessSession.Click += new System.EventHandler(this.StartProcessSessionClick);
			// 
			// SessionProcessForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(512, 484);
			this.Controls.Add(this.StartProcessSession);
			this.Controls.Add(this.sendPaysToBankCheckBox);
			this.Controls.Add(this.shiftOpenOrdersCheckBox);
			this.Controls.Add(this.openNewSessionCheckBox);
			this.Controls.Add(this.Info);
			this.Controls.Add(this.totalProgress);
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Name = "SessionProcessForm";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Закрытие сессии";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SessionProcessForm_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar totalProgress;
        private System.Windows.Forms.RichTextBox Info;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.CheckBox openNewSessionCheckBox;
        private System.Windows.Forms.CheckBox shiftOpenOrdersCheckBox;
        private System.Windows.Forms.CheckBox sendPaysToBankCheckBox;
        private System.Windows.Forms.Button StartProcessSession;
    }
}