﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.SessionForms
{
    public partial class SessionListForm : Form
    {
        public SessionListForm()
        {
            InitializeComponent();
            LoadSessionList();
        }

        private void LoadSessionList()
        {
            mainGridControl.DataSource = Content.SessionManager.GetSessionDocuments();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateDataGrid(sender, e);
        }

        private void UpdateDataGrid(object sender, EventArgs e)
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            LoadSessionList();
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (Session)view.GetRow(e.RowHandle);

            if (currentRow.IsMarkToDelete)
                e.Appearance.BackColor = Color.Salmon;

            if (currentRow.IsProcessed)
                e.Appearance.BackColor = Color.LightGreen;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var selectedRow = (Session)mainGridView.GetFocusedRow();

            if (selectedRow == null)
            {
                MessageBox.Show(Resources.SessionDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string msg = selectedRow.IsMarkToDelete
                             ? Resources.UnMarkDeleteDocument
                             : Resources.MarkDeleteDocument;

            if (MessageBox.Show(msg, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Content.SessionManager.DeleteSessionDocument(selectedRow);
                UpdateDataGrid(sender, e);
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            var info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var sessionId = (int) view.GetRowCellValue(info.RowHandle, columnNumber);
                GetSessionForm(sessionId);
            }
        }

        private void GetSessionForm(int sessionId)
        {
            var sessionDocument = Content.SessionManager.GetSessionDocument(sessionId);

            if (sessionDocument == null || sessionDocument.Closed == null) return;

            var sessionForm = new SessionForm(sessionDocument);

            sessionForm.FormClosed += UpdateDataGrid;
            sessionForm.Show();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }

        private void btnPrinters_Click(object sender, EventArgs e)
        {

        }

        private void btnPrintSupplyForm_Click(object sender, EventArgs e)
        {
            var handle = mainGridView.FocusedRowHandle;
            if (mainGridView.IsDataRow(handle))
            {
                var sessionId = (int)mainGridView.GetRowCellValue(handle, columnNumber);

                var pointOfSales = Content.PointOfSaleRepository.GetItems();
                foreach (var point in pointOfSales)
                {
                    PrintingManager.EndSessionCheckPrint(Content.SessionManager.GetById(sessionId), point.ID);
                }
            }
        }
    }
}