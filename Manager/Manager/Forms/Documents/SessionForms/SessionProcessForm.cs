﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Entities.Others.TimeSheets;
using LaRenzo.DataRepository.Entities.PrintWorksModels;
using LaRenzo.DataRepository.Entities.States;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Others.Times;
using LaRenzo.Interfaces.SignalMessages;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.Documents.SessionForms
{
    public partial class SessionProcessForm : Form
    {
        /// <summary> Id для закрывающейся сессии (-1=текущую) </summary>
        private int _сloseSessionId = -1;

        private readonly Stopwatch _timer = new Stopwatch();

        public event SeesionClosedHandler SessionClosed;

        protected virtual void OnSessionClosed(Session s)
        {
            SessionClosed?.Invoke(s, EventArgs.Empty);
        }

        public delegate void SeesionClosedHandler(Session s, EventArgs e);

        public event SeesionOpenedHandler SessionOpened;

        protected virtual void OnSessionOpened(Session s)
        {
            SessionOpened?.Invoke(s, EventArgs.Empty);
        }

        public delegate void SeesionOpenedHandler(Session s, EventArgs e);


        public SessionProcessForm()
        {
            InitializeComponent();
        }


        public SessionProcessForm(int сloseSessionId)
        {
            InitializeComponent();

            // Указать сессию с которой происходит работа
            _сloseSessionId = сloseSessionId;

            // Отменить возможность переоткрытия сессии
            openNewSessionCheckBox.Checked = false;
            sendPaysToBankCheckBox.Checked = false;

            openNewSessionCheckBox.Enabled = false;
        }

        private void SessionManagerOnSessionProcessing(SessionProcessingSignalMessage message)
        {
            var totalCount = message.TotalOrders + message.TotalCafeOrders;
            var totalProcessed = message.OrdersProcessed + message.CafeOrdersProcessed;
            totalProgress.Invoke(new Action(() =>
                {
                    totalProgress.Minimum = 0;
                    totalProgress.Maximum = totalCount;
                    totalProgress.Value = totalProcessed;
                }
            ));

            var avgTime = totalProcessed == 0 ? 3 : _timer.Elapsed.TotalSeconds / totalProcessed;

            var secToFinish = avgTime * (totalCount - totalProcessed);

            var timeToFinish = TimeSpan.FromSeconds(secToFinish);

            int value;
            string measure;

            if (secToFinish < 60)
            {
                value = timeToFinish.Seconds;
                measure = "сек";
            }
            else
            {
                if (secToFinish < 1440)
                {
                    value = timeToFinish.Minutes;
                    measure = "мин";
                }
                else
                {
                    value = timeToFinish.Hours;
                    measure = "ч";
                }
            }

            Info.Invoke(new Action(() =>
            {
                Info.Text = String.Format(
                    "Позиций в заказах: {0} / {1} {2}Позиций в заказах кафе: {3} / {4} {5}Осталось времени: {6} {7} {8}Время на 1 заказ: {9:0.##} сек",
                    message.OrdersProcessed, message.TotalOrders,
                    Environment.NewLine, message.CafeOrdersProcessed, message.TotalCafeOrders,
                    Environment.NewLine, value, measure,
                    Environment.NewLine, avgTime);
            }));
            var textForStatus = String.Empty;

            switch (message.Status)
            {
                case SessionProcessingStatus.LoadingData:
                    textForStatus = "Загрузка данных из базы данных...";
                    break;
                case SessionProcessingStatus.Processing:
                    textForStatus = "Сессия обрабатывается (подготовка данных)...";
                    break;
                case SessionProcessingStatus.Saving:
                    textForStatus = string.Format("Подготовка данных завершена. {0}Сохранение...",
                                                  Environment.NewLine);
                    break;
                case SessionProcessingStatus.Done:
                    textForStatus = "Завершено";
                    break;
            }

            Info.Invoke(new Action(() =>
            {
                Info.Text += string.Format("{0}-------------{0}{0}{1}{0}{2} сек", Environment.NewLine,
                    textForStatus, _timer.Elapsed.TotalSeconds);
            }));
        }

        private void SessionProcessForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker.IsBusy)
            {
                var result = MessageBox.Show("Если вы закроете окно, процесс списания будет прерван! Закрыть окно?", "Процесс не закончен",
                                             MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    Content.SignalProcessor.UnBind<SessionProcessingSignalMessage>();
                    backgroundWorker.CancelAsync();
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Начать обработку сессии </summary>
        ////============================================
        private async void StartProcessSessionClick(object sender, EventArgs e)
        {
            // Получить выбранные пользователем параметры закрытия сессии
            var closeSessionOptions = new CloseSessionOptions
            {
                DoOpenNewSession = openNewSessionCheckBox.Checked,
                DoSendBankInteractions = sendPaysToBankCheckBox.Checked,
                DoTransferOpenOrders = shiftOpenOrdersCheckBox.Enabled && shiftOpenOrdersCheckBox.Checked
            };

            // Сессия для закрытия
            Session sessionToClose;

            // Заказы из таблицы времени
            List<TimeSheet> saveTimeSheets = null;

            // Если id закрываемой <= 0, то взять текущую, иначе поднять по id
            if (_сloseSessionId > 0)
            {
                sessionToClose = Content.SessionManager.GetById(_сloseSessionId);
            }
            
            else
            {
                sessionToClose = await Content.SessionManager.GetOpened();

                // Если надо перенести открытые заказы на таблицу времени
                if (closeSessionOptions.DoTransferOpenOrders)
                {
                    // Сохранить заказы в таблице времени
                    saveTimeSheets = Content.TimeSheetManager.GetList();
                }
            }

            // Результат закрытия сессии
            CloseSessionResult closeSessionResult = await Content.SessionManager.Close(sessionToClose, closeSessionOptions, Settings.Default.SiteProtocol);

            // Проверить на невозможность закрыть сессию
            if (closeSessionResult != CloseSessionResult.Closed)
            {
                if (closeSessionResult == CloseSessionResult.WebOrdersInProcessing)
                {
                    MessageBox.Show("Имеются зависшие не обработанные веб заказы. Необходимо их удалить. Закрытие невозможно.");
                }
                else if (closeSessionResult == CloseSessionResult.OrdersOpened)
                {
                    MessageBox.Show("Имеются не закрытые заказы на вынос и нет настройки о их переносе. Закрытие невозможно.");
                }
                else
                {
                    MessageBox.Show("Имеются не закрытые заказы в кафе. Закрытие невозможно.");
                }

                Close();
            }
            else
            {
                // ID закрывающейся сессии может сменится, если это первой закрытие. Был -1, стал положительным число
                _сloseSessionId = _сloseSessionId < 0 ? Content.SessionManager.GetLastClosedSessionId() : _сloseSessionId;

                // Вызвать событие закрытия сессии
                OnSessionClosed(sessionToClose);

                // Если надо, открыть новою сессию
                if (closeSessionOptions.DoOpenNewSession)
                {
                    OnSessionOpened(null);

                    // Если надо перенести открытые заказы на таблицу времени
                    if (closeSessionOptions.DoTransferOpenOrders && saveTimeSheets != null)
                    {
                        // Вернуть ререзвацию в таблицу времени
                        await Content.TimeSheetManager.TransferOpenOrdersToTimeSheets(saveTimeSheets);
                    }
                }

                MessageBox.Show(Resources.SessionTicketWarning, Resources.SessionTicket, MessageBoxButtons.OK);

                string message = String.Empty;
                var pointOfSales = Content.PointOfSaleRepository.GetItems();
                foreach (var point in pointOfSales)
                {
                    message = PrintingManager.EndSessionCheckPrint(sessionToClose, point.ID);
                }

                if (!string.IsNullOrEmpty(message))
                {
                    MessageBox.Show(message);
                    Content.PrintWorksRepository.AddPrintWork(new PrintWork
                    {
                        Created = DateTime.Now,
                        IsSession = true,
                        SessionId = sessionToClose.ID,
                        State = (int)State.Error
                    });
                }
                else
                {
                    Content.PrintWorksRepository.AddPrintWork(new PrintWork
                    {
                        Created = DateTime.Now,
                        IsSession = true,
                        SessionId = sessionToClose.ID,
                        State = (int)State.Compleated
                    });

                    PrintingManagerElves.PrintZ(Settings.Default.PrinterPort,Settings.Default.PrinterIp);
                }

                await ProcessWiringses();

                Close();
            }
        }

        private async Task ProcessWiringses()
        {
            if (Content.SessionManager.SessionHasWirings(_сloseSessionId))
            {
                MessageBox.Show(Resources.SessionAlreadyHasWiringses, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                Content.SignalProcessor.Bind<SessionProcessingSignalMessage>(SessionManagerOnSessionProcessing);

                var key = ExecutionTimeLogger.Start();
                await Content.SessionManager.ProcessSessionDocument(_сloseSessionId);

                ExecutionTimeLogger.Stop(key, "Проведение сессии");

                Content.SignalProcessor.UnBind<SessionProcessingSignalMessage>();
                Close();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            shiftOpenOrdersCheckBox.Enabled = openNewSessionCheckBox.Checked;
        }
    }
}
