﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.DataRepository.Repositories.Wiringses.RemainsOfGood;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.Documents.SessionForms
{
    public partial class SessionForm : Form
    {
        private bool DocumentIsProcessed { set; get; }

        private readonly List<SessionDocumentItem> _documentItems = new List<SessionDocumentItem>();

        public SessionForm(Session document)
        {
            InitializeComponent();

            WindowsFormProvider.FillControlsFromModel(this, document);

            DocumentIsProcessed = document.IsProcessed;

            dtnOpenDocDate.Value = document.Opened;
            dtnCloseDocDate.Value = document.Closed == null ? default(DateTime) : document.Closed.Value;

            Bind(document.ID);
        }

        /// <summary>
        /// Заполняет таблицу данными из сессии
        /// </summary>
        /// <param name="id">Ид сессии</param>
        private void Bind(int id)
        {
            mainGridControl.DataSource = Content.SessionManager.LoadItemOfSession(id);
        }

        private void SessionForm_Load(object sender, EventArgs e)
        {
            if (DocumentIsProcessed)
            {
                btnProcess.Text = Resources.Unprocess;
                btnProcess.ForeColor = Color.DarkRed;
            }
            else
            {
                btnProcess.Text = Resources.Process;
                btnProcess.ForeColor = Color.Green;
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (DocumentIsProcessed)
            {
                UnProcessDocument();
            }
            else
            {
                ProcessDocument();
            }
        }

        private void UnProcessDocument()
        {
            var key = ExecutionTimeLogger.Start();
            Content.RemainsOfGoodWiringsManager.UnProcessSessionDocument(Convert.ToInt32(txtNumber.Text));

            DocumentIsProcessed = false;

            btnProcess.Text = Resources.Process;
            btnProcess.ForeColor = Color.Green;
            
            ExecutionTimeLogger.Stop(key, "Отмена проводки сессии");
        }

        private void ProcessDocument()
        {
            if (string.IsNullOrEmpty(txtNumber.Text))
            {
                MessageBox.Show(Resources.DocIsNotProcessed, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            int documentId = Convert.ToInt32(txtNumber.Text);
            /*var wiringses = new List<RemainsOfGoodWirings>();

            try
            {
                wiringses = RemainsOfGoodWiringsManager.GetWiringsesFromDish(documentId, dtnCloseDocDate.Value,
                                                                             _documentItems);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
            RemainsOfGoodWiringsManager.ProcessSessionDocument(documentId, wiringses);

            */

            var form = new SessionProcessForm(documentId);
            form.Closed += delegate
            {
                btnProcess.Text = Resources.Unprocess;
                btnProcess.ForeColor = Color.DarkRed;
                DocumentIsProcessed = true;
                Bind(documentId);}; 
            form.ShowDialog();

            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
           
            //if (!currentRow.IsEditable)
            //{
            //    e.Appearance.BeginUpdate();

            //    e.Appearance.BackColor = Color.LightGray;

            //    e.Appearance.EndUpdate();
            //}
        }

        private void mainGridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            //var view = sender as GridView;

            //if (view == null || e.RowHandle < 0) return;

            //var currentRow = (SessionDocumentItem)view.GetRow(e.RowHandle);


        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            var brick =
               e.Graph.DrawString(
                   string.Format("Смена открыта {0:dd.MM.yyyy} закрыта {1:dd.MM.yyyy}", dtnOpenDocDate.Value, dtnCloseDocDate.Value), Color.Navy,
                   new RectangleF(0, 0, 560, 30), BorderSide.None);

            brick.Font = new Font("Arial", 12);
            brick.HorzAlignment = HorzAlignment.Near;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }

        private void printableComponentLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            var brick =
                e.Graph.DrawString(
                    string.Format("Закрытие смены №{0}.", txtNumber.Text), Color.Navy,
                    new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 16);
            brick.HorzAlignment = HorzAlignment.Near;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Near);
        }
    }
}