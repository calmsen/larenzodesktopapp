﻿namespace LaRenzo.Forms.Documents.SupplyForms
{
    partial class SupplyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupplyForm));
            this.spltMain = new System.Windows.Forms.SplitContainer();
            this.spltLeft = new System.Windows.Forms.SplitContainer();
            this.treeListCategories = new DevExpress.XtraTreeList.TreeList();
            this.NameCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.IdCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.lstProduct = new System.Windows.Forms.ListBox();
            this.spltRight = new System.Windows.Forms.SplitContainer();
            this.spltMainCenter = new System.Windows.Forms.SplitContainer();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnBaseMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.grpPay = new System.Windows.Forms.GroupBox();
            this.numPaySum = new System.Windows.Forms.NumericUpDown();
            this.lblPaySum = new System.Windows.Forms.Label();
            this.dtnPayDate = new System.Windows.Forms.DateTimePicker();
            this.lblPayDate = new System.Windows.Forms.Label();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpInputDoc = new System.Windows.Forms.GroupBox();
            this.lblInputNumber = new System.Windows.Forms.Label();
            this.dtnInputDate = new System.Windows.Forms.DateTimePicker();
            this.lblInputDate = new System.Windows.Forms.Label();
            this.txtInputNumber = new System.Windows.Forms.TextBox();
            this.cmbWarehouse = new System.Windows.Forms.ComboBox();
            this.cmbPartner = new System.Windows.Forms.ComboBox();
            this.btnSelectWarehouse = new System.Windows.Forms.Button();
            this.btnSelectParent = new System.Windows.Forms.Button();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.dtnDocDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.lblPartner = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.richOtherInfo = new System.Windows.Forms.RichTextBox();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.spltMain)).BeginInit();
            this.spltMain.Panel1.SuspendLayout();
            this.spltMain.Panel2.SuspendLayout();
            this.spltMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltLeft)).BeginInit();
            this.spltLeft.Panel1.SuspendLayout();
            this.spltLeft.Panel2.SuspendLayout();
            this.spltLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListCategories)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spltRight)).BeginInit();
            this.spltRight.Panel1.SuspendLayout();
            this.spltRight.Panel2.SuspendLayout();
            this.spltRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltMainCenter)).BeginInit();
            this.spltMainCenter.Panel1.SuspendLayout();
            this.spltMainCenter.Panel2.SuspendLayout();
            this.spltMainCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.bottomPanel.SuspendLayout();
            this.grpPay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPaySum)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.grpInputDoc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.SuspendLayout();
            // 
            // spltMain
            // 
            this.spltMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltMain.Location = new System.Drawing.Point(0, 0);
            this.spltMain.Name = "spltMain";
            // 
            // spltMain.Panel1
            // 
            this.spltMain.Panel1.Controls.Add(this.spltLeft);
            // 
            // spltMain.Panel2
            // 
            this.spltMain.Panel2.Controls.Add(this.spltRight);
            this.spltMain.Size = new System.Drawing.Size(1216, 692);
            this.spltMain.SplitterDistance = 406;
            this.spltMain.TabIndex = 0;
            // 
            // spltLeft
            // 
            this.spltLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltLeft.Location = new System.Drawing.Point(0, 0);
            this.spltLeft.Name = "spltLeft";
            // 
            // spltLeft.Panel1
            // 
            this.spltLeft.Panel1.Controls.Add(this.treeListCategories);
            // 
            // spltLeft.Panel2
            // 
            this.spltLeft.Panel2.Controls.Add(this.lstProduct);
            this.spltLeft.Size = new System.Drawing.Size(406, 692);
            this.spltLeft.SplitterDistance = 191;
            this.spltLeft.TabIndex = 0;
            // 
            // treeListCategories
            // 
            this.treeListCategories.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.NameCol,
            this.IdCol});
            this.treeListCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListCategories.DragNodesMode = DevExpress.XtraTreeList.TreeListDragNodesMode.Standard;
            this.treeListCategories.Location = new System.Drawing.Point(0, 0);
            this.treeListCategories.Name = "treeListCategories";
            this.treeListCategories.OptionsBehavior.DragNodes = true;
            this.treeListCategories.OptionsBehavior.Editable = false;
            this.treeListCategories.OptionsBehavior.EnableFiltering = true;
            this.treeListCategories.ParentFieldName = "ParentCategoryID";
            this.treeListCategories.SelectImageList = this.imgList;
            this.treeListCategories.Size = new System.Drawing.Size(191, 692);
            this.treeListCategories.TabIndex = 0;
            this.treeListCategories.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListCategories_FocusedNodeChanged);
            // 
            // NameCol
            // 
            this.NameCol.Caption = "Категории";
            this.NameCol.FieldName = "Name";
            this.NameCol.MinWidth = 37;
            this.NameCol.Name = "NameCol";
            this.NameCol.Visible = true;
            this.NameCol.VisibleIndex = 0;
            // 
            // IdCol
            // 
            this.IdCol.Caption = "ID";
            this.IdCol.FieldName = "ID";
            this.IdCol.Name = "IdCol";
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "Deleket-Sleek-Xp-Basic-Folder.ico");
            // 
            // lstProduct
            // 
            this.lstProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstProduct.FormattingEnabled = true;
            this.lstProduct.Location = new System.Drawing.Point(0, 0);
            this.lstProduct.Name = "lstProduct";
            this.lstProduct.Size = new System.Drawing.Size(211, 692);
            this.lstProduct.TabIndex = 0;
            this.lstProduct.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstProduct_MouseDoubleClick);
            // 
            // spltRight
            // 
            this.spltRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltRight.Location = new System.Drawing.Point(0, 0);
            this.spltRight.Name = "spltRight";
            // 
            // spltRight.Panel1
            // 
            this.spltRight.Panel1.Controls.Add(this.spltMainCenter);
            // 
            // spltRight.Panel2
            // 
            this.spltRight.Panel2.Controls.Add(this.richOtherInfo);
            this.spltRight.Size = new System.Drawing.Size(806, 692);
            this.spltRight.SplitterDistance = 592;
            this.spltRight.TabIndex = 0;
            // 
            // spltMainCenter
            // 
            this.spltMainCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltMainCenter.Location = new System.Drawing.Point(0, 0);
            this.spltMainCenter.Name = "spltMainCenter";
            this.spltMainCenter.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltMainCenter.Panel1
            // 
            this.spltMainCenter.Panel1.Controls.Add(this.mainGridControl);
            // 
            // spltMainCenter.Panel2
            // 
            this.spltMainCenter.Panel2.Controls.Add(this.bottomPanel);
            this.spltMainCenter.Size = new System.Drawing.Size(592, 692);
            this.spltMainCenter.SplitterDistance = 524;
            this.spltMainCenter.TabIndex = 0;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(592, 524);
            this.mainGridControl.TabIndex = 5;
            this.mainGridControl.Tag = "1";
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            this.mainGridControl.DoubleClick += new System.EventHandler(this.mainGridControl_DoubleClick);
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnName,
            this.columnAmmount,
            this.columnPrice,
            this.columnSum,
            this.columnBaseMeasure});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.mainGridView_CellValueChanged);
            // 
            // columnName
            // 
            this.columnName.Caption = "Название";
            this.columnName.FieldName = "Product.Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 0;
            this.columnName.Width = 216;
            // 
            // columnAmmount
            // 
            this.columnAmmount.Caption = "Количество";
            this.columnAmmount.FieldName = "Amount";
            this.columnAmmount.Name = "columnAmmount";
            this.columnAmmount.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.columnAmmount.Visible = true;
            this.columnAmmount.VisibleIndex = 1;
            this.columnAmmount.Width = 127;
            // 
            // columnPrice
            // 
            this.columnPrice.Caption = "Цена";
            this.columnPrice.FieldName = "Price";
            this.columnPrice.Name = "columnPrice";
            this.columnPrice.OptionsColumn.AllowEdit = false;
            this.columnPrice.OptionsColumn.AllowFocus = false;
            this.columnPrice.OptionsColumn.ReadOnly = true;
            this.columnPrice.Visible = true;
            this.columnPrice.VisibleIndex = 3;
            this.columnPrice.Width = 105;
            // 
            // columnSum
            // 
            this.columnSum.Caption = "Сумма";
            this.columnSum.DisplayFormat.FormatString = "c2 руб.";
            this.columnSum.FieldName = "Sum";
            this.columnSum.Name = "columnSum";
            this.columnSum.ShowUnboundExpressionMenu = true;
            this.columnSum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnSum.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.columnSum.Visible = true;
            this.columnSum.VisibleIndex = 4;
            this.columnSum.Width = 112;
            // 
            // columnBaseMeasure
            // 
            this.columnBaseMeasure.Caption = "Ед. Изм.";
            this.columnBaseMeasure.FieldName = "Product.Measure.Name";
            this.columnBaseMeasure.Name = "columnBaseMeasure";
            this.columnBaseMeasure.OptionsColumn.AllowEdit = false;
            this.columnBaseMeasure.OptionsColumn.ReadOnly = true;
            this.columnBaseMeasure.Visible = true;
            this.columnBaseMeasure.VisibleIndex = 2;
            this.columnBaseMeasure.Width = 70;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.grpPay);
            this.bottomPanel.Controls.Add(this.pnlBottom);
            this.bottomPanel.Controls.Add(this.grpInputDoc);
            this.bottomPanel.Controls.Add(this.cmbWarehouse);
            this.bottomPanel.Controls.Add(this.cmbPartner);
            this.bottomPanel.Controls.Add(this.btnSelectWarehouse);
            this.bottomPanel.Controls.Add(this.btnSelectParent);
            this.bottomPanel.Controls.Add(this.txtNumber);
            this.bottomPanel.Controls.Add(this.dtnDocDate);
            this.bottomPanel.Controls.Add(this.lblDate);
            this.bottomPanel.Controls.Add(this.lblWarehouse);
            this.bottomPanel.Controls.Add(this.lblPartner);
            this.bottomPanel.Controls.Add(this.lblNumber);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomPanel.Location = new System.Drawing.Point(0, 0);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(592, 164);
            this.bottomPanel.TabIndex = 6;
            // 
            // grpPay
            // 
            this.grpPay.Controls.Add(this.numPaySum);
            this.grpPay.Controls.Add(this.lblPaySum);
            this.grpPay.Controls.Add(this.dtnPayDate);
            this.grpPay.Controls.Add(this.lblPayDate);
            this.grpPay.Location = new System.Drawing.Point(17, 94);
            this.grpPay.Name = "grpPay";
            this.grpPay.Size = new System.Drawing.Size(341, 40);
            this.grpPay.TabIndex = 56;
            this.grpPay.TabStop = false;
            this.grpPay.Text = "Платеж";
            // 
            // numPaySum
            // 
            this.numPaySum.DecimalPlaces = 2;
            this.numPaySum.Location = new System.Drawing.Point(71, 16);
            this.numPaySum.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.numPaySum.Name = "numPaySum";
            this.numPaySum.Size = new System.Drawing.Size(110, 20);
            this.numPaySum.TabIndex = 7;
            this.numPaySum.Tag = "PaySum";
            this.numPaySum.ValueChanged += new System.EventHandler(this.numPaySum_ValueChanged);
            // 
            // lblPaySum
            // 
            this.lblPaySum.AutoSize = true;
            this.lblPaySum.Location = new System.Drawing.Point(21, 18);
            this.lblPaySum.Name = "lblPaySum";
            this.lblPaySum.Size = new System.Drawing.Size(44, 13);
            this.lblPaySum.TabIndex = 0;
            this.lblPaySum.Text = "Сумма:";
            // 
            // dtnPayDate
            // 
            this.dtnPayDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtnPayDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtnPayDate.Location = new System.Drawing.Point(214, 15);
            this.dtnPayDate.Name = "dtnPayDate";
            this.dtnPayDate.Size = new System.Drawing.Size(121, 20);
            this.dtnPayDate.TabIndex = 8;
            this.dtnPayDate.Tag = "PayDate";
            // 
            // lblPayDate
            // 
            this.lblPayDate.AutoSize = true;
            this.lblPayDate.Location = new System.Drawing.Point(187, 18);
            this.lblPayDate.Name = "lblPayDate";
            this.lblPayDate.Size = new System.Drawing.Size(21, 13);
            this.lblPayDate.TabIndex = 0;
            this.lblPayDate.Text = "от:";
            // 
            // pnlBottom
            // 
            this.pnlBottom.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottom.Controls.Add(this.btnPrint);
            this.pnlBottom.Controls.Add(this.btnProcess);
            this.pnlBottom.Controls.Add(this.btnSave);
            this.pnlBottom.Controls.Add(this.btnCancel);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 140);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(592, 24);
            this.pnlBottom.TabIndex = 55;
            // 
            // btnPrint
            // 
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPrint.Location = new System.Drawing.Point(229, 0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(87, 22);
            this.btnPrint.TabIndex = 11;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnProcess.ForeColor = System.Drawing.Color.Green;
            this.btnProcess.Location = new System.Drawing.Point(316, 0);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(100, 22);
            this.btnProcess.TabIndex = 12;
            this.btnProcess.Text = "Провести";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(416, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "Записать";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(503, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // grpInputDoc
            // 
            this.grpInputDoc.Controls.Add(this.lblInputNumber);
            this.grpInputDoc.Controls.Add(this.dtnInputDate);
            this.grpInputDoc.Controls.Add(this.lblInputDate);
            this.grpInputDoc.Controls.Add(this.txtInputNumber);
            this.grpInputDoc.Location = new System.Drawing.Point(364, 14);
            this.grpInputDoc.Name = "grpInputDoc";
            this.grpInputDoc.Size = new System.Drawing.Size(225, 74);
            this.grpInputDoc.TabIndex = 54;
            this.grpInputDoc.TabStop = false;
            this.grpInputDoc.Text = "Входящий документ";
            // 
            // lblInputNumber
            // 
            this.lblInputNumber.AutoSize = true;
            this.lblInputNumber.Location = new System.Drawing.Point(21, 23);
            this.lblInputNumber.Name = "lblInputNumber";
            this.lblInputNumber.Size = new System.Drawing.Size(44, 13);
            this.lblInputNumber.TabIndex = 1;
            this.lblInputNumber.Text = "Номер:";
            // 
            // dtnInputDate
            // 
            this.dtnInputDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtnInputDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtnInputDate.Location = new System.Drawing.Point(71, 46);
            this.dtnInputDate.Name = "dtnInputDate";
            this.dtnInputDate.Size = new System.Drawing.Size(148, 20);
            this.dtnInputDate.TabIndex = 10;
            this.dtnInputDate.Tag = "InputDate";
            // 
            // lblInputDate
            // 
            this.lblInputDate.AutoSize = true;
            this.lblInputDate.Location = new System.Drawing.Point(43, 49);
            this.lblInputDate.Name = "lblInputDate";
            this.lblInputDate.Size = new System.Drawing.Size(21, 13);
            this.lblInputDate.TabIndex = 1;
            this.lblInputDate.Text = "от:";
            // 
            // txtInputNumber
            // 
            this.txtInputNumber.Location = new System.Drawing.Point(71, 20);
            this.txtInputNumber.Name = "txtInputNumber";
            this.txtInputNumber.Size = new System.Drawing.Size(148, 20);
            this.txtInputNumber.TabIndex = 9;
            this.txtInputNumber.Tag = "InputNumber";
            // 
            // cmbWarehouse
            // 
            this.cmbWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWarehouse.FormattingEnabled = true;
            this.cmbWarehouse.Location = new System.Drawing.Point(88, 67);
            this.cmbWarehouse.Name = "cmbWarehouse";
            this.cmbWarehouse.Size = new System.Drawing.Size(238, 21);
            this.cmbWarehouse.TabIndex = 5;
            this.cmbWarehouse.Tag = "WarehouseID";
            // 
            // cmbPartner
            // 
            this.cmbPartner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPartner.FormattingEnabled = true;
            this.cmbPartner.Location = new System.Drawing.Point(88, 40);
            this.cmbPartner.Name = "cmbPartner";
            this.cmbPartner.Size = new System.Drawing.Size(238, 21);
            this.cmbPartner.TabIndex = 3;
            this.cmbPartner.Tag = "PartnerID";
            // 
            // btnSelectWarehouse
            // 
            this.btnSelectWarehouse.Location = new System.Drawing.Point(334, 66);
            this.btnSelectWarehouse.Name = "btnSelectWarehouse";
            this.btnSelectWarehouse.Size = new System.Drawing.Size(24, 20);
            this.btnSelectWarehouse.TabIndex = 6;
            this.btnSelectWarehouse.Text = "...";
            this.btnSelectWarehouse.UseVisualStyleBackColor = true;
            this.btnSelectWarehouse.Click += new System.EventHandler(this.btnSelectWarehouse_Click);
            // 
            // btnSelectParent
            // 
            this.btnSelectParent.Location = new System.Drawing.Point(334, 41);
            this.btnSelectParent.Name = "btnSelectParent";
            this.btnSelectParent.Size = new System.Drawing.Size(24, 20);
            this.btnSelectParent.TabIndex = 4;
            this.btnSelectParent.Text = "...";
            this.btnSelectParent.UseVisualStyleBackColor = true;
            this.btnSelectParent.Click += new System.EventHandler(this.btnSelectParent_Click);
            // 
            // txtNumber
            // 
            this.txtNumber.Enabled = false;
            this.txtNumber.Location = new System.Drawing.Point(88, 15);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(110, 20);
            this.txtNumber.TabIndex = 1;
            this.txtNumber.Tag = "id";
            // 
            // dtnDocDate
            // 
            this.dtnDocDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtnDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtnDocDate.Location = new System.Drawing.Point(230, 14);
            this.dtnDocDate.Name = "dtnDocDate";
            this.dtnDocDate.Size = new System.Drawing.Size(128, 20);
            this.dtnDocDate.TabIndex = 2;
            this.dtnDocDate.Tag = "DocumentDate";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(204, 18);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(21, 13);
            this.lblDate.TabIndex = 42;
            this.lblDate.Text = "от:";
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(41, 70);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(41, 13);
            this.lblWarehouse.TabIndex = 44;
            this.lblWarehouse.Text = "Склад:";
            // 
            // lblPartner
            // 
            this.lblPartner.AutoSize = true;
            this.lblPartner.Location = new System.Drawing.Point(14, 43);
            this.lblPartner.Name = "lblPartner";
            this.lblPartner.Size = new System.Drawing.Size(68, 13);
            this.lblPartner.TabIndex = 45;
            this.lblPartner.Text = "Поставщик:";
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(38, 18);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(44, 13);
            this.lblNumber.TabIndex = 46;
            this.lblNumber.Text = "Номер:";
            // 
            // richOtherInfo
            // 
            this.richOtherInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richOtherInfo.Location = new System.Drawing.Point(0, 0);
            this.richOtherInfo.Name = "richOtherInfo";
            this.richOtherInfo.Size = new System.Drawing.Size(210, 692);
            this.richOtherInfo.TabIndex = 0;
            this.richOtherInfo.Tag = "Note";
            this.richOtherInfo.Text = "Дополнительная информация:";
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            this.printableComponentLink.Landscape = true;
            this.printableComponentLink.Margins = new System.Drawing.Printing.Margins(100, 100, 76, 100);
            this.printableComponentLink.MinMargins = new System.Drawing.Printing.Margins(20, 20, 50, 20);
            this.printableComponentLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateMarginalHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateMarginalHeaderArea);
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // SupplyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 692);
            this.Controls.Add(this.spltMain);
            this.Name = "SupplyForm";
            this.Text = "Поступление товаров на склад";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SupplyForm_FormClosing);
            this.Load += new System.EventHandler(this.SupplyForm_Load);
            this.spltMain.Panel1.ResumeLayout(false);
            this.spltMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltMain)).EndInit();
            this.spltMain.ResumeLayout(false);
            this.spltLeft.Panel1.ResumeLayout(false);
            this.spltLeft.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltLeft)).EndInit();
            this.spltLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListCategories)).EndInit();
            this.spltRight.Panel1.ResumeLayout(false);
            this.spltRight.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltRight)).EndInit();
            this.spltRight.ResumeLayout(false);
            this.spltMainCenter.Panel1.ResumeLayout(false);
            this.spltMainCenter.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltMainCenter)).EndInit();
            this.spltMainCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.bottomPanel.ResumeLayout(false);
            this.bottomPanel.PerformLayout();
            this.grpPay.ResumeLayout(false);
            this.grpPay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPaySum)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.grpInputDoc.ResumeLayout(false);
            this.grpInputDoc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spltMain;
        private System.Windows.Forms.SplitContainer spltLeft;
        private System.Windows.Forms.SplitContainer spltRight;
        private DevExpress.XtraTreeList.TreeList treeListCategories;
        private DevExpress.XtraTreeList.Columns.TreeListColumn NameCol;
        private DevExpress.XtraTreeList.Columns.TreeListColumn IdCol;
        private System.Windows.Forms.RichTextBox richOtherInfo;
        private System.Windows.Forms.SplitContainer spltMainCenter;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.GroupBox grpPay;
        private System.Windows.Forms.Label lblPaySum;
        private System.Windows.Forms.DateTimePicker dtnPayDate;
        private System.Windows.Forms.Label lblPayDate;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox grpInputDoc;
        private System.Windows.Forms.Label lblInputNumber;
        private System.Windows.Forms.DateTimePicker dtnInputDate;
        private System.Windows.Forms.Label lblInputDate;
        private System.Windows.Forms.TextBox txtInputNumber;
        private System.Windows.Forms.ComboBox cmbWarehouse;
        private System.Windows.Forms.ComboBox cmbPartner;
        private System.Windows.Forms.Button btnSelectWarehouse;
        private System.Windows.Forms.Button btnSelectParent;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.DateTimePicker dtnDocDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Label lblPartner;
        private System.Windows.Forms.Label lblNumber;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrice;
        private DevExpress.XtraGrid.Columns.GridColumn columnSum;
        private DevExpress.XtraGrid.Columns.GridColumn columnBaseMeasure;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.ListBox lstProduct;
        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.NumericUpDown numPaySum;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;


    }
}