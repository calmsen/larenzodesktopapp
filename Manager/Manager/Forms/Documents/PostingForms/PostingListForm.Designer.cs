﻿namespace LaRenzo.Forms.Documents.PostingForms
{
    partial class PostingListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCopy = new System.Windows.Forms.ToolStripButton();
            this.btnEdit = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPrinters = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnPrintSupplyForm = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnWarehous = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIsProcessed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.searchButton = new System.Windows.Forms.ToolStripButton();
            this.mainToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.toolStripSeparator1,
            this.btnCopy,
            this.btnEdit,
            this.btnDelete,
            this.toolStripSeparator2,
            this.btnPrinters,
            this.btnRefresh,
            this.toolStripSeparator3,
            this.searchButton});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(686, 31);
            this.mainToolStrip.TabIndex = 3;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(28, 28);
            this.btnAdd.Text = "Создать поступление";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // btnCopy
            // 
            this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopy.Image = global::LaRenzo.Properties.Resources.copy;
            this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(28, 28);
            this.btnCopy.Text = "Скопировать посупление";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEdit.Image = global::LaRenzo.Properties.Resources.edit_wh;
            this.btnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(28, 28);
            this.btnEdit.Text = "Редактировать поступление";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(28, 28);
            this.btnDelete.Text = "Пометить на удаление";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // btnPrinters
            // 
            this.btnPrinters.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrinters.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPrintSupplyForm});
            this.btnPrinters.Image = global::LaRenzo.Properties.Resources.print_printer;
            this.btnPrinters.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrinters.Name = "btnPrinters";
            this.btnPrinters.Size = new System.Drawing.Size(37, 28);
            this.btnPrinters.Text = "toolStripDropDownButton1";
            // 
            // btnPrintSupplyForm
            // 
            this.btnPrintSupplyForm.Name = "btnPrintSupplyForm";
            this.btnPrintSupplyForm.Size = new System.Drawing.Size(194, 22);
            this.btnPrintSupplyForm.Text = "Поступление товаров";
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = global::LaRenzo.Properties.Resources.refresh;
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(28, 28);
            this.btnRefresh.Text = "Обновить";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 31);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(686, 437);
            this.mainGridControl.TabIndex = 4;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnNumber,
            this.columnDate,
            this.columnWarehous,
            this.columnSum,
            this.columnIsProcessed,
            this.columnUser});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
            this.mainGridView.DoubleClick += new System.EventHandler(this.mainGridView_DoubleClick);
            // 
            // columnNumber
            // 
            this.columnNumber.Caption = "Номер";
            this.columnNumber.FieldName = "ID";
            this.columnNumber.Name = "columnNumber";
            this.columnNumber.OptionsColumn.AllowEdit = false;
            this.columnNumber.OptionsColumn.ReadOnly = true;
            this.columnNumber.Visible = true;
            this.columnNumber.VisibleIndex = 1;
            this.columnNumber.Width = 123;
            // 
            // columnDate
            // 
            this.columnDate.Caption = "Дата";
            this.columnDate.FieldName = "DocumentDate";
            this.columnDate.Name = "columnDate";
            this.columnDate.OptionsColumn.AllowEdit = false;
            this.columnDate.OptionsColumn.ReadOnly = true;
            this.columnDate.Visible = true;
            this.columnDate.VisibleIndex = 2;
            this.columnDate.Width = 123;
            // 
            // columnWarehous
            // 
            this.columnWarehous.Caption = "Склад";
            this.columnWarehous.FieldName = "Warehouse.Name";
            this.columnWarehous.Name = "columnWarehous";
            this.columnWarehous.OptionsColumn.AllowEdit = false;
            this.columnWarehous.OptionsColumn.ReadOnly = true;
            this.columnWarehous.Visible = true;
            this.columnWarehous.VisibleIndex = 3;
            this.columnWarehous.Width = 84;
            // 
            // columnSum
            // 
            this.columnSum.Caption = "Сумма";
            this.columnSum.DisplayFormat.FormatString = "F2";
            this.columnSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnSum.FieldName = "DocumentSum";
            this.columnSum.Name = "columnSum";
            this.columnSum.OptionsColumn.AllowEdit = false;
            this.columnSum.OptionsColumn.ReadOnly = true;
            this.columnSum.Visible = true;
            this.columnSum.VisibleIndex = 4;
            this.columnSum.Width = 88;
            // 
            // columnIsProcessed
            // 
            this.columnIsProcessed.Caption = "Проведен";
            this.columnIsProcessed.FieldName = "IsProcessed";
            this.columnIsProcessed.Name = "columnIsProcessed";
            this.columnIsProcessed.OptionsColumn.AllowEdit = false;
            this.columnIsProcessed.OptionsColumn.ReadOnly = true;
            this.columnIsProcessed.Visible = true;
            this.columnIsProcessed.VisibleIndex = 0;
            this.columnIsProcessed.Width = 50;
            // 
            // columnUser
            // 
            this.columnUser.Caption = "Пользователь";
            this.columnUser.FieldName = "User";
            this.columnUser.Name = "columnUser";
            this.columnUser.OptionsColumn.AllowEdit = false;
            this.columnUser.OptionsColumn.AllowFocus = false;
            this.columnUser.OptionsColumn.ReadOnly = true;
            this.columnUser.Visible = true;
            this.columnUser.VisibleIndex = 5;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // searchButton
            // 
            this.searchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchButton.Image = global::LaRenzo.Properties.Resources.search;
            this.searchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(28, 28);
            this.searchButton.Text = "Найти";
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // PostingListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 468);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.mainToolStrip);
            this.Name = "PostingListForm";
            this.Text = "Оприходование товаров";
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnCopy;
        private System.Windows.Forms.ToolStripButton btnEdit;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripDropDownButton btnPrinters;
        private System.Windows.Forms.ToolStripMenuItem btnPrintSupplyForm;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnNumber;
        private DevExpress.XtraGrid.Columns.GridColumn columnDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnWarehous;
        private DevExpress.XtraGrid.Columns.GridColumn columnSum;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsProcessed;
        private DevExpress.XtraGrid.Columns.GridColumn columnUser;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton searchButton;
    }
}