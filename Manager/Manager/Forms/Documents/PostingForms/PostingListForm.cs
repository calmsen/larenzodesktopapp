﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.Posting;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Posting;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.PostingForms
{
    public partial class PostingListForm : Form
    {
        public PostingListForm()
        {
            InitializeComponent();
            LoadPostingList(new object(), new EventArgs());
        }

        private void LoadPostingList(object sender, EventArgs args)
        {
            mainGridControl.DataSource = Content.PostingManager.GetPostingDocuments();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var postingForm = new PostingForm();
            postingForm.FormClosed += UpdateDataGrid;
            postingForm.Show();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateDataGrid(sender, e);
        }

        private void UpdateDataGrid(object sender, EventArgs e)
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            LoadPostingList(sender, e);
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (PostingDocument) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetPostingForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.PostingDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void GetPostingForm(bool isEdit, int postingId)
        {
            var postingDocument = Content.PostingManager.GetPostingDocument(postingId);

            var postingForm = new PostingForm(isEdit, postingDocument);

            postingForm.FormClosed += LoadPostingList;
            
            postingForm.Show();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (PostingDocument) mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetPostingForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.PostingDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var selectedRow = (PostingDocument) mainGridView.GetFocusedRow();

            if (selectedRow == null)
            {
                MessageBox.Show(Resources.PostingDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            string msg = selectedRow.IsMarkToDelete
                             ? Resources.UnMarkDeleteDocument
                             : Resources.MarkDeleteDocument;

            if (MessageBox.Show(msg, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Content.PostingManager.DeletePostingDocument(selectedRow);
                LoadPostingList(sender, e);
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            var info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var postingId = (int) view.GetRowCellValue(info.RowHandle, columnNumber);
                GetPostingForm(true, postingId);
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (PostingDocument) view.GetRow(e.RowHandle);

            if (currentRow.IsMarkToDelete)
                e.Appearance.BackColor = Color.Salmon;

            if (currentRow.IsProcessed)
                e.Appearance.BackColor = Color.LightGreen;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}