﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.WriteoffDish;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.WriteoffDish;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Documents.WriteOffDishes
{
    public partial class WriteoffDishListForm : Form
    {
        public WriteoffDishListForm()
        {
            InitializeComponent();
            LoadWriteOffDishList(new object(), new EventArgs());
        }

        private void LoadWriteOffDishList(object sender, EventArgs args)
        {
            mainGridControl.DataSource = Content.WriteoffDishManager.GetWriteoffDishDocuments();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var writeoffDishForm = new WriteoffDishForm();
            writeoffDishForm.FormClosed += UpdateDataGrid;
            writeoffDishForm.Show();
        }

        private void UpdateDataGrid(object sender, EventArgs e)
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
            LoadWriteOffDishList(sender, e);
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateDataGrid(sender, e);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var selectedRow = (WriteoffDishDocument)mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetWriteoffDishForm(true, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.WriteoffDishDocumentIsNotSelected, Resources.WarningTitle,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            var selectedRow = (WriteoffDishDocument)mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                GetWriteoffDishForm(false, selectedRow.ID);
            }
            else
            {
                MessageBox.Show(Resources.WriteoffDishDocumentIsNotSelected, Resources.WarningTitle,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void GetWriteoffDishForm(bool isEdit, int writeoffDishId)
        {
            var writeoffDishDocument = Content.WriteoffDishManager.GetWriteoffDishDocument(writeoffDishId);

            var writeoffForm = new WriteoffDishForm(isEdit, writeoffDishDocument);

            writeoffForm.FormClosed += UpdateDataGrid;
            writeoffForm.Show();
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (WriteoffDishDocument)view.GetRow(e.RowHandle);

            if (currentRow.IsMarkToDelete)
                e.Appearance.BackColor = Color.Salmon;

            if (currentRow.IsProcessed)
                e.Appearance.BackColor = Color.LightGreen;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var selectedRow = (WriteoffDishDocument)mainGridView.GetFocusedRow();

            if (selectedRow == null)
            {
                MessageBox.Show(Resources.WriteoffDishDocumentIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                               MessageBoxIcon.Warning);
                return;
            }

            string msg = selectedRow.IsMarkToDelete
                             ? Resources.UnMarkDeleteDocument
                             : Resources.MarkDeleteDocument;

            if (MessageBox.Show(msg, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Content.WriteoffDishManager.DeleteWriteoffDocument(selectedRow);
                UpdateDataGrid(sender, e);
            }
        }

        private void mainGridView_DoubleClick(object sender, EventArgs e)
        {
            var view = (GridView)sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            var info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
                var supplyId = (int)view.GetRowCellValue(info.RowHandle, columnNumber);
                GetWriteoffDishForm(true, supplyId);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
    }
}