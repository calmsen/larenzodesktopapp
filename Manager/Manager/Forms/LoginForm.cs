﻿using System;
using System.Windows.Forms;
using DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.Forms.Parameters.OptionForms;
using LaRenzo.FormsProvider;
using LaRenzo.Tools;


namespace LaRenzo.Forms
{
    public partial class LoginForm : Form
    {
        private const string AUTH_ERROR_MSG = "Ошибка авторизации";        
        private const string AUTH_ERROR_MSG_BRANCH = "Ошибка авторизации, данный пользователь не имеет доступ в текущий филиал";
        private const string DB_ERROR_MSG = "Не удалось подключиться к базе данных. Проверьте настройки";


        /// <param name="user"> Имя пользователя </param>
        public delegate void UserLoggedEventHandler(User user);

        /// <summary> Срабатывает после успешной авторизации пользователя </summary>
        public static event UserLoggedEventHandler UserLogged;

        public LoginForm()
        {
            InitializeComponent();
            branchComboBox1.Visible = true;
            branchComboBox1.InitializeOrRefreshList(new BranchFilter {  NotID = 1 });
            branchComboBox1.SelectValueById(Content.BranchRepository.SelectedBranchId);
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Попытка паторизации </summary>
        ////========================================
        protected virtual async void TryLogin()
        {
            try
            {
                Text = @"  Авторизация. Подключение ";
                onConnectTimer.Enabled = true;
                SetControlEnabled(false);

                var key = ExecutionTimeLogger.Start();

                // Попытка авторизации
                var user = Content.UserManager.Login(txtName.Text, txtPassword.Text);
                if (user != null)
                {
                    if (user.BranchId != Content.BranchRepository.SelectedBranchId)
                    {
                        Text = @"Авторизация";
                        onConnectTimer.Enabled = false;
                        SetControlEnabled(true);
                        MessageBox.Show(AUTH_ERROR_MSG_BRANCH);
                        ExecutionTimeLogger.Stop(key, "Авторизация пользователя (неудача)");
                    }
                    else
                    {
                        UserLogged(user);
                        ExecutionTimeLogger.Stop(key, "Авторизация пользователя");
                        Close();
                    }
                }
                else
                {
                    Text = @"  Авторизация";

                    onConnectTimer.Enabled = false;
                    SetControlEnabled(true);
                    MessageBox.Show(AUTH_ERROR_MSG);
                    ExecutionTimeLogger.Stop(key, "Авторизация пользователя (неудача)");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(DB_ERROR_MSG + "/r/n/r/n" + ex.ToString());
                Close();
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Установить доступность ввода элементов </summary>
        ////===========================================================
        private void SetControlEnabled(bool isEnabled)
        {
            loginButton.Enabled = isEnabled;
            txtName.Enabled = isEnabled;
            txtPassword.Enabled = isEnabled;
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Нажатие кнопки в поле "имя пользователя" </summary>
        ////=============================================================
        private void TextNameKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPassword.Focus();
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Нажатие кнопки в поле "пароль" </summary>
        ////===================================================
        private void TextPasswordKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TryLogin();
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Нажатие кнопки в окне </summary>
        ////==========================================
        private void LoginFormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }


        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнопка "Войти" </summary>
        ////===================================
        private void LoginButtonClick(object sender, EventArgs e)
        {
            TryLogin();
        }
        
        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Таймер "подключения" (рисует точки прогресса) </summary>/==================================================================
        private void OnConnectTimerTick(object sender, EventArgs e)
        {
            Text += @".";
        }
    }
}
