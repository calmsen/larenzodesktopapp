﻿using System;
using System.Windows.Forms;

namespace LaRenzo.Forms.StaffForms.Controls
{
	public partial class DateTimeControl : UserControl
	{
		public DateTimeControl()
		{
			InitializeComponent();

			// Устиновить значения по умолчанию
			dateTimePicker.Value = DateTime.Now;
			timeEdit.EditValue = DateTime.Now.AddHours(3);
		}

		/// <summary> Выбранное значение </summary>
		public DateTime SelectValue
		{
			get
			{
				return new DateTime(dateTimePicker.Value.Year, dateTimePicker.Value.Month, dateTimePicker.Value.Day, timeEdit.Time.Hour, timeEdit.Time.Minute, 0);
			}
			set
			{
				dateTimePicker.Value = value;
				timeEdit.EditValue = value;
			}
		}

		private void timeEdit_EditValueChanged(object sender, EventArgs e)
		{

		}
	}
}
