﻿namespace LaRenzo.Forms.Controls
{
	public abstract partial class BaseCombo<T> 
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.listComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // listComboBox
            // 
            this.listComboBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.listComboBox.FormattingEnabled = true;
            this.listComboBox.Location = new System.Drawing.Point(0, 0);
            this.listComboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listComboBox.Name = "listComboBox";
            this.listComboBox.Size = new System.Drawing.Size(257, 24);
            this.listComboBox.TabIndex = 0;
            this.listComboBox.SelectedIndexChanged += new System.EventHandler(this.ListComboBoxSelectedIndexChanged);
            // 
            // BaseCombo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listComboBox);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "BaseCombo";
            this.Size = new System.Drawing.Size(296, 25);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ComboBox listComboBox;
	}
}
