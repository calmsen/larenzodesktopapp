﻿namespace LaRenzo.Forms.Parameters.PrinterForms
{
    partial class PrintingMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.detailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.DetailPrintName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnPrintWorkId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrintWorkOrderId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrintWorkOrderName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrintWorkOrderTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrintWorkCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrintWorksLastTry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrintWorkState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.linqServerModeSourcePrintWorks = new DevExpress.Data.Linq.LinqServerModeSource();
            this.WorksContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.RepeatWorkMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DetailContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.RepeatDetailMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDate = new System.Windows.Forms.Label();
            this.dTimeDateOfData = new System.Windows.Forms.DateTimePicker();
            this.nudGenerateAmount = new System.Windows.Forms.NumericUpDown();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.lblLastOrders = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourcePrintWorks)).BeginInit();
            this.WorksContextMenu.SuspendLayout();
            this.DetailContextMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGenerateAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // detailGridView
            // 
            this.detailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.DetailPrintName});
            this.detailGridView.GridControl = this.mainGridControl;
            this.detailGridView.Name = "detailGridView";
            this.detailGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.detailGridView_RowStyle);
            this.detailGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.detailGridView_PopupMenuShowing);
            // 
            // DetailPrintName
            // 
            this.DetailPrintName.Caption = "Чек";
            this.DetailPrintName.FieldName = "Name";
            this.DetailPrintName.Name = "DetailPrintName";
            this.DetailPrintName.OptionsColumn.AllowEdit = false;
            this.DetailPrintName.OptionsColumn.AllowFocus = false;
            this.DetailPrintName.OptionsColumn.ReadOnly = true;
            this.DetailPrintName.Visible = true;
            this.DetailPrintName.VisibleIndex = 0;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.detailGridView;
            gridLevelNode1.RelationName = "Details";
            this.mainGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.mainGridControl.Location = new System.Drawing.Point(0, 37);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(866, 454);
            this.mainGridControl.TabIndex = 0;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView,
            this.detailGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnPrintWorkId,
            this.columnPrintWorkOrderId,
            this.columnPrintWorkOrderName,
            this.columnPrintWorkOrderTime,
            this.columnPrintWorkCreated,
            this.columnPrintWorksLastTry,
            this.columnPrintWorkState});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
            this.mainGridView.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.mainGridView_MasterRowGetChildList);
            this.mainGridView.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.mainGridView_MasterRowGetRelationName);
            this.mainGridView.MasterRowGetRelationDisplayCaption += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.mainGridView_MasterRowGetRelationDisplayCaption);
            this.mainGridView.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.mainGridView_MasterRowGetRelationCount);
            this.mainGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.mainGridView_PopupMenuShowing);
            // 
            // columnPrintWorkId
            // 
            this.columnPrintWorkId.Caption = "ИД";
            this.columnPrintWorkId.FieldName = "ID";
            this.columnPrintWorkId.Name = "columnPrintWorkId";
            this.columnPrintWorkId.OptionsColumn.AllowEdit = false;
            this.columnPrintWorkId.OptionsColumn.AllowFocus = false;
            this.columnPrintWorkId.OptionsColumn.ReadOnly = true;
            this.columnPrintWorkId.Visible = true;
            this.columnPrintWorkId.VisibleIndex = 0;
            // 
            // columnPrintWorkOrderId
            // 
            this.columnPrintWorkOrderId.Caption = "ИД заказа";
            this.columnPrintWorkOrderId.FieldName = "OrderId";
            this.columnPrintWorkOrderId.Name = "columnPrintWorkOrderId";
            this.columnPrintWorkOrderId.OptionsColumn.AllowEdit = false;
            this.columnPrintWorkOrderId.OptionsColumn.AllowFocus = false;
            this.columnPrintWorkOrderId.OptionsColumn.ReadOnly = true;
            this.columnPrintWorkOrderId.Visible = true;
            this.columnPrintWorkOrderId.VisibleIndex = 1;
            // 
            // columnPrintWorkOrderName
            // 
            this.columnPrintWorkOrderName.Caption = "Код заказа";
            this.columnPrintWorkOrderName.FieldName = "OrderName";
            this.columnPrintWorkOrderName.Name = "columnPrintWorkOrderName";
            this.columnPrintWorkOrderName.OptionsColumn.AllowEdit = false;
            this.columnPrintWorkOrderName.OptionsColumn.AllowFocus = false;
            this.columnPrintWorkOrderName.OptionsColumn.ReadOnly = true;
            this.columnPrintWorkOrderName.Visible = true;
            this.columnPrintWorkOrderName.VisibleIndex = 2;
            // 
            // columnPrintWorkOrderTime
            // 
            this.columnPrintWorkOrderTime.Caption = "Время заказа";
            this.columnPrintWorkOrderTime.FieldName = "OrderTime";
            this.columnPrintWorkOrderTime.Name = "columnPrintWorkOrderTime";
            this.columnPrintWorkOrderTime.OptionsColumn.AllowEdit = false;
            this.columnPrintWorkOrderTime.OptionsColumn.AllowFocus = false;
            this.columnPrintWorkOrderTime.OptionsColumn.ReadOnly = true;
            this.columnPrintWorkOrderTime.Visible = true;
            this.columnPrintWorkOrderTime.VisibleIndex = 3;
            // 
            // columnPrintWorkCreated
            // 
            this.columnPrintWorkCreated.Caption = "Отправлено на печать";
            this.columnPrintWorkCreated.FieldName = "Created";
            this.columnPrintWorkCreated.Name = "columnPrintWorkCreated";
            this.columnPrintWorkCreated.OptionsColumn.AllowEdit = false;
            this.columnPrintWorkCreated.OptionsColumn.AllowFocus = false;
            this.columnPrintWorkCreated.OptionsColumn.ReadOnly = true;
            this.columnPrintWorkCreated.Visible = true;
            this.columnPrintWorkCreated.VisibleIndex = 4;
            // 
            // columnPrintWorksLastTry
            // 
            this.columnPrintWorksLastTry.Caption = "Обработано";
            this.columnPrintWorksLastTry.FieldName = "LastTry";
            this.columnPrintWorksLastTry.Name = "columnPrintWorksLastTry";
            this.columnPrintWorksLastTry.OptionsColumn.AllowEdit = false;
            this.columnPrintWorksLastTry.OptionsColumn.AllowFocus = false;
            this.columnPrintWorksLastTry.OptionsColumn.ReadOnly = true;
            this.columnPrintWorksLastTry.Visible = true;
            this.columnPrintWorksLastTry.VisibleIndex = 5;
            // 
            // columnPrintWorkState
            // 
            this.columnPrintWorkState.Caption = "Статус";
            this.columnPrintWorkState.FieldName = "State";
            this.columnPrintWorkState.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.columnPrintWorkState.Name = "columnPrintWorkState";
            this.columnPrintWorkState.OptionsColumn.AllowEdit = false;
            this.columnPrintWorkState.OptionsColumn.AllowFocus = false;
            this.columnPrintWorkState.OptionsColumn.ReadOnly = true;
            this.columnPrintWorkState.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.None, "State", "test"),
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.None, "State", "test2"),
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.None, "State", "test3")});
            this.columnPrintWorkState.Visible = true;
            this.columnPrintWorkState.VisibleIndex = 6;
            // 
            // linqServerModeSourcePrintWorks
            // 
            this.linqServerModeSourcePrintWorks.KeyExpression = "ID";
            // 
            // WorksContextMenu
            // 
            this.WorksContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RepeatWorkMenuItem});
            this.WorksContextMenu.Name = "WorksContextMenu";
            this.WorksContextMenu.Size = new System.Drawing.Size(174, 26);
            // 
            // RepeatWorkMenuItem
            // 
            this.RepeatWorkMenuItem.Name = "RepeatWorkMenuItem";
            this.RepeatWorkMenuItem.Size = new System.Drawing.Size(173, 22);
            this.RepeatWorkMenuItem.Text = "Повторить печать";
            // 
            // DetailContextMenu
            // 
            this.DetailContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RepeatDetailMenuItem});
            this.DetailContextMenu.Name = "DetailContextMenu";
            this.DetailContextMenu.Size = new System.Drawing.Size(174, 26);
            // 
            // RepeatDetailMenuItem
            // 
            this.RepeatDetailMenuItem.Name = "RepeatDetailMenuItem";
            this.RepeatDetailMenuItem.Size = new System.Drawing.Size(173, 22);
            this.RepeatDetailMenuItem.Text = "Повторить печать";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblDate);
            this.panel1.Controls.Add(this.dTimeDateOfData);
            this.panel1.Controls.Add(this.nudGenerateAmount);
            this.panel1.Controls.Add(this.btnGenerate);
            this.panel1.Controls.Add(this.lblLastOrders);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(866, 37);
            this.panel1.TabIndex = 2;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(411, 9);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(49, 13);
            this.lblDate.TabIndex = 13;
            this.lblDate.Text = "На дату:";
            // 
            // dTimeDateOfData
            // 
            this.dTimeDateOfData.Location = new System.Drawing.Point(463, 7);
            this.dTimeDateOfData.Name = "dTimeDateOfData";
            this.dTimeDateOfData.Size = new System.Drawing.Size(109, 20);
            this.dTimeDateOfData.TabIndex = 12;
            // 
            // nudGenerateAmount
            // 
            this.nudGenerateAmount.Location = new System.Drawing.Point(285, 7);
            this.nudGenerateAmount.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudGenerateAmount.Name = "nudGenerateAmount";
            this.nudGenerateAmount.Size = new System.Drawing.Size(120, 20);
            this.nudGenerateAmount.TabIndex = 11;
            this.nudGenerateAmount.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(579, 5);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(126, 23);
            this.btnGenerate.TabIndex = 10;
            this.btnGenerate.Text = "Получить данные";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // lblLastOrders
            // 
            this.lblLastOrders.AutoSize = true;
            this.lblLastOrders.Location = new System.Drawing.Point(12, 9);
            this.lblLastOrders.Name = "lblLastOrders";
            this.lblLastOrders.Size = new System.Drawing.Size(267, 13);
            this.lblLastOrders.TabIndex = 9;
            this.lblLastOrders.Text = "Какое количество последних заказов отобразить?";
            // 
            // PrintingMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 491);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.panel1);
            this.Name = "PrintingMonitor";
            this.Text = "Задания для печати";
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourcePrintWorks)).EndInit();
            this.WorksContextMenu.ResumeLayout(false);
            this.DetailContextMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGenerateAmount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView detailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrintWorkId;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrintWorkState;
        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.Data.Linq.LinqServerModeSource linqServerModeSourcePrintWorks;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrintWorkOrderId;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrintWorkOrderName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrintWorkOrderTime;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrintWorkCreated;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrintWorksLastTry;
        private DevExpress.XtraGrid.Columns.GridColumn DetailPrintName;
        private System.Windows.Forms.ContextMenuStrip WorksContextMenu;
        private System.Windows.Forms.ContextMenuStrip DetailContextMenu;
        private System.Windows.Forms.ToolStripMenuItem RepeatWorkMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RepeatDetailMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown nudGenerateAmount;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Label lblLastOrders;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.DateTimePicker dTimeDateOfData;
    }
}