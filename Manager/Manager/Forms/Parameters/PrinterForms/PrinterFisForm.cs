﻿using System;
using System.Configuration;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Parameters.PrinterForms
{

	
	
	public partial class PrinterFisForm : Form
    {
        public PrinterFisForm()
        {
            InitializeComponent();
            textBoxIpAddress.Text = Settings.Default.PrinterIp;
            numericUpDownPort.Value = Settings.Default.PrinterPort;
        }
        

        private void buttonReportX_Click(object sender, EventArgs e)
        {
            PrintingManagerElves.PrintX(Settings.Default.PrinterPort, Settings.Default.PrinterIp);
        }

        private void buttonReportZ_Click(object sender, EventArgs e)
        {
            PrintingManagerElves.PrintZ(Settings.Default.PrinterPort, Settings.Default.PrinterIp);
        }

        private void buttonOpenSession_Click(object sender, EventArgs e)
        {
            PrintingManagerElves.OpenSession(Settings.Default.PrinterPort, Settings.Default.PrinterIp);
        }

        private void buttonCloseSession_Click(object sender, EventArgs e)
        {
            PrintingManagerElves.CloseSession(Settings.Default.PrinterPort, Settings.Default.PrinterIp);
        }

        private void buttonCheckConnection_Click(object sender, EventArgs e)
        {
            PrintingManagerElves.CheckConnection(Settings.Default.PrinterPort, Settings.Default.PrinterIp);
        }

        private void textBoxIpAddress_TextChanged(object sender, EventArgs e)
        {
            
        }
        
        private void numericUpDownPort_ValueChanged(object sender, EventArgs e)
        {
        }
    }
}
