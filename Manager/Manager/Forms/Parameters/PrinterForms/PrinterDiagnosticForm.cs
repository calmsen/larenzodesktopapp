﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Printers;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Printers;

namespace LaRenzo.Forms.Parameters.PrinterForms
{
    public partial class PrinterDiagnosticForm : Form
    {
        public List<Printer> Printers { get; set; }

        public PrinterDiagnosticForm()
        {
            InitializeComponent();
        }

        private void PrinterDiagnosticForm_Load(object sender, EventArgs e)
        {
            Printers = Content.PrintWorksRepository.GetAllPrinters();

            checkedListBox1.Items.Clear();
            checkedListBox1.DisplayMember = "Name";

            foreach (var printer in Printers)
            {
                checkedListBox1.Items.Add(printer);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (var i = 0; i < checkedListBox1.Items.Count; i++)
            {
                checkedListBox1.SetItemChecked(i, true);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (var i = 0; i < checkedListBox1.Items.Count; i++)
            {
                checkedListBox1.SetItemChecked(i, false);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var document = new PrintDocument();
            document.PrintPage += (send, args) =>
            {
                int i = 0;
                int fontsize = 1;
                int shift = 1;
                foreach (var data in GetRandomString().Take(int.Parse(textBox1.Text)))
                {
                    fontsize += shift;

                    args.Graphics.DrawString(data, new Font("Tahoma", fontsize), Brushes.Black, new PointF(0, i++ * 20));

                    if (fontsize > 25 || fontsize < 2) shift *= -1;
                    
                }

            };

            foreach (var item in checkedListBox1.CheckedItems)
            {
                var printer = item as Printer;
                
                if (printer == null) continue;

                var settings = new PrinterSettings { PrinterName = printer.Name };
                document.PrinterSettings = settings;
                document.Print();

            }
           

        }

        private IEnumerable<string> GetRandomString(int length = 15)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            
            while (true)
                yield return new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
