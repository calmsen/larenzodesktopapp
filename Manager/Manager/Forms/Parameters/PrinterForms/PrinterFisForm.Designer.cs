﻿using DevExpress.Data;

namespace LaRenzo.Forms.Parameters.PrinterForms
{
    partial class PrinterFisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonReportX = new System.Windows.Forms.Button();
            this.buttonReportZ = new System.Windows.Forms.Button();
            this.buttonOpenSession = new System.Windows.Forms.Button();
            this.buttonCloseSession = new System.Windows.Forms.Button();
            this.buttonCheckConnection = new System.Windows.Forms.Button();
            this.textBoxIpAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownPort = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPort)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonReportX
            // 
            this.buttonReportX.Location = new System.Drawing.Point(13, 13);
            this.buttonReportX.Name = "buttonReportX";
            this.buttonReportX.Size = new System.Drawing.Size(596, 38);
            this.buttonReportX.TabIndex = 0;
            this.buttonReportX.Text = "Напечатать X отчёт (Отчёт без отчистки)";
            this.buttonReportX.UseVisualStyleBackColor = true;
            this.buttonReportX.Click += new System.EventHandler(this.buttonReportX_Click);
            // 
            // buttonReportZ
            // 
            this.buttonReportZ.Location = new System.Drawing.Point(13, 57);
            this.buttonReportZ.Name = "buttonReportZ";
            this.buttonReportZ.Size = new System.Drawing.Size(596, 38);
            this.buttonReportZ.TabIndex = 1;
            this.buttonReportZ.Text = "Напечатать Z отчёт (Отчёт c отчисткой)";
            this.buttonReportZ.UseVisualStyleBackColor = true;
            this.buttonReportZ.Click += new System.EventHandler(this.buttonReportZ_Click);
            // 
            // buttonOpenSession
            // 
            this.buttonOpenSession.Location = new System.Drawing.Point(13, 101);
            this.buttonOpenSession.Name = "buttonOpenSession";
            this.buttonOpenSession.Size = new System.Drawing.Size(596, 38);
            this.buttonOpenSession.TabIndex = 2;
            this.buttonOpenSession.Text = "Открыть Сессию";
            this.buttonOpenSession.UseVisualStyleBackColor = true;
            this.buttonOpenSession.Click += new System.EventHandler(this.buttonOpenSession_Click);
            // 
            // buttonCloseSession
            // 
            this.buttonCloseSession.Location = new System.Drawing.Point(13, 145);
            this.buttonCloseSession.Name = "buttonCloseSession";
            this.buttonCloseSession.Size = new System.Drawing.Size(596, 38);
            this.buttonCloseSession.TabIndex = 3;
            this.buttonCloseSession.Text = "Закрыть Сессию";
            this.buttonCloseSession.UseVisualStyleBackColor = true;
            this.buttonCloseSession.Click += new System.EventHandler(this.buttonCloseSession_Click);
            // 
            // buttonCheckConnection
            // 
            this.buttonCheckConnection.Location = new System.Drawing.Point(13, 553);
            this.buttonCheckConnection.Name = "buttonCheckConnection";
            this.buttonCheckConnection.Size = new System.Drawing.Size(596, 38);
            this.buttonCheckConnection.TabIndex = 4;
            this.buttonCheckConnection.Text = "Проверить связь";
            this.buttonCheckConnection.UseVisualStyleBackColor = true;
            this.buttonCheckConnection.Click += new System.EventHandler(this.buttonCheckConnection_Click);
            // 
            // textBoxIpAddress
            // 
            this.textBoxIpAddress.Location = new System.Drawing.Point(97, 451);
            this.textBoxIpAddress.Name = "textBoxIpAddress";
            this.textBoxIpAddress.Size = new System.Drawing.Size(511, 22);
            this.textBoxIpAddress.TabIndex = 5;
            this.textBoxIpAddress.TextChanged += new System.EventHandler(this.textBoxIpAddress_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 454);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ip Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 491);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Port:";
            // 
            // numericUpDownPort
            // 
            this.numericUpDownPort.Location = new System.Drawing.Point(97, 489);
            this.numericUpDownPort.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownPort.Name = "numericUpDownPort";
            this.numericUpDownPort.Size = new System.Drawing.Size(511, 22);
            this.numericUpDownPort.TabIndex = 9;
            this.numericUpDownPort.ValueChanged += new System.EventHandler(this.numericUpDownPort_ValueChanged);
            // 
            // PrinterFisForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 603);
            this.Controls.Add(this.numericUpDownPort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxIpAddress);
            this.Controls.Add(this.buttonCheckConnection);
            this.Controls.Add(this.buttonCloseSession);
            this.Controls.Add(this.buttonOpenSession);
            this.Controls.Add(this.buttonReportZ);
            this.Controls.Add(this.buttonReportX);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PrinterFisForm";
            this.Text = "Управление фискальным принтером";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPort)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonReportX;
        private System.Windows.Forms.Button buttonReportZ;
        private System.Windows.Forms.Button buttonOpenSession;
        private System.Windows.Forms.Button buttonCloseSession;
        private System.Windows.Forms.Button buttonCheckConnection;
        private System.Windows.Forms.TextBox textBoxIpAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownPort;
    }
}