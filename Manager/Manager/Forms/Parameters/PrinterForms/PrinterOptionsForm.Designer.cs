﻿using DevExpress.Data;

namespace LaRenzo.Forms.Parameters.PrinterForms
{
    partial class PrinterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.lbPrinters = new System.Windows.Forms.ListBox();
            this.btnAddPrinterTask = new System.Windows.Forms.Button();
            this.lblCheckTipe = new System.Windows.Forms.Label();
            this.cbCheckType = new System.Windows.Forms.ComboBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btnAddPrinter = new System.Windows.Forms.ToolStripButton();
            this.btnCheckPrinter = new System.Windows.Forms.ToolStripButton();
            this.btnRemovePrinter = new System.Windows.Forms.ToolStripButton();
            this.gcPrinterTasks = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnPrinterName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCheckTypeCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.idColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnInstructionAlias = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcPrinterTasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.toolStrip2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcPrinterTasks);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(466, 490);
            this.splitContainerControl1.SplitterPosition = 234;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 31);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.lbPrinters);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.btnAddPrinterTask);
            this.splitContainerControl2.Panel2.Controls.Add(this.lblCheckTipe);
            this.splitContainerControl2.Panel2.Controls.Add(this.cbCheckType);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(466, 203);
            this.splitContainerControl2.SplitterPosition = 295;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // lbPrinters
            // 
            this.lbPrinters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbPrinters.FormattingEnabled = true;
            this.lbPrinters.Location = new System.Drawing.Point(0, 0);
            this.lbPrinters.Name = "lbPrinters";
            this.lbPrinters.Size = new System.Drawing.Size(295, 203);
            this.lbPrinters.TabIndex = 0;
            // 
            // btnAddPrinterTask
            // 
            this.btnAddPrinterTask.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddPrinterTask.Location = new System.Drawing.Point(0, 148);
            this.btnAddPrinterTask.Name = "btnAddPrinterTask";
            this.btnAddPrinterTask.Size = new System.Drawing.Size(166, 55);
            this.btnAddPrinterTask.TabIndex = 2;
            this.btnAddPrinterTask.Text = "Добавить задачу";
            this.btnAddPrinterTask.UseVisualStyleBackColor = true;
            this.btnAddPrinterTask.Click += new System.EventHandler(this.btnAddPrinterTask_Click);
            // 
            // lblCheckTipe
            // 
            this.lblCheckTipe.AutoSize = true;
            this.lblCheckTipe.Location = new System.Drawing.Point(9, 11);
            this.lblCheckTipe.Name = "lblCheckTipe";
            this.lblCheckTipe.Size = new System.Drawing.Size(104, 13);
            this.lblCheckTipe.TabIndex = 1;
            this.lblCheckTipe.Text = "Выберите вид чека";
            // 
            // cbCheckType
            // 
            this.cbCheckType.FormattingEnabled = true;
            this.cbCheckType.Location = new System.Drawing.Point(12, 27);
            this.cbCheckType.Name = "cbCheckType";
            this.cbCheckType.Size = new System.Drawing.Size(142, 21);
            this.cbCheckType.TabIndex = 0;
            // 
            // toolStrip2
            // 
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddPrinter,
            this.btnCheckPrinter,
            this.btnRemovePrinter});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(466, 31);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btnAddPrinter
            // 
            this.btnAddPrinter.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnAddPrinter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddPrinter.Name = "btnAddPrinter";
            this.btnAddPrinter.Size = new System.Drawing.Size(136, 28);
            this.btnAddPrinter.Text = "Добавить принтер";
            this.btnAddPrinter.Click += new System.EventHandler(this.btnAddPrinter_Click);
            // 
            // btnCheckPrinter
            // 
            this.btnCheckPrinter.Image = global::LaRenzo.Properties.Resources.button_ok_4587;
            this.btnCheckPrinter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCheckPrinter.Name = "btnCheckPrinter";
            this.btnCheckPrinter.Size = new System.Drawing.Size(144, 28);
            this.btnCheckPrinter.Text = "Проверить принтер";
            this.btnCheckPrinter.Visible = false;
            this.btnCheckPrinter.Click += new System.EventHandler(this.btnCheckPrinter_Click);
            // 
            // btnRemovePrinter
            // 
            this.btnRemovePrinter.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btnRemovePrinter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRemovePrinter.Name = "btnRemovePrinter";
            this.btnRemovePrinter.Size = new System.Drawing.Size(128, 28);
            this.btnRemovePrinter.Text = "Удалить принтер";
            this.btnRemovePrinter.Click += new System.EventHandler(this.btnRemovePrinter_Click);
            // 
            // gcPrinterTasks
            // 
            this.gcPrinterTasks.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcPrinterTasks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcPrinterTasks.Location = new System.Drawing.Point(0, 0);
            this.gcPrinterTasks.MainView = this.gridView1;
            this.gcPrinterTasks.Name = "gcPrinterTasks";
            this.gcPrinterTasks.Size = new System.Drawing.Size(466, 251);
            this.gcPrinterTasks.TabIndex = 1;
            this.gcPrinterTasks.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gcPrinterTasks.DoubleClick += new System.EventHandler(this.gcPrinterTasks_DoubleClick);
            this.gcPrinterTasks.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gcPrinterTasks_KeyDown);
            this.gcPrinterTasks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gcPrinterTasks_KeyPress);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnPrinterName,
            this.columnCheckTypeCol,
            this.idColumn,
            this.columnInstructionAlias});
            this.gridView1.GridControl = this.gcPrinterTasks;
            this.gridView1.Name = "gridView1";
            this.gridView1.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
            // 
            // columnPrinterName
            // 
            this.columnPrinterName.Caption = "Принтер";
            this.columnPrinterName.FieldName = "Name";
            this.columnPrinterName.Name = "columnPrinterName";
            this.columnPrinterName.OptionsColumn.AllowEdit = false;
            this.columnPrinterName.OptionsColumn.ReadOnly = true;
            this.columnPrinterName.Visible = true;
            this.columnPrinterName.VisibleIndex = 0;
            // 
            // columnCheckTypeCol
            // 
            this.columnCheckTypeCol.Caption = "Вид чека";
            this.columnCheckTypeCol.FieldName = "Title";
            this.columnCheckTypeCol.Name = "columnCheckTypeCol";
            this.columnCheckTypeCol.OptionsColumn.AllowEdit = false;
            this.columnCheckTypeCol.OptionsColumn.ReadOnly = true;
            this.columnCheckTypeCol.Visible = true;
            this.columnCheckTypeCol.VisibleIndex = 1;
            // 
            // idColumn
            // 
            this.idColumn.Caption = "ID";
            this.idColumn.FieldName = "ID";
            this.idColumn.Name = "idColumn";
            // 
            // columnInstructionAlias
            // 
            this.columnInstructionAlias.Caption = "Команда";
            this.columnInstructionAlias.FieldName = "InstructionAlias";
            this.columnInstructionAlias.Name = "columnInstructionAlias";
            // 
            // PrinterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 490);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "PrinterForm";
            this.Text = "Управление принтерами";
            this.Load += new System.EventHandler(this.PrinterForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcPrinterTasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private System.Windows.Forms.ListBox lbPrinters;
        private System.Windows.Forms.Button btnAddPrinterTask;
        private System.Windows.Forms.Label lblCheckTipe;
        private System.Windows.Forms.ComboBox cbCheckType;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btnAddPrinter;
        private System.Windows.Forms.ToolStripButton btnCheckPrinter;
        private System.Windows.Forms.ToolStripButton btnRemovePrinter;
        private DevExpress.XtraGrid.GridControl gcPrinterTasks;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrinterName;
        private DevExpress.XtraGrid.Columns.GridColumn columnCheckTypeCol;
        private DevExpress.XtraGrid.Columns.GridColumn idColumn;
        private DevExpress.XtraGrid.Columns.GridColumn columnInstructionAlias;
    }
}