﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Parameters.PointOfSale
{
    public partial class PointOfSaleList : Form
    {

		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary>  </summary>
		////========================
		public PointOfSaleList()
        {
            InitializeComponent();
			RefreshList();
		}

		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary>  </summary>
		////========================
		private void RefreshList()
        {
            var first = mainGridView.TopRowIndex;
            var id = mainGridView.GetDataSourceRowIndex(mainGridView.FocusedRowHandle);
			
			mainGridControl.DataSource = Content.PointOfSaleRepository.GetItems();
            mainGridView.FocusedRowHandle = mainGridView.GetRowHandle(id);
            mainGridView.MakeRowVisible(first);
        }


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Клик по "Добавить" </summary>
		////=======================================
		private void AddButtonClick(object sender, EventArgs e)
		{
			OpenDetails();
		}
		

		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Клик по "Обновить" </summary>
		////=======================================
		private void RefreshButtonClick(object sender, EventArgs e)
        {
			RefreshList();
        }


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Клик по "Скопировать" </summary>
		////==========================================
		private void CopyButtonClick(object sender, EventArgs e)
        {
			var selectedRow = (DataRepository.Entities.PointOfSale)mainGridView.GetFocusedRow();
            if (selectedRow != null)
            {
                OpenDetails(selectedRow);
            }
            else
            {
                MessageBox.Show(Resources.PartnerIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Клик по "Удалить" </summary>
		////======================================
		private void DeleteButtonClick(object sender, EventArgs e)
		{
			if (MessageBox.Show(@"Вы действительно хотите удалить точку продаж?", Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				var selectedRow = (DataRepository.Entities.Settings.Brand)mainGridView.GetFocusedRow();
				if (selectedRow != null)
				{
					Content.BrandManager.Remove(selectedRow.ID);
					RefreshList();
				}
				else
				{
					MessageBox.Show(@"Выберите точку продаж");
				}
			}
		}


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Клик по "Редактировать" </summary>
		////============================================
		private void EditButtonClick(object sender, EventArgs e)
		{
			try
			{
				var selectedRow = (DataRepository.Entities.PointOfSale)mainGridView.GetFocusedRow();
				if (selectedRow != null)
				{
					OpenDetails(selectedRow);
				}
				else
				{
					MessageBox.Show(Resources.PartnerIsNotSelected, Resources.WarningTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Открыть форму деталей </summary>
		////===========================================
		private void OpenDetails(DataRepository.Entities.PointOfSale pointOfSale = null)
		{
            var editForm = new PointOfSaleEdit(pointOfSale);

			if (editForm.ShowDialog() == DialogResult.OK)
			{
				RefreshList();
			}
        }


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Двойной клик по элементу </summary>
		////=============================================
		private void MainGridViewDoubleClick(object sender, EventArgs e)
        {
            var view = (GridView) sender;

            var pt = view.GridControl.PointToClient(MousePosition);

            GridHitInfo info = view.CalcHitInfo(pt);
            if (view.IsDataRow(info.RowHandle))
            {
				var selectedItem = (DataRepository.Entities.PointOfSale)view.GetRow(info.RowHandle);
				OpenDetails(selectedItem);
            }
        }
		
		
		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary>  </summary>
		////========================
        private void MainGridViewRowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary>  </summary>
		////========================
        private void SearchButtonClick(object sender, EventArgs e)
        {
            mainGridView.FindPanelVisible = true;
        }
	}
}
