﻿namespace LaRenzo.Forms.Parameters.TransferForms
{
    partial class TransferChiefExpert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TransferWizard = new DevExpress.XtraWizard.WizardControl();
            this.StartLoad = new DevExpress.XtraWizard.WelcomeWizardPage();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblThird = new System.Windows.Forms.Label();
            this.lblSecond = new System.Windows.Forms.Label();
            this.lblFirst = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblWarning = new System.Windows.Forms.Label();
            this.lblFileTransfer = new System.Windows.Forms.Label();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.topPanel = new System.Windows.Forms.Panel();
            this.lblTransfer = new System.Windows.Forms.Label();
            this.LoadCompleated = new DevExpress.XtraWizard.CompletionWizardPage();
            this.lblCompleated = new System.Windows.Forms.Label();
            this.IngridientsLoad = new DevExpress.XtraWizard.WizardPage();
            this.mainIngridientGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainIngridientGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnIngridientId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIngridient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EditRowButton = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.columnIngridientExternal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIngridientIdExternal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EditFolderButton = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.columnProductCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlMainIngr = new System.Windows.Forms.Panel();
            this.cmbCatalogFromIngr = new System.Windows.Forms.CheckBox();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.UseConstFolder = new System.Windows.Forms.CheckBox();
            this.lblIfNotComparer = new System.Windows.Forms.Label();
            this.cmbAction = new System.Windows.Forms.ComboBox();
            this.lblNewIngrCatalog = new System.Windows.Forms.Label();
            this.cmbFolderForNew = new System.Windows.Forms.ComboBox();
            this.lblIngridientsCountValue = new System.Windows.Forms.Label();
            this.lblIngridientsCount = new System.Windows.Forms.Label();
            this.FirstLoadPage = new DevExpress.XtraWizard.WizardPage();
            this.progressLoadFromFile = new DevExpress.XtraWaitForm.ProgressPanel();
            this.SaveIngridients = new DevExpress.XtraWizard.WizardPage();
            this.progressSaveIngridients = new DevExpress.XtraWaitForm.ProgressPanel();
            this.ProductLoad = new DevExpress.XtraWizard.WizardPage();
            this.mainDishGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainDishGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnDishId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDishName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DishEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.columnDishChefName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDishChefId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDishChefCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DishCategoryEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlMainDish = new System.Windows.Forms.Panel();
            this.chkDishFromComparer = new System.Windows.Forms.CheckBox();
            this.btnSelectDishCategory = new System.Windows.Forms.Button();
            this.chkStaticDishCatalog = new System.Windows.Forms.CheckBox();
            this.lblNotDishConf = new System.Windows.Forms.Label();
            this.cmbDishAction = new System.Windows.Forms.ComboBox();
            this.lblNewDishCatalog = new System.Windows.Forms.Label();
            this.cmbNewDishCatalog = new System.Windows.Forms.ComboBox();
            this.lblDishCount = new System.Windows.Forms.Label();
            this.lblDishes = new System.Windows.Forms.Label();
            this.DishLoading = new DevExpress.XtraWizard.WizardPage();
            this.progressDishLoading = new DevExpress.XtraWaitForm.ProgressPanel();
            this.IngridientsLoaded = new DevExpress.XtraWizard.WizardPage();
            this.label1 = new System.Windows.Forms.Label();
            this.DishLoading1 = new DevExpress.XtraWizard.WizardPage();
            this.progressDishLoad = new DevExpress.XtraWaitForm.ProgressPanel();
            this.AddressDlg = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.TransferWizard)).BeginInit();
            this.TransferWizard.SuspendLayout();
            this.StartLoad.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.LoadCompleated.SuspendLayout();
            this.IngridientsLoad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainIngridientGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainIngridientGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditRowButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditFolderButton)).BeginInit();
            this.pnlMainIngr.SuspendLayout();
            this.FirstLoadPage.SuspendLayout();
            this.SaveIngridients.SuspendLayout();
            this.ProductLoad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDishGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDishGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DishEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DishCategoryEdit)).BeginInit();
            this.pnlMainDish.SuspendLayout();
            this.DishLoading.SuspendLayout();
            this.IngridientsLoaded.SuspendLayout();
            this.DishLoading1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TransferWizard
            // 
            this.TransferWizard.CancelText = "Отмена";
            this.TransferWizard.Controls.Add(this.StartLoad);
            this.TransferWizard.Controls.Add(this.LoadCompleated);
            this.TransferWizard.Controls.Add(this.IngridientsLoad);
            this.TransferWizard.Controls.Add(this.FirstLoadPage);
            this.TransferWizard.Controls.Add(this.SaveIngridients);
            this.TransferWizard.Controls.Add(this.ProductLoad);
            this.TransferWizard.Controls.Add(this.DishLoading);
            this.TransferWizard.Controls.Add(this.IngridientsLoaded);
            this.TransferWizard.Controls.Add(this.DishLoading1);
            this.TransferWizard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransferWizard.FinishText = "&Загрузить";
            this.TransferWizard.HelpText = "&Помощь";
            this.TransferWizard.Location = new System.Drawing.Point(0, 0);
            this.TransferWizard.Name = "TransferWizard";
            this.TransferWizard.NextText = "&Далее >";
            this.TransferWizard.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.StartLoad,
            this.FirstLoadPage,
            this.IngridientsLoad,
            this.SaveIngridients,
            this.IngridientsLoaded,
            this.DishLoading1,
            this.ProductLoad,
            this.DishLoading,
            this.LoadCompleated});
            this.TransferWizard.PreviousText = "< &Назад";
            this.TransferWizard.Size = new System.Drawing.Size(802, 481);
            this.TransferWizard.Text = "Назад";
            this.TransferWizard.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero;
            this.TransferWizard.CancelClick += new System.ComponentModel.CancelEventHandler(this.TransferWizard_CancelClick);
            this.TransferWizard.NextClick += new DevExpress.XtraWizard.WizardCommandButtonClickEventHandler(this.TransferWizard_NextClick);
            // 
            // StartLoad
            // 
            this.StartLoad.Controls.Add(this.mainPanel);
            this.StartLoad.Name = "StartLoad";
            this.StartLoad.Size = new System.Drawing.Size(742, 319);
            this.StartLoad.Text = "Загрузка данных из программы Шеф Эксперт";
            // 
            // mainPanel
            // 
            this.mainPanel.Controls.Add(this.btnClear);
            this.mainPanel.Controls.Add(this.lblThird);
            this.mainPanel.Controls.Add(this.lblSecond);
            this.mainPanel.Controls.Add(this.lblFirst);
            this.mainPanel.Controls.Add(this.lblTitle);
            this.mainPanel.Controls.Add(this.lblWarning);
            this.mainPanel.Controls.Add(this.lblFileTransfer);
            this.mainPanel.Controls.Add(this.btnSelectFile);
            this.mainPanel.Controls.Add(this.txtPath);
            this.mainPanel.Controls.Add(this.topPanel);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(742, 319);
            this.mainPanel.TabIndex = 0;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Image = global::LaRenzo.Properties.Resources.cross;
            this.btnClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClear.Location = new System.Drawing.Point(713, 112);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(24, 24);
            this.btnClear.TabIndex = 11;
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblThird
            // 
            this.lblThird.AutoSize = true;
            this.lblThird.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblThird.Location = new System.Drawing.Point(7, 95);
            this.lblThird.Name = "lblThird";
            this.lblThird.Size = new System.Drawing.Size(398, 13);
            this.lblThird.TabIndex = 10;
            this.lblThird.Text = "3) Выбрать выгрузку по внутренним кодам и нажать на кнопку \"Выгрузить\".";
            // 
            // lblSecond
            // 
            this.lblSecond.AutoSize = true;
            this.lblSecond.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblSecond.Location = new System.Drawing.Point(7, 76);
            this.lblSecond.Name = "lblSecond";
            this.lblSecond.Size = new System.Drawing.Size(480, 13);
            this.lblSecond.TabIndex = 9;
            this.lblSecond.Text = "2) Во вкладке универсальный обмен данными выбрать необходимые элементы для обмена" +
    ".";
            // 
            // lblFirst
            // 
            this.lblFirst.AutoSize = true;
            this.lblFirst.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblFirst.Location = new System.Drawing.Point(7, 58);
            this.lblFirst.Name = "lblFirst";
            this.lblFirst.Size = new System.Drawing.Size(350, 13);
            this.lblFirst.TabIndex = 8;
            this.lblFirst.Text = "1) Перейти на вкладку обмен данными в программе Шеф эксперт .";
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblTitle.Location = new System.Drawing.Point(7, 40);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(732, 30);
            this.lblTitle.TabIndex = 7;
            this.lblTitle.Text = "Для корректной загрузки из программы Шеф Эксперт:";
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWarning.ForeColor = System.Drawing.Color.Firebrick;
            this.lblWarning.Location = new System.Drawing.Point(7, 27);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(70, 13);
            this.lblWarning.TabIndex = 6;
            this.lblWarning.Text = "Внимание!";
            // 
            // lblFileTransfer
            // 
            this.lblFileTransfer.AutoSize = true;
            this.lblFileTransfer.Location = new System.Drawing.Point(7, 118);
            this.lblFileTransfer.Name = "lblFileTransfer";
            this.lblFileTransfer.Size = new System.Drawing.Size(80, 13);
            this.lblFileTransfer.TabIndex = 2;
            this.lblFileTransfer.Text = "Файл обмена:";
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectFile.Location = new System.Drawing.Point(683, 112);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(24, 24);
            this.btnSelectFile.TabIndex = 1;
            this.btnSelectFile.Text = "...";
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // txtPath
            // 
            this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPath.Location = new System.Drawing.Point(93, 115);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(584, 20);
            this.txtPath.TabIndex = 0;
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.lblTransfer);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(742, 24);
            this.topPanel.TabIndex = 5;
            // 
            // lblTransfer
            // 
            this.lblTransfer.AutoSize = true;
            this.lblTransfer.Location = new System.Drawing.Point(7, 5);
            this.lblTransfer.Name = "lblTransfer";
            this.lblTransfer.Size = new System.Drawing.Size(93, 13);
            this.lblTransfer.TabIndex = 5;
            this.lblTransfer.Text = "Обмен данными:";
            // 
            // LoadCompleated
            // 
            this.LoadCompleated.AllowBack = false;
            this.LoadCompleated.AllowCancel = false;
            this.LoadCompleated.Controls.Add(this.lblCompleated);
            this.LoadCompleated.Name = "LoadCompleated";
            this.LoadCompleated.Size = new System.Drawing.Size(742, 319);
            this.LoadCompleated.Text = "Загрузка успешно завершена....";
            // 
            // lblCompleated
            // 
            this.lblCompleated.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCompleated.AutoSize = true;
            this.lblCompleated.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCompleated.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblCompleated.Location = new System.Drawing.Point(250, 150);
            this.lblCompleated.Name = "lblCompleated";
            this.lblCompleated.Size = new System.Drawing.Size(235, 17);
            this.lblCompleated.TabIndex = 10;
            this.lblCompleated.Text = "Загрузка успешно завершена!";
            // 
            // IngridientsLoad
            // 
            this.IngridientsLoad.AllowBack = false;
            this.IngridientsLoad.Controls.Add(this.mainIngridientGridControl);
            this.IngridientsLoad.Controls.Add(this.pnlMainIngr);
            this.IngridientsLoad.Name = "IngridientsLoad";
            this.IngridientsLoad.Size = new System.Drawing.Size(742, 319);
            this.IngridientsLoad.Text = "Ингридиенты";
            // 
            // mainIngridientGridControl
            // 
            this.mainIngridientGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainIngridientGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainIngridientGridControl.MainView = this.mainIngridientGridView;
            this.mainIngridientGridControl.Name = "mainIngridientGridControl";
            this.mainIngridientGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.EditRowButton,
            this.EditFolderButton});
            this.mainIngridientGridControl.Size = new System.Drawing.Size(742, 257);
            this.mainIngridientGridControl.TabIndex = 10;
            this.mainIngridientGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainIngridientGridView});
            // 
            // mainIngridientGridView
            // 
            this.mainIngridientGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnIngridientId,
            this.columnIngridient,
            this.columnIngridientExternal,
            this.columnIngridientIdExternal,
            this.columnCategory,
            this.columnProductCategory});
            this.mainIngridientGridView.GridControl = this.mainIngridientGridControl;
            this.mainIngridientGridView.Name = "mainIngridientGridView";
            // 
            // columnIngridientId
            // 
            this.columnIngridientId.Caption = "ИД";
            this.columnIngridientId.FieldName = "Product.ID";
            this.columnIngridientId.Name = "columnIngridientId";
            this.columnIngridientId.OptionsColumn.AllowEdit = false;
            this.columnIngridientId.OptionsColumn.AllowFocus = false;
            this.columnIngridientId.OptionsColumn.ReadOnly = true;
            this.columnIngridientId.Visible = true;
            this.columnIngridientId.VisibleIndex = 0;
            this.columnIngridientId.Width = 80;
            // 
            // columnIngridient
            // 
            this.columnIngridient.Caption = "Ингридиент";
            this.columnIngridient.ColumnEdit = this.EditRowButton;
            this.columnIngridient.FieldName = "Product.Name";
            this.columnIngridient.Name = "columnIngridient";
            this.columnIngridient.Visible = true;
            this.columnIngridient.VisibleIndex = 1;
            this.columnIngridient.Width = 214;
            // 
            // EditRowButton
            // 
            this.EditRowButton.AutoHeight = false;
            this.EditRowButton.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EditRowButton.Name = "EditRowButton";
            this.EditRowButton.Click += new System.EventHandler(this.EditRowButton_Click);
            // 
            // columnIngridientExternal
            // 
            this.columnIngridientExternal.Caption = "Ингридиент(Шеф Эксперт)";
            this.columnIngridientExternal.FieldName = "ChefProduct.Name";
            this.columnIngridientExternal.Name = "columnIngridientExternal";
            this.columnIngridientExternal.OptionsColumn.AllowEdit = false;
            this.columnIngridientExternal.OptionsColumn.AllowFocus = false;
            this.columnIngridientExternal.OptionsColumn.ReadOnly = true;
            this.columnIngridientExternal.Visible = true;
            this.columnIngridientExternal.VisibleIndex = 4;
            this.columnIngridientExternal.Width = 336;
            // 
            // columnIngridientIdExternal
            // 
            this.columnIngridientIdExternal.Caption = "ИД (Шеф Эксперт)";
            this.columnIngridientIdExternal.FieldName = "ChefProduct.ID";
            this.columnIngridientIdExternal.Name = "columnIngridientIdExternal";
            this.columnIngridientIdExternal.OptionsColumn.AllowEdit = false;
            this.columnIngridientIdExternal.OptionsColumn.AllowFocus = false;
            this.columnIngridientIdExternal.OptionsColumn.ReadOnly = true;
            this.columnIngridientIdExternal.Visible = true;
            this.columnIngridientIdExternal.VisibleIndex = 3;
            this.columnIngridientIdExternal.Width = 94;
            // 
            // columnCategory
            // 
            this.columnCategory.Caption = "Категория";
            this.columnCategory.ColumnEdit = this.EditFolderButton;
            this.columnCategory.FieldName = "ChefProduct.ProductCategory.Name";
            this.columnCategory.Name = "columnCategory";
            this.columnCategory.Visible = true;
            this.columnCategory.VisibleIndex = 5;
            // 
            // EditFolderButton
            // 
            this.EditFolderButton.AutoHeight = false;
            this.EditFolderButton.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EditFolderButton.Name = "EditFolderButton";
            this.EditFolderButton.Click += new System.EventHandler(this.EditFolderButton_Click);
            // 
            // columnProductCategory
            // 
            this.columnProductCategory.Caption = "Категория";
            this.columnProductCategory.FieldName = "Product.ProductCategory.Name";
            this.columnProductCategory.Name = "columnProductCategory";
            this.columnProductCategory.OptionsColumn.AllowEdit = false;
            this.columnProductCategory.OptionsColumn.AllowFocus = false;
            this.columnProductCategory.OptionsColumn.ReadOnly = true;
            this.columnProductCategory.Visible = true;
            this.columnProductCategory.VisibleIndex = 2;
            // 
            // pnlMainIngr
            // 
            this.pnlMainIngr.Controls.Add(this.cmbCatalogFromIngr);
            this.pnlMainIngr.Controls.Add(this.btnSelectFolder);
            this.pnlMainIngr.Controls.Add(this.UseConstFolder);
            this.pnlMainIngr.Controls.Add(this.lblIfNotComparer);
            this.pnlMainIngr.Controls.Add(this.cmbAction);
            this.pnlMainIngr.Controls.Add(this.lblNewIngrCatalog);
            this.pnlMainIngr.Controls.Add(this.cmbFolderForNew);
            this.pnlMainIngr.Controls.Add(this.lblIngridientsCountValue);
            this.pnlMainIngr.Controls.Add(this.lblIngridientsCount);
            this.pnlMainIngr.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMainIngr.Location = new System.Drawing.Point(0, 257);
            this.pnlMainIngr.Name = "pnlMainIngr";
            this.pnlMainIngr.Size = new System.Drawing.Size(742, 62);
            this.pnlMainIngr.TabIndex = 11;
            // 
            // cmbCatalogFromIngr
            // 
            this.cmbCatalogFromIngr.AutoSize = true;
            this.cmbCatalogFromIngr.Location = new System.Drawing.Point(399, 35);
            this.cmbCatalogFromIngr.Name = "cmbCatalogFromIngr";
            this.cmbCatalogFromIngr.Size = new System.Drawing.Size(248, 17);
            this.cmbCatalogFromIngr.TabIndex = 9;
            this.cmbCatalogFromIngr.Text = "Каталог из соответствующего ингридиента";
            this.cmbCatalogFromIngr.UseVisualStyleBackColor = true;
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Enabled = false;
            this.btnSelectFolder.Location = new System.Drawing.Point(369, 6);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(24, 20);
            this.btnSelectFolder.TabIndex = 8;
            this.btnSelectFolder.Text = "...";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.btnSelectFolder_Click);
            // 
            // UseConstFolder
            // 
            this.UseConstFolder.AutoSize = true;
            this.UseConstFolder.Location = new System.Drawing.Point(399, 8);
            this.UseConstFolder.Name = "UseConstFolder";
            this.UseConstFolder.Size = new System.Drawing.Size(169, 17);
            this.UseConstFolder.TabIndex = 7;
            this.UseConstFolder.Text = "Предопределенный каталог";
            this.UseConstFolder.UseVisualStyleBackColor = true;
            this.UseConstFolder.CheckedChanged += new System.EventHandler(this.UseConstFolder_CheckedChanged);
            // 
            // lblIfNotComparer
            // 
            this.lblIfNotComparer.AutoSize = true;
            this.lblIfNotComparer.Location = new System.Drawing.Point(34, 36);
            this.lblIfNotComparer.Name = "lblIfNotComparer";
            this.lblIfNotComparer.Size = new System.Drawing.Size(127, 13);
            this.lblIfNotComparer.TabIndex = 6;
            this.lblIfNotComparer.Text = "Если нет соответствия:";
            // 
            // cmbAction
            // 
            this.cmbAction.FormattingEnabled = true;
            this.cmbAction.Location = new System.Drawing.Point(167, 33);
            this.cmbAction.Name = "cmbAction";
            this.cmbAction.Size = new System.Drawing.Size(226, 21);
            this.cmbAction.TabIndex = 5;
            // 
            // lblNewIngrCatalog
            // 
            this.lblNewIngrCatalog.AutoSize = true;
            this.lblNewIngrCatalog.Location = new System.Drawing.Point(3, 9);
            this.lblNewIngrCatalog.Name = "lblNewIngrCatalog";
            this.lblNewIngrCatalog.Size = new System.Drawing.Size(158, 13);
            this.lblNewIngrCatalog.TabIndex = 4;
            this.lblNewIngrCatalog.Text = "Каталог новых ингридиентов:";
            // 
            // cmbFolderForNew
            // 
            this.cmbFolderForNew.Enabled = false;
            this.cmbFolderForNew.FormattingEnabled = true;
            this.cmbFolderForNew.Location = new System.Drawing.Point(167, 6);
            this.cmbFolderForNew.Name = "cmbFolderForNew";
            this.cmbFolderForNew.Size = new System.Drawing.Size(196, 21);
            this.cmbFolderForNew.TabIndex = 3;
            // 
            // lblIngridientsCountValue
            // 
            this.lblIngridientsCountValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIngridientsCountValue.AutoSize = true;
            this.lblIngridientsCountValue.Location = new System.Drawing.Point(692, 9);
            this.lblIngridientsCountValue.Name = "lblIngridientsCountValue";
            this.lblIngridientsCountValue.Size = new System.Drawing.Size(13, 13);
            this.lblIngridientsCountValue.TabIndex = 1;
            this.lblIngridientsCountValue.Text = "0";
            // 
            // lblIngridientsCount
            // 
            this.lblIngridientsCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIngridientsCount.AutoSize = true;
            this.lblIngridientsCount.Location = new System.Drawing.Point(604, 9);
            this.lblIngridientsCount.Name = "lblIngridientsCount";
            this.lblIngridientsCount.Size = new System.Drawing.Size(82, 13);
            this.lblIngridientsCount.TabIndex = 0;
            this.lblIngridientsCount.Text = "Ингридиентов:";
            // 
            // FirstLoadPage
            // 
            this.FirstLoadPage.AllowBack = false;
            this.FirstLoadPage.AllowCancel = false;
            this.FirstLoadPage.AllowNext = false;
            this.FirstLoadPage.Controls.Add(this.progressLoadFromFile);
            this.FirstLoadPage.Name = "FirstLoadPage";
            this.FirstLoadPage.Size = new System.Drawing.Size(742, 319);
            this.FirstLoadPage.Text = "Загрузка...";
            // 
            // progressLoadFromFile
            // 
            this.progressLoadFromFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressLoadFromFile.Appearance.Options.UseBackColor = true;
            this.progressLoadFromFile.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.progressLoadFromFile.AppearanceCaption.Options.UseFont = true;
            this.progressLoadFromFile.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.progressLoadFromFile.AppearanceDescription.Options.UseFont = true;
            this.progressLoadFromFile.AutoHeight = true;
            this.progressLoadFromFile.Caption = "Пожалуйста подождите";
            this.progressLoadFromFile.Description = "Идет загрузка ...";
            this.progressLoadFromFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressLoadFromFile.ImageHorzOffset = 271;
            this.progressLoadFromFile.Location = new System.Drawing.Point(0, 0);
            this.progressLoadFromFile.Name = "progressLoadFromFile";
            this.progressLoadFromFile.Size = new System.Drawing.Size(742, 319);
            this.progressLoadFromFile.TabIndex = 1;
            // 
            // SaveIngridients
            // 
            this.SaveIngridients.AllowBack = false;
            this.SaveIngridients.AllowCancel = false;
            this.SaveIngridients.AllowNext = false;
            this.SaveIngridients.Controls.Add(this.progressSaveIngridients);
            this.SaveIngridients.Name = "SaveIngridients";
            this.SaveIngridients.Size = new System.Drawing.Size(742, 319);
            this.SaveIngridients.Text = "Сохранение ингридиентов...";
            // 
            // progressSaveIngridients
            // 
            this.progressSaveIngridients.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressSaveIngridients.Appearance.Options.UseBackColor = true;
            this.progressSaveIngridients.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.progressSaveIngridients.AppearanceCaption.Options.UseFont = true;
            this.progressSaveIngridients.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.progressSaveIngridients.AppearanceDescription.Options.UseFont = true;
            this.progressSaveIngridients.AutoHeight = true;
            this.progressSaveIngridients.Caption = "Пожалуйста подождите";
            this.progressSaveIngridients.Description = "Идет загрузка ...";
            this.progressSaveIngridients.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressSaveIngridients.ImageHorzOffset = 271;
            this.progressSaveIngridients.Location = new System.Drawing.Point(0, 0);
            this.progressSaveIngridients.Name = "progressSaveIngridients";
            this.progressSaveIngridients.Size = new System.Drawing.Size(742, 319);
            this.progressSaveIngridients.TabIndex = 2;
            // 
            // ProductLoad
            // 
            this.ProductLoad.AllowBack = false;
            this.ProductLoad.Controls.Add(this.mainDishGridControl);
            this.ProductLoad.Controls.Add(this.pnlMainDish);
            this.ProductLoad.Name = "ProductLoad";
            this.ProductLoad.Size = new System.Drawing.Size(742, 319);
            this.ProductLoad.Text = "Блюда";
            // 
            // mainDishGridControl
            // 
            this.mainDishGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDishGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainDishGridControl.MainView = this.mainDishGridView;
            this.mainDishGridControl.Name = "mainDishGridControl";
            this.mainDishGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.DishEdit,
            this.DishCategoryEdit});
            this.mainDishGridControl.Size = new System.Drawing.Size(742, 257);
            this.mainDishGridControl.TabIndex = 13;
            this.mainDishGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainDishGridView});
            // 
            // mainDishGridView
            // 
            this.mainDishGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnDishId,
            this.columnDishName,
            this.columnDishChefName,
            this.columnDishChefId,
            this.columnDishChefCategory,
            this.gridColumn6});
            this.mainDishGridView.GridControl = this.mainDishGridControl;
            this.mainDishGridView.Name = "mainDishGridView";
            // 
            // columnDishId
            // 
            this.columnDishId.Caption = "ИД";
            this.columnDishId.FieldName = "Dish.ID";
            this.columnDishId.Name = "columnDishId";
            this.columnDishId.OptionsColumn.AllowEdit = false;
            this.columnDishId.OptionsColumn.AllowFocus = false;
            this.columnDishId.OptionsColumn.ReadOnly = true;
            this.columnDishId.Visible = true;
            this.columnDishId.VisibleIndex = 0;
            this.columnDishId.Width = 80;
            // 
            // columnDishName
            // 
            this.columnDishName.Caption = "Блюдо";
            this.columnDishName.ColumnEdit = this.DishEdit;
            this.columnDishName.FieldName = "Dish.Name";
            this.columnDishName.Name = "columnDishName";
            this.columnDishName.Visible = true;
            this.columnDishName.VisibleIndex = 1;
            this.columnDishName.Width = 214;
            // 
            // DishEdit
            // 
            this.DishEdit.AutoHeight = false;
            this.DishEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DishEdit.Name = "DishEdit";
            this.DishEdit.Click += new System.EventHandler(this.DishEdit_Click);
            // 
            // columnDishChefName
            // 
            this.columnDishChefName.Caption = "Блюдо (Шеф Эксперт)";
            this.columnDishChefName.FieldName = "DishChef.Name";
            this.columnDishChefName.Name = "columnDishChefName";
            this.columnDishChefName.OptionsColumn.AllowEdit = false;
            this.columnDishChefName.OptionsColumn.AllowFocus = false;
            this.columnDishChefName.OptionsColumn.ReadOnly = true;
            this.columnDishChefName.Visible = true;
            this.columnDishChefName.VisibleIndex = 4;
            this.columnDishChefName.Width = 336;
            // 
            // columnDishChefId
            // 
            this.columnDishChefId.Caption = "ИД (Шеф Эксперт)";
            this.columnDishChefId.FieldName = "DishChef.ID";
            this.columnDishChefId.Name = "columnDishChefId";
            this.columnDishChefId.OptionsColumn.AllowEdit = false;
            this.columnDishChefId.OptionsColumn.AllowFocus = false;
            this.columnDishChefId.OptionsColumn.ReadOnly = true;
            this.columnDishChefId.Visible = true;
            this.columnDishChefId.VisibleIndex = 3;
            this.columnDishChefId.Width = 94;
            // 
            // columnDishChefCategory
            // 
            this.columnDishChefCategory.Caption = "Категория (Шеф Эксперт)";
            this.columnDishChefCategory.ColumnEdit = this.DishCategoryEdit;
            this.columnDishChefCategory.FieldName = "DishChef.DishCategory.Name";
            this.columnDishChefCategory.Name = "columnDishChefCategory";
            this.columnDishChefCategory.OptionsColumn.AllowEdit = false;
            this.columnDishChefCategory.OptionsColumn.AllowFocus = false;
            this.columnDishChefCategory.OptionsColumn.ReadOnly = true;
            this.columnDishChefCategory.Visible = true;
            this.columnDishChefCategory.VisibleIndex = 5;
            // 
            // DishCategoryEdit
            // 
            this.DishCategoryEdit.AutoHeight = false;
            this.DishCategoryEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DishCategoryEdit.Name = "DishCategoryEdit";
            this.DishCategoryEdit.Click += new System.EventHandler(this.DishCategoryEdit_Click);
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Категория";
            this.gridColumn6.FieldName = "Dish.Category.Name";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            // 
            // pnlMainDish
            // 
            this.pnlMainDish.Controls.Add(this.chkDishFromComparer);
            this.pnlMainDish.Controls.Add(this.btnSelectDishCategory);
            this.pnlMainDish.Controls.Add(this.chkStaticDishCatalog);
            this.pnlMainDish.Controls.Add(this.lblNotDishConf);
            this.pnlMainDish.Controls.Add(this.cmbDishAction);
            this.pnlMainDish.Controls.Add(this.lblNewDishCatalog);
            this.pnlMainDish.Controls.Add(this.cmbNewDishCatalog);
            this.pnlMainDish.Controls.Add(this.lblDishCount);
            this.pnlMainDish.Controls.Add(this.lblDishes);
            this.pnlMainDish.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlMainDish.Location = new System.Drawing.Point(0, 257);
            this.pnlMainDish.Name = "pnlMainDish";
            this.pnlMainDish.Size = new System.Drawing.Size(742, 62);
            this.pnlMainDish.TabIndex = 12;
            // 
            // chkDishFromComparer
            // 
            this.chkDishFromComparer.AutoSize = true;
            this.chkDishFromComparer.Location = new System.Drawing.Point(399, 35);
            this.chkDishFromComparer.Name = "chkDishFromComparer";
            this.chkDishFromComparer.Size = new System.Drawing.Size(216, 17);
            this.chkDishFromComparer.TabIndex = 9;
            this.chkDishFromComparer.Text = "Каталог из соответствующего блюда";
            this.chkDishFromComparer.UseVisualStyleBackColor = true;
            // 
            // btnSelectDishCategory
            // 
            this.btnSelectDishCategory.Enabled = false;
            this.btnSelectDishCategory.Location = new System.Drawing.Point(369, 6);
            this.btnSelectDishCategory.Name = "btnSelectDishCategory";
            this.btnSelectDishCategory.Size = new System.Drawing.Size(24, 20);
            this.btnSelectDishCategory.TabIndex = 8;
            this.btnSelectDishCategory.Text = "...";
            this.btnSelectDishCategory.UseVisualStyleBackColor = true;
            this.btnSelectDishCategory.Visible = false;
            this.btnSelectDishCategory.Click += new System.EventHandler(this.btnSelectDishCategory_Click);
            // 
            // chkStaticDishCatalog
            // 
            this.chkStaticDishCatalog.AutoSize = true;
            this.chkStaticDishCatalog.Location = new System.Drawing.Point(399, 8);
            this.chkStaticDishCatalog.Name = "chkStaticDishCatalog";
            this.chkStaticDishCatalog.Size = new System.Drawing.Size(169, 17);
            this.chkStaticDishCatalog.TabIndex = 7;
            this.chkStaticDishCatalog.Text = "Предопределенный каталог";
            this.chkStaticDishCatalog.UseVisualStyleBackColor = true;
            this.chkStaticDishCatalog.Visible = false;
            // 
            // lblNotDishConf
            // 
            this.lblNotDishConf.AutoSize = true;
            this.lblNotDishConf.Location = new System.Drawing.Point(34, 36);
            this.lblNotDishConf.Name = "lblNotDishConf";
            this.lblNotDishConf.Size = new System.Drawing.Size(127, 13);
            this.lblNotDishConf.TabIndex = 6;
            this.lblNotDishConf.Text = "Если нет соответствия:";
            // 
            // cmbDishAction
            // 
            this.cmbDishAction.FormattingEnabled = true;
            this.cmbDishAction.Location = new System.Drawing.Point(167, 33);
            this.cmbDishAction.Name = "cmbDishAction";
            this.cmbDishAction.Size = new System.Drawing.Size(226, 21);
            this.cmbDishAction.TabIndex = 5;
            // 
            // lblNewDishCatalog
            // 
            this.lblNewDishCatalog.AutoSize = true;
            this.lblNewDishCatalog.Location = new System.Drawing.Point(47, 10);
            this.lblNewDishCatalog.Name = "lblNewDishCatalog";
            this.lblNewDishCatalog.Size = new System.Drawing.Size(114, 13);
            this.lblNewDishCatalog.TabIndex = 4;
            this.lblNewDishCatalog.Text = "Каталог новых блюд:";
            this.lblNewDishCatalog.Visible = false;
            // 
            // cmbNewDishCatalog
            // 
            this.cmbNewDishCatalog.Enabled = false;
            this.cmbNewDishCatalog.FormattingEnabled = true;
            this.cmbNewDishCatalog.Location = new System.Drawing.Point(167, 6);
            this.cmbNewDishCatalog.Name = "cmbNewDishCatalog";
            this.cmbNewDishCatalog.Size = new System.Drawing.Size(196, 21);
            this.cmbNewDishCatalog.TabIndex = 3;
            this.cmbNewDishCatalog.Visible = false;
            // 
            // lblDishCount
            // 
            this.lblDishCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDishCount.AutoSize = true;
            this.lblDishCount.Location = new System.Drawing.Point(692, 9);
            this.lblDishCount.Name = "lblDishCount";
            this.lblDishCount.Size = new System.Drawing.Size(13, 13);
            this.lblDishCount.TabIndex = 1;
            this.lblDishCount.Text = "0";
            // 
            // lblDishes
            // 
            this.lblDishes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDishes.AutoSize = true;
            this.lblDishes.Location = new System.Drawing.Point(604, 9);
            this.lblDishes.Name = "lblDishes";
            this.lblDishes.Size = new System.Drawing.Size(37, 13);
            this.lblDishes.TabIndex = 0;
            this.lblDishes.Text = "Блюд:";
            // 
            // DishLoading
            // 
            this.DishLoading.AllowBack = false;
            this.DishLoading.AllowCancel = false;
            this.DishLoading.AllowNext = false;
            this.DishLoading.Controls.Add(this.progressDishLoading);
            this.DishLoading.Name = "DishLoading";
            this.DishLoading.Size = new System.Drawing.Size(742, 319);
            this.DishLoading.Text = "Сохранение блюд...";
            // 
            // progressDishLoading
            // 
            this.progressDishLoading.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressDishLoading.Appearance.Options.UseBackColor = true;
            this.progressDishLoading.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.progressDishLoading.AppearanceCaption.Options.UseFont = true;
            this.progressDishLoading.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.progressDishLoading.AppearanceDescription.Options.UseFont = true;
            this.progressDishLoading.AutoHeight = true;
            this.progressDishLoading.Caption = "Пожалуйста подождите";
            this.progressDishLoading.Description = "Идет загрузка ...";
            this.progressDishLoading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressDishLoading.ImageHorzOffset = 271;
            this.progressDishLoading.Location = new System.Drawing.Point(0, 0);
            this.progressDishLoading.Name = "progressDishLoading";
            this.progressDishLoading.Size = new System.Drawing.Size(742, 319);
            this.progressDishLoading.TabIndex = 3;
            // 
            // IngridientsLoaded
            // 
            this.IngridientsLoaded.AllowBack = false;
            this.IngridientsLoaded.Controls.Add(this.label1);
            this.IngridientsLoaded.Name = "IngridientsLoaded";
            this.IngridientsLoaded.Size = new System.Drawing.Size(742, 319);
            this.IngridientsLoaded.Text = "Ингридиенты загружены";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(244, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(274, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Загрузка ингридиентов завершена!";
            // 
            // DishLoading1
            // 
            this.DishLoading1.AllowBack = false;
            this.DishLoading1.AllowCancel = false;
            this.DishLoading1.AllowNext = false;
            this.DishLoading1.Controls.Add(this.progressDishLoad);
            this.DishLoading1.Name = "DishLoading1";
            this.DishLoading1.Size = new System.Drawing.Size(742, 319);
            this.DishLoading1.Text = "Загрузка...";
            // 
            // progressDishLoad
            // 
            this.progressDishLoad.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressDishLoad.Appearance.Options.UseBackColor = true;
            this.progressDishLoad.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.progressDishLoad.AppearanceCaption.Options.UseFont = true;
            this.progressDishLoad.AppearanceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.progressDishLoad.AppearanceDescription.Options.UseFont = true;
            this.progressDishLoad.AutoHeight = true;
            this.progressDishLoad.Caption = "Пожалуйста подождите";
            this.progressDishLoad.Description = "Идет загрузка ...";
            this.progressDishLoad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressDishLoad.ImageHorzOffset = 271;
            this.progressDishLoad.Location = new System.Drawing.Point(0, 0);
            this.progressDishLoad.Name = "progressDishLoad";
            this.progressDishLoad.Size = new System.Drawing.Size(742, 319);
            this.progressDishLoad.TabIndex = 3;
            // 
            // AddressDlg
            // 
            this.AddressDlg.Filter = "*.xml|*.xml";
            // 
            // TransferChiefExpert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 481);
            this.Controls.Add(this.TransferWizard);
            this.Name = "TransferChiefExpert";
            this.Text = "Загрузка данных из программы Шеф Эксперт";
            this.Resize += new System.EventHandler(this.TransferChiefExpert_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.TransferWizard)).EndInit();
            this.TransferWizard.ResumeLayout(false);
            this.StartLoad.ResumeLayout(false);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.LoadCompleated.ResumeLayout(false);
            this.LoadCompleated.PerformLayout();
            this.IngridientsLoad.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainIngridientGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainIngridientGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditRowButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditFolderButton)).EndInit();
            this.pnlMainIngr.ResumeLayout(false);
            this.pnlMainIngr.PerformLayout();
            this.FirstLoadPage.ResumeLayout(false);
            this.FirstLoadPage.PerformLayout();
            this.SaveIngridients.ResumeLayout(false);
            this.SaveIngridients.PerformLayout();
            this.ProductLoad.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainDishGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDishGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DishEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DishCategoryEdit)).EndInit();
            this.pnlMainDish.ResumeLayout(false);
            this.pnlMainDish.PerformLayout();
            this.DishLoading.ResumeLayout(false);
            this.DishLoading.PerformLayout();
            this.IngridientsLoaded.ResumeLayout(false);
            this.IngridientsLoaded.PerformLayout();
            this.DishLoading1.ResumeLayout(false);
            this.DishLoading1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWizard.WizardControl TransferWizard;
        private DevExpress.XtraWizard.WelcomeWizardPage StartLoad;
        private DevExpress.XtraWizard.CompletionWizardPage LoadCompleated;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Label lblFileTransfer;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Label lblTransfer;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblThird;
        private System.Windows.Forms.Label lblSecond;
        private System.Windows.Forms.Label lblFirst;
        private DevExpress.XtraWizard.WizardPage IngridientsLoad;
        private DevExpress.XtraWizard.WizardPage FirstLoadPage;
        public DevExpress.XtraWaitForm.ProgressPanel progressLoadFromFile;
        private DevExpress.XtraGrid.GridControl mainIngridientGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainIngridientGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnIngridientId;
        private DevExpress.XtraGrid.Columns.GridColumn columnIngridient;
        private DevExpress.XtraGrid.Columns.GridColumn columnIngridientExternal;
        private DevExpress.XtraGrid.Columns.GridColumn columnIngridientIdExternal;
        private System.Windows.Forms.Panel pnlMainIngr;
        private System.Windows.Forms.Label lblNewIngrCatalog;
        private System.Windows.Forms.ComboBox cmbFolderForNew;
        private System.Windows.Forms.Label lblIngridientsCountValue;
        private System.Windows.Forms.Label lblIngridientsCount;
        private System.Windows.Forms.OpenFileDialog AddressDlg;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblIfNotComparer;
        private System.Windows.Forms.ComboBox cmbAction;
        private System.Windows.Forms.CheckBox UseConstFolder;
        private System.Windows.Forms.Button btnSelectFolder;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit EditRowButton;
        private DevExpress.XtraWizard.WizardPage SaveIngridients;
        public DevExpress.XtraWaitForm.ProgressPanel progressSaveIngridients;
        private DevExpress.XtraGrid.Columns.GridColumn columnCategory;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit EditFolderButton;
        private DevExpress.XtraGrid.Columns.GridColumn columnProductCategory;
        private System.Windows.Forms.CheckBox cmbCatalogFromIngr;
        private DevExpress.XtraWizard.WizardPage ProductLoad;
        private DevExpress.XtraGrid.GridControl mainDishGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainDishGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnDishId;
        private DevExpress.XtraGrid.Columns.GridColumn columnDishName;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit DishEdit;
        private DevExpress.XtraGrid.Columns.GridColumn columnDishChefName;
        private DevExpress.XtraGrid.Columns.GridColumn columnDishChefId;
        private DevExpress.XtraGrid.Columns.GridColumn columnDishChefCategory;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit DishCategoryEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.Panel pnlMainDish;
        private System.Windows.Forms.CheckBox chkDishFromComparer;
        private System.Windows.Forms.Button btnSelectDishCategory;
        private System.Windows.Forms.CheckBox chkStaticDishCatalog;
        private System.Windows.Forms.Label lblNotDishConf;
        private System.Windows.Forms.ComboBox cmbDishAction;
        private System.Windows.Forms.Label lblNewDishCatalog;
        private System.Windows.Forms.ComboBox cmbNewDishCatalog;
        private System.Windows.Forms.Label lblDishCount;
        private System.Windows.Forms.Label lblDishes;
        private DevExpress.XtraWizard.WizardPage DishLoading;
        public DevExpress.XtraWaitForm.ProgressPanel progressDishLoading;
        private System.Windows.Forms.Label lblCompleated;
        private DevExpress.XtraWizard.WizardPage IngridientsLoaded;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraWizard.WizardPage DishLoading1;
        public DevExpress.XtraWaitForm.ProgressPanel progressDishLoad;
    }
}