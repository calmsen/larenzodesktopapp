﻿namespace LaRenzo.Forms.Parameters.OptionForms
{
	partial class DevEmailSettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.appDevTextBox = new System.Windows.Forms.TextBox();
			this.siteDevTextBox = new System.Windows.Forms.TextBox();
			this.mailAddressTextBox = new System.Windows.Forms.TextBox();
			this.mailUserTextBox = new System.Windows.Forms.TextBox();
			this.mailPassTextBox = new System.Windows.Forms.TextBox();
			this.mailSmtpSeverTextBox = new System.Windows.Forms.TextBox();
			this.mailSmtpPortTextBox = new System.Windows.Forms.TextBox();
			this.isUseSSLCheckBox = new System.Windows.Forms.CheckBox();
			this.saveButton = new System.Windows.Forms.Button();
			this.testMailButton = new System.Windows.Forms.Button();
			this.sendMailBackgroundWorker = new System.ComponentModel.BackgroundWorker();
			this.sendMailProgressBar = new System.Windows.Forms.ProgressBar();
			this.showPassButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(223, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Email разработчиков программы";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 37);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(188, 17);
			this.label2.TabIndex = 1;
			this.label2.Text = "Email разработчиков сайта";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(30, 102);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 17);
			this.label3.TabIndex = 2;
			this.label3.Text = "Адрес ";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(30, 130);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(101, 17);
			this.label4.TabIndex = 3;
			this.label4.Text = "Пользователь";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 73);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(130, 17);
			this.label5.TabIndex = 4;
			this.label5.Text = "Аккаунт отправки:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(30, 158);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(57, 17);
			this.label6.TabIndex = 5;
			this.label6.Text = "Пароль";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(30, 186);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(96, 17);
			this.label7.TabIndex = 6;
			this.label7.Text = "SMTP сервер";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(30, 214);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(81, 17);
			this.label8.TabIndex = 7;
			this.label8.Text = "SMTP порт";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(30, 242);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(130, 17);
			this.label9.TabIndex = 8;
			this.label9.Text = "Использовать SSL";
			// 
			// appDevTextBox
			// 
			this.appDevTextBox.Location = new System.Drawing.Point(241, 6);
			this.appDevTextBox.Name = "appDevTextBox";
			this.appDevTextBox.Size = new System.Drawing.Size(267, 22);
			this.appDevTextBox.TabIndex = 9;
			// 
			// siteDevTextBox
			// 
			this.siteDevTextBox.Location = new System.Drawing.Point(241, 34);
			this.siteDevTextBox.Name = "siteDevTextBox";
			this.siteDevTextBox.Size = new System.Drawing.Size(267, 22);
			this.siteDevTextBox.TabIndex = 10;
			// 
			// mailAddressTextBox
			// 
			this.mailAddressTextBox.Location = new System.Drawing.Point(170, 99);
			this.mailAddressTextBox.Name = "mailAddressTextBox";
			this.mailAddressTextBox.Size = new System.Drawing.Size(254, 22);
			this.mailAddressTextBox.TabIndex = 11;
			// 
			// mailUserTextBox
			// 
			this.mailUserTextBox.Location = new System.Drawing.Point(170, 127);
			this.mailUserTextBox.Name = "mailUserTextBox";
			this.mailUserTextBox.Size = new System.Drawing.Size(254, 22);
			this.mailUserTextBox.TabIndex = 12;
			// 
			// mailPassTextBox
			// 
			this.mailPassTextBox.Location = new System.Drawing.Point(170, 155);
			this.mailPassTextBox.Name = "mailPassTextBox";
			this.mailPassTextBox.PasswordChar = '*';
			this.mailPassTextBox.Size = new System.Drawing.Size(254, 22);
			this.mailPassTextBox.TabIndex = 13;
			// 
			// mailSmtpSeverTextBox
			// 
			this.mailSmtpSeverTextBox.Location = new System.Drawing.Point(170, 183);
			this.mailSmtpSeverTextBox.Name = "mailSmtpSeverTextBox";
			this.mailSmtpSeverTextBox.Size = new System.Drawing.Size(254, 22);
			this.mailSmtpSeverTextBox.TabIndex = 14;
			// 
			// mailSmtpPortTextBox
			// 
			this.mailSmtpPortTextBox.Location = new System.Drawing.Point(170, 211);
			this.mailSmtpPortTextBox.Name = "mailSmtpPortTextBox";
			this.mailSmtpPortTextBox.Size = new System.Drawing.Size(254, 22);
			this.mailSmtpPortTextBox.TabIndex = 15;
			// 
			// isUseSSLCheckBox
			// 
			this.isUseSSLCheckBox.AutoSize = true;
			this.isUseSSLCheckBox.Location = new System.Drawing.Point(170, 239);
			this.isUseSSLCheckBox.Name = "isUseSSLCheckBox";
			this.isUseSSLCheckBox.Size = new System.Drawing.Size(18, 17);
			this.isUseSSLCheckBox.TabIndex = 16;
			this.isUseSSLCheckBox.UseVisualStyleBackColor = true;
			// 
			// saveButton
			// 
			this.saveButton.Location = new System.Drawing.Point(407, 265);
			this.saveButton.Name = "saveButton";
			this.saveButton.Size = new System.Drawing.Size(101, 31);
			this.saveButton.TabIndex = 17;
			this.saveButton.Text = "Сохранить";
			this.saveButton.UseVisualStyleBackColor = true;
			this.saveButton.Click += new System.EventHandler(this.SaveButtonClick);
			// 
			// testMailButton
			// 
			this.testMailButton.Location = new System.Drawing.Point(170, 265);
			this.testMailButton.Name = "testMailButton";
			this.testMailButton.Size = new System.Drawing.Size(101, 31);
			this.testMailButton.TabIndex = 18;
			this.testMailButton.Text = "Проверить";
			this.testMailButton.UseVisualStyleBackColor = true;
			this.testMailButton.Click += new System.EventHandler(this.TestMailButtonClick);
			// 
			// sendMailBackgroundWorker
			// 
			this.sendMailBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.SendMailBackgroundWorkerDoWork);
			// 
			// sendMailProgressBar
			// 
			this.sendMailProgressBar.Location = new System.Drawing.Point(15, 102);
			this.sendMailProgressBar.Name = "sendMailProgressBar";
			this.sendMailProgressBar.Size = new System.Drawing.Size(482, 45);
			this.sendMailProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.sendMailProgressBar.TabIndex = 19;
			this.sendMailProgressBar.Visible = false;
			// 
			// showPassButton
			// 
			this.showPassButton.Location = new System.Drawing.Point(430, 155);
			this.showPassButton.Name = "showPassButton";
			this.showPassButton.Size = new System.Drawing.Size(78, 22);
			this.showPassButton.TabIndex = 20;
			this.showPassButton.Text = "Показать";
			this.showPassButton.UseVisualStyleBackColor = true;
			this.showPassButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ShowPassButtonMouseDown);
			this.showPassButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ShowPassButtonMouseUp);
			// 
			// DevEmailSettingsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(516, 302);
			this.Controls.Add(this.showPassButton);
			this.Controls.Add(this.sendMailProgressBar);
			this.Controls.Add(this.testMailButton);
			this.Controls.Add(this.saveButton);
			this.Controls.Add(this.isUseSSLCheckBox);
			this.Controls.Add(this.mailSmtpPortTextBox);
			this.Controls.Add(this.mailSmtpSeverTextBox);
			this.Controls.Add(this.mailPassTextBox);
			this.Controls.Add(this.mailUserTextBox);
			this.Controls.Add(this.mailAddressTextBox);
			this.Controls.Add(this.siteDevTextBox);
			this.Controls.Add(this.appDevTextBox);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DevEmailSettingsForm";
			this.Text = "Настройки почты разработчиков";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox appDevTextBox;
		private System.Windows.Forms.TextBox siteDevTextBox;
		private System.Windows.Forms.TextBox mailAddressTextBox;
		private System.Windows.Forms.TextBox mailUserTextBox;
		private System.Windows.Forms.TextBox mailPassTextBox;
		private System.Windows.Forms.TextBox mailSmtpSeverTextBox;
		private System.Windows.Forms.TextBox mailSmtpPortTextBox;
		private System.Windows.Forms.CheckBox isUseSSLCheckBox;
		private System.Windows.Forms.Button saveButton;
		private System.Windows.Forms.Button testMailButton;
		private System.ComponentModel.BackgroundWorker sendMailBackgroundWorker;
		private System.Windows.Forms.ProgressBar sendMailProgressBar;
		private System.Windows.Forms.Button showPassButton;
	}
}