﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Content = LaRenzo.DataRepository.Repositories.Content;

namespace LaRenzo.Forms.Parameters.OptionForms
{
    public partial class OptionsForm : Form
    {

		//_____________________________________________________________________________________________________________________________________________________________________________________
		public OptionsForm()
        {
            InitializeComponent();
        }
		

		//_____________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> "Кнопка" сохранить  </summary>
		////========================================
		private void BtnSaveOptionsClick(object sender, EventArgs e)
	    {
			// Сохранить текущие настройки с контролов формы
			SaveSettings();

			// Закрыть форму
            Close();
        }

		

	    private void OptionsForm_Load(object sender, EventArgs e)
	    {
		    tabControl.TabPages.Clear();
			
			List<DataRepository.Entities.Settings.Brand> brands = Content.BrandManager.GetList();

		    foreach (DataRepository.Entities.Settings.Brand brand in brands)
		    {
			    if (!string.IsNullOrEmpty(brand.Name))
			    {
				    TabPage tp = new TabPage
				    {
					    Text = brand.Name,
						Visible = true
				    };

				    SettingsControl sc = new SettingsControl();
					sc.SetCurrentSettingToFormControls(brand.ID);
					sc.Dock = DockStyle.Fill;

					tp.Controls.Add(sc);

					tabControl.TabPages.Add(tp);
			    }
		    }
	    }


	    private void SaveSettings()
	    {
			List<DataRepository.Entities.Settings.Brand> brands = Content.BrandManager.GetList();

			foreach (TabPage tab in tabControl.TabPages)
			{
				foreach (Control control in tab.Controls)
				{
					var sc = control as SettingsControl;

					if (sc != null)
					{
						sc.SaveCurrentSettingFromFormControls();
					}
				}
			} 
	    }
    }
}
