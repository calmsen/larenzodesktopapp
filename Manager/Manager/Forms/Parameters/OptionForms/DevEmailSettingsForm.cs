﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.Tools;

namespace LaRenzo.Forms.Parameters.OptionForms
{
	public partial class DevEmailSettingsForm : Form
	{
		/// <summary> Ошибка отправки почты </summary>
		private string sendMailError = "";

		//___________________________________________________________________________________________________________________________________________________________________________________
		public DevEmailSettingsForm()
		{
			InitializeComponent();
			
			sendMailBackgroundWorker.RunWorkerCompleted += (SendMailBackgroundWorkerRunWorkerCompleted);

			LoadDataToForm();
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Сохранить" </summary>
		////=======================================
		private void LoadDataToForm()
		{
			appDevTextBox.Text = Content.GlobalOptionsManager.AppDevEmail;
			siteDevTextBox.Text = Content.GlobalOptionsManager.SiteDevEmail;

			mailAddressTextBox.Text = Content.GlobalOptionsManager.EmailAddress;
			mailUserTextBox.Text = Content.GlobalOptionsManager.EmailUser;
			mailPassTextBox.Text = Content.GlobalOptionsManager.EmailPass;
			mailSmtpSeverTextBox.Text = Content.GlobalOptionsManager.EmailSMTPServer;
			mailSmtpPortTextBox.Text = Content.GlobalOptionsManager.EmailSMTPPort.ToString();
			isUseSSLCheckBox.Checked = Content.GlobalOptionsManager.EmailUseSSL;
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Сохранить" </summary>
		////=======================================
		private void SaveDataFromForm()
		{
			Content.GlobalOptionsManager.AppDevEmail = appDevTextBox.Text;
			Content.GlobalOptionsManager.SiteDevEmail = siteDevTextBox.Text;
			
			Content.GlobalOptionsManager.EmailAddress = mailAddressTextBox.Text;
			Content.GlobalOptionsManager.EmailUser = mailUserTextBox.Text;
			Content.GlobalOptionsManager.EmailPass = mailPassTextBox.Text;
			Content.GlobalOptionsManager.EmailSMTPServer = mailSmtpSeverTextBox.Text;
			Content.GlobalOptionsManager.EmailSMTPPortStr = mailSmtpPortTextBox.Text;
			Content.GlobalOptionsManager.EmailUseSSL = isUseSSLCheckBox.Checked;
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Сохранить" </summary>
		////=======================================
		private void SaveButtonClick(object sender, EventArgs e)
		{
			SaveDataFromForm();
			Close();
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Проверить" отправку </summary>
		////================================================
		private void TestMailButtonClick(object sender, EventArgs e)
		{
			if (sendMailBackgroundWorker.IsBusy == false)
			{
				sendMailError = "";
				// Элементы формы не доступны
				FormElementsEnabled = false;
				sendMailBackgroundWorker.RunWorkerAsync();
			}
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить параметры почты </summary>
		////=============================================
		private MailSendParams GetMailSendParams(string toMailAddr)
		{
			int smtpPort;
			int.TryParse(mailSmtpPortTextBox.Text, out smtpPort);
			
			var mailParams = new MailSendParams
			{
				MailTitle = "Тестовое письмо",
				Message = "Тело тестового письма ",
				ToMailAddress = toMailAddr,
				FromParam = new MailSendParamsFrom
				{
					Address = mailAddressTextBox.Text,
					IsEnabledSSL = isUseSSLCheckBox.Checked,
					User = mailUserTextBox.Text,
					Pass = mailPassTextBox.Text,
					SmtpServer = mailSmtpSeverTextBox.Text,
					SmtpServerPort = smtpPort
				},
				AttachListFileNames = new List<string>(),
				AttachListFileBodys = new List<string>()
			};

			return mailParams;
		}
		

		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Выполнить в фоне отправку почты </summary>
		////====================================================
		private void SendMailBackgroundWorkerDoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
		{
			var mailManager = new SendMailManager();

			var mailParams = GetMailSendParams(Content.GlobalOptionsManager.AppDevEmail);

			try
			{
				mailManager.SendMail(mailParams);
			}
			catch (Exception ex)
			{
				sendMailError = ex.ToString();
			}
			
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Отправка почты окончена </summary>
		////============================================
		void SendMailBackgroundWorkerRunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
		{
			if (string.IsNullOrEmpty(sendMailError))
			{
				MessageBox.Show(@"Тестовое сообщение упешно отправлено");
			}
			else
			{
				MessageBox.Show(@"Произошла ошибка:\r\n" + sendMailError);
			}

			// Элементы формы доступны
			FormElementsEnabled = true;
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Доступность элементов формы </summary>
		private bool FormElementsEnabled
		{
			set
			{
				sendMailProgressBar.Visible = !value;

				testMailButton.Enabled = value;
				saveButton.Enabled = value;
				appDevTextBox.Enabled = value;
				siteDevTextBox.Enabled = value;
				mailAddressTextBox.Enabled = value;
				mailUserTextBox.Enabled = value;
				mailPassTextBox.Enabled = value;
				mailSmtpSeverTextBox.Enabled = value;
				mailSmtpPortTextBox.Enabled = value;
				isUseSSLCheckBox.Enabled = value;
				showPassButton.Enabled = value;
			}
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Показать" пароль нажата </summary>
		////====================================================
		private void ShowPassButtonMouseDown(object sender, MouseEventArgs e)
		{
			mailPassTextBox.PasswordChar = '\0';
		}


		//___________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Показать" пароль </summary>
		////=============================================
		private void ShowPassButtonMouseUp(object sender, MouseEventArgs e)
		{
			mailPassTextBox.PasswordChar = '*';
		}
	}
}
