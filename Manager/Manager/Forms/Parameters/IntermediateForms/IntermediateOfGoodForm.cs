﻿using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Wirings;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Wiringses.IntermadiatesGood;
using LaRenzo.Properties;

namespace LaRenzo.Forms.Parameters.IntermediateForms
{
    public partial class IntermediateOfGoodForm : Form
    {
        public IntermediateOfGoodForm()
        {
            InitializeComponent();
            LoadIntermediateOfGood(null, new FormClosedEventArgs(CloseReason.None));
        }

        private void btnRefresh_Click(object sender, System.EventArgs e)
        {
            LoadIntermediateOfGood(sender, new FormClosedEventArgs(CloseReason.None));
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            var movingForm = new IntermediateForm();
            movingForm.FormClosed += LoadIntermediateOfGood;
            movingForm.Show();
        }

        private void LoadIntermediateOfGood(object sender, FormClosedEventArgs e)
        {
            mainGridControl.DataSource = Content.ItermadiateGoodWirningManager.GetList();
        }

        private void btnDelete_Click(object sender, System.EventArgs e)
        {
            var selectedRow = (IntermediateGoodWirings) mainGridView.GetFocusedRow();

            if (selectedRow == null)
            {
                MessageBox.Show(Resources.IntermediateOfGood, Resources.Warning, MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return;
            }

            if (MessageBox.Show(Resources.IntermediateOfGoodDelete, Resources.WarningTitle, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Content.ItermadiateGoodWirningManager.DeleteWirning(selectedRow);
                LoadIntermediateOfGood(sender, new FormClosedEventArgs(CloseReason.None));
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;

            if (view == null || e.RowHandle < 0) return;

            var currentRow = (IntermediateGoodWirings)view.GetRow(e.RowHandle);

            if (!currentRow.IsActual)
                e.Appearance.BackColor = Color.DarkOrange;

            if (e.RowHandle == view.FocusedRowHandle)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }
    }
}
