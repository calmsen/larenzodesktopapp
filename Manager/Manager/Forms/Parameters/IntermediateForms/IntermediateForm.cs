﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Wiringses.IntermadiatesGood;

namespace LaRenzo.Forms.Parameters.IntermediateForms
{
    public partial class IntermediateForm : Form
    {
        public IntermediateForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Content.ItermadiateGoodWirningManager.AddWirning(dtIntermediateDate.Value);

            DialogResult = DialogResult.OK;

            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
