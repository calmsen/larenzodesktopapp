﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Forms.Controls;

namespace LaRenzo.Forms.Parameters.Brand
{
    public partial class BrandCombo : BaseCombo<DataRepository.Entities.Settings.Brand>
    {
        //____________________________________________________________________________________________________________________________________________________________________________________
        public BrandCombo()
        {
            InitializeComponent();
        }

        internal override string FildName()
        {
            return "Name";
        }

        internal override List<DataRepository.Entities.Settings.Brand> GetList(FilterModel filter = null)
        {
            return Content.BrandManager.GetList();
        }
    }
}
