﻿namespace LaRenzo.Forms
{
    partial class SessionOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tEditSession = new DevExpress.XtraEditors.TimeEdit();
            this.btnOpenSessoion = new System.Windows.Forms.Button();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPeopleWorks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnEquipmentWorks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblActivePointOfSales = new System.Windows.Forms.Label();
            this.chlbPointOfSales = new System.Windows.Forms.CheckedListBox();
            this.chkCanEditTime = new System.Windows.Forms.CheckBox();
            this.tEditDeltaWindow = new DevExpress.XtraEditors.TimeEdit();
            this.lblDeltaWindow = new System.Windows.Forms.Label();
            this.tEditDelta = new DevExpress.XtraEditors.TimeEdit();
            this.lblDelta = new System.Windows.Forms.Label();
            this.cmbIsVacationDelivery = new System.Windows.Forms.CheckBox();
            this.lblStartSession = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAccept = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tEditSession.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tEditDeltaWindow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tEditDelta.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tEditSession
            // 
            this.tEditSession.CausesValidation = false;
            this.tEditSession.EditValue = new System.DateTime(2012, 1, 16, 0, 0, 0, 0);
            this.tEditSession.Location = new System.Drawing.Point(223, 9);
            this.tEditSession.Margin = new System.Windows.Forms.Padding(4);
            this.tEditSession.Name = "tEditSession";
            // 
            // 
            // 
            this.tEditSession.Properties.AllowFocused = false;
            this.tEditSession.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tEditSession.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tEditSession.Properties.DisplayFormat.FormatString = "t";
            this.tEditSession.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditSession.Properties.EditFormat.FormatString = "t";
            this.tEditSession.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditSession.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.tEditSession.Properties.Mask.EditMask = "HH:mm";
            this.tEditSession.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.tEditSession.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.tEditSession.Properties.NullText = "Время кухни";
            this.tEditSession.Size = new System.Drawing.Size(101, 22);
            this.tEditSession.TabIndex = 11;
            // 
            // btnOpenSessoion
            // 
            this.btnOpenSessoion.Location = new System.Drawing.Point(491, 15);
            this.btnOpenSessoion.Margin = new System.Windows.Forms.Padding(4);
            this.btnOpenSessoion.Name = "btnOpenSessoion";
            this.btnOpenSessoion.Size = new System.Drawing.Size(289, 47);
            this.btnOpenSessoion.TabIndex = 12;
            this.btnOpenSessoion.Text = "Открыть сессию";
            this.btnOpenSessoion.UseVisualStyleBackColor = true;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // 
            // 
            this.mainGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Location = new System.Drawing.Point(0, 269);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(796, 199);
            this.mainGridControl.TabIndex = 13;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnName,
            this.columnPeopleWorks,
            this.columnEquipmentWorks});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsView.ShowGroupPanel = false;
            this.mainGridView.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            // 
            // columnName
            // 
            this.columnName.Caption = "Цех";
            this.columnName.FieldName = "Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 0;
            // 
            // columnPeopleWorks
            // 
            this.columnPeopleWorks.Caption = "Людей работает";
            this.columnPeopleWorks.FieldName = "PeopleWorks";
            this.columnPeopleWorks.Name = "columnPeopleWorks";
            this.columnPeopleWorks.Visible = true;
            this.columnPeopleWorks.VisibleIndex = 1;
            // 
            // columnEquipmentWorks
            // 
            this.columnEquipmentWorks.Caption = "Оборудования работает";
            this.columnEquipmentWorks.FieldName = "EquipmentWorks";
            this.columnEquipmentWorks.Name = "columnEquipmentWorks";
            this.columnEquipmentWorks.Visible = true;
            this.columnEquipmentWorks.VisibleIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblActivePointOfSales);
            this.panel1.Controls.Add(this.chlbPointOfSales);
            this.panel1.Controls.Add(this.chkCanEditTime);
            this.panel1.Controls.Add(this.tEditDeltaWindow);
            this.panel1.Controls.Add(this.lblDeltaWindow);
            this.panel1.Controls.Add(this.tEditDelta);
            this.panel1.Controls.Add(this.lblDelta);
            this.panel1.Controls.Add(this.cmbIsVacationDelivery);
            this.panel1.Controls.Add(this.lblStartSession);
            this.panel1.Controls.Add(this.tEditSession);
            this.panel1.Controls.Add(this.btnOpenSessoion);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(796, 269);
            this.panel1.TabIndex = 14;
            // 
            // lblActivePointOfSales
            // 
            this.lblActivePointOfSales.AutoSize = true;
            this.lblActivePointOfSales.Location = new System.Drawing.Point(16, 172);
            this.lblActivePointOfSales.Name = "lblActivePointOfSales";
            this.lblActivePointOfSales.Size = new System.Drawing.Size(114, 17);
            this.lblActivePointOfSales.TabIndex = 22;
            this.lblActivePointOfSales.Text = "Активные точки";
            // 
            // chlbPointOfSales
            // 
            this.chlbPointOfSales.FormattingEnabled = true;
            this.chlbPointOfSales.Location = new System.Drawing.Point(222, 172);
            this.chlbPointOfSales.Name = "chlbPointOfSales";
            this.chlbPointOfSales.Size = new System.Drawing.Size(243, 89);
            this.chlbPointOfSales.TabIndex = 21;
            // 
            // chkCanEditTime
            // 
            this.chkCanEditTime.AutoSize = true;
            this.chkCanEditTime.Location = new System.Drawing.Point(15, 69);
            this.chkCanEditTime.Margin = new System.Windows.Forms.Padding(4);
            this.chkCanEditTime.Name = "chkCanEditTime";
            this.chkCanEditTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkCanEditTime.Size = new System.Drawing.Size(262, 21);
            this.chkCanEditTime.TabIndex = 20;
            this.chkCanEditTime.Text = "      Редактировать время доставки";
            this.chkCanEditTime.UseVisualStyleBackColor = true;
            // 
            // tEditDeltaWindow
            // 
            this.tEditDeltaWindow.CausesValidation = false;
            this.tEditDeltaWindow.EditValue = new System.DateTime(2012, 1, 16, 0, 0, 0, 0);
            this.tEditDeltaWindow.Location = new System.Drawing.Point(223, 132);
            this.tEditDeltaWindow.Margin = new System.Windows.Forms.Padding(4);
            this.tEditDeltaWindow.Name = "tEditDeltaWindow";
            // 
            // 
            // 
            this.tEditDeltaWindow.Properties.AllowFocused = false;
            this.tEditDeltaWindow.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tEditDeltaWindow.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tEditDeltaWindow.Properties.DisplayFormat.FormatString = "t";
            this.tEditDeltaWindow.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditDeltaWindow.Properties.EditFormat.FormatString = "t";
            this.tEditDeltaWindow.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditDeltaWindow.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.tEditDeltaWindow.Properties.Mask.EditMask = "HH:mm";
            this.tEditDeltaWindow.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.tEditDeltaWindow.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.tEditDeltaWindow.Properties.NullText = "Время кухни";
            this.tEditDeltaWindow.Size = new System.Drawing.Size(101, 22);
            this.tEditDeltaWindow.TabIndex = 19;
            // 
            // lblDeltaWindow
            // 
            this.lblDeltaWindow.AutoSize = true;
            this.lblDeltaWindow.Location = new System.Drawing.Point(16, 135);
            this.lblDeltaWindow.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDeltaWindow.Name = "lblDeltaWindow";
            this.lblDeltaWindow.Size = new System.Drawing.Size(162, 17);
            this.lblDeltaWindow.TabIndex = 18;
            this.lblDeltaWindow.Text = "Время доставки \"Окно\"";
            // 
            // tEditDelta
            // 
            this.tEditDelta.CausesValidation = false;
            this.tEditDelta.EditValue = new System.DateTime(2012, 1, 16, 0, 0, 0, 0);
            this.tEditDelta.Location = new System.Drawing.Point(223, 97);
            this.tEditDelta.Margin = new System.Windows.Forms.Padding(4);
            this.tEditDelta.Name = "tEditDelta";
            // 
            // 
            // 
            this.tEditDelta.Properties.AllowFocused = false;
            this.tEditDelta.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.tEditDelta.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.tEditDelta.Properties.DisplayFormat.FormatString = "t";
            this.tEditDelta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditDelta.Properties.EditFormat.FormatString = "t";
            this.tEditDelta.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.tEditDelta.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.tEditDelta.Properties.Mask.EditMask = "HH:mm";
            this.tEditDelta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.tEditDelta.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.tEditDelta.Properties.NullText = "Время кухни";
            this.tEditDelta.Size = new System.Drawing.Size(101, 22);
            this.tEditDelta.TabIndex = 17;
            this.tEditDelta.EditValueChanged += new System.EventHandler(this.tEditDelta_EditValueChanged);
            // 
            // lblDelta
            // 
            this.lblDelta.AutoSize = true;
            this.lblDelta.Location = new System.Drawing.Point(16, 101);
            this.lblDelta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDelta.Name = "lblDelta";
            this.lblDelta.Size = new System.Drawing.Size(189, 17);
            this.lblDelta.TabIndex = 16;
            this.lblDelta.Text = "Время смещения доставки ";
            // 
            // cmbIsVacationDelivery
            // 
            this.cmbIsVacationDelivery.AutoSize = true;
            this.cmbIsVacationDelivery.Location = new System.Drawing.Point(16, 40);
            this.cmbIsVacationDelivery.Margin = new System.Windows.Forms.Padding(4);
            this.cmbIsVacationDelivery.Name = "cmbIsVacationDelivery";
            this.cmbIsVacationDelivery.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmbIsVacationDelivery.Size = new System.Drawing.Size(261, 21);
            this.cmbIsVacationDelivery.TabIndex = 15;
            this.cmbIsVacationDelivery.Text = "                 Доставка выходного дня";
            this.cmbIsVacationDelivery.UseVisualStyleBackColor = true;
            // 
            // lblStartSession
            // 
            this.lblStartSession.AutoSize = true;
            this.lblStartSession.Location = new System.Drawing.Point(16, 11);
            this.lblStartSession.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStartSession.Name = "lblStartSession";
            this.lblStartSession.Size = new System.Drawing.Size(151, 17);
            this.lblStartSession.TabIndex = 13;
            this.lblStartSession.Text = "Время начала сессии";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Controls.Add(this.btnAccept);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 468);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(796, 46);
            this.panel2.TabIndex = 15;
            // 
            // btnCancel
            // 
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Enabled = false;
            this.btnCancel.Location = new System.Drawing.Point(680, 7);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(112, 30);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Отменить";
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnCancel.UseCompatibleTextRendering = true;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAccept.Enabled = false;
            this.btnAccept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAccept.Location = new System.Drawing.Point(563, 7);
            this.btnAccept.Margin = new System.Windows.Forms.Padding(4);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(112, 30);
            this.btnAccept.TabIndex = 2;
            this.btnAccept.Text = "Применить";
            this.btnAccept.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAccept.UseCompatibleTextRendering = true;
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // SessionOptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 514);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SessionOptionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки сессии";
            this.Load += new System.EventHandler(this.SessionOptionsForm_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OpenSessionForm_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.tEditSession.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tEditDeltaWindow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tEditDelta.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TimeEdit tEditSession;
        private System.Windows.Forms.Button btnOpenSessoion;
        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPeopleWorks;
        private DevExpress.XtraGrid.Columns.GridColumn columnEquipmentWorks;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblStartSession;
        private System.Windows.Forms.CheckBox cmbIsVacationDelivery;
        private DevExpress.XtraEditors.TimeEdit tEditDelta;
        private System.Windows.Forms.Label lblDelta;
        private DevExpress.XtraEditors.TimeEdit tEditDeltaWindow;
        private System.Windows.Forms.Label lblDeltaWindow;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.CheckBox chkCanEditTime;
        private System.Windows.Forms.Label lblActivePointOfSales;
        private System.Windows.Forms.CheckedListBox chlbPointOfSales;
    }
}