﻿using System;
using DevExpress.Utils;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.Commands;
using DevExpress.XtraRichEdit.Services;

namespace LaRenzo.Forms.HelpForms
{
    #region customsavecommand
    public class CustomSaveDocumentCommand : SaveDocumentCommand
    {
        private IRichEditControl _control;
        
        public CustomSaveDocumentCommand(IRichEditControl control)
            : base(control)
        {
            _control = control;
        }

        protected override void ExecuteCore()
        {
            var richEdit = (HelpPageRichEditControl) _control;
            richEdit.OnHelpPageSaved(new EventArgs());
            richEdit.Modified = false;
        }
    }
    #endregion customsavecommand


    #region #iricheditcommandfactoryservice
    public class CustomRichEditCommandFactoryService : IRichEditCommandFactoryService
    {
        readonly IRichEditCommandFactoryService service;
        readonly RichEditControl control;

        public CustomRichEditCommandFactoryService(RichEditControl control,
            IRichEditCommandFactoryService service)
        {
            Guard.ArgumentNotNull(control, "control");
            Guard.ArgumentNotNull(service, "service");
            this.control = control;
            this.service = service;
        }

        #region IRichEditCommandFactoryService Members
        public RichEditCommand CreateCommand(RichEditCommandId id)
        {
            if (id == RichEditCommandId.FileSave)
                return new CustomSaveDocumentCommand(control);

            return service.CreateCommand(id);
        }
        #endregion
    }
    #endregion #iricheditcommandfactoryservice

}
