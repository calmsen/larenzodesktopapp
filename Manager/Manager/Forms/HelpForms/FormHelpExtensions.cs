﻿using System.Windows.Forms;

namespace LaRenzo.Forms.HelpForms
{
    public static class FormHelpExtensions
    {
        /// <summary>
        /// Включает поддержку вызова справочной системы для данной формы по нажатию клавиши "F1"
        /// </summary>
        /// <param name="form">Указатель на форму, для которой необходимо включить поддержку справочной системы</param>
        /// <param name="internalName">Внутрненнее название страницы (используется для связи форм и справочной информации)</param>
        /// <param name="name">Имя страницы в справочной системе. Будет использовано только для создания страницы, если ее еще нет в системе</param>
        public static string ActivateHelpPage(this Form form, string internalName = "", string name = "")
        {
            form.KeyPreview = true;
            form.KeyDown += (sender, args) =>
            {
                if (args.KeyCode == Keys.F1) HelpForm.GetHelpForm(internalName, name);
            };

            return internalName;
        }


        public static void ShowHelpPage(string internalName, string name)
        {
            HelpForm.GetHelpForm(internalName, name);
        }

    }
}
