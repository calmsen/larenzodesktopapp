﻿using System;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.Services;

namespace LaRenzo.Forms.HelpForms
{
    public class HelpPageRichEditControl: RichEditControl
    {
        public delegate void HelpPageContentSavedEventHandler(object sender, EventArgs args);

        public event HelpPageContentSavedEventHandler HelpPageContentSaved;

        public virtual void OnHelpPageSaved(EventArgs args)
        {
            var handler = HelpPageContentSaved;
            if (handler != null) handler(this, args);
        }

        public HelpPageRichEditControl()
        {
            ReplaceRichEditCommandFactoryService(this);
        }

        void ReplaceRichEditCommandFactoryService(RichEditControl control)
        {
            var service = control.GetService<IRichEditCommandFactoryService>();
            if (service == null)
                return;
            control.RemoveService(typeof(IRichEditCommandFactoryService));
            control.AddService(typeof(IRichEditCommandFactoryService), new CustomRichEditCommandFactoryService(control, service));
        }

    }


}
