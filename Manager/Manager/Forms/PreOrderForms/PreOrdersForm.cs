﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataRepository.Entities;
using DevExpress.XtraGrid.Views.Grid;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using PopupMenuShowingEventArgs = DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs;

namespace LaRenzo.Forms.PreOrderForms
{
    public partial class PreOrdersForm : Form
    {

        private List<AccessRightInfo> UserRights { get; set; }

        public PreOrdersForm()
        {
            InitializeComponent();

            создатьЗаказToolStripMenuItem.Click += async (s, e) => await CreateOrderBtn_Click();
            
            редактироватьToolStripMenuItem.Click += async (s, e) => await EditPreOrderBtn_Click();
            
            mainGridView.DoubleClick += async (s, e) => await mainGridView_DoubleClick();
        }

        /// <summary> Выбранный предзаказ </summary>
        private PreOrder _selectedPreorder;


        private void PreOrdersForm_Load(object sender, EventArgs e)
        {
            UserRights = Content.UserAccessManager.GetForUser(Content.UserManager.UserLogged.ID);

            dTimeDatePreOrders.Enabled =
                cmbShowAllAfter.Enabled =
                    cmbShowAllAfter.Checked =
                        DeleteMenuButton.Enabled =
                        btnEditPreOrderListOptions.Enabled =
                            Content.UserAccessManager.GroupContainsRight(UserRights, AccessRightAlias.PreOrderWatchTomorrow);

            Bind();
        }

        private void Bind()
        {
            var data = Content.PreOrderManager.GetList(dTimeDatePreOrders.Value, cmbShowAllAfter.Checked);
            var neededProductList = Content.PreOrderManager.GetNeededProductList(dTimeDatePreOrders.Value, cmbShowAllAfter.Checked);
            mainGridControl.DataSource = data;
            productGridControl.DataSource = neededProductList;
        }

        private void datePreOrders_ValueChanged(object sender, EventArgs e)
        {
            Bind();
        }

        private void checkBoxShowAllAfter_CheckedChanged(object sender, EventArgs e)
        {
            Bind();
        }


        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Двойной клик по предзаказу </summary>
        ////===============================================
        private async Task mainGridView_DoubleClick()
        {
            var rowHandle = mainGridView.GetSelectedRows()[0];

            var isDataRow = mainGridView.IsDataRow(rowHandle);

            if (isDataRow)
            {
                var preOrder = mainGridView.GetRow(rowHandle) as PreOrder;

                if (!IsSelectedRowDeletedPreOrder(preOrder))
                {
                    await OpenSelectedPreOrder(PreOrderFormMode.CreateOrder, preOrder);
                }
            }
            else
            {
                MessageBox.Show("");
            }
        }


        private bool IsSelectedRowDeletedPreOrder(PreOrder preOrder)
        {
            return preOrder != null && preOrder.IsDeleted;
        }


        private async Task OpenSelectedPreOrder(PreOrderFormMode mode, PreOrder preOrder)
        {
            var forEdit = mode == PreOrderFormMode.EditPreOrder;

            if (forEdit && !Content.UserAccessManager.GroupContainsRight(UserRights, AccessRightAlias.PreOrderEdit))
            {
                MessageBox.Show(@"У вас нет прав на редактирование предзаказа");
            }
            else if (!forEdit && !Content.UserAccessManager.GroupContainsRight(UserRights, AccessRightAlias.PreOrderCreate))
            {
                MessageBox.Show(@"У вас нет прав на создание заказа из предзаказа");
            }
            else if (preOrder != null && Content.UserManager.UserLogged != null)
            {
                await EditDetails(preOrder, forEdit);
            }
        }


        public async Task EditDetails(PreOrder preorder, bool forEdit = false)
        {
            try
            {
                // Проверка на соответствие бренду
                if (preorder.Order.Brand.ID == Content.BrandManager.SelectedBrand.ID)
                {
                    splashScreenManagerWaitFormDetails.ShowWaitForm();
                    splashScreenManagerWaitFormDetails.SetWaitFormCaption("Открывается заказ...");
                    splashScreenManagerWaitFormDetails.SetWaitFormDescription("Подождите немного");
                    var brand = Content.BrandManager.GetList(new BrandFilter { BrandName = "Ларензо" }).FirstOrDefault();
                    var form = await DeliveryOrderFormFactory.GetForm(preorder, brand, forEdit ? DetailsFormModes.EditOrder : DetailsFormModes.NewOrderFromPreOrder);

                    form.OrderProcessed += (state, order) => Bind();
                    form.Show();
                    splashScreenManagerWaitFormDetails.CloseWaitForm();
                }
                else
                {
                    MessageBox.Show(@"Вы не можете обрабатывать заявки другого бренда");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void mainGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            var view = sender as GridView;

            if (view != null)
            {
                var hitInfo = view.CalcHitInfo(e.Point);

                if (view.IsDataRow(hitInfo.RowHandle))
                {
                    var preOrder = view.GetRow(hitInfo.RowHandle) as PreOrder;

                    if (preOrder != null)
                    {
                        _selectedPreorder = preOrder;

                        // Проверка на соответствие бренду
                        if (preOrder.Order.Brand.ID == Content.BrandManager.SelectedBrand.ID)
                        {

                            ContextMenu.Items.Clear();

                            if (preOrder.IsDeleted)
                            {
                                if (Content.UserAccessManager.GroupContainsRight(UserRights, AccessRightAlias.PreOrderRestore))
                                {
                                    ContextMenu.Items.Add(new ToolStripButton("Восстановить", null, RestorePreOrderBtn_Click));
                                }
                            }
                            else
                            {
                                if (Content.UserAccessManager.GroupContainsRight(UserRights, AccessRightAlias.PreOrderCreate))
                                {
                                    ContextMenu.Items.Add(new ToolStripButton("Создать заказ", null, async (s, eventElem) => await CreateOrderBtn_Click()));
                                }

                                if (Content.UserAccessManager.GroupContainsRight(UserRights, AccessRightAlias.PreOrderEdit))
                                {
                                    ContextMenu.Items.Add(new ToolStripButton("Редактировать", null, async (s, eventElem) => await EditPreOrderBtn_Click()));
                                }

                                if (Content.UserAccessManager.GroupContainsRight(UserRights, AccessRightAlias.PreOrderRemove))
                                {
                                    ContextMenu.Items.Add(new ToolStripButton("Удалить", null, RemovePreOrderBtn_Click));
                                }
                            }

                            ContextMenu.Width = 300;

                            if (ContextMenu.Items.Count > 0)
                            {
                                ContextMenu.Show(view.GridControl, e.Point);
                            }
                            else
                            {
                                MessageBox.Show(@"У вас нет прав на работу с этим предзаказом");
                            }
                        }
                        else
                        {
                            MessageBox.Show(@"Вы не можете обрабатывать заявки другого бренда");
                        }
                    }
                }
            }
        }

        private async Task CreateOrderBtn_Click()
        {
            if (!IsSelectedRowDeletedPreOrder(_selectedPreorder))
            {
                await OpenSelectedPreOrder(PreOrderFormMode.CreateOrder, _selectedPreorder);
            }
        }

        private async Task EditPreOrderBtn_Click()
        {
            if (!IsSelectedRowDeletedPreOrder(_selectedPreorder))
            {
                await OpenSelectedPreOrder(PreOrderFormMode.EditPreOrder, _selectedPreorder);
            }
        }

        private void RemovePreOrderBtn_Click(object sender, EventArgs e)
        {
            var row = mainGridView.GetSelectedRows()[0];
            if (mainGridView.IsDataRow(row))
            {
                if (Content.UserManager.UserLogged != null)
                {
                    var preOrder = mainGridView.GetRow(row) as PreOrder;
                    if (preOrder != null)
                    {
                        Content.PreOrderManager.Remove(preOrder.ID);
                    }
                    Bind();
                }
            }
        }

        private void RestorePreOrderBtn_Click(object sender, EventArgs e)
        {
            var row = mainGridView.GetSelectedRows()[0];
            if (mainGridView.IsDataRow(row))
            {
                if (Content.UserManager.UserLogged != null)
                {
                    var preOrder = mainGridView.GetRow(row) as PreOrder;
                    if (preOrder != null)
                        Content.PreOrderManager.Restore(preOrder.ID);
                    Bind();
                }
            }
        }

        private void btnEditPreOrderListOptions_Click(object sender, EventArgs e)
        {
            var selectedValue = (PreOrder)mainGridView.GetFocusedRow();

            if (selectedValue != null)
            {
                var editTime = new EditPreOrderTime(selectedValue.ID, selectedValue.ReservationDateTime);

                if (editTime.ShowDialog() == DialogResult.OK)
                    Bind();
            }
        }

        private void mainGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            var view = sender as GridView;
            if (e.RowHandle < 0 || view == null) return;

            var preOrder = view.GetRow(e.RowHandle) as PreOrder;

            if (preOrder == null) return;

            if (preOrder.IsDeleted) e.Appearance.BackColor = Color.Salmon;
        }


    }

    public enum PreOrderFormMode
    {
        EditPreOrder,
        CreateOrder
    }
}
