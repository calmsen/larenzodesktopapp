﻿namespace LaRenzo.DataRepository.Forms
{
    partial class ReservationDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFirstTime = new System.Windows.Forms.Button();
            this.btnSecondTime = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnFirstTime
            // 
            this.btnFirstTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnFirstTime.Location = new System.Drawing.Point(25, 65);
            this.btnFirstTime.Name = "btnFirstTime";
            this.btnFirstTime.Size = new System.Drawing.Size(150, 47);
            this.btnFirstTime.TabIndex = 0;
            this.btnFirstTime.Text = "button1";
            this.btnFirstTime.UseVisualStyleBackColor = true;
            this.btnFirstTime.Click += new System.EventHandler(this.btnFirstTime_Click);
            // 
            // btnSecondTime
            // 
            this.btnSecondTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSecondTime.Location = new System.Drawing.Point(181, 65);
            this.btnSecondTime.Name = "btnSecondTime";
            this.btnSecondTime.Size = new System.Drawing.Size(148, 47);
            this.btnSecondTime.TabIndex = 1;
            this.btnSecondTime.Text = "button2";
            this.btnSecondTime.UseVisualStyleBackColor = true;
            this.btnSecondTime.Click += new System.EventHandler(this.btnSecondTime_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(22, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(309, 29);
            this.label1.TabIndex = 2;
            this.label1.Text = "Выберите время резерва";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // ReservationDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 135);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSecondTime);
            this.Controls.Add(this.btnFirstTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReservationDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Резерв";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFirstTime;
        private System.Windows.Forms.Button btnSecondTime;
        private System.Windows.Forms.Label label1;
    }
}