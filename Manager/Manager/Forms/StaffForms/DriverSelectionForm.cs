﻿using System;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Drivers;

namespace LaRenzo.Forms.StaffForms
{
    public partial class DriverSelectionForm : Form
    {
        /// <summary>
        /// </summary>
        /// <param name="orderId">Ид заказа</param>
        /// <param name="driver">Ссылка на водителя </param>
        public delegate void DriverSelectedEventHandler(int orderId, Driver driver);

        /// <summary>
        /// Срабатывает после выбора водителя
        /// </summary>
        public event DriverSelectedEventHandler DriverSelected;

        public void OnDriverSelected(int orderId, Driver driver)
        {
            DriverSelectedEventHandler handler = DriverSelected;
            if (handler != null) handler(orderId, driver);
        }

        public int OrderId { get; set; }

        
        public DriverSelectionForm(int orderId)
        {
            InitializeComponent();

            OrderId = orderId;

            cmbDriver.Items.Clear();
            // ReSharper disable CoVariantArrayConversion
            cmbDriver.Items.AddRange(Content.DriverManager.GetList().OrderBy(x => x.Name).Select(x => x.Name).ToArray());
            // ReSharper restore CoVariantArrayConversion
	        
			// Если есть водители, то устанавливаем выбор с первого
			if (cmbDriver.Items.Count > 0)
	        {
		        cmbDriver.SelectedIndex = 0;
	        }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var driver = Content.DriverManager.GetByName(cmbDriver.Text);
            if (driver != null) OnDriverSelected(OrderId, driver);

            Close();
        }

    }
}
