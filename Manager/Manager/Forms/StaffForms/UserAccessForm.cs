﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraTreeList;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.FormsProvider;

namespace LaRenzo.Forms.StaffForms
{
    public partial class UserAccessForm : Form
    {
        private User PreSelectedUser;
        private RightListWithChangesDetection detector;
        
        public UserAccessForm()
        {
            InitializeComponent();
        }

        public UserAccessForm(int userId)
        {
            InitializeComponent();
            PreSelectedUser = Content.UserManager.GetById(userId);
        }

        private void UserAccessForm_Load(object sender, EventArgs e)
        {
            treeList1.ShownEditor += treeList1_ShownEditor;
            
            userComboBox.ComboBox.DataSource = Content.UserManager.GetList();
            userComboBox.ComboBox.DisplayMember = "Name";

            if (PreSelectedUser != null)
                foreach (var item in userComboBox.Items)
                {
                    if (((User) item).ID == PreSelectedUser.ID) userComboBox.SelectedItem = item;
                }

        }

        void treeList1_ShownEditor(object sender, EventArgs e)
        {
            var tree = sender as TreeList;
            tree.ActiveEditor.EditValueChanged -= OnEditValueChanged;
            tree.ActiveEditor.EditValueChanged += OnEditValueChanged;
        }

        void OnEditValueChanged(object sender, EventArgs e)
        {
            treeList1.PostEditor();
            treeList1.ShowEditor();
        }

        private void userComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedUser = (User) userComboBox.SelectedItem;

            detector = new RightListWithChangesDetection(Content.UserAccessManager.GetForUser(selectedUser.ID, true));

            treeList1.DataSource = detector.Data;
            treeList1.ParentFieldName = "ParentCode";
            treeList1.KeyFieldName = "Code";
            treeList1.CellValueChanged += (o, args) => toolStripLabel1.Text = "Есть несохраненные изменения";

            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Content.UserAccessManager.SetForUser(((User)userComboBox.SelectedItem).ID, ((BindingList<AccessRightInfo>)treeList1.DataSource).ToList());
            toolStripLabel1.Text = "Сохранено";
        }

        private void treeList1_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
        {

        }


        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            detector.Data.ForEach(x => x.Allowed = true);
        }

        private void toolStripButton2_Click_1(object sender, EventArgs e)
        {
            detector.Data.ForEach(x => x.Allowed = false);
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            treeList1.ExpandAll();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            treeList1.CollapseAll();
        }

        private void userComboBox_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<UserAccessCopyForm>.ActivateForm(() => new UserAccessCopyForm((User)userComboBox.SelectedItem));
        }

    }
}
