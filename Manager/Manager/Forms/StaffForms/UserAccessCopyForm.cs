﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;

namespace LaRenzo.Forms.StaffForms
{
    public partial class UserAccessCopyForm : Form
    {
        public User CopyToUser { get; set; }
        
        public UserAccessCopyForm(User copyToUser)
        {
            InitializeComponent();
            CopyToUser = copyToUser;
        }

        private void UserAccessCopyForm_Load(object sender, EventArgs e)
        {
            var users = Content.UserManager.GetList().Where(x => x.ID != CopyToUser.ID).ToList();

            foreach (var user in users)
            {
                var button = new Button();
                button.Text = user.Name;
                User user1 = user;
                button.Click += (o, args) => CopyFromUserToUser(user1);
                button.AutoSizeMode = AutoSizeMode.GrowOnly;
                button.AutoSize = true;
                flowLayoutPanel1.Controls.Add(button);
            }
        }

        private void CopyFromUserToUser(User fromUser)
        {
            Content.UserAccessManager.SetForUser(CopyToUser.ID, Content.UserAccessManager.GetForUser(fromUser.ID));
            Close();
        }
    }
}
