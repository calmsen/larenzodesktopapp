﻿using System;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.ControlsLib.BindFilterListControl;
using LaRenzo.DataRepository.Entities.Catalogs.Users;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Others;
using LaRenzo.Properties;

namespace LaRenzo.Forms.StaffForms
{
    public partial class UsersForm : Form
    {
        private int SelectedUserId
        {
            get
            {
                if (bFilterListUsers.GetSelected() != null)
                {
                    return bFilterListUsers.GetSelected().ID;
                }

                return 0;
            }
        }
        public UsersForm()
        {
            InitializeComponent();
            foreach (var password in Enum.GetValues(typeof(CashboxPassword)))
            {
                comboBoxPrintPassword.Items.Add((int)password);
            }
        }

        public void ChangeState(int state, int id = 0)
        {
            switch (state)
            {
                case 0:
                    btnUserAdd.Enabled = true;
                    btnUserDelete.Enabled = false;
                    btnUserUpdate.Enabled = false;
                    break;
                case 1:
                    btnUserAdd.Enabled = false;
                    btnUserDelete.Enabled = true;
                    btnUserUpdate.Enabled = true;
                    break;
            }

            if (id > 0)
            {
                User user = Content.UserManager.GetById(id);
                txtUserName.Text = user.Name;
                chkBoxUserIsAdmin.Checked = (user.RoleID == 1);
                chkBoxUserIsAdmin.Enabled = (Content.UserManager.UserLogged.ID != user.ID);
                comboBoxPrintPassword.SelectedItem = (int)user.CashboxPassword;
                chkWarehouseManager.Checked = (user.RoleID == 3);
                chkWarehouseManager.Enabled = (Content.UserManager.UserLogged.ID != user.ID);

                chkBoxUserIsCafe.Checked = (user.RoleID == 4);
                chkBoxUserIsCafe.Enabled = (Content.UserManager.UserLogged.ID != user.ID);
            }
            else
            {
                txtUserName.Text = txtUserPass.Text = txtUserPassAgain.Text = String.Empty;
                chkBoxUserIsAdmin.Checked = false;
                chkBoxUserIsAdmin.Enabled = true;

                chkWarehouseManager.Checked = false;
                chkWarehouseManager.Enabled = true;

                chkBoxUserIsCafe.Checked = false;
                chkBoxUserIsCafe.Enabled = true;

                bFilterListUsers.Deselect();
            }


            var users = Content.UserManager.GetList();
            bFilterListUsers.Bind(users.Select(x => new BindFilterList.BindListRow(x.ID, x.Name)).ToList());

        }

        public User ParseUser(bool forUpdate)
        {
            var user = new User
                           {
                               Name = txtUserName.Text
                           };
            Enum.TryParse(comboBoxPrintPassword.SelectedItem.ToString(),out CashboxPassword outPassword);
            user.CashboxPassword = outPassword;
            if (txtUserPass.Text == txtUserPassAgain.Text) user.Password = txtUserPass.Text;
            else
            {
                MessageBox.Show(Resources.PasswordIsNotEquals);
                return null;
            }

            user.RoleID = (chkBoxUserIsAdmin.Checked)
                              ? 1
                              : (chkWarehouseManager.Checked) ? 3 : (chkBoxUserIsCafe.Checked) ? 4 : 2;

            string errors = Content.UserManager.Validate(user, !forUpdate, !forUpdate);
            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                return null;
            }

            return user;
        }

        private void btnUserDrop_Click(object sender, EventArgs e)
        {
            ChangeState(0);
        }

        private void btnUserUpdate_Click(object sender, EventArgs e)
        {
            User user = ParseUser(true);
            if (user != null)
            {
                user.ID = SelectedUserId;
                user.Password = string.IsNullOrWhiteSpace(user.Password)
                                    ? Content.UserManager.GetById(user.ID).Password
                                    : new Hasher().GetHashString(user.Password);
                Content.UserManager.Update(user);
                ChangeState(0);
            }
        }

        private void btnUserDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Content.UserManager.Remove(SelectedUserId);
                ChangeState(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UsersForm_Load(object sender, EventArgs e)
        {
            ChangeState(0);
        }



        private void btnUserAdd_Click(object sender, EventArgs e)
        {
            var user = ParseUser(false);
            if (user != null)
            {
                user.Password = new Hasher().GetHashString(user.Password);
                Content.UserManager.Add(user);
                ChangeState(0);
            }
        }

        private void bFilterListUsers_SelectedRowChanged(object sender, SelectedRowChangedEventArgs e)
        {
            ChangeState(1, e.ID);
        }

        private void btnUserRights_Click(object sender, EventArgs e)
        {
            if (SelectedUserId == 0)
            {
                MessageBox.Show("Сначала создайте и сохраните пользователя, или выбирите из списка слева!");
                return;
            }
            var rightForm = new UserAccessForm(SelectedUserId);
            rightForm.Show();
        }
    }
}