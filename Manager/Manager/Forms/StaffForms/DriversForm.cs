﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.ControlsLib.BindFilterListControl;
using LaRenzo.DataRepository.Entities.Catalogs.Drivers;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Drivers;
using LaRenzo.DataRepository.Repositories.Others;
using LaRenzo.Properties;

namespace LaRenzo.Forms.StaffForms
{
    public partial class DriversForm : Form
    {
        public DriversForm()
        {
            InitializeComponent();
        }

        public void ChangeState(int state, int id = -1)
        {
            switch (state)
            {
                case 0:
                    btnAdd.Enabled = true;
                    btnDelete.Enabled = false;
                    btnUpdate.Enabled = false;
                    break;
                case 1:
                    btnAdd.Enabled = false;
                    btnDelete.Enabled = true;
                    btnUpdate.Enabled = true;
                    break;
            }

            if (id > 0)
            {
                var driver = Content.DriverManager.GetById(id);
                txtName.Text = driver.Name;
                txtIdToEdit.Text = id.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                txtName.Text = txtIdToEdit.Text = txtNewPass.Text = txtRePass.Text = string.Empty;
                bFilterListDrivers.Deselect();
            }


            var drivers = Content.DriverManager.GetList();
            bFilterListDrivers.Bind(
                drivers.Select(x => new BindFilterList.BindListRow(x.ID, x.Name))
                    .ToList());

        }

        public Driver ParseDriver(bool forUpdate)
        {
            var driver = new Driver
                          {
                              Name = txtName.Text,
                          };
            if (txtNewPass.Text == txtRePass.Text) driver.Password = txtNewPass.Text;
            else
            {
                MessageBox.Show(Resources.PasswordIsNotEquals);
                return null;
            }
            string errors = Content.DriverManager.Validate(driver, !forUpdate);
            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                return null;
            }

            return driver;
        }

        private void btnDrop_Click(object sender, EventArgs e)
        {
            ChangeState(0);
        }

        private void btnCatAdd_Click(object sender, EventArgs e)
        {
            Driver driver = ParseDriver(false);
            if (driver != null)
            {
                driver.Password = new Hasher().GetHashString(driver.Password);
                Content.DriverManager.Add(driver);
                ChangeState(0);
            }
        }

        private void btnCatUpdate_Click(object sender, EventArgs e)
        {
            Driver driver = ParseDriver(true);
            if (driver != null)
            {
                driver.ID = int.Parse(txtIdToEdit.Text);

                driver.Password = string.IsNullOrWhiteSpace(driver.Password)
                                    ? Content.DriverManager.GetById(driver.ID).Password
                                    : new Hasher().GetHashString(driver.Password);

                Content.DriverManager.Update(driver);
                ChangeState(0);
            }
        }

        private void btnCatDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Content.DriverManager.Remove(int.Parse(txtIdToEdit.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            ChangeState(0);
        }

        private void CategoriesForm_Load(object sender, EventArgs e)
        {
            ChangeState(0);
        }

        private void bFilterListDrivers_SelectedRowChanged(object sender, SelectedRowChangedEventArgs e)
        {
            ChangeState(1, e.ID);
        }
    }
}
