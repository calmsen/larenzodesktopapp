﻿using System;
using System.Linq;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Addresses;
using LaRenzo.Properties;

namespace LaRenzo.Forms.StaffForms
{
    public partial class SimpleComboBoxDialogForm : Form
    {
        public string FieldName { get; set; }
        public int Result { get; set; }
        public string Input { get; set; }

        public SimpleComboBoxDialogForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbAddress.SelectedItem == null)
            {
                var ep = new ErrorProvider();
                ep.SetError(cmbAddress, Resources.ErrorRequiredField);
            }
            else
            {
                Result = ((Address) cmbAddress.SelectedItem).ID;
                DialogResult = DialogResult.OK;
            }
        }

        private void SimpleComboBoxDialogForm_Load(object sender, EventArgs e)
        {
            cmbAddress.DataSource =
                Content.AddressManager.GetAddressList()
                                       .Where(x => x.AddressGroupID == null)
                                       .ToList();
            cmbAddress.DisplayMember = "Name";
            cmbAddress.SelectedItem = null;
            lblName.Text = FieldName;
        }
    }
}
