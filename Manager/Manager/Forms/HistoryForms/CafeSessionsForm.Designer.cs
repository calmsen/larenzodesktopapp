﻿namespace LaRenzo.Forms.HistoryForms
{
    partial class CafeSessionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.detailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnCafeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCafeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCafeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCafeTable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coulmnDocSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnIdCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnOpenDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCloseDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.linqServerModeSourceSessions = new DevExpress.Data.Linq.LinqServerModeSource();
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourceSessions)).BeginInit();
            this.SuspendLayout();
            // 
            // detailGridView
            // 
            this.detailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnCafeId,
            this.columnCafeNumber,
            this.columnCafeDate,
            this.columnCafeTable,
            this.columnUser,
            this.coulmnDocSum});
            this.detailGridView.GridControl = this.mainGridControl;
            this.detailGridView.Name = "detailGridView";
            // 
            // columnCafeId
            // 
            this.columnCafeId.Caption = "Ид";
            this.columnCafeId.FieldName = "ID";
            this.columnCafeId.Name = "columnCafeId";
            this.columnCafeId.OptionsColumn.AllowEdit = false;
            this.columnCafeId.OptionsColumn.AllowFocus = false;
            this.columnCafeId.OptionsColumn.ReadOnly = true;
            this.columnCafeId.Visible = true;
            this.columnCafeId.VisibleIndex = 0;
            // 
            // columnCafeNumber
            // 
            this.columnCafeNumber.Caption = "Код";
            this.columnCafeNumber.FieldName = "Code";
            this.columnCafeNumber.Name = "columnCafeNumber";
            this.columnCafeNumber.OptionsColumn.AllowEdit = false;
            this.columnCafeNumber.OptionsColumn.AllowFocus = false;
            this.columnCafeNumber.OptionsColumn.ReadOnly = true;
            this.columnCafeNumber.Visible = true;
            this.columnCafeNumber.VisibleIndex = 1;
            // 
            // columnCafeDate
            // 
            this.columnCafeDate.Caption = "Дата заказа";
            this.columnCafeDate.FieldName = "Created";
            this.columnCafeDate.Name = "columnCafeDate";
            this.columnCafeDate.OptionsColumn.AllowEdit = false;
            this.columnCafeDate.OptionsColumn.AllowFocus = false;
            this.columnCafeDate.OptionsColumn.ReadOnly = true;
            this.columnCafeDate.Visible = true;
            this.columnCafeDate.VisibleIndex = 2;
            // 
            // columnCafeTable
            // 
            this.columnCafeTable.Caption = "Столик";
            this.columnCafeTable.FieldName = "Table.Name";
            this.columnCafeTable.Name = "columnCafeTable";
            this.columnCafeTable.OptionsColumn.AllowEdit = false;
            this.columnCafeTable.OptionsColumn.AllowFocus = false;
            this.columnCafeTable.OptionsColumn.ReadOnly = true;
            this.columnCafeTable.Visible = true;
            this.columnCafeTable.VisibleIndex = 3;
            // 
            // columnUser
            // 
            this.columnUser.Caption = "Пользователь";
            this.columnUser.FieldName = "User";
            this.columnUser.Name = "columnUser";
            this.columnUser.OptionsColumn.AllowEdit = false;
            this.columnUser.OptionsColumn.AllowFocus = false;
            this.columnUser.OptionsColumn.ReadOnly = true;
            this.columnUser.Visible = true;
            this.columnUser.VisibleIndex = 5;
            // 
            // coulmnDocSum
            // 
            this.coulmnDocSum.Caption = "Сумма документа";
            this.coulmnDocSum.FieldName = "DocumentSum";
            this.coulmnDocSum.Name = "coulmnDocSum";
            this.coulmnDocSum.OptionsColumn.AllowEdit = false;
            this.coulmnDocSum.OptionsColumn.AllowFocus = false;
            this.coulmnDocSum.OptionsColumn.ReadOnly = true;
            this.coulmnDocSum.Visible = true;
            this.coulmnDocSum.VisibleIndex = 4;
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.detailGridView;
            gridLevelNode1.RelationName = "CafeOrderDocuments";
            this.mainGridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(987, 467);
            this.mainGridControl.TabIndex = 4;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView,
            this.detailGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnIdCol,
            this.columnOpenDate,
            this.columnCloseDate,
            this.columnTotal});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.MasterRowGetChildList += new DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventHandler(this.gridView1_MasterRowGetChildList);
            this.mainGridView.MasterRowGetRelationName += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridView1_MasterRowGetRelationName);
            this.mainGridView.MasterRowGetRelationDisplayCaption += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridView1_MasterRowGetRelationDisplayCaption);
            this.mainGridView.MasterRowGetRelationCount += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventHandler(this.gridView1_MasterRowGetRelationCount);
            // 
            // columnIdCol
            // 
            this.columnIdCol.Caption = "Ид";
            this.columnIdCol.FieldName = "ID";
            this.columnIdCol.Name = "columnIdCol";
            this.columnIdCol.OptionsColumn.AllowEdit = false;
            this.columnIdCol.OptionsColumn.AllowFocus = false;
            this.columnIdCol.OptionsColumn.ReadOnly = true;
            this.columnIdCol.Visible = true;
            this.columnIdCol.VisibleIndex = 0;
            // 
            // columnOpenDate
            // 
            this.columnOpenDate.Caption = "Открыта";
            this.columnOpenDate.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm";
            this.columnOpenDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnOpenDate.FieldName = "Opened";
            this.columnOpenDate.Name = "columnOpenDate";
            this.columnOpenDate.OptionsColumn.AllowEdit = false;
            this.columnOpenDate.OptionsColumn.AllowFocus = false;
            this.columnOpenDate.OptionsColumn.ReadOnly = true;
            this.columnOpenDate.Visible = true;
            this.columnOpenDate.VisibleIndex = 1;
            // 
            // columnCloseDate
            // 
            this.columnCloseDate.Caption = "Закрыта";
            this.columnCloseDate.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm";
            this.columnCloseDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnCloseDate.FieldName = "Closed";
            this.columnCloseDate.Name = "columnCloseDate";
            this.columnCloseDate.OptionsColumn.AllowEdit = false;
            this.columnCloseDate.OptionsColumn.AllowFocus = false;
            this.columnCloseDate.OptionsColumn.ReadOnly = true;
            this.columnCloseDate.Visible = true;
            this.columnCloseDate.VisibleIndex = 2;
            // 
            // columnTotal
            // 
            this.columnTotal.Caption = "Выручка за сессиию";
            this.columnTotal.FieldName = "TotalCafeRevenue";
            this.columnTotal.Name = "columnTotal";
            this.columnTotal.OptionsColumn.AllowEdit = false;
            this.columnTotal.OptionsColumn.AllowFocus = false;
            this.columnTotal.OptionsColumn.ReadOnly = true;
            this.columnTotal.Visible = true;
            this.columnTotal.VisibleIndex = 3;
            // 
            // linqServerModeSourceSessions
            // 
            this.linqServerModeSourceSessions.KeyExpression = "ID";
            // 
            // CafeSessionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 467);
            this.Controls.Add(this.mainGridControl);
            this.Name = "CafeSessionsForm";
            this.Text = "Все сессии";
            ((System.ComponentModel.ISupportInitialize)(this.detailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linqServerModeSourceSessions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.Data.Linq.LinqServerModeSource linqServerModeSourceSessions;
        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnIdCol;
        private DevExpress.XtraGrid.Columns.GridColumn columnOpenDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnCloseDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnTotal;
        private DevExpress.XtraGrid.Views.Grid.GridView detailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnCafeId;
        private DevExpress.XtraGrid.Columns.GridColumn columnCafeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn columnCafeDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnCafeTable;
        private DevExpress.XtraGrid.Columns.GridColumn columnUser;
        private DevExpress.XtraGrid.Columns.GridColumn coulmnDocSum;
    }
}