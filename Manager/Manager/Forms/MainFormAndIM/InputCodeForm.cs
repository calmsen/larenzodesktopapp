﻿using System;
using System.Windows.Forms;

namespace LaRenzo.Forms.MainFormAndIM
{
	public partial class InputCodeForm : Form
	{
		public InputCodeForm()
		{
			InitializeComponent();
		}

		
		//__________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Отмена" </summary>
		////====================================
		private void CancelButtonClick(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}


		//__________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Кнопка "Ok" </summary>
		////================================
		private void OKButtonClick(object sender, EventArgs e)
		{
			TestCode();
		}


		//__________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Нажата кнопка в TextBox'e </summary>
		////==============================================
		private void DefenderTextBoxKeyUp(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				TestCode();
			}
		}


		//__________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Проверка введённого кода </summary>
		////=============================================
		private void TestCode()
		{
			// Введённый код
			int inputCode;
			int.TryParse(defenderTextBox.Text, out inputCode);

			// Правильный код
			int realCode = (DateTime.Now.Hour)*2;

			if (realCode == inputCode)
			{
				DialogResult = DialogResult.OK;
			}
			else
			{
				MessageBox.Show(@"Введёный код не верен");
			}
		}
	}
}
