﻿namespace LaRenzo.Forms.MainFormAndIM
{
    partial class ClearDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.checkBoxMain = new System.Windows.Forms.CheckBox();
            this.checkBoxReserve = new System.Windows.Forms.CheckBox();
            this.checkBoxDBFree = new System.Windows.Forms.CheckBox();
            this.checkBoxAttention = new System.Windows.Forms.CheckBox();
            this.checkBoxErrorHandling = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCode = new System.Windows.Forms.TextBox();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(284, 29);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(120, 20);
            this.dateTimePicker1.TabIndex = 0;
            this.dateTimePicker1.Value = new System.DateTime(2015, 10, 1, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Дата, раньше кторой все данные будут удалены";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Salmon;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(25, 385);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(437, 84);
            this.button1.TabIndex = 4;
            this.button1.Text = "Удалить данные";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(25, 102);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(437, 153);
            this.richTextBox1.TabIndex = 5;
            this.richTextBox1.Text = "";
            // 
            // checkBoxMain
            // 
            this.checkBoxMain.AutoSize = true;
            this.checkBoxMain.Location = new System.Drawing.Point(22, 265);
            this.checkBoxMain.Name = "checkBoxMain";
            this.checkBoxMain.Size = new System.Drawing.Size(356, 17);
            this.checkBoxMain.TabIndex = 6;
            this.checkBoxMain.Text = "Я понимаю, что будут безвозвратно удалены данные, поэтому я:";
            this.checkBoxMain.UseVisualStyleBackColor = true;
            this.checkBoxMain.CheckedChanged += new System.EventHandler(this.checkBoxMain_CheckedChanged);
            // 
            // checkBoxReserve
            // 
            this.checkBoxReserve.AutoSize = true;
            this.checkBoxReserve.Enabled = false;
            this.checkBoxReserve.Location = new System.Drawing.Point(54, 288);
            this.checkBoxReserve.Name = "checkBoxReserve";
            this.checkBoxReserve.Size = new System.Drawing.Size(405, 17);
            this.checkBoxReserve.TabIndex = 7;
            this.checkBoxReserve.Text = "Создал резервную копию базы данных и удостоверился, что она работает";
            this.checkBoxReserve.UseVisualStyleBackColor = true;
            // 
            // checkBoxDBFree
            // 
            this.checkBoxDBFree.AutoSize = true;
            this.checkBoxDBFree.Enabled = false;
            this.checkBoxDBFree.Location = new System.Drawing.Point(54, 311);
            this.checkBoxDBFree.Name = "checkBoxDBFree";
            this.checkBoxDBFree.Size = new System.Drawing.Size(388, 17);
            this.checkBoxDBFree.TabIndex = 8;
            this.checkBoxDBFree.Text = "Убедился, что все остальные пользователи не используют программу";
            this.checkBoxDBFree.UseVisualStyleBackColor = true;
            // 
            // checkBoxAttention
            // 
            this.checkBoxAttention.AutoSize = true;
            this.checkBoxAttention.Enabled = false;
            this.checkBoxAttention.Location = new System.Drawing.Point(54, 334);
            this.checkBoxAttention.Name = "checkBoxAttention";
            this.checkBoxAttention.Size = new System.Drawing.Size(214, 17);
            this.checkBoxAttention.TabIndex = 9;
            this.checkBoxAttention.Text = "Внимателен, и не отмечу эту галочку";
            this.checkBoxAttention.UseVisualStyleBackColor = true;
            // 
            // checkBoxErrorHandling
            // 
            this.checkBoxErrorHandling.AutoSize = true;
            this.checkBoxErrorHandling.Enabled = false;
            this.checkBoxErrorHandling.Location = new System.Drawing.Point(54, 357);
            this.checkBoxErrorHandling.Name = "checkBoxErrorHandling";
            this.checkBoxErrorHandling.Size = new System.Drawing.Size(368, 17);
            this.checkBoxErrorHandling.TabIndex = 10;
            this.checkBoxErrorHandling.Text = "В случае ошибки я восстановлю базу из бэкапа и продолжу работу";
            this.checkBoxErrorHandling.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(208, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Код запуска";
            // 
            // textBoxCode
            // 
            this.textBoxCode.Location = new System.Drawing.Point(284, 59);
            this.textBoxCode.Name = "textBoxCode";
            this.textBoxCode.Size = new System.Drawing.Size(68, 20);
            this.textBoxCode.TabIndex = 13;
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(28, 475);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(431, 31);
            this.marqueeProgressBarControl1.TabIndex = 15;
            this.marqueeProgressBarControl1.Visible = false;
            // 
            // ClearDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 518);
            this.Controls.Add(this.marqueeProgressBarControl1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxCode);
            this.Controls.Add(this.checkBoxErrorHandling);
            this.Controls.Add(this.checkBoxAttention);
            this.Controls.Add(this.checkBoxDBFree);
            this.Controls.Add(this.checkBoxReserve);
            this.Controls.Add(this.checkBoxMain);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker1);
            this.Name = "ClearDataForm";
            this.Text = "Удаление истории программы";
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.CheckBox checkBoxMain;
        private System.Windows.Forms.CheckBox checkBoxReserve;
        private System.Windows.Forms.CheckBox checkBoxDBFree;
        private System.Windows.Forms.CheckBox checkBoxAttention;
        private System.Windows.Forms.CheckBox checkBoxErrorHandling;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCode;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
    }
}