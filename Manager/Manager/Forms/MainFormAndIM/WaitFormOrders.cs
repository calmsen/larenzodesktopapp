﻿using DevExpress.XtraWaitForm;

namespace LaRenzo.Forms.MainFormAndIM
{
    public partial class WaitFormOrders : WaitForm
    {
        public WaitFormOrders()
        {
            InitializeComponent();
            progressPanel1.AutoHeight = true;
        }

        #region Overrides

        public override void SetCaption(string caption)
        {
            base.SetCaption(caption);
            progressPanel1.Caption = caption;
        }
        public override void SetDescription(string description)
        {
            base.SetDescription(description);
            progressPanel1.Description = description;
        }

        #endregion

        public enum WaitFormCommand
        {
        }
    }
}