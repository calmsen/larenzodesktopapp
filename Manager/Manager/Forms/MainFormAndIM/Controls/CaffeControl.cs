﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.ControlsLib.TableControls;
using LaRenzo.DataRepository.Entities.Catalogs.Tables;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Tables;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Documents.CaffeOrders;
using LaRenzo.DataRepository.Repositories.Documents.Sessions;
using LaRenzo.Forms.Documents.CafeOrderForms;
using LaRenzo.FormsProvider;
using LaRenzo.Tools;

namespace LaRenzo.Forms.MainFormAndIM.Controls
{
    public partial class CaffeControl : UserControl
    {
        public CaffeControl()
        {
            InitializeComponent();
            this.toolStripButton6.Click += async(s,a) => await toolStripButton6_Click();
        }


        public async Task SetControls()
        {
            mainLPanel.Visible = true;
            await UpdateTableState();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<CafeOrderForm>.ActivateForm(filter => new CafeOrderForm(filter), new CafeOrderDocumentFilter { CurrentTable = new Table { Name = "Возьми с собой" } });
        }


        //__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнолка "Обновить" </summary>
        ////======================================
        private async Task toolStripButton6_Click()
        {
            await UpdateTableState();
        }


        //__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Обновить состояние столов </summary>
        ////==============================================
        private async Task UpdateTableState()
        {
            List<Table> tables = Content.TableRepository.GetItems().ToList();

            foreach (var table in tables)
            {
                var control = mainLPanel.Controls.OfType<TableControl>().FirstOrDefault(x => x.Content.ID == table.ID);

                if (control != null)
                {
                    control.TableState = table.IsOpen ? TableState.Open : TableState.Close;
                }
            }

            var session = await Content.SessionManager.GetOpened();

            // Если сессия открыта и пользователь админ
            if (session != null && Content.UserManager.UserLogged.RoleID == 1)
            {
                var cafeOrderRepository = Content.CafeOrderDocumentRepository;
                var doc = cafeOrderRepository.GetList(session.ID);


                if (doc != null)
                {
                    try
                    {
                        var orderCount = cafeOrderRepository.Count(session.ID, false);

                        var orderNal = cafeOrderRepository.GetList(session.ID, false, PaymentType.Money);
                        var orderSum = orderNal.Sum(x => x.DocumentSum);

                        var ordersUnNal = cafeOrderRepository.GetList(session.ID, false, PaymentType.NoMoney);

                        var ordersCard = cafeOrderRepository.GetList(session.ID, false, PaymentType.Card);

                        decimal orderOnlineSum = 0;

                        if (ordersUnNal.Any())
                        {
                            orderOnlineSum = ordersUnNal.Sum(x => x.DocumentSum);
                        }

                        decimal orderCardsSum = 0;

                        if (ordersCard.Any())
                        {
                            orderCardsSum = ordersCard.Sum(x => x.DocumentSum);
                        }

                        tblOrder.Text = $"Заказов: {orderCount}";
                        tblSum.Text = $"Наличные: {orderSum}  Оплата картой: {orderCardsSum}  Безнал на сумму: {orderOnlineSum}";
                    }
                    catch (Exception)
                    {


                    }
                }
            }
            BindTableData();
        }


        //___________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Запонить талицу столиков </summary>
        ////=============================================
        public void BindTableData()
        {
            mainLPanel.Controls.Clear();

            List<Table> tables = Content.TableRepository.GetItems().ToList();

            SetGridParams(tables.Count / 5);
            if (tables.Count >= 5)
            {
                for (int i = 0; i < tables.Count; i = i + 5)
                {
                    if (i + 5 <= tables.Count)
                    {
                        var table = CreateTableControl(
                            tables[i], 
                            tables[i + 1], 
                            tables[i + 2], 
                            tables[i + 3],
                            tables[i + 4]);
                        AddEventToTableButton(table.TableButton, tables[i]);
                        AddEventToTableButton(table.Place1Button, tables[i + 1]);
                        AddEventToTableButton(table.Place2Button, tables[i + 2]);
                        AddEventToTableButton(table.Place3Button, tables[i + 3]);
                        AddEventToTableButton(table.Place4Button, tables[i + 4]);
                        mainLPanel.Controls.Add(table, i.IsEven() ? 0 : 1, i / 10);
                    }
                }
            }
        }


        private void AddEventToTableButton(Button tableButton, Table table)
        {
            tableButton.Click += (sender, args) =>
            {
                var form = SingletonFormProvider<CafeOrderForm>.ActivateForm(filter => new CafeOrderForm(filter), new CafeOrderDocumentFilter { CurrentTable = table });

                form.Closed += async(o, eventArgs) => await UpdateTableState();
            };
        }


        private TableControl CreateTableControl(Table tableMain, Table tableP1, Table tableP2, Table tableP3, Table tableP4)
        {
            return new TableControl
            {
                Dock = DockStyle.Fill,
                OpenColor = Color.LightSalmon,
                CloseColor = Color.LightGreen,
                TableState = tableMain.IsOpen ? TableState.Open : TableState.Close,
                Place1State = tableP1.IsOpenP1 ? TableState.Open : TableState.Close,
                Place2State = tableP2.IsOpenP2 ? TableState.Open : TableState.Close,
                Place3State = tableP3.IsOpenP3 ? TableState.Open : TableState.Close,
                Place4State = tableP4.IsOpenP4 ? TableState.Open : TableState.Close,
                Content = tableMain,
                ShowTimer = false
            };
        }


        private void SetGridParams(int recordCount)
        {
            int rowCount = !recordCount.IsEven() ? recordCount / 2 + 1 : recordCount / 2;

            mainLPanel.RowCount = rowCount;

            if (rowCount > 0)
            {
                mainLPanel.RowStyles.Clear();

                for (int i = 0; i < rowCount; i++)
                {
                    mainLPanel.RowStyles.Add(new RowStyle { SizeType = SizeType.Percent, Height = (100f / rowCount) });
                }
            }
        }
    }
}
