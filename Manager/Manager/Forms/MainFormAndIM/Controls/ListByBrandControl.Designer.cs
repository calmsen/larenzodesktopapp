﻿namespace LaRenzo.Forms.MainFormAndIM.Controls
{
    partial class ListByBrandControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListByBrandControl));
            this.deliveryTStrip = new System.Windows.Forms.ToolStrip();
            this.buttonAddOrder = new System.Windows.Forms.ToolStripButton();
            this.buttonPreOrder = new System.Windows.Forms.ToolStripButton();
            this.toolSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonWebOrders = new System.Windows.Forms.ToolStripButton();
            this.toolPrintSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.PrintErrorDetector = new System.Windows.Forms.ToolStripButton();
            this.toolSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.searchButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnSms = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFisCheckButtons = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIdCol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.brandColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnStreet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTimeCreating = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTimeKitchen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnTimeClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCheck = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnHouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnFlat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDoDelivery = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnisWindow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDriver = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIsWeb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnBlackList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnIsOnlinePayment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPaymentMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSource = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.waitOrdersCountLabel = new System.Windows.Forms.Label();
            this.timerBeepForOrders = new System.Windows.Forms.Timer(this.components);
            this.redPhonePanel = new System.Windows.Forms.Panel();
            this.bluePhonePanel = new System.Windows.Forms.Panel();
            this.gridColumnTimeDelivered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.deliveryTStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // deliveryTStrip
            // 
            this.deliveryTStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.deliveryTStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonAddOrder,
            this.buttonPreOrder,
            this.toolSeparator1,
            this.buttonWebOrders,
            this.toolPrintSeparator,
            this.PrintErrorDetector,
            this.toolSeparator2,
            this.buttonUpdate,
            this.toolStripSeparator9,
            this.searchButton,
            this.toolStripButton7});
            this.deliveryTStrip.Location = new System.Drawing.Point(0, 0);
            this.deliveryTStrip.Name = "deliveryTStrip";
            this.deliveryTStrip.Size = new System.Drawing.Size(1091, 39);
            this.deliveryTStrip.TabIndex = 6;
            this.deliveryTStrip.Text = "toolStrip2";
            // 
            // buttonAddOrder
            // 
            this.buttonAddOrder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonAddOrder.Image = global::LaRenzo.Properties.Resources.plus;
            this.buttonAddOrder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonAddOrder.Name = "buttonAddOrder";
            this.buttonAddOrder.Size = new System.Drawing.Size(36, 36);
            this.buttonAddOrder.Text = "Новый заказ. \"N\" на клавиатуре";
            // 
            // buttonPreOrder
            // 
            this.buttonPreOrder.Image = global::LaRenzo.Properties.Resources.bookmarkgreen_1662;
            this.buttonPreOrder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonPreOrder.Name = "buttonPreOrder";
            this.buttonPreOrder.Size = new System.Drawing.Size(36, 36);
            this.buttonPreOrder.Tag = "3171";
            this.buttonPreOrder.Click += new System.EventHandler(this.buttonPreOrder_Click);
            // 
            // toolSeparator1
            // 
            this.toolSeparator1.Name = "toolSeparator1";
            this.toolSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // buttonWebOrders
            // 
            this.buttonWebOrders.Image = global::LaRenzo.Properties.Resources.badgealtexclamation_4268;
            this.buttonWebOrders.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonWebOrders.Name = "buttonWebOrders";
            this.buttonWebOrders.Size = new System.Drawing.Size(151, 36);
            this.buttonWebOrders.Text = "Онлайн заказов нет";
            this.buttonWebOrders.Click += new System.EventHandler(this.buttonWebOrders_Click);
            // 
            // toolPrintSeparator
            // 
            this.toolPrintSeparator.Name = "toolPrintSeparator";
            this.toolPrintSeparator.Size = new System.Drawing.Size(6, 39);
            // 
            // PrintErrorDetector
            // 
            this.PrintErrorDetector.Enabled = false;
            this.PrintErrorDetector.Image = global::LaRenzo.Properties.Resources.printer_error;
            this.PrintErrorDetector.ImageTransparentColor = System.Drawing.Color.White;
            this.PrintErrorDetector.Name = "PrintErrorDetector";
            this.PrintErrorDetector.Size = new System.Drawing.Size(220, 36);
            this.PrintErrorDetector.Text = "Внимание! Есть ошибки печати.";
            this.PrintErrorDetector.Visible = false;
            // 
            // toolSeparator2
            // 
            this.toolSeparator2.Name = "toolSeparator2";
            this.toolSeparator2.Size = new System.Drawing.Size(6, 39);
            this.toolSeparator2.Visible = false;
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonUpdate.Image = global::LaRenzo.Properties.Resources.refresh;
            this.buttonUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(36, 36);
            this.buttonUpdate.Text = "Обновить";
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 39);
            // 
            // searchButton
            // 
            this.searchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchButton.Image = global::LaRenzo.Properties.Resources.search;
            this.searchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(36, 36);
            this.searchButton.Text = "Найти";
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click_1);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton7.Text = "Звонки";
            // 
            // mainGridControl
            // 
            this.mainGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Location = new System.Drawing.Point(0, 42);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.mainGridControl.Size = new System.Drawing.Size(1091, 527);
            this.mainGridControl.TabIndex = 7;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.AppearancePrint.Row.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mainGridView.AppearancePrint.Row.Options.UseFont = true;
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnSms,
            this.gridColumnFisCheckButtons,
            this.columnIdCol,
            this.brandColumn,
            this.columnName,
            this.columnPhone,
            this.columnStreet,
            this.columnTotalCost,
            this.columnCode,
            this.columnTimeCreating,
            this.columnState,
            this.columnTimeKitchen,
            this.columnTimeClient,
            this.columnCheck,
            this.columnHouse,
            this.columnFlat,
            this.columnDoDelivery,
            this.columnisWindow,
            this.columnDriver,
            this.columnUser,
            this.columnIsWeb,
            this.columnBlackList,
            this.columnIsOnlinePayment,
            this.columnPaymentMethod,
            this.columnSource,
            this.gridColumnTimeDelivered});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "Code", this.columnCode, "{0}", "2"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TotalCost", this.columnTotalCost, "{0} руб", "1")});
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.mainGridView.OptionsView.BestFitMaxRowCount = 10;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.mainGridView_RowCellStyle);
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.GridView2RowStyle);
            this.mainGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.mainGridView_PopupMenuShowing);
            this.mainGridView.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(this.mainGridView_CustomSummaryCalculate);
            this.mainGridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.mainGridView_CustomUnboundColumnData);
            // 
            // gridColumnSms
            // 
            this.gridColumnSms.Caption = "Sms";
            this.gridColumnSms.FieldName = "SmsSended";
            this.gridColumnSms.Name = "gridColumnSms";
            this.gridColumnSms.OptionsColumn.AllowEdit = false;
            this.gridColumnSms.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.gridColumnSms.Visible = true;
            this.gridColumnSms.VisibleIndex = 0;
            this.gridColumnSms.Width = 42;
            // 
            // gridColumnFisCheckButtons
            // 
            this.gridColumnFisCheckButtons.Caption = "Фискальный чек";
            this.gridColumnFisCheckButtons.FieldName = "FiscalCheckPrinted";
            this.gridColumnFisCheckButtons.Name = "gridColumnFisCheckButtons";
            this.gridColumnFisCheckButtons.OptionsColumn.AllowEdit = false;
            this.gridColumnFisCheckButtons.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.gridColumnFisCheckButtons.Visible = true;
            this.gridColumnFisCheckButtons.VisibleIndex = 1;
            this.gridColumnFisCheckButtons.Width = 77;
            // 
            // columnIdCol
            // 
            this.columnIdCol.Caption = "Ид";
            this.columnIdCol.FieldName = "ID";
            this.columnIdCol.Name = "columnIdCol";
            this.columnIdCol.OptionsColumn.AllowEdit = false;
            this.columnIdCol.OptionsColumn.AllowFocus = false;
            this.columnIdCol.OptionsColumn.ReadOnly = true;
            // 
            // brandColumn
            // 
            this.brandColumn.Caption = "Бренд";
            this.brandColumn.FieldName = "Brand.Name";
            this.brandColumn.Name = "brandColumn";
            this.brandColumn.OptionsColumn.AllowEdit = false;
            this.brandColumn.OptionsColumn.AllowFocus = false;
            this.brandColumn.OptionsColumn.ReadOnly = true;
            this.brandColumn.Visible = true;
            this.brandColumn.VisibleIndex = 2;
            this.brandColumn.Width = 43;
            // 
            // columnName
            // 
            this.columnName.Caption = "Имя";
            this.columnName.FieldName = "Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 4;
            this.columnName.Width = 39;
            // 
            // columnPhone
            // 
            this.columnPhone.Caption = "Телефон";
            this.columnPhone.FieldName = "Phone";
            this.columnPhone.Name = "columnPhone";
            this.columnPhone.OptionsColumn.AllowEdit = false;
            this.columnPhone.OptionsColumn.AllowFocus = false;
            this.columnPhone.OptionsColumn.ReadOnly = true;
            this.columnPhone.Visible = true;
            this.columnPhone.VisibleIndex = 5;
            this.columnPhone.Width = 39;
            // 
            // columnStreet
            // 
            this.columnStreet.Caption = "Улица";
            this.columnStreet.FieldName = "Street";
            this.columnStreet.Name = "columnStreet";
            this.columnStreet.OptionsColumn.AllowEdit = false;
            this.columnStreet.OptionsColumn.AllowFocus = false;
            this.columnStreet.OptionsColumn.ReadOnly = true;
            this.columnStreet.Visible = true;
            this.columnStreet.VisibleIndex = 6;
            this.columnStreet.Width = 39;
            // 
            // columnTotalCost
            // 
            this.columnTotalCost.Caption = "Сумма заказа";
            this.columnTotalCost.FieldName = "TotalCost";
            this.columnTotalCost.Name = "columnTotalCost";
            this.columnTotalCost.OptionsColumn.AllowEdit = false;
            this.columnTotalCost.OptionsColumn.AllowFocus = false;
            this.columnTotalCost.OptionsColumn.ReadOnly = true;
            this.columnTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TotalCost", "{0} руб.", "3")});
            this.columnTotalCost.Tag = "";
            this.columnTotalCost.Visible = true;
            this.columnTotalCost.VisibleIndex = 9;
            this.columnTotalCost.Width = 39;
            // 
            // columnCode
            // 
            this.columnCode.Caption = "Код заказа";
            this.columnCode.FieldName = "Code";
            this.columnCode.Name = "columnCode";
            this.columnCode.OptionsColumn.AllowEdit = false;
            this.columnCode.OptionsColumn.AllowFocus = false;
            this.columnCode.OptionsColumn.ReadOnly = true;
            this.columnCode.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Code", "{0} заказов")});
            this.columnCode.Visible = true;
            this.columnCode.VisibleIndex = 3;
            this.columnCode.Width = 42;
            // 
            // columnTimeCreating
            // 
            this.columnTimeCreating.Caption = "Время создания";
            this.columnTimeCreating.DisplayFormat.FormatString = "HH:mm";
            this.columnTimeCreating.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnTimeCreating.FieldName = "Created";
            this.columnTimeCreating.Name = "columnTimeCreating";
            this.columnTimeCreating.OptionsColumn.AllowEdit = false;
            this.columnTimeCreating.OptionsColumn.AllowFocus = false;
            this.columnTimeCreating.OptionsColumn.ReadOnly = true;
            this.columnTimeCreating.Visible = true;
            this.columnTimeCreating.VisibleIndex = 11;
            this.columnTimeCreating.Width = 39;
            // 
            // columnState
            // 
            this.columnState.Caption = "Состояние заказа";
            this.columnState.FieldName = "State.Name";
            this.columnState.Name = "columnState";
            this.columnState.OptionsColumn.AllowEdit = false;
            this.columnState.OptionsColumn.AllowFocus = false;
            this.columnState.OptionsColumn.ReadOnly = true;
            this.columnState.Visible = true;
            this.columnState.VisibleIndex = 10;
            this.columnState.Width = 39;
            // 
            // columnTimeKitchen
            // 
            this.columnTimeKitchen.Caption = "Время кухня";
            this.columnTimeKitchen.DisplayFormat.FormatString = "HH:mm";
            this.columnTimeKitchen.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnTimeKitchen.FieldName = "TimeKitchen";
            this.columnTimeKitchen.Name = "columnTimeKitchen";
            this.columnTimeKitchen.OptionsColumn.AllowEdit = false;
            this.columnTimeKitchen.OptionsColumn.AllowFocus = false;
            this.columnTimeKitchen.OptionsColumn.ReadOnly = true;
            this.columnTimeKitchen.Visible = true;
            this.columnTimeKitchen.VisibleIndex = 12;
            this.columnTimeKitchen.Width = 39;
            // 
            // columnTimeClient
            // 
            this.columnTimeClient.Caption = "Время клиент";
            this.columnTimeClient.DisplayFormat.FormatString = "HH:mm";
            this.columnTimeClient.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnTimeClient.FieldName = "TimeClient";
            this.columnTimeClient.Name = "columnTimeClient";
            this.columnTimeClient.OptionsColumn.AllowEdit = false;
            this.columnTimeClient.OptionsColumn.AllowFocus = false;
            this.columnTimeClient.OptionsColumn.ReadOnly = true;
            this.columnTimeClient.Visible = true;
            this.columnTimeClient.VisibleIndex = 13;
            this.columnTimeClient.Width = 39;
            // 
            // columnCheck
            // 
            this.columnCheck.Caption = "Чек распечатан";
            this.columnCheck.FieldName = "DoPrintedByOperator";
            this.columnCheck.Name = "columnCheck";
            this.columnCheck.OptionsColumn.AllowEdit = false;
            this.columnCheck.OptionsColumn.AllowFocus = false;
            this.columnCheck.OptionsColumn.ReadOnly = true;
            this.columnCheck.Visible = true;
            this.columnCheck.VisibleIndex = 14;
            this.columnCheck.Width = 39;
            // 
            // columnHouse
            // 
            this.columnHouse.Caption = "Дом";
            this.columnHouse.FieldName = "House";
            this.columnHouse.Name = "columnHouse";
            this.columnHouse.OptionsColumn.AllowEdit = false;
            this.columnHouse.OptionsColumn.AllowFocus = false;
            this.columnHouse.OptionsColumn.ReadOnly = true;
            this.columnHouse.Visible = true;
            this.columnHouse.VisibleIndex = 7;
            this.columnHouse.Width = 39;
            // 
            // columnFlat
            // 
            this.columnFlat.Caption = "Квартира";
            this.columnFlat.FieldName = "Appartament";
            this.columnFlat.Name = "columnFlat";
            this.columnFlat.OptionsColumn.AllowEdit = false;
            this.columnFlat.OptionsColumn.AllowFocus = false;
            this.columnFlat.OptionsColumn.ReadOnly = true;
            this.columnFlat.Visible = true;
            this.columnFlat.VisibleIndex = 8;
            this.columnFlat.Width = 39;
            // 
            // columnDoDelivery
            // 
            this.columnDoDelivery.Caption = "Доставка";
            this.columnDoDelivery.FieldName = "DoCalculateDelivery";
            this.columnDoDelivery.Name = "columnDoDelivery";
            this.columnDoDelivery.OptionsColumn.AllowEdit = false;
            this.columnDoDelivery.OptionsColumn.AllowFocus = false;
            this.columnDoDelivery.OptionsColumn.ReadOnly = true;
            this.columnDoDelivery.Visible = true;
            this.columnDoDelivery.VisibleIndex = 15;
            this.columnDoDelivery.Width = 39;
            // 
            // columnisWindow
            // 
            this.columnisWindow.Caption = "Окно";
            this.columnisWindow.FieldName = "IsWindow";
            this.columnisWindow.Name = "columnisWindow";
            this.columnisWindow.OptionsColumn.AllowEdit = false;
            this.columnisWindow.OptionsColumn.AllowFocus = false;
            this.columnisWindow.OptionsColumn.ReadOnly = true;
            this.columnisWindow.Visible = true;
            this.columnisWindow.VisibleIndex = 16;
            this.columnisWindow.Width = 39;
            // 
            // columnDriver
            // 
            this.columnDriver.Caption = "Водитель";
            this.columnDriver.FieldName = "Driver.Name";
            this.columnDriver.Name = "columnDriver";
            this.columnDriver.OptionsColumn.AllowEdit = false;
            this.columnDriver.OptionsColumn.AllowFocus = false;
            this.columnDriver.OptionsColumn.ReadOnly = true;
            this.columnDriver.Visible = true;
            this.columnDriver.VisibleIndex = 17;
            this.columnDriver.Width = 39;
            // 
            // columnUser
            // 
            this.columnUser.Caption = "Пользователь";
            this.columnUser.FieldName = "User";
            this.columnUser.Name = "columnUser";
            this.columnUser.OptionsColumn.AllowEdit = false;
            this.columnUser.OptionsColumn.ReadOnly = true;
            this.columnUser.Visible = true;
            this.columnUser.VisibleIndex = 18;
            this.columnUser.Width = 39;
            // 
            // columnIsWeb
            // 
            this.columnIsWeb.Caption = "Web";
            this.columnIsWeb.FieldName = "IsWebRequest";
            this.columnIsWeb.Name = "columnIsWeb";
            this.columnIsWeb.OptionsColumn.AllowEdit = false;
            this.columnIsWeb.OptionsColumn.AllowFocus = false;
            this.columnIsWeb.OptionsColumn.ReadOnly = true;
            this.columnIsWeb.Visible = true;
            this.columnIsWeb.VisibleIndex = 19;
            this.columnIsWeb.Width = 39;
            // 
            // columnBlackList
            // 
            this.columnBlackList.Caption = "В черном списке?";
            this.columnBlackList.FieldName = "columnBlackList";
            this.columnBlackList.Name = "columnBlackList";
            this.columnBlackList.OptionsColumn.AllowEdit = false;
            this.columnBlackList.OptionsColumn.ReadOnly = true;
            this.columnBlackList.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.columnBlackList.Visible = true;
            this.columnBlackList.VisibleIndex = 20;
            this.columnBlackList.Width = 39;
            // 
            // columnIsOnlinePayment
            // 
            this.columnIsOnlinePayment.Caption = "Безнал";
            this.columnIsOnlinePayment.FieldName = "IsOnlinePayment";
            this.columnIsOnlinePayment.Name = "columnIsOnlinePayment";
            this.columnIsOnlinePayment.OptionsColumn.AllowEdit = false;
            this.columnIsOnlinePayment.OptionsColumn.ReadOnly = true;
            this.columnIsOnlinePayment.Visible = true;
            this.columnIsOnlinePayment.VisibleIndex = 21;
            this.columnIsOnlinePayment.Width = 39;
            // 
            // columnPaymentMethod
            // 
            this.columnPaymentMethod.Caption = "Метод оплаты";
            this.columnPaymentMethod.FieldName = "PaymentMethod";
            this.columnPaymentMethod.Name = "columnPaymentMethod";
            this.columnPaymentMethod.OptionsColumn.AllowEdit = false;
            this.columnPaymentMethod.OptionsColumn.ReadOnly = true;
            this.columnPaymentMethod.Visible = true;
            this.columnPaymentMethod.VisibleIndex = 22;
            this.columnPaymentMethod.Width = 53;
            // 
            // columnSource
            // 
            this.columnSource.Caption = "Источник";
            this.columnSource.FieldName = "Source";
            this.columnSource.Name = "columnSource";
            this.columnSource.OptionsColumn.AllowEdit = false;
            this.columnSource.Visible = true;
            this.columnSource.VisibleIndex = 23;
            this.columnSource.Width = 114;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 572);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1091, 22);
            this.statusStrip.TabIndex = 8;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatus
            // 
            this.toolStripStatus.Name = "toolStripStatus";
            this.toolStripStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // waitOrdersCountLabel
            // 
            this.waitOrdersCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.waitOrdersCountLabel.AutoSize = true;
            this.waitOrdersCountLabel.Location = new System.Drawing.Point(3, 575);
            this.waitOrdersCountLabel.Name = "waitOrdersCountLabel";
            this.waitOrdersCountLabel.Size = new System.Drawing.Size(107, 13);
            this.waitOrdersCountLabel.TabIndex = 9;
            this.waitOrdersCountLabel.Text = "Ожидают доставки:";
            // 
            // timerBeepForOrders
            // 
            this.timerBeepForOrders.Interval = 3000;
            // 
            // redPhonePanel
            // 
            this.redPhonePanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("redPhonePanel.BackgroundImage")));
            this.redPhonePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.redPhonePanel.Location = new System.Drawing.Point(461, 208);
            this.redPhonePanel.Name = "redPhonePanel";
            this.redPhonePanel.Size = new System.Drawing.Size(98, 85);
            this.redPhonePanel.TabIndex = 10;
            this.redPhonePanel.Visible = false;
            // 
            // bluePhonePanel
            // 
            this.bluePhonePanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bluePhonePanel.BackgroundImage")));
            this.bluePhonePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bluePhonePanel.Location = new System.Drawing.Point(597, 209);
            this.bluePhonePanel.Name = "bluePhonePanel";
            this.bluePhonePanel.Size = new System.Drawing.Size(89, 83);
            this.bluePhonePanel.TabIndex = 11;
            this.bluePhonePanel.Visible = false;
            // 
            // gridColumnTimeDelivered
            // 
            this.gridColumnTimeDelivered.Caption = "Время доставки";
            this.gridColumnTimeDelivered.DisplayFormat.FormatString = "HH:mm";
            this.gridColumnTimeDelivered.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumnTimeDelivered.FieldName = "ChangeToDelivered";
            this.gridColumnTimeDelivered.Name = "gridColumnTimeDelivered";
            this.gridColumnTimeDelivered.OptionsColumn.AllowEdit = false;
            this.gridColumnTimeDelivered.OptionsColumn.AllowFocus = false;
            this.gridColumnTimeDelivered.OptionsColumn.ReadOnly = true;
            this.gridColumnTimeDelivered.Visible = true;
            this.gridColumnTimeDelivered.VisibleIndex = 24;
            // 
            // ListByBrandControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bluePhonePanel);
            this.Controls.Add(this.redPhonePanel);
            this.Controls.Add(this.waitOrdersCountLabel);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.deliveryTStrip);
            this.Name = "ListByBrandControl";
            this.Size = new System.Drawing.Size(1091, 594);
            this.deliveryTStrip.ResumeLayout(false);
            this.deliveryTStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip deliveryTStrip;
        public System.Windows.Forms.ToolStripButton buttonAddOrder;
        private System.Windows.Forms.ToolStripButton buttonPreOrder;
        private System.Windows.Forms.ToolStripSeparator toolSeparator1;
        private System.Windows.Forms.ToolStripButton buttonWebOrders;
        private System.Windows.Forms.ToolStripSeparator toolPrintSeparator;
        public System.Windows.Forms.ToolStripButton PrintErrorDetector;
        private System.Windows.Forms.ToolStripSeparator toolSeparator2;
        public System.Windows.Forms.ToolStripButton buttonUpdate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton searchButton;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        public DevExpress.XtraGrid.Columns.GridColumn columnIdCol;
        private DevExpress.XtraGrid.Columns.GridColumn brandColumn;
        public DevExpress.XtraGrid.Columns.GridColumn columnName;
        public DevExpress.XtraGrid.Columns.GridColumn columnPhone;
        public DevExpress.XtraGrid.Columns.GridColumn columnStreet;
        public DevExpress.XtraGrid.Columns.GridColumn columnTotalCost;
        public DevExpress.XtraGrid.Columns.GridColumn columnCode;
        public DevExpress.XtraGrid.Columns.GridColumn columnTimeCreating;
        public DevExpress.XtraGrid.Columns.GridColumn columnState;
        public DevExpress.XtraGrid.Columns.GridColumn columnTimeKitchen;
        public DevExpress.XtraGrid.Columns.GridColumn columnTimeClient;
        public DevExpress.XtraGrid.Columns.GridColumn columnCheck;
        private DevExpress.XtraGrid.Columns.GridColumn columnHouse;
        private DevExpress.XtraGrid.Columns.GridColumn columnFlat;
        private DevExpress.XtraGrid.Columns.GridColumn columnDoDelivery;
        private DevExpress.XtraGrid.Columns.GridColumn columnisWindow;
        private DevExpress.XtraGrid.Columns.GridColumn columnDriver;
        private DevExpress.XtraGrid.Columns.GridColumn columnUser;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsWeb;
        private DevExpress.XtraGrid.Columns.GridColumn columnBlackList;
        private DevExpress.XtraGrid.Columns.GridColumn columnIsOnlinePayment;
        private DevExpress.XtraGrid.Columns.GridColumn columnPaymentMethod;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatus;
        private System.Windows.Forms.Label waitOrdersCountLabel;
        private System.Windows.Forms.Timer timerBeepForOrders;
        private System.Windows.Forms.Panel redPhonePanel;
        private System.Windows.Forms.Panel bluePhonePanel;
        private DevExpress.XtraGrid.Columns.GridColumn columnSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFisCheckButtons;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSms;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTimeDelivered;
    }
}
