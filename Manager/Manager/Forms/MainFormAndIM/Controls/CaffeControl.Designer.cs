﻿namespace LaRenzo.Forms.MainFormAndIM.Controls
{
	partial class CaffeControl
	{
		/// <summary> 
		/// Требуется переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором компонентов

		/// <summary> 
		/// Обязательный метод для поддержки конструктора - не изменяйте 
		/// содержимое данного метода при помощи редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.WithSelfTButton = new System.Windows.Forms.ToolStrip();
			this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.tblOrder = new System.Windows.Forms.ToolStripLabel();
			this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
			this.tblSum = new System.Windows.Forms.ToolStripLabel();
			this.mainLPanel = new System.Windows.Forms.TableLayoutPanel();
			this.WithSelfTButton.SuspendLayout();
			this.SuspendLayout();
			// 
			// WithSelfTButton
			// 
			this.WithSelfTButton.ImageScalingSize = new System.Drawing.Size(32, 32);
			this.WithSelfTButton.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripSeparator6,
            this.toolStripButton5,
            this.toolStripSeparator7,
            this.toolStripButton6,
            this.toolStripSeparator2,
            this.tblOrder,
            this.toolStripSeparator8,
            this.tblSum});
			this.WithSelfTButton.Location = new System.Drawing.Point(0, 0);
			this.WithSelfTButton.Name = "WithSelfTButton";
			this.WithSelfTButton.Size = new System.Drawing.Size(1012, 39);
			this.WithSelfTButton.TabIndex = 7;
			this.WithSelfTButton.Text = "toolStrip2";
			// 
			// toolStripButton1
			// 
			this.toolStripButton1.Image = global::LaRenzo.Properties.Resources.plus;
			this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton1.Name = "toolStripButton1";
			this.toolStripButton1.Size = new System.Drawing.Size(133, 36);
			this.toolStripButton1.Text = " Возьми с собой";
			this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
			// 
			// toolStripSeparator6
			// 
			this.toolStripSeparator6.Name = "toolStripSeparator6";
			this.toolStripSeparator6.Size = new System.Drawing.Size(6, 39);
			// 
			// toolStripButton5
			// 
			this.toolStripButton5.Enabled = false;
			this.toolStripButton5.Image = global::LaRenzo.Properties.Resources.printer_error;
			this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.White;
			this.toolStripButton5.Name = "toolStripButton5";
			this.toolStripButton5.Size = new System.Drawing.Size(220, 36);
			this.toolStripButton5.Text = "Внимание! Есть ошибки печати.";
			this.toolStripButton5.Visible = false;
			// 
			// toolStripSeparator7
			// 
			this.toolStripSeparator7.Name = "toolStripSeparator7";
			this.toolStripSeparator7.Size = new System.Drawing.Size(6, 39);
			this.toolStripSeparator7.Visible = false;
			// 
			// toolStripButton6
			// 
			this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton6.Image = global::LaRenzo.Properties.Resources.refresh;
			this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton6.Name = "toolStripButton6";
			this.toolStripButton6.Size = new System.Drawing.Size(36, 36);
			this.toolStripButton6.Text = "Обновить";
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
			// 
			// tblOrder
			// 
			this.tblOrder.Name = "tblOrder";
			this.tblOrder.Size = new System.Drawing.Size(53, 36);
			this.tblOrder.Text = "Заказов:";
			// 
			// toolStripSeparator8
			// 
			this.toolStripSeparator8.Name = "toolStripSeparator8";
			this.toolStripSeparator8.Size = new System.Drawing.Size(6, 39);
			// 
			// tblSum
			// 
			this.tblSum.Name = "tblSum";
			this.tblSum.Size = new System.Drawing.Size(48, 36);
			this.tblSum.Text = "Сумма:";
			// 
			// mainLPanel
			// 
			this.mainLPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
			this.mainLPanel.ColumnCount = 2;
			this.mainLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.mainLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.mainLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainLPanel.Location = new System.Drawing.Point(0, 39);
			this.mainLPanel.Name = "mainLPanel";
			this.mainLPanel.RowCount = 1;
			this.mainLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLPanel.Size = new System.Drawing.Size(1012, 595);
			this.mainLPanel.TabIndex = 8;
			// 
			// CaffeControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.mainLPanel);
			this.Controls.Add(this.WithSelfTButton);
			this.Name = "CaffeControl";
			this.Size = new System.Drawing.Size(1012, 634);
			this.WithSelfTButton.ResumeLayout(false);
			this.WithSelfTButton.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip WithSelfTButton;
		public System.Windows.Forms.ToolStripButton toolStripButton1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
		public System.Windows.Forms.ToolStripButton toolStripButton5;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
		public System.Windows.Forms.ToolStripButton toolStripButton6;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripLabel tblOrder;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
		private System.Windows.Forms.ToolStripLabel tblSum;
		private System.Windows.Forms.TableLayoutPanel mainLPanel;
	}
}
