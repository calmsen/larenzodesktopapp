﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.Forms.AmountFactorForms;
using LaRenzo.Forms.BlackList;
using LaRenzo.Forms.Catalog.Classifier.AddressForms;
using LaRenzo.Forms.Catalog.Classifier.MeasuresForms;
using LaRenzo.Forms.Catalog.Dishes;
using LaRenzo.Forms.Catalog.PartnersForms;
using LaRenzo.Forms.Catalog.ProductForms;
using LaRenzo.Forms.Catalog.SetsForms;
using LaRenzo.Forms.Catalog.Tables;
using LaRenzo.Forms.Catalog.WarehouseForms;
using LaRenzo.Forms.DataForms;
using LaRenzo.Forms.DeliverySchedule;
using LaRenzo.Forms.DiscountCardForms;
using LaRenzo.Forms.Documents.CafeOrderForms;
using LaRenzo.Forms.Documents.DeliveryOrderForms;
using LaRenzo.Forms.Documents.InventoryForms;
using LaRenzo.Forms.Documents.MovingForms;
using LaRenzo.Forms.Documents.PostingForms;
using LaRenzo.Forms.Documents.ReturnForms;
using LaRenzo.Forms.Documents.SessionForms;
using LaRenzo.Forms.Documents.SupplyForms;
using LaRenzo.Forms.Documents.WriteOff;
using LaRenzo.Forms.Documents.WriteOffDishes;
using LaRenzo.Forms.Finance;
using LaRenzo.Forms.HelpForms;
using LaRenzo.Forms.HistoryForms;
using LaRenzo.Forms.Parameters.Brand;
using LaRenzo.Forms.Parameters.OptionForms;
using LaRenzo.Forms.Parameters.PrinterForms;
using LaRenzo.Forms.Parameters.TransferForms;
using LaRenzo.Forms.ReportForms;
using LaRenzo.Forms.StaffForms;
using LaRenzo.Forms.TimeForms;
using LaRenzo.FormsProvider;
using LaRenzo.Properties;
using System.Linq;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Repositories.Call;
using LaRenzo.Forms.Call;
using LaRenzo.Forms.Parameters.PointOfSale;
using LaRenzo.Forms.Stats;
using LaRenzo.Tools;
using DataRepository.Entities;

namespace LaRenzo.Forms.MainFormAndIM
{
    public partial class MainForm : Form
    {
        private bool IsAuth { get; set; }
        /// <summary> Открыта ли сессия </summary>
        private bool IsSessionOpened { get; set; }

        public static MainForm CurrentMainForm;

        public MainForm()
        {
            InitializeComponent();
            string connectString = System.Configuration.ConfigurationManager.ConnectionStrings["BasicCS"].ToString();
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectString);

            string ipAddress = builder.DataSource;
            Text =
                $"Zen Pizza Сборка от {Settings.Default.VersionDate}" +
                $", Версия {Settings.Default.CurrentVersion}, Подключение к базе {ipAddress}";
            this.ActivateHelpPage("MainForm", "Главное окно");

            DeliveryOrderForm.OrderProcessedStatic += ProcessOrder;
            
            CurrentMainForm = this;

            mainTb.TabPages.Clear();

            LoginForm.UserLogged  += async user => await ProcessUserLogged(user);
            KeyDown += async (s, e) => await MainForm_KeyDown(e);
            buttonSessionOptions.Click += async (s, e) => await SessionOptionsMenuItem_Click();

            TimeTableMenuItem.Click += (s, e) => TimeTableMenuItem_Click();
        }

        public sealed override string Text
        {
            get => base.Text;
            set => base.Text = value;
        }


        /// <summary>
        /// Инициализация звонков
        /// </summary>
        private void InitCall()
        {
            try
            {
                FileInfo fi = new FileInfo("OperNumber.txt");

                if (fi.Exists)
                {
                    StreamReader sr = fi.OpenText();
                    string operNumber = sr.ReadLine();
                    string connectionString = sr.ReadLine();
                    sr.Close();

                    if (!string.IsNullOrEmpty(operNumber))
                    {
                        CallManager.Instance.Connection(CurrentMainForm, operNumber, connectionString);
                        CallManager.Instance.NowCallEvent += CallManager_NowCall;
                    }
                    else
                    {
                        //MessageBox.Show(@"Установите номер оператора");
                        //Application.Exit();
                    }
                }
                else
                {
                    //MessageBox.Show(@"Установите номер оператора");
                    //Application.Exit();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        //__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Звершение звонков </summary>
        ////======================================
        private void CloseCall()
        {
            try
            {
                if (CallManager.Instance.IsConected)
                {
                    CallManager.Instance.NowCallEvent -= CallManager_NowCall;
                    CallManager.Instance.Disconnection();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________
        /// <summary> Идёт звонок </summary>
        ////===============================
        void CallManager_NowCall(object sender, CallEvent e)
        {

            var interCall = new InterCall(1);

            if (!CallManager.Instance.IsOpenChoose)
            {
                interCall.SetCall(e.CurentCall);
                interCall.ShowDialog();
            }
            else
            {
                callNotifyIcon.BalloonTipText = @"Входящий: " + e.CurentCall.CallData.Name + " " + e.CurentCall.CallData.PhoneNumber;
                callNotifyIcon.ShowBalloonTip(4000);
                callNotifyIcon.Click += callNotifyIcon_Click;
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________
        void callNotifyIcon_Click(object sender, EventArgs e)
        {
            var interCall = new InterCall(1);

            interCall.SetCall(CallManager.Instance.NowCall);
            interCall.ShowDialog();
        }
        
        private async  void MainForm_Load(object sender, EventArgs e)
        {
            ClearLoginInfo();
            ClearSessionInfo();


            UpdateInterface();
#if DEBUG
            var user = Content.UserManager.Login("Руслан", "2079000");
            await ProcessUserLogged(user);
#endif
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void UserLoginMenuItem_Click(object sender, EventArgs e)
        {
            if (!Content.UserManager.IsLogged())
            {
                SingletonFormProvider<LoginForm>.ActivateForm(() => new LoginForm());
            }
            else
            {
                LogOut();
            }
        }

        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Кнопка Открыть/Закрыть сессию </summary>
        ////==================================================
        private async void SessionMenuItemClick(object sender, EventArgs e)
        {
            // Если сессия открыта
            var isSessionOpened = await Content.SessionManager.IsSessionOpen();
            if (isSessionOpened)
            {
                // Если согласны закрыть сессию 
                if (MessageBox.Show(Resources.CloseSessionQuestion, Resources.CloseSession, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    // Закрыть сессию
                    CloseSession();
                }
                else
                {
                    //
                    await ChangeIF_OpenSession();
                }
            }
            else
            {
                // Показать диалог открытия сессии
                await ShowOpenSessionForm();
            }
        }


        //___________________________________________________________________________________________________________________________________
        /// <summary> Показать диалог открытия сессии </summary>
        ////====================================================
        private async Task ShowOpenSessionForm()
        {
            var form = new SessionOptionsForm();
            await form.Initialization();
            form.OpenSessionSuccess += async delegate { await ChangeIF_OpenSession(); };
            SingletonFormProvider<SessionOptionsForm>.ActivateForm(() => form);
        }

        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(Resources.CloseProgramQuestion, Resources.CloseProgram, MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                CloseCall();
            }

        }


        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void AutoUpdater_Tick(object sender, EventArgs e)
        {
            RefreshGridData();
        }


        //internal int afterKitchenCount;


        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void SessionsMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<SessionsForm>.ActivateForm(() => new SessionsForm());
        }

        private async Task MainForm_KeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!Content.UserManager.IsLogged())
                {
                    SingletonFormProvider<LoginForm>.ActivateForm(() => new LoginForm());
                    return;
                }
                if (!IsSessionOpened)
                {
                    await ShowOpenSessionForm();
                }
            }
            if (e.KeyCode == Keys.N)
            {
                await NewOrder();
            }
        }

        private void CategoriesMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<DishCategoryListForm>.ActivateForm(() => new DishCategoryListForm());
        }

        private void DishMenuItem_Click(object sender, EventArgs e)
        {

            SingletonFormProvider<DishListForm>.ActivateForm(() => new DishListForm());
        }

        private void SaleReportMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<SaleReportForm>.ActivateForm(() => new SaleReportForm());
        }

        private void DayReportMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<DayOrdersForm>.ActivateForm(() => new DayOrdersForm());
        }




        private void ProductMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<ProductListForm>.ActivateForm(() => new ProductListForm());
        }

        private void ProductCategoriesMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<ProductCategoriesListForm>.ActivateForm(() => new ProductCategoriesListForm());
        }


        private void ParametrOptionsMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<OptionsForm>.ActivateForm(() => new OptionsForm());
        }


        private void TimeTableMenuItem_Click()
        {
            var form = new TimeSheetForm();
            SingletonFormProvider<TimeSheetForm>.ActivateForm(() => form);
        }

        private void UsersMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<UsersForm>.ActivateForm(() => new UsersForm());
        }

        private void DriversMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<DriversForm>.ActivateForm(() => new DriversForm());
        }

        private void PrintersMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<PrinterForm>.ActivateForm(() => new PrinterForm());
        }

        private void SectionsMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<SectionsForm>.ActivateForm(() => new SectionsForm());
        }

        private void MenuItemFactors_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<AmountFactorCollectionsForm>.ActivateForm(() => new AmountFactorCollectionsForm());
        }

        private async Task SessionOptionsMenuItem_Click()
        {
            var form = new SessionOptionsForm(false);
            await form.Initialization();
            SingletonFormProvider<SessionOptionsForm>.ActivateForm(() => form);
        }

        private void DiscountMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<DiscountCardsForm>.ActivateForm(() => new DiscountCardsForm());
        }

        private void DeliveryScheduleMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<DeliveryScheduleForm>.ActivateForm(() => new DeliveryScheduleForm());
        }

        private void AddressMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<AddressListForm>.ActivateForm(() => new AddressListForm());
        }

        private void SupplyDocsMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<SupplyListForm>.ActivateForm(() => new SupplyListForm());
        }

        private void PartnerMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<PartnerListForm>.ActivateForm(() => new PartnerListForm());
        }
        private void MeasureMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<MeasureListForm>.ActivateForm(() => new MeasureListForm());
        }
        private void WarehouseMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<WarehouseListForm>.ActivateForm(() => new WarehouseListForm());
        }
        private void OtherSetCoeffMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<SetListForm>.ActivateForm(() => new SetListForm());
        }

        private void OtherSetRuleMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<SetParamsListForm>.ActivateForm(() => new SetParamsListForm());
        }
        private void ReturnDocMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<ReturnListForm>.ActivateForm(() => new ReturnListForm());
        }

        private void MovingDocMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<MovingListForm>.ActivateForm(() => new MovingListForm());
        }
        private void WriteOffDocMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<WriteoffListForm>.ActivateForm(() => new WriteoffListForm());
        }
        private void PostingDocMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<PostingListForm>.ActivateForm(() => new PostingListForm());
        }

        private void InventoryMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<InventoryListForm>.ActivateForm(() => new InventoryListForm());
        }

        private void СashСhangeMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<SessionListForm>.ActivateForm(() => new SessionListForm());
        }

        private void RemainsOfGoodReport_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<RemainsOfGoodReport>.ActivateForm(() => new RemainsOfGoodReport());
        }
        private void SettlsWithPartnerReport_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<SettlsWithPartnerReport>.ActivateForm(() => new SettlsWithPartnerReport());
        }

        private void WriteoffDishMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<WriteoffDishListForm>.ActivateForm(() => new WriteoffDishListForm());
        }
        private void DataTransferChiefMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<TransferChiefExpert>.ActivateForm(() => new TransferChiefExpert());
        }

        private void TransferOfGoodMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<TransferOfGoodsReport>.ActivateForm(() => new TransferOfGoodsReport());
        }

        private void TablesMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<TableListForm>.ActivateForm(() => new TableListForm());
        }

        private void CafeMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<CafeOrderListForm>.ActivateForm(() => new CafeOrderListForm());
        }

        private void AllOrdersCafeMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<CafeSessionsForm>.ActivateForm(() => new CafeSessionsForm());
        }

        private void DeliveryMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<AllOrdersForm>.ActivateForm(() => new AllOrdersForm());
        }

        private void ProductOfPartnersMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<ProductOfPartners>.ActivateForm(() => new ProductOfPartners());
        }

        private void blackListMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<BlackListForm>.ActivateForm(() => new BlackListForm());
        }

        private void buttonPerformanceCounter_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<ExecutionTimeLoggerForm>.ActivateForm(() => new ExecutionTimeLoggerForm());
        }

        private void комбинацииБлюдToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<DishCombinationReport>.ActivateForm(() => new DishCombinationReport());
        }

        private void праваДоступаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<UserAccessForm>.ActivateForm(() => new UserAccessForm());
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<PrintingMonitor>.ActivateForm(() => new PrintingMonitor());
        }

        private void TransactionMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<PaymentListForm>.ActivateForm(() => new PaymentListForm());
        }

        private void WalletMenuOption_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<WalletForm>.ActivateForm(() => new WalletForm());
        }

        private void аналитикаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<PaymentDashboard>.ActivateForm(() => new PaymentDashboard());
        }

        private void TransactioCategoryMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<PaymentCategoryForm>.ActivateForm(() => new PaymentCategoryForm());
        }

        private void TestFinanceDataMenuOtion_Click(object sender, EventArgs e)
        {
            var log = new StringBuilder();

            //Кошельки

            var walletRepo = Content.WalletRepository;

            log.AppendLine("Wallet repo created");
            File.WriteAllText("log.txt", log.ToString());

            //Данные методы создают кошелек, если его еще нет
            walletRepo.GetBankWallet();
            walletRepo.GetCashWallet();
            walletRepo.GetDebtWallet();

            log.AppendLine("Wallets created");
            File.WriteAllText("log.txt", log.ToString());

            var categoryRepo = Content.PaymentCategoryRepository;

            log.AppendLine("Category repo created");
            File.WriteAllText("log.txt", log.ToString());

            categoryRepo.RemoveAllWithPayments();

            log.AppendLine("Old Cats removed");
            File.WriteAllText("log.txt", log.ToString());

            var zpCat = categoryRepo.InsertItem(new PaymentCategory("Зарплата"));
            var vdCat = categoryRepo.InsertItem(new PaymentCategory("Водители", zpCat.ID));

            var ivCat = categoryRepo.InsertItem(new PaymentCategory("Иванов Дмитрий", vdCat.ID));
            var petCat = categoryRepo.InsertItem(new PaymentCategory("Петенько Жора", vdCat.ID));
            var loCat = categoryRepo.InsertItem(new PaymentCategory("Локоть Сергей", vdCat.ID));

            var postCat = categoryRepo.InsertItem(new PaymentCategory("Поставщики"));
            var post1 = categoryRepo.InsertItem(new PaymentCategory("ООО Черника", postCat.ID));
            var post2 = categoryRepo.InsertItem(new PaymentCategory("ИП Гитаров", postCat.ID));

            var income = categoryRepo.InsertItem(new PaymentCategory("Доходы"));

            var cafe = categoryRepo.InsertItem(new PaymentCategory("Кафе", income.ID));
            var delivery = categoryRepo.InsertItem(new PaymentCategory("Доставка", income.ID));

            log.AppendLine("New cats created");
            File.WriteAllText("log.txt", log.ToString());

            var endDate = DateTime.Now.AddDays(-1);
            var totalDays = 10;

            var rand = new Random();

            log.AppendLine("Rand created");
            File.WriteAllText("log.txt", log.ToString());

            log.AppendLine(string.Format("ID test : {0}", ivCat.ID));
            File.WriteAllText("log.txt", log.ToString());

            for (int i = totalDays - 1; i >= 0; i--)
            {
                var date = endDate.AddDays(-i);

                var paymentRepo = Content.PaymentRepository;

                log.AppendLine(string.Format("Payment repo created at {0}", date));
                File.WriteAllText("log.txt", log.ToString());

                paymentRepo.AddCashPayment(-100 * rand.Next(1, 10), "За доставку", ivCat.ID, date);
                paymentRepo.AddCashPayment(-100 * rand.Next(1, 10), "За доставку", petCat.ID, date);
                paymentRepo.AddCashPayment(-100 * rand.Next(1, 10), "За доставку", loCat.ID, date);

                log.AppendLine("Add some Cash payments for drivers");
                File.WriteAllText("log.txt", log.ToString());

                if (i % 3 == 0) paymentRepo.AddCashPayment(-1 * rand.Next(500, 10000), "По накладной №" + rand.Next(154, 200), post1.ID, date);
                if (i % 4 == 0) paymentRepo.AddCashPayment(-1 * rand.Next(500, 10000), "По накладной №" + rand.Next(154, 200), post2.ID, date);

                paymentRepo.AddBankPayment(rand.Next(7000, 25000), string.Empty, cafe.ID, date);
                paymentRepo.AddBankPayment(rand.Next(2000, 4000), string.Empty, delivery.ID, date);

                paymentRepo.AddCashPayment(rand.Next(5000, 14000), string.Empty, cafe.ID, date);
                paymentRepo.AddCashPayment(rand.Next(50000, 100000), string.Empty, delivery.ID, date);
            }

            log.AppendLine("Payments created");
            File.WriteAllText("log.txt", log.ToString());

            MessageBox.Show("Создание тестовых данных завершено");
        }

        private void toolStripButton4_Click_1(object sender, EventArgs e)
        {
            SingletonFormProvider<PrinterDiagnosticForm>.ActivateForm(() => new PrinterDiagnosticForm());
        }

        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Меню "связь с разработчиками" </summary>
        ////==================================================
        private void DevToolStripMenuItemClick(object sender, EventArgs e)
        {
            SingletonFormProvider<SendMailForm>.ActivateForm(() => new SendMailForm());
        }

        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Меню "О программе" </summary>
        ////==================================================
        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {
            var aboutForm = new AboutBox();
            aboutForm.ShowDialog();
        }

        //____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Отпущины клавиши </summary>
        ////=====================================
        private void MainFormKeyUp(object sender, KeyEventArgs e)
        {
            // Если сочитание клавишь и Админские права
            if (e.Control && e.Shift && e.Alt && e.KeyCode == Keys.H && Content.UserManager.IsAdmin(Content.UserManager.UserLogged))
            {
                // Показать окно ввода
                var inputCodeForm = new InputCodeForm();
                inputCodeForm.ShowDialog();

                // Если окну "понравился" код, показать секретное меню
                if (inputCodeForm.DialogResult == DialogResult.OK)
                {
                    HistoryMenuItem.Visible = true;
                }
            }
        }

        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Меню "Помощь по программе" </summary>
        ////==================================================
        private void HelpToAppToolStripMenuItemClick(object sender, EventArgs e)
        {
            FormHelpExtensions.ShowHelpPage("DeliveryHelp", "Вкладка Доставка");
        }

        private void брендыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<BrandList>.ActivateForm(() => new BrandList());
        }

        /// <summary>
        /// Выставляем выбранные брэнд в зависимости от выбранного таба
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mainTb_Selected(object sender, TabControlEventArgs e)
        {
            if (mainTb.SelectedTab != null)
            {
                var selectedBrand = Content.BrandManager.GetList(new BrandFilter { BrandName = mainTb.SelectedTab.Text });
                if (selectedBrand != null && selectedBrand.Count > 0)
                {
                    Content.BrandManager.SelectedBrand = selectedBrand.FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Открытия формы статистики заказов по пользователям
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OrdersByUserMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<StatsForm>.ActivateForm(() => new StatsForm());
        }

        /// <summary>
        /// Открытие формы управления фискальным принтером
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FisPrinterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<PrinterFisForm>.ActivateForm(() => new PrinterFisForm());
        }

        private void clientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<ReportСlientsForm>.ActivateForm(() => new ReportСlientsForm());
        }

        private void simpleSaleReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<SaleReportSimpleForm>.ActivateForm(() => new SaleReportSimpleForm());
        }

        private void goneClientForPeriodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<ReportNotActiveClientForm>.ActivateForm(() => new ReportNotActiveClientForm());
        }

        private void ReportNewClientsForPeriodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<ReportNewClientForm>.ActivateForm(() => new ReportNewClientForm());
        }

        private void dishCompatibilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<DishesMostCompatibility>.ActivateForm(() => new DishesMostCompatibility());
        }

        private void PointOfSaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<PointOfSaleList>.ActivateForm(() => new PointOfSaleList());
        }

        private void ToolStripMenuItemDeliveryTime_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<ReportDeliveryTime>.ActivateForm(() => new ReportDeliveryTime());
        }

        private void WarehouseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingletonFormProvider<WarehouseListForDishForm>.ActivateForm(() => new WarehouseListForDishForm());
        }

        private void workWithDiscountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var couponForm = new CouponListForm();
            couponForm.Initialize();
            SingletonFormProvider<CouponListForm>.ActivateForm(() => couponForm);
        }

        private void failureOrdersReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var failureOrdersRepport = new FailureOrdersReport();
            failureOrdersRepport.Show();
        }
    }
}