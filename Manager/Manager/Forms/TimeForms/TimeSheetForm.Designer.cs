﻿namespace LaRenzo.Forms.TimeForms
{
    partial class TimeSheetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.btnClearTime = new System.Windows.Forms.ToolStripButton();
            this.btnBlockTime = new System.Windows.Forms.ToolStripButton();
            this.btnCurrentTime = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.lblSelectedRowsCount = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lbSelectedOrder = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorkerGetGuid = new System.ComponentModel.BackgroundWorker();
            this.splashScreenManagerChangeState = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::LaRenzo.Forms.WaitFormGeneral), true, true);
            this.toolStripComboBoxBrand = new System.Windows.Forms.ToolStripComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            this.mainToolStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainGridControl
            // 
            this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainGridControl.Location = new System.Drawing.Point(0, 0);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(968, 583);
            this.mainGridControl.TabIndex = 0;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsBehavior.Editable = false;
            this.mainGridView.OptionsSelection.MultiSelect = true;
            this.mainGridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.mainGridView.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            this.mainGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnClearTime,
            this.btnBlockTime,
            this.btnCurrentTime,
            this.toolStripSeparator1,
            this.lblSelectedRowsCount,
            this.toolStripSeparator2,
            this.lbSelectedOrder,
            this.toolStripComboBoxBrand,
            this.toolStripComboBox});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(968, 31);
            this.mainToolStrip.TabIndex = 1;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // btnClearTime
            // 
            this.btnClearTime.Image = global::LaRenzo.Properties.Resources.add_wh;
            this.btnClearTime.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClearTime.Name = "btnClearTime";
            this.btnClearTime.Size = new System.Drawing.Size(87, 28);
            this.btnClearTime.Text = "Очистить";
            this.btnClearTime.Click += new System.EventHandler(this.btnClearTime_Click);
            // 
            // btnBlockTime
            // 
            this.btnBlockTime.Image = global::LaRenzo.Properties.Resources.close_wh;
            this.btnBlockTime.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnBlockTime.Name = "btnBlockTime";
            this.btnBlockTime.Size = new System.Drawing.Size(85, 28);
            this.btnBlockTime.Text = "Перерыв";
            this.btnBlockTime.Click += new System.EventHandler(this.btnBlockTime_Click);
            // 
            // btnCurrentTime
            // 
            this.btnCurrentTime.Image = global::LaRenzo.Properties.Resources.diagram_20_4574;
            this.btnCurrentTime.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCurrentTime.Name = "btnCurrentTime";
            this.btnCurrentTime.Size = new System.Drawing.Size(120, 28);
            this.btnCurrentTime.Text = "Текущее время";
            this.btnCurrentTime.Click += new System.EventHandler(this.btnCurrentTime_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // lblSelectedRowsCount
            // 
            this.lblSelectedRowsCount.Name = "lblSelectedRowsCount";
            this.lblSelectedRowsCount.Size = new System.Drawing.Size(108, 28);
            this.lblSelectedRowsCount.Text = "Выделено 0 минут";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // lbSelectedOrder
            // 
            this.lbSelectedOrder.Name = "lbSelectedOrder";
            this.lbSelectedOrder.Size = new System.Drawing.Size(53, 28);
            this.lbSelectedOrder.Text = "Не заказ";
            // 
            // toolStripComboBox
            // 
            this.toolStripComboBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripComboBox.Name = "toolStripComboBox";
            this.toolStripComboBox.Size = new System.Drawing.Size(121, 31);
            this.toolStripComboBox.ToolTipText = "Точка продаж";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.mainGridControl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(968, 583);
            this.panel1.TabIndex = 2;
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Interval = 5000;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdater_Tick);
            // 
            // backgroundWorkerGetGuid
            // 
            this.backgroundWorkerGetGuid.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerGetGuid_DoWork);
            this.backgroundWorkerGetGuid.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerGetGuid_RunWorkerCompleted);
            // 
            // toolStripComboBoxBrand
            // 
            this.toolStripComboBoxBrand.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripComboBoxBrand.Name = "toolStripComboBoxBrand";
            this.toolStripComboBoxBrand.Size = new System.Drawing.Size(121, 31);
            // 
            // TimeSheetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 614);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.mainToolStrip);
            this.Name = "TimeSheetForm";
            this.Text = "Управление временем";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TimeSheetForm_FormClosing);
            this.Load += new System.EventHandler(this.TimeSheetForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl mainGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton btnClearTime;
        private System.Windows.Forms.ToolStripButton btnBlockTime;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timerUpdate;
        private System.Windows.Forms.ToolStripButton btnCurrentTime;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel lblSelectedRowsCount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel lbSelectedOrder;
        private System.ComponentModel.BackgroundWorker backgroundWorkerGetGuid;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxBrand;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManagerChangeState;
    }
}