﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Reports.SettlsWithPartnerReport;
using LaRenzo.Interfaces.SignalMessages;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.ReportForms
{
    public partial class SettlsWithPartnerReport : Form
    {
        private Guid performanceGuid;
        public SettlsWithPartnerReport()
        {
            InitializeComponent();
            dTimeEnd.Value = DateTime.Now;
            ReportHelper.SetUpButtonDefault(btnGenerate);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void UpdateData()
        {
            if (backgroundWorkerReportGenerator.IsBusy)
                MessageBox.Show(Resources.GeneraitingAnotherReport);
            else
            {
                performanceGuid = ExecutionTimeLogger.Start();
                ReportHelper.SetUpButtonOnStart(btnGenerate);
                backgroundWorkerReportGenerator.RunWorkerAsync();
            }
        }

        private void backgroundWorkerReportGenerator_DoWork(object sender, DoWorkEventArgs e)
        {
            
            var settlsWithPartnerReportManager = Content.SettlsWithPartnerReportManager;
            var guid = Guid.NewGuid();
            Content.SignalProcessor.UnBind<ReportDateProgressChangedSignalMessage>();
            Content.SignalProcessor.Bind<ReportDateProgressChangedSignalMessage>((m) => {
                if (guid != m.Guid)
                    return;
                backgroundWorkerReportGenerator.ReportProgress(m.Percentage);
            });

            e.Result = settlsWithPartnerReportManager.GetReportDate(dTimeEnd.Value);

        }


        private void backgroundWorkerReportGenerator_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pBarGeneration.Value = e.ProgressPercentage;
        }

        private void backgroundWorkerReportGenerator_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            mainGridControl.DataSource = null;
            mainGridControl.DataSource = e.Result;
            ReportHelper.SetUpButtonDefault(btnGenerate);
            ExecutionTimeLogger.Stop(performanceGuid, "Отчет взаиморасчеты с контрагентами");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //mainGridView.ShowPrintPreview();
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, DevExpress.XtraPrinting.CreateAreaEventArgs e)
        {
            DevExpress.XtraPrinting.TextBrick brick;

            brick =
                e.Graph.DrawString(string.Format("Взаиморасчеты с контрагентами на {0:dd.MM.yyyy hh:mm}", dTimeEnd.Value),
                                   Color.Navy, new RectangleF(0, 0, 560, 60), DevExpress.XtraPrinting.BorderSide.None);

            brick.Font = new Font("Arial", 16);

            brick.StringFormat = new DevExpress.XtraPrinting.BrickStringFormat(StringAlignment.Center);
        }
    }
}