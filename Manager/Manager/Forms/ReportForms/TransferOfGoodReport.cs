﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraPrinting;
using LaRenzo.DataRepository.Entities.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Warehouses;
using LaRenzo.DataRepository.Repositories.Reports.TransferOfGoodReport;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.ReportForms
{
    public partial class TransferOfGoodsReport : Form
    {
        private bool isBusy = false;
        public TransferOfGoodsReport()
        {
            InitializeComponent();
            dTimeStart.Value = DateTime.Now.AddMonths(-1);
            dTimeStart.MaxDate = DateTime.Now;
            dTimeEnd.Value = DateTime.Now;
            dTimeEnd.MinDate = dTimeStart.Value;
            ReportHelper.SetUpButtonDefault(btnGenerate);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            UpdateData();
        }

        private async void UpdateData()
        {
            if (isBusy)
            {
                MessageBox.Show(Resources.GeneraitingAnotherReport);
                return;
            }

            if (dTimeStart.Value - dTimeEnd.Value > new TimeSpan(0))
            {
                MessageBox.Show("Дата начала построения отчета должна быть меньше даты окончания");
                return;
            }

            isBusy = true;
            var key = ExecutionTimeLogger.Start();


            var warehouseId = cmbWarehouse.SelectedItem == null
                ? -1
                : ((Warehouse) cmbWarehouse.SelectedItem).ID;


            ReportHelper.SetUpButtonOnStart(btnGenerate);
            //btnGenerate.Text = "Генерируется отчет...";
            
            warehouseId = cmbWarehouse.SelectedItem == null
                ? -1
                : ((Warehouse) cmbWarehouse.SelectedItem).ID;

            var warehouse = cmbWarehouse.Items.Cast<Warehouse>().FirstOrDefault(x => x.ID == warehouseId);
            var warehouseName = warehouse == null ? "Неизветсно" : warehouse.Name;

            var data =
                await
                    Task.Run(
                        () =>
                            Content.TransferOfGoodReportManager.GetReportDate(dTimeStart.Value, dTimeEnd.Value,
                                warehouseId));

            mainGridControl.DataSource = null;
            mainGridControl.DataSource = data;

            ReportHelper.SetUpButtonDefault(btnGenerate);
            //btnGenerate.Text = "Сгенерировать отчет";
            ExecutionTimeLogger.Stop(key, String.Format("Отчет движения товаров на складах. Склад '{0}'", warehouseName));
            isBusy = false;
        }


        private void RemainsOfGoodReport_Load(object sender, EventArgs e)
        {
            var warehouses = Content.WarehouseManager.GetWarehouses();
            //Склад "Все" пока работает с ошибкой
            //warehouses.Add(new Warehouse { ID = -1, IsBase = false, Name = "Все" });
            cmbWarehouse.DataSource = warehouses.OrderBy(x=>x.ID).ToList();
            cmbWarehouse.DisplayMember = "Name";
            cmbWarehouse.ValueMember = "ID";
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            TextBrick brick = e.Graph.DrawString(string.Format("Движение товаров на складах с {0:dd.MM.yyyy hh:mm} по {1:dd.MM.yyyy hh:mm}", dTimeStart.Value, dTimeEnd.Value), Color.Navy, new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 16);

            brick.StringFormat = new BrickStringFormat(StringAlignment.Center);

        }

        private void mainGridView_MasterRowGetRelationDisplayCaption(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "Документы движения";
        }

        private void mainGridView_MasterRowGetRelationCount(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;
        }

        private void mainGridView_MasterRowGetRelationName(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "detailGridView";
        }

        private void mainGridView_MasterRowGetChildList(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventArgs e)
        {
            if (mainGridView != null)
            {
                var transferOfGoodRecord = mainGridView.GetRow(e.RowHandle) as TransferOfGoodReportRecord;
                if (transferOfGoodRecord != null)
                {
                    var warehouseId = cmbWarehouse.SelectedItem == null
                                      ? -1
                                      : ((Warehouse) cmbWarehouse.SelectedItem).ID;

                    e.ChildList = Content.TransferOfGoodReportManager.GetDetailRecords(dTimeStart.Value, dTimeEnd.Value, warehouseId, transferOfGoodRecord.ProductId);
                }
            }
        }

        private void dTimeEnd_ValueChanged(object sender, EventArgs e)
        {
            dTimeStart.MaxDate = dTimeEnd.Value;
        }

        private void dTimeStart_ValueChanged(object sender, EventArgs e)
        {
            dTimeEnd.MinDate = dTimeStart.Value;
        }
    }
}