﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.Statistics;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.ReportForms
{
    public partial class SaleReportForm : Form
    {
        private Guid performanceKey;
        public SaleReportForm()
        {
            InitializeComponent();
            mainGridView.OptionsBehavior.AlignGroupSummaryInGroupRow = DefaultBoolean.True;
            dTimeStart.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59).AddMonths(-1);
            dTimeEnd.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0,0,0);//DateTime.Now;
            cmbSaleMode.SelectedIndex = 0;
            ReportHelper.SetUpButtonDefault(btnGenerate);
        }

        private void gridView1_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            /*if (e.Column.FieldName == "Cost" && e.IsGetData)
            {
                // ReSharper disable CSharpWarnings::CS0612
                int quantity = Convert.ToInt32(mainGridView.GetRowCellValue(e.ListSourceRowIndex, columnAmount));

                int price = Convert.ToInt32(mainGridView.GetRowCellValue(e.ListSourceRowIndex, columnPrice));
                // ReSharper restore CSharpWarnings::CS0612
                e.Value = quantity * price;
            }*/
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            UpdateData();
        }

        
        private void SaleReportForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) UpdateData();
            if (e.KeyCode == Keys.Escape) Close();
        }

        private void dateTimePickerEnd_ValueChanged(object sender, EventArgs e)
        {
            var picker = (DateTimePicker) sender;
            picker.Value = new DateTime(picker.Value.Year, picker.Value.Month, picker.Value.Day, 23, 59, 59);
        }

        private void dateTimePickerStart_ValueChanged(object sender, EventArgs e)
        {
            var picker = (DateTimePicker)sender;
            picker.Value = new DateTime(picker.Value.Year, picker.Value.Month, picker.Value.Day, 0, 0, 0);
        }

        private void UpdateData()
        {
	        if (backgroundWorkerReportGenerator.IsBusy)
	        {
		        MessageBox.Show(Resources.GeneraitingAnotherReport);
	        }
	        else
	        {
				pBarGeneration.Style = ProgressBarStyle.Marquee;
				performanceKey = ExecutionTimeLogger.Start();
		        ReportHelper.SetUpButtonDefault(btnGenerate);
		        backgroundWorkerReportGenerator.RunWorkerAsync();
	        }
        }


        private void backgroundWorkerReportGenerator_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int mode = 0;

            Invoke((MethodInvoker)delegate
                {
                    mode = cmbSaleMode.SelectedIndex;
                });

            var staticManager = Content.StatisticManager;
            //staticManager.ProgressChanged += (o, percentage) => backgroundWorkerReportGenerator.ReportProgress(percentage);
			
            e.Result = staticManager.GetSalesReportData(dTimeStart.Value, dTimeEnd.Value, mode, checkBoxIsActiveDish.Checked);
        }

        private void backgroundWorkerReportGenerator_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            pBarGeneration.Value = e.ProgressPercentage;
        }

        private void backgroundWorkerReportGenerator_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            pBarGeneration.Style = ProgressBarStyle.Blocks;
	        pBarGeneration.Value = pBarGeneration.Maximum;
			
			mainGridControl.DataSource = null;
            mainGridControl.DataSource = e.Result;
            ExecutionTimeLogger.Stop(performanceKey, "Отчет по продажам");
            ReportHelper.SetUpButtonDefault(btnGenerate);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
	        string reportName = string.Format("Отчет по продажам ({0}) c {1:dd.MM.yyyy} по {2:dd.MM.yyyy}", cmbSaleMode.Items[cmbSaleMode.SelectedIndex], dTimeStart.Value, dTimeEnd.Value);

			TextBrick brick = e.Graph.DrawString(reportName, Color.Navy, new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 16);

            brick.StringFormat = new BrickStringFormat(StringAlignment.Center);
        }

        private void mainGridControl_Click(object sender, EventArgs e)
        {

        }
    }
}
