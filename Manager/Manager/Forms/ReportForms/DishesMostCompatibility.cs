﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.Statistics;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.ReportForms
{
    public partial class DishesMostCompatibility : Form
    {
        public DishesMostCompatibility()
        {
            InitializeComponent();
            mainGridView.VertScrollVisibility = ScrollVisibility.Auto;
            mainGridView.ScrollStyle = ScrollStyleFlags.LiveVertScroll;
            ReportHelper.SetUpButtonDefault(btnGenerate);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            btnGenerate.Enabled = false;
            UpdateData();
        }

        private void SaleReportForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) UpdateData();
            if (e.KeyCode == Keys.Escape) Close();
        }

        private void UpdateData()
        {
            if (backgroundWorkerReportGenerator.IsBusy)
            {
                MessageBox.Show(Resources.GeneraitingAnotherReport);
            }
            else
            {
                ReportHelper.SetUpButtonDefault(btnGenerate);
                backgroundWorkerReportGenerator.RunWorkerAsync();
            }
        }


        private void backgroundWorkerReportGenerator_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            Invoke((MethodInvoker)delegate
                {
                    
                });

            var staticManager = Content.StatisticManager;
            e.Result = staticManager.GetMostCompatibilityDishes();
        }

        private void backgroundWorkerReportGenerator_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mainGridControl.DataSource = null;
            mainGridControl.DataSource = e.Result;
            ReportHelper.SetUpButtonDefault(btnGenerate);
            btnGenerate.Enabled = true;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            printableComponentLink.CreateDocument();
            printableComponentLink.ShowPreview();
        }

        private void printableComponentLink_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
        {
            string reportName =
                $"Совместимость блюд";

            TextBrick brick = e.Graph.DrawString(reportName, Color.Navy, new RectangleF(0, 0, 560, 60), BorderSide.None);

            brick.Font = new Font("Arial", 16);

            brick.StringFormat = new BrickStringFormat(StringAlignment.Center);
        }
    }
}
