﻿namespace LaRenzo.Forms.ReportForms
{
    partial class SaleReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.brandColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.isCafe = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMargin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnRevenue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMarginPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPointOfSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCreatedMonth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCreatedDayOfWeek = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.сolumnAddressGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dTimeStart = new System.Windows.Forms.DateTimePicker();
            this.dTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.lblPeriodStart = new System.Windows.Forms.Label();
            this.lblPeriodEnd = new System.Windows.Forms.Label();
            this.pBarGeneration = new System.Windows.Forms.ProgressBar();
            this.backgroundWorkerReportGenerator = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnPrint = new System.Windows.Forms.Button();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.cmbSaleMode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxIsActiveDish = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.SuspendLayout();
            // 
            // mainGridControl
            // 
            this.mainGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Location = new System.Drawing.Point(0, 89);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Margin = new System.Windows.Forms.Padding(4);
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(1322, 481);
            this.mainGridControl.TabIndex = 3;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            this.mainGridControl.Click += new System.EventHandler(this.mainGridControl_Click);
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.brandColumn,
            this.columnId,
            this.columnName,
            this.columnPrice,
            this.columnAmount,
            this.columnSum,
            this.columnCategory,
            this.isCafe,
            this.columnCost,
            this.columnMargin,
            this.columnRevenue,
            this.columnMarginPercent,
            this.columnPointOfSale,
            this.columnDate,
            this.columnCreatedMonth,
            this.columnCreatedDayOfWeek,
            this.columnHour,
            this.columnUser,
            this.сolumnAddressGroup,
            this.columnSection});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Average, "columnMarginPercent", this.columnMarginPercent, "{0:P1}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Amount", this.columnAmount, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", this.columnSum, "{0:F2} ₽")});
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.BestFitMaxRowCount = 10;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
            // 
            // brandColumn
            // 
            this.brandColumn.Caption = "Бренд";
            this.brandColumn.FieldName = "Brand.Name";
            this.brandColumn.Name = "brandColumn";
            this.brandColumn.Visible = true;
            this.brandColumn.VisibleIndex = 0;
            // 
            // columnId
            // 
            this.columnId.Caption = "Ид";
            this.columnId.FieldName = "Dish.ID";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowFocus = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 1;
            this.columnId.Width = 63;
            // 
            // columnName
            // 
            this.columnName.Caption = "Название блюда";
            this.columnName.FieldName = "Dish.Name";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 2;
            this.columnName.Width = 63;
            // 
            // columnPrice
            // 
            this.columnPrice.Caption = "Цена";
            this.columnPrice.FieldName = "Price";
            this.columnPrice.Name = "columnPrice";
            this.columnPrice.OptionsColumn.AllowEdit = false;
            this.columnPrice.OptionsColumn.AllowFocus = false;
            this.columnPrice.OptionsColumn.ReadOnly = true;
            this.columnPrice.Visible = true;
            this.columnPrice.VisibleIndex = 6;
            this.columnPrice.Width = 60;
            // 
            // columnAmount
            // 
            this.columnAmount.Caption = "Количество";
            this.columnAmount.FieldName = "Amount";
            this.columnAmount.Name = "columnAmount";
            this.columnAmount.OptionsColumn.AllowEdit = false;
            this.columnAmount.OptionsColumn.AllowFocus = false;
            this.columnAmount.OptionsColumn.ReadOnly = true;
            this.columnAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnAmount.Visible = true;
            this.columnAmount.VisibleIndex = 7;
            this.columnAmount.Width = 60;
            // 
            // columnSum
            // 
            this.columnSum.Caption = "Сумма";
            this.columnSum.DisplayFormat.FormatString = "F2";
            this.columnSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnSum.FieldName = "Total";
            this.columnSum.Name = "columnSum";
            this.columnSum.OptionsColumn.AllowEdit = false;
            this.columnSum.OptionsColumn.AllowFocus = false;
            this.columnSum.OptionsColumn.ReadOnly = true;
            this.columnSum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "{0:F2} ₽")});
            this.columnSum.UnboundExpression = "[Amount] * [Price]";
            this.columnSum.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.columnSum.Visible = true;
            this.columnSum.VisibleIndex = 8;
            this.columnSum.Width = 60;
            // 
            // columnCategory
            // 
            this.columnCategory.Caption = "Категория";
            this.columnCategory.FieldName = "Dish.Category.Name";
            this.columnCategory.Name = "columnCategory";
            this.columnCategory.OptionsColumn.AllowEdit = false;
            this.columnCategory.OptionsColumn.AllowFocus = false;
            this.columnCategory.OptionsColumn.ReadOnly = true;
            this.columnCategory.Visible = true;
            this.columnCategory.VisibleIndex = 10;
            this.columnCategory.Width = 60;
            // 
            // isCafe
            // 
            this.isCafe.Caption = "Кафе";
            this.isCafe.FieldName = "IsCafe";
            this.isCafe.Name = "isCafe";
            this.isCafe.Visible = true;
            this.isCafe.VisibleIndex = 11;
            this.isCafe.Width = 72;
            // 
            // columnCost
            // 
            this.columnCost.Caption = "Себестоимость";
            this.columnCost.DisplayFormat.FormatString = "F2";
            this.columnCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnCost.FieldName = "Cost";
            this.columnCost.Name = "columnCost";
            this.columnCost.OptionsColumn.AllowEdit = false;
            this.columnCost.OptionsColumn.ReadOnly = true;
            this.columnCost.Visible = true;
            this.columnCost.VisibleIndex = 3;
            this.columnCost.Width = 63;
            // 
            // columnMargin
            // 
            this.columnMargin.Caption = "Наценка, ₽";
            this.columnMargin.DisplayFormat.FormatString = "F2";
            this.columnMargin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnMargin.FieldName = "columnMargin";
            this.columnMargin.Name = "columnMargin";
            this.columnMargin.OptionsColumn.AllowEdit = false;
            this.columnMargin.OptionsColumn.ReadOnly = true;
            this.columnMargin.UnboundExpression = "[Price] - [Cost]";
            this.columnMargin.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.columnMargin.Visible = true;
            this.columnMargin.VisibleIndex = 4;
            this.columnMargin.Width = 63;
            // 
            // columnRevenue
            // 
            this.columnRevenue.Caption = "Прибыль";
            this.columnRevenue.DisplayFormat.FormatString = "F2";
            this.columnRevenue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnRevenue.FieldName = "columnRevenue";
            this.columnRevenue.Name = "columnRevenue";
            this.columnRevenue.OptionsColumn.AllowEdit = false;
            this.columnRevenue.OptionsColumn.ReadOnly = true;
            this.columnRevenue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "columnRevenue", "{0:F2} ₽")});
            this.columnRevenue.UnboundExpression = "[columnMargin] * [Amount]";
            this.columnRevenue.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.columnRevenue.Visible = true;
            this.columnRevenue.VisibleIndex = 9;
            this.columnRevenue.Width = 60;
            // 
            // columnMarginPercent
            // 
            this.columnMarginPercent.Caption = "Наценка, %";
            this.columnMarginPercent.DisplayFormat.FormatString = "P1";
            this.columnMarginPercent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnMarginPercent.FieldName = "columnMarginPercent";
            this.columnMarginPercent.Name = "columnMarginPercent";
            this.columnMarginPercent.OptionsColumn.AllowEdit = false;
            this.columnMarginPercent.OptionsColumn.ReadOnly = true;
            this.columnMarginPercent.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Average, "columnMarginPercent", "{0:P1}")});
            this.columnMarginPercent.UnboundExpression = "[columnMargin]  /  [Cost]";
            this.columnMarginPercent.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.columnMarginPercent.Visible = true;
            this.columnMarginPercent.VisibleIndex = 5;
            this.columnMarginPercent.Width = 72;
            // 
            // columnPointOfSale
            // 
            this.columnPointOfSale.Caption = "Точка продаж";
            this.columnPointOfSale.FieldName = "PointOfSale";
            this.columnPointOfSale.Name = "columnPointOfSale";
            // 
            // columnDate
            // 
            this.columnDate.Caption = "Дата";
            this.columnDate.DisplayFormat.FormatString = "dd.MM.yyyy";
            this.columnDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.columnDate.FieldName = "CreatedDate";
            this.columnDate.Name = "columnDate";
            // 
            // columnCreatedMonth
            // 
            this.columnCreatedMonth.Caption = "Месяц";
            this.columnCreatedMonth.FieldName = "CreatedMonth";
            this.columnCreatedMonth.Name = "columnCreatedMonth";
            // 
            // columnCreatedDayOfWeek
            // 
            this.columnCreatedDayOfWeek.Caption = "День недели";
            this.columnCreatedDayOfWeek.FieldName = "CreatedDayOfWeek";
            this.columnCreatedDayOfWeek.Name = "columnCreatedDayOfWeek";
            // 
            // columnHour
            // 
            this.columnHour.Caption = "Час";
            this.columnHour.FieldName = "CreatedHour";
            this.columnHour.Name = "columnHour";
            // 
            // columnUser
            // 
            this.columnUser.Caption = "Менеджер";
            this.columnUser.FieldName = "User";
            this.columnUser.Name = "columnUser";
            // 
            // сolumnAddressGroup
            // 
            this.сolumnAddressGroup.Caption = "Район";
            this.сolumnAddressGroup.FieldName = "AddressGroup";
            this.сolumnAddressGroup.Name = "сolumnAddressGroup";
            // 
            // columnSection
            // 
            this.columnSection.Caption = "Цех";
            this.columnSection.FieldName = "Dish.Category.Section.Name";
            this.columnSection.Name = "columnSection";
            // 
            // dTimeStart
            // 
            this.dTimeStart.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dTimeStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTimeStart.Location = new System.Drawing.Point(17, 46);
            this.dTimeStart.Margin = new System.Windows.Forms.Padding(5);
            this.dTimeStart.Name = "dTimeStart";
            this.dTimeStart.Size = new System.Drawing.Size(160, 22);
            this.dTimeStart.TabIndex = 1;
            // 
            // dTimeEnd
            // 
            this.dTimeEnd.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dTimeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTimeEnd.Location = new System.Drawing.Point(188, 46);
            this.dTimeEnd.Margin = new System.Windows.Forms.Padding(5);
            this.dTimeEnd.Name = "dTimeEnd";
            this.dTimeEnd.Size = new System.Drawing.Size(160, 22);
            this.dTimeEnd.TabIndex = 2;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGenerate.Location = new System.Drawing.Point(744, 14);
            this.btnGenerate.Margin = new System.Windows.Forms.Padding(5);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(345, 30);
            this.btnGenerate.TabIndex = 3;
            this.btnGenerate.Text = "Сгенерировать отчет";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // lblPeriodStart
            // 
            this.lblPeriodStart.AutoSize = true;
            this.lblPeriodStart.Location = new System.Drawing.Point(14, 14);
            this.lblPeriodStart.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblPeriodStart.Name = "lblPeriodStart";
            this.lblPeriodStart.Size = new System.Drawing.Size(118, 17);
            this.lblPeriodStart.TabIndex = 7;
            this.lblPeriodStart.Text = "Начало периода";
            // 
            // lblPeriodEnd
            // 
            this.lblPeriodEnd.AutoSize = true;
            this.lblPeriodEnd.Location = new System.Drawing.Point(185, 14);
            this.lblPeriodEnd.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblPeriodEnd.Name = "lblPeriodEnd";
            this.lblPeriodEnd.Size = new System.Drawing.Size(109, 17);
            this.lblPeriodEnd.TabIndex = 8;
            this.lblPeriodEnd.Text = "Конец периода";
            // 
            // pBarGeneration
            // 
            this.pBarGeneration.Location = new System.Drawing.Point(744, 54);
            this.pBarGeneration.Margin = new System.Windows.Forms.Padding(5);
            this.pBarGeneration.Name = "pBarGeneration";
            this.pBarGeneration.Size = new System.Drawing.Size(345, 10);
            this.pBarGeneration.TabIndex = 9;
            // 
            // backgroundWorkerReportGenerator
            // 
            this.backgroundWorkerReportGenerator.WorkerReportsProgress = true;
            this.backgroundWorkerReportGenerator.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerReportGenerator_DoWork);
            this.backgroundWorkerReportGenerator.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerReportGenerator_ProgressChanged);
            this.backgroundWorkerReportGenerator.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerReportGenerator_RunWorkerCompleted);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(1110, 14);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(5);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(177, 30);
            this.btnPrint.TabIndex = 23;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // cmbSaleMode
            // 
            this.cmbSaleMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSaleMode.FormattingEnabled = true;
            this.cmbSaleMode.Items.AddRange(new object[] {
            "Все",
            "Кафе",
            "Доставка"});
            this.cmbSaleMode.Location = new System.Drawing.Point(361, 46);
            this.cmbSaleMode.Margin = new System.Windows.Forms.Padding(5);
            this.cmbSaleMode.Name = "cmbSaleMode";
            this.cmbSaleMode.Size = new System.Drawing.Size(212, 24);
            this.cmbSaleMode.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(358, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 25;
            this.label1.Text = "Продажи:";
            // 
            // checkBoxIsActiveDish
            // 
            this.checkBoxIsActiveDish.AutoSize = true;
            this.checkBoxIsActiveDish.Checked = true;
            this.checkBoxIsActiveDish.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIsActiveDish.Location = new System.Drawing.Point(591, 46);
            this.checkBoxIsActiveDish.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxIsActiveDish.Name = "checkBoxIsActiveDish";
            this.checkBoxIsActiveDish.Size = new System.Drawing.Size(144, 21);
            this.checkBoxIsActiveDish.TabIndex = 26;
            this.checkBoxIsActiveDish.Text = "Только активные";
            this.checkBoxIsActiveDish.UseVisualStyleBackColor = true;
            // 
            // SaleReportForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1322, 571);
            this.Controls.Add(this.checkBoxIsActiveDish);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbSaleMode);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.pBarGeneration);
            this.Controls.Add(this.lblPeriodEnd);
            this.Controls.Add(this.lblPeriodStart);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.dTimeEnd);
            this.Controls.Add(this.dTimeStart);
            this.Controls.Add(this.mainGridControl);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "SaleReportForm";
            this.Text = "Отчёт по продажам";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SaleReportForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrice;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnSum;
        private DevExpress.XtraGrid.Columns.GridColumn columnCategory;
        private System.Windows.Forms.DateTimePicker dTimeStart;
        private System.Windows.Forms.DateTimePicker dTimeEnd;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Label lblPeriodStart;
        private System.Windows.Forms.Label lblPeriodEnd;
        private System.Windows.Forms.ProgressBar pBarGeneration;
        private System.ComponentModel.BackgroundWorker backgroundWorkerReportGenerator;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button btnPrint;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
        private System.Windows.Forms.ComboBox cmbSaleMode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.Columns.GridColumn isCafe;
        private DevExpress.XtraGrid.Columns.GridColumn columnCost;
        private DevExpress.XtraGrid.Columns.GridColumn columnMargin;
        private DevExpress.XtraGrid.Columns.GridColumn columnRevenue;
        private DevExpress.XtraGrid.Columns.GridColumn columnMarginPercent;
        private DevExpress.XtraGrid.Columns.GridColumn brandColumn;
        private DevExpress.XtraGrid.Columns.GridColumn columnPointOfSale;
        private DevExpress.XtraGrid.Columns.GridColumn columnDate;
        private DevExpress.XtraGrid.Columns.GridColumn columnCreatedMonth;
        private DevExpress.XtraGrid.Columns.GridColumn columnCreatedDayOfWeek;
        private DevExpress.XtraGrid.Columns.GridColumn columnHour;
        private DevExpress.XtraGrid.Columns.GridColumn columnUser;
        private DevExpress.XtraGrid.Columns.GridColumn сolumnAddressGroup;
        private System.Windows.Forms.CheckBox checkBoxIsActiveDish;
        private DevExpress.XtraGrid.Columns.GridColumn columnSection;
    }
}