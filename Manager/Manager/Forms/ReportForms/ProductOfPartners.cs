﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Partners;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Reports.PartnersProduct;
using LaRenzo.Properties;
using LaRenzo.Tools;

namespace LaRenzo.Forms.ReportForms
{
    public partial class ProductOfPartners : Form
    {
        private Guid performanceGuid;
        public ProductOfPartners()
        {
            InitializeComponent();
            ReportHelper.SetUpButtonDefault(btnGenerate);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void UpdateData()
        {
            if (backgroundWorkerReportGenerator.IsBusy)
                MessageBox.Show(Resources.GeneraitingAnotherReport);
            else
            {
                ReportHelper.SetUpButtonOnStart(btnGenerate);
                performanceGuid = ExecutionTimeLogger.Start();
                backgroundWorkerReportGenerator.RunWorkerAsync();
            }
        }

        private void backgroundWorkerReportGenerator_DoWork(object sender, DoWorkEventArgs e)
        {
            
            
            var manager = Content.PartnersProductManager;
            e.Result = manager.GetReportData();
        }

        private void backgroundWorkerReportGenerator_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pBarGeneration.Value = e.ProgressPercentage;
        }

        private void backgroundWorkerReportGenerator_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            mainGridControl.DataSource = null;
            mainGridControl.DataSource = e.Result;
            ReportHelper.SetUpButtonDefault(btnGenerate);
            ExecutionTimeLogger.Stop(performanceGuid, "Отчет Контрагент -> Продукты");
        }

        private void mainGridView_MasterRowGetChildList(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetChildListEventArgs e)
        {
            var manager = Content.PartnersProductManager;
            if (mainGridView != null)
            {
                var record = mainGridView.GetRow(e.RowHandle) as Partner;
                if (record != null)
                    e.ChildList = manager.GetDetailRecords(record.ID);
            }
        }

        private void mainGridView_MasterRowGetRelationName(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "detailGridView";
        }

        private void mainGridView_MasterRowGetRelationCount(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationCountEventArgs e)
        {
            e.RelationCount = 1;
        }

        private void mainGridView_MasterRowGetRelationDisplayCaption(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
        {
            e.RelationName = "Контрагенты";
        }
    }
}
