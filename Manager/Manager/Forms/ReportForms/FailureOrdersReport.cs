﻿using DevExpress.Utils;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Documents.Orders;
using System;
using System.Windows.Forms;

namespace LaRenzo.Forms.ReportForms
{
    public partial class FailureOrdersReport : Form
    {
        public FailureOrdersReport()
        {
            InitializeComponent();
            gridViewReport.OptionsBehavior.AlignGroupSummaryInGroupRow = DefaultBoolean.True;
        }

        private void buttonGenerateReport_Click(object sender, EventArgs e)
        {
            var failureOrders = Content.OrderManager.GetFailureOrders(pickerDateFrom.Value, pickerDateTo.Value);
            gridReport.DataSource = failureOrders;
        }
    }
}
