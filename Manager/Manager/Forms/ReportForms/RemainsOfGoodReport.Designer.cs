﻿namespace LaRenzo.Forms.ReportForms
{
    partial class RemainsOfGoodReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainGridControl = new DevExpress.XtraGrid.GridControl();
            this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.columnId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMinAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnDelta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMedian = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnAveragePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnLastPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnMaxGap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridWeightedPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lblPeriodEnd = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.dTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.backgroundWorkerReportGenerator = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmbWarehouse = new System.Windows.Forms.ComboBox();
            this.lblWarehouse = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.SuspendLayout();
            // 
            // mainGridControl
            // 
            this.mainGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.mainGridControl.Location = new System.Drawing.Point(0, 34);
            this.mainGridControl.MainView = this.mainGridView;
            this.mainGridControl.Name = "mainGridControl";
            this.mainGridControl.Size = new System.Drawing.Size(1338, 557);
            this.mainGridControl.TabIndex = 12;
            this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
            // 
            // mainGridView
            // 
            this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnId,
            this.columnName,
            this.columnAmount,
            this.columnMinAmount,
            this.columnDelta,
            this.columnMeasure,
            this.columnPrice,
            this.columnSum,
            this.columnCategory,
            this.columnMedian,
            this.columnAveragePrice,
            this.columnLastPrice,
            this.gridColumn1,
            this.columnMaxGap,
            this.gridWeightedPrice});
            this.mainGridView.GridControl = this.mainGridControl;
            this.mainGridView.Name = "mainGridView";
            this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.mainGridView.OptionsView.BestFitMaxRowCount = 10;
            this.mainGridView.OptionsView.ShowFooter = true;
            this.mainGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.columnDelta, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.mainGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.mainGridView_RowStyle);
            // 
            // columnId
            // 
            this.columnId.Caption = "Ид";
            this.columnId.FieldName = "ProductId";
            this.columnId.Name = "columnId";
            this.columnId.OptionsColumn.AllowEdit = false;
            this.columnId.OptionsColumn.AllowFocus = false;
            this.columnId.OptionsColumn.ReadOnly = true;
            this.columnId.Visible = true;
            this.columnId.VisibleIndex = 0;
            this.columnId.Width = 77;
            // 
            // columnName
            // 
            this.columnName.Caption = "Продукт";
            this.columnName.FieldName = "ProductName";
            this.columnName.Name = "columnName";
            this.columnName.OptionsColumn.AllowEdit = false;
            this.columnName.OptionsColumn.AllowFocus = false;
            this.columnName.OptionsColumn.ReadOnly = true;
            this.columnName.Visible = true;
            this.columnName.VisibleIndex = 1;
            this.columnName.Width = 330;
            // 
            // columnAmount
            // 
            this.columnAmount.Caption = "Количество";
            this.columnAmount.FieldName = "ProductCount";
            this.columnAmount.Name = "columnAmount";
            this.columnAmount.OptionsColumn.AllowEdit = false;
            this.columnAmount.OptionsColumn.AllowFocus = false;
            this.columnAmount.OptionsColumn.ReadOnly = true;
            this.columnAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnAmount.Visible = true;
            this.columnAmount.VisibleIndex = 2;
            this.columnAmount.Width = 89;
            // 
            // columnMinAmount
            // 
            this.columnMinAmount.Caption = "Минимальный остаток";
            this.columnMinAmount.FieldName = "ProductMinCount";
            this.columnMinAmount.Name = "columnMinAmount";
            this.columnMinAmount.OptionsColumn.AllowEdit = false;
            this.columnMinAmount.OptionsColumn.AllowFocus = false;
            this.columnMinAmount.OptionsColumn.ReadOnly = true;
            this.columnMinAmount.Visible = true;
            this.columnMinAmount.VisibleIndex = 3;
            this.columnMinAmount.Width = 89;
            // 
            // columnDelta
            // 
            this.columnDelta.Caption = "Дельта";
            this.columnDelta.FieldName = "ProductDelta";
            this.columnDelta.Name = "columnDelta";
            this.columnDelta.OptionsColumn.AllowEdit = false;
            this.columnDelta.OptionsColumn.AllowFocus = false;
            this.columnDelta.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.columnDelta.OptionsColumn.ReadOnly = true;
            this.columnDelta.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.columnDelta.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnDelta.Visible = true;
            this.columnDelta.VisibleIndex = 4;
            this.columnDelta.Width = 89;
            // 
            // columnMeasure
            // 
            this.columnMeasure.Caption = "Ед.Изм";
            this.columnMeasure.FieldName = "ProductMeasure";
            this.columnMeasure.Name = "columnMeasure";
            this.columnMeasure.OptionsColumn.AllowEdit = false;
            this.columnMeasure.OptionsColumn.AllowFocus = false;
            this.columnMeasure.OptionsColumn.ReadOnly = true;
            this.columnMeasure.Visible = true;
            this.columnMeasure.VisibleIndex = 5;
            this.columnMeasure.Width = 202;
            // 
            // columnPrice
            // 
            this.columnPrice.Caption = "Цена";
            this.columnPrice.FieldName = "Price";
            this.columnPrice.Name = "columnPrice";
            this.columnPrice.OptionsColumn.AllowEdit = false;
            this.columnPrice.OptionsColumn.AllowFocus = false;
            this.columnPrice.OptionsColumn.ReadOnly = true;
            this.columnPrice.Visible = true;
            this.columnPrice.VisibleIndex = 6;
            this.columnPrice.Width = 90;
            // 
            // columnSum
            // 
            this.columnSum.Caption = "Сумма";
            this.columnSum.DisplayFormat.FormatString = "F2";
            this.columnSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnSum.FieldName = "Sum";
            this.columnSum.Name = "columnSum";
            this.columnSum.OptionsColumn.AllowEdit = false;
            this.columnSum.OptionsColumn.AllowFocus = false;
            this.columnSum.OptionsColumn.ReadOnly = true;
            this.columnSum.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.columnSum.Visible = true;
            this.columnSum.VisibleIndex = 7;
            this.columnSum.Width = 194;
            // 
            // columnCategory
            // 
            this.columnCategory.Caption = "Категория";
            this.columnCategory.FieldName = "ProductCategory";
            this.columnCategory.Name = "columnCategory";
            this.columnCategory.Visible = true;
            this.columnCategory.VisibleIndex = 8;
            // 
            // columnMedian
            // 
            this.columnMedian.Caption = "Медиана";
            this.columnMedian.DisplayFormat.FormatString = "F2";
            this.columnMedian.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnMedian.FieldName = "Median";
            this.columnMedian.Name = "columnMedian";
            this.columnMedian.OptionsColumn.AllowEdit = false;
            this.columnMedian.OptionsColumn.ReadOnly = true;
            this.columnMedian.Visible = true;
            this.columnMedian.VisibleIndex = 9;
            // 
            // columnAveragePrice
            // 
            this.columnAveragePrice.Caption = "Средняя цена";
            this.columnAveragePrice.DisplayFormat.FormatString = "F2";
            this.columnAveragePrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnAveragePrice.FieldName = "AveragePrice";
            this.columnAveragePrice.Name = "columnAveragePrice";
            this.columnAveragePrice.OptionsColumn.AllowEdit = false;
            this.columnAveragePrice.OptionsColumn.ReadOnly = true;
            this.columnAveragePrice.Visible = true;
            this.columnAveragePrice.VisibleIndex = 10;
            // 
            // columnLastPrice
            // 
            this.columnLastPrice.Caption = "Последняя цена";
            this.columnLastPrice.DisplayFormat.FormatString = "F2";
            this.columnLastPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnLastPrice.FieldName = "LastPrice";
            this.columnLastPrice.Name = "columnLastPrice";
            this.columnLastPrice.OptionsColumn.AllowEdit = false;
            this.columnLastPrice.OptionsColumn.ReadOnly = true;
            this.columnLastPrice.Visible = true;
            this.columnLastPrice.VisibleIndex = 11;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Изменение цены";
            this.gridColumn1.DisplayFormat.FormatString = "F2";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn1.FieldName = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.UnboundExpression = "[LastPrice] - [AveragePrice]";
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 12;
            // 
            // columnMaxGap
            // 
            this.columnMaxGap.Caption = "Максимальное изменение цены";
            this.columnMaxGap.DisplayFormat.FormatString = "F1";
            this.columnMaxGap.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnMaxGap.FieldName = "MaxPriceGapPercantage";
            this.columnMaxGap.Name = "columnMaxGap";
            this.columnMaxGap.OptionsColumn.AllowEdit = false;
            this.columnMaxGap.OptionsColumn.ReadOnly = true;
            this.columnMaxGap.Visible = true;
            this.columnMaxGap.VisibleIndex = 13;
            // 
            // gridWeightedPrice
            // 
            this.gridWeightedPrice.Caption = "Взвешенная цена";
            this.gridWeightedPrice.DisplayFormat.FormatString = "F2";
            this.gridWeightedPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridWeightedPrice.FieldName = "WeightedPrice";
            this.gridWeightedPrice.Name = "gridWeightedPrice";
            this.gridWeightedPrice.ToolTip = "Цена с учетом поставок за этот месяц и предыдущее";
            this.gridWeightedPrice.Visible = true;
            this.gridWeightedPrice.VisibleIndex = 14;
            // 
            // lblPeriodEnd
            // 
            this.lblPeriodEnd.AutoSize = true;
            this.lblPeriodEnd.Location = new System.Drawing.Point(11, 10);
            this.lblPeriodEnd.Name = "lblPeriodEnd";
            this.lblPeriodEnd.Size = new System.Drawing.Size(36, 13);
            this.lblPeriodEnd.TabIndex = 15;
            this.lblPeriodEnd.Text = "Дата:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnGenerate.Location = new System.Drawing.Point(424, 5);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(143, 23);
            this.btnGenerate.TabIndex = 13;
            this.btnGenerate.Text = "Сгенерировать отчет";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // dTimeEnd
            // 
            this.dTimeEnd.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dTimeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTimeEnd.Location = new System.Drawing.Point(53, 6);
            this.dTimeEnd.Name = "dTimeEnd";
            this.dTimeEnd.Size = new System.Drawing.Size(150, 20);
            this.dTimeEnd.TabIndex = 11;
            // 
            // backgroundWorkerReportGenerator
            // 
            this.backgroundWorkerReportGenerator.WorkerReportsProgress = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // cmbWarehouse
            // 
            this.cmbWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWarehouse.FormattingEnabled = true;
            this.cmbWarehouse.Location = new System.Drawing.Point(252, 7);
            this.cmbWarehouse.Name = "cmbWarehouse";
            this.cmbWarehouse.Size = new System.Drawing.Size(166, 21);
            this.cmbWarehouse.TabIndex = 17;
            // 
            // lblWarehouse
            // 
            this.lblWarehouse.AutoSize = true;
            this.lblWarehouse.Location = new System.Drawing.Point(208, 12);
            this.lblWarehouse.Name = "lblWarehouse";
            this.lblWarehouse.Size = new System.Drawing.Size(38, 13);
            this.lblWarehouse.TabIndex = 18;
            this.lblWarehouse.Text = "Склад";
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(1226, 5);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 23);
            this.btnPrint.TabIndex = 24;
            this.btnPrint.Text = "Печать";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // printingSystem
            // 
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.mainGridControl;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            this.printableComponentLink.CreateReportHeaderArea += new DevExpress.XtraPrinting.CreateAreaEventHandler(this.printableComponentLink_CreateReportHeaderArea);
            // 
            // RemainsOfGoodReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1338, 595);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.lblWarehouse);
            this.Controls.Add(this.cmbWarehouse);
            this.Controls.Add(this.mainGridControl);
            this.Controls.Add(this.lblPeriodEnd);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.dTimeEnd);
            this.Name = "RemainsOfGoodReport";
            this.Text = "Остатки товаров";
            this.Load += new System.EventHandler(this.RemainsOfGoodReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraGrid.GridControl mainGridControl;
        public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
        private DevExpress.XtraGrid.Columns.GridColumn columnId;
        private DevExpress.XtraGrid.Columns.GridColumn columnName;
        private DevExpress.XtraGrid.Columns.GridColumn columnAmount;
        private System.Windows.Forms.Label lblPeriodEnd;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.DateTimePicker dTimeEnd;
        private System.ComponentModel.BackgroundWorker backgroundWorkerReportGenerator;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private DevExpress.XtraGrid.Columns.GridColumn columnMinAmount;
        private DevExpress.XtraGrid.Columns.GridColumn columnDelta;
        private DevExpress.XtraGrid.Columns.GridColumn columnMeasure;
        private System.Windows.Forms.ComboBox cmbWarehouse;
        private System.Windows.Forms.Label lblWarehouse;
        private System.Windows.Forms.Button btnPrint;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;
        private DevExpress.XtraGrid.Columns.GridColumn columnPrice;
        private DevExpress.XtraGrid.Columns.GridColumn columnSum;
        private DevExpress.XtraGrid.Columns.GridColumn columnCategory;
        private DevExpress.XtraGrid.Columns.GridColumn columnAveragePrice;
        private DevExpress.XtraGrid.Columns.GridColumn columnLastPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn columnMedian;
        private DevExpress.XtraGrid.Columns.GridColumn columnMaxGap;
        private DevExpress.XtraGrid.Columns.GridColumn gridWeightedPrice;

    }
}