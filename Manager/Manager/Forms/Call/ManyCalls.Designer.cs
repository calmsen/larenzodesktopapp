﻿namespace LaRenzo.Forms.Call
{
	partial class ManyCalls
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManyCalls));
			this.manyCallsGridControl = new DevExpress.XtraGrid.GridControl();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.delButton = new System.Windows.Forms.Button();
			this.button14 = new System.Windows.Forms.Button();
			this.button13 = new System.Windows.Forms.Button();
			this.button12 = new System.Windows.Forms.Button();
			this.button11 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.callButton = new System.Windows.Forms.Button();
			this.numberTextBox = new System.Windows.Forms.TextBox();
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.offPanel = new System.Windows.Forms.Panel();
			this.onPanel = new System.Windows.Forms.Panel();
			this.testCallTimer = new System.Windows.Forms.Timer(this.components);
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.nowCallsGridControl = new DevExpress.XtraGrid.GridControl();
			this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			((System.ComponentModel.ISupportInitialize)(this.manyCallsGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nowCallsGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
			this.tabPage1.SuspendLayout();
			this.SuspendLayout();
			// 
			// manyCallsGridControl
			// 
			this.manyCallsGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.manyCallsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.manyCallsGridControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.manyCallsGridControl.Location = new System.Drawing.Point(3, 3);
			this.manyCallsGridControl.MainView = this.gridView1;
			this.manyCallsGridControl.Name = "manyCallsGridControl";
			this.manyCallsGridControl.Size = new System.Drawing.Size(896, 309);
			this.manyCallsGridControl.TabIndex = 0;
			this.manyCallsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn1,
            this.gridColumn7,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn4});
			this.gridView1.GridControl = this.manyCallsGridControl;
			this.gridView1.Name = "gridView1";
			this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
			// 
			// gridColumn15
			// 
			this.gridColumn15.Caption = "Id";
			this.gridColumn15.FieldName = "ID";
			this.gridColumn15.Name = "gridColumn15";
			this.gridColumn15.Visible = true;
			this.gridColumn15.VisibleIndex = 3;
			// 
			// gridColumn1
			// 
			this.gridColumn1.Caption = "Клиент ";
			this.gridColumn1.FieldName = "Name";
			this.gridColumn1.Name = "gridColumn1";
			this.gridColumn1.OptionsColumn.AllowEdit = false;
			this.gridColumn1.OptionsColumn.AllowFocus = false;
			this.gridColumn1.OptionsColumn.ReadOnly = true;
			this.gridColumn1.Visible = true;
			this.gridColumn1.VisibleIndex = 0;
			// 
			// gridColumn7
			// 
			this.gridColumn7.Caption = "Телефон";
			this.gridColumn7.FieldName = "PhoneNumber";
			this.gridColumn7.Name = "gridColumn7";
			this.gridColumn7.OptionsColumn.AllowEdit = false;
			this.gridColumn7.OptionsColumn.AllowFocus = false;
			this.gridColumn7.OptionsColumn.ReadOnly = true;
			this.gridColumn7.Visible = true;
			this.gridColumn7.VisibleIndex = 1;
			// 
			// gridColumn2
			// 
			this.gridColumn2.Caption = "Статус";
			this.gridColumn2.FieldName = "StatusName";
			this.gridColumn2.Name = "gridColumn2";
			this.gridColumn2.OptionsColumn.AllowEdit = false;
			this.gridColumn2.OptionsColumn.AllowFocus = false;
			this.gridColumn2.OptionsColumn.ReadOnly = true;
			this.gridColumn2.Visible = true;
			this.gridColumn2.VisibleIndex = 2;
			// 
			// gridColumn3
			// 
			this.gridColumn3.Caption = "Время звонка";
			this.gridColumn3.DisplayFormat.FormatString = "yyyy.MM.dd HH:mm";
			this.gridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.gridColumn3.FieldName = "Created";
			this.gridColumn3.Name = "gridColumn3";
			this.gridColumn3.OptionsColumn.AllowEdit = false;
			this.gridColumn3.OptionsColumn.AllowFocus = false;
			this.gridColumn3.OptionsColumn.ReadOnly = true;
			this.gridColumn3.Visible = true;
			this.gridColumn3.VisibleIndex = 4;
			// 
			// gridColumn5
			// 
			this.gridColumn5.Caption = "Оператор";
			this.gridColumn5.FieldName = "User.Name";
			this.gridColumn5.Name = "gridColumn5";
			this.gridColumn5.OptionsColumn.AllowEdit = false;
			this.gridColumn5.OptionsColumn.AllowFocus = false;
			this.gridColumn5.OptionsColumn.ReadOnly = true;
			this.gridColumn5.Visible = true;
			this.gridColumn5.VisibleIndex = 5;
			// 
			// gridColumn6
			// 
			this.gridColumn6.Caption = "Является VIP";
			this.gridColumn6.FieldName = "IsVip";
			this.gridColumn6.Name = "gridColumn6";
			this.gridColumn6.OptionsColumn.AllowEdit = false;
			this.gridColumn6.OptionsColumn.AllowFocus = false;
			this.gridColumn6.OptionsColumn.ReadOnly = true;
			this.gridColumn6.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
			this.gridColumn6.Visible = true;
			this.gridColumn6.VisibleIndex = 6;
			// 
			// gridColumn4
			// 
			this.gridColumn4.Caption = "Действие";
			this.gridColumn4.Name = "gridColumn4";
			this.gridColumn4.OptionsColumn.AllowEdit = false;
			this.gridColumn4.OptionsColumn.AllowFocus = false;
			this.gridColumn4.OptionsColumn.ReadOnly = true;
			this.gridColumn4.Visible = true;
			this.gridColumn4.VisibleIndex = 7;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.delButton);
			this.groupBox1.Controls.Add(this.button14);
			this.groupBox1.Controls.Add(this.button13);
			this.groupBox1.Controls.Add(this.button12);
			this.groupBox1.Controls.Add(this.button11);
			this.groupBox1.Controls.Add(this.button10);
			this.groupBox1.Controls.Add(this.button9);
			this.groupBox1.Controls.Add(this.button8);
			this.groupBox1.Controls.Add(this.button7);
			this.groupBox1.Controls.Add(this.button6);
			this.groupBox1.Controls.Add(this.button5);
			this.groupBox1.Controls.Add(this.button4);
			this.groupBox1.Controls.Add(this.button3);
			this.groupBox1.Controls.Add(this.button2);
			this.groupBox1.Controls.Add(this.callButton);
			this.groupBox1.Controls.Add(this.numberTextBox);
			this.groupBox1.Location = new System.Drawing.Point(928, 6);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(214, 347);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Софтофон";
			// 
			// delButton
			// 
			this.delButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.delButton.Location = new System.Drawing.Point(155, 19);
			this.delButton.Name = "delButton";
			this.delButton.Size = new System.Drawing.Size(59, 33);
			this.delButton.TabIndex = 15;
			this.delButton.Text = "<";
			this.delButton.UseVisualStyleBackColor = true;
			this.delButton.Click += new System.EventHandler(this.delButton_Click);
			// 
			// button14
			// 
			this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button14.Location = new System.Drawing.Point(82, 224);
			this.button14.Name = "button14";
			this.button14.Size = new System.Drawing.Size(60, 35);
			this.button14.TabIndex = 14;
			this.button14.Text = "+";
			this.button14.UseVisualStyleBackColor = true;
			this.button14.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button13
			// 
			this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button13.Location = new System.Drawing.Point(148, 200);
			this.button13.Name = "button13";
			this.button13.Size = new System.Drawing.Size(60, 35);
			this.button13.TabIndex = 13;
			this.button13.Text = "#";
			this.button13.UseVisualStyleBackColor = true;
			this.button13.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button12
			// 
			this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button12.Location = new System.Drawing.Point(16, 200);
			this.button12.Name = "button12";
			this.button12.Size = new System.Drawing.Size(60, 35);
			this.button12.TabIndex = 12;
			this.button12.Text = "*";
			this.button12.UseVisualStyleBackColor = true;
			this.button12.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button11
			// 
			this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button11.Location = new System.Drawing.Point(82, 183);
			this.button11.Name = "button11";
			this.button11.Size = new System.Drawing.Size(60, 35);
			this.button11.TabIndex = 11;
			this.button11.Text = "0";
			this.button11.UseVisualStyleBackColor = true;
			this.button11.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button10
			// 
			this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button10.Location = new System.Drawing.Point(154, 142);
			this.button10.Name = "button10";
			this.button10.Size = new System.Drawing.Size(60, 35);
			this.button10.TabIndex = 10;
			this.button10.Text = "9";
			this.button10.UseVisualStyleBackColor = true;
			this.button10.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button9
			// 
			this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button9.Location = new System.Drawing.Point(82, 142);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(60, 35);
			this.button9.TabIndex = 9;
			this.button9.Text = "8";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button8
			// 
			this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button8.Location = new System.Drawing.Point(6, 142);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(60, 35);
			this.button8.TabIndex = 8;
			this.button8.Text = "7";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button7
			// 
			this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button7.Location = new System.Drawing.Point(154, 103);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(60, 35);
			this.button7.TabIndex = 7;
			this.button7.Text = "6";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button6
			// 
			this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button6.Location = new System.Drawing.Point(82, 103);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(60, 35);
			this.button6.TabIndex = 6;
			this.button6.Text = "5";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button5
			// 
			this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button5.Location = new System.Drawing.Point(6, 103);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(60, 35);
			this.button5.TabIndex = 5;
			this.button5.Text = "4";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button4
			// 
			this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button4.Location = new System.Drawing.Point(154, 64);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(60, 35);
			this.button4.TabIndex = 4;
			this.button4.Text = "3";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button3
			// 
			this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button3.Location = new System.Drawing.Point(82, 64);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(60, 35);
			this.button3.TabIndex = 3;
			this.button3.Text = "2";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// button2
			// 
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button2.Location = new System.Drawing.Point(6, 64);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(60, 35);
			this.button2.TabIndex = 2;
			this.button2.Text = "1";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.DigitButtonClick);
			// 
			// callButton
			// 
			this.callButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("callButton.BackgroundImage")));
			this.callButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.callButton.Location = new System.Drawing.Point(75, 265);
			this.callButton.Name = "callButton";
			this.callButton.Size = new System.Drawing.Size(74, 71);
			this.callButton.TabIndex = 1;
			this.callButton.UseVisualStyleBackColor = true;
			// 
			// numberTextBox
			// 
			this.numberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.numberTextBox.Location = new System.Drawing.Point(6, 19);
			this.numberTextBox.Multiline = true;
			this.numberTextBox.Name = "numberTextBox";
			this.numberTextBox.Size = new System.Drawing.Size(143, 33);
			this.numberTextBox.TabIndex = 0;
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "notifyIcon1";
			this.notifyIcon1.Visible = true;
			// 
			// offPanel
			// 
			this.offPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("offPanel.BackgroundImage")));
			this.offPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.offPanel.Location = new System.Drawing.Point(548, 190);
			this.offPanel.Name = "offPanel";
			this.offPanel.Size = new System.Drawing.Size(48, 46);
			this.offPanel.TabIndex = 7;
			this.offPanel.Visible = false;
			// 
			// onPanel
			// 
			this.onPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("onPanel.BackgroundImage")));
			this.onPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.onPanel.Location = new System.Drawing.Point(548, 119);
			this.onPanel.Name = "onPanel";
			this.onPanel.Size = new System.Drawing.Size(48, 51);
			this.onPanel.TabIndex = 6;
			this.onPanel.Visible = false;
			// 
			// testCallTimer
			// 
			this.testCallTimer.Enabled = true;
			this.testCallTimer.Interval = 1500;
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Location = new System.Drawing.Point(12, 12);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(910, 341);
			this.tabControl1.TabIndex = 8;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.nowCallsGridControl);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(902, 315);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Текущие звонки";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// nowCallsGridControl
			// 
			this.nowCallsGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.nowCallsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.nowCallsGridControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.nowCallsGridControl.Location = new System.Drawing.Point(3, 3);
			this.nowCallsGridControl.MainView = this.gridView2;
			this.nowCallsGridControl.Name = "nowCallsGridControl";
			this.nowCallsGridControl.Size = new System.Drawing.Size(896, 309);
			this.nowCallsGridControl.TabIndex = 1;
			this.nowCallsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
			// 
			// gridView2
			// 
			this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14});
			this.gridView2.GridControl = this.nowCallsGridControl;
			this.gridView2.Name = "gridView2";
			this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
			// 
			// gridColumn8
			// 
			this.gridColumn8.Caption = "Клиент ";
			this.gridColumn8.FieldName = "Name";
			this.gridColumn8.Name = "gridColumn8";
			this.gridColumn8.Visible = true;
			this.gridColumn8.VisibleIndex = 0;
			// 
			// gridColumn9
			// 
			this.gridColumn9.Caption = "Телефон";
			this.gridColumn9.FieldName = "PhoneNumber";
			this.gridColumn9.Name = "gridColumn9";
			this.gridColumn9.Visible = true;
			this.gridColumn9.VisibleIndex = 1;
			// 
			// gridColumn10
			// 
			this.gridColumn10.Caption = "Статус";
			this.gridColumn10.FieldName = "StatusName";
			this.gridColumn10.Name = "gridColumn10";
			this.gridColumn10.Visible = true;
			this.gridColumn10.VisibleIndex = 2;
			// 
			// gridColumn11
			// 
			this.gridColumn11.Caption = "Время звонка";
			this.gridColumn11.DisplayFormat.FormatString = "HH:mm";
			this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.gridColumn11.FieldName = "Created";
			this.gridColumn11.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
			this.gridColumn11.Name = "gridColumn11";
			this.gridColumn11.Visible = true;
			this.gridColumn11.VisibleIndex = 3;
			// 
			// gridColumn12
			// 
			this.gridColumn12.Caption = "Оператор";
			this.gridColumn12.FieldName = "User.Name";
			this.gridColumn12.Name = "gridColumn12";
			this.gridColumn12.Visible = true;
			this.gridColumn12.VisibleIndex = 4;
			// 
			// gridColumn13
			// 
			this.gridColumn13.Caption = "Является VIP";
			this.gridColumn13.FieldName = "IsVip";
			this.gridColumn13.Name = "gridColumn13";
			this.gridColumn13.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
			this.gridColumn13.Visible = true;
			this.gridColumn13.VisibleIndex = 5;
			// 
			// gridColumn14
			// 
			this.gridColumn14.Caption = "Действие";
			this.gridColumn14.Name = "gridColumn14";
			this.gridColumn14.Visible = true;
			this.gridColumn14.VisibleIndex = 6;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.manyCallsGridControl);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(902, 315);
			this.tabPage1.TabIndex = 2;
			this.tabPage1.Text = "История звонков";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// ManyCalls
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1145, 354);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.offPanel);
			this.Controls.Add(this.onPanel);
			this.Controls.Add(this.groupBox1);
			this.Name = "ManyCalls";
			this.Text = "Мереждер звонков";
			((System.ComponentModel.ISupportInitialize)(this.manyCallsGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.nowCallsGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
			this.tabPage1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraGrid.GridControl manyCallsGridControl;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button button14;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.Button button12;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button callButton;
		private System.Windows.Forms.TextBox numberTextBox;
		private System.Windows.Forms.Button delButton;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.Panel offPanel;
		private System.Windows.Forms.Panel onPanel;
		private System.Windows.Forms.Timer testCallTimer;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage1;
		private DevExpress.XtraGrid.GridControl nowCallsGridControl;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
	}
}