﻿namespace LaRenzo.Forms.Call
{
	partial class InterCall
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.clientNameLabel = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.clientPhoneLabel = new System.Windows.Forms.Label();
			this.clientStatusLabel = new System.Windows.Forms.Label();
			this.mainGridControl = new DevExpress.XtraGrid.GridControl();
			this.mainGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.columnIdCol = new DevExpress.XtraGrid.Columns.GridColumn();
			this.brandColumn = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnName = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnTimeCreating = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnState = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnTimeClient = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnDoDelivery = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnisWindow = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnDriver = new DevExpress.XtraGrid.Columns.GridColumn();
			this.columnUser = new DevExpress.XtraGrid.Columns.GridColumn();
			this.oldGridControl = new DevExpress.XtraGrid.GridControl();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.answerButton = new System.Windows.Forms.Button();
			this.oldNamesTextBox = new System.Windows.Forms.TextBox();
			this.blackLabel = new System.Windows.Forms.Label();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.oldGridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.ForeColor = System.Drawing.Color.Gray;
			this.label1.Location = new System.Drawing.Point(32, 10);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(87, 25);
			this.label1.TabIndex = 0;
			this.label1.Text = "Клиент:";
			// 
			// clientNameLabel
			// 
			this.clientNameLabel.AutoSize = true;
			this.clientNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.clientNameLabel.Location = new System.Drawing.Point(125, 6);
			this.clientNameLabel.Name = "clientNameLabel";
			this.clientNameLabel.Size = new System.Drawing.Size(195, 29);
			this.clientNameLabel.TabIndex = 1;
			this.clientNameLabel.Text = "clientNameLabel";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.ForeColor = System.Drawing.Color.Gray;
			this.label3.Location = new System.Drawing.Point(382, 5);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(105, 25);
			this.label3.TabIndex = 2;
			this.label3.Text = "Телефон:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.ForeColor = System.Drawing.Color.Gray;
			this.label4.Location = new System.Drawing.Point(403, 46);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(84, 25);
			this.label4.TabIndex = 3;
			this.label4.Text = "Статус:";
			// 
			// clientPhoneLabel
			// 
			this.clientPhoneLabel.AutoSize = true;
			this.clientPhoneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.clientPhoneLabel.Location = new System.Drawing.Point(493, 2);
			this.clientPhoneLabel.Name = "clientPhoneLabel";
			this.clientPhoneLabel.Size = new System.Drawing.Size(200, 29);
			this.clientPhoneLabel.TabIndex = 4;
			this.clientPhoneLabel.Text = "clientPhoneLabel";
			// 
			// clientStatusLabel
			// 
			this.clientStatusLabel.AutoSize = true;
			this.clientStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.clientStatusLabel.Location = new System.Drawing.Point(493, 46);
			this.clientStatusLabel.Name = "clientStatusLabel";
			this.clientStatusLabel.Size = new System.Drawing.Size(196, 29);
			this.clientStatusLabel.TabIndex = 5;
			this.clientStatusLabel.Text = "clientStatusLabel";
			// 
			// mainGridControl
			// 
			this.mainGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.mainGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainGridControl.Location = new System.Drawing.Point(3, 3);
			this.mainGridControl.MainView = this.mainGridView;
			this.mainGridControl.Name = "mainGridControl";
			this.mainGridControl.Size = new System.Drawing.Size(742, 143);
			this.mainGridControl.TabIndex = 8;
			this.mainGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.mainGridView});
			// 
			// mainGridView
			// 
			this.mainGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.columnIdCol,
            this.brandColumn,
            this.columnName,
            this.columnTimeCreating,
            this.columnState,
            this.columnTimeClient,
            this.columnDoDelivery,
            this.columnisWindow,
            this.columnDriver,
            this.columnUser});
			this.mainGridView.GridControl = this.mainGridControl;
			this.mainGridView.Name = "mainGridView";
			this.mainGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
			this.mainGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
			this.mainGridView.OptionsSelection.EnableAppearanceFocusedRow = false;
			this.mainGridView.OptionsView.BestFitMaxRowCount = 10;
			this.mainGridView.OptionsView.ShowFooter = true;
			this.mainGridView.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.mainGridView_RowCellStyle);
			// 
			// columnIdCol
			// 
			this.columnIdCol.Caption = "Ид";
			this.columnIdCol.FieldName = "ID";
			this.columnIdCol.Name = "columnIdCol";
			this.columnIdCol.OptionsColumn.AllowEdit = false;
			this.columnIdCol.OptionsColumn.AllowFocus = false;
			this.columnIdCol.OptionsColumn.ReadOnly = true;
			// 
			// brandColumn
			// 
			this.brandColumn.Caption = "Бренд";
			this.brandColumn.FieldName = "Brand.Name";
			this.brandColumn.Name = "brandColumn";
			this.brandColumn.Visible = true;
			this.brandColumn.VisibleIndex = 0;
			this.brandColumn.Width = 43;
			// 
			// columnName
			// 
			this.columnName.Caption = "Имя";
			this.columnName.FieldName = "Name";
			this.columnName.Name = "columnName";
			this.columnName.OptionsColumn.AllowEdit = false;
			this.columnName.OptionsColumn.AllowFocus = false;
			this.columnName.OptionsColumn.ReadOnly = true;
			this.columnName.Visible = true;
			this.columnName.VisibleIndex = 1;
			this.columnName.Width = 39;
			// 
			// columnTimeCreating
			// 
			this.columnTimeCreating.Caption = "Время создания";
			this.columnTimeCreating.DisplayFormat.FormatString = "HH:mm";
			this.columnTimeCreating.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.columnTimeCreating.FieldName = "Created";
			this.columnTimeCreating.Name = "columnTimeCreating";
			this.columnTimeCreating.OptionsColumn.AllowEdit = false;
			this.columnTimeCreating.OptionsColumn.AllowFocus = false;
			this.columnTimeCreating.OptionsColumn.ReadOnly = true;
			this.columnTimeCreating.Visible = true;
			this.columnTimeCreating.VisibleIndex = 3;
			this.columnTimeCreating.Width = 39;
			// 
			// columnState
			// 
			this.columnState.Caption = "Состояние заказа";
			this.columnState.FieldName = "State.Name";
			this.columnState.Name = "columnState";
			this.columnState.OptionsColumn.AllowEdit = false;
			this.columnState.OptionsColumn.AllowFocus = false;
			this.columnState.OptionsColumn.ReadOnly = true;
			this.columnState.Visible = true;
			this.columnState.VisibleIndex = 2;
			this.columnState.Width = 39;
			// 
			// columnTimeClient
			// 
			this.columnTimeClient.Caption = "Время клиент";
			this.columnTimeClient.DisplayFormat.FormatString = "HH:mm";
			this.columnTimeClient.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.columnTimeClient.FieldName = "TimeClient";
			this.columnTimeClient.Name = "columnTimeClient";
			this.columnTimeClient.OptionsColumn.AllowEdit = false;
			this.columnTimeClient.OptionsColumn.AllowFocus = false;
			this.columnTimeClient.OptionsColumn.ReadOnly = true;
			this.columnTimeClient.Visible = true;
			this.columnTimeClient.VisibleIndex = 4;
			this.columnTimeClient.Width = 39;
			// 
			// columnDoDelivery
			// 
			this.columnDoDelivery.Caption = "Доставка";
			this.columnDoDelivery.FieldName = "DoCalculateDelivery";
			this.columnDoDelivery.Name = "columnDoDelivery";
			this.columnDoDelivery.OptionsColumn.AllowEdit = false;
			this.columnDoDelivery.OptionsColumn.AllowFocus = false;
			this.columnDoDelivery.OptionsColumn.ReadOnly = true;
			this.columnDoDelivery.Visible = true;
			this.columnDoDelivery.VisibleIndex = 5;
			this.columnDoDelivery.Width = 39;
			// 
			// columnisWindow
			// 
			this.columnisWindow.Caption = "Окно";
			this.columnisWindow.FieldName = "IsWindow";
			this.columnisWindow.Name = "columnisWindow";
			this.columnisWindow.OptionsColumn.AllowEdit = false;
			this.columnisWindow.OptionsColumn.AllowFocus = false;
			this.columnisWindow.OptionsColumn.ReadOnly = true;
			this.columnisWindow.Visible = true;
			this.columnisWindow.VisibleIndex = 6;
			this.columnisWindow.Width = 39;
			// 
			// columnDriver
			// 
			this.columnDriver.Caption = "Водитель";
			this.columnDriver.FieldName = "Driver.Name";
			this.columnDriver.Name = "columnDriver";
			this.columnDriver.OptionsColumn.AllowEdit = false;
			this.columnDriver.OptionsColumn.AllowFocus = false;
			this.columnDriver.OptionsColumn.ReadOnly = true;
			this.columnDriver.Visible = true;
			this.columnDriver.VisibleIndex = 7;
			this.columnDriver.Width = 39;
			// 
			// columnUser
			// 
			this.columnUser.Caption = "Пользователь";
			this.columnUser.FieldName = "User";
			this.columnUser.Name = "columnUser";
			this.columnUser.OptionsColumn.AllowEdit = false;
			this.columnUser.OptionsColumn.ReadOnly = true;
			this.columnUser.Visible = true;
			this.columnUser.VisibleIndex = 8;
			this.columnUser.Width = 39;
			// 
			// oldGridControl
			// 
			this.oldGridControl.Cursor = System.Windows.Forms.Cursors.Default;
			this.oldGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.oldGridControl.Location = new System.Drawing.Point(3, 3);
			this.oldGridControl.MainView = this.gridView1;
			this.oldGridControl.Name = "oldGridControl";
			this.oldGridControl.Size = new System.Drawing.Size(742, 143);
			this.oldGridControl.TabIndex = 9;
			this.oldGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
			// 
			// gridView1
			// 
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn9,
            this.gridColumn11,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
			this.gridView1.GridControl = this.oldGridControl;
			this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TotalCost", this.gridColumn6, "{0} руб.", "1")});
			this.gridView1.Name = "gridView1";
			this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
			this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
			this.gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
			this.gridView1.OptionsView.BestFitMaxRowCount = 10;
			this.gridView1.OptionsView.ShowFooter = true;
			this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
			// 
			// gridColumn1
			// 
			this.gridColumn1.Caption = "Ид";
			this.gridColumn1.FieldName = "ID";
			this.gridColumn1.Name = "gridColumn1";
			this.gridColumn1.OptionsColumn.AllowEdit = false;
			this.gridColumn1.OptionsColumn.AllowFocus = false;
			this.gridColumn1.OptionsColumn.ReadOnly = true;
			// 
			// gridColumn2
			// 
			this.gridColumn2.Caption = "Бренд";
			this.gridColumn2.FieldName = "Brand.Name";
			this.gridColumn2.Name = "gridColumn2";
			this.gridColumn2.Visible = true;
			this.gridColumn2.VisibleIndex = 0;
			this.gridColumn2.Width = 43;
			// 
			// gridColumn6
			// 
			this.gridColumn6.Caption = "Сумма заказа";
			this.gridColumn6.FieldName = "TotalCost";
			this.gridColumn6.Name = "gridColumn6";
			this.gridColumn6.OptionsColumn.AllowEdit = false;
			this.gridColumn6.OptionsColumn.AllowFocus = false;
			this.gridColumn6.OptionsColumn.ReadOnly = true;
			this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "TotalCost", "{0} руб.", "3")});
			this.gridColumn6.Tag = "";
			this.gridColumn6.Visible = true;
			this.gridColumn6.VisibleIndex = 1;
			this.gridColumn6.Width = 39;
			// 
			// gridColumn9
			// 
			this.gridColumn9.Caption = "Состояние заказа";
			this.gridColumn9.FieldName = "State.Name";
			this.gridColumn9.Name = "gridColumn9";
			this.gridColumn9.OptionsColumn.AllowEdit = false;
			this.gridColumn9.OptionsColumn.AllowFocus = false;
			this.gridColumn9.OptionsColumn.ReadOnly = true;
			this.gridColumn9.Visible = true;
			this.gridColumn9.VisibleIndex = 2;
			this.gridColumn9.Width = 39;
			// 
			// gridColumn11
			// 
			this.gridColumn11.Caption = "Время клиент";
			this.gridColumn11.DisplayFormat.FormatString = "HH:mm";
			this.gridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.gridColumn11.FieldName = "TimeClient";
			this.gridColumn11.Name = "gridColumn11";
			this.gridColumn11.OptionsColumn.AllowEdit = false;
			this.gridColumn11.OptionsColumn.AllowFocus = false;
			this.gridColumn11.OptionsColumn.ReadOnly = true;
			this.gridColumn11.Visible = true;
			this.gridColumn11.VisibleIndex = 3;
			this.gridColumn11.Width = 39;
			// 
			// gridColumn15
			// 
			this.gridColumn15.Caption = "Доставка";
			this.gridColumn15.FieldName = "DoCalculateDelivery";
			this.gridColumn15.Name = "gridColumn15";
			this.gridColumn15.OptionsColumn.AllowEdit = false;
			this.gridColumn15.OptionsColumn.AllowFocus = false;
			this.gridColumn15.OptionsColumn.ReadOnly = true;
			this.gridColumn15.Visible = true;
			this.gridColumn15.VisibleIndex = 4;
			this.gridColumn15.Width = 39;
			// 
			// gridColumn16
			// 
			this.gridColumn16.Caption = "Окно";
			this.gridColumn16.FieldName = "IsWindow";
			this.gridColumn16.Name = "gridColumn16";
			this.gridColumn16.OptionsColumn.AllowEdit = false;
			this.gridColumn16.OptionsColumn.AllowFocus = false;
			this.gridColumn16.OptionsColumn.ReadOnly = true;
			this.gridColumn16.Visible = true;
			this.gridColumn16.VisibleIndex = 5;
			this.gridColumn16.Width = 39;
			// 
			// gridColumn17
			// 
			this.gridColumn17.Caption = "Водитель";
			this.gridColumn17.FieldName = "Driver.Name";
			this.gridColumn17.Name = "gridColumn17";
			this.gridColumn17.OptionsColumn.AllowEdit = false;
			this.gridColumn17.OptionsColumn.AllowFocus = false;
			this.gridColumn17.OptionsColumn.ReadOnly = true;
			this.gridColumn17.Visible = true;
			this.gridColumn17.VisibleIndex = 6;
			this.gridColumn17.Width = 39;
			// 
			// answerButton
			// 
			this.answerButton.BackColor = System.Drawing.Color.YellowGreen;
			this.answerButton.Cursor = System.Windows.Forms.Cursors.Default;
			this.answerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.answerButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.answerButton.Location = new System.Drawing.Point(25, 45);
			this.answerButton.Name = "answerButton";
			this.answerButton.Size = new System.Drawing.Size(265, 70);
			this.answerButton.TabIndex = 11;
			this.answerButton.Text = "Ответить";
			this.answerButton.UseVisualStyleBackColor = false;
			// 
			// oldNamesTextBox
			// 
			this.oldNamesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.oldNamesTextBox.Location = new System.Drawing.Point(102, 46);
			this.oldNamesTextBox.Multiline = true;
			this.oldNamesTextBox.Name = "oldNamesTextBox";
			this.oldNamesTextBox.ReadOnly = true;
			this.oldNamesTextBox.Size = new System.Drawing.Size(190, 70);
			this.oldNamesTextBox.TabIndex = 12;
			// 
			// blackLabel
			// 
			this.blackLabel.AutoSize = true;
			this.blackLabel.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.blackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.blackLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.blackLabel.Location = new System.Drawing.Point(401, 81);
			this.blackLabel.Name = "blackLabel";
			this.blackLabel.Size = new System.Drawing.Size(264, 39);
			this.blackLabel.TabIndex = 13;
			this.blackLabel.Text = "Чёрный клиент";
			this.blackLabel.Visible = false;
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Location = new System.Drawing.Point(-3, 123);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(756, 175);
			this.tabControl1.TabIndex = 14;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.mainGridControl);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(748, 149);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Текщие заказы";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.oldGridControl);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(748, 149);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "История заказов";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// timer1
			// 
			this.timer1.Interval = 1500;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// InterCall
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			this.ClientSize = new System.Drawing.Size(753, 301);
			this.Controls.Add(this.answerButton);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.blackLabel);
			this.Controls.Add(this.oldNamesTextBox);
			this.Controls.Add(this.clientStatusLabel);
			this.Controls.Add(this.clientPhoneLabel);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.clientNameLabel);
			this.Controls.Add(this.label1);
			this.Name = "InterCall";
			this.Text = "Входящий звонок";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InterCall_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.mainGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mainGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.oldGridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label clientNameLabel;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label clientPhoneLabel;
		private System.Windows.Forms.Label clientStatusLabel;
		public DevExpress.XtraGrid.GridControl mainGridControl;
		public DevExpress.XtraGrid.Views.Grid.GridView mainGridView;
		public DevExpress.XtraGrid.Columns.GridColumn columnIdCol;
		private DevExpress.XtraGrid.Columns.GridColumn brandColumn;
		public DevExpress.XtraGrid.Columns.GridColumn columnName;
		public DevExpress.XtraGrid.Columns.GridColumn columnTimeCreating;
		public DevExpress.XtraGrid.Columns.GridColumn columnState;
		public DevExpress.XtraGrid.Columns.GridColumn columnTimeClient;
		private DevExpress.XtraGrid.Columns.GridColumn columnDoDelivery;
		private DevExpress.XtraGrid.Columns.GridColumn columnisWindow;
		private DevExpress.XtraGrid.Columns.GridColumn columnDriver;
		private DevExpress.XtraGrid.Columns.GridColumn columnUser;
		public DevExpress.XtraGrid.GridControl oldGridControl;
		public DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		public DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
		public DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
		public DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
		public DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
		private System.Windows.Forms.Button answerButton;
		private System.Windows.Forms.TextBox oldNamesTextBox;
		private System.Windows.Forms.Label blackLabel;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Timer timer1;
	}
}