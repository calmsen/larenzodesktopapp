﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Others.DeliverySchedulies;
using LaRenzo.Properties;

namespace LaRenzo.Forms.DeliverySchedule
{
    public partial class MultiEditScheduleForm : Form
    {
        private readonly int _groupId;
        private readonly string[] _weekDays = new[]
            {"Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"};
        private readonly DataRepository.Entities.Others.DeliverySchedulies.DeliverySchedule _schedule;

        public MultiEditScheduleForm()
        {
            InitializeComponent();
        }

        public MultiEditScheduleForm(int groupId):this()
        {
            _groupId = groupId;
        }

        public MultiEditScheduleForm(LaRenzo.DataRepository.Entities.Others.DeliverySchedulies.DeliverySchedule schedule) : this()
        {
            _groupId = schedule.AddressGroupID ?? 0;
            _schedule = schedule;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tEditDeliveryBegin.Time > tEditDeliveryEnd.Time)
            {
                var ep = new ErrorProvider();
                ep.SetError(tEditDeliveryEnd, Resources.TimeBeginMatchMore);
                return;
            }

            var beginDelivery = new TimeSpan(tEditDeliveryBegin.Time.Hour, tEditDeliveryBegin.Time.Minute, tEditDeliveryBegin.Time.Second);
            var endDelivery = new TimeSpan(tEditDeliveryEnd.Time.Hour, tEditDeliveryEnd.Time.Minute, tEditDeliveryEnd.Time.Second);
            var waitTime = new TimeSpan(tEditWaiting.Time.Hour, tEditWaiting.Time.Minute, tEditWaiting.Time.Second);

            var poinOfSaleId = (int)comboBoxPointOfSale.SelectedValue;
            var priority = Convert.ToInt32(numPriority.Value);
            if (_schedule != null)
            {
                var listOfDay = new List<int> { _schedule.DayOfWeekID };
                Content.DeliveryScheduleManager.UpdateDeliverySchedule(_schedule.ID, listOfDay, beginDelivery, endDelivery, waitTime, _groupId, poinOfSaleId, priority);
                DialogResult = DialogResult.OK;
            }
            else
            {
                var listOfDay = 
                (from checkBox in flowLayoutPanel.Controls.OfType<CheckBox>().ToArray()
                 where checkBox.Checked
                 select Convert.ToInt32(checkBox.Tag))
                    .ToList();

                if (CheckDaySchedule(listOfDay, beginDelivery, endDelivery, poinOfSaleId))
                {
                    Content.DeliveryScheduleManager.AddDeliverySchedule(listOfDay, beginDelivery, endDelivery, waitTime, _groupId, poinOfSaleId, priority);

                    DialogResult = DialogResult.OK;
                }
            }
            
        }

        private bool CheckDaySchedule(List<int> listOfDay, TimeSpan deliveryBegin, TimeSpan deliveryEnd, int poinOfSaleId)
        {
            List<int> crossedDays = Content.DeliveryScheduleManager.CheckDeliverySchedule(listOfDay, deliveryBegin, deliveryEnd, _groupId, poinOfSaleId);

            if (crossedDays.Count != 0)
            {
                var str = new StringBuilder();
                foreach (var crossedDay in crossedDays)
                {
                    str.Append(string.Format("Для дня недели {0} обнаружено пересечеение по времени\n",
                                             _weekDays[crossedDay-1]));
                }

                tipError.ShowHint(str.ToString(), Resources.WarningTitle, pnlTop,
                                  ToolTipLocation.TopCenter);
                return false;
            }

            return true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void MultiEditScheduleForm_Load(object sender, EventArgs e)
        {
            var points = Content.PointOfSaleRepository.GetItems();
            comboBoxPointOfSale.DataSource = points;
            comboBoxPointOfSale.DisplayMember = "PointName";
            comboBoxPointOfSale.ValueMember = "Id";
            comboBoxPointOfSale.SelectedValue = points.FirstOrDefault()?.ID;
            if (_schedule != null)
            {
                tEditDeliveryBegin.Time = new DateTime(_schedule.DeliveryBegin.Ticks);
                tEditDeliveryEnd.Time = new DateTime(_schedule.DeliveryEnd.Ticks);
                tEditWaiting.Time = new DateTime(_schedule.DeliveryTime.Ticks);
                comboBoxPointOfSale.SelectedValue = _schedule.PointOfSaleId;
                numPriority.Value = _schedule.Priority;
                foreach(var dayOfWeek in flowLayoutPanel.Controls.OfType<CheckBox>())
                {
                    if (Convert.ToInt32(dayOfWeek.Tag) == _schedule.DayOfWeekID)
                    {
                        dayOfWeek.Checked = true;
                    }
                    dayOfWeek.Enabled = false;
                }
            }
        }
    }
}
