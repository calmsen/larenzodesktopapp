﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LaRenzo.ControlsLib.BindFilterListControl;
using LaRenzo.DataRepository.Entities;
using LaRenzo.DataRepository.Entities.Catalogs.Addresses;
using LaRenzo.DataRepository.Entities.Others.DeliverySchedulies;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Addresses;
using LaRenzo.DataRepository.Repositories.Others.DeliverySchedulies;
using LaRenzo.Forms.StaffForms;
using LaRenzo.Properties;

namespace LaRenzo.Forms.DeliverySchedule
{
    public partial class DeliveryScheduleForm : Form
    {
        private List<DataRepository.Entities.Others.DeliverySchedulies.DeliverySchedule> _scheduleList;

        public DeliveryScheduleForm()
        {
            InitializeComponent();
        }

        private void DeliverySchedule_Load(object sender, EventArgs e)
        {
            FillGroupDataSources();
            FillComboBoxPointOfSale();
        }

        private void FillComboBoxPointOfSale()
        {
            var points = new List<PointOfSale> { new PointOfSale { ID = 0, PointName = "Все точки продаж" } };
            var pointOfSales = Content.PointOfSaleRepository.GetItems();
            points.AddRange(pointOfSales);
            comboBoxPointOfSale.ComboBox.DataSource = points;
            comboBoxPointOfSale.ComboBox.DisplayMember = "PointName";
            comboBoxPointOfSale.ComboBox.ValueMember = "Id";
            comboBoxPointOfSale.ComboBox.SelectedValue = 0;
        }

        private List<AddressGroup> FilterAddressGroups(List<AddressGroup> addressGroupList)
        {
            int pointOfSaleId = ((comboBoxPointOfSale.ComboBox.SelectedValue as int?) ?? 0);
            if (pointOfSaleId != 0)
            {
                var scheduleList = Content.DeliveryScheduleManager.GetScheduleList();
                var addressGroupIDs = scheduleList
                    .Where(x => x.PointOfSaleId == pointOfSaleId)
                    .Select(x => x.AddressGroupID)
                    .ToList();
                addressGroupList = addressGroupList.Where(x => addressGroupIDs.Contains(x.ID)).ToList();
            }
            return addressGroupList;
        }

        public void FillGroupDataSources()
        {
            var addressGroupList = Content.DeliveryScheduleManager.GetAddressGroupList();
            addressGroupList = FilterAddressGroups(addressGroupList);
            GroupList.DataSource = addressGroupList;
            GroupList.DisplayMember = "Name";
        }

        public void FillAddressDataSources(int groupId)
        {
            var addresses = Content.AddressManager.GetAddressList(groupId);
            bFilterAddresses.AddRange(addresses.Select(x => new BindFilterList.BindListRow(x.ID, x.Name)).ToList());
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var addGroupDialog = new SimpleTextDialogForm
                {
                    FieldName = Resources.AddressGroupFieldText,
                    Text = Resources.AddAddressGroupDialogText
                };

            if (DialogResult.OK == addGroupDialog.ShowDialog(this))
            {
                Content.DeliveryScheduleManager.AddAddressGroup(new AddressGroup { Name = addGroupDialog.Result });
                FillGroupDataSources();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            FillGroupDataSources();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            var selectedItem = GroupList.SelectedItem as AddressGroup;
            if (selectedItem == null)
            {
                MessageBox.Show(Resources.AddressGroupNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                return;
            }

            var result = MessageBox.Show(Resources.DeleteAddressGroupQuestion, Resources.WarningTitle, MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                Content.DeliveryScheduleManager.DeleteAddressGroup(selectedItem.ID);
            }
            FillGroupDataSources();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

            var selectedItem = GroupList.SelectedItem as AddressGroup;
            if (selectedItem != null)
            {

                var editGroupDialog = new SimpleTextDialogForm
                {
                    FieldName = Resources.AddressGroupFieldText,
                    Text = Resources.AddAddressGroupDialogText,
                    Input = selectedItem.Name
                };

                if (DialogResult.OK == editGroupDialog.ShowDialog(this))
                {
                    Content.DeliveryScheduleManager.UpdateAddressGroup(selectedItem.ID, editGroupDialog.Result);
                }

                FillGroupDataSources();
            }
            else
            {
                MessageBox.Show(Resources.AddressGroupNotSelected, Resources.WarningTitle, MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }

        private void btnAddAddress_Click(object sender, EventArgs e)
        {
            var addAddressDialog = new SimpleComboBoxDialogForm
            {
                FieldName = Resources.AddressFieldText,
                Text = Resources.AddAddressDialogText
            };

            if (DialogResult.OK == addAddressDialog.ShowDialog(this))
            {
                var selectedItem = GroupList.SelectedItem as AddressGroup;
                if (selectedItem != null)
                {
                    Content.AddressManager.BindToGroup(addAddressDialog.Result, selectedItem.ID);
                    FillAddressDataSources(selectedItem.ID);
                }
            }
        }

        private void SetCommandEnable(bool isEnable)
        {
            btnAddAddress.Enabled = isEnable;
            btndeleteAddress.Enabled = isEnable;
            btnAddSchedule.Enabled = isEnable;
            btnDeleteSchedule.Enabled = isEnable;
        }

        private void btnDeleteAddress_Click(object sender, EventArgs e)
        {
            var selectedItem = bFilterAddresses.GetSelected();
            if (selectedItem != null)
            {
                if (DialogResult.OK ==
                    MessageBox.Show(Resources.DeleteAddressMessage, Resources.WarningTitle, MessageBoxButtons.OKCancel,
                                    MessageBoxIcon.Question))
                {
                    Content.AddressManager.ClearGroupBind(selectedItem.ID);
                    var selectedGroup = GroupList.SelectedItem as AddressGroup;
                    if (selectedGroup != null)
                    {
                        FillAddressDataSources(selectedGroup.ID);
                    }
                }
            }
        }

        private void btnRefreshAddress_Click(object sender, EventArgs e)
        {
            var selectedGroup = GroupList.SelectedItem as AddressGroup;
            if (selectedGroup != null)
            {
                FillAddressDataSources(selectedGroup.ID);
            }
        }

        private void Schedule_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(ea.Location);
            if (info.InRow || info.InRowCell)
            {
                var columnId = view.Columns.ColumnByFieldName("ID");
                var id = (int)view.GetRowCellValue(info.RowHandle, columnId);
                var schedule = _scheduleList.FirstOrDefault(x => x.ID == id);
                if (schedule == null)
                    return;
                var scheduleForm = new MultiEditScheduleForm(schedule);
                if (scheduleForm.ShowDialog() == DialogResult.OK)
                {
                    FillSchedule(schedule.AddressGroupID ?? 0);
                }
            }
        }

        private void FillSchedule(int id)
        {
            _scheduleList = Content.DeliveryScheduleManager.GetScheduleList();
            if (_scheduleList.Count == 0 )
                return;
            int pointOfSaleId = ((comboBoxPointOfSale.ComboBox.SelectedValue as int?) ?? 0);
            if (pointOfSaleId != 0)
                _scheduleList = _scheduleList.Where(x => x.PointOfSaleId == pointOfSaleId).ToList();
            foreach (TabPage page in tabMain.TabPages)
            {
                var grid = page.Controls.OfType<GridControl>().FirstOrDefault();
                grid.MainView.DoubleClick -= Schedule_DoubleClick;
                grid.MainView.DoubleClick += Schedule_DoubleClick;
                if (grid != null)
                {
                    grid.DataSource = (from schedule in _scheduleList
                                       where
                                           (schedule.DayOfWeekID == Convert.ToInt32(grid.Tag) &&
                                            schedule.AddressGroupID == id)
                                       select schedule).ToList();
                    //scheduleList.Where(
                    //    x =>
                    //    x.DayOfWeekID == Convert.ToInt32(grid.Tag) &&
                    //    x.AddressGroupID == id).ToList();
                }
            }
        }

        private void btnAddSchedule_Click(object sender, EventArgs e)
        {
            var selectedGroup = GroupList.SelectedItem as AddressGroup;
            if (selectedGroup != null)
            {
                var scheduleForm = new MultiEditScheduleForm(selectedGroup.ID);
                if (scheduleForm.ShowDialog() == DialogResult.OK)
                {
                    FillSchedule(selectedGroup.ID);
                }       
            }
        }

        private void btnRefreshSchedule_Click(object sender, EventArgs e)
        {
            var selected = GroupList.SelectedItem as AddressGroup;
            if (selected != null)
            {
                FillSchedule(selected.ID);
            }    
        }

        private void GroupListFilter_TextChanged(object sender, EventArgs e)
        {
            var addressGroupList = Content.DeliveryScheduleManager.GetAddressGroupList();
            addressGroupList = GroupListFilter.Text.Any()
                                       ? addressGroupList
                                                                .Where(
                                                                    x =>
                                                                    x.Name.ToUpper()
                                                                     .Contains(GroupListFilter.Text.ToUpper()))
                                                                .ToList()
                                       : addressGroupList;
            addressGroupList = FilterAddressGroups(addressGroupList);
            GroupList.DataSource = addressGroupList;
            
            if (GroupList.SelectedItem == null)
            {
                SetCommandEnable(false);
                bFilterAddresses.Clear();

                foreach (TabPage page in tabMain.TabPages)
                {
                    var grid = page.Controls.OfType<GridControl>().FirstOrDefault();
                    if (grid != null)
                    {
                        grid.DataSource = null;
                    }
                }
            }
        }

        private void GroupList_SelectedValueChanged(object sender, EventArgs e)
        {
            var selectedItem = GroupList.SelectedItem as AddressGroup;
            if (selectedItem != null)
            {
                FillAddressDataSources(selectedItem.ID);
                FillSchedule(selectedItem.ID);
                SetCommandEnable(true);
            }
        }

        private void btnDeleteSchedule_Click(object sender, EventArgs e)
        {
            var selectView = tabMain.SelectedTab.Controls.OfType<GridControl>().FirstOrDefault();
            
            if (selectView != null)
            {
                var view = (selectView.FocusedView as GridView);

                if (view != null && view.IsDataRow(view.FocusedRowHandle))
                {
                    var columnId = view.Columns.ColumnByFieldName("ID");
                    var id = (int)view.GetRowCellValue(view.FocusedRowHandle, columnId);
                    Content.DeliveryScheduleManager.DeleteDeliverySchedule(id);
                }
            }

            var selectedItem = GroupList.SelectedItem as AddressGroup;
            if (selectedItem != null)
            {
                FillSchedule(selectedItem.ID);
            }
        }

        private void comboBoxPointOfSale_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillGroupDataSources();
        }
    }
}
