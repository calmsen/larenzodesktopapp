﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Finance;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Catalogs.Users;
using LaRenzo.DataRepository.Repositories.Finance;

namespace LaRenzo.Forms.Finance
{
    public partial class NewPaymentForm : Form
    {
        public NewPaymentForm()
        {
            InitializeComponent();
        }

        private void NewPaymentForm_Load(object sender, EventArgs e)
        {
            listBox1.DataSource = Content.WalletRepository.GetItems();
            listBox1.DisplayMember = "Name";
            listBox1.ValueMember = "ID";


            treeList1.DataSource = Content.PaymentCategoryRepository.GetItems();
            treeList1.ParentFieldName = "Parent";
            treeList1.KeyFieldName = "ID";
            treeList1.PreviewFieldName = "Name";

            treeList1.ExpandAll();

            textBox1.Focus();

            nowYesterdayOtherDatePicker1.MaxDate = DateTime.Now;
            nowYesterdayOtherDatePicker1.MinDate = DateTime.Now.AddDays(-14);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProcessPayment();
        }

        private void ProcessPayment()
        {
            if (!button1.Enabled)
            {
                ValidateError(nowYesterdayOtherDatePicker1, "Выбирите дату платежа");
                return;
            }

            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                ValidateError(textBox1, "Заполните поле сумма платежа");
                return;
            }

            var payment = new Payment
            {
                WalletID = (int)listBox1.SelectedValue,
                CategoryID = (int)treeList1.FocusedNode.GetValue("ID"),
                Comment = richTextBox1.Text,
                UserID = Content.UserManager.UserLogged.ID,
                Created = nowYesterdayOtherDatePicker1.Date,
                Total = Convert.ToDecimal(textBox1.Text)
            };

            Content.PaymentRepository.InsertItem(payment);
            Close();
        }

        private void ValidateError(Control control, string msg)
        {
            if (MessageBox.Show(msg, "Ошибка", MessageBoxButtons.OK) == DialogResult.OK)
            {
                control.Focus();
            }
        }

        private void nowYesterdayOtherDatePicker1_DateChanged(object sender, ControlsLib.Other.DateChangedEventArgs e)
        {
            button1.Enabled = true;
            button1.Text = string.Format("Создать платеж от {0:D}", e.Date);
        }

        private void NewPaymentForm_KeyPress(object sender, KeyPressEventArgs e)
        {
          
        }

        private void NewPaymentForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ProcessPayment();
            }
        }
    }
}
