﻿using System;
using System.Windows.Forms;
using LaRenzo.DataRepository.Repositories;
using LaRenzo.DataRepository.Repositories.Finance;

namespace LaRenzo.Forms.Finance
{
    public partial class WalletForm : Form
    {
        public WalletForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Content.WalletRepository.CreateDefaultWallets();
        }

        private void WalletForm_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = Content.WalletRepository.GetItems();
        }
    }
}
