﻿using System;
using System.Windows.Forms;

namespace LaRenzo.Forms.Finance
{
    public partial class CategoryPrompt : Form
    {
        public new string Name { get; set; }

        public CategoryPrompt()
        {
            InitializeComponent();
        }

        public CategoryPrompt(string name):this()
        {
            Name = name;
        }

        private void CategoryPrompt_Load(object sender, EventArgs e)
        {
            textBox1.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process();
        }

        private void Process()
        {
            Name = textBox1.Text;
            Close();
        }

        private void CategoryPrompt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) Process();
        }
    }
}
