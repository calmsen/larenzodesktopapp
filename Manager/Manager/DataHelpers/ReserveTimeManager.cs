﻿using LaRenzo.DataRepository.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Repositories.Others.Times;
using LaRenzo.DataRepository.Entities.Others.TimeSheets;
using System.Windows.Forms;
using System.Threading.Tasks;
using LaRenzo.DataRepository.Repositories;

namespace LaRenzo.DataHelpers
{
    public static class ReserveTimeManager
    {
        //_________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Резервирование времени на кухне </summary>
        ////====================================================
        public static DateTime ReserveTime(Dictionary<Section, int> sectionTimeLists, Guid orderGuid, bool isNearTime, DateTime targetTime, int addDayCount, int brandId, int pointOfSaleId)
        {
            // Очистить прошлые резервации
            Content.TimeSheetManager.ClearReservation(orderGuid);

            bool isOutOfTimeUpward = false;
            bool isOutOfTimeDownward = false;

            DateTime now = DateTime.Now.ToLocalTime();
            DateTime normalizedTargetTime = Content.TimeSheetManager.GetNormalizedDateTime(new TimeSpan(targetTime.Hour, targetTime.Minute, 0), addDayCount);

            var timeSheetsToUpdate = new List<TimeSheet>();
            var timeSheetsToUpdateOpositeDirection = new List<TimeSheet>();

            // Получить список позиций на обновления
            foreach (var sectionTime in sectionTimeLists)
            {
                if (sectionTime.Value > 0)
                {
                    try
                    {
                        timeSheetsToUpdate.AddRange(Content.TimeSheetManager.GetSheetsForReserveInSection(sectionTime.Key.ID, sectionTime.Value, 0, normalizedTargetTime, isNearTime, true, addDayCount, pointOfSaleId, brandId));
                    }
                    catch (OutOfTimeSheetsException)
                    {
                        if (isNearTime)
                        {
                            isOutOfTimeDownward = true;
                        }
                        else
                        {
                            isOutOfTimeUpward = true;
                        }
                    }
                }
            }
            if ((isOutOfTimeDownward || isOutOfTimeUpward) && isNearTime)
            {
                new ReservationDialog(null, null);
            }
            // Если не ближайшее
            if (!isNearTime)
            {
                DateTime? maxTimeSameDirectionShift = (isOutOfTimeUpward) ? (DateTime?)null : timeSheetsToUpdate.Max(x => x.Time);

                if (maxTimeSameDirectionShift != normalizedTargetTime.AddMinutes(-1))
                {
                    foreach (var sectionTime in sectionTimeLists)
                    {
                        int minutesForSection = sectionTime.Value;

                        if (minutesForSection > 0)
                        {
                            try
                            {
                                timeSheetsToUpdateOpositeDirection.AddRange(
                                                    Content.TimeSheetManager.GetSheetsForReserveInSection(sectionTime.Key.ID, minutesForSection, 0, normalizedTargetTime, true, false, addDayCount, pointOfSaleId, brandId));
                            }
                            catch (OutOfTimeSheetsException)
                            {
                                isOutOfTimeDownward = true;
                            }
                        }

                    }

                    DateTime? maxTimeOppositeDirectionShift = isOutOfTimeDownward ? (DateTime?)null : timeSheetsToUpdateOpositeDirection.Max(x => x.Time);

                    var dialog = new ReservationDialog(maxTimeSameDirectionShift, maxTimeOppositeDirectionShift);


                    if (dialog.ShowDialog() == DialogResult.No)
                    {
                        timeSheetsToUpdate = timeSheetsToUpdateOpositeDirection;
                    }

                    dialog.Close();
                    isNearTime = true;
                }
            }
            var timeGap = Content.TimeSheetManager.CalculateMaxTimeGap(timeSheetsToUpdate);

            if (timeGap > Content.GlobalOptionsManager.GetMaxTimeGap(brandId) && isNearTime)
            {
                // Новое целевое время
                DateTime newTargetTime = timeSheetsToUpdate.Max(x => x.Time).AddMinutes(1);
                return ReserveTimeBackward(sectionTimeLists, orderGuid, newTargetTime, Content.TimeSheetManager.GetAddDayCountByTime(newTargetTime), brandId, pointOfSaleId);
            }

            if (!timeSheetsToUpdate.Any())
            {
                DateTime nowReserveTime = DateTime.Now;
                return nowReserveTime;
            }

            Content.TimeSheetManager.MarkReservedAndSave(orderGuid, timeSheetsToUpdate, brandId);

            DateTime maxTimeShit = timeSheetsToUpdate.Max(x => x.Time);

            // Вывести дату в нормализованном формате
            DateTime totalReserveTime = now.Date.Add(maxTimeShit.TimeOfDay).AddMinutes(1);

            return totalReserveTime;
        }



        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Поиск резерва на ближайшее время  </summary>
        ////======================================================
        public static async Task<DateTime> ReserveTimeForward(Dictionary<Section, int> sectionTimeLists, Guid orderGuid, int brandId, int pointOfSale)
        {
            DateTime targetTime = DateTime.Now;

            var session = await Content.SessionManager.GetOpened();

            if (session == null) throw new Exception("Сессия не открыта");

            return ReserveTime(sectionTimeLists, orderGuid, true, targetTime, Content.SessionManager.AddDayCount(session), brandId, pointOfSale);

        }




        //__________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Поиск резерва на заданное время  </summary>
        ////=====================================================
        public static DateTime ReserveTimeBackward(Dictionary<Section, int> sectionTimeLists, Guid orderGuid, DateTime targetTime, int addDayCount, int brandId, int pointOfSale)
        {
            const bool IS_NEAR_TIME = false;
            return ReserveTime(sectionTimeLists, orderGuid, IS_NEAR_TIME, targetTime, addDayCount, brandId, pointOfSale);
        }
    }
}
