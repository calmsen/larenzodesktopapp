﻿using System.Collections;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace LaRenzo.DataHelpers
{
    // Handles TreeListView states
    public class TreeListViewStateManager
    {
        private ArrayList _expanded;
        private ArrayList _selected;
        private object _focused;
        private int _topIndex;

        public TreeListViewStateManager() : this(null) { }
        public TreeListViewStateManager(TreeList treeList)
        {
            _treeList = treeList;
            _expanded = new ArrayList();
            _selected = new ArrayList();
        }

        public void Clear()
        {
            _expanded.Clear();
            _selected.Clear();
            _focused = null;
            _topIndex = 0;
        }
        private ArrayList GetExpanded()
        {
            var op = new OperationSaveExpanded();
            TreeList.NodesIterator.DoOperation(op);
            return op.Nodes;
        }
        private ArrayList GetSelected()
        {
            var al = new ArrayList();
            foreach (TreeListNode node in TreeList.Selection)
                al.Add(node.GetValue(TreeList.KeyFieldName));
            return al;
        }

        public void LoadState()
        {
            TreeList.BeginUpdate();
            try
            {
                TreeList.CollapseAll();
                TreeListNode node;
                foreach (object key in _expanded)
                {
                    node = TreeList.FindNodeByKeyID(key);
                    if (node != null)
                        node.Expanded = true;
                }
                foreach (object key in _selected)
                {
                    node = TreeList.FindNodeByKeyID(key);
                    if (node != null)
                        TreeList.Selection.Add(node);
                }
                TreeList.FocusedNode = TreeList.FindNodeByKeyID(_focused);
            }
            finally
            {
                TreeList.EndUpdate();
                TreeList.TopVisibleNodeIndex = TreeList.GetVisibleIndexByNode(TreeList.FocusedNode) - _topIndex;
            }
        }
        public void SaveState()
        {
            if (TreeList.FocusedNode != null)
            {
                _expanded = GetExpanded();
                _selected = GetSelected();
                _focused = TreeList.FocusedNode[TreeList.KeyFieldName];
                _topIndex = TreeList.GetVisibleIndexByNode(TreeList.FocusedNode) -
TreeList.TopVisibleNodeIndex;
            }
            else
                Clear();
        }

        private TreeList _treeList;
        public TreeList TreeList
        {
            get
            {
                return _treeList;
            }
            set
            {
                _treeList = value;
                Clear();
            }
        }

        class OperationSaveExpanded : TreeListOperation
        {
            private ArrayList _al = new ArrayList();
            public override void Execute(TreeListNode node)
            {
                if (node.HasChildren && node.Expanded)
                    _al.Add(node.GetValue(node.TreeList.KeyFieldName));
            }
            public ArrayList Nodes { get { return _al; } }
        }
    }
}
