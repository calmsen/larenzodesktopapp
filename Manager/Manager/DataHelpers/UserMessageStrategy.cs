﻿using LaRenzo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LaRenzo.DataHelpers
{
    public class UserMessageStrategy : IUserMessageStrategy
    {
        public void Show(string message)
        {
            MessageBox.Show(message);
        }

        public void Show(string message, Exception innerException)
        {
            MessageBox.Show(message + "\r\n" + innerException.ToString());
        }
    }
}
