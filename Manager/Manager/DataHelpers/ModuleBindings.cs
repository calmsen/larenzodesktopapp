﻿using Ninject.Modules;
using Ninject.Extensions.Conventions;
using DataLib.Models;
using System.Reflection;
using LaRenzo.Interfaces;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.DataRepository;
using LaRenzo.DataRepository.Repositories.Catalogs.Categories;
using LaRenzo.DataRepository.Repositories.Catalogs.Dishes;

namespace LaRenzo.DataHelpers
{
    public class ModuleBindings : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(x => x
            .From(Assembly.GetAssembly(typeof(PizzaAppDB)))
            .SelectAllClasses()
            .EndingWith("Repository")
            .Excluding<CategoryRepository>()
            .Excluding<DishRepository>()
            .BindDefaultInterface()
            .Configure(b => b.InSingletonScope()));

            Kernel.Bind(x => x
            .From(Assembly.GetAssembly(typeof(PizzaAppDB)))
            .SelectAllClasses()
            .EndingWith("Manager")
            .BindDefaultInterface()
            .Configure(b => b.InSingletonScope()));

            Bind<IThreadLocalStorage>().To<ThreadLocalStorage>();
            Bind<IBaseOptionsFunctions>().To<BaseOptionsFunctions>();
            Bind<ISignalProcessor>().To<SignalProcessor>();
            Bind<IUserMessageStrategy>().To<UserMessageStrategy>();
            Bind<ICategoryRepository>().To<CategoryCachedRepository>();
            Bind<IDishRepository>().To<DishCachedRepository>();
        }
    }
}
