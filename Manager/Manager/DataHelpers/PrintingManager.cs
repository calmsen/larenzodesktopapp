﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;
using LaRenzo.DataRepository.Entities.Catalogs.Printers;
using LaRenzo.DataRepository.Entities.Catalogs.Sections;
using LaRenzo.DataRepository.Entities.Documents.CafeOrders;
using LaRenzo.DataRepository.Entities.Documents.DeliveryOrders;
using LaRenzo.DataRepository.Entities.Documents.Sessions;
using LaRenzo.DataRepository.Entities.PrintWorksModels;
using LaRenzo.DataRepository.Entities.States;
using LaRenzo.DataRepository.Repositories.Barcodes;
using LaRenzo.DataRepository.Repositories.Others.GlobalOptions;
using LaRenzo.TransferLibrary;


namespace LaRenzo.DataRepository.Repositories.Catalogs.Printers
{
    public class PrintingManager
    {
        /// <summary> Типы принтеров </summary>
        private struct PrinterName
        {
            /// <summary> Принтер для печати Сессонного чека </summary>
            public const string Session = "session";
            /// <summary> Принтер для печати чека доставки </summary>
            public const string Delivery = "delivery";
            /// <summary> Принтер для печати чека на сумку </summary>
            public const string Bag = "bag";
            /// <summary> Принтер для печати чека для клиента </summary>
            public const string Client = "client";
            /// <summary> Принтер для печати чека для  </summary>
            public const string CafeClient = "cafe_clnt";
            /// <summary> Принтер для печати чека для  </summary>
            public const string CafeKitchen = "cafe_kitchen";
            /// <summary> Принтер для печати чека для  </summary>
            public const string CafeBar = "cafe_bar";
        }

        /// <summary> Текст быстрой доставки </summary>
        private const string FastDriver = "Быстрая доставка";

        // Максимальная ширина чека
        private const int MaxCheckWidth = 300;
        // Цвет щрифтка чека
        private static readonly Brush CheckBrush = Brushes.Black;

        private const int ItemMaxStringLength = 19;
        private const int MaxStringLength = 28;
        private const int SmallMaxStringLength = 36;

        /// <summary> Шрифт для заголока всех чеков, кроме чека информации о блюдах </summary>
        private static Font HeaderFont(int orderBrandId) { return PrintersFontsOptionsFunctions.Instance.PrinterHeaderFontByBrandId(orderBrandId); }

        /// <summary> Шрифт для чека: сессии инфо об админ. операторы, время кухни для цехов кухни, доставки, оператора на кухню </summary>
        private static Font BodyFont(int orderBrandId) { return PrintersFontsOptionsFunctions.Instance.PrinterBodyFontByBrandId(orderBrandId); }

        /// <summary> Шрифт для блюд в чеке для кухни </summary>
        private static Font ItemsFont(int orderBrandId) { return PrintersFontsOptionsFunctions.Instance.PrinterItemsFontByBrandId(orderBrandId); }

        /// <summary> Шрифт для тела чека для клиента </summary>
        private static Font SmallFont(int orderBrandId) { return PrintersFontsOptionsFunctions.Instance.PrinterSmallFontByBrandId(orderBrandId); }

        /// <summary> Шрифт для тела чека информации о блюдах в заказе </summary>
        private static Font StickyFont(int orderBrandId) { return PrintersFontsOptionsFunctions.Instance.PrinterStikyFontByBrandId(orderBrandId); }

        /// <summary> Шрифт для тела чека при закрытии сессии </summary>
        private static Font SessionBodyFont(int orderBrandId) { return PrintersFontsOptionsFunctions.Instance.PrinterSessionFontByBrandId(orderBrandId); }

        /// <summary> Формат заголовка </summary>
        private static StringFormat HeaderStringFormat
        {
            get
            {
                var headerFormat = new StringFormat
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };

                return headerFormat;
            }
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Вернуть первую строчку чека по бренду заказа </summary>
        ////=================================================================
        private static string GetFirstCheckStringByOrderId(int orderBrandId)
        {
            return Content.GlobalOptionsManager.GetFirstStringCheck(orderBrandId);
        }

        private static void PrintSessionPankratovCheck(Session session, int pointOfSaleId, int brendId)
        {
            List<Order> orders = Content.OrderManager.GetSessionInfo(session.ID);

            var pmList = PaymentSessionSummaryInfo.CreateWithSiteOnlineCard(orders);

            var printDocument = new PrintDocument();

            printDocument.PrintPage += (sender, args) =>
            {
                args.Graphics.DrawString("ЧЕК НА СЕССИЮ", HeaderFont(brendId), Brushes.Black, new PointF(0, 0));
                args.Graphics.DrawString(string.Format("СЕССИЯ №{0}", session.ID), HeaderFont(brendId), Brushes.Black, new PointF(0, 30));
                args.Graphics.DrawString("ИП ПАНКРАТОВ", HeaderFont(brendId), Brushes.Black, new PointF(0, 60));
                args.Graphics.DrawString(Spleater(), BodyFont(brendId), Brushes.Black, new PointF(0, 95));

                var builder = new StringBuilder();
                builder.AppendFormat("Сессия открыта:{0:dd/MM/yyyy HH:mm}\n", session.Opened);
                builder.AppendFormat("Сессия закрыта:{0:dd/MM/yyyy HH:mm}\n", session.Closed);

                builder.AppendFormat("\nВсего заказов (без отмен):{0}\n", pmList.TotalCount);
                builder.AppendFormat("Сумма:{0} руб.\n", pmList.TotalSum);

                builder.AppendFormat("\nСайты Ларензо и Рэмбо\n\n");

                builder.AppendFormat("Отменено: {0}\n", pmList.TotalCanceledCount);
                builder.AppendFormat("Сумма: {0} руб.\n", pmList.TotalCanceledSum);

                foreach (var summary in pmList.SummaryList.Where(x => !x.IsVlru))
                {
                    if (summary.Sum <= 0 && summary.Count <= 0) continue;

                    builder.AppendFormat("Оплачено ({0}): {1}\n", summary.Description, summary.Count);
                    builder.AppendFormat("{0}: {1} руб.\n", summary.Description, summary.Sum);
                }

                builder.AppendFormat("Сайты Ларензо и Рэмбо (итого): {0}\n", pmList.TotalCount);
                builder.AppendFormat("Сумма: {0} руб.\n", pmList.TotalSum);

               
                var stringForPrinting = builder.ToString();
                args.Graphics.DrawString(stringForPrinting, SessionBodyFont(brendId), Brushes.Black, new PointF(0, 110));

                args.Graphics.DrawString(Spleater(), BodyFont(brendId), Brushes.Black, new PointF(0, 850));

                args.Graphics.DrawString(SignPlace("Оператор 1."), BodyFont(brendId), Brushes.Black, new PointF(0, 880));
                args.Graphics.DrawString(SignPlace("Оператор 2."), BodyFont(brendId), Brushes.Black, new PointF(0, 910));
                args.Graphics.DrawString(SignPlace("Администратор"), BodyFont(brendId), Brushes.Black, new PointF(0, 940));
            };

            // Печать на устройства
            AddToPrintQuery(PrinterName.Session, printDocument, pointOfSaleId);
        }

        private static void PrintSessionCommonCheck(Session session, int pointOfSaleId, int brendId)
        {
            var cardsData = Content.PrintingRepository.GetCardsData(session.ID);

            List<Order> orders = Content.OrderManager.GetSessionInfo(session.ID);

            var pmList = PaymentSessionSummaryInfo.CreateWithoutSiteOnlineCard(orders);

            var pmListVlru = PaymentSessionSummaryInfo.CreateWithVlruSite(orders);

            var printDocument = new PrintDocument();

            printDocument.PrintPage += (sender, args) =>
            {
                args.Graphics.DrawString("ЧЕК НА СЕССИЮ", HeaderFont(brendId), Brushes.Black, new PointF(0, 0));
                args.Graphics.DrawString(string.Format("СЕССИЯ №{0}", session.ID), HeaderFont(brendId), Brushes.Black, new PointF(0, 30));
                args.Graphics.DrawString(Spleater(), BodyFont(brendId), Brushes.Black, new PointF(0, 65));

                var builder = new StringBuilder();
                builder.AppendFormat("Сессия открыта:{0:dd/MM/yyyy HH:mm}\n", session.Opened);
                builder.AppendFormat("Сессия закрыта:{0:dd/MM/yyyy HH:mm}\n", session.Closed);

                builder.AppendFormat("\nВсего заказов (без отмен):{0}\n", pmList.TotalCount + pmListVlru.TotalCount + cardsData.offlineCafeCount + cardsData.onlineCafeCount + cardsData.cardsCafeCount);
                builder.AppendFormat("Сумма:{0} руб.\n", pmList.TotalSum + pmListVlru.TotalSum + cardsData.offlineCafeSum + cardsData.onlineCafeSum + cardsData.cardsCafeSum);

                builder.AppendFormat("\nДоставка\n\n");

                builder.AppendFormat("Отменено: {0}\n", pmList.TotalCanceledCount);
                builder.AppendFormat("Сумма: {0} руб.\n", pmList.TotalCanceledSum);

                foreach (var summary in pmList.SummaryList.Where(x => !x.IsVlru))
                {
                    if (summary.Sum <= 0 && summary.Count <= 0) continue;

                    builder.AppendFormat("Оплачено ({0}): {1}\n", summary.Description, summary.Count);
                    builder.AppendFormat("{0}: {1} руб.\n", summary.Description, summary.Sum);
                }

                builder.AppendFormat("По доставке (итого): {0}\n", pmList.TotalCount);
                builder.AppendFormat("Сумма: {0} руб.\n", pmList.TotalSum);

                builder.AppendFormat("\nВл.ру\n\n");

                builder.AppendFormat("Отменено: {0}\n", pmListVlru.TotalCanceledCount);
                builder.AppendFormat("Сумма: {0} руб.\n", pmListVlru.TotalCanceledSum);

                foreach (var summary in pmListVlru.SummaryList)
                {
                    if (summary.Sum <= 0 && summary.Count <= 0) continue;

                    builder.AppendFormat("Оплачено ({0}): {1}\n", summary.Description, summary.Count);
                    builder.AppendFormat("{0}: {1} руб.\n", summary.Description, summary.Sum);
                }

                builder.AppendFormat("Вл.ру (итого): {0}\n", pmListVlru.TotalCount);
                builder.AppendFormat("Сумма: {0} руб.\n", pmListVlru.TotalSum);

                builder.AppendFormat("\nКафе\n\n");

                builder.AppendFormat("Безналом: {0}\n", cardsData.onlineCafeCount);
                builder.AppendFormat("Сумма: {0} руб.\n", cardsData.onlineCafeSum);

                builder.AppendFormat("Наличными: {0}\n", cardsData.offlineCafeCount);
                builder.AppendFormat("Сумма: {0} руб.\n", cardsData.offlineCafeSum);

                builder.AppendFormat("Оплачено картами: {0}\n", cardsData.cardsCafeCount);
                builder.AppendFormat("Сумма: {0} руб.\n", cardsData.cardsCafeSum);

                builder.AppendFormat("В кафе (итого): {0}\n", cardsData.offlineCafeCount + cardsData.onlineCafeCount + cardsData.cardsCafeCount);
                builder.AppendFormat("Сумма: {0} руб.\n", cardsData.offlineCafeSum + cardsData.onlineCafeSum + cardsData.cardsCafeSum);

                var stringForPrinting = builder.ToString();
                args.Graphics.DrawString(stringForPrinting, SessionBodyFont(brendId), Brushes.Black, new PointF(0, 80));

                args.Graphics.DrawString(Spleater(), BodyFont(brendId), Brushes.Black, new PointF(0, 850));

                args.Graphics.DrawString(SignPlace("Оператор 1."), BodyFont(brendId), Brushes.Black, new PointF(0, 880));
                args.Graphics.DrawString(SignPlace("Оператор 2."), BodyFont(brendId), Brushes.Black, new PointF(0, 910));
                args.Graphics.DrawString(SignPlace("Администратор"), BodyFont(brendId), Brushes.Black, new PointF(0, 940));
            };

            // Печать на устройства
            AddToPrintQuery(PrinterName.Session, printDocument, pointOfSaleId);
        }

        private static void PrintSessionCheck(Session session, int pointOfSaleId, int brendId)
        {
            var cardsData = Content.PrintingRepository.GetCardsData(session.ID);

            List<Order> orders = Content.OrderManager.GetSessionInfo(session.ID);

            var pmList = PaymentSessionSummaryInfo.CreateDefault(orders);

            var pmListVlru = PaymentSessionSummaryInfo.CreateWithVlruSite(orders);

            var printDocument = new PrintDocument();

            printDocument.PrintPage += (sender, args) =>
            {
                args.Graphics.DrawString("ЧЕК НА СЕССИЮ", HeaderFont(brendId), Brushes.Black, new PointF(0, 0));
                args.Graphics.DrawString(string.Format("СЕССИЯ №{0}", session.ID), HeaderFont(brendId), Brushes.Black, new PointF(0, 30));
                args.Graphics.DrawString(Spleater(), BodyFont(brendId), Brushes.Black, new PointF(0, 65));

                var builder = new StringBuilder();
                builder.AppendFormat("Сессия открыта:{0:dd/MM/yyyy HH:mm}\n", session.Opened);
                builder.AppendFormat("Сессия закрыта:{0:dd/MM/yyyy HH:mm}\n", session.Closed);

                builder.AppendFormat("\nВсего заказов (без отмен):{0}\n", pmList.TotalCount + pmListVlru.TotalCount + cardsData.offlineCafeCount + cardsData.onlineCafeCount + cardsData.cardsCafeCount);
                builder.AppendFormat("Сумма:{0} руб.\n", pmList.TotalSum + pmListVlru.TotalSum + cardsData.offlineCafeSum + cardsData.onlineCafeSum + cardsData.cardsCafeSum);

                builder.AppendFormat("\nДоставка\n\n");

                builder.AppendFormat("Отменено: {0}\n", pmList.TotalCanceledCount);
                builder.AppendFormat("Сумма: {0} руб.\n", pmList.TotalCanceledSum);

                foreach (var summary in pmList.SummaryList.Where(x => !x.IsVlru))
                {
                    if (summary.Sum <= 0 && summary.Count <= 0) continue;

                    builder.AppendFormat("Оплачено ({0}): {1}\n", summary.Description, summary.Count);
                    builder.AppendFormat("{0}: {1} руб.\n", summary.Description, summary.Sum);
                }

                builder.AppendFormat("По доставке (итого): {0}\n", pmList.TotalCount);
                builder.AppendFormat("Сумма: {0} руб.\n", pmList.TotalSum);

                builder.AppendFormat("\nВл.ру\n\n");

                builder.AppendFormat("Отменено: {0}\n", pmListVlru.TotalCanceledCount);
                builder.AppendFormat("Сумма: {0} руб.\n", pmListVlru.TotalCanceledSum);

                foreach (var summary in pmListVlru.SummaryList)
                {
                    if (summary.Sum <= 0 && summary.Count <= 0) continue;

                    builder.AppendFormat("Оплачено ({0}): {1}\n", summary.Description, summary.Count);
                    builder.AppendFormat("{0}: {1} руб.\n", summary.Description, summary.Sum);
                }

                builder.AppendFormat("Вл.ру (итого): {0}\n", pmListVlru.TotalCount);
                builder.AppendFormat("Сумма: {0} руб.\n", pmListVlru.TotalSum);

                builder.AppendFormat("\nКафе\n\n");

                builder.AppendFormat("Безналом: {0}\n", cardsData.onlineCafeCount);
                builder.AppendFormat("Сумма: {0} руб.\n", cardsData.onlineCafeSum);

                builder.AppendFormat("Наличными: {0}\n", cardsData.offlineCafeCount);
                builder.AppendFormat("Сумма: {0} руб.\n", cardsData.offlineCafeSum);

                builder.AppendFormat("Оплачено картами: {0}\n", cardsData.cardsCafeCount);
                builder.AppendFormat("Сумма: {0} руб.\n", cardsData.cardsCafeSum);

                builder.AppendFormat("В кафе (итого): {0}\n", cardsData.offlineCafeCount + cardsData.onlineCafeCount + cardsData.cardsCafeCount);
                builder.AppendFormat("Сумма: {0} руб.\n", cardsData.offlineCafeSum + cardsData.onlineCafeSum + cardsData.cardsCafeSum);

                var stringForPrinting = builder.ToString();
                args.Graphics.DrawString(stringForPrinting, SessionBodyFont(brendId), Brushes.Black, new PointF(0, 80));

                args.Graphics.DrawString(Spleater(), BodyFont(brendId), Brushes.Black, new PointF(0, 850));

                args.Graphics.DrawString(SignPlace("Оператор 1."), BodyFont(brendId), Brushes.Black, new PointF(0, 880));
                args.Graphics.DrawString(SignPlace("Оператор 2."), BodyFont(brendId), Brushes.Black, new PointF(0, 910));
                args.Graphics.DrawString(SignPlace("Администратор"), BodyFont(brendId), Brushes.Black, new PointF(0, 940));
            };

            // Печать на устройства
            AddToPrintQuery(PrinterName.Session, printDocument, pointOfSaleId);
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Напечатать чек результатов закрытии сессии </summary>
        ////===============================================================
        public static string EndSessionCheckPrint(Session session, int pointOfSaleId)
        {
            string errorMsg = null;

            int brendId = Content.BrandManager.SelectedBrand.ID;

            try
            {
                if (session.Closed == null)
                {
                    errorMsg = "Сессия не закрыта! Чек не может быть напечатан.";
                }
                else
                {
                    PrintSessionCheck(session, pointOfSaleId, brendId);
                    PrintSessionCommonCheck(session, pointOfSaleId, brendId);
                    PrintSessionPankratovCheck(session, pointOfSaleId, brendId);
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }

            return errorMsg;
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        private static string SignPlace(string person)
        {
            var text = new StringBuilder(person);
            text.Append(Repeater("_", MaxStringLength - person.Length));
            return text.ToString();
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        private static string Repeater(string pattern, int amount)
        {
            var text = new StringBuilder();
            for (int i = 0; i < amount; i++)
            {
                text.Append(pattern);
            }
            return text.ToString();
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        private static string Spleater(string pattern = "*", bool isSmall = false)
        {
            var text = new StringBuilder();
            text.Append(isSmall ? Repeater(pattern, SmallMaxStringLength) : Repeater(pattern, MaxStringLength));

            text.Append(Environment.NewLine);

            return text.ToString();
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Чек наклейка - информация о составе блюда </summary>
        ////==============================================================
        public static void PrintDishInfoCheck(Order order, Dish dish, bool pct)
        {
            var printDocument = new PrintDocument();
            const int checkWidth = 200;

            int brendId = order.Brand.ID;

            string topHeader = GetFirstCheckStringByOrderId(order.Brand.ID) + "\r\n";
            topHeader += string.Format("Изготовлено и упаковано: {0:dd.MM.yyyy} {1:HH:mm}", order.TimeKitchen, order.TimeKitchen);

            string headerText = dish.Name;
            string Struct = string.Format("Состав: {0}", dish.Structure);
            string dishPfc = string.Format("Белки: {0}г, Жиры: {1}г, Углеводы: {2}г", dish.Protein, dish.Fat, dish.Carbohydrates);
            string energy = string.Format("Энергетическая ценность: {0} Ккал", dish.CoeffEnergy);
            string netto = string.Format("Масса нетто: {0}г", dish.Netto);
            string startSign = (dish.StartTemperature > 0 ? "+" : "");
            string endSign = (dish.EndTemperature > 0 ? "+" : "");
            string fridg = string.Format("Годен в течение {0} часов при температуре от {1}{2}°C до {3}{4}°C", dish.FitHour, startSign, dish.StartTemperature, endSign, dish.EndTemperature);
            string addrManufactured = string.Format("Изготовитель: {0}", dish.Manufacturer);
            string addressProduction = string.Format("Адрес производства: {0}", dish.AddressProduction);
            string cto = string.Format("СТО: {0}", dish.CTO);
            printDocument.PrintPage += (sender, args) =>
                {
                    var headerFormat = new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };

                    var headerRect = new RectangleF
                    {
                        Location = new PointF(0, 0),
                        Size = new SizeF(checkWidth, ((int)args.Graphics.MeasureString(headerText, BodyFont(brendId), checkWidth, headerFormat).Height))
                    };

                    args.Graphics.DrawString(headerText, StickyFont(brendId), Brushes.Black, new RectangleF(new PointF(0, 0), new SizeF(checkWidth, headerRect.Height)), headerFormat);


                    var headerTopRect = new RectangleF
                    {
                        Location = new PointF(0, (int)headerRect.Bottom),
                        Size = new SizeF(checkWidth, ((int)args.Graphics.MeasureString(topHeader, StickyFont(brendId), checkWidth, new StringFormat { LineAlignment = StringAlignment.Near }).Height))
                    };

                    args.Graphics.DrawString(topHeader, StickyFont(brendId), Brushes.Black, headerTopRect, new StringFormat { LineAlignment = StringAlignment.Near });


                    var bodyRectStruct = new RectangleF
                    {
                        Location = new PointF(0, (int)headerTopRect.Bottom),
                        Size = new SizeF(checkWidth, ((int)args.Graphics.MeasureString(Struct, StickyFont(brendId), checkWidth, new StringFormat { LineAlignment = StringAlignment.Near }).Height))
                    };

                    args.Graphics.DrawString(Struct, StickyFont(brendId), Brushes.Black,
                                             new RectangleF(new PointF(0, (int)headerTopRect.Bottom), new SizeF(checkWidth, bodyRectStruct.Height)), new StringFormat { LineAlignment = StringAlignment.Near });

                    var bodyRectPfc = new RectangleF
                    {
                        Location = new PointF(0, (int)bodyRectStruct.Bottom),
                        Size = new SizeF(checkWidth,
                                         ((int)
                                          args.Graphics.MeasureString(dishPfc, StickyFont(brendId), checkWidth, new StringFormat { LineAlignment = StringAlignment.Near }).Height))
                    };

                    args.Graphics.DrawString(dishPfc, StickyFont(brendId), Brushes.Black,
                                             new RectangleF(new PointF(0, (int)bodyRectStruct.Bottom), new SizeF(checkWidth, bodyRectPfc.Height)), new StringFormat { LineAlignment = StringAlignment.Near });

                    var bodyRectEnergy = new RectangleF
                    {
                        Location = new PointF(0, (int)bodyRectPfc.Bottom),
                        Size = new SizeF(checkWidth, ((int)args.Graphics.MeasureString(energy, StickyFont(brendId), checkWidth, new StringFormat { LineAlignment = StringAlignment.Near }).Height))
                    };

                    args.Graphics.DrawString(energy, StickyFont(brendId), Brushes.Black,
                                             new RectangleF(new PointF(0, (int)bodyRectPfc.Bottom), new SizeF(checkWidth, bodyRectEnergy.Height)), new StringFormat { LineAlignment = StringAlignment.Near });

                    var bodyRectNetto = new RectangleF
                    {
                        Location = new PointF(0, (int)bodyRectEnergy.Bottom),
                        Size = new SizeF(checkWidth, ((int)args.Graphics.MeasureString(netto, StickyFont(brendId), checkWidth, new StringFormat { LineAlignment = StringAlignment.Near }).Height))
                    };

                    args.Graphics.DrawString(netto, StickyFont(brendId), Brushes.Black,
                                             new RectangleF(new PointF(0, (int)bodyRectEnergy.Bottom), new SizeF(checkWidth, bodyRectNetto.Height)), new StringFormat { LineAlignment = StringAlignment.Near });
                    var bodyRectFridg = new RectangleF
                    {
                        Location = new PointF(0, (int)bodyRectNetto.Bottom),
                        Size = new SizeF(checkWidth, ((int)args.Graphics.MeasureString(fridg, StickyFont(brendId), checkWidth, new StringFormat { LineAlignment = StringAlignment.Near }).Height))
                    };

                    args.Graphics.DrawString(fridg, StickyFont(brendId), Brushes.Black,
                                             new RectangleF(new PointF(0, (int)bodyRectNetto.Bottom), new SizeF(checkWidth, bodyRectFridg.Height)), new StringFormat { LineAlignment = StringAlignment.Near });

                    var bottomRectManuf = new RectangleF
                    {
                        Location = new PointF(0, (int)bodyRectFridg.Bottom + 10),
                        Size = new SizeF(checkWidth, ((int)args.Graphics.MeasureString(addrManufactured, StickyFont(brendId), checkWidth, new StringFormat { LineAlignment = StringAlignment.Near }).Height))
                    };

                    args.Graphics.DrawString(addrManufactured, StickyFont(brendId), Brushes.Black,
                                             new RectangleF(new PointF(0, (int)bodyRectFridg.Bottom + 10), new SizeF(checkWidth, bottomRectManuf.Height)), new StringFormat { LineAlignment = StringAlignment.Near });

                    var bottomRectProd = new RectangleF
                    {
                        Location = new PointF(0, (int)bottomRectManuf.Bottom),
                        Size = new SizeF(checkWidth, ((int)args.Graphics.MeasureString(addressProduction, StickyFont(brendId), checkWidth, new StringFormat { LineAlignment = StringAlignment.Near }).Height))
                    };

                    args.Graphics.DrawString(addressProduction, StickyFont(brendId), Brushes.Black,
                                             new RectangleF(new PointF(0, (int)bottomRectManuf.Bottom), new SizeF(checkWidth, bottomRectProd.Height)), new StringFormat { LineAlignment = StringAlignment.Near });

                    var bottomRectCto = new RectangleF
                    {
                        Location = new PointF(0, (int)bottomRectProd.Bottom),
                        Size = new SizeF(checkWidth,
                                         ((int)
                                          args.Graphics.MeasureString(cto, StickyFont(brendId), checkWidth, new StringFormat { LineAlignment = StringAlignment.Near }).Height))
                    };

                    args.Graphics.DrawString(cto, StickyFont(brendId), Brushes.Black,
                                             new RectangleF(new PointF(0, (int)bottomRectProd.Bottom), new SizeF(checkWidth, bottomRectCto.Height)), new StringFormat { LineAlignment = StringAlignment.Near });



                    var imageRect = new RectangleF
                    {
                        Location = new PointF(0, (int)bottomRectProd.Bottom + 10),
                        Size = new SizeF(50, 40)
                    };


                    Image i = pct ? LaRenzo.Properties.Resources.rct : LaRenzo.Properties.Resources.eac;
                    args.Graphics.DrawImage(i, imageRect);

                    if (dish.Barcode.Length >= 13)
                    {
                        var ean13 = new Ean13
                        {
                            CountryCode = dish.Barcode.Substring(0, 2),
                            ManufacturerCode = dish.Barcode.Substring(2, 5),
                            ProductCode = dish.Barcode.Substring(7, 5),
                            ChecksumDigit = dish.Barcode.Substring(12, 1),
                            Scale = 0.8f
                        };

                        var barcodeRect = new RectangleF
                        {
                            Location = new PointF(75, (int)bottomRectProd.Bottom),
                            Size = new SizeF(100, 50)
                        }; Image img = ean13.CreateBitmap();
                        args.Graphics.DrawImage(img, barcodeRect);

                    }
                };



            // Напечатать на устройстве
            //string printerType = "dish_" + (pct ? "rst" : "eac") + "_";

            List<Printer> printerList = Content.PrintWorksRepository.GetPrinterToRstAndEac(dish.Category.SectionID, pct, order.PointOfSaleId);

            AddToPrintQuery(printerList, printDocument);
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать общего чека для кухни </summary>
        ////=================================================
        public static void PrintSectionCheckFromOrder(Order order, Section section)
        {
            int brendId = order.Brand.ID;

            var text = new StringBuilder();
            List<OrderItem> items = Content.OrderManager.GetOrderItemsArray(order, section).ToList();
            var additionalItems = new List<OrderItem>();

            var set = Content.SetCalculationManager.GetSets(items);
            if (set != null) additionalItems.AddRange(set);

            if (section == null)
            {
                additionalItems.AddRange(Content.BookletManager.GetBooklets(order.TotalCost));
                additionalItems.AddRange(Content.CutleryManager.GetCutleries(items));
            }

            text.Append(Repeater("*", 19));
            text.Append("\r\n");

            bool wasStick = false;

            // Напечатать заказанные блюда
            var k = 0;
            for (int i = 0; i < items.Count; i++)
            {
                text.Append(AddKitchenItemToCheck(items[i], i));
                k = i + 1;
            }

            // Напедатать дополнительные блюда
            if (additionalItems.Any())
            {
                text.Append(Repeater("-", 19));
                for (int i = 0; i < additionalItems.Count; i++)
                {
                    if (additionalItems[i].Dish.InternalName == "Палочки") // || item.Dish.InternalName == "Набор для ролл")
                    {
                        wasStick = true;
                    }

                    text.Append(AddKitchenItemToCheck(additionalItems[i], i + k, order.PersonsCount));
                }
            }

            // Добавить в чек палочки, если их не было, но они нужны
            if (!wasStick && order.PersonsCount > 0)
            {
                text.Append(AddKitchenSticksToCheck(additionalItems.Count + k, order.PersonsCount));
            }

            text.Append(Spleater());

            var printDocument = new PrintDocument();
            string name = (section == null ? "ОБЩИЙ" : section.Name);

            string headerText = GetFirstCheckStringByOrderId(order.Brand.ID) + "\r\n";
            headerText += string.Format("КУХНЯ:{0}\nЗАКАЗ №{1}{2}{4} \n от {3:dd.MM.yyyy}", name.ToUpper(), order.Code, order.IsWindow ? "\n(ОКНО)" : string.Empty, order.TimeKitchen, order.IsFastDriver ? "\n" + FastDriver : string.Empty);

            headerText += string.Format("Оператор: {0}\n", order.User);

            printDocument.PrintPage += (sender, args) =>
            {
                // Высота заголовка
                float sizeHeaderHeight = args.Graphics.MeasureString(headerText, HeaderFont(brendId), MaxCheckWidth, HeaderStringFormat).Height;

                // Размеры заголовка
                var headerRect = new RectangleF
                {
                    Location = new PointF(0, 0),
                    Size = new SizeF(MaxCheckWidth, sizeHeaderHeight)
                };

                args.Graphics.DrawString(headerText, HeaderFont(brendId), CheckBrush, headerRect, HeaderStringFormat);

                //===============================================
                // Время кухни
                int kitchenTimeUpPointY = (int)headerRect.Bottom;

                // Высота заголовка
                float kitchenTimeHeight = (int)args.Graphics.MeasureString(string.Format("{0:HH:mm}", order.TimeKitchen), HeaderFont(brendId), MaxCheckWidth, StringFormat.GenericTypographic).Height;

                var kitchenTimeRect = new RectangleF
                {
                    Location = new PointF(0, kitchenTimeUpPointY),
                    Size = new SizeF(MaxCheckWidth, kitchenTimeHeight + 3)
                };

                args.Graphics.DrawString("Время кухни: ", BodyFont(brendId), CheckBrush, kitchenTimeRect, new StringFormat { LineAlignment = StringAlignment.Far });

                args.Graphics.DrawString(string.Format("{0:HH:mm}", order.TimeKitchen), HeaderFont(brendId), CheckBrush, new RectangleF(new PointF(120, kitchenTimeUpPointY), new SizeF(MaxCheckWidth, kitchenTimeRect.Height)));

                var spleaterRect = new RectangleF
                {
                    Location = new PointF(0, (int)kitchenTimeRect.Bottom),
                    Size = new SizeF(MaxCheckWidth, ((int)args.Graphics.MeasureString(Spleater(), BodyFont(brendId), MaxCheckWidth, StringFormat.GenericTypographic).Height))
                };

                var itemsRect = new RectangleF
                {
                    Location = new PointF(0, (int)(kitchenTimeRect.Bottom - spleaterRect.Height / 2) + 3),
                    Size = new SizeF(MaxCheckWidth, ((int)args.Graphics.MeasureString(text.ToString(), ItemsFont(brendId), MaxCheckWidth, StringFormat.GenericTypographic).Height))
                };

                args.Graphics.DrawString(text.ToString(), ItemsFont(brendId), CheckBrush, itemsRect, StringFormat.GenericTypographic);

                // Если чек не из цеха
                if (section == null)
                {
                    if (order.DoCalculateDelivery)
                    {
                        var delivTxtTimeRect = new RectangleF
                        {
                            Location = new PointF(0, (int)(itemsRect.Bottom - spleaterRect.Height / 2)),
                            Size = new SizeF(MaxCheckWidth, ((int)args.Graphics.MeasureString("Время доставки: ", BodyFont(brendId), 300, StringFormat.GenericTypographic).Height))
                        };
                        args.Graphics.DrawString("Время доставки: ", BodyFont(brendId), Brushes.Black, delivTxtTimeRect, StringFormat.GenericTypographic);

                        var delivTimeRect = new RectangleF
                        {
                            Location = new PointF(0, (int)(itemsRect.Bottom - spleaterRect.Height / 2)),
                            Size = new SizeF(MaxCheckWidth, ((int)args.Graphics.MeasureString(String.Format("{0:t}\n", order.TimeClient), HeaderFont(brendId), 300, StringFormat.GenericTypographic).Height))
                        };

                        args.Graphics.DrawString(string.Format("{0:t}\n", order.TimeClient), HeaderFont(brendId), Brushes.Black, new RectangleF(new PointF(150, itemsRect.Bottom - spleaterRect.Height), new SizeF(300, delivTimeRect.Height)));

                        var builder = new StringBuilder();
                        builder.Append("Адрес: ");
                        builder.AppendFormat("ул.{0}, {1}", order.Street, order.House);
                        if (!String.IsNullOrEmpty(order.Appartament))
                        {
                            builder.AppendFormat(" кв. {0}", order.Appartament);
                        }
                        if (!String.IsNullOrEmpty(order.Porch))
                        {
                            builder.AppendFormat(" пд. {0}", order.Porch);
                        }
                        builder.AppendFormat("\nИмя заказчика: {0}", order.Name);
                        builder.AppendFormat("\nТелефон: {0}", order.Phone);
                        builder.AppendFormat("\nСумма: {0} р", order.TotalCost);
                        builder.AppendFormat("\nДополнительно: {0}\n{1}\n", order.Additional, GetAdditionalFlagText(order));


                        var additionalRect = new RectangleF
                        {
                            Location = new PointF(0, (int)delivTxtTimeRect.Bottom),
                            Size = new SizeF(300, (int)args.Graphics.MeasureString(builder.ToString(), BodyFont(brendId), 300, StringFormat.GenericTypographic).Height)
                        };

                        args.Graphics.DrawString(builder.ToString(), BodyFont(brendId), Brushes.Black, additionalRect);
                    }
                    else
                    {
                        var notDelivRect = new RectangleF
                        {
                            Location = new PointF(0, (int)(itemsRect.Bottom - spleaterRect.Height / 2)),
                            Size = new SizeF(300, (int)args.Graphics.MeasureString("БЕЗ ДОСТАВКИ", ItemsFont(brendId), 300, StringFormat.GenericTypographic).Height)
                        };

                        args.Graphics.DrawString("БЕЗ ДОСТАВКИ", BodyFont(brendId), Brushes.Black, notDelivRect, new StringFormat { Alignment = StringAlignment.Center });

                        var spleaterLastRect = new RectangleF
                        {
                            Location = new PointF(0, (int)notDelivRect.Bottom),
                            Size = new SizeF(300, (int)args.Graphics.MeasureString(Spleater(), ItemsFont(brendId), 300, StringFormat.GenericTypographic).Height)
                        };

                        args.Graphics.DrawString(Spleater(), ItemsFont(brendId), Brushes.Black, spleaterLastRect, StringFormat.GenericTypographic);
                    }
                }
            };

            // Печать на устройства
            AddToPrintQuery(section, printDocument, order.PointOfSaleId);
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать чека для кухни для цеха </summary>
        ////===================================================
        public static void PrintSectionCheckFromCafeOrderDocument(CafeOrderDocument cafeOrderDocument, Section section, List<CafeOrderDocumentItem> cafeOrderDocumentItemsList)
        {
            int brendId = Content.BrandManager.SelectedBrand.ID;

            // Время кухни
            DateTime kitchenTime = DateTime.Now;

            var text = new StringBuilder();
            List<OrderItem> items = Content.OrderManager.GetOrderItemsArrayByKitchen(cafeOrderDocumentItemsList, section).ToList();

            // Если есть элементы для печати в "этом" цеху, то печатаем
            if (items.Count > 0)
            {
                var additionalItems = new List<OrderItem>();

                var set = Content.SetCalculationManager.GetSets(items);
                if (set != null) additionalItems.AddRange(set);

                text.Append(Repeater("*", 19));

                // Добавить в чек основные блюда
                var k = 0;
                for (int i = 0; i < items.Count; i++)
                {
                    if (items[i].IsEditable)
                    {
                        text.Append(AddKitchenItemToCheck(items[i], i));
                        k = i + 1;
                    }
                }

                // Добавить в чек дополнительные блюда
                if (additionalItems.Any())
                {
                    text.Append(Repeater("-", 19));
                    for (int i = 0; i < additionalItems.Count; i++)
                    {
                        text.Append(AddKitchenItemToCheck(additionalItems[i], i + k));
                    }
                }
                text.Append(Spleater());

                string cafeTableName;

                if (cafeOrderDocument.Table != null)
                {
                    cafeTableName = cafeOrderDocument.Table.Name;
                }
                else
                {
                    cafeTableName = "Возьми с собой";
                }

                var printDocument = new PrintDocument();
                string name = section == null ? "ОБЩИЙ" : section.Name;
                string headerText = string.Format("КУХНЯ:{0}\nЗАКАЗ КАФЕ {1} \n от {2:dd.MM.yyyy}", name.ToUpper(), cafeTableName, kitchenTime);

                printDocument.PrintPage += (sender, args) =>
                {
                    //=============================================
                    // Заголовок

                    // Высота заголовка
                    float sizeHeaderHeight = args.Graphics.MeasureString(headerText, HeaderFont(brendId), MaxCheckWidth, HeaderStringFormat).Height;

                    // Размеры заголовка
                    var headerRect = new RectangleF
                    {
                        Location = new PointF(0, 0),    // Расположение прямоугольника
                        Size = new SizeF(MaxCheckWidth, sizeHeaderHeight)
                    };

                    // Отрисовать заголовок
                    args.Graphics.DrawString(headerText, HeaderFont(brendId), CheckBrush, headerRect, HeaderStringFormat);

                    //=============================================
                    // Время кухни

                    int kitchenTimeUpPointY = (int)headerRect.Bottom;
                    // Высота заголовка
                    float kitchenTimeHeight = (int)args.Graphics.MeasureString(string.Format("{0:HH:mm}", kitchenTime), HeaderFont(brendId), MaxCheckWidth, StringFormat.GenericTypographic).Height;

                    kitchenTimeUpPointY += 50;

                    var kitchenTimeRect = new RectangleF
                    {
                        Location = new PointF(0, kitchenTimeUpPointY),
                        Size = new SizeF(MaxCheckWidth, kitchenTimeHeight)
                    };

                    args.Graphics.DrawString("Время кухни: ", BodyFont(brendId), CheckBrush, new RectangleF(new PointF(0, (int)headerRect.Bottom), new SizeF(300, kitchenTimeRect.Height)), new StringFormat { LineAlignment = StringAlignment.Far });

                    args.Graphics.DrawString(string.Format("{0:HH:mm}", kitchenTime), HeaderFont(brendId), CheckBrush, new RectangleF(new PointF(120, (int)headerRect.Bottom), new SizeF(300, kitchenTimeRect.Height)));

                    var spleaterRect = new RectangleF
                    {
                        Location = new PointF(0, (int)kitchenTimeRect.Bottom),
                        Size = new SizeF(300, ((int)args.Graphics.MeasureString(Spleater(), BodyFont(brendId), 300, StringFormat.GenericTypographic).Height))
                    };

                    //args.Graphics.DrawString(Spleater(), ItemsFont(brendId), Brushes.Black, spleaterRect, StringFormat.GenericTypographic);

                    var itemsRect = new RectangleF
                    {
                        Location = new PointF(0, (int)(kitchenTimeRect.Bottom - spleaterRect.Height / 2)),
                        Size = new SizeF(300, ((int)args.Graphics.MeasureString(text.ToString(), ItemsFont(brendId), 300, StringFormat.GenericTypographic).Height))
                    };

                    args.Graphics.DrawString(text.ToString(), ItemsFont(brendId), Brushes.Black, itemsRect, StringFormat.GenericTypographic);

                };
                var pointOfSale = Content.PointOfSaleRepository.GetItems().FirstOrDefault();
                // Печать на устройства
                if (pointOfSale != null)
                {
                    AddToPrintQuery(section, printDocument, pointOfSale.ID);
                }
            }
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать на устройства по цеху кухни </summary>
        /// <param name="section"> Цех кухни </param>
        /// <param name="printDocument"> Тело печати </param>
        /// <param name="pointOfSaleId"></param>
        ////=======================================================
        private static void AddToPrintQuery(Section section, PrintDocument printDocument, int pointOfSaleId)
        {
            // Если нет сеции, то выбрать общий принетр
            IEnumerable<Printer> printers = section == null ? Content.PrintWorksRepository.GetPrinter("kitchen_all", pointOfSaleId) : Content.PrintWorksRepository.GetPrinter(section, pointOfSaleId);

            // Напечатать на выбранные принтеры
            AddToPrintQuery(printers, printDocument);
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать на устройства по названию принтера </summary>
        /// <param name="printerName"> Название принетра </param>
        /// <param name="printDocument"> Тело печати </param>
        /// <param name="pointOfSaleId"></param>
        ////==============================================================
        private static void AddToPrintQuery(string printerName, PrintDocument printDocument, int pointOfSaleId)
        {
            // Выбрать заданный принтер
            IEnumerable<Printer> printers = Content.PrintWorksRepository.GetPrinter(printerName, pointOfSaleId);

            // Напечатать на выбранные принтеры
            AddToPrintQuery(printers, printDocument);
        }


        //_____________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать на устройства </summary>
        /// <param name="printers"> Заданные принтеры </param>
        /// <param name="printDocument"> Тело печати </param>
        ////=================================================
        private static void AddToPrintQuery(IEnumerable<Printer> printers, PrintDocument printDocument)
        {
            PrinterQuery.AddPrintTask(printers, printDocument);
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        public static void PrintDeliveryCheck(Order order)
        {

            int brendId = order.Brand.ID;

            string headerText = GetFirstCheckStringByOrderId(order.Brand.ID) + "\r\n";
            headerText += string.Format("ДОСТАВКА\nЗАКАЗ №{0} {1}", order.Code, order.IsFastDriver ? "\n" + FastDriver : string.Empty);


            string nearTime = "+++ Расчётное время +++\n";
            if (order.IsNearTime)
            {
                nearTime = "--- Ближайшее время ---\n";
            }

            var builder = new StringBuilder();
            builder.Append(Spleater());
            builder.AppendFormat("Время доставки: {0:dd.MM.yyyy HH:mm} \n" + nearTime, order.TimeClient);
            builder.Append("Адрес: ");
            builder.AppendFormat("ул.{0}, {1}", order.Street, order.House);
            if (!String.IsNullOrEmpty(order.Appartament))
            {
                builder.AppendFormat(" кв. {0}", order.Appartament);
            }
            if (!String.IsNullOrEmpty(order.Porch))
            {
                builder.AppendFormat(" пд. {0}", order.Porch);
            }
            builder.AppendFormat("\nИмя заказчика: {0}", order.Name);
            builder.AppendFormat("\nТелефон: {0}", order.Phone);


            builder.AppendFormat("\nСумма: {0} р", order.TotalCost);
            builder.AppendFormat("\nОплата: {0}", GetPaymentMethodName(order));

            if (!String.IsNullOrEmpty(order.Additional))
            {
                builder.AppendFormat("\nДополнительно: {0}\n{1}\n", order.Additional, GetAdditionalFlagText(order));
            }
            builder.Append(Spleater());
            builder.Append(SignPlace("\nЗаказ проверил"));

            var printDocument = new PrintDocument();
            printDocument.PrintPage += (sender, args) =>
            {
                var headerFormat = new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
                var headerRect = new RectangleF
                {
                    Location = new PointF(0, 0),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(headerText, HeaderFont(brendId), 300, headerFormat).Height))
                };

                args.Graphics.DrawString(headerText, HeaderFont(brendId), Brushes.Black, headerRect, headerFormat);

                var additionalRect = new RectangleF
                {
                    Location = new PointF(0, (int)headerRect.Bottom),
                    Size = new SizeF(300, (int)args.Graphics.MeasureString(builder.ToString(), BodyFont(brendId), 300, StringFormat.GenericTypographic).Height)
                };

                args.Graphics.DrawString(builder.ToString(), BodyFont(brendId), Brushes.Black, additionalRect);
            };

            // Напечатать
            AddToPrintQuery(PrinterName.Delivery, printDocument, order.PointOfSaleId);
        }


        private static string GetAdditionalFlagText(Order order)
        {
            string additional = "";

            if (order.Is5k)
            {
                additional += "5000!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
            }

            if (order.IsUnCash)
            {
                additional += "Без. нал???????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????";
            }

            if (order.IsClientPaid)
            {
                additional += "Оплачено картой%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
            }

            return additional;
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        public static void PrintBagCheck(Order order)
        {
            int brendId = order.Brand.ID;

            var topText = new StringBuilder();
            string headerText = GetFirstCheckStringByOrderId(order.Brand.ID) + "\r\n";
            topText.AppendFormat(headerText);
            topText.AppendFormat("Заказ № {0}\n", order.Code);
            topText.AppendFormat("Оплата: {0}\n", order.IsOnlinePayment ? "Безнал" : "Наличные");
            topText.AppendFormat("ул.{0}, ", order.Street);
            topText.AppendFormat("{0}", order.House);

            if (!String.IsNullOrEmpty(order.Appartament))
            {
                topText.AppendFormat(" кв. {0}", order.Appartament);
            }
            if (!String.IsNullOrEmpty(order.Porch))
            {
                topText.AppendFormat(" пд. {0}", order.Porch);
            }

            var bottomText = new StringBuilder();

            bottomText.AppendFormat("ул.{0}, ", order.Street);
            bottomText.AppendFormat("{0}", order.House);
            if (!String.IsNullOrEmpty(order.Appartament))
                bottomText.AppendFormat(" кв. {0}", order.Appartament);
            if (!String.IsNullOrEmpty(order.Porch))
                bottomText.AppendFormat(" пд. {0}", order.Porch);

            //var paymentMethodAlias = PaymentMethodAlias.GetAll().FirstOrDefault(x => x.PaymentMethod == order.PaymentMethod);

            bottomText.AppendFormat("\nОплата: {0}", GetPaymentMethodName(order));
            bottomText.AppendFormat("\nЗаказ № {0}", order.Code);

            var spleater = new StringBuilder();
            spleater.Append(Repeater(".\n", 40));

            var printDocument = new PrintDocument();
            printDocument.PrintPage += (sender, args) =>
            {
                var headerFormat = new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
                var topRect = new RectangleF
                {
                    Location = new PointF(0, 0),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(topText.ToString(), HeaderFont(brendId), 300, headerFormat).Height))
                };

                args.Graphics.DrawString(topText.ToString(), HeaderFont(brendId), Brushes.Black, topRect, headerFormat);

                var spleaterRect = new RectangleF
                {
                    Location = new PointF(0, topRect.Bottom),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(spleater.ToString(), HeaderFont(brendId), 300, headerFormat).Height))
                };

                args.Graphics.DrawString(spleater.ToString(), HeaderFont(brendId), Brushes.Black, spleaterRect, headerFormat);

                var bottomRect = new RectangleF
                {
                    Location = new PointF(0, spleaterRect.Bottom),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(bottomText.ToString(), HeaderFont(brendId), 300, headerFormat).Height))
                };

                args.Graphics.DrawString(bottomText.ToString(), HeaderFont(brendId), Brushes.Black, bottomRect, headerFormat);
            };

            // Напечатать
            AddToPrintQuery(PrinterName.Bag, printDocument, order.PointOfSaleId);
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать чеков для всех отделов кухни, которые задействованы в заказе </summary>
        ////========================================================================================
        public static void PrintCheckToAllKitchenSectionFromKitchen(CafeOrderDocument cafeOrderDocument, int? printWorkId, List<CafeOrderDocumentItem> cafeOrderDocumentItemsList)
        {
            // Секции кухни, которые есть в элементах заказа кухни
            var sections = Content.PrintWorksRepository.GetSectionsByCafeOrderId(cafeOrderDocument.ID);

            // Для каждой секции кухни
            foreach (var section in sections)
            {
                // Напечатать чек для кухни из кафе
                PrintSectionCheckFromCafeOrderDocument(cafeOrderDocument, section, cafeOrderDocumentItemsList);

                // Если есть задача печати, обновить инфу в ней
                if (printWorkId != -1)
                {
                    Content.PrintWorksRepository.AddDetailPrintWork(new DetailPrintWork
                    {
                        PrintWorksID = printWorkId,
                        State = (int)State.Compleated,
                        SectionID = section.ID
                    });
                }
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать чеков для всех отделов кухни, которые задействованы в заказе </summary>
        ////========================================================================================
        private static void PrintCheckToAllKitchenSection(Order order, int? printWorkId)
        {
            // Секции кухни, которые есть в элементах заказа
            var sections = Content.PrintWorksRepository.GetSectionsByOrderId(order.ID);

            // Для каждой секции кухни
            foreach (var section in sections)
            {
                // Напечатать чек
                PrintSectionCheckFromOrder(order, section);

                // Если есть задача печати, обновить инфу в ней
                if (printWorkId != -1)
                {
                    Content.PrintWorksRepository.AddDetailPrintWork(new DetailPrintWork
                    {
                        PrintWorksID = printWorkId,
                        State = (int)State.Compleated,
                        SectionID = section.ID
                    });
                }
            }
        }


        //___________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать чека заказа </summary>
        /// <returns> Сообщение об ошибке </returns>
        ////=======================================
        public static async Task<string> Print(Order order, int portNumber, string ipAddress, int? printWorkId)
        {
            try
            {
                // Печать чеков для всех отделов кухни, которые задействованы в заказе
                PrintCheckToAllKitchenSection(order, printWorkId);

                // Общий чек по всей кухни
                PrintSectionCheckFromOrder(order, null);

                // Чек для клиента

                await PrintClientCheck(order);

                // Если есть задача печати - обновить её
                if (printWorkId != -1)
                {
                    Content.PrintWorksRepository.AddDetailPrintWork(new DetailPrintWork
                    {
                        PrintWorksID = printWorkId,
                        State = (int)State.Compleated,
                        IsGeneral = true
                    });

                    Content.PrintWorksRepository.AddDetailPrintWork(new DetailPrintWork
                    {
                        PrintWorksID = printWorkId,
                        State = (int)State.Compleated,
                        IsClient = true
                    });
                }

                // Если нужен чек доставки
                if (order.DoCalculateDelivery)
                {
                    PrintDeliveryCheck(order);
                    PrintBagCheck(order);

                    if (printWorkId != -1)
                    {
                        Content.PrintWorksRepository.AddDetailPrintWork(new DetailPrintWork
                        {
                            PrintWorksID = printWorkId,
                            State = (int)State.Compleated,
                            IsDelivery = true
                        });

                        Content.PrintWorksRepository.AddDetailPrintWork(new DetailPrintWork
                        {
                            PrintWorksID = printWorkId,
                            State = (int)State.Compleated,
                            IsBag = true
                        });
                    }
                }
                var orderitems = Content.PrintingRepository.GetOrderItems(order);
                foreach (var orderitem in orderitems)
                {
                    orderitem.Dish.Category = Content.PrintingRepository.GetCategory(orderitem.Dish.CategoryID);
                    for (int i = 0; i < orderitem.Amount; i++)
                    {
                        if (orderitem.Dish.MarkEAC)
                        {
                            PrintDishInfoCheck(order, orderitem.Dish, false);
                        }

                        if (orderitem.Dish.MarkGOST)
                        {
                            PrintDishInfoCheck(order, orderitem.Dish, true);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return null;
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        private static string GetPaymentMethodName(Order order)
        {
            var paymentMethodAlias = new PaymentMethodAlias().GetAll().FirstOrDefault(x => x.PaymentMethod == order.PaymentMethod);

            if (paymentMethodAlias == null) return order.IsWebRequest ? "Безнал (ст)" : "Наличные (ст)";

            return paymentMethodAlias.Alias;
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать чека клиента </summary>
        ////========================================
        public static async Task PrintClientCheck(Order order)
        {
            int brendId = order.Brand.ID;

            var text = new StringBuilder();

            List<OrderItem> items = Content.OrderManager.GetOrderItemsArray(order).ToList();
            List<OrderItem> orderItems = items.ToList();
            items.AddRange(Content.SetCalculationManager.GetSets(items, true));

            int deliveryPrice = await Content.OrderManager.GetDeliveryPrice(orderItems, brendId, order.DiscountCard, order.Coupon);

            for (int i = 0; i < items.Count; i++) AddItemToClientCheck(i, items[i], ref text);

            var discount = order.TotalCostWithoutDiscount - order.TotalCost;

            var totalItems = items.Count;

            if (discount > 0)
            {
                var item = new OrderItem
                {
                    Amount = 1,
                    Dish = new Dish
                    {
                        Name = "Скидка",
                        Price = -discount,
                    }
                };

                AddItemToClientCheck(totalItems++, item, ref text);
            }

            if (order.DoCalculateDelivery && deliveryPrice > 0)
            {
                var item = new OrderItem
                {
                    Amount = 1,
                    Dish = new Dish
                    {
                        Name = "Доставка",
                        Price = deliveryPrice,
                        DiscountAppliсability = false
                    }
                };

                // ReSharper disable once RedundantAssignment
                AddItemToClientCheck(totalItems++, item, ref text);
            }

            text.Append(Spleater("*", true));
            text.Append(TotalPlace(string.Format("Итого: {0}р", order.TotalCost)));
            text.Append(Environment.NewLine);
            text.Append(TotalPlace(string.Format("Расчет: {0}", GetPaymentMethodName(order))));
            text.Append(Environment.NewLine);
            text.Append(Environment.NewLine);
            text.Append(Content.BrandManager == null ? "" : Content.GlobalOptionsManager.GetCheckFooterByBrandId(brendId));
            text.Append(Environment.NewLine);

            var printDocument = new PrintDocument();
            printDocument.PrintPage += (sender, args) =>
                {
                    string headerText = GetFirstCheckStringByOrderId(order.Brand.ID) + "\r\n";
                    args.Graphics.DrawString(headerText, SmallFont(brendId), Brushes.Black, new PointF(0, 0));
                    args.Graphics.DrawString(string.Format("        Ваш заказ № {0}", order.Code), SmallFont(brendId), Brushes.Black, new PointF(0, 18));
                    args.Graphics.DrawString("*****************************", SmallFont(brendId), Brushes.Black, new PointF(0, 36));
                    args.Graphics.DrawString(string.Format("Время доставки: {0:dd.MM.yyyy HH:mm}", order.TimeClient), SmallFont(brendId), Brushes.Black, new PointF(0, 54));
                    args.Graphics.DrawString(Spleater("*", true), SmallFont(brendId), Brushes.Black, new PointF(0, 70));
                    args.Graphics.DrawString(text.ToString(), SmallFont(brendId), Brushes.Black, new PointF(0, 88));
                };

            // Напечатать
            AddToPrintQuery(PrinterName.Client, printDocument, order.PointOfSaleId);
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        public static void PrintCaffeClientCheck(CafeOrderDocument document)
        {
            var text = new StringBuilder();

            int brendId = 1;

            for (int i = 0; i < document.CafeOrderItems.Count; i++)
            {
                AddItemToClientCheck(i, document.CafeOrderItems[i], ref text);
            }

            if (document.DiscountCardID.HasValue)
            {
                AddItemToClientCheck(document.CafeOrderItems.Count, new CafeOrderDocumentItem
                {
                    Amount = 1,
                    Dish = new Dish
                    {
                        Name = "Скидка",
                        CafePrice = (int)(document.CafeOrderItems.Sum(x => x.Amount * x.Dish.CafePrice) - document.DocumentSum)
                    }
                }, ref text);
            }

            string paymentType = "";

            switch (document.PaymentType)
            {
                case PaymentType.Money:
                    paymentType = "Наличные";
                    break;
                case PaymentType.Card:
                    paymentType = "Карта";
                    break;
                case PaymentType.NoMoney:
                    paymentType = "Безналичный расчёт";
                    break;
            }

            text.Append(Spleater("*", true));
            text.Append(TotalPlace(string.Format("Итого: {0} р", document.DocumentSum)));
            text.Append(Environment.NewLine);
            text.Append(TotalPlace(string.Format("Расчет: {0}", paymentType)));
            text.Append(Environment.NewLine);
            text.Append(Environment.NewLine);
            text.Append(Content.BrandManager == null ? "" : Content.GlobalOptionsManager.GetCheckFooterByBrandId(brendId));
            text.Append(Environment.NewLine);

            var printDocument = new PrintDocument();
            printDocument.PrintPage += (sender, args) =>
            {
                args.Graphics.DrawString(string.Format("        Ваш заказ № {0} {1}", document.Code, document.Table), SmallFont(brendId), Brushes.Black,
                                         new PointF(0, 0));
                //args.Graphics.DrawString("      ООО \"Альянс Фуд Групп\"", SmallFont(brendId), Brushes.Black, new PointF(0, 18));
                args.Graphics.DrawString("*****************************", SmallFont(brendId), Brushes.Black, new PointF(0, 18));
                args.Graphics.DrawString(string.Format("Время заказа: {0:dd.MM.yyyy HH:mm}", document.Created), SmallFont(brendId),
                                         Brushes.Black, new PointF(0, 36));
                args.Graphics.DrawString(Spleater("*", true), SmallFont(brendId), Brushes.Black, new PointF(0, 54));
                args.Graphics.DrawString(text.ToString(), SmallFont(brendId), Brushes.Black, new PointF(0, 70));
            };

            var pointOfSale = Content.PointOfSaleRepository.GetItems().FirstOrDefault();
            // Печать на устройства
            if (pointOfSale != null)
            {
                // Печать на устройство
                AddToPrintQuery(PrinterName.CafeClient, printDocument, pointOfSale.ID);
            }            
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Печать чека кафе, который относит девочка на кухню </summary>
        ////=======================================================================
        public static void PrintSectionCafeCheck(CafeOrderDocument document)
        {
            int brendId = Content.BrandManager.SelectedBrand.ID;

            var text = new StringBuilder();
            text.Append(Repeater("*", 19));

            for (int i = 0; i < document.CafeOrderItems.Count; i++)
            {
                if (document.CafeOrderItems[i].IsEditable)
                {
                    text.Append(AddKitchenItemToCheck(document.CafeOrderItems[i], i));
                }
            }

            text.Append(Spleater());

            var printDocument = new PrintDocument();
            string headerText = string.Format("КУХНЯ:{0}\nЗАКАЗ №{1} {2} \n от {3:dd.MM.yyyy}", "Кафе", document.Code, document.Table, DateTime.Now);

            printDocument.PrintPage += (sender, args) =>
            {
                var headerFormat = new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
                var headerRect = new RectangleF
                {
                    Location = new PointF(0, 0),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(headerText, HeaderFont(brendId), 300, headerFormat).Height))
                };

                args.Graphics.DrawString(headerText, HeaderFont(brendId), Brushes.Black, headerRect, headerFormat);

                var kitchenTimeRect = new RectangleF
                {
                    Location = new PointF(0, (int)headerRect.Bottom),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(string.Format("{0:HH:mm}", DateTime.Now), HeaderFont(brendId), 300, StringFormat.GenericTypographic).Height))
                };

                args.Graphics.DrawString("Время кухни: ", BodyFont(brendId), Brushes.Black,
                                         new RectangleF(new PointF(0, (int)headerRect.Bottom),
                                                        new SizeF(300, kitchenTimeRect.Height)),
                                         new StringFormat { LineAlignment = StringAlignment.Far });

                args.Graphics.DrawString(string.Format("{0:HH:mm}", DateTime.Now), HeaderFont(brendId), Brushes.Black,
                                         new RectangleF(new PointF(120, (int)headerRect.Bottom),
                                             new SizeF(300, kitchenTimeRect.Height)));

                var spleaterRect = new RectangleF
                {
                    Location = new PointF(0, (int)kitchenTimeRect.Bottom),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(Spleater(), BodyFont(brendId), 300, StringFormat.GenericTypographic).Height))
                };

                var itemsRect = new RectangleF
                {
                    Location = new PointF(0, (int)(kitchenTimeRect.Bottom - spleaterRect.Height / 2)),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(text.ToString(), ItemsFont(brendId), 300, StringFormat.GenericTypographic).Height))
                };

                args.Graphics.DrawString(text.ToString(), ItemsFont(brendId), Brushes.Black, itemsRect, StringFormat.GenericTypographic);
            };
            var pointOfSale = Content.PointOfSaleRepository.GetItems().FirstOrDefault();
            // Печать на устройства
            if (pointOfSale != null)
            {
                // Напечатать
                AddToPrintQuery(PrinterName.CafeKitchen, printDocument, pointOfSale.ID);
            }
        }
               
        //______________________________________________________________________________________________________________________________________________________________________________________
        public static void PrintSectionBarCheck(CafeOrderDocument document)
        {
            int brendId = Content.BrandManager.SelectedBrand.ID;

            var text = new StringBuilder();
            text.Append(Repeater("*", 19));

            int k = 0;

            for (int i = 0; i < document.CafeOrderItems.Count; i++)
            {
                if (document.CafeOrderItems[i] != null && document.CafeOrderItems[i].Dish != null &&
                    document.CafeOrderItems[i].IsEditable && document.CafeOrderItems[i].Dish.IsAlcohole)
                {
                    text.Append(AddKitchenItemToCheck(document.CafeOrderItems[i], i));
                    k += 1;
                }
            }

            text.Append(Spleater());

            var printDocument = new PrintDocument();
            string headerText = string.Format("БАР\nЗАКАЗ №{0} {1} \n от {2:dd.MM.yyyy}", document.Code, document.Table, DateTime.Now);

            printDocument.PrintPage += (sender, args) =>
            {
                var headerFormat = new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
                var headerRect = new RectangleF
                {
                    Location = new PointF(0, 0),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(headerText, HeaderFont(brendId), 300, headerFormat).Height))
                };

                args.Graphics.DrawString(headerText, HeaderFont(brendId), Brushes.Black, headerRect, headerFormat);

                var kitchenTimeRect = new RectangleF
                {
                    Location = new PointF(0, (int)headerRect.Bottom),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(string.Format("{0:HH:mm}", DateTime.Now), HeaderFont(brendId), 300, StringFormat.GenericTypographic).Height))
                };

                args.Graphics.DrawString("Время кухни: ", BodyFont(brendId), Brushes.Black,
                                         new RectangleF(new PointF(0, (int)headerRect.Bottom),
                                                        new SizeF(300, kitchenTimeRect.Height)),
                                         new StringFormat { LineAlignment = StringAlignment.Far });

                args.Graphics.DrawString(string.Format("{0:HH:mm}", DateTime.Now), HeaderFont(brendId), Brushes.Black,
                                         new RectangleF(new PointF(120, (int)headerRect.Bottom),
                                             new SizeF(300, kitchenTimeRect.Height)));

                var spleaterRect = new RectangleF
                {
                    Location = new PointF(0, (int)kitchenTimeRect.Bottom),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(Spleater(), BodyFont(brendId), 300, StringFormat.GenericTypographic).Height))
                };

                var itemsRect = new RectangleF
                {
                    Location = new PointF(0, (int)(kitchenTimeRect.Bottom - spleaterRect.Height / 2)),
                    Size = new SizeF(300, ((int)args.Graphics.MeasureString(text.ToString(), ItemsFont(brendId), 300, StringFormat.GenericTypographic).Height))
                };

                args.Graphics.DrawString(text.ToString(), ItemsFont(brendId), Brushes.Black, itemsRect, StringFormat.GenericTypographic);
            };

            if (k < 1)
                return;
            var pointOfSale = Content.PointOfSaleRepository.GetItems().FirstOrDefault();
            // Печать на устройства
            if (pointOfSale != null)
            {
                // Напечатать
                AddToPrintQuery(PrinterName.CafeBar, printDocument, pointOfSale.ID);
            }
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        private static void AddItemToClientCheck(int i, CafeOrderDocumentItem item, ref StringBuilder text)
        {
            int lineLength = 0;

            text.Append(i + 1 + ". ");
            text.Append(item.Dish.Name);
            text.Append(Environment.NewLine);

            int itemCost = item.Dish.CafePrice * item.Amount;

            text.Append("   ");
            lineLength += (i + 1).ToString(CultureInfo.InvariantCulture).Length + 2;

            text.Append(item.Amount);
            lineLength += item.Amount.ToString(CultureInfo.InvariantCulture).Length;

            text.Append("х");
            lineLength++;

            text.Append(item.Dish.CafePrice);
            lineLength += item.Dish.CafePrice.ToString(CultureInfo.InvariantCulture).Length;

            text.Append("р");
            lineLength++;

            lineLength += itemCost.ToString(CultureInfo.InvariantCulture).Length;

            lineLength++;

            text.Append(Repeater(".", SmallMaxStringLength - lineLength));

            text.Append(itemCost);
            text.Append("р");

            text.Append(Environment.NewLine);
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        private static void AddItemToClientCheck(int i, OrderItem item, ref StringBuilder text)
        {
            int lineLength = 0;

            text.Append(i + 1 + ". ");
            text.Append(item.Dish.Name);
            text.Append(Environment.NewLine);

            int itemCost = item.Dish.Price * item.Amount;

            text.Append("   ");
            lineLength += (i + 1).ToString(CultureInfo.InvariantCulture).Length + 2;

            text.Append(item.Amount);
            lineLength += item.Amount.ToString(CultureInfo.InvariantCulture).Length;

            text.Append("х");
            lineLength++;

            text.Append(item.Dish.Price);
            lineLength += item.Dish.Price.ToString(CultureInfo.InvariantCulture).Length;

            text.Append("р");
            lineLength++;

            lineLength += itemCost.ToString(CultureInfo.InvariantCulture).Length;
            // Длина стоимости
            lineLength++; // +1 на символ "р"

            text.Append(Repeater(".", SmallMaxStringLength - lineLength));

            text.Append(itemCost);
            text.Append("р");

            text.Append(Environment.NewLine);
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        private static string TotalPlace(string person)
        {
            var text = new StringBuilder();
            text.Append(Repeater(" ", SmallMaxStringLength - person.Length));
            text.Append(person);

            return text.ToString();
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        private static string AddKitchenItemToCheck(CafeOrderDocumentItem item, int i)
        {
            var text = new StringBuilder();
            // Вспомогательная переменная, нужна, чтобы вычислить количество точек (.......) в строке.
            int lineLength = 0;
            text.Append((i + 1) + ".");
            //lineLength +=
            //    (i + 1).ToString(CultureInfo.InvariantCulture).Length + 2;

            text.Append(item.Dish.InternalName + "\n");
            //lineLength += item.Dish.InternalName.Length;

            lineLength += item.Amount.ToString(CultureInfo.InvariantCulture).Length; // Сколько символов занимает количество блюда
            lineLength += 2; // "шт" В конце строки

            text.Append(Repeater(".", ItemMaxStringLength - lineLength));
            text.Append(item.Amount);
            text.Append("шт");
            text.Append(Environment.NewLine);

            return text.ToString();
        }


        //______________________________________________________________________________________________________________________________________________________________________________________
        private static string AddKitchenItemToCheck(OrderItem item, int i, int personCount = 0)
        {
            var text = new StringBuilder();
            // Вспомогательная переменная, нужна, чтобы вычислить количество точек (.......) в строке.
            int lineLength = 0;
            text.Append((i + 1) + ".");


            int count = item.Amount;

            if (personCount > 0 && item.Dish.InternalName == "Палочки")
            {
                count = personCount;
            }

            text.Append(item.Dish.InternalName + "\n");

            lineLength += count.ToString(CultureInfo.InvariantCulture).Length; // Сколько символов занимает количество блюда
            lineLength += 2; // "шт" В конце строки

            text.Append(Repeater(".", ItemMaxStringLength - lineLength));
            text.Append(count);
            text.Append("шт");
            text.Append(Environment.NewLine);

            return text.ToString();
        }

        //______________________________________________________________________________________________________________________________________________________________________________________
        private static string AddKitchenSticksToCheck(int i, int personCount = 0)
        {
            var text = new StringBuilder();
            // Вспомогательная переменная, нужна, чтобы вычислить количество точек (.......) в строке.
            int lineLength = 0;
            text.Append((i + 1) + ".");
            //lineLength +=
            //    (i + 1).ToString(CultureInfo.InvariantCulture).Length + 2;

            int count = personCount;

            text.Append("Палочки" + "\n");
            //lineLength += item.Dish.InternalName.Length;

            lineLength += count.ToString(CultureInfo.InvariantCulture).Length; // Сколько символов занимает количество блюда
            lineLength += 2; // "шт" В конце строки

            text.Append(Repeater(".", ItemMaxStringLength - lineLength));
            text.Append(count);
            text.Append("шт");
            text.Append(Environment.NewLine);

            return text.ToString();
        }
    }
}
