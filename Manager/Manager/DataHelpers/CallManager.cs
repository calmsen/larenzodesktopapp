﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using AgatInfinityConnector;
using LaRenzo.DataRepository.Entities.Calls;

namespace LaRenzo.DataRepository.Repositories.Call
{
    public class CallManager
    {
        /// <summary>
        /// Менеджер звонков
        /// </summary>
        public static readonly CallManager Instance = new CallManager();
        
        public IAgatInfinityConnectorFactory Factory;
        public IAgatInfinityConnector Connector;

        /// <summary> Номер оператора </summary>
        public string OperNumber { get; private set; }

        public bool IsConected;

        public bool IsOpenChoose = false;

        public bool IsInit = false;

        /// <summary> Делегат события </summary>
        public delegate void EventHandler(object sender, CallEvent e);

        /// <summary> Делегат события </summary>
        public event EventHandler NowCallEvent;

        /// <summary> Сейчас идёт разговор </summary>
        public bool IsNowSpeak;

        /// <summary> Сейчас идёт звонок </summary>
        public CallInfo NowCall;
        

        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Соедениться </summary>
        ////================================
        public void Connection(Control control, string operNumber, string connectionString = "Login:\"IntegrationUser\" Password:\"12345\" ServerAddress:\"192.168.0.8\" ServerPort:10010 BiDir")
        {
            try
            {
                OperNumber = operNumber;

                Factory = AgatInfinityConnectorManager.GetAvailableFactories().FirstOrDefault(item => item.FactoryName == "Infinity X");

                if (Factory == null)
                {
                    //MessageBox.Show(@"Factory Infinity X not found");
                }
                else
                {
                    Connector = Factory.CreateInstance(control);
                    Connector.Connected += connector_Connected;

                    Connector.Connect(connectionString, true);

                    IsInit = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Не возможно подключиться к серверу телефонии: \r\n" + ex.ToString());

            }
        }


        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Соедениться </summary>
        ////================================
        public void Disconnection()
        {
            try
            {
                Connector.Disconnect();
                Factory = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Не возможно отключиться от сервера телефонии: \r\n" + ex.ToString());

            }
        }


        static ICallManagement callManagement;

        private void connector_Connected(object sender, EventArgs e)
        {
            IsConected = true;

            callManagement = Connector.GetCallManagement(OperNumber);

            callManagement.CallCreated += (callManagement_CallCreated);
            callManagement.CallDeleted += (callManagement_CallDeleted);
            callManagement.ExtensionStateChanged += (callManagement_ExtensionStateChanged);
        }


        private void callManagement_CallCreated(object sender, CallEventArgs e)
        {
            e.Call.StateChanged += (Call_StateChanged);
            e.Call.NumberChanged += OnCallOnNumberChanged;
            e.Call.CommandsStateChanged += (Call_CommandsStateChanged);
            e.Call.DigitsSent += (Call_DigitsSent);
        }

        private async void OnCallOnNumberChanged(object se, CallEventArgs ev)
        {
            await Call_NumberChanged(se, ev);
        }

        private void callManagement_CallDeleted(object sender, CallEventArgs e)
        {
            curentCalls.Remove(e.Call);
            e.Call.StateChanged -= (Call_StateChanged);
            e.Call.NumberChanged -= OnCallOnNumberChanged;
            e.Call.CommandsStateChanged -= (Call_CommandsStateChanged);
            e.Call.DigitsSent -= (Call_DigitsSent);
        }


        ////__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Определился номер </summary>
        ////======================================
        private async Task Call_NumberChanged(object sender, CallEventArgs e)
        {
            // Обработать звонок
            CallInfo ci = await ProcessNewCall(e.Call);

            //
            NowCall = ci;

            // Послать звонок в форму
            OnNowCall(sender, new CallEvent(ci));
        }


        ////__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Изменился статус звонка </summary>
        ////============================================
        private void Call_StateChanged(object sender, CallStateEventArgs e)
        {
            switch (e.NewState)
            {
                case CallState.Ringing:
                    IsNowSpeak = false;
                    break;
                case CallState.Disconnected:
                    if (e.OldState == CallState.Ringing)
                    {
                        curentCalls.Remove(e.Call);

                        NowCall.CallData.Droped = DateTime.Now;
                        NowCall.CallData.StatusName = "Пропущен";
                        Content.CallRepository.Save(NowCall.CallData);
                        IsNowSpeak = false;
                        NowCall = null;
                    }
                    else if (e.OldState == CallState.DialNumber)
                    {
                        NowCall.CallData.StatusName = "Отмена вызова";
                        Content.CallRepository.Save(NowCall.CallData);
                        IsNowSpeak = false;
                        NowCall = null;
                    }
                    else
                    {
                        NowCall.CallData.StatusName = "В ожидании";
                        Content.CallRepository.Save(NowCall.CallData);
                        IsNowSpeak = false;
                        NowCall = null;
                    }
                    break;
                case CallState.Finished:
                    if (NowCall != null)
                    {
                        if (e.OldState == CallState.Disconnected)
                        {
                            NowCall.CallData.Droped = DateTime.Now;
                            NowCall.CallData.StatusName = "Пропущен";
                            Content.CallRepository.Save(NowCall.CallData);
                            IsNowSpeak = false;
                            NowCall = null;
                        }
                        else
                        {
                            NowCall.CallData.Droped = DateTime.Now;
                            NowCall.CallData.StatusName = "Завершён";
                            Content.CallRepository.Save(NowCall.CallData);
                            IsNowSpeak = false;
                            NowCall = null;
                        }
                    }
                    break;
                case CallState.Connected:
                    NowCall.CallData.StatusName = "Разговор";
                    IsNowSpeak = true;
                    Content.CallRepository.Save(NowCall.CallData);
                    break;
                case CallState.Hold:
                    NowCall.CallData.StatusName = "На удержании";
                    Content.CallRepository.Save(NowCall.CallData);
                    IsNowSpeak = false;
                    NowCall = null;
                    break;
                case CallState.DialNumber:
                    NowCall.CallData.StatusName = "Набор номера";
                    Content.CallRepository.Save(NowCall.CallData);
                    IsNowSpeak = true;
                    break;
                case CallState.Reminder:
                    NowCall.CallData.StatusName = "???";
                    Content.CallRepository.Save(NowCall.CallData);
                    break;
                case CallState.Alerting:
                    NowCall.CallData.StatusName = "Alerting";
                    Content.CallRepository.Save(NowCall.CallData);
                    break;
                default:
                    break;

            }



        }


        private void Call_CommandsStateChanged(object sender, CallEventArgs e)
        {
            //AddCMLog("CallCommandsStateChanged: ID=" + e.Call.ID);
            //this.RefreshButtons();
        }

        private void Call_DigitsSent(object sender, CallDigitsSentEventArgs e)
        {
            //AddCMLog("CallDigitsSent: ID=" + e.Call.ID + " Digits=" + e.Digits);
        }


        //______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Функция вызова события - ЗВОНОК </summary>
        ////====================================================
        public void OnNowCall(object sender, CallEvent e)
        {
            EventHandler handler = NowCallEvent;

            if (handler != null)
            {
                handler.Invoke(sender, e);
            }

        }


        //______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Ответить на звонок </summary>
        ////=======================================
        public void AnswerCall(CallData cd)
        {
            foreach (ICall c in curentCalls)
            {
                if (c.ID == cd.IdCall)
                {
                    bool isAnswer = AnswerCall(c);

                    // Если получилось ответить
                    if (isAnswer)
                    {
                        // Установить время ответа
                        cd.Answed = DateTime.Now;
                        cd.IsOnline = true;
                        cd.StatusName = "Разговор";
                        cd.User = Content.UserManager.UserLogged;

                        Content.CallRepository.Save(cd);
                    }
                }
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Ответить на звонок </summary>
        ////=======================================
        public bool AnswerCall(ICall call)
        {
            bool isAnswer = false;

            bool canAnswer = call.CanAnswer;
            if (canAnswer)
            {
                isAnswer = call.Answer();
            }

            return isAnswer;
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        public void DropCall(CallData cd)
        {
            for (int i = 0; i < curentCalls.Count; i++)
            {
                if (curentCalls[i].Number == cd.PhoneNumber)
                {
                    bool isDroped = DropCall(curentCalls[i]);

                    // Если получилось ответить
                    if (isDroped)
                    {
                        // Установить время завершения
                        cd.Droped = DateTime.Now;
                        cd.IsOnline = false;
                        cd.StatusName = "Завершён";

                        Content.CallRepository.Save(cd);
                    }
                }
            }
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        public bool DropCall(ICall call)
        {
            bool isDroped = false;

            if (call.CanDrop)
            {
                isDroped = call.Drop();

                curentCalls.Remove(call);
            }

            return isDroped;
        }



        private List<ICall> curentCalls = new List<ICall>();

        public List<ICall> CurentCalls
        {
            get
            {
                return curentCalls;
            }
        }


        //________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Позвонить </summary>
        ////==============================
        public async Task<CallInfo> Call(string number)
        {
            if (number.Length == 10 && number[0] != '8' && number[0] != '7')
            {
                number = "8" + number;
            }


            var cd = new CallData
            {
                PhoneNumber = number,
                Created = DateTime.Now,
                IsEnter = false,
                User = Content.UserManager.UserLogged,
                Session = await Content.SessionManager.GetOpened()
            };

            var ci = new CallInfo(cd);
            await ci.Initialyze(Content.CallRepository.Save, Content.OrderManager.GetOrdersByPhone);
            NowCall = ci;

            Content.CallRepository.Save(cd);

            if (IsConected && callManagement != null)
            {
                callManagement = Connector.GetCallManagement(OperNumber);

                ICall call = callManagement.MakeCall(number, "");
                curentCalls.Add(call);

                cd.IdCall = call.ID;
            }

            Content.CallRepository.Save(cd);


            return ci;
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        private void callManagement_ExtensionStateChanged(object sender, ExtensionStateChangedArgs e)
        {

            //AddCMLog("ExtensionStateChanged: Extension=" + e.Extension + " State=" + e.State.ToString());
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Обработать новый звонок </summary>
        ////============================================
        private async Task<CallInfo> ProcessNewCall(ICall iCall)
        {
            curentCalls.Add(iCall);

            return await GetCallInfoByPhone(iCall.Number, iCall.ID, "Входящий");
        }


        //_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
        /// <summary> Вернуть информацию по обратному звонку </summary>
        ////===========================================================
        public async Task<CallInfo> GetCallInfoByPhone(string phoneNumber, string callId = "", string comment = "Обратный звонок")
        {
            var cd = new CallData
            {
                Created = DateTime.Now,
                IdCall = callId,
                PhoneNumber = phoneNumber,
                Session = await Content.SessionManager.GetOpened(),
                User = Content.UserManager.UserLogged,
                IsEnter = true,
                StatusName = comment
            };

            var ci = new CallInfo(cd);
            await ci.Initialyze(Content.CallRepository.Save, Content.OrderManager.GetOrdersByPhone);
            NowCall = ci;

            return ci;
        }


        public async Task<bool> IsMissedCall()
        {
            numbs = new List<Numb>();

            var list = await Content.CallRepository.ListCalls();
            if (list != null)
            {
                List<CallData> cdSort = list.OrderBy(x => x.Created).ToList();

                int l = 0;

                foreach (CallData cd in cdSort)
                {
                    if (l > 120)
                    {

                    }

                    if (cd.StatusName == "Пропущен")
                    {
                        AddNumber(cd.PhoneNumber);
                    }
                    else
                    {
                        TestNumber(cd.PhoneNumber);
                    }

                    l++;
                }
            }
            return numbs.Count > 0;
        }


        private void TestNumber(string number)
        {
            for (int i = 0; i < numbs.Count; i++)
            {
                if (numbs[i].Phone == number)
                {
                    numbs[i] = new Numb { Phone = number, Count = numbs[i].Count - 1 };

                    if (numbs[i].Count >= 0)
                    {
                        numbs.Remove(numbs[i]);
                    }
                }
            }
        }


        private void AddNumber(string number)
        {
            bool isFind = false;

            for (int i = 0; i < numbs.Count; i++)
            {
                if (numbs[i].Phone == number)
                {
                    numbs[i] = new Numb { Phone = number, Count = numbs[i].Count + 1 };
                    break;
                }
            }

            if (!isFind)
            {
                numbs.Add(new Numb { Phone = number, Count = 1 });
            }
        }


        private List<Numb> numbs = new List<Numb>();

        private CallManager()
        {
        }

        struct Numb
        {
            public string Phone;
            public int Count;
        }        
    }
}
