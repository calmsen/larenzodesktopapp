﻿using System;
using System.Collections.Generic;
using System.Linq;
using LaRenzo.DataRepository.Entities.Catalogs.Dishes;

namespace LaRenzo.DataRepository.Repositories.Reports.DishCombinationReport
{
    public class DishCombinationManager
    {
        List<DishCombinationSource> _data = new List<DishCombinationSource>();

        private readonly int _dishAmount;

        private int[] _bestCombo;

        public int TotalProcessed;

        private DishCombinationDataStorage _dataStorage;

        public List<Tuple<string, int>> TopList { get; set; }

        public DishCombinationDataStorage DataStorage
        {
            get { return _dataStorage; }
        }

        public DishCombinationManager(int dishAmount)
        {
            _dishAmount = dishAmount;
            _bestCombo = new int[_dishAmount];


            TopList = new List<Tuple<string, int>>();

        }

        public void GetMostPopularDishCombination(IEnumerable<int> categoriesIDs, int searchAmount = 10)
        {
            _data = Content.DishCombinationRepository.GetMostPopularDishCombination(_dishAmount);
            var actualDishList = Content.DishCombinationRepository.GetActualDishList(categoriesIDs);


            _dataStorage = new DishCombinationDataStorage(_dishAmount, _data, categoriesIDs, actualDishList);
            if (!_dataStorage.CanBeCalculated) return;

            do
            {
                var amount = _dataStorage.GetCurrentElementCount();

                var valueToEnter = GetTopListValueToEnter(searchAmount);

                if (amount > valueToEnter)
                {
                    _dataStorage.MaxAmount = valueToEnter;
                    _bestCombo = _dataStorage.GetIndexes();

                    TopList.Add(new Tuple<string, int>(string.Join(", ", _dataStorage.GetDishList(_bestCombo).Select(x => x.Name)), amount));

                }


                //Console.WriteLine("{0} :: {1}", string.Join(", ", dataStorage.GetIndexes()), amount);
                TotalProcessed++;


            } while (_dataStorage.GoToNextElement());


        }

        /// <summary>
        /// Возвращает значение, которое обеспечит попадание блюда в TopList
        /// </summary>
        /// <param name="searchAmount"></param>
        /// <returns></returns>
        public int GetTopListValueToEnter(int searchAmount)
        {
            if (TopList.Count < searchAmount) return 0;
            var some = TopList.OrderByDescending(x => x.Item2).Take(searchAmount).ToList();
            return some.Min(x => x.Item2);
        }

        public int[] GetCurrentIndexes()
        {
            return DataStorage == null ? new int[] { } : DataStorage.GetIndexes();
        }
    }

    

    

    

}

