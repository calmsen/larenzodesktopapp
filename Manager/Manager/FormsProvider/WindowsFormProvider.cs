﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace LaRenzo.FormsProvider
{
    public class WindowsFormProvider
    {
        public static void FillFormsParams(Control control, Dictionary<string, string> parameters)
        {
            if (control.Tag == null) return;

            string key = control.Tag.ToString();

            if (parameters.ContainsKey(key))
            {
                control.Text = parameters[key];
            }
        }

        public static void FillControlsFromModel<T>(Control parent, T model) where T : class
        {
            if (model == null)
                return;

            Dictionary<string, PropertyInfo> fields = GetModelFields(model);
            var controlList = GetControlHierarchy(parent).ToList();

            foreach (var control in controlList)
            {
                if (control.Tag != null)
                {
                    var key = control.Tag.ToString().ToLower();
                    if (fields.ContainsKey(key))
                    {
                        var type = control.GetType().Name;
                        var value = fields[key].GetValue(model, null);
                        switch (type)
                        {
                            case "CheckBox":
                                {
                                    control.GetType()
                                           .GetProperty("Checked")
                                           .SetValue(control, value ?? false, null);
                                    break;
                                }
                            case "TextBox":
                                {
                                    control.Text = value == null ? null : fields[key].GetValue(model, null).ToString();
                                    break;
                                }
                            case "RichTextBox":
                                {
                                    control.Text = value == null ? null : fields[key].GetValue(model, null).ToString();
                                    break;
                                }
                            case "ComboBox":
                                {
                                    control.GetType().GetProperty("SelectedValue").SetValue(control, value ?? -1, null);
                                    break;
                                }
                            case "DateTimePicker":
                                {
                                    control.GetType()
                                           .GetProperty("Value")
                                           .SetValue(control, value ?? DateTime.MinValue, null);
                                    break;
                                }
                        }
                    }
                }
            }
        }

        private static IEnumerable<Control> GetControlHierarchy(Control root)
        {
            var queue = new Queue<Control>();

            queue.Enqueue(root);

            do
            {
                var control = queue.Dequeue();

                yield return control;

                foreach (var child in control.Controls.OfType<Control>())
                    queue.Enqueue(child);

            } while (queue.Count > 0);

        }

        private static Dictionary<string, PropertyInfo> GetModelFields<T>(T model) where T : class
        {
            if (model == null)
                return null;

            var fields = model.GetType().GetProperties().ToDictionary(property => property.Name.ToLower(), property => property);

            return fields;
        }

        public static void ShowForm(Form form)
        {
            form.Show();
            form.Focus();
        }
    }
}
